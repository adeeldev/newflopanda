"use strict";



const $ = window.jQuery;
$(window).load(function () {
    $(function () {
        // config
        const REPLAY_SCALE = 0.999;
        const SPEED = 1;
        let arr = [];
        const $body = $('body');
        //$play.attr('disabled', 1);
        let xhr2 = new XMLHttpRequest();
        xhr2.open('GET', 'http://localhost:3000/api/common/authenticate/getRecordingIds/N_wAyldv5', true);
        xhr2.onload = function () {
            if (xhr2.status == 200) {
                let data = JSON.parse(this.response);
                arr = data.data;
                let currentIndex2 = localStorage.getItem("currentIndexLocal");
                let valid2 = localStorage.getItem("validLocal", "true")
                if (valid2 === 'true' && Number(currentIndex2) > data.data.length) {
                    localStorage.clear();
                }
                let currentIndexLocal = localStorage.getItem("currentIndexLocal");
                let valid = localStorage.getItem("validLocal", "true");
                if (valid === null || valid === '' || valid === 'false') {
                    localStorage.setItem('currentIndexLocal', 0)
                    getRecordingDetail(data.data[0].id);
                } else {
                    if (currentIndexLocal !== null || currentIndexLocal !== '' && Number(currentIndexLocal) <= data.data.length && valid === 'true') {
                        localStorage.setItem('currentIndexLocal', (Number(currentIndexLocal) + 1))
                        getRecordingDetail(data.data[Number(currentIndexLocal) + 1].id);
                    }
                }
            }
        }
        xhr2.send(null);


        function getRecordingDetail(recordingId) {
            let xhr = new XMLHttpRequest();
            xhr.open('GET', 'http://192.168.0.148:80/api/common/authenticate/sharedRecording/' + recordingId, true);
            xhr.onload = function () {
                if (xhr.status == 200) {
                    //$play.attr('enabled', 1)
                    let data = JSON.parse(this.response);
                    console.log(data.data.rec);
                    const $iframe = $('<iframe>');
                    $iframe.height(data.data.rec.height * REPLAY_SCALE);
                    $iframe.width(data.data.rec.width * REPLAY_SCALE);
                    $iframe.css({
                        '-ms-zoom': `${REPLAY_SCALE}`,
                        '-moz-transform': `scale(${REPLAY_SCALE})`,
                        '-moz-transform-origin': `0 0`,
                        '-o-transform': `scale(${REPLAY_SCALE})`,
                        '-o-transform-origin': `0 0`,
                        '-webkit-transform': `scale(${REPLAY_SCALE})`,
                        '-webkit-transform-origin': `0 0`
                    });
                    $body.append($iframe);

                    // Load HTML
                    $iframe[0].contentDocument.documentElement.innerHTML = data.data.rec.htmlCopy;
                    const $iframeDoc = $($iframe[0].contentDocument.documentElement);

                    // Insert fake cursor
                    const $fakeCursor = $('<div class="cursor"></div>')
                    $iframeDoc.find('body').append($fakeCursor);

                    let i = 0;
                    const startPlay = Date.now();

                    (function draw() {
                        let event = data.data.rec.events[i];
                        if (!event) {
                            return;
                        }
                        let offsetRecording = event.time - data.data.rec.recordingStartTime;
                        let offsetPlay = (Date.now() - startPlay) * SPEED;
                        if (offsetPlay >= offsetRecording) {
                            drawEvent(event, $fakeCursor, $iframeDoc);
                            i++;
                        }

                        if (i < data.data.rec.events.length) {
                            requestAnimationFrame(draw);
                        } else {
                            $iframe.remove();
                            let currentIndexLocal = localStorage.getItem("currentIndexLocal");
                            if (Number(currentIndexLocal) === (arr.length - 1)) {
                                localStorage.clear();
                                window.close();
                            } else {
                                localStorage.setItem("validLocal", "true")
                                window.location.href = window.location.href;
                            }
                        }
                    })();
                }
            };
            xhr.send(null);
        }




        function drawEvent(event, $fakeCursor, $iframeDoc) {

            if (event.type === 'click' || event.type === 'mousemove') {
                //debugger

                $fakeCursor.css({
                    // position:"relative",
                    top: event.y,
                    left: event.x - 25
                });
            }
            if (event.type === 'hideFeedback') {

                $iframeDoc.find("#" + event.id).css({
                    display: "none"
                });

            }
            if (event.type === 'click') {

                this.flashClass($fakeCursor, 'click');
                //const path =  this.getPath(event.target);
                const $element = $iframeDoc.find(event.path);
                this.flashClass($element, 'clicked');
            }

            if (event.type === 'keypress') {

                //const path = window.jQuery(event.target).getPath();
                const $element = $iframeDoc.find(event.path);
                $element.trigger({ type: 'keypress', keyCode: event.keyCode })
                $element.val(event.value);
            }
            if (event.type === 'change') {


                $iframeDoc.find(event.path).append("");
                $iframeDoc.find(event.path).text(event.value);
            }
            if (event.type === 'newNode') {

                $iframeDoc.find(event.path).append(event.innerHTML);
            }
            if (event.type === 'radio') {

                $iframeDoc.find("#" + event.id).attr("checked", true);

            }
            if (event.type === 'highlight') {
                $iframeDoc.find(event.path).html(event.value);
            }
            if (event.type === 'clearHighlight') {

                $iframeDoc.find(event.path).html(event.value);
            }
            if (event.type === 'scroll') {


                var $scrollTo = $.scrollTo = function (target, duration, settings) {
                    return $(window).scrollTo(target, duration, settings);
                };

                $scrollTo.defaults = {
                    axis: 'xy',
                    duration: 0,
                    limit: true
                };

                function isWin(elem) {
                    return !elem.nodeName ||
                        $.inArray(elem.nodeName.toLowerCase(), ['iframe', '#document', 'html', 'body']) !== -1;
                }

                $.fn.scrollTo = function (target, duration, settings) {
                    if (typeof duration === 'object') {
                        settings = duration;
                        duration = 0;
                    }
                    if (typeof settings === 'function') {
                        settings = { onAfter: settings };
                    }
                    if (target === 'max') {
                        target = 9e9;
                    }

                    settings = $.extend({}, $scrollTo.defaults, settings);
                    // Speed is still recognized for backwards compatibility
                    duration = duration || settings.duration;
                    // Make sure the settings are given right
                    var queue = settings.queue && settings.axis.length > 1;
                    if (queue) {
                        // Let's keep the overall duration
                        duration /= 2;
                    }
                    settings.offset = both(settings.offset);
                    settings.over = both(settings.over);

                    return this.each(function () {
                        // Null target yields nothing, just like jQuery does
                        if (target === null) return;

                        var win = isWin(this),
                            elem = win ? this.contentWindow || window : this,
                            $elem = $(elem),
                            targ = target,
                            attr = {},
                            toff;

                        switch (typeof targ) {
                            // A number will pass the regex
                            case 'number':
                            case 'string':
                                if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)) {
                                    targ = both(targ);
                                    // We are done
                                    break;
                                }
                                // Relative/Absolute selector
                                targ = win ? $(targ) : $(targ, elem);
                                / falls through /
                            case 'object':
                                if (targ.length === 0) return;
                                // DOMElement / jQuery
                                if (targ.is || targ.style) {
                                    // Get the real position of the target
                                    toff = (targ = $(targ)).offset();
                                }
                        }

                        var offset = $.isFunction(settings.offset) && settings.offset(elem, targ) || settings.offset;

                        $.each(settings.axis.split(''), function (i, axis) {
                            var Pos = axis === 'x' ? 'Left' : 'Top',
                                pos = Pos.toLowerCase(),
                                key = 'scroll' + Pos,
                                prev = $elem[key](),
                                max = $scrollTo.max(elem, axis);

                            if (toff) {// jQuery / DOMElement
                                attr[key] = toff[pos] + (win ? 0 : prev - $elem.offset()[pos]);

                                // If it's a dom element, reduce the margin
                                if (settings.margin) {
                                    attr[key] -= parseInt(targ.css('margin' + Pos), 10) || 0;
                                    attr[key] -= parseInt(targ.css('border' + Pos + 'Width'), 10) || 0;
                                }

                                attr[key] += offset[pos] || 0;

                                if (settings.over[pos]) {
                                    // Scroll to a fraction of its width/height
                                    attr[key] += targ[axis === 'x' ? 'width' : 'height']() * settings.over[pos];
                                }
                            } else {
                                var val = targ[pos];
                                // Handle percentage values
                                attr[key] = val.slice && val.slice(-1) === '%' ?
                                    parseFloat(val) / 100 * max
                                    : val;
                            }

                            // Number or 'number'
                            if (settings.limit && /^\d+$/.test(attr[key])) {
                                // Check the limits
                                attr[key] = attr[key] <= 0 ? 0 : Math.min(attr[key], max);
                            }

                            // Don't waste time animating, if there's no need.
                            if (!i && settings.axis.length > 1) {
                                if (prev === attr[key]) {
                                    // No animation needed
                                    attr = {};
                                } else if (queue) {
                                    // Intermediate animation
                                    animate(settings.onAfterFirst);
                                    // Don't animate this axis again in the next iteration.
                                    attr = {};
                                }
                            }
                        });

                        animate(settings.onAfter);

                        function animate(callback) {
                            var opts = $.extend({}, settings, {
                                // The queue setting conflicts with animate()
                                // Force it to always be true
                                queue: true,
                                duration: duration,
                                complete: callback && function () {
                                    callback.call(elem, targ, settings);
                                }
                            });
                            $elem.animate(attr, opts);
                        }
                    });
                };

                $scrollTo.max = function (elem, axis) {
                    var Dim = axis === 'x' ? 'Width' : 'Height',
                        scroll = 'scroll' + Dim;

                    if (!isWin(elem))
                        return elem[scroll] - $(elem)[Dim.toLowerCase()]();

                    var size = 'client' + Dim,
                        doc = elem.ownerDocument || elem.document,
                        html = doc.documentElement,
                        body = doc.body;

                    return Math.max(html[scroll], body[scroll]) - Math.min(html[size], body[size]);
                };

                function both(val) {
                    return $.isFunction(val) || $.isPlainObject(val) ? val : { top: val, left: val };
                }

                // Add special hooks so that window scroll properties can be animated
                $.Tween.propHooks.scrollLeft =
                    $.Tween.propHooks.scrollTop = {
                        get: function (t) {
                            return $(t.elem)[t.prop]();
                        },
                        set: function (t) {
                            var curr = this.get(t);
                            // If interrupt is true and user scrolled, stop animating
                            if (t.options.interrupt && t._last && t._last !== curr) {
                                return $(t.elem).stop();
                            }
                            var next = Math.round(t.now);
                            if (curr !== next) {
                                $(t.elem)[t.prop](next);
                                t._last = this.get(t);
                            }
                        }
                    };

                $("#myIframe").scrollTo(event.y, 0, { queue: false });


            }

        }


        function flashClass($el, className) {
            $el.addClass(className).delay(200).queue(() => $el.removeClass(className).dequeue());
        }
    });

});
