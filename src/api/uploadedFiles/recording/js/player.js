const $ = window.jQuery;
let baseUrl = "https://www.flopanda.com/v1";
let seconds = 0;
let SPEED = 1;
let i = 0;
var timeInterval;
var startPlay;
var $fakeCursor;
var $iframeDoc;
var $iframe;
var stopRecoriding = false;
const REPLAY_SCALE = 0.999;
const $body = $('body');
let RecordingRecord = {};
let arr = [];
let createIframe = false;
var totalTimeGap = 0;
var changePlayerDuration = true;
var formInteractLength = -1;
var formInteractIndex = -1;
var clickRageLength = -1;
var clickRageIndex = -1;
var clickErrorLength = -1;
var clickErrorIndex = -1;
var submitLength = -1;
var submitIndex = -1;

$(window).on('load', function () {
    $(function () {
        let xhr2 = new XMLHttpRequest();
        xhr2.open('GET', baseUrl + '/session/N_wAyldv5/recording', true);
        xhr2.setRequestHeader('Authorization', 'Bearer U2FsdGVkX1+1mAdrO2E/pJ0sUrN3MuzzS4RGGV08QBZp9mE0THBBUXRD+o7ckxUgDGYXwCo6sNVtFngcbC4z6w==');
        xhr2.setRequestHeader('x-auth-token', 'bTaRn943gKfChM1');
        xhr2.onload = function () {
            if (xhr2.status == 200) {
                localStorage.clear();
                let data = JSON.parse(this.response);
                arr = data.data;
                let select = document.getElementById("pageName");
                for (let index = 0; index < data.data.length; index++) {
                    opt = document.createElement("option");
                    opt.value = index;
                    opt.textContent = data.data[index].pageName;
                    select.appendChild(opt);
                }
                let currentIndex2 = localStorage.getItem("currentIndexLocal");
                let valid2 = localStorage.getItem("validLocal", "true")
                if (valid2 === 'true' && Number(currentIndex2) > data.data.length) {
                    localStorage.clear();
                }
                let currentIndexLocal = localStorage.getItem("currentIndexLocal");
                let valid = localStorage.getItem("validLocal", "true");
                if (valid === null || valid === '' || valid === 'false') {
                    localStorage.setItem('currentIndexLocal', 0)
                    getRecordingDetail(data.data[0]._id);
                } else {
                    if (currentIndexLocal !== null || currentIndexLocal !== '' && Number(currentIndexLocal) <= data.data.length && valid === 'true') {
                        localStorage.setItem('currentIndexLocal', (Number(currentIndexLocal) + 1))
                        getRecordingDetail(data.data[Number(currentIndexLocal) + 1]._id);
                    }
                }
                selectBox();
            }
        }
        xhr2.send(null);
    });
});
function flashClass($el, className) {
    $el.addClass(className).delay(200).queue(() => $el.removeClass(className).dequeue());
}
function getRecordingDetail(recordingId) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', baseUrl + '/recording/' + recordingId, true);
    xhr.setRequestHeader('Authorization', 'Bearer U2FsdGVkX1+1mAdrO2E/pJ0sUrN3MuzzS4RGGV08QBZp9mE0THBBUXRD+o7ckxUgDGYXwCo6sNVtFngcbC4z6w==');
    xhr.setRequestHeader('x-auth-token', 'bTaRn943gKfChM1');
    xhr.onload = function () {
        if (xhr.status == 200) {
            //$play.attr('enabled', 1)
            let data = JSON.parse(this.response);
            RecordingRecord = data;
            drawGradient();
            if (createIframe === false) {
                if (data.data.recording.device === "Mobile") {
                    createdIframe = '<iframe id="myIframe" class="iframe-set mobile-iframe">'
                } else if (data.data.recording.device === "Tablet") {
                    createdIframe = '<iframe id="myIframe" class="iframe-set tablet-iframe">'
                }
                else {
                    createdIframe = '<iframe id="myIframe">'
                }
                $iframe = $(createdIframe);
                $iframe.height(data.data.recording.height * REPLAY_SCALE);
                $iframe.width(data.data.recording.width * REPLAY_SCALE);
                $iframe.css({
                    '-ms-zoom': `${REPLAY_SCALE}`,
                    '-moz-transform': `scale(${REPLAY_SCALE})`,
                    '-moz-transform-origin': `0 0`,
                    '-o-transform': `scale(${REPLAY_SCALE})`,
                    '-o-transform-origin': `0 0`,
                    '-webkit-transform': `scale(${REPLAY_SCALE})`,
                    '-webkit-transform-origin': `0 0`
                });
                if (data.data.recording.device === "Desktop") {
                    let a = (window.jQuery(window).width() - 30) / data.data.recording.width;
                    $iframe[0].style.transform = `scale(${a})`
                    $iframe[0].style.transformOrigin = '0% 0% 0px';
                }
                $('#iframeScreen').append($iframe);
                //createIframe = true;
                // let splitHrefHtml = data.data.recording.htmlCopy.split("href='//").join('href="https://');
                // let splitSrcHtml = splitHrefHtml.split("src='//").join('src="https://');
                setTimeout(() => {
                    $iframe[0].contentDocument.documentElement.innerHTML = data.data.recording.htmlCopy;
                    $iframeDoc = $($iframe[0].contentDocument.documentElement);
                    for (let index = 0; index < data.data.recording.styles.length; index++) {
                        $iframeDoc.find('head').append('<style type="text/css">' + data.data.recording.styles[index] + '</style>');
                    }
                    $iframeDoc.find('body').css({
                        'overflow-y': 'hidden ',
                        'cursor': 'none ',
                        'pointerEvents': 'none '
                    });
                    $iframeDoc.find('footer').css({
                        'overflow-y': 'hidden ',
                        'cursor': 'none ',
                        'pointerEvents': 'none '
                    });
                    $iframeDoc.find('header').css({
                        'overflow-y': 'hidden ',
                        'cursor': 'none ',
                        'pointerEvents': 'none'
                    });
                    $fakeCursor = $('<div id="pandaCursor" style="position:absolute;z-index:9000000000000000;" class="cursor"><img width="13" style="margin-top: 3px" src="./cursor.png" /></div>')
                    $iframeDoc.find('body').append($fakeCursor);
                    for (let i = 0; i < data.data.excludedContent.length; i++) {
                        $iframeDoc.find(data.data.excludedContent[0]).css('visibility', 'hidden');
                    }
                    let node = document.getElementById('tags');
                    while (node.hasChildNodes()) {
                        node.removeChild(node.lastChild);
                    }
                    document.getElementById('seconds-duration').innerText = formatDuration(data.data.recording.duration);
                    document.getElementById('startTime').innerText = moment(data.data.recording.startTime).format("YYYY-MM-DD , h:mm:ss a");
                    document.getElementById('rec-duration').innerText = formatDuration(data.data.recording.duration);
                    document.getElementById('location').innerText = data.data.recording.countryName;
                    document.getElementById('ip').innerText = data.data.recording.ip;
                    // document.getElementById('height').innerText = data.data.recording.height;
                    // document.getElementById('resolution').innerText = data.data.recording.resolution;
                    document.getElementById('referer').innerText = data.data.recording.referer;
                    document.getElementById("device").innerText = data.data.recording.device;
                    document.getElementById("browserName").innerText = data.data.recording.browserName;
                    document.getElementById("osName").innerText = data.data.recording.osName;
                    document.getElementById("loader").style.visibility = "hidden";
                    startPlay = Date.now();
                    document.getElementById('seekbar').max = data.data.recording.duration * 100;
                    if (data.data.recording.formInteract === 1) {
                        $("#tags").append("<span id='tagName' onclick='goToFormInteract()' class='tag tag-label'>form-interact</span>");
                    }
                    if (data.data.recording.formSubmit === 1) {
                        $("#tags").append("<span id='tagName' onclick='goToSubmit()' class='tag tag-label'>Submit</span>");
                    }
                    if (data.data.recording.clickRage === 1) {
                        $("#tags").append("<span id='tagName' onclick='goToClickRage()' class='tag tag-label'>Click Rage</span>");
                    }
                    if (data.data.recording.clickError === 1) {
                        $("#tags").append("<span id='tagName' onclick='goToClickError()' class='tag tag-label'>Click Error</span>");
                    }
                    if (data.data.recording.formInteract === 1 &&
                        data.data.recording.formSubmit === 0) {
                        $("#tags").append("<span id='tagName' class='tag tag-label'>form-dropOff</span>");
                    }

                    timeInterval = setInterval(showTimer, 10);

                    playVideo();
                }, 2000);
            }
        }
    };
    xhr.send(null);
}
function showTimer() {
    if (stopRecoriding === false) {
        if (seconds < RecordingRecord.data.recording.duration) {
            seconds += 0.01;
            document.getElementById('seekbar').value = Number(document.getElementById('seekbar').value) + 1;
            document.getElementById('seconds-counter').innerText = formatDuration(seconds.toFixed(0));
        }
    }
}
drawEvent = (event, $fakeCursor, $iframeDoc) => {
    if (event.type === 'click' || event.type === 'mousemove') {
        $iframeDoc.find('#pandaCursor').css({
            // position:"relative",
            top: event.y - 25,
            left: event.x - 25
        });
    }
    if (event.type === 'hideFeedback') {
        $iframeDoc.find("#" + event.id).css({
            display: "none"
        });

    }
    if (event.type === "hideModal") {
        $iframeDoc.find("#" + event.id).css({
            display: "none"
        });
    }
    if (event.type === 'click') {

        this.flashClass($fakeCursor, 'click');
        //const path =  this.getPath(event.target);
        const $element = $iframeDoc.find(event.path);
        this.flashClass($element, 'clicked');
    }

    if (event.type === 'keypress') {
        //const path = window.jQuery(event.target).getPath();
        const $element = $iframeDoc.find(event.path);
        // $element.prop('type', 'password');
        //$element.trigger({ type: 'keypress', keyCode: event.keyCode })
        $element.val(event.eValue);
    }
    if (event.type === 'change') {

        $iframeDoc.find(event.path).append("");
        $iframeDoc.find(event.path).text(event.eValue);
    }
    // if (event.type === 'newNode') {

    //     $iframeDoc.find(event.path).append(event.innerHTML);
    // }
    if (event.type === 'addChildList') {

        if (event.id !== undefined || "") {

            if ($iframeDoc.find("#" + event.id).length == 0) {
                let path = $iframeDoc.find(event.path);
                path.append(event.innerHTML);
            } else {
                let path = $iframeDoc.find("#" + event.id);
                path.html(event.innerHTML);
            }
        } else {
            let path = $iframeDoc.find(event.path);
            path.append(event.innerHTML);
        }
    }
    if (event.type === 'removeChildList') {
        if (event.id !== undefined || "") {
            if ($iframeDoc.find("#" + event.id).length == 0) {
                let path = $iframeDoc.find(event.path);
                path.remove();
            } else {
                let path = $iframeDoc.find("#" + event.id);
                path.remove();
            }
        } else {
            let path = $iframeDoc.find(event.path);
            path.remove();
        }

    }
    if (event.type === 'classAttributes') {
        if (event.id !== undefined || "") {
            $iframeDoc.find("#" + event.id).removeClass().addClass(event.className)
        } else {
            $iframeDoc.find(event.path).removeClass().addClass(event.className);
        }

    }
    if (event.type === 'styleAttributes') {
        if (event.id !== undefined || "") {
            if (Object.keys(event.css).length === 0) {
                $iframeDoc.find("#" + event.id).removeAttr("style")
            } else {
                //  $iframeDoc.find("#" + event.id).css(event.css);
                $iframeDoc.find("#" + event.id).attr('style', event.css)
            }
            if (event.path === "body") {
                $iframeDoc.find('body').css({
                    'overflow-y': 'hidden ',
                    'cursor': 'none ',
                    'pointerEvents': 'none '
                });
            }
        } else {
            if (Object.keys(event.css).length === 0) {
                $iframeDoc.find(event.path).removeAttr("style")
            } else {
                //  $iframeDoc.find(event.path).css(event.css);
                $iframeDoc.find(event.path).attr('style', event.css)
            }
            if (event.path === "body") {
                $iframeDoc.find('body').css({
                    'overflow-y': 'hidden ',
                    'cursor': 'none ',
                    'pointerEvents': 'none '
                });
            }
        }

    }
    if (event.type === 'dataProgressAttributes') {
        $iframeDoc.find(event.path).attr('data-progress', event.eValue);
    }
    if (event.type === 'valueAttributes') {
        $iframeDoc.find(event.path).val(event.eValue);
    }
    if (event.type === "dropdownAttributes") {
        let d = $iframeDoc.find(event.path);
        for (let index = 0; index < d[0].length; index++) {
            if (d[0][index].value === event.eValue) {
                d[0][index].selected = true
            }


        }
    }
    if (event.type === 'checkBox') {
        $iframeDoc.find(event.path).prop('checked', event.checked);
    }
    if (event.type === 'radio') {
        $iframeDoc.find(event.path).prop('checked', event.checked);
        // $iframeDoc.find("#" + event.id).attr("checked", true);

    }
    if (event.type === 'highlight') {
        //$iframeDoc.find(event.path).html(event.eValue);
    }
    if (event.type === 'clearHighlight') {

        // $iframeDoc.find(event.path).html(event.eValue);
    }
    if (event.type === 'scroll') {


        var $scrollTo = $.scrollTo = function (target, duration, settings) {
            return $(window).scrollTo(target, duration, settings);
        };

        $scrollTo.defaults = {
            axis: 'xy',
            duration: 0,
            limit: true
        };

        function isWin(elem) {
            return !elem.nodeName ||
                $.inArray(elem.nodeName.toLowerCase(), ['iframe', '#document', 'html', 'body']) !== -1;
        }

        $.fn.scrollTo = function (target, duration, settings) {
            if (typeof duration === 'object') {
                settings = duration;
                duration = 0;
            }
            if (typeof settings === 'function') {
                settings = { onAfter: settings };
            }
            if (target === 'max') {
                target = 9e9;
            }

            settings = $.extend({}, $scrollTo.defaults, settings);
            // Speed is still recognized for backwards compatibility
            duration = duration || settings.duration;
            // Make sure the settings are given right
            var queue = settings.queue && settings.axis.length > 1;
            if (queue) {
                // Let's keep the overall duration
                duration /= 2;
            }
            settings.offset = both(settings.offset);
            settings.over = both(settings.over);

            return this.each(function () {
                // Null target yields nothing, just like jQuery does
                if (target === null) return;

                var win = isWin(this),
                    elem = win ? this.contentWindow || window : this,
                    $elem = $(elem),
                    targ = target,
                    attr = {},
                    toff;

                switch (typeof targ) {
                    // A number will pass the regex
                    case 'number':
                    case 'string':
                        if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)) {
                            targ = both(targ);
                            // We are done
                            break;
                        }
                        // Relative/Absolute selector
                        targ = win ? $(targ) : $(targ, elem);
                    /* falls through */
                    case 'object':
                        if (targ.length === 0) return;
                        // DOMElement / jQuery
                        if (targ.is || targ.style) {
                            // Get the real position of the target
                            toff = (targ = $(targ)).offset();
                        }
                }

                var offset = $.isFunction(settings.offset) && settings.offset(elem, targ) || settings.offset;

                $.each(settings.axis.split(''), function (i, axis) {
                    var Pos = axis === 'x' ? 'Left' : 'Top',
                        pos = Pos.toLowerCase(),
                        key = 'scroll' + Pos,
                        prev = $elem[key](),
                        max = $scrollTo.max(elem, axis);

                    if (toff) {// jQuery / DOMElement
                        attr[key] = toff[pos] + (win ? 0 : prev - $elem.offset()[pos]);

                        // If it's a dom element, reduce the margin
                        if (settings.margin) {
                            attr[key] -= parseInt(targ.css('margin' + Pos), 10) || 0;
                            attr[key] -= parseInt(targ.css('border' + Pos + 'Width'), 10) || 0;
                        }

                        attr[key] += offset[pos] || 0;

                        if (settings.over[pos]) {
                            // Scroll to a fraction of its width/height
                            attr[key] += targ[axis === 'x' ? 'width' : 'height']() * settings.over[pos];
                        }
                    } else {
                        var val = targ[pos];
                        // Handle percentage values
                        attr[key] = val.slice && val.slice(-1) === '%' ?
                            parseFloat(val) / 100 * max
                            : val;
                    }

                    // Number or 'number'
                    if (settings.limit && /^\d+$/.test(attr[key])) {
                        // Check the limits
                        attr[key] = attr[key] <= 0 ? 0 : Math.min(attr[key], max);
                    }

                    // Don't waste time animating, if there's no need.
                    if (!i && settings.axis.length > 1) {
                        if (prev === attr[key]) {
                            // No animation needed
                            attr = {};
                        } else if (queue) {
                            // Intermediate animation
                            animate(settings.onAfterFirst);
                            // Don't animate this axis again in the next iteration.
                            attr = {};
                        }
                    }
                });

                animate(settings.onAfter);

                function animate(callback) {
                    var opts = $.extend({}, settings, {
                        // The queue setting conflicts with animate()
                        // Force it to always be true
                        queue: true,
                        duration: duration,
                        complete: callback && function () {
                            callback.call(elem, targ, settings);
                        }
                    });
                    $elem.animate(attr, opts);
                }
            });
        };

        $scrollTo.max = function (elem, axis) {
            var Dim = axis === 'x' ? 'Width' : 'Height',
                scroll = 'scroll' + Dim;

            if (!isWin(elem))
                return elem[scroll] - $(elem)[Dim.toLowerCase()]();

            var size = 'client' + Dim,
                doc = elem.ownerDocument || elem.document,
                html = doc.documentElement,
                body = doc.body;

            return Math.max(html[scroll], body[scroll]) - Math.min(html[size], body[size]);
        };

        function both(val) {
            return $.isFunction(val) || $.isPlainObject(val) ? val : { top: val, left: val };
        }

        // Add special hooks so that window scroll properties can be animated
        $.Tween.propHooks.scrollLeft =
            $.Tween.propHooks.scrollTop = {
                get: function (t) {
                    return $(t.elem)[t.prop]();
                },
                set: function (t) {
                    var curr = this.get(t);
                    // If interrupt is true and user scrolled, stop animating
                    if (t.options.interrupt && t._last && t._last !== curr) {
                        return $(t.elem).stop();
                    }
                    var next = Math.round(t.now);
                    if (curr !== next) {
                        $(t.elem)[t.prop](next);
                        t._last = this.get(t);
                    }
                }
            };

        $("#myIframe").scrollTo(event.y, 0, { queue: false });


    }

}
function playVideo() {
    (function draw() {
        let event = RecordingRecord.data.events[i];
        if (!event) {
            setInterval(function () {
                if (seconds >= RecordingRecord.data.duration) {
                    $iframe.remove();
                    let currentIndexLocal = localStorage.getItem("currentIndexLocal");
                    if (Number(currentIndexLocal) === (arr.length - 1)) {
                        //  localStorage.clear();
                        // window.close();
                        clearInterval(timeInterval);
                        $('#replayVideo').modal('show');
                    } else {
                        localStorage.setItem("validLocal", "true")
                    }
                }
            }, 1000);
        } else {
            let offsetRecording = event.time - RecordingRecord.data.recording.startTime;
            let eventTime = Date.now() + totalTimeGap;
            let offsetPlay = (eventTime - startPlay);
            if (offsetPlay >= offsetRecording && seconds >= event.timeInSec) {
                drawEvent(event, $fakeCursor, $iframeDoc);
                i++;
            }
            if (i < RecordingRecord.data.events.length) {
                if (stopRecoriding === false) {
                    requestAnimationFrame(draw);
                }
            } else {
                setInterval(function () {
                    if (seconds >= RecordingRecord.data.recording.duration) {
                        $iframe.remove();
                        let currentIndexLocal = localStorage.getItem("currentIndexLocal");
                        if (Number(currentIndexLocal) === (arr.length - 1)) {
                            //  localStorage.clear();
                            // window.close();
                            clearInterval(timeInterval);
                            $('#replayVideo').modal('show');
                        } else {
                            localStorage.setItem("validLocal", "true")
                            checkAndRequestRecording();
                        }
                    }
                }, 1000);
            }
        }
    })();
}
function pauseRecording() {
    if (stopRecoriding === false) {
        document.getElementById("playerStatus").classList.remove('fa-pause');
        document.getElementById("playerStatus").classList.add('fa-play');
        stopRecoriding = !stopRecoriding;
        clearInterval(timeInterval);
    } else if (stopRecoriding === true) {
        let currentTime = Date.now();
        totalTimeGap -= currentTime - timeGap;
        document.getElementById("playerStatus").classList.remove('fa-play');
        document.getElementById("playerStatus").classList.add('fa-pause');
        stopRecoriding = !stopRecoriding;
        timeInterval = setInterval(this.showTimer, 1000);
        playVideo();
    }
}
function changeRecording() {
    let index = document.getElementById('pageName').value;
    if (Number(index) === 0) {
        document.getElementById("myIframe").remove();
        document.getElementById("loader").style.visibility = "";
        localStorage.clear();
        checkAndRequestRecording();
        // window.location.href = window.location.href;
    } else if (Number(index) > 0) {
        document.getElementById("myIframe").remove();
        document.getElementById("loader").style.visibility = "";
        localStorage.setItem("currentIndexLocal", (Number(index) - 1))
        localStorage.setItem("validLocal", "true");
        checkAndRequestRecording();
        //  window.location.href = window.location.href;
    }

}
function checkAndRequestRecording() {
    let currentIndex2 = localStorage.getItem("currentIndexLocal");
    let valid2 = localStorage.getItem("validLocal", "true")
    if (valid2 === 'true' && Number(currentIndex2) > arr.length) {
        localStorage.clear();
    }
    let currentIndexLocal = localStorage.getItem("currentIndexLocal");
    let valid = localStorage.getItem("validLocal", "true");
    if (valid === null || valid === '' || valid === 'false') {
        localStorage.setItem('currentIndexLocal', 0);
        seconds = 0;
        i = 0;
        clearInterval(timeInterval);
        $('#replayVideo').modal('hide');
        document.getElementById('seconds-counter').innerText = 0;
        document.getElementById('seconds-duration').innerText = 0;
        getRecordingDetail(arr[0]._id);
        selectBox()
    } else {
        if (currentIndexLocal !== null || currentIndexLocal !== '' && Number(currentIndexLocal) <= data.data.length && valid === 'true') {
            localStorage.setItem('currentIndexLocal', (Number(currentIndexLocal) + 1))
            seconds = 0;
            i = 0;
            clearInterval(timeInterval);
            $('#replayVideo').modal('hide');
            document.getElementById('seekbar').value = 0;
            document.getElementById('seconds-counter').innerText = 0;
            document.getElementById('seconds-duration').innerText = 0;
            getRecordingDetail(arr[Number(currentIndexLocal) + 1]._id);
            selectBox()
        }
    }
}
function replayVideo() {
    localStorage.clear();
    checkAndRequestRecording();
}
function getNextRecording() {
    $iframe.remove();
    localStorage.setItem("validLocal", "true")
    let currentIndex = Number(localStorage.getItem('currentIndexLocal'));
    checkAndRequestRecording();
}
getPreviousRecording = () => {
    $iframe.remove();
    let currentIndex = Number(localStorage.getItem('currentIndexLocal'));
    if (currentIndex === 0) {
        localStorage.clear();
        checkAndRequestRecording();
    } else if (currentIndex > 0) {
        localStorage.setItem("valid", "true")
        localStorage.setItem("currentIndexLocal", (currentIndex - 2))
        checkAndRequestRecording();
    }

}
function onSelectChange(index) {
    $iframe.remove();
    if (Number(index) === 0) {
        localStorage.clear();
        checkAndRequestRecording();
    } else if (Number(index) > 0) {
        localStorage.setItem("currentIndexLocal", (Number(index) - 1))
        localStorage.setItem("validLocal", "true")
        checkAndRequestRecording();
    }
}
function selectBox() {
    var x, i, j, selElmnt, a, b, c;
    /*look for any elements with the class "custom-select":*/
    let checkDropDown = document.getElementsByClassName("select-selected");
    if (checkDropDown.length > 0) {
        for (let l = 0; l < checkDropDown.length; l++) {
            checkDropDown[l].remove();
        }
    }
    x = document.getElementsByClassName("custom-select");
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        let index = Number(localStorage.getItem("currentIndexLocal"))
        a.innerHTML = selElmnt.options[index].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");

        for (j = 0; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.id = selElmnt.options[j].value;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
                onSelectChange(this.id)
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }
    function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < x.length; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }
}
function formatDuration(seconds) {
    // const hours = seconds / 3600
    let res = ''
    if (seconds === 60) {
        res = "1 m"
    } else if (seconds < 60) {
        res = seconds + " s"
    } else if (seconds === 3600) {
        res = "1 h"
    } else if (seconds < 3600) {
        const minutes = (seconds % 3600) / 60;
        //res = Math.round(minutes) + " min"
        seconds %= 60
        res = [minutes, seconds].map(format).join(' m ') + " s"
    } else if (seconds === 7200) {
        res = "2 h"
    } else {
        const hours = seconds / 3600
        const minutes = (seconds % 3600) / 60
        seconds %= 60

        res = [hours, minutes].map(format).join(' h ') + " m"
    }
    return res;
}
function format(val) {
    return ('0' + Math.floor(val)).slice(-2)
}
function drawGradient() {
    let noEventColor = "rgb(221, 221, 221)";
    let mouseMoveColor = "rgb(255, 0, 0)";
    let clickColor = 'rgb(255, 208, 0)';
    let formInteractColor = 'rgb(88,183,0,1)';
    let gradientStyle = `linear-gradient(to right, `;
    let timeStampArray = [];
    let mouseMove = RecordingRecord.data.events.filter(x =>
        x.type === "scroll" || x.type === "mousemove")
    let clicks = RecordingRecord.data.events.filter(x =>
        x.type === "click")
    let formInteract = RecordingRecord.data.events.filter(x =>
        x.type === "keypress" || x.type === "checkBox" || x.type === "change"
        || x.type === "radio" || x.type === "valueAttributes");
    formInteractLength = formInteract.length;
    let incrementalValOfTimestamp = Number((RecordingRecord.data.recording.duration / 200) * 1000);
    let val = incrementalValOfTimestamp
    for (let index = 0; index < 200; index++) {
        let dateObj = new Date(RecordingRecord.data.recording.startTime);
        timeStampArray.push(Number(dateObj.setMilliseconds(dateObj.getMilliseconds() + val)));
        val += incrementalValOfTimestamp;
    }
    let mouseMoveTime =
        [...new Set(mouseMove.map(x => x.time))]
    let clicksTime =
        [...new Set(clicks.map(x => x.time))]
    let formInteractTime =
        [...new Set(formInteract.map(x => x.time))]
    let previousTimeVal = 0;
    for (let index = 1; index <= timeStampArray.length; index++) {
        let element = timeStampArray[index];
        if (formInteractTime.find(x => x <= element && x > previousTimeVal) !== undefined) {
            if (index === timeStampArray.length) {
                gradientStyle = gradientStyle + ` ${formInteractColor} ${index / 2}%)`
            } else {
                gradientStyle = gradientStyle + ` ${formInteractColor} ${index / 2}%,`
            }
        } else if (clicksTime.find(x => x <= element && x > previousTimeVal) !== undefined) {
            if (index === timeStampArray.length) {
                gradientStyle = gradientStyle + ` ${clickColor} ${index / 2}%)`
            } else {
                gradientStyle = gradientStyle + ` ${clickColor} ${index / 2}%,`
            }
        } else if (mouseMoveTime.find(x => x <= element && x > previousTimeVal) !== undefined) {
            if (index === timeStampArray.length) {
                gradientStyle = gradientStyle + ` ${mouseMoveColor} ${index / 2}%)`
            } else {
                gradientStyle = gradientStyle + ` ${mouseMoveColor} ${index / 2}%,`
            }
        } else {
            if (index === timeStampArray.length) {
                gradientStyle = gradientStyle + ` ${noEventColor} ${index / 2}%)`
            } else {
                gradientStyle = gradientStyle + ` ${noEventColor} ${index / 2}%,`
            }
        }
        previousTimeVal = element;
    }
    document.getElementsByClassName('seekbar-bg')[0].style.background = gradientStyle
}
function onChangePlayerDuration(value) {
    let val = value;
    changePlayerDuration = false;
    let updatedSecond = Number(val) / 100;
    if (updatedSecond > seconds) {
        if (i !== RecordingRecord.data.events.length) {
            let eventsToDraw = RecordingRecord.data.events.filter(event => event.time >= RecordingRecord.data.events[i].time && event.timeInSec <= Number(updatedSecond.toFixed(0)))
            for (const event of eventsToDraw) {
                drawForwardRecordingEvents(event)
            }
        }

        let date = new Date(Date.now());
        totalTimeGap += date.setSeconds(date.getSeconds() + (updatedSecond - seconds)) - Date.now();
        seconds = updatedSecond;

        let index = -1;
        for (let j = 0; j < RecordingRecord.data.events.length; j++) {
            const element = RecordingRecord.data.events[j];
            if (element.timeInSec === (Number(seconds.toFixed(0)))) {
                index = j;
                break;
            } else if (element.timeInSec > (seconds) && j !== 0) {
                index = j - 1;
                break;
            }
        }
        if (index !== -1) {
            if (RecordingRecord.data.events.length === i) {
                i = index
                playVideo();
            } else {
                i = index
            }
        }
    } else {
        let date = new Date(Date.now());
        totalTimeGap -= Date.now() - date.setSeconds(date.getSeconds() - (seconds - updatedSecond));
        seconds = updatedSecond;
        resetIframeHtml();
        let eventsToDraw = RecordingRecord.data.events.filter(event => event.timeInSec < Number(seconds.toFixed(0)))
        for (const event of eventsToDraw) {
            drawForwardRecordingEvents(event)
        }
        let index = -1;
        for (let j = 0; j < RecordingRecord.data.events.length; j++) {
            const element = RecordingRecord.data.events[j];
            if (element.timeInSec === Number(seconds.toFixed(0))) {
                index = j;
                break;
            } else if (element.timeInSec > Number(seconds.toFixed(0)) && j !== 0) {
                index = j - 1;
                break;
            }
        }
        if (index !== -1) {
            if (RecordingRecord.data.events.length === i) {
                i = index
                playVideo();
            } else {
                i = index
            }
        }
    }
    document.getElementById('seekbar').value = Number(val)
    document.getElementById('seconds-counter').innerText = formatDuration(seconds.toFixed(0));
}

function resetIframeHtml() {
    $iframe[0].contentDocument.documentElement.innerHTML = RecordingRecord.data.recording.htmlCopy;
    $iframeDoc = window.jQuery($iframe[0].contentDocument.documentElement);
    if (RecordingRecord.data.recording.styles !== undefined) {
        for (let index = 0; index < RecordingRecord.data.recording.styles.length; index++) {
            $iframeDoc.find('head').append('<style type="text/css">' + RecordingRecord.data.recording.styles[index] + '</style>');
        }
    }
    $iframeDoc.find('body').css({
        'overflow-y': 'hidden ',
        'cursor': 'none ',
        'pointerEvents': 'none '
    });
    // $iframeDoc.find('footer').css({
    //     'overflow-y': 'hidden ',
    //     'cursor': 'none ',
    //     'pointerEvents': 'none '
    // });
    // $iframeDoc.find('header').css({
    //     'overflow-y': 'hidden ',
    //     'cursor': 'none ',
    //     'pointerEvents': 'none'
    // });
    $fakeCursor = window.jQuery('<div id="pandaCursor" style="position:absolute;z-index:9000000000000000;" class="cursor"><img width="13" style="margin-top: 3px" src="./cursor.png" /></div>')
    $iframeDoc.find('body').append($fakeCursor);
    for (let i = 0; i < RecordingRecord.data.excludedContent.length; i++) {
        $iframeDoc.find(RecordingRecord.data.excludedContent[0]).css('visibility', 'hidden');
    }
}

function drawForwardRecordingEvents(event) {
    if (event.type === 'mousemove') {
        $iframeDoc.find('#pandaCursor').css({
            // position:"relative",
            top: event.y,
            left: event.x
        });
    } else if (event.type === 'hideFeedback') {
        $iframeDoc.find("#" + event.id).css({
            display: "none"
        });

    } else if (event.type === "hideModal") {
        $iframeDoc.find("#" + event.id).css({
            display: "none"
        });
    } else if (event.type === 'click') {

        this.flashClass($fakeCursor, 'click');
        //const path =  this.getPath(event.target);
        const $element = $iframeDoc.find(event.path);
        this.flashClass($element, 'clicked');
    } else if (event.type === 'keypress') {
        //const path = window.jQuery(event.target).getPath();
        const $element = $iframeDoc.find(event.path);
        // $element.prop('type', 'password');
        //$element.trigger({ type: 'keypress', keyCode: event.keyCode })
        $element.val(event.eValue);
    } else if (event.type === 'change') {

        $iframeDoc.find(event.path).append("");
        $iframeDoc.find(event.path).text(event.eValue);
    } else if (event.type === 'addChildList') {
        if (event.id) {
            if (event.id === "" && $iframeDoc.find("#" + event.id).length == 0) {
                let path = $iframeDoc.find(event.path);
                if (path.length > 0) {
                    if (path[0].id !== 'pandaCursor') {
                        path.append(event.innerHTML);
                    }
                }
            } else {
                let path = $iframeDoc.find("#" + event.id);
                path.html(event.innerHTML);
            }
        } else {
            let path = $iframeDoc.find(event.path);
            if (path.length > 0) {
                if (path[0].id !== 'pandaCursor') {
                    path.append(event.innerHTML);
                }
            }
        }
    } else if (event.type === 'removeChildList') {
        if (event.id !== undefined || "") {
            if ($iframeDoc.find("#" + event.id).length == 0) {
                let path = $iframeDoc.find(event.path);
                path.remove();
            } else {
                let path = $iframeDoc.find("#" + event.id);
                path.remove();
            }
        } else {
            let path = $iframeDoc.find(event.path);
            path.remove();
        }

    } else if (event.type === 'classAttributes') {
        if (event.id) {
            if (event.id === "") {
                $iframeDoc.find(event.path).removeClass().addClass(event.className);
            } else {
                $iframeDoc.find("#" + event.id).removeClass().addClass(event.className)
            }
        } else {
            if ($iframeDoc.find(event.path).length > 0) {
                if ($iframeDoc.find(event.path)[0].id !== "pandaCursor") {
                    $iframeDoc.find(event.path).removeClass().addClass(event.className);
                }
            }

        }

    } else if (event.type === 'styleAttributes') {
        if (event.id !== undefined || "") {
            if (Object.keys(event.css).length === 0) {
                $iframeDoc.find("#" + event.id).removeAttr("style")
            } else {
                $iframeDoc.find("#" + event.id).attr('style', event.css)
            }
            if (event.path === "body") {
                $iframeDoc.find('body').css({
                    'overflow-y': 'hidden ',
                    'cursor': 'none ',
                    'pointerEvents': 'none '
                });
            }
        } else {

            if (Object.keys(event.css).length === 0) {
                $iframeDoc.find(event.path).removeAttr("style")
            } else {
                //  $iframeDoc.find(event.path).css(event.css);

                $iframeDoc.find(event.path).attr('style', event.css)
            }
            if (event.path === "body") {
                $iframeDoc.find('body').css({
                    'overflow-y': 'hidden ',
                    'cursor': 'none ',
                    'pointerEvents': 'none '
                });
            }
        }
    } else if (event.type === 'dataProgressAttributes') {
        $iframeDoc.find(event.path).attr('data-progress', event.eValue);
    } else if (event.type === 'valueAttributes') {
        $iframeDoc.find(event.path).val(event.eValue);
    } else if (event.type === "dropdownAttributes") {
        let d = $iframeDoc.find(event.path);
        for (let index = 0; index < d[0].length; index++) {
            if (d[0][index].value === event.eValue) {
                d[0][index].selected = true
            }


        }
    } else if (event.type === 'checkBox') {
        $iframeDoc.find(event.path).prop('checked', event.checked);
    } else if (event.type === 'radio') {
        $iframeDoc.find(event.path).prop('checked', event.checked);
        // $iframeDoc.find("#" + event.id).attr("checked", true);

    } else if (event.type === 'scroll') {
        var $scrollTo = $.scrollTo = function (target, duration, settings) {
            return $(window).scrollTo(target, duration, settings);
        };
        $scrollTo.defaults = {
            axis: 'xy',
            duration: 0,
            limit: true
        };
        function isWin(elem) {
            return !elem.nodeName ||
                $.inArray(elem.nodeName.toLowerCase(), ['iframe', '#document', 'html', 'body']) !== -1;
        }

        $.fn.scrollTo = function (target, duration, settings) {
            if (typeof duration === 'object') {
                settings = duration;
                duration = 0;
            }
            if (typeof settings === 'function') {
                settings = { onAfter: settings };
            }
            if (target === 'max') {
                target = 9e9;
            }

            settings = $.extend({}, $scrollTo.defaults, settings);
            // Speed is still recognized for backwards compatibility
            duration = duration || settings.duration;
            // Make sure the settings are given right
            var queue = settings.queue && settings.axis.length > 1;
            if (queue) {
                // Let's keep the overall duration
                duration /= 2;
            }
            settings.offset = both(settings.offset);
            settings.over = both(settings.over);

            return this.each(function () {
                // Null target yields nothing, just like jQuery does
                if (target === null) return;

                var win = isWin(this),
                    elem = win ? this.contentWindow || window : this,
                    $elem = $(elem),
                    targ = target,
                    attr = {},
                    toff;

                switch (typeof targ) {
                    // A number will pass the regex
                    case 'number':
                    case 'string':
                        if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)) {
                            targ = both(targ);
                            // We are done
                            break;
                        }
                        // Relative/Absolute selector
                        targ = win ? $(targ) : $(targ, elem);
                    /* falls through */
                    case 'object':
                        if (targ.length === 0) return;
                        // DOMElement / jQuery
                        if (targ.is || targ.style) {
                            // Get the real position of the target
                            toff = (targ = $(targ)).offset();
                        }
                }

                var offset = $.isFunction(settings.offset) && settings.offset(elem, targ) || settings.offset;

                $.each(settings.axis.split(''), function (i, axis) {
                    var Pos = axis === 'x' ? 'Left' : 'Top',
                        pos = Pos.toLowerCase(),
                        key = 'scroll' + Pos,
                        prev = $elem[key](),
                        max = $scrollTo.max(elem, axis);

                    if (toff) {// jQuery / DOMElement
                        attr[key] = toff[pos] + (win ? 0 : prev - $elem.offset()[pos]);

                        // If it's a dom element, reduce the margin
                        if (settings.margin) {
                            attr[key] -= parseInt(targ.css('margin' + Pos), 10) || 0;
                            attr[key] -= parseInt(targ.css('border' + Pos + 'Width'), 10) || 0;
                        }

                        attr[key] += offset[pos] || 0;

                        if (settings.over[pos]) {
                            // Scroll to a fraction of its width/height
                            attr[key] += targ[axis === 'x' ? 'width' : 'height']() * settings.over[pos];
                        }
                    } else {
                        var val = targ[pos];
                        // Handle percentage values
                        attr[key] = val.slice && val.slice(-1) === '%' ?
                            parseFloat(val) / 100 * max
                            : val;
                    }

                    // Number or 'number'
                    if (settings.limit && /^\d+$/.test(attr[key])) {
                        // Check the limits
                        attr[key] = attr[key] <= 0 ? 0 : Math.min(attr[key], max);
                    }

                    // Don't waste time animating, if there's no need.
                    if (!i && settings.axis.length > 1) {
                        if (prev === attr[key]) {
                            // No animation needed
                            attr = {};
                        } else if (queue) {
                            // Intermediate animation
                            animate(settings.onAfterFirst);
                            // Don't animate this axis again in the next iteration.
                            attr = {};
                        }
                    }
                });

                animate(settings.onAfter);

                function animate(callback) {
                    var opts = $.extend({}, settings, {
                        // The queue setting conflicts with animate()
                        // Force it to always be true
                        queue: true,
                        duration: duration,
                        complete: callback && function () {
                            callback.call(elem, targ, settings);
                        }
                    });
                    $elem.animate(attr, opts);
                }
            });
        };

        $scrollTo.max = function (elem, axis) {
            var Dim = axis === 'x' ? 'Width' : 'Height',
                scroll = 'scroll' + Dim;

            if (!isWin(elem))
                return elem[scroll] - $(elem)[Dim.toLowerCase()]();

            var size = 'client' + Dim,
                doc = elem.ownerDocument || elem.document,
                html = doc.documentElement,
                body = doc.body;

            return Math.max(html[scroll], body[scroll]) - Math.min(html[size], body[size]);
        };

        function both(val) {
            return $.isFunction(val) || $.isPlainObject(val) ? val : { top: val, left: val };
        }

        // Add special hooks so that window scroll properties can be animated
        $.Tween.propHooks.scrollLeft =
            $.Tween.propHooks.scrollTop = {
                get: function (t) {
                    return $(t.elem)[t.prop]();
                },
                set: function (t) {
                    var curr = this.get(t);
                    // If interrupt is true and user scrolled, stop animating
                    if (t.options.interrupt && t._last && t._last !== curr) {
                        return $(t.elem).stop();
                    }
                    var next = Math.round(t.now);
                    if (curr !== next) {
                        $(t.elem)[t.prop](next);
                        t._last = this.get(t);
                    }
                }
            };

        $("#myIframe").scrollTo(event.y, 0, { queue: false });


    }
}

function goToFormInteract() {
    let formInteract = RecordingRecord.data.events.filter(x =>
        x.type === "keypress" || x.type === "checkBox" || x.type === "change"
        || x.type === "radio" || x.type === "valueAttributes");
    if (formInteractLength > -1) {
        if (formInteractIndex === (formInteractLength - 1)) {
            formInteractIndex = 0;
        } else {
            if (formInteractIndex === -1) {
                formInteractIndex = formInteractIndex + 1;
            } else {
                for (let index = 1; index <= formInteract.length; index++) {
                    if (formInteractIndex === formInteract.length) {
                        formInteractIndex = 0;
                        break;
                    } else {
                        let diff = (formInteract[formInteractIndex + 1].time - formInteract[formInteractIndex].time) / 1000;
                        if (diff >= 0.8) {
                            formInteractIndex = formInteractIndex + 1;
                            break;
                        } else {
                            formInteractIndex += 1;
                        }
                    }
                }
            }

        }
        let updatedSecond;
        if (formInteract[formInteractIndex]) {
            updatedSecond = ((formInteract[formInteractIndex].time - RecordingRecord.data.recording.startTime) / 1000);
        } else {
            updatedSecond = ((formInteract[formInteract.length - 1].time - RecordingRecord.data.recording.startTime) / 1000);
        }
        if (updatedSecond > seconds) {
            if (i !== RecordingRecord.data.events.length) {
                let eventsToDraw = RecordingRecord.data.events.filter(event => event.time >= RecordingRecord.data.events[i].time && event.timeInSec <= Number(updatedSecond.toFixed(0)))
                for (const event of eventsToDraw) {
                    drawForwardRecordingEvents(event)
                }
            }

            let date = new Date(Date.now());
            totalTimeGap += date.setSeconds(date.getSeconds() + (updatedSecond - seconds)) - Date.now();
            seconds = updatedSecond;

            let index = -1;
            for (let j = 0; j < RecordingRecord.data.events.length; j++) {
                const element = RecordingRecord.data.events[j];
                if (element.timeInSec === (Number(seconds.toFixed(0)))) {
                    index = j;
                    break;
                } else if (element.timeInSec > (seconds) && j !== 0) {
                    index = j - 1;
                    break;
                }
            }
            if (index !== -1) {
                if (RecordingRecord.data.events.length === i) {
                    i = index
                    playVideo();
                } else {
                    i = index
                }
            }
        } else {
            let date = new Date(Date.now());
            totalTimeGap -= Date.now() - date.setSeconds(date.getSeconds() - (seconds - updatedSecond));
            seconds = updatedSecond;
            resetIframeHtml();
            let eventsToDraw = RecordingRecord.data.events.filter(event => event.timeInSec < Number(seconds.toFixed(0)))
            for (const event of eventsToDraw) {
                drawForwardRecordingEvents(event)
            }
            let index = -1;
            for (let j = 0; j < RecordingRecord.data.events.length; j++) {
                const element = RecordingRecord.data.events[j];
                if (element.timeInSec === Number(seconds.toFixed(0))) {
                    index = j;
                    break;
                } else if (element.timeInSec > Number(seconds.toFixed(0)) && j !== 0) {
                    index = j - 1;
                    break;
                }
            }
            if (index !== -1) {
                if (RecordingRecord.data.events.length === i) {
                    i = index
                    playVideo();
                } else {
                    i = index
                }
            }
        }
        document.getElementById('seekbar').value = Number((((formInteract[formInteractIndex].time - RecordingRecord.data.recording.startTime) / 1000) * 100).toFixed(0))
    }
}

function goToClickRage() {
    let clickRage = RecordingRecord.data.events.filter(x =>
        x.type === "clickRage");
    clickRageLength = clickRage.length;
    if (clickRageLength > -1) {
        if (clickRageIndex === (clickRageLength - 1)) {
            clickRageIndex = 0;
        } else {
            if (clickRageIndex === -1) {
                clickRageIndex = clickRageIndex + 1;
            } else {
                for (let index = 1; index <= clickRage.length; index++) {
                    if (clickRageIndex === clickRage.length - 2) {
                        clickRageIndex = 0;
                        break;
                    } else {
                        let diff = (clickRage[clickRageIndex + 1].time - clickRage[clickRageIndex].time) / 1000;
                        if (diff >= 0.8) {
                            clickRageIndex = clickRageIndex + index;
                            break;
                        } else {
                            clickRageIndex += 1;
                        }
                    }
                }
            }

        }
        let updatedSecond = ((clickRage[clickRageIndex].time - RecordingRecord.data.recording.startTime) / 1000);
        if (updatedSecond > seconds) {
            if (i !== RecordingRecord.data.events.length) {
                let eventsToDraw = RecordingRecord.data.events.filter(event => event.time >= RecordingRecord.data.events[i].time && event.timeInSec <= Number(updatedSecond.toFixed(0)))
                for (const event of eventsToDraw) {
                    drawForwardRecordingEvents(event)
                }
            }

            let date = new Date(Date.now());
            totalTimeGap += date.setSeconds(date.getSeconds() + (updatedSecond - seconds)) - Date.now();
            seconds = updatedSecond;
            if (seconds > RecordingRecord.data.recording.duration) {
                seconds = RecordingRecord.data.recording.duration
                document.getElementById('seconds-counter').innerText = formatDuration(RecordingRecord.data.recording.duration);
            }
            let index = -1;
            for (let j = 0; j < RecordingRecord.data.events.length; j++) {
                const element = RecordingRecord.data.events[j];
                if (element.timeInSec === (Number(seconds.toFixed(0)))) {
                    index = j;
                    break;
                } else if (element.timeInSec > (seconds) && j !== 0) {
                    index = j - 1;
                    break;
                }
            }
            if (index !== -1) {
                if (RecordingRecord.data.events.length === i) {
                    i = index
                    playVideo();
                } else {
                    i = index
                }
            }
        } else {
            let date = new Date(Date.now());
            totalTimeGap -= Date.now() - date.setSeconds(date.getSeconds() - (seconds - updatedSecond));
            seconds = updatedSecond;
            resetIframeHtml();
            let eventsToDraw = RecordingRecord.data.events.filter(event => event.timeInSec < Number(seconds.toFixed(0)))
            for (const event of eventsToDraw) {
                drawForwardRecordingEvents(event)
            }
            let index = -1;
            for (let j = 0; j < RecordingRecord.data.events.length; j++) {
                const element = RecordingRecord.data.events[j];
                if (element.timeInSec === Number(seconds.toFixed(0))) {
                    index = j;
                    break;
                } else if (element.timeInSec > Number(seconds.toFixed(0)) && j !== 0) {
                    index = j - 1;
                    break;
                }
            }
            if (index !== -1) {
                if (RecordingRecord.data.events.length === i) {
                    i = index
                    playVideo();
                } else {
                    i = index
                }
            }
        }
        document.getElementById('seekbar').value = Number((((clickRage[clickRageIndex].time - RecordingRecord.data.recording.startTime) / 1000) * 100).toFixed(0))
    }

}

function goToClickError() {
    let clickError = RecordingRecord.data.events.filter(x =>
        x.type === "clickError");
    clickErrorLength = clickError.length;
    if (clickErrorLength > -1) {
        if (clickErrorIndex === (clickErrorLength - 1)) {
            clickErrorIndex = 0;
        } else {
            if (clickErrorIndex === -1) {
                clickErrorIndex = clickErrorIndex + 1;
            } else {
                for (let index = 1; index <= clickError.length; index++) {
                    if (clickErrorIndex === clickError.length - 2) {
                        clickErrorIndex = 0;
                        break;
                    } else {
                        let diff = (clickError[clickErrorIndex + 1].time - clickError[clickErrorIndex].time) / 1000;
                        if (diff >= 0.8) {
                            clickErrorIndex = clickErrorIndex + index;
                            break;
                        } else {
                            clickErrorIndex += 1;
                        }
                    }
                }
            }
        }
        let updatedSecond = ((clickError[clickErrorIndex].time - RecordingRecord.data.recording.startTime) / 1000);
        if (updatedSecond > seconds) {
            if (i !== RecordingRecord.data.events.length) {
                let eventsToDraw = RecordingRecord.data.events.filter(event => event.time >= RecordingRecord.data.events[i].time && event.timeInSec <= Number(updatedSecond.toFixed(0)))
                for (const event of eventsToDraw) {
                    drawForwardRecordingEvents(event)
                }
            }
            let date = new Date(Date.now());
            totalTimeGap += date.setSeconds(date.getSeconds() + (updatedSecond - seconds)) - Date.now();
            seconds = updatedSecond;
            if (seconds > RecordingRecord.data.recording.duration) {
                seconds = RecordingRecord.data.recording.duration
                document.getElementById('seconds-counter').innerText = formatDuration(RecordingRecord.data.recording.duration);
            }
            let index = -1;
            for (let j = 0; j < RecordingRecord.data.events.length; j++) {
                const element = RecordingRecord.data.events[j];
                if (element.timeInSec === (Number(seconds.toFixed(0)))) {
                    index = j;
                    break;
                } else if (element.timeInSec > (seconds) && j !== 0) {
                    index = j - 1;
                    break;
                }
            }
            if (index !== -1) {
                if (RecordingRecord.data.events.length === i) {
                    i = index
                    playVideo();
                } else {
                    i = index
                }
            }
        } else {
            let date = new Date(Date.now());
            totalTimeGap -= Date.now() - date.setSeconds(date.getSeconds() - (seconds - updatedSecond));
            seconds = updatedSecond;
            resetIframeHtml();
            let eventsToDraw = RecordingRecord.data.events.filter(event => event.timeInSec < Number(seconds.toFixed(0)))
            for (const event of eventsToDraw) {
                drawForwardRecordingEvents(event)
            }
            let index = -1;
            for (let j = 0; j < RecordingRecord.data.events.length; j++) {
                const element = RecordingRecord.data.events[j];
                if (element.timeInSec === Number(seconds.toFixed(0))) {
                    index = j;
                    break;
                } else if (element.timeInSec > Number(seconds.toFixed(0)) && j !== 0) {
                    index = j - 1;
                    break;
                }
            }
            if (index !== -1) {
                if (RecordingRecord.data.events.length === i) {
                    i = index
                    playVideo();
                } else {
                    i = index
                }
            }
        }
        document.getElementById('seekbar').value = Number((((clickError[clickErrorIndex].time - RecordingRecord.data.recording.startTime) / 1000) * 100).toFixed(0))

    }

}

function goToSubmit() {
    let submit = RecordingRecord.data.events.filter(x =>
        x.type === "submit");
    submitLength = submit.length;
    if (submitLength > -1) {
        if (submitIndex === (submitLength - 1)) {
            submitIndex = 0;
        } else {
            if (submitIndex === -1) {
                submitIndex = submitIndex + 1;
            } else {
                for (let index = 1; index <= submit.length; index++) {
                    if (submitIndex === submit.length - 2) {
                        submitIndex = 0;
                        break;
                    } else {
                        let diff = (submit[submitIndex + 1].time - submit[submitIndex].time) / 1000;
                        if (diff >= 0.8) {
                            submitIndex = submitIndex + index;
                            break;
                        } else {
                            submitIndex += 1;
                        }
                    }
                }
            }
        }
    }
    let updatedSecond = ((submit[submitIndex].time - RecordingRecord.data.recording.startTime) / 1000);
    if (updatedSecond > seconds) {
        if (i !== RecordingRecord.data.events.length) {
            let eventsToDraw = RecordingRecord.data.events.filter(event => event.time >= RecordingRecord.data.events[i].time && event.timeInSec <= Number(updatedSecond.toFixed(0)))
            for (const event of eventsToDraw) {
                drawForwardRecordingEvents(event)
            }
        }
        let date = new Date(Date.now());
        totalTimeGap += date.setSeconds(date.getSeconds() + (updatedSecond - seconds)) - Date.now();
        seconds = updatedSecond;
        if (seconds > RecordingRecord.data.recording.duration) {
            seconds = RecordingRecord.data.recording.duration
            document.getElementById('seconds-counter').innerText = formatDuration(RecordingRecord.data.recording.duration);
        }
        let index = -1;
        for (let j = 0; j < RecordingRecord.data.events.length; j++) {
            const element = RecordingRecord.data.events[j];
            if (element.timeInSec === (Number(seconds.toFixed(0)))) {
                index = j;
                break;
            } else if (element.timeInSec > (seconds) && j !== 0) {
                index = j - 1;
                break;
            }
        }
        if (index !== -1) {
            if (RecordingRecord.data.events.length === i) {
                i = index
                playVideo();
            } else {
                i = index
            }
        }
    } else {
        let date = new Date(Date.now());
        totalTimeGap -= Date.now() - date.setSeconds(date.getSeconds() - (seconds - updatedSecond));
        seconds = updatedSecond;
        resetIframeHtml();
        let eventsToDraw = RecordingRecord.data.events.filter(event => event.timeInSec < Number(seconds.toFixed(0)))
        for (const event of eventsToDraw) {
            drawForwardRecordingEvents(event)
        }
        let index = -1;
        for (let j = 0; j < RecordingRecord.data.events.length; j++) {
            const element = RecordingRecord.data.events[j];
            if (element.timeInSec === Number(seconds.toFixed(0))) {
                index = j;
                break;
            } else if (element.timeInSec > Number(seconds.toFixed(0)) && j !== 0) {
                index = j - 1;
                break;
            }
        }
        if (index !== -1) {
            if (RecordingRecord.data.events.length === i) {
                i = index
                playVideo();
            } else {
                i = index
            }
        }
    }
    document.getElementById('seekbar').value = Number((((submit[submitIndex].time - RecordingRecord.data.recording.startTime) / 1000) * 100).toFixed(0))
}