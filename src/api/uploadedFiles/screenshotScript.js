/*! jQuery v1.8.3 jquery.com | jquery.org/license */
(function (e, t) {
    function _(e) {
        var t = M[e] = {};
        return v.each(e.split(y), function (e, n) {
            t[n] = !0
        }), t
    }

    function H(e, n, r) {
        if (r === t && e.nodeType === 1) {
            var i = "data-" + n.replace(P, "-$1").toLowerCase();
            r = e.getAttribute(i);
            if (typeof r == "string") {
                try {
                    r = r === "true" ? !0 : r === "false" ? !1 : r === "null" ? null : +r + "" === r ? +r : D.test(r) ? v.parseJSON(r) : r
                } catch (s) { }
                v.data(e, n, r)
            } else r = t
        }
        return r
    }

    function B(e) {
        var t;
        for (t in e) {
            if (t === "data" && v.isEmptyObject(e[t])) continue;
            if (t !== "toJSON") return !1
        }
        return !0
    }

    function et() {
        return !1
    }

    function tt() {
        return !0
    }

    function ut(e) {
        return !e || !e.parentNode || e.parentNode.nodeType === 11
    }

    function at(e, t) {
        do e = e[t]; while (e && e.nodeType !== 1);
        return e
    }

    function ft(e, t, n) {
        t = t || 0;
        if (v.isFunction(t)) return v.grep(e, function (e, r) {
            var i = !!t.call(e, r, e);
            return i === n
        });
        if (t.nodeType) return v.grep(e, function (e, r) {
            return e === t === n
        });
        if (typeof t == "string") {
            var r = v.grep(e, function (e) {
                return e.nodeType === 1
            });
            if (it.test(t)) return v.filter(t, r, !n);
            t = v.filter(t, r)
        }
        return v.grep(e, function (e, r) {
            return v.inArray(e, t) >= 0 === n
        })
    }

    function lt(e) {
        var t = ct.split("|"),
            n = e.createDocumentFragment();
        if (n.createElement)
            while (t.length) n.createElement(t.pop());
        return n
    }

    function Lt(e, t) {
        return e.getElementsByTagName(t)[0] || e.appendChild(e.ownerDocument.createElement(t))
    }

    function At(e, t) {
        if (t.nodeType !== 1 || !v.hasData(e)) return;
        var n, r, i, s = v._data(e),
            o = v._data(t, s),
            u = s.events;
        if (u) {
            delete o.handle, o.events = {};
            for (n in u)
                for (r = 0, i = u[n].length; r < i; r++) v.event.add(t, n, u[n][r])
        }
        o.data && (o.data = v.extend({}, o.data))
    }

    function Ot(e, t) {
        var n;
        if (t.nodeType !== 1) return;
        t.clearAttributes && t.clearAttributes(), t.mergeAttributes && t.mergeAttributes(e), n = t.nodeName.toLowerCase(), n === "object" ? (t.parentNode && (t.outerHTML = e.outerHTML), v.support.html5Clone && e.innerHTML && !v.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : n === "input" && Et.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : n === "option" ? t.selected = e.defaultSelected : n === "input" || n === "textarea" ? t.defaultValue = e.defaultValue : n === "script" && t.text !== e.text && (t.text = e.text), t.removeAttribute(v.expando)
    }

    function Mt(e) {
        return typeof e.getElementsByTagName != "undefined" ? e.getElementsByTagName("*") : typeof e.querySelectorAll != "undefined" ? e.querySelectorAll("*") : []
    }

    function _t(e) {
        Et.test(e.type) && (e.defaultChecked = e.checked)
    }

    function Qt(e, t) {
        if (t in e) return t;
        var n = t.charAt(0).toUpperCase() + t.slice(1),
            r = t,
            i = Jt.length;
        while (i--) {
            t = Jt[i] + n;
            if (t in e) return t
        }
        return r
    }

    function Gt(e, t) {
        return e = t || e, v.css(e, "display") === "none" || !v.contains(e.ownerDocument, e)
    }

    function Yt(e, t) {
        var n, r, i = [],
            s = 0,
            o = e.length;
        for (; s < o; s++) {
            n = e[s];
            if (!n.style) continue;
            i[s] = v._data(n, "olddisplay"), t ? (!i[s] && n.style.display === "none" && (n.style.display = ""), n.style.display === "" && Gt(n) && (i[s] = v._data(n, "olddisplay", nn(n.nodeName)))) : (r = Dt(n, "display"), !i[s] && r !== "none" && v._data(n, "olddisplay", r))
        }
        for (s = 0; s < o; s++) {
            n = e[s];
            if (!n.style) continue;
            if (!t || n.style.display === "none" || n.style.display === "") n.style.display = t ? i[s] || "" : "none"
        }
        return e
    }

    function Zt(e, t, n) {
        var r = Rt.exec(t);
        return r ? Math.max(0, r[1] - (n || 0)) + (r[2] || "px") : t
    }

    function en(e, t, n, r) {
        var i = n === (r ? "border" : "content") ? 4 : t === "width" ? 1 : 0,
            s = 0;
        for (; i < 4; i += 2) n === "margin" && (s += v.css(e, n + $t[i], !0)), r ? (n === "content" && (s -= parseFloat(Dt(e, "padding" + $t[i])) || 0), n !== "margin" && (s -= parseFloat(Dt(e, "border" + $t[i] + "Width")) || 0)) : (s += parseFloat(Dt(e, "padding" + $t[i])) || 0, n !== "padding" && (s += parseFloat(Dt(e, "border" + $t[i] + "Width")) || 0));
        return s
    }

    function tn(e, t, n) {
        var r = t === "width" ? e.offsetWidth : e.offsetHeight,
            i = !0,
            s = v.support.boxSizing && v.css(e, "boxSizing") === "border-box";
        if (r <= 0 || r == null) {
            r = Dt(e, t);
            if (r < 0 || r == null) r = e.style[t];
            if (Ut.test(r)) return r;
            i = s && (v.support.boxSizingReliable || r === e.style[t]), r = parseFloat(r) || 0
        }
        return r + en(e, t, n || (s ? "border" : "content"), i) + "px"
    }

    function nn(e) {
        if (Wt[e]) return Wt[e];
        var t = v("<" + e + ">").appendTo(i.body),
            n = t.css("display");
        t.remove();
        if (n === "none" || n === "") {
            Pt = i.body.appendChild(Pt || v.extend(i.createElement("iframe"), {
                frameBorder: 0,
                width: 0,
                height: 0
            }));
            if (!Ht || !Pt.createElement) Ht = (Pt.contentWindow || Pt.contentDocument).document, Ht.write("<!doctype html><html><body>"), Ht.close();
            t = Ht.body.appendChild(Ht.createElement(e)), n = Dt(t, "display"), i.body.removeChild(Pt)
        }
        return Wt[e] = n, n
    }

    function fn(e, t, n, r) {
        var i;
        if (v.isArray(t)) v.each(t, function (t, i) {
            n || sn.test(e) ? r(e, i) : fn(e + "[" + (typeof i == "object" ? t : "") + "]", i, n, r)
        });
        else if (!n && v.type(t) === "object")
            for (i in t) fn(e + "[" + i + "]", t[i], n, r);
        else r(e, t)
    }

    function Cn(e) {
        return function (t, n) {
            typeof t != "string" && (n = t, t = "*");
            var r, i, s, o = t.toLowerCase().split(y),
                u = 0,
                a = o.length;
            if (v.isFunction(n))
                for (; u < a; u++) r = o[u], s = /^\+/.test(r), s && (r = r.substr(1) || "*"), i = e[r] = e[r] || [], i[s ? "unshift" : "push"](n)
        }
    }

    function kn(e, n, r, i, s, o) {
        s = s || n.dataTypes[0], o = o || {}, o[s] = !0;
        var u, a = e[s],
            f = 0,
            l = a ? a.length : 0,
            c = e === Sn;
        for (; f < l && (c || !u); f++) u = a[f](n, r, i), typeof u == "string" && (!c || o[u] ? u = t : (n.dataTypes.unshift(u), u = kn(e, n, r, i, u, o)));
        return (c || !u) && !o["*"] && (u = kn(e, n, r, i, "*", o)), u
    }

    function Ln(e, n) {
        var r, i, s = v.ajaxSettings.flatOptions || {};
        for (r in n) n[r] !== t && ((s[r] ? e : i || (i = {}))[r] = n[r]);
        i && v.extend(!0, e, i)
    }

    function An(e, n, r) {
        var i, s, o, u, a = e.contents,
            f = e.dataTypes,
            l = e.responseFields;
        for (s in l) s in r && (n[l[s]] = r[s]);
        while (f[0] === "*") f.shift(), i === t && (i = e.mimeType || n.getResponseHeader("content-type"));
        if (i)
            for (s in a)
                if (a[s] && a[s].test(i)) {
                    f.unshift(s);
                    break
                }
        if (f[0] in r) o = f[0];
        else {
            for (s in r) {
                if (!f[0] || e.converters[s + " " + f[0]]) {
                    o = s;
                    break
                }
                u || (u = s)
            }
            o = o || u
        }
        if (o) return o !== f[0] && f.unshift(o), r[o]
    }

    function On(e, t) {
        var n, r, i, s, o = e.dataTypes.slice(),
            u = o[0],
            a = {},
            f = 0;
        e.dataFilter && (t = e.dataFilter(t, e.dataType));
        if (o[1])
            for (n in e.converters) a[n.toLowerCase()] = e.converters[n];
        for (; i = o[++f];)
            if (i !== "*") {
                if (u !== "*" && u !== i) {
                    n = a[u + " " + i] || a["* " + i];
                    if (!n)
                        for (r in a) {
                            s = r.split(" ");
                            if (s[1] === i) {
                                n = a[u + " " + s[0]] || a["* " + s[0]];
                                if (n) {
                                    n === !0 ? n = a[r] : a[r] !== !0 && (i = s[0], o.splice(f--, 0, i));
                                    break
                                }
                            }
                        }
                    if (n !== !0)
                        if (n && e["throws"]) t = n(t);
                        else try {
                            t = n(t)
                        } catch (l) {
                            return {
                                state: "parsererror",
                                error: n ? l : "No conversion from " + u + " to " + i
                            }
                        }
                }
                u = i
            }
        return {
            state: "success",
            data: t
        }
    }

    function Fn() {
        try {
            return new e.XMLHttpRequest
        } catch (t) { }
    }

    function In() {
        try {
            return new e.ActiveXObject("Microsoft.XMLHTTP")
        } catch (t) { }
    }

    function $n() {
        return setTimeout(function () {
            qn = t
        }, 0), qn = v.now()
    }

    function Jn(e, t) {
        v.each(t, function (t, n) {
            var r = (Vn[t] || []).concat(Vn["*"]),
                i = 0,
                s = r.length;
            for (; i < s; i++)
                if (r[i].call(e, t, n)) return
        })
    }

    function Kn(e, t, n) {
        var r, i = 0,
            s = 0,
            o = Xn.length,
            u = v.Deferred().always(function () {
                delete a.elem
            }),
            a = function () {
                var t = qn || $n(),
                    n = Math.max(0, f.startTime + f.duration - t),
                    r = n / f.duration || 0,
                    i = 1 - r,
                    s = 0,
                    o = f.tweens.length;
                for (; s < o; s++) f.tweens[s].run(i);
                return u.notifyWith(e, [f, i, n]), i < 1 && o ? n : (u.resolveWith(e, [f]), !1)
            },
            f = u.promise({
                elem: e,
                props: v.extend({}, t),
                opts: v.extend(!0, {
                    specialEasing: {}
                }, n),
                originalProperties: t,
                originalOptions: n,
                startTime: qn || $n(),
                duration: n.duration,
                tweens: [],
                createTween: function (t, n, r) {
                    var i = v.Tween(e, f.opts, t, n, f.opts.specialEasing[t] || f.opts.easing);
                    return f.tweens.push(i), i
                },
                stop: function (t) {
                    var n = 0,
                        r = t ? f.tweens.length : 0;
                    for (; n < r; n++) f.tweens[n].run(1);
                    return t ? u.resolveWith(e, [f, t]) : u.rejectWith(e, [f, t]), this
                }
            }),
            l = f.props;
        Qn(l, f.opts.specialEasing);
        for (; i < o; i++) {
            r = Xn[i].call(f, e, l, f.opts);
            if (r) return r
        }
        return Jn(f, l), v.isFunction(f.opts.start) && f.opts.start.call(e, f), v.fx.timer(v.extend(a, {
            anim: f,
            queue: f.opts.queue,
            elem: e
        })), f.progress(f.opts.progress).done(f.opts.done, f.opts.complete).fail(f.opts.fail).always(f.opts.always)
    }

    function Qn(e, t) {
        var n, r, i, s, o;
        for (n in e) {
            r = v.camelCase(n), i = t[r], s = e[n], v.isArray(s) && (i = s[1], s = e[n] = s[0]), n !== r && (e[r] = s, delete e[n]), o = v.cssHooks[r];
            if (o && "expand" in o) {
                s = o.expand(s), delete e[r];
                for (n in s) n in e || (e[n] = s[n], t[n] = i)
            } else t[r] = i
        }
    }

    function Gn(e, t, n) {
        var r, i, s, o, u, a, f, l, c, h = this,
            p = e.style,
            d = {},
            m = [],
            g = e.nodeType && Gt(e);
        n.queue || (l = v._queueHooks(e, "fx"), l.unqueued == null && (l.unqueued = 0, c = l.empty.fire, l.empty.fire = function () {
            l.unqueued || c()
        }), l.unqueued++ , h.always(function () {
            h.always(function () {
                l.unqueued-- , v.queue(e, "fx").length || l.empty.fire()
            })
        })), e.nodeType === 1 && ("height" in t || "width" in t) && (n.overflow = [p.overflow, p.overflowX, p.overflowY], v.css(e, "display") === "inline" && v.css(e, "float") === "none" && (!v.support.inlineBlockNeedsLayout || nn(e.nodeName) === "inline" ? p.display = "inline-block" : p.zoom = 1)), n.overflow && (p.overflow = "hidden", v.support.shrinkWrapBlocks || h.done(function () {
            p.overflow = n.overflow[0], p.overflowX = n.overflow[1], p.overflowY = n.overflow[2]
        }));
        for (r in t) {
            s = t[r];
            if (Un.exec(s)) {
                delete t[r], a = a || s === "toggle";
                if (s === (g ? "hide" : "show")) continue;
                m.push(r)
            }
        }
        o = m.length;
        if (o) {
            u = v._data(e, "fxshow") || v._data(e, "fxshow", {}), "hidden" in u && (g = u.hidden), a && (u.hidden = !g), g ? v(e).show() : h.done(function () {
                v(e).hide()
            }), h.done(function () {
                var t;
                v.removeData(e, "fxshow", !0);
                for (t in d) v.style(e, t, d[t])
            });
            for (r = 0; r < o; r++) i = m[r], f = h.createTween(i, g ? u[i] : 0), d[i] = u[i] || v.style(e, i), i in u || (u[i] = f.start, g && (f.end = f.start, f.start = i === "width" || i === "height" ? 1 : 0))
        }
    }

    function Yn(e, t, n, r, i) {
        return new Yn.prototype.init(e, t, n, r, i)
    }

    function Zn(e, t) {
        var n, r = {
            height: e
        },
            i = 0;
        t = t ? 1 : 0;
        for (; i < 4; i += 2 - t) n = $t[i], r["margin" + n] = r["padding" + n] = e;
        return t && (r.opacity = r.width = e), r
    }

    function tr(e) {
        return v.isWindow(e) ? e : e.nodeType === 9 ? e.defaultView || e.parentWindow : !1
    }
    var n, r, i = e.document,
        s = e.location,
        o = e.navigator,
        u = e.jQuery,
        a = e.$,
        f = Array.prototype.push,
        l = Array.prototype.slice,
        c = Array.prototype.indexOf,
        h = Object.prototype.toString,
        p = Object.prototype.hasOwnProperty,
        d = String.prototype.trim,
        v = function (e, t) {
            return new v.fn.init(e, t, n)
        },
        m = /[\-+]?(?:\d*\.|)\d+(?:[eE][\-+]?\d+|)/.source,
        g = /\S/,
        y = /\s+/,
        b = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        w = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,
        E = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        S = /^[\],:{}\s]*$/,
        x = /(?:^|:|,)(?:\s*\[)+/g,
        T = /\\(?:["\\\/bfnrt]|u[\da-fA-F]{4})/g,
        N = /"[^"\\\r\n]*"|true|false|null|-?(?:\d\d*\.|)\d+(?:[eE][\-+]?\d+|)/g,
        C = /^-ms-/,
        k = /-([\da-z])/gi,
        L = function (e, t) {
            return (t + "").toUpperCase()
        },
        A = function () {
            i.addEventListener ? (i.removeEventListener("DOMContentLoaded", A, !1), v.ready()) : i.readyState === "complete" && (i.detachEvent("onreadystatechange", A), v.ready())
        },
        O = {};
    v.fn = v.prototype = {
        constructor: v,
        init: function (e, n, r) {
            var s, o, u, a;
            if (!e) return this;
            if (e.nodeType) return this.context = this[0] = e, this.length = 1, this;
            if (typeof e == "string") {
                e.charAt(0) === "<" && e.charAt(e.length - 1) === ">" && e.length >= 3 ? s = [null, e, null] : s = w.exec(e);
                if (s && (s[1] || !n)) {
                    if (s[1]) return n = n instanceof v ? n[0] : n, a = n && n.nodeType ? n.ownerDocument || n : i, e = v.parseHTML(s[1], a, !0), E.test(s[1]) && v.isPlainObject(n) && this.attr.call(e, n, !0), v.merge(this, e);
                    o = i.getElementById(s[2]);
                    if (o && o.parentNode) {
                        if (o.id !== s[2]) return r.find(e);
                        this.length = 1, this[0] = o
                    }
                    return this.context = i, this.selector = e, this
                }
                return !n || n.jquery ? (n || r).find(e) : this.constructor(n).find(e)
            }
            return v.isFunction(e) ? r.ready(e) : (e.selector !== t && (this.selector = e.selector, this.context = e.context), v.makeArray(e, this))
        },
        selector: "",
        jquery: "1.8.3",
        length: 0,
        size: function () {
            return this.length
        },
        toArray: function () {
            return l.call(this)
        },
        get: function (e) {
            return e == null ? this.toArray() : e < 0 ? this[this.length + e] : this[e]
        },
        pushStack: function (e, t, n) {
            var r = v.merge(this.constructor(), e);
            return r.prevObject = this, r.context = this.context, t === "find" ? r.selector = this.selector + (this.selector ? " " : "") + n : t && (r.selector = this.selector + "." + t + "(" + n + ")"), r
        },
        each: function (e, t) {
            return v.each(this, e, t)
        },
        ready: function (e) {
            return v.ready.promise().done(e), this
        },
        eq: function (e) {
            return e = +e, e === -1 ? this.slice(e) : this.slice(e, e + 1)
        },
        first: function () {
            return this.eq(0)
        },
        last: function () {
            return this.eq(-1)
        },
        slice: function () {
            return this.pushStack(l.apply(this, arguments), "slice", l.call(arguments).join(","))
        },
        map: function (e) {
            return this.pushStack(v.map(this, function (t, n) {
                return e.call(t, n, t)
            }))
        },
        end: function () {
            return this.prevObject || this.constructor(null)
        },
        push: f,
        sort: [].sort,
        splice: [].splice
    }, v.fn.init.prototype = v.fn, v.extend = v.fn.extend = function () {
        var e, n, r, i, s, o, u = arguments[0] || {},
            a = 1,
            f = arguments.length,
            l = !1;
        typeof u == "boolean" && (l = u, u = arguments[1] || {}, a = 2), typeof u != "object" && !v.isFunction(u) && (u = {}), f === a && (u = this, --a);
        for (; a < f; a++)
            if ((e = arguments[a]) != null)
                for (n in e) {
                    r = u[n], i = e[n];
                    if (u === i) continue;
                    l && i && (v.isPlainObject(i) || (s = v.isArray(i))) ? (s ? (s = !1, o = r && v.isArray(r) ? r : []) : o = r && v.isPlainObject(r) ? r : {}, u[n] = v.extend(l, o, i)) : i !== t && (u[n] = i)
                }
        return u
    }, v.extend({
        noConflict: function (t) {
            return e.$ === v && (e.$ = a), t && e.jQuery === v && (e.jQuery = u), v
        },
        isReady: !1,
        readyWait: 1,
        holdReady: function (e) {
            e ? v.readyWait++ : v.ready(!0)
        },
        ready: function (e) {
            if (e === !0 ? --v.readyWait : v.isReady) return;
            if (!i.body) return setTimeout(v.ready, 1);
            v.isReady = !0;
            if (e !== !0 && --v.readyWait > 0) return;
            r.resolveWith(i, [v]), v.fn.trigger && v(i).trigger("ready").off("ready")
        },
        isFunction: function (e) {
            return v.type(e) === "function"
        },
        isArray: Array.isArray || function (e) {
            return v.type(e) === "array"
        },
        isWindow: function (e) {
            return e != null && e == e.window
        },
        isNumeric: function (e) {
            return !isNaN(parseFloat(e)) && isFinite(e)
        },
        type: function (e) {
            return e == null ? String(e) : O[h.call(e)] || "object"
        },
        isPlainObject: function (e) {
            if (!e || v.type(e) !== "object" || e.nodeType || v.isWindow(e)) return !1;
            try {
                if (e.constructor && !p.call(e, "constructor") && !p.call(e.constructor.prototype, "isPrototypeOf")) return !1
            } catch (n) {
                return !1
            }
            var r;
            for (r in e);
            return r === t || p.call(e, r)
        },
        isEmptyObject: function (e) {
            var t;
            for (t in e) return !1;
            return !0
        },
        error: function (e) {
            throw new Error(e)
        },
        parseHTML: function (e, t, n) {
            var r;
            return !e || typeof e != "string" ? null : (typeof t == "boolean" && (n = t, t = 0), t = t || i, (r = E.exec(e)) ? [t.createElement(r[1])] : (r = v.buildFragment([e], t, n ? null : []), v.merge([], (r.cacheable ? v.clone(r.fragment) : r.fragment).childNodes)))
        },
        parseJSON: function (t) {
            if (!t || typeof t != "string") return null;
            t = v.trim(t);
            if (e.JSON && e.JSON.parse) return e.JSON.parse(t);
            if (S.test(t.replace(T, "@").replace(N, "]").replace(x, ""))) return (new Function("return " + t))();
            v.error("Invalid JSON: " + t)
        },
        parseXML: function (n) {
            var r, i;
            if (!n || typeof n != "string") return null;
            try {
                e.DOMParser ? (i = new DOMParser, r = i.parseFromString(n, "text/xml")) : (r = new ActiveXObject("Microsoft.XMLDOM"), r.async = "false", r.loadXML(n))
            } catch (s) {
                r = t
            }
            return (!r || !r.documentElement || r.getElementsByTagName("parsererror").length) && v.error("Invalid XML: " + n), r
        },
        noop: function () { },
        globalEval: function (t) {
            t && g.test(t) && (e.execScript || function (t) {
                e.eval.call(e, t)
            })(t)
        },
        camelCase: function (e) {
            return e.replace(C, "ms-").replace(k, L)
        },
        nodeName: function (e, t) {
            return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
        },
        each: function (e, n, r) {
            var i, s = 0,
                o = e.length,
                u = o === t || v.isFunction(e);
            if (r) {
                if (u) {
                    for (i in e)
                        if (n.apply(e[i], r) === !1) break
                } else
                    for (; s < o;)
                        if (n.apply(e[s++], r) === !1) break
            } else if (u) {
                for (i in e)
                    if (n.call(e[i], i, e[i]) === !1) break
            } else
                for (; s < o;)
                    if (n.call(e[s], s, e[s++]) === !1) break; return e
        },
        trim: d && !d.call("\ufeff\u00a0") ? function (e) {
            return e == null ? "" : d.call(e)
        } : function (e) {
            return e == null ? "" : (e + "").replace(b, "")
        },
        makeArray: function (e, t) {
            var n, r = t || [];
            return e != null && (n = v.type(e), e.length == null || n === "string" || n === "function" || n === "regexp" || v.isWindow(e) ? f.call(r, e) : v.merge(r, e)), r
        },
        inArray: function (e, t, n) {
            var r;
            if (t) {
                if (c) return c.call(t, e, n);
                r = t.length, n = n ? n < 0 ? Math.max(0, r + n) : n : 0;
                for (; n < r; n++)
                    if (n in t && t[n] === e) return n
            }
            return -1
        },
        merge: function (e, n) {
            var r = n.length,
                i = e.length,
                s = 0;
            if (typeof r == "number")
                for (; s < r; s++) e[i++] = n[s];
            else
                while (n[s] !== t) e[i++] = n[s++];
            return e.length = i, e
        },
        grep: function (e, t, n) {
            var r, i = [],
                s = 0,
                o = e.length;
            n = !!n;
            for (; s < o; s++) r = !!t(e[s], s), n !== r && i.push(e[s]);
            return i
        },
        map: function (e, n, r) {
            var i, s, o = [],
                u = 0,
                a = e.length,
                f = e instanceof v || a !== t && typeof a == "number" && (a > 0 && e[0] && e[a - 1] || a === 0 || v.isArray(e));
            if (f)
                for (; u < a; u++) i = n(e[u], u, r), i != null && (o[o.length] = i);
            else
                for (s in e) i = n(e[s], s, r), i != null && (o[o.length] = i);
            return o.concat.apply([], o)
        },
        guid: 1,
        proxy: function (e, n) {
            var r, i, s;
            return typeof n == "string" && (r = e[n], n = e, e = r), v.isFunction(e) ? (i = l.call(arguments, 2), s = function () {
                return e.apply(n, i.concat(l.call(arguments)))
            }, s.guid = e.guid = e.guid || v.guid++ , s) : t
        },
        access: function (e, n, r, i, s, o, u) {
            var a, f = r == null,
                l = 0,
                c = e.length;
            if (r && typeof r == "object") {
                for (l in r) v.access(e, n, l, r[l], 1, o, i);
                s = 1
            } else if (i !== t) {
                a = u === t && v.isFunction(i), f && (a ? (a = n, n = function (e, t, n) {
                    return a.call(v(e), n)
                }) : (n.call(e, i), n = null));
                if (n)
                    for (; l < c; l++) n(e[l], r, a ? i.call(e[l], l, n(e[l], r)) : i, u);
                s = 1
            }
            return s ? e : f ? n.call(e) : c ? n(e[0], r) : o
        },
        now: function () {
            return (new Date).getTime()
        }
    }), v.ready.promise = function (t) {
        if (!r) {
            r = v.Deferred();
            if (i.readyState === "complete") setTimeout(v.ready, 1);
            else if (i.addEventListener) i.addEventListener("DOMContentLoaded", A, !1), e.addEventListener("load", v.ready, !1);
            else {
                i.attachEvent("onreadystatechange", A), e.attachEvent("onload", v.ready);
                var n = !1;
                try {
                    n = e.frameElement == null && i.documentElement
                } catch (s) { }
                n && n.doScroll && function o() {
                    if (!v.isReady) {
                        try {
                            n.doScroll("left")
                        } catch (e) {
                            return setTimeout(o, 50)
                        }
                        v.ready()
                    }
                }()
            }
        }
        return r.promise(t)
    }, v.each("Boolean Number String Function Array Date RegExp Object".split(" "), function (e, t) {
        O["[object " + t + "]"] = t.toLowerCase()
    }), n = v(i);
    var M = {};
    v.Callbacks = function (e) {
        e = typeof e == "string" ? M[e] || _(e) : v.extend({}, e);
        var n, r, i, s, o, u, a = [],
            f = !e.once && [],
            l = function (t) {
                n = e.memory && t, r = !0, u = s || 0, s = 0, o = a.length, i = !0;
                for (; a && u < o; u++)
                    if (a[u].apply(t[0], t[1]) === !1 && e.stopOnFalse) {
                        n = !1;
                        break
                    }
                i = !1, a && (f ? f.length && l(f.shift()) : n ? a = [] : c.disable())
            },
            c = {
                add: function () {
                    if (a) {
                        var t = a.length;
                        (function r(t) {
                            v.each(t, function (t, n) {
                                var i = v.type(n);
                                i === "function" ? (!e.unique || !c.has(n)) && a.push(n) : n && n.length && i !== "string" && r(n)
                            })
                        })(arguments), i ? o = a.length : n && (s = t, l(n))
                    }
                    return this
                },
                remove: function () {
                    return a && v.each(arguments, function (e, t) {
                        var n;
                        while ((n = v.inArray(t, a, n)) > -1) a.splice(n, 1), i && (n <= o && o-- , n <= u && u--)
                    }), this
                },
                has: function (e) {
                    return v.inArray(e, a) > -1
                },
                empty: function () {
                    return a = [], this
                },
                disable: function () {
                    return a = f = n = t, this
                },
                disabled: function () {
                    return !a
                },
                lock: function () {
                    return f = t, n || c.disable(), this
                },
                locked: function () {
                    return !f
                },
                fireWith: function (e, t) {
                    return t = t || [], t = [e, t.slice ? t.slice() : t], a && (!r || f) && (i ? f.push(t) : l(t)), this
                },
                fire: function () {
                    return c.fireWith(this, arguments), this
                },
                fired: function () {
                    return !!r
                }
            };
        return c
    }, v.extend({
        Deferred: function (e) {
            var t = [
                ["resolve", "done", v.Callbacks("once memory"), "resolved"],
                ["reject", "fail", v.Callbacks("once memory"), "rejected"],
                ["notify", "progress", v.Callbacks("memory")]
            ],
                n = "pending",
                r = {
                    state: function () {
                        return n
                    },
                    always: function () {
                        return i.done(arguments).fail(arguments), this
                    },
                    then: function () {
                        var e = arguments;
                        return v.Deferred(function (n) {
                            v.each(t, function (t, r) {
                                var s = r[0],
                                    o = e[t];
                                i[r[1]](v.isFunction(o) ? function () {
                                    var e = o.apply(this, arguments);
                                    e && v.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[s + "With"](this === i ? n : this, [e])
                                } : n[s])
                            }), e = null
                        }).promise()
                    },
                    promise: function (e) {
                        return e != null ? v.extend(e, r) : r
                    }
                },
                i = {};
            return r.pipe = r.then, v.each(t, function (e, s) {
                var o = s[2],
                    u = s[3];
                r[s[1]] = o.add, u && o.add(function () {
                    n = u
                }, t[e ^ 1][2].disable, t[2][2].lock), i[s[0]] = o.fire, i[s[0] + "With"] = o.fireWith
            }), r.promise(i), e && e.call(i, i), i
        },
        when: function (e) {
            var t = 0,
                n = l.call(arguments),
                r = n.length,
                i = r !== 1 || e && v.isFunction(e.promise) ? r : 0,
                s = i === 1 ? e : v.Deferred(),
                o = function (e, t, n) {
                    return function (r) {
                        t[e] = this, n[e] = arguments.length > 1 ? l.call(arguments) : r, n === u ? s.notifyWith(t, n) : --i || s.resolveWith(t, n)
                    }
                },
                u, a, f;
            if (r > 1) {
                u = new Array(r), a = new Array(r), f = new Array(r);
                for (; t < r; t++) n[t] && v.isFunction(n[t].promise) ? n[t].promise().done(o(t, f, n)).fail(s.reject).progress(o(t, a, u)) : --i
            }
            return i || s.resolveWith(f, n), s.promise()
        }
    }), v.support = function () {
        var t, n, r, s, o, u, a, f, l, c, h, p = i.createElement("div");
        p.setAttribute("className", "t"), p.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", n = p.getElementsByTagName("*"), r = p.getElementsByTagName("a")[0];
        if (!n || !r || !n.length) return {};
        s = i.createElement("select"), o = s.appendChild(i.createElement("option")), u = p.getElementsByTagName("input")[0], r.style.cssText = "top:1px;float:left;opacity:.5", t = {
            leadingWhitespace: p.firstChild.nodeType === 3,
            tbody: !p.getElementsByTagName("tbody").length,
            htmlSerialize: !!p.getElementsByTagName("link").length,
            style: /top/.test(r.getAttribute("style")),
            hrefNormalized: r.getAttribute("href") === "/a",
            opacity: /^0.5/.test(r.style.opacity),
            cssFloat: !!r.style.cssFloat,
            checkOn: u.value === "on",
            optSelected: o.selected,
            getSetAttribute: p.className !== "t",
            enctype: !!i.createElement("form").enctype,
            html5Clone: i.createElement("nav").cloneNode(!0).outerHTML !== "<:nav></:nav>",
            boxModel: i.compatMode === "CSS1Compat",
            submitBubbles: !0,
            changeBubbles: !0,
            focusinBubbles: !1,
            deleteExpando: !0,
            noCloneEvent: !0,
            inlineBlockNeedsLayout: !1,
            shrinkWrapBlocks: !1,
            reliableMarginRight: !0,
            boxSizingReliable: !0,
            pixelPosition: !1
        }, u.checked = !0, t.noCloneChecked = u.cloneNode(!0).checked, s.disabled = !0, t.optDisabled = !o.disabled;
        try {
            delete p.test
        } catch (d) {
            t.deleteExpando = !1
        } !p.addEventListener && p.attachEvent && p.fireEvent && (p.attachEvent("onclick", h = function () {
            t.noCloneEvent = !1
        }), p.cloneNode(!0).fireEvent("onclick"), p.detachEvent("onclick", h)), u = i.createElement("input"), u.value = "t", u.setAttribute("type", "radio"), t.radioValue = u.value === "t", u.setAttribute("checked", "checked"), u.setAttribute("name", "t"), p.appendChild(u), a = i.createDocumentFragment(), a.appendChild(p.lastChild), t.checkClone = a.cloneNode(!0).cloneNode(!0).lastChild.checked, t.appendChecked = u.checked, a.removeChild(u), a.appendChild(p);
        if (p.attachEvent)
            for (l in {
                submit: !0,
                change: !0,
                focusin: !0
            }) f = "on" + l, c = f in p, c || (p.setAttribute(f, "return;"), c = typeof p[f] == "function"), t[l + "Bubbles"] = c;
        return v(function () {
            var n, r, s, o, u = "padding:0;margin:0;border:0;display:block;overflow:hidden;",
                a = i.getElementsByTagName("body")[0];
            if (!a) return;
            n = i.createElement("div"), n.style.cssText = "visibility:hidden;border:0;width:0;height:0;position:static;top:0;margin-top:1px", a.insertBefore(n, a.firstChild), r = i.createElement("div"), n.appendChild(r), r.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", s = r.getElementsByTagName("td"), s[0].style.cssText = "padding:0;margin:0;border:0;display:none", c = s[0].offsetHeight === 0, s[0].style.display = "", s[1].style.display = "none", t.reliableHiddenOffsets = c && s[0].offsetHeight === 0, r.innerHTML = "", r.style.cssText = "box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%;", t.boxSizing = r.offsetWidth === 4, t.doesNotIncludeMarginInBodyOffset = a.offsetTop !== 1, e.getComputedStyle && (t.pixelPosition = (e.getComputedStyle(r, null) || {}).top !== "1%", t.boxSizingReliable = (e.getComputedStyle(r, null) || {
                width: "4px"
            }).width === "4px", o = i.createElement("div"), o.style.cssText = r.style.cssText = u, o.style.marginRight = o.style.width = "0", r.style.width = "1px", r.appendChild(o), t.reliableMarginRight = !parseFloat((e.getComputedStyle(o, null) || {}).marginRight)), typeof r.style.zoom != "undefined" && (r.innerHTML = "", r.style.cssText = u + "width:1px;padding:1px;display:inline;zoom:1", t.inlineBlockNeedsLayout = r.offsetWidth === 3, r.style.display = "block", r.style.overflow = "visible", r.innerHTML = "<div></div>", r.firstChild.style.width = "5px", t.shrinkWrapBlocks = r.offsetWidth !== 3, n.style.zoom = 1), a.removeChild(n), n = r = s = o = null
        }), a.removeChild(p), n = r = s = o = u = a = p = null, t
    }();
    var D = /(?:\{[\s\S]*\}|\[[\s\S]*\])$/,
        P = /([A-Z])/g;
    v.extend({
        cache: {},
        deletedIds: [],
        uuid: 0,
        expando: "jQuery" + (v.fn.jquery + Math.random()).replace(/\D/g, ""),
        noData: {
            embed: !0,
            object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
            applet: !0
        },
        hasData: function (e) {
            return e = e.nodeType ? v.cache[e[v.expando]] : e[v.expando], !!e && !B(e)
        },
        data: function (e, n, r, i) {
            if (!v.acceptData(e)) return;
            var s, o, u = v.expando,
                a = typeof n == "string",
                f = e.nodeType,
                l = f ? v.cache : e,
                c = f ? e[u] : e[u] && u;
            if ((!c || !l[c] || !i && !l[c].data) && a && r === t) return;
            c || (f ? e[u] = c = v.deletedIds.pop() || v.guid++ : c = u), l[c] || (l[c] = {}, f || (l[c].toJSON = v.noop));
            if (typeof n == "object" || typeof n == "function") i ? l[c] = v.extend(l[c], n) : l[c].data = v.extend(l[c].data, n);
            return s = l[c], i || (s.data || (s.data = {}), s = s.data), r !== t && (s[v.camelCase(n)] = r), a ? (o = s[n], o == null && (o = s[v.camelCase(n)])) : o = s, o
        },
        removeData: function (e, t, n) {
            if (!v.acceptData(e)) return;
            var r, i, s, o = e.nodeType,
                u = o ? v.cache : e,
                a = o ? e[v.expando] : v.expando;
            if (!u[a]) return;
            if (t) {
                r = n ? u[a] : u[a].data;
                if (r) {
                    v.isArray(t) || (t in r ? t = [t] : (t = v.camelCase(t), t in r ? t = [t] : t = t.split(" ")));
                    for (i = 0, s = t.length; i < s; i++) delete r[t[i]];
                    if (!(n ? B : v.isEmptyObject)(r)) return
                }
            }
            if (!n) {
                delete u[a].data;
                if (!B(u[a])) return
            }
            o ? v.cleanData([e], !0) : v.support.deleteExpando || u != u.window ? delete u[a] : u[a] = null
        },
        _data: function (e, t, n) {
            return v.data(e, t, n, !0)
        },
        acceptData: function (e) {
            var t = e.nodeName && v.noData[e.nodeName.toLowerCase()];
            return !t || t !== !0 && e.getAttribute("classid") === t
        }
    }), v.fn.extend({
        data: function (e, n) {
            var r, i, s, o, u, a = this[0],
                f = 0,
                l = null;
            if (e === t) {
                if (this.length) {
                    l = v.data(a);
                    if (a.nodeType === 1 && !v._data(a, "parsedAttrs")) {
                        s = a.attributes;
                        for (u = s.length; f < u; f++) o = s[f].name, o.indexOf("data-") || (o = v.camelCase(o.substring(5)), H(a, o, l[o]));
                        v._data(a, "parsedAttrs", !0)
                    }
                }
                return l
            }
            return typeof e == "object" ? this.each(function () {
                v.data(this, e)
            }) : (r = e.split(".", 2), r[1] = r[1] ? "." + r[1] : "", i = r[1] + "!", v.access(this, function (n) {
                if (n === t) return l = this.triggerHandler("getData" + i, [r[0]]), l === t && a && (l = v.data(a, e), l = H(a, e, l)), l === t && r[1] ? this.data(r[0]) : l;
                r[1] = n, this.each(function () {
                    var t = v(this);
                    t.triggerHandler("setData" + i, r), v.data(this, e, n), t.triggerHandler("changeData" + i, r)
                })
            }, null, n, arguments.length > 1, null, !1))
        },
        removeData: function (e) {
            return this.each(function () {
                v.removeData(this, e)
            })
        }
    }), v.extend({
        queue: function (e, t, n) {
            var r;
            if (e) return t = (t || "fx") + "queue", r = v._data(e, t), n && (!r || v.isArray(n) ? r = v._data(e, t, v.makeArray(n)) : r.push(n)), r || []
        },
        dequeue: function (e, t) {
            t = t || "fx";
            var n = v.queue(e, t),
                r = n.length,
                i = n.shift(),
                s = v._queueHooks(e, t),
                o = function () {
                    v.dequeue(e, t)
                };
            i === "inprogress" && (i = n.shift(), r--), i && (t === "fx" && n.unshift("inprogress"), delete s.stop, i.call(e, o, s)), !r && s && s.empty.fire()
        },
        _queueHooks: function (e, t) {
            var n = t + "queueHooks";
            return v._data(e, n) || v._data(e, n, {
                empty: v.Callbacks("once memory").add(function () {
                    v.removeData(e, t + "queue", !0), v.removeData(e, n, !0)
                })
            })
        }
    }), v.fn.extend({
        queue: function (e, n) {
            var r = 2;
            return typeof e != "string" && (n = e, e = "fx", r--), arguments.length < r ? v.queue(this[0], e) : n === t ? this : this.each(function () {
                var t = v.queue(this, e, n);
                v._queueHooks(this, e), e === "fx" && t[0] !== "inprogress" && v.dequeue(this, e)
            })
        },
        dequeue: function (e) {
            return this.each(function () {
                v.dequeue(this, e)
            })
        },
        delay: function (e, t) {
            return e = v.fx ? v.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function (t, n) {
                var r = setTimeout(t, e);
                n.stop = function () {
                    clearTimeout(r)
                }
            })
        },
        clearQueue: function (e) {
            return this.queue(e || "fx", [])
        },
        promise: function (e, n) {
            var r, i = 1,
                s = v.Deferred(),
                o = this,
                u = this.length,
                a = function () {
                    --i || s.resolveWith(o, [o])
                };
            typeof e != "string" && (n = e, e = t), e = e || "fx";
            while (u--) r = v._data(o[u], e + "queueHooks"), r && r.empty && (i++ , r.empty.add(a));
            return a(), s.promise(n)
        }
    });
    var j, F, I, q = /[\t\r\n]/g,
        R = /\r/g,
        U = /^(?:button|input)$/i,
        z = /^(?:button|input|object|select|textarea)$/i,
        W = /^a(?:rea|)$/i,
        X = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
        V = v.support.getSetAttribute;
    v.fn.extend({
        attr: function (e, t) {
            return v.access(this, v.attr, e, t, arguments.length > 1)
        },
        removeAttr: function (e) {
            return this.each(function () {
                v.removeAttr(this, e)
            })
        },
        prop: function (e, t) {
            return v.access(this, v.prop, e, t, arguments.length > 1)
        },
        removeProp: function (e) {
            return e = v.propFix[e] || e, this.each(function () {
                try {
                    this[e] = t, delete this[e]
                } catch (n) { }
            })
        },
        addClass: function (e) {
            var t, n, r, i, s, o, u;
            if (v.isFunction(e)) return this.each(function (t) {
                v(this).addClass(e.call(this, t, this.className))
            });
            if (e && typeof e == "string") {
                t = e.split(y);
                for (n = 0, r = this.length; n < r; n++) {
                    i = this[n];
                    if (i.nodeType === 1)
                        if (!i.className && t.length === 1) i.className = e;
                        else {
                            s = " " + i.className + " ";
                            for (o = 0, u = t.length; o < u; o++) s.indexOf(" " + t[o] + " ") < 0 && (s += t[o] + " ");
                            i.className = v.trim(s)
                        }
                }
            }
            return this
        },
        removeClass: function (e) {
            var n, r, i, s, o, u, a;
            if (v.isFunction(e)) return this.each(function (t) {
                v(this).removeClass(e.call(this, t, this.className))
            });
            if (e && typeof e == "string" || e === t) {
                n = (e || "").split(y);
                for (u = 0, a = this.length; u < a; u++) {
                    i = this[u];
                    if (i.nodeType === 1 && i.className) {
                        r = (" " + i.className + " ").replace(q, " ");
                        for (s = 0, o = n.length; s < o; s++)
                            while (r.indexOf(" " + n[s] + " ") >= 0) r = r.replace(" " + n[s] + " ", " ");
                        i.className = e ? v.trim(r) : ""
                    }
                }
            }
            return this
        },
        toggleClass: function (e, t) {
            var n = typeof e,
                r = typeof t == "boolean";
            return v.isFunction(e) ? this.each(function (n) {
                v(this).toggleClass(e.call(this, n, this.className, t), t)
            }) : this.each(function () {
                if (n === "string") {
                    var i, s = 0,
                        o = v(this),
                        u = t,
                        a = e.split(y);
                    while (i = a[s++]) u = r ? u : !o.hasClass(i), o[u ? "addClass" : "removeClass"](i)
                } else if (n === "undefined" || n === "boolean") this.className && v._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : v._data(this, "__className__") || ""
            })
        },
        hasClass: function (e) {
            var t = " " + e + " ",
                n = 0,
                r = this.length;
            for (; n < r; n++)
                if (this[n].nodeType === 1 && (" " + this[n].className + " ").replace(q, " ").indexOf(t) >= 0) return !0;
            return !1
        },
        val: function (e) {
            var n, r, i, s = this[0];
            if (!arguments.length) {
                if (s) return n = v.valHooks[s.type] || v.valHooks[s.nodeName.toLowerCase()], n && "get" in n && (r = n.get(s, "value")) !== t ? r : (r = s.value, typeof r == "string" ? r.replace(R, "") : r == null ? "" : r);
                return
            }
            return i = v.isFunction(e), this.each(function (r) {
                var s, o = v(this);
                if (this.nodeType !== 1) return;
                i ? s = e.call(this, r, o.val()) : s = e, s == null ? s = "" : typeof s == "number" ? s += "" : v.isArray(s) && (s = v.map(s, function (e) {
                    return e == null ? "" : e + ""
                })), n = v.valHooks[this.type] || v.valHooks[this.nodeName.toLowerCase()];
                if (!n || !("set" in n) || n.set(this, s, "value") === t) this.value = s
            })
        }
    }), v.extend({
        valHooks: {
            option: {
                get: function (e) {
                    var t = e.attributes.value;
                    return !t || t.specified ? e.value : e.text
                }
            },
            select: {
                get: function (e) {
                    var t, n, r = e.options,
                        i = e.selectedIndex,
                        s = e.type === "select-one" || i < 0,
                        o = s ? null : [],
                        u = s ? i + 1 : r.length,
                        a = i < 0 ? u : s ? i : 0;
                    for (; a < u; a++) {
                        n = r[a];
                        if ((n.selected || a === i) && (v.support.optDisabled ? !n.disabled : n.getAttribute("disabled") === null) && (!n.parentNode.disabled || !v.nodeName(n.parentNode, "optgroup"))) {
                            t = v(n).val();
                            if (s) return t;
                            o.push(t)
                        }
                    }
                    return o
                },
                set: function (e, t) {
                    var n = v.makeArray(t);
                    return v(e).find("option").each(function () {
                        this.selected = v.inArray(v(this).val(), n) >= 0
                    }), n.length || (e.selectedIndex = -1), n
                }
            }
        },
        attrFn: {},
        attr: function (e, n, r, i) {
            var s, o, u, a = e.nodeType;
            if (!e || a === 3 || a === 8 || a === 2) return;
            if (i && v.isFunction(v.fn[n])) return v(e)[n](r);
            if (typeof e.getAttribute == "undefined") return v.prop(e, n, r);
            u = a !== 1 || !v.isXMLDoc(e), u && (n = n.toLowerCase(), o = v.attrHooks[n] || (X.test(n) ? F : j));
            if (r !== t) {
                if (r === null) {
                    v.removeAttr(e, n);
                    return
                }
                return o && "set" in o && u && (s = o.set(e, r, n)) !== t ? s : (e.setAttribute(n, r + ""), r)
            }
            return o && "get" in o && u && (s = o.get(e, n)) !== null ? s : (s = e.getAttribute(n), s === null ? t : s)
        },
        removeAttr: function (e, t) {
            var n, r, i, s, o = 0;
            if (t && e.nodeType === 1) {
                r = t.split(y);
                for (; o < r.length; o++) i = r[o], i && (n = v.propFix[i] || i, s = X.test(i), s || v.attr(e, i, ""), e.removeAttribute(V ? i : n), s && n in e && (e[n] = !1))
            }
        },
        attrHooks: {
            type: {
                set: function (e, t) {
                    if (U.test(e.nodeName) && e.parentNode) v.error("type property can't be changed");
                    else if (!v.support.radioValue && t === "radio" && v.nodeName(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }
            },
            value: {
                get: function (e, t) {
                    return j && v.nodeName(e, "button") ? j.get(e, t) : t in e ? e.value : null
                },
                set: function (e, t, n) {
                    if (j && v.nodeName(e, "button")) return j.set(e, t, n);
                    e.value = t
                }
            }
        },
        propFix: {
            tabindex: "tabIndex",
            readonly: "readOnly",
            "for": "htmlFor",
            "class": "className",
            maxlength: "maxLength",
            cellspacing: "cellSpacing",
            cellpadding: "cellPadding",
            rowspan: "rowSpan",
            colspan: "colSpan",
            usemap: "useMap",
            frameborder: "frameBorder",
            contenteditable: "contentEditable"
        },
        prop: function (e, n, r) {
            var i, s, o, u = e.nodeType;
            if (!e || u === 3 || u === 8 || u === 2) return;
            return o = u !== 1 || !v.isXMLDoc(e), o && (n = v.propFix[n] || n, s = v.propHooks[n]), r !== t ? s && "set" in s && (i = s.set(e, r, n)) !== t ? i : e[n] = r : s && "get" in s && (i = s.get(e, n)) !== null ? i : e[n]
        },
        propHooks: {
            tabIndex: {
                get: function (e) {
                    var n = e.getAttributeNode("tabindex");
                    return n && n.specified ? parseInt(n.value, 10) : z.test(e.nodeName) || W.test(e.nodeName) && e.href ? 0 : t
                }
            }
        }
    }), F = {
        get: function (e, n) {
            var r, i = v.prop(e, n);
            return i === !0 || typeof i != "boolean" && (r = e.getAttributeNode(n)) && r.nodeValue !== !1 ? n.toLowerCase() : t
        },
        set: function (e, t, n) {
            var r;
            return t === !1 ? v.removeAttr(e, n) : (r = v.propFix[n] || n, r in e && (e[r] = !0), e.setAttribute(n, n.toLowerCase())), n
        }
    }, V || (I = {
        name: !0,
        id: !0,
        coords: !0
    }, j = v.valHooks.button = {
        get: function (e, n) {
            var r;
            return r = e.getAttributeNode(n), r && (I[n] ? r.value !== "" : r.specified) ? r.value : t
        },
        set: function (e, t, n) {
            var r = e.getAttributeNode(n);
            return r || (r = i.createAttribute(n), e.setAttributeNode(r)), r.value = t + ""
        }
    }, v.each(["width", "height"], function (e, t) {
        v.attrHooks[t] = v.extend(v.attrHooks[t], {
            set: function (e, n) {
                if (n === "") return e.setAttribute(t, "auto"), n
            }
        })
    }), v.attrHooks.contenteditable = {
        get: j.get,
        set: function (e, t, n) {
            t === "" && (t = "false"), j.set(e, t, n)
        }
    }), v.support.hrefNormalized || v.each(["href", "src", "width", "height"], function (e, n) {
        v.attrHooks[n] = v.extend(v.attrHooks[n], {
            get: function (e) {
                var r = e.getAttribute(n, 2);
                return r === null ? t : r
            }
        })
    }), v.support.style || (v.attrHooks.style = {
        get: function (e) {
            return e.style.cssText.toLowerCase() || t
        },
        set: function (e, t) {
            return e.style.cssText = t + ""
        }
    }), v.support.optSelected || (v.propHooks.selected = v.extend(v.propHooks.selected, {
        get: function (e) {
            var t = e.parentNode;
            return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
        }
    })), v.support.enctype || (v.propFix.enctype = "encoding"), v.support.checkOn || v.each(["radio", "checkbox"], function () {
        v.valHooks[this] = {
            get: function (e) {
                return e.getAttribute("value") === null ? "on" : e.value
            }
        }
    }), v.each(["radio", "checkbox"], function () {
        v.valHooks[this] = v.extend(v.valHooks[this], {
            set: function (e, t) {
                if (v.isArray(t)) return e.checked = v.inArray(v(e).val(), t) >= 0
            }
        })
    });
    var $ = /^(?:textarea|input|select)$/i,
        J = /^([^\.]*|)(?:\.(.+)|)$/,
        K = /(?:^|\s)hover(\.\S+|)\b/,
        Q = /^key/,
        G = /^(?:mouse|contextmenu)|click/,
        Y = /^(?:focusinfocus|focusoutblur)$/,
        Z = function (e) {
            return v.event.special.hover ? e : e.replace(K, "mouseenter$1 mouseleave$1")
        };
    v.event = {
        add: function (e, n, r, i, s) {
            var o, u, a, f, l, c, h, p, d, m, g;
            if (e.nodeType === 3 || e.nodeType === 8 || !n || !r || !(o = v._data(e))) return;
            r.handler && (d = r, r = d.handler, s = d.selector), r.guid || (r.guid = v.guid++), a = o.events, a || (o.events = a = {}), u = o.handle, u || (o.handle = u = function (e) {
                return typeof v == "undefined" || !!e && v.event.triggered === e.type ? t : v.event.dispatch.apply(u.elem, arguments)
            }, u.elem = e), n = v.trim(Z(n)).split(" ");
            for (f = 0; f < n.length; f++) {
                l = J.exec(n[f]) || [], c = l[1], h = (l[2] || "").split(".").sort(), g = v.event.special[c] || {}, c = (s ? g.delegateType : g.bindType) || c, g = v.event.special[c] || {}, p = v.extend({
                    type: c,
                    origType: l[1],
                    data: i,
                    handler: r,
                    guid: r.guid,
                    selector: s,
                    needsContext: s && v.expr.match.needsContext.test(s),
                    namespace: h.join(".")
                }, d), m = a[c];
                if (!m) {
                    m = a[c] = [], m.delegateCount = 0;
                    if (!g.setup || g.setup.call(e, i, h, u) === !1) e.addEventListener ? e.addEventListener(c, u, !1) : e.attachEvent && e.attachEvent("on" + c, u)
                }
                g.add && (g.add.call(e, p), p.handler.guid || (p.handler.guid = r.guid)), s ? m.splice(m.delegateCount++, 0, p) : m.push(p), v.event.global[c] = !0
            }
            e = null
        },
        global: {},
        remove: function (e, t, n, r, i) {
            var s, o, u, a, f, l, c, h, p, d, m, g = v.hasData(e) && v._data(e);
            if (!g || !(h = g.events)) return;
            t = v.trim(Z(t || "")).split(" ");
            for (s = 0; s < t.length; s++) {
                o = J.exec(t[s]) || [], u = a = o[1], f = o[2];
                if (!u) {
                    for (u in h) v.event.remove(e, u + t[s], n, r, !0);
                    continue
                }
                p = v.event.special[u] || {}, u = (r ? p.delegateType : p.bindType) || u, d = h[u] || [], l = d.length, f = f ? new RegExp("(^|\\.)" + f.split(".").sort().join("\\.(?:.*\\.|)") + "(\\.|$)") : null;
                for (c = 0; c < d.length; c++) m = d[c], (i || a === m.origType) && (!n || n.guid === m.guid) && (!f || f.test(m.namespace)) && (!r || r === m.selector || r === "**" && m.selector) && (d.splice(c--, 1), m.selector && d.delegateCount-- , p.remove && p.remove.call(e, m));
                d.length === 0 && l !== d.length && ((!p.teardown || p.teardown.call(e, f, g.handle) === !1) && v.removeEvent(e, u, g.handle), delete h[u])
            }
            v.isEmptyObject(h) && (delete g.handle, v.removeData(e, "events", !0))
        },
        customEvent: {
            getData: !0,
            setData: !0,
            changeData: !0
        },
        trigger: function (n, r, s, o) {
            if (!s || s.nodeType !== 3 && s.nodeType !== 8) {
                var u, a, f, l, c, h, p, d, m, g, y = n.type || n,
                    b = [];
                if (Y.test(y + v.event.triggered)) return;
                y.indexOf("!") >= 0 && (y = y.slice(0, -1), a = !0), y.indexOf(".") >= 0 && (b = y.split("."), y = b.shift(), b.sort());
                if ((!s || v.event.customEvent[y]) && !v.event.global[y]) return;
                n = typeof n == "object" ? n[v.expando] ? n : new v.Event(y, n) : new v.Event(y), n.type = y, n.isTrigger = !0, n.exclusive = a, n.namespace = b.join("."), n.namespace_re = n.namespace ? new RegExp("(^|\\.)" + b.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, h = y.indexOf(":") < 0 ? "on" + y : "";
                if (!s) {
                    u = v.cache;
                    for (f in u) u[f].events && u[f].events[y] && v.event.trigger(n, r, u[f].handle.elem, !0);
                    return
                }
                n.result = t, n.target || (n.target = s), r = r != null ? v.makeArray(r) : [], r.unshift(n), p = v.event.special[y] || {};
                if (p.trigger && p.trigger.apply(s, r) === !1) return;
                m = [
                    [s, p.bindType || y]
                ];
                if (!o && !p.noBubble && !v.isWindow(s)) {
                    g = p.delegateType || y, l = Y.test(g + y) ? s : s.parentNode;
                    for (c = s; l; l = l.parentNode) m.push([l, g]), c = l;
                    c === (s.ownerDocument || i) && m.push([c.defaultView || c.parentWindow || e, g])
                }
                for (f = 0; f < m.length && !n.isPropagationStopped(); f++) l = m[f][0], n.type = m[f][1], d = (v._data(l, "events") || {})[n.type] && v._data(l, "handle"), d && d.apply(l, r), d = h && l[h], d && v.acceptData(l) && d.apply && d.apply(l, r) === !1 && n.preventDefault();
                return n.type = y, !o && !n.isDefaultPrevented() && (!p._default || p._default.apply(s.ownerDocument, r) === !1) && (y !== "click" || !v.nodeName(s, "a")) && v.acceptData(s) && h && s[y] && (y !== "focus" && y !== "blur" || n.target.offsetWidth !== 0) && !v.isWindow(s) && (c = s[h], c && (s[h] = null), v.event.triggered = y, s[y](), v.event.triggered = t, c && (s[h] = c)), n.result
            }
            return
        },
        dispatch: function (n) {
            n = v.event.fix(n || e.event);
            var r, i, s, o, u, a, f, c, h, p, d = (v._data(this, "events") || {})[n.type] || [],
                m = d.delegateCount,
                g = l.call(arguments),
                y = !n.exclusive && !n.namespace,
                b = v.event.special[n.type] || {},
                w = [];
            g[0] = n, n.delegateTarget = this;
            if (b.preDispatch && b.preDispatch.call(this, n) === !1) return;
            if (m && (!n.button || n.type !== "click"))
                for (s = n.target; s != this; s = s.parentNode || this)
                    if (s.disabled !== !0 || n.type !== "click") {
                        u = {}, f = [];
                        for (r = 0; r < m; r++) c = d[r], h = c.selector, u[h] === t && (u[h] = c.needsContext ? v(h, this).index(s) >= 0 : v.find(h, this, null, [s]).length), u[h] && f.push(c);
                        f.length && w.push({
                            elem: s,
                            matches: f
                        })
                    }
            d.length > m && w.push({
                elem: this,
                matches: d.slice(m)
            });
            for (r = 0; r < w.length && !n.isPropagationStopped(); r++) {
                a = w[r], n.currentTarget = a.elem;
                for (i = 0; i < a.matches.length && !n.isImmediatePropagationStopped(); i++) {
                    c = a.matches[i];
                    if (y || !n.namespace && !c.namespace || n.namespace_re && n.namespace_re.test(c.namespace)) n.data = c.data, n.handleObj = c, o = ((v.event.special[c.origType] || {}).handle || c.handler).apply(a.elem, g), o !== t && (n.result = o, o === !1 && (n.preventDefault(), n.stopPropagation()))
                }
            }
            return b.postDispatch && b.postDispatch.call(this, n), n.result
        },
        props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function (e, t) {
                return e.which == null && (e.which = t.charCode != null ? t.charCode : t.keyCode), e
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function (e, n) {
                var r, s, o, u = n.button,
                    a = n.fromElement;
                return e.pageX == null && n.clientX != null && (r = e.target.ownerDocument || i, s = r.documentElement, o = r.body, e.pageX = n.clientX + (s && s.scrollLeft || o && o.scrollLeft || 0) - (s && s.clientLeft || o && o.clientLeft || 0), e.pageY = n.clientY + (s && s.scrollTop || o && o.scrollTop || 0) - (s && s.clientTop || o && o.clientTop || 0)), !e.relatedTarget && a && (e.relatedTarget = a === e.target ? n.toElement : a), !e.which && u !== t && (e.which = u & 1 ? 1 : u & 2 ? 3 : u & 4 ? 2 : 0), e
            }
        },
        fix: function (e) {
            if (e[v.expando]) return e;
            var t, n, r = e,
                s = v.event.fixHooks[e.type] || {},
                o = s.props ? this.props.concat(s.props) : this.props;
            e = v.Event(r);
            for (t = o.length; t;) n = o[--t], e[n] = r[n];
            return e.target || (e.target = r.srcElement || i), e.target.nodeType === 3 && (e.target = e.target.parentNode), e.metaKey = !!e.metaKey, s.filter ? s.filter(e, r) : e
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                delegateType: "focusin"
            },
            blur: {
                delegateType: "focusout"
            },
            beforeunload: {
                setup: function (e, t, n) {
                    v.isWindow(this) && (this.onbeforeunload = n)
                },
                teardown: function (e, t) {
                    this.onbeforeunload === t && (this.onbeforeunload = null)
                }
            }
        },
        simulate: function (e, t, n, r) {
            var i = v.extend(new v.Event, n, {
                type: e,
                isSimulated: !0,
                originalEvent: {}
            });
            r ? v.event.trigger(i, null, t) : v.event.dispatch.call(t, i), i.isDefaultPrevented() && n.preventDefault()
        }
    }, v.event.handle = v.event.dispatch, v.removeEvent = i.removeEventListener ? function (e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n, !1)
    } : function (e, t, n) {
        var r = "on" + t;
        e.detachEvent && (typeof e[r] == "undefined" && (e[r] = null), e.detachEvent(r, n))
    }, v.Event = function (e, t) {
        if (!(this instanceof v.Event)) return new v.Event(e, t);
        e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || e.returnValue === !1 || e.getPreventDefault && e.getPreventDefault() ? tt : et) : this.type = e, t && v.extend(this, t), this.timeStamp = e && e.timeStamp || v.now(), this[v.expando] = !0
    }, v.Event.prototype = {
        preventDefault: function () {
            this.isDefaultPrevented = tt;
            var e = this.originalEvent;
            if (!e) return;
            e.preventDefault ? e.preventDefault() : e.returnValue = !1
        },
        stopPropagation: function () {
            this.isPropagationStopped = tt;
            var e = this.originalEvent;
            if (!e) return;
            e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0
        },
        stopImmediatePropagation: function () {
            this.isImmediatePropagationStopped = tt, this.stopPropagation()
        },
        isDefaultPrevented: et,
        isPropagationStopped: et,
        isImmediatePropagationStopped: et
    }, v.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout"
    }, function (e, t) {
        v.event.special[e] = {
            delegateType: t,
            bindType: t,
            handle: function (e) {
                var n, r = this,
                    i = e.relatedTarget,
                    s = e.handleObj,
                    o = s.selector;
                if (!i || i !== r && !v.contains(r, i)) e.type = s.origType, n = s.handler.apply(this, arguments), e.type = t;
                return n
            }
        }
    }), v.support.submitBubbles || (v.event.special.submit = {
        setup: function () {
            if (v.nodeName(this, "form")) return !1;
            v.event.add(this, "click._submit keypress._submit", function (e) {
                var n = e.target,
                    r = v.nodeName(n, "input") || v.nodeName(n, "button") ? n.form : t;
                r && !v._data(r, "_submit_attached") && (v.event.add(r, "submit._submit", function (e) {
                    e._submit_bubble = !0
                }), v._data(r, "_submit_attached", !0))
            })
        },
        postDispatch: function (e) {
            e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && v.event.simulate("submit", this.parentNode, e, !0))
        },
        teardown: function () {
            if (v.nodeName(this, "form")) return !1;
            v.event.remove(this, "._submit")
        }
    }), v.support.changeBubbles || (v.event.special.change = {
        setup: function () {
            if ($.test(this.nodeName)) {
                if (this.type === "checkbox" || this.type === "radio") v.event.add(this, "propertychange._change", function (e) {
                    e.originalEvent.propertyName === "checked" && (this._just_changed = !0)
                }), v.event.add(this, "click._change", function (e) {
                    this._just_changed && !e.isTrigger && (this._just_changed = !1), v.event.simulate("change", this, e, !0)
                });
                return !1
            }
            v.event.add(this, "beforeactivate._change", function (e) {
                var t = e.target;
                $.test(t.nodeName) && !v._data(t, "_change_attached") && (v.event.add(t, "change._change", function (e) {
                    this.parentNode && !e.isSimulated && !e.isTrigger && v.event.simulate("change", this.parentNode, e, !0)
                }), v._data(t, "_change_attached", !0))
            })
        },
        handle: function (e) {
            var t = e.target;
            if (this !== t || e.isSimulated || e.isTrigger || t.type !== "radio" && t.type !== "checkbox") return e.handleObj.handler.apply(this, arguments)
        },
        teardown: function () {
            return v.event.remove(this, "._change"), !$.test(this.nodeName)
        }
    }), v.support.focusinBubbles || v.each({
        focus: "focusin",
        blur: "focusout"
    }, function (e, t) {
        var n = 0,
            r = function (e) {
                v.event.simulate(t, e.target, v.event.fix(e), !0)
            };
        v.event.special[t] = {
            setup: function () {
                n++ === 0 && i.addEventListener(e, r, !0)
            },
            teardown: function () {
                --n === 0 && i.removeEventListener(e, r, !0)
            }
        }
    }), v.fn.extend({
        on: function (e, n, r, i, s) {
            var o, u;
            if (typeof e == "object") {
                typeof n != "string" && (r = r || n, n = t);
                for (u in e) this.on(u, n, r, e[u], s);
                return this
            }
            r == null && i == null ? (i = n, r = n = t) : i == null && (typeof n == "string" ? (i = r, r = t) : (i = r, r = n, n = t));
            if (i === !1) i = et;
            else if (!i) return this;
            return s === 1 && (o = i, i = function (e) {
                return v().off(e), o.apply(this, arguments)
            }, i.guid = o.guid || (o.guid = v.guid++)), this.each(function () {
                v.event.add(this, e, i, r, n)
            })
        },
        one: function (e, t, n, r) {
            return this.on(e, t, n, r, 1)
        },
        off: function (e, n, r) {
            var i, s;
            if (e && e.preventDefault && e.handleObj) return i = e.handleObj, v(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this;
            if (typeof e == "object") {
                for (s in e) this.off(s, n, e[s]);
                return this
            }
            if (n === !1 || typeof n == "function") r = n, n = t;
            return r === !1 && (r = et), this.each(function () {
                v.event.remove(this, e, r, n)
            })
        },
        bind: function (e, t, n) {
            return this.on(e, null, t, n)
        },
        unbind: function (e, t) {
            return this.off(e, null, t)
        },
        live: function (e, t, n) {
            return v(this.context).on(e, this.selector, t, n), this
        },
        die: function (e, t) {
            return v(this.context).off(e, this.selector || "**", t), this
        },
        delegate: function (e, t, n, r) {
            return this.on(t, e, n, r)
        },
        undelegate: function (e, t, n) {
            return arguments.length === 1 ? this.off(e, "**") : this.off(t, e || "**", n)
        },
        trigger: function (e, t) {
            return this.each(function () {
                v.event.trigger(e, t, this)
            })
        },
        triggerHandler: function (e, t) {
            if (this[0]) return v.event.trigger(e, t, this[0], !0)
        },
        toggle: function (e) {
            var t = arguments,
                n = e.guid || v.guid++,
                r = 0,
                i = function (n) {
                    var i = (v._data(this, "lastToggle" + e.guid) || 0) % r;
                    return v._data(this, "lastToggle" + e.guid, i + 1), n.preventDefault(), t[i].apply(this, arguments) || !1
                };
            i.guid = n;
            while (r < t.length) t[r++].guid = n;
            return this.click(i)
        },
        hover: function (e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }
    }), v.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (e, t) {
        v.fn[t] = function (e, n) {
            return n == null && (n = e, e = null), arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }, Q.test(t) && (v.event.fixHooks[t] = v.event.keyHooks), G.test(t) && (v.event.fixHooks[t] = v.event.mouseHooks)
    }),
        function (e, t) {
            function nt(e, t, n, r) {
                n = n || [], t = t || g;
                var i, s, a, f, l = t.nodeType;
                if (!e || typeof e != "string") return n;
                if (l !== 1 && l !== 9) return [];
                a = o(t);
                if (!a && !r)
                    if (i = R.exec(e))
                        if (f = i[1]) {
                            if (l === 9) {
                                s = t.getElementById(f);
                                if (!s || !s.parentNode) return n;
                                if (s.id === f) return n.push(s), n
                            } else if (t.ownerDocument && (s = t.ownerDocument.getElementById(f)) && u(t, s) && s.id === f) return n.push(s), n
                        } else {
                            if (i[2]) return S.apply(n, x.call(t.getElementsByTagName(e), 0)), n;
                            if ((f = i[3]) && Z && t.getElementsByClassName) return S.apply(n, x.call(t.getElementsByClassName(f), 0)), n
                        }
                return vt(e.replace(j, "$1"), t, n, r, a)
            }

            function rt(e) {
                return function (t) {
                    var n = t.nodeName.toLowerCase();
                    return n === "input" && t.type === e
                }
            }

            function it(e) {
                return function (t) {
                    var n = t.nodeName.toLowerCase();
                    return (n === "input" || n === "button") && t.type === e
                }
            }

            function st(e) {
                return N(function (t) {
                    return t = +t, N(function (n, r) {
                        var i, s = e([], n.length, t),
                            o = s.length;
                        while (o--) n[i = s[o]] && (n[i] = !(r[i] = n[i]))
                    })
                })
            }

            function ot(e, t, n) {
                if (e === t) return n;
                var r = e.nextSibling;
                while (r) {
                    if (r === t) return -1;
                    r = r.nextSibling
                }
                return 1
            }

            function ut(e, t) {
                var n, r, s, o, u, a, f, l = L[d][e + " "];
                if (l) return t ? 0 : l.slice(0);
                u = e, a = [], f = i.preFilter;
                while (u) {
                    if (!n || (r = F.exec(u))) r && (u = u.slice(r[0].length) || u), a.push(s = []);
                    n = !1;
                    if (r = I.exec(u)) s.push(n = new m(r.shift())), u = u.slice(n.length), n.type = r[0].replace(j, " ");
                    for (o in i.filter) (r = J[o].exec(u)) && (!f[o] || (r = f[o](r))) && (s.push(n = new m(r.shift())), u = u.slice(n.length), n.type = o, n.matches = r);
                    if (!n) break
                }
                return t ? u.length : u ? nt.error(e) : L(e, a).slice(0)
            }

            function at(e, t, r) {
                var i = t.dir,
                    s = r && t.dir === "parentNode",
                    o = w++;
                return t.first ? function (t, n, r) {
                    while (t = t[i])
                        if (s || t.nodeType === 1) return e(t, n, r)
                } : function (t, r, u) {
                    if (!u) {
                        var a, f = b + " " + o + " ",
                            l = f + n;
                        while (t = t[i])
                            if (s || t.nodeType === 1) {
                                if ((a = t[d]) === l) return t.sizset;
                                if (typeof a == "string" && a.indexOf(f) === 0) {
                                    if (t.sizset) return t
                                } else {
                                    t[d] = l;
                                    if (e(t, r, u)) return t.sizset = !0, t;
                                    t.sizset = !1
                                }
                            }
                    } else
                        while (t = t[i])
                            if (s || t.nodeType === 1)
                                if (e(t, r, u)) return t
                }
            }

            function ft(e) {
                return e.length > 1 ? function (t, n, r) {
                    var i = e.length;
                    while (i--)
                        if (!e[i](t, n, r)) return !1;
                    return !0
                } : e[0]
            }

            function lt(e, t, n, r, i) {
                var s, o = [],
                    u = 0,
                    a = e.length,
                    f = t != null;
                for (; u < a; u++)
                    if (s = e[u])
                        if (!n || n(s, r, i)) o.push(s), f && t.push(u);
                return o
            }

            function ct(e, t, n, r, i, s) {
                return r && !r[d] && (r = ct(r)), i && !i[d] && (i = ct(i, s)), N(function (s, o, u, a) {
                    var f, l, c, h = [],
                        p = [],
                        d = o.length,
                        v = s || dt(t || "*", u.nodeType ? [u] : u, []),
                        m = e && (s || !t) ? lt(v, h, e, u, a) : v,
                        g = n ? i || (s ? e : d || r) ? [] : o : m;
                    n && n(m, g, u, a);
                    if (r) {
                        f = lt(g, p), r(f, [], u, a), l = f.length;
                        while (l--)
                            if (c = f[l]) g[p[l]] = !(m[p[l]] = c)
                    }
                    if (s) {
                        if (i || e) {
                            if (i) {
                                f = [], l = g.length;
                                while (l--) (c = g[l]) && f.push(m[l] = c);
                                i(null, g = [], f, a)
                            }
                            l = g.length;
                            while (l--) (c = g[l]) && (f = i ? T.call(s, c) : h[l]) > -1 && (s[f] = !(o[f] = c))
                        }
                    } else g = lt(g === o ? g.splice(d, g.length) : g), i ? i(null, o, g, a) : S.apply(o, g)
                })
            }

            function ht(e) {
                var t, n, r, s = e.length,
                    o = i.relative[e[0].type],
                    u = o || i.relative[" "],
                    a = o ? 1 : 0,
                    f = at(function (e) {
                        return e === t
                    }, u, !0),
                    l = at(function (e) {
                        return T.call(t, e) > -1
                    }, u, !0),
                    h = [function (e, n, r) {
                        return !o && (r || n !== c) || ((t = n).nodeType ? f(e, n, r) : l(e, n, r))
                    }];
                for (; a < s; a++)
                    if (n = i.relative[e[a].type]) h = [at(ft(h), n)];
                    else {
                        n = i.filter[e[a].type].apply(null, e[a].matches);
                        if (n[d]) {
                            r = ++a;
                            for (; r < s; r++)
                                if (i.relative[e[r].type]) break;
                            return ct(a > 1 && ft(h), a > 1 && e.slice(0, a - 1).join("").replace(j, "$1"), n, a < r && ht(e.slice(a, r)), r < s && ht(e = e.slice(r)), r < s && e.join(""))
                        }
                        h.push(n)
                    }
                return ft(h)
            }

            function pt(e, t) {
                var r = t.length > 0,
                    s = e.length > 0,
                    o = function (u, a, f, l, h) {
                        var p, d, v, m = [],
                            y = 0,
                            w = "0",
                            x = u && [],
                            T = h != null,
                            N = c,
                            C = u || s && i.find.TAG("*", h && a.parentNode || a),
                            k = b += N == null ? 1 : Math.E;
                        T && (c = a !== g && a, n = o.el);
                        for (;
                            (p = C[w]) != null; w++) {
                            if (s && p) {
                                for (d = 0; v = e[d]; d++)
                                    if (v(p, a, f)) {
                                        l.push(p);
                                        break
                                    }
                                T && (b = k, n = ++o.el)
                            }
                            r && ((p = !v && p) && y-- , u && x.push(p))
                        }
                        y += w;
                        if (r && w !== y) {
                            for (d = 0; v = t[d]; d++) v(x, m, a, f);
                            if (u) {
                                if (y > 0)
                                    while (w--) !x[w] && !m[w] && (m[w] = E.call(l));
                                m = lt(m)
                            }
                            S.apply(l, m), T && !u && m.length > 0 && y + t.length > 1 && nt.uniqueSort(l)
                        }
                        return T && (b = k, c = N), x
                    };
                return o.el = 0, r ? N(o) : o
            }

            function dt(e, t, n) {
                var r = 0,
                    i = t.length;
                for (; r < i; r++) nt(e, t[r], n);
                return n
            }

            function vt(e, t, n, r, s) {
                var o, u, f, l, c, h = ut(e),
                    p = h.length;
                if (!r && h.length === 1) {
                    u = h[0] = h[0].slice(0);
                    if (u.length > 2 && (f = u[0]).type === "ID" && t.nodeType === 9 && !s && i.relative[u[1].type]) {
                        t = i.find.ID(f.matches[0].replace($, ""), t, s)[0];
                        if (!t) return n;
                        e = e.slice(u.shift().length)
                    }
                    for (o = J.POS.test(e) ? -1 : u.length - 1; o >= 0; o--) {
                        f = u[o];
                        if (i.relative[l = f.type]) break;
                        if (c = i.find[l])
                            if (r = c(f.matches[0].replace($, ""), z.test(u[0].type) && t.parentNode || t, s)) {
                                u.splice(o, 1), e = r.length && u.join("");
                                if (!e) return S.apply(n, x.call(r, 0)), n;
                                break
                            }
                    }
                }
                return a(e, h)(r, t, s, n, z.test(e)), n
            }

            function mt() { }
            var n, r, i, s, o, u, a, f, l, c, h = !0,
                p = "undefined",
                d = ("sizcache" + Math.random()).replace(".", ""),
                m = String,
                g = e.document,
                y = g.documentElement,
                b = 0,
                w = 0,
                E = [].pop,
                S = [].push,
                x = [].slice,
                T = [].indexOf || function (e) {
                    var t = 0,
                        n = this.length;
                    for (; t < n; t++)
                        if (this[t] === e) return t;
                    return -1
                },
                N = function (e, t) {
                    return e[d] = t == null || t, e
                },
                C = function () {
                    var e = {},
                        t = [];
                    return N(function (n, r) {
                        return t.push(n) > i.cacheLength && delete e[t.shift()], e[n + " "] = r
                    }, e)
                },
                k = C(),
                L = C(),
                A = C(),
                O = "[\\x20\\t\\r\\n\\f]",
                M = "(?:\\\\.|[-\\w]|[^\\x00-\\xa0])+",
                _ = M.replace("w", "w#"),
                D = "([*^$|!~]?=)",
                P = "\\[" + O + "*(" + M + ")" + O + "*(?:" + D + O + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + _ + ")|)|)" + O + "*\\]",
                H = ":(" + M + ")(?:\\((?:(['\"])((?:\\\\.|[^\\\\])*?)\\2|([^()[\\]]*|(?:(?:" + P + ")|[^:]|\\\\.)*|.*))\\)|)",
                B = ":(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + O + "*((?:-\\d)?\\d*)" + O + "*\\)|)(?=[^-]|$)",
                j = new RegExp("^" + O + "+|((?:^|[^\\\\])(?:\\\\.)*)" + O + "+$", "g"),
                F = new RegExp("^" + O + "*," + O + "*"),
                I = new RegExp("^" + O + "*([\\x20\\t\\r\\n\\f>+~])" + O + "*"),
                q = new RegExp(H),
                R = /^(?:#([\w\-]+)|(\w+)|\.([\w\-]+))$/,
                U = /^:not/,
                z = /[\x20\t\r\n\f]*[+~]/,
                W = /:not\($/,
                X = /h\d/i,
                V = /input|select|textarea|button/i,
                $ = /\\(?!\\)/g,
                J = {
                    ID: new RegExp("^#(" + M + ")"),
                    CLASS: new RegExp("^\\.(" + M + ")"),
                    NAME: new RegExp("^\\[name=['\"]?(" + M + ")['\"]?\\]"),
                    TAG: new RegExp("^(" + M.replace("w", "w*") + ")"),
                    ATTR: new RegExp("^" + P),
                    PSEUDO: new RegExp("^" + H),
                    POS: new RegExp(B, "i"),
                    CHILD: new RegExp("^:(only|nth|first|last)-child(?:\\(" + O + "*(even|odd|(([+-]|)(\\d*)n|)" + O + "*(?:([+-]|)" + O + "*(\\d+)|))" + O + "*\\)|)", "i"),
                    needsContext: new RegExp("^" + O + "*[>+~]|" + B, "i")
                },
                K = function (e) {
                    var t = g.createElement("div");
                    try {
                        return e(t)
                    } catch (n) {
                        return !1
                    } finally {
                        t = null
                    }
                },
                Q = K(function (e) {
                    return e.appendChild(g.createComment("")), !e.getElementsByTagName("*").length
                }),
                G = K(function (e) {
                    return e.innerHTML = "<a href='#'></a>", e.firstChild && typeof e.firstChild.getAttribute !== p && e.firstChild.getAttribute("href") === "#"
                }),
                Y = K(function (e) {
                    e.innerHTML = "<select></select>";
                    var t = typeof e.lastChild.getAttribute("multiple");
                    return t !== "boolean" && t !== "string"
                }),
                Z = K(function (e) {
                    return e.innerHTML = "<div class='hidden e'></div><div class='hidden'></div>", !e.getElementsByClassName || !e.getElementsByClassName("e").length ? !1 : (e.lastChild.className = "e", e.getElementsByClassName("e").length === 2)
                }),
                et = K(function (e) {
                    e.id = d + 0, e.innerHTML = "<a name='" + d + "'></a><div name='" + d + "'></div>", y.insertBefore(e, y.firstChild);
                    var t = g.getElementsByName && g.getElementsByName(d).length === 2 + g.getElementsByName(d + 0).length;
                    return r = !g.getElementById(d), y.removeChild(e), t
                });
            try {
                x.call(y.childNodes, 0)[0].nodeType
            } catch (tt) {
                x = function (e) {
                    var t, n = [];
                    for (; t = this[e]; e++) n.push(t);
                    return n
                }
            }
            nt.matches = function (e, t) {
                return nt(e, null, null, t)
            }, nt.matchesSelector = function (e, t) {
                return nt(t, null, null, [e]).length > 0
            }, s = nt.getText = function (e) {
                var t, n = "",
                    r = 0,
                    i = e.nodeType;
                if (i) {
                    if (i === 1 || i === 9 || i === 11) {
                        if (typeof e.textContent == "string") return e.textContent;
                        for (e = e.firstChild; e; e = e.nextSibling) n += s(e)
                    } else if (i === 3 || i === 4) return e.nodeValue
                } else
                    for (; t = e[r]; r++) n += s(t);
                return n
            }, o = nt.isXML = function (e) {
                var t = e && (e.ownerDocument || e).documentElement;
                return t ? t.nodeName !== "HTML" : !1
            }, u = nt.contains = y.contains ? function (e, t) {
                var n = e.nodeType === 9 ? e.documentElement : e,
                    r = t && t.parentNode;
                return e === r || !!(r && r.nodeType === 1 && n.contains && n.contains(r))
            } : y.compareDocumentPosition ? function (e, t) {
                return t && !!(e.compareDocumentPosition(t) & 16)
            } : function (e, t) {
                while (t = t.parentNode)
                    if (t === e) return !0;
                return !1
            }, nt.attr = function (e, t) {
                var n, r = o(e);
                return r || (t = t.toLowerCase()), (n = i.attrHandle[t]) ? n(e) : r || Y ? e.getAttribute(t) : (n = e.getAttributeNode(t), n ? typeof e[t] == "boolean" ? e[t] ? t : null : n.specified ? n.value : null : null)
            }, i = nt.selectors = {
                cacheLength: 50,
                createPseudo: N,
                match: J,
                attrHandle: G ? {} : {
                    href: function (e) {
                        return e.getAttribute("href", 2)
                    },
                    type: function (e) {
                        return e.getAttribute("type")
                    }
                },
                find: {
                    ID: r ? function (e, t, n) {
                        if (typeof t.getElementById !== p && !n) {
                            var r = t.getElementById(e);
                            return r && r.parentNode ? [r] : []
                        }
                    } : function (e, n, r) {
                        if (typeof n.getElementById !== p && !r) {
                            var i = n.getElementById(e);
                            return i ? i.id === e || typeof i.getAttributeNode !== p && i.getAttributeNode("id").value === e ? [i] : t : []
                        }
                    },
                    TAG: Q ? function (e, t) {
                        if (typeof t.getElementsByTagName !== p) return t.getElementsByTagName(e)
                    } : function (e, t) {
                        var n = t.getElementsByTagName(e);
                        if (e === "*") {
                            var r, i = [],
                                s = 0;
                            for (; r = n[s]; s++) r.nodeType === 1 && i.push(r);
                            return i
                        }
                        return n
                    },
                    NAME: et && function (e, t) {
                        if (typeof t.getElementsByName !== p) return t.getElementsByName(name)
                    },
                    CLASS: Z && function (e, t, n) {
                        if (typeof t.getElementsByClassName !== p && !n) return t.getElementsByClassName(e)
                    }
                },
                relative: {
                    ">": {
                        dir: "parentNode",
                        first: !0
                    },
                    " ": {
                        dir: "parentNode"
                    },
                    "+": {
                        dir: "previousSibling",
                        first: !0
                    },
                    "~": {
                        dir: "previousSibling"
                    }
                },
                preFilter: {
                    ATTR: function (e) {
                        return e[1] = e[1].replace($, ""), e[3] = (e[4] || e[5] || "").replace($, ""), e[2] === "~=" && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                    },
                    CHILD: function (e) {
                        return e[1] = e[1].toLowerCase(), e[1] === "nth" ? (e[2] || nt.error(e[0]), e[3] = +(e[3] ? e[4] + (e[5] || 1) : 2 * (e[2] === "even" || e[2] === "odd")), e[4] = +(e[6] + e[7] || e[2] === "odd")) : e[2] && nt.error(e[0]), e
                    },
                    PSEUDO: function (e) {
                        var t, n;
                        if (J.CHILD.test(e[0])) return null;
                        if (e[3]) e[2] = e[3];
                        else if (t = e[4]) q.test(t) && (n = ut(t, !0)) && (n = t.indexOf(")", t.length - n) - t.length) && (t = t.slice(0, n), e[0] = e[0].slice(0, n)), e[2] = t;
                        return e.slice(0, 3)
                    }
                },
                filter: {
                    ID: r ? function (e) {
                        return e = e.replace($, ""),
                            function (t) {
                                return t.getAttribute("id") === e
                            }
                    } : function (e) {
                        return e = e.replace($, ""),
                            function (t) {
                                var n = typeof t.getAttributeNode !== p && t.getAttributeNode("id");
                                return n && n.value === e
                            }
                    },
                    TAG: function (e) {
                        return e === "*" ? function () {
                            return !0
                        } : (e = e.replace($, "").toLowerCase(), function (t) {
                            return t.nodeName && t.nodeName.toLowerCase() === e
                        })
                    },
                    CLASS: function (e) {
                        var t = k[d][e + " "];
                        return t || (t = new RegExp("(^|" + O + ")" + e + "(" + O + "|$)")) && k(e, function (e) {
                            return t.test(e.className || typeof e.getAttribute !== p && e.getAttribute("class") || "")
                        })
                    },
                    ATTR: function (e, t, n) {
                        return function (r, i) {
                            var s = nt.attr(r, e);
                            return s == null ? t === "!=" : t ? (s += "", t === "=" ? s === n : t === "!=" ? s !== n : t === "^=" ? n && s.indexOf(n) === 0 : t === "*=" ? n && s.indexOf(n) > -1 : t === "$=" ? n && s.substr(s.length - n.length) === n : t === "~=" ? (" " + s + " ").indexOf(n) > -1 : t === "|=" ? s === n || s.substr(0, n.length + 1) === n + "-" : !1) : !0
                        }
                    },
                    CHILD: function (e, t, n, r) {
                        return e === "nth" ? function (e) {
                            var t, i, s = e.parentNode;
                            if (n === 1 && r === 0) return !0;
                            if (s) {
                                i = 0;
                                for (t = s.firstChild; t; t = t.nextSibling)
                                    if (t.nodeType === 1) {
                                        i++;
                                        if (e === t) break
                                    }
                            }
                            return i -= r, i === n || i % n === 0 && i / n >= 0
                        } : function (t) {
                            var n = t;
                            switch (e) {
                                case "only":
                                case "first":
                                    while (n = n.previousSibling)
                                        if (n.nodeType === 1) return !1;
                                    if (e === "first") return !0;
                                    n = t;
                                case "last":
                                    while (n = n.nextSibling)
                                        if (n.nodeType === 1) return !1;
                                    return !0
                            }
                        }
                    },
                    PSEUDO: function (e, t) {
                        var n, r = i.pseudos[e] || i.setFilters[e.toLowerCase()] || nt.error("unsupported pseudo: " + e);
                        return r[d] ? r(t) : r.length > 1 ? (n = [e, e, "", t], i.setFilters.hasOwnProperty(e.toLowerCase()) ? N(function (e, n) {
                            var i, s = r(e, t),
                                o = s.length;
                            while (o--) i = T.call(e, s[o]), e[i] = !(n[i] = s[o])
                        }) : function (e) {
                            return r(e, 0, n)
                        }) : r
                    }
                },
                pseudos: {
                    not: N(function (e) {
                        var t = [],
                            n = [],
                            r = a(e.replace(j, "$1"));
                        return r[d] ? N(function (e, t, n, i) {
                            var s, o = r(e, null, i, []),
                                u = e.length;
                            while (u--)
                                if (s = o[u]) e[u] = !(t[u] = s)
                        }) : function (e, i, s) {
                            return t[0] = e, r(t, null, s, n), !n.pop()
                        }
                    }),
                    has: N(function (e) {
                        return function (t) {
                            return nt(e, t).length > 0
                        }
                    }),
                    contains: N(function (e) {
                        return function (t) {
                            return (t.textContent || t.innerText || s(t)).indexOf(e) > -1
                        }
                    }),
                    enabled: function (e) {
                        return e.disabled === !1
                    },
                    disabled: function (e) {
                        return e.disabled === !0
                    },
                    checked: function (e) {
                        var t = e.nodeName.toLowerCase();
                        return t === "input" && !!e.checked || t === "option" && !!e.selected
                    },
                    selected: function (e) {
                        return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                    },
                    parent: function (e) {
                        return !i.pseudos.empty(e)
                    },
                    empty: function (e) {
                        var t;
                        e = e.firstChild;
                        while (e) {
                            if (e.nodeName > "@" || (t = e.nodeType) === 3 || t === 4) return !1;
                            e = e.nextSibling
                        }
                        return !0
                    },
                    header: function (e) {
                        return X.test(e.nodeName)
                    },
                    text: function (e) {
                        var t, n;
                        return e.nodeName.toLowerCase() === "input" && (t = e.type) === "text" && ((n = e.getAttribute("type")) == null || n.toLowerCase() === t)
                    },
                    radio: rt("radio"),
                    checkbox: rt("checkbox"),
                    file: rt("file"),
                    password: rt("password"),
                    image: rt("image"),
                    submit: it("submit"),
                    reset: it("reset"),
                    button: function (e) {
                        var t = e.nodeName.toLowerCase();
                        return t === "input" && e.type === "button" || t === "button"
                    },
                    input: function (e) {
                        return V.test(e.nodeName)
                    },
                    focus: function (e) {
                        var t = e.ownerDocument;
                        return e === t.activeElement && (!t.hasFocus || t.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                    },
                    active: function (e) {
                        return e === e.ownerDocument.activeElement
                    },
                    first: st(function () {
                        return [0]
                    }),
                    last: st(function (e, t) {
                        return [t - 1]
                    }),
                    eq: st(function (e, t, n) {
                        return [n < 0 ? n + t : n]
                    }),
                    even: st(function (e, t) {
                        for (var n = 0; n < t; n += 2) e.push(n);
                        return e
                    }),
                    odd: st(function (e, t) {
                        for (var n = 1; n < t; n += 2) e.push(n);
                        return e
                    }),
                    lt: st(function (e, t, n) {
                        for (var r = n < 0 ? n + t : n; --r >= 0;) e.push(r);
                        return e
                    }),
                    gt: st(function (e, t, n) {
                        for (var r = n < 0 ? n + t : n; ++r < t;) e.push(r);
                        return e
                    })
                }
            }, f = y.compareDocumentPosition ? function (e, t) {
                return e === t ? (l = !0, 0) : (!e.compareDocumentPosition || !t.compareDocumentPosition ? e.compareDocumentPosition : e.compareDocumentPosition(t) & 4) ? -1 : 1
            } : function (e, t) {
                if (e === t) return l = !0, 0;
                if (e.sourceIndex && t.sourceIndex) return e.sourceIndex - t.sourceIndex;
                var n, r, i = [],
                    s = [],
                    o = e.parentNode,
                    u = t.parentNode,
                    a = o;
                if (o === u) return ot(e, t);
                if (!o) return -1;
                if (!u) return 1;
                while (a) i.unshift(a), a = a.parentNode;
                a = u;
                while (a) s.unshift(a), a = a.parentNode;
                n = i.length, r = s.length;
                for (var f = 0; f < n && f < r; f++)
                    if (i[f] !== s[f]) return ot(i[f], s[f]);
                return f === n ? ot(e, s[f], -1) : ot(i[f], t, 1)
            }, [0, 0].sort(f), h = !l, nt.uniqueSort = function (e) {
                var t, n = [],
                    r = 1,
                    i = 0;
                l = h, e.sort(f);
                if (l) {
                    for (; t = e[r]; r++) t === e[r - 1] && (i = n.push(r));
                    while (i--) e.splice(n[i], 1)
                }
                return e
            }, nt.error = function (e) {
                throw new Error("Syntax error, unrecognized expression: " + e)
            }, a = nt.compile = function (e, t) {
                var n, r = [],
                    i = [],
                    s = A[d][e + " "];
                if (!s) {
                    t || (t = ut(e)), n = t.length;
                    while (n--) s = ht(t[n]), s[d] ? r.push(s) : i.push(s);
                    s = A(e, pt(i, r))
                }
                return s
            }, g.querySelectorAll && function () {
                var e, t = vt,
                    n = /'|\\/g,
                    r = /\=[\x20\t\r\n\f]*([^'"\]]*)[\x20\t\r\n\f]*\]/g,
                    i = [":focus"],
                    s = [":active"],
                    u = y.matchesSelector || y.mozMatchesSelector || y.webkitMatchesSelector || y.oMatchesSelector || y.msMatchesSelector;
                K(function (e) {
                    e.innerHTML = "<select><option selected=''></option></select>", e.querySelectorAll("[selected]").length || i.push("\\[" + O + "*(?:checked|disabled|ismap|multiple|readonly|selected|value)"), e.querySelectorAll(":checked").length || i.push(":checked")
                }), K(function (e) {
                    e.innerHTML = "<p test=''></p>", e.querySelectorAll("[test^='']").length && i.push("[*^$]=" + O + "*(?:\"\"|'')"), e.innerHTML = "<input type='hidden'/>", e.querySelectorAll(":enabled").length || i.push(":enabled", ":disabled")
                }), i = new RegExp(i.join("|")), vt = function (e, r, s, o, u) {
                    if (!o && !u && !i.test(e)) {
                        var a, f, l = !0,
                            c = d,
                            h = r,
                            p = r.nodeType === 9 && e;
                        if (r.nodeType === 1 && r.nodeName.toLowerCase() !== "object") {
                            a = ut(e), (l = r.getAttribute("id")) ? c = l.replace(n, "\\$&") : r.setAttribute("id", c), c = "[id='" + c + "'] ", f = a.length;
                            while (f--) a[f] = c + a[f].join("");
                            h = z.test(e) && r.parentNode || r, p = a.join(",")
                        }
                        if (p) try {
                            return S.apply(s, x.call(h.querySelectorAll(p), 0)), s
                        } catch (v) { } finally {
                                l || r.removeAttribute("id")
                            }
                    }
                    return t(e, r, s, o, u)
                }, u && (K(function (t) {
                    e = u.call(t, "div");
                    try {
                        u.call(t, "[test!='']:sizzle"), s.push("!=", H)
                    } catch (n) { }
                }), s = new RegExp(s.join("|")), nt.matchesSelector = function (t, n) {
                    n = n.replace(r, "='$1']");
                    if (!o(t) && !s.test(n) && !i.test(n)) try {
                        var a = u.call(t, n);
                        if (a || e || t.document && t.document.nodeType !== 11) return a
                    } catch (f) { }
                    return nt(n, null, null, [t]).length > 0
                })
            }(), i.pseudos.nth = i.pseudos.eq, i.filters = mt.prototype = i.pseudos, i.setFilters = new mt, nt.attr = v.attr, v.find = nt, v.expr = nt.selectors, v.expr[":"] = v.expr.pseudos, v.unique = nt.uniqueSort, v.text = nt.getText, v.isXMLDoc = nt.isXML, v.contains = nt.contains
        }(e);
    var nt = /Until$/,
        rt = /^(?:parents|prev(?:Until|All))/,
        it = /^.[^:#\[\.,]*$/,
        st = v.expr.match.needsContext,
        ot = {
            children: !0,
            contents: !0,
            next: !0,
            prev: !0
        };
    v.fn.extend({
        find: function (e) {
            var t, n, r, i, s, o, u = this;
            if (typeof e != "string") return v(e).filter(function () {
                for (t = 0, n = u.length; t < n; t++)
                    if (v.contains(u[t], this)) return !0
            });
            o = this.pushStack("", "find", e);
            for (t = 0, n = this.length; t < n; t++) {
                r = o.length, v.find(e, this[t], o);
                if (t > 0)
                    for (i = r; i < o.length; i++)
                        for (s = 0; s < r; s++)
                            if (o[s] === o[i]) {
                                o.splice(i--, 1);
                                break
                            }
            }
            return o
        },
        has: function (e) {
            var t, n = v(e, this),
                r = n.length;
            return this.filter(function () {
                for (t = 0; t < r; t++)
                    if (v.contains(this, n[t])) return !0
            })
        },
        not: function (e) {
            return this.pushStack(ft(this, e, !1), "not", e)
        },
        filter: function (e) {
            return this.pushStack(ft(this, e, !0), "filter", e)
        },
        is: function (e) {
            return !!e && (typeof e == "string" ? st.test(e) ? v(e, this.context).index(this[0]) >= 0 : v.filter(e, this).length > 0 : this.filter(e).length > 0)
        },
        closest: function (e, t) {
            var n, r = 0,
                i = this.length,
                s = [],
                o = st.test(e) || typeof e != "string" ? v(e, t || this.context) : 0;
            for (; r < i; r++) {
                n = this[r];
                while (n && n.ownerDocument && n !== t && n.nodeType !== 11) {
                    if (o ? o.index(n) > -1 : v.find.matchesSelector(n, e)) {
                        s.push(n);
                        break
                    }
                    n = n.parentNode
                }
            }
            return s = s.length > 1 ? v.unique(s) : s, this.pushStack(s, "closest", e)
        },
        index: function (e) {
            return e ? typeof e == "string" ? v.inArray(this[0], v(e)) : v.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.prevAll().length : -1
        },
        add: function (e, t) {
            var n = typeof e == "string" ? v(e, t) : v.makeArray(e && e.nodeType ? [e] : e),
                r = v.merge(this.get(), n);
            return this.pushStack(ut(n[0]) || ut(r[0]) ? r : v.unique(r))
        },
        addBack: function (e) {
            return this.add(e == null ? this.prevObject : this.prevObject.filter(e))
        }
    }), v.fn.andSelf = v.fn.addBack, v.each({
        parent: function (e) {
            var t = e.parentNode;
            return t && t.nodeType !== 11 ? t : null
        },
        parents: function (e) {
            return v.dir(e, "parentNode")
        },
        parentsUntil: function (e, t, n) {
            return v.dir(e, "parentNode", n)
        },
        next: function (e) {
            return at(e, "nextSibling")
        },
        prev: function (e) {
            return at(e, "previousSibling")
        },
        nextAll: function (e) {
            return v.dir(e, "nextSibling")
        },
        prevAll: function (e) {
            return v.dir(e, "previousSibling")
        },
        nextUntil: function (e, t, n) {
            return v.dir(e, "nextSibling", n)
        },
        prevUntil: function (e, t, n) {
            return v.dir(e, "previousSibling", n)
        },
        siblings: function (e) {
            return v.sibling((e.parentNode || {}).firstChild, e)
        },
        children: function (e) {
            return v.sibling(e.firstChild)
        },
        contents: function (e) {
            return v.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : v.merge([], e.childNodes)
        }
    }, function (e, t) {
        v.fn[e] = function (n, r) {
            var i = v.map(this, t, n);
            return nt.test(e) || (r = n), r && typeof r == "string" && (i = v.filter(r, i)), i = this.length > 1 && !ot[e] ? v.unique(i) : i, this.length > 1 && rt.test(e) && (i = i.reverse()), this.pushStack(i, e, l.call(arguments).join(","))
        }
    }), v.extend({
        filter: function (e, t, n) {
            return n && (e = ":not(" + e + ")"), t.length === 1 ? v.find.matchesSelector(t[0], e) ? [t[0]] : [] : v.find.matches(e, t)
        },
        dir: function (e, n, r) {
            var i = [],
                s = e[n];
            while (s && s.nodeType !== 9 && (r === t || s.nodeType !== 1 || !v(s).is(r))) s.nodeType === 1 && i.push(s), s = s[n];
            return i
        },
        sibling: function (e, t) {
            var n = [];
            for (; e; e = e.nextSibling) e.nodeType === 1 && e !== t && n.push(e);
            return n
        }
    });
    var ct = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        ht = / jQuery\d+="(?:null|\d+)"/g,
        pt = /^\s+/,
        dt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        vt = /<([\w:]+)/,
        mt = /<tbody/i,
        gt = /<|&#?\w+;/,
        yt = /<(?:script|style|link)/i,
        bt = /<(?:script|object|embed|option|style)/i,
        wt = new RegExp("<(?:" + ct + ")[\\s/>]", "i"),
        Et = /^(?:checkbox|radio)$/,
        St = /checked\s*(?:[^=]|=\s*.checked.)/i,
        xt = /\/(java|ecma)script/i,
        Tt = /^\s*<!(?:\[CDATA\[|\-\-)|[\]\-]{2}>\s*$/g,
        Nt = {
            option: [1, "<select multiple='multiple'>", "</select>"],
            legend: [1, "<fieldset>", "</fieldset>"],
            thead: [1, "<table>", "</table>"],
            tr: [2, "<table><tbody>", "</tbody></table>"],
            td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
            col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
            area: [1, "<map>", "</map>"],
            _default: [0, "", ""]
        },
        Ct = lt(i),
        kt = Ct.appendChild(i.createElement("div"));
    Nt.optgroup = Nt.option, Nt.tbody = Nt.tfoot = Nt.colgroup = Nt.caption = Nt.thead, Nt.th = Nt.td, v.support.htmlSerialize || (Nt._default = [1, "X<div>", "</div>"]), v.fn.extend({
        text: function (e) {
            return v.access(this, function (e) {
                return e === t ? v.text(this) : this.empty().append((this[0] && this[0].ownerDocument || i).createTextNode(e))
            }, null, e, arguments.length)
        },
        wrapAll: function (e) {
            if (v.isFunction(e)) return this.each(function (t) {
                v(this).wrapAll(e.call(this, t))
            });
            if (this[0]) {
                var t = v(e, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
                    var e = this;
                    while (e.firstChild && e.firstChild.nodeType === 1) e = e.firstChild;
                    return e
                }).append(this)
            }
            return this
        },
        wrapInner: function (e) {
            return v.isFunction(e) ? this.each(function (t) {
                v(this).wrapInner(e.call(this, t))
            }) : this.each(function () {
                var t = v(this),
                    n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        },
        wrap: function (e) {
            var t = v.isFunction(e);
            return this.each(function (n) {
                v(this).wrapAll(t ? e.call(this, n) : e)
            })
        },
        unwrap: function () {
            return this.parent().each(function () {
                v.nodeName(this, "body") || v(this).replaceWith(this.childNodes)
            }).end()
        },
        append: function () {
            return this.domManip(arguments, !0, function (e) {
                (this.nodeType === 1 || this.nodeType === 11) && this.appendChild(e)
            })
        },
        prepend: function () {
            return this.domManip(arguments, !0, function (e) {
                (this.nodeType === 1 || this.nodeType === 11) && this.insertBefore(e, this.firstChild)
            })
        },
        before: function () {
            if (!ut(this[0])) return this.domManip(arguments, !1, function (e) {
                this.parentNode.insertBefore(e, this)
            });
            if (arguments.length) {
                var e = v.clean(arguments);
                return this.pushStack(v.merge(e, this), "before", this.selector)
            }
        },
        after: function () {
            if (!ut(this[0])) return this.domManip(arguments, !1, function (e) {
                this.parentNode.insertBefore(e, this.nextSibling)
            });
            if (arguments.length) {
                var e = v.clean(arguments);
                return this.pushStack(v.merge(this, e), "after", this.selector)
            }
        },
        remove: function (e, t) {
            var n, r = 0;
            for (;
                (n = this[r]) != null; r++)
                if (!e || v.filter(e, [n]).length) !t && n.nodeType === 1 && (v.cleanData(n.getElementsByTagName("*")), v.cleanData([n])), n.parentNode && n.parentNode.removeChild(n);
            return this
        },
        empty: function () {
            var e, t = 0;
            for (;
                (e = this[t]) != null; t++) {
                e.nodeType === 1 && v.cleanData(e.getElementsByTagName("*"));
                while (e.firstChild) e.removeChild(e.firstChild)
            }
            return this
        },
        clone: function (e, t) {
            return e = e == null ? !1 : e, t = t == null ? e : t, this.map(function () {
                return v.clone(this, e, t)
            })
        },
        html: function (e) {
            return v.access(this, function (e) {
                var n = this[0] || {},
                    r = 0,
                    i = this.length;
                if (e === t) return n.nodeType === 1 ? n.innerHTML.replace(ht, "") : t;
                if (typeof e == "string" && !yt.test(e) && (v.support.htmlSerialize || !wt.test(e)) && (v.support.leadingWhitespace || !pt.test(e)) && !Nt[(vt.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = e.replace(dt, "<$1></$2>");
                    try {
                        for (; r < i; r++) n = this[r] || {}, n.nodeType === 1 && (v.cleanData(n.getElementsByTagName("*")), n.innerHTML = e);
                        n = 0
                    } catch (s) { }
                }
                n && this.empty().append(e)
            }, null, e, arguments.length)
        },
        replaceWith: function (e) {
            return ut(this[0]) ? this.length ? this.pushStack(v(v.isFunction(e) ? e() : e), "replaceWith", e) : this : v.isFunction(e) ? this.each(function (t) {
                var n = v(this),
                    r = n.html();
                n.replaceWith(e.call(this, t, r))
            }) : (typeof e != "string" && (e = v(e).detach()), this.each(function () {
                var t = this.nextSibling,
                    n = this.parentNode;
                v(this).remove(), t ? v(t).before(e) : v(n).append(e)
            }))
        },
        detach: function (e) {
            return this.remove(e, !0)
        },
        domManip: function (e, n, r) {
            e = [].concat.apply([], e);
            var i, s, o, u, a = 0,
                f = e[0],
                l = [],
                c = this.length;
            if (!v.support.checkClone && c > 1 && typeof f == "string" && St.test(f)) return this.each(function () {
                v(this).domManip(e, n, r)
            });
            if (v.isFunction(f)) return this.each(function (i) {
                var s = v(this);
                e[0] = f.call(this, i, n ? s.html() : t), s.domManip(e, n, r)
            });
            if (this[0]) {
                i = v.buildFragment(e, this, l), o = i.fragment, s = o.firstChild, o.childNodes.length === 1 && (o = s);
                if (s) {
                    n = n && v.nodeName(s, "tr");
                    for (u = i.cacheable || c - 1; a < c; a++) r.call(n && v.nodeName(this[a], "table") ? Lt(this[a], "tbody") : this[a], a === u ? o : v.clone(o, !0, !0))
                }
                o = s = null, l.length && v.each(l, function (e, t) {
                    t.src ? v.ajax ? v.ajax({
                        url: t.src,
                        type: "GET",
                        dataType: "script",
                        async: !1,
                        global: !1,
                        "throws": !0
                    }) : v.error("no ajax") : v.globalEval((t.text || t.textContent || t.innerHTML || "").replace(Tt, "")), t.parentNode && t.parentNode.removeChild(t)
                })
            }
            return this
        }
    }), v.buildFragment = function (e, n, r) {
        var s, o, u, a = e[0];
        return n = n || i, n = !n.nodeType && n[0] || n, n = n.ownerDocument || n, e.length === 1 && typeof a == "string" && a.length < 512 && n === i && a.charAt(0) === "<" && !bt.test(a) && (v.support.checkClone || !St.test(a)) && (v.support.html5Clone || !wt.test(a)) && (o = !0, s = v.fragments[a], u = s !== t), s || (s = n.createDocumentFragment(), v.clean(e, n, s, r), o && (v.fragments[a] = u && s)), {
            fragment: s,
            cacheable: o
        }
    }, v.fragments = {}, v.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function (e, t) {
        v.fn[e] = function (n) {
            var r, i = 0,
                s = [],
                o = v(n),
                u = o.length,
                a = this.length === 1 && this[0].parentNode;
            if ((a == null || a && a.nodeType === 11 && a.childNodes.length === 1) && u === 1) return o[t](this[0]), this;
            for (; i < u; i++) r = (i > 0 ? this.clone(!0) : this).get(), v(o[i])[t](r), s = s.concat(r);
            return this.pushStack(s, e, o.selector)
        }
    }), v.extend({
        clone: function (e, t, n) {
            var r, i, s, o;
            v.support.html5Clone || v.isXMLDoc(e) || !wt.test("<" + e.nodeName + ">") ? o = e.cloneNode(!0) : (kt.innerHTML = e.outerHTML, kt.removeChild(o = kt.firstChild));
            if ((!v.support.noCloneEvent || !v.support.noCloneChecked) && (e.nodeType === 1 || e.nodeType === 11) && !v.isXMLDoc(e)) {
                Ot(e, o), r = Mt(e), i = Mt(o);
                for (s = 0; r[s]; ++s) i[s] && Ot(r[s], i[s])
            }
            if (t) {
                At(e, o);
                if (n) {
                    r = Mt(e), i = Mt(o);
                    for (s = 0; r[s]; ++s) At(r[s], i[s])
                }
            }
            return r = i = null, o
        },
        clean: function (e, t, n, r) {
            var s, o, u, a, f, l, c, h, p, d, m, g, y = t === i && Ct,
                b = [];
            if (!t || typeof t.createDocumentFragment == "undefined") t = i;
            for (s = 0;
                (u = e[s]) != null; s++) {
                typeof u == "number" && (u += "");
                if (!u) continue;
                if (typeof u == "string")
                    if (!gt.test(u)) u = t.createTextNode(u);
                    else {
                        y = y || lt(t), c = t.createElement("div"), y.appendChild(c), u = u.replace(dt, "<$1></$2>"), a = (vt.exec(u) || ["", ""])[1].toLowerCase(), f = Nt[a] || Nt._default, l = f[0], c.innerHTML = f[1] + u + f[2];
                        while (l--) c = c.lastChild;
                        if (!v.support.tbody) {
                            h = mt.test(u), p = a === "table" && !h ? c.firstChild && c.firstChild.childNodes : f[1] === "<table>" && !h ? c.childNodes : [];
                            for (o = p.length - 1; o >= 0; --o) v.nodeName(p[o], "tbody") && !p[o].childNodes.length && p[o].parentNode.removeChild(p[o])
                        } !v.support.leadingWhitespace && pt.test(u) && c.insertBefore(t.createTextNode(pt.exec(u)[0]), c.firstChild), u = c.childNodes, c.parentNode.removeChild(c)
                    }
                u.nodeType ? b.push(u) : v.merge(b, u)
            }
            c && (u = c = y = null);
            if (!v.support.appendChecked)
                for (s = 0;
                    (u = b[s]) != null; s++) v.nodeName(u, "input") ? _t(u) : typeof u.getElementsByTagName != "undefined" && v.grep(u.getElementsByTagName("input"), _t);
            if (n) {
                m = function (e) {
                    if (!e.type || xt.test(e.type)) return r ? r.push(e.parentNode ? e.parentNode.removeChild(e) : e) : n.appendChild(e)
                };
                for (s = 0;
                    (u = b[s]) != null; s++)
                    if (!v.nodeName(u, "script") || !m(u)) n.appendChild(u), typeof u.getElementsByTagName != "undefined" && (g = v.grep(v.merge([], u.getElementsByTagName("script")), m), b.splice.apply(b, [s + 1, 0].concat(g)), s += g.length)
            }
            return b
        },
        cleanData: function (e, t) {
            var n, r, i, s, o = 0,
                u = v.expando,
                a = v.cache,
                f = v.support.deleteExpando,
                l = v.event.special;
            for (;
                (i = e[o]) != null; o++)
                if (t || v.acceptData(i)) {
                    r = i[u], n = r && a[r];
                    if (n) {
                        if (n.events)
                            for (s in n.events) l[s] ? v.event.remove(i, s) : v.removeEvent(i, s, n.handle);
                        a[r] && (delete a[r], f ? delete i[u] : i.removeAttribute ? i.removeAttribute(u) : i[u] = null, v.deletedIds.push(r))
                    }
                }
        }
    }),
        function () {
            var e, t;
            v.uaMatch = function (e) {
                e = e.toLowerCase();
                var t = /(chrome)[ \/]([\w.]+)/.exec(e) || /(webkit)[ \/]([\w.]+)/.exec(e) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(e) || /(msie) ([\w.]+)/.exec(e) || e.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(e) || [];
                return {
                    browser: t[1] || "",
                    version: t[2] || "0"
                }
            }, e = v.uaMatch(o.userAgent), t = {}, e.browser && (t[e.browser] = !0, t.version = e.version), t.chrome ? t.webkit = !0 : t.webkit && (t.safari = !0), v.browser = t, v.sub = function () {
                function e(t, n) {
                    return new e.fn.init(t, n)
                }
                v.extend(!0, e, this), e.superclass = this, e.fn = e.prototype = this(), e.fn.constructor = e, e.sub = this.sub, e.fn.init = function (r, i) {
                    return i && i instanceof v && !(i instanceof e) && (i = e(i)), v.fn.init.call(this, r, i, t)
                }, e.fn.init.prototype = e.fn;
                var t = e(i);
                return e
            }
        }();
    var Dt, Pt, Ht, Bt = /alpha\([^)]*\)/i,
        jt = /opacity=([^)]*)/,
        Ft = /^(top|right|bottom|left)$/,
        It = /^(none|table(?!-c[ea]).+)/,
        qt = /^margin/,
        Rt = new RegExp("^(" + m + ")(.*)$", "i"),
        Ut = new RegExp("^(" + m + ")(?!px)[a-z%]+$", "i"),
        zt = new RegExp("^([-+])=(" + m + ")", "i"),
        Wt = {
            BODY: "block"
        },
        Xt = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        Vt = {
            letterSpacing: 0,
            fontWeight: 400
        },
        $t = ["Top", "Right", "Bottom", "Left"],
        Jt = ["Webkit", "O", "Moz", "ms"],
        Kt = v.fn.toggle;
    v.fn.extend({
        css: function (e, n) {
            return v.access(this, function (e, n, r) {
                return r !== t ? v.style(e, n, r) : v.css(e, n)
            }, e, n, arguments.length > 1)
        },
        show: function () {
            return Yt(this, !0)
        },
        hide: function () {
            return Yt(this)
        },
        toggle: function (e, t) {
            var n = typeof e == "boolean";
            return v.isFunction(e) && v.isFunction(t) ? Kt.apply(this, arguments) : this.each(function () {
                (n ? e : Gt(this)) ? v(this).show() : v(this).hide()
            })
        }
    }), v.extend({
        cssHooks: {
            opacity: {
                get: function (e, t) {
                    if (t) {
                        var n = Dt(e, "opacity");
                        return n === "" ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            fillOpacity: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            "float": v.support.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function (e, n, r, i) {
            if (!e || e.nodeType === 3 || e.nodeType === 8 || !e.style) return;
            var s, o, u, a = v.camelCase(n),
                f = e.style;
            n = v.cssProps[a] || (v.cssProps[a] = Qt(f, a)), u = v.cssHooks[n] || v.cssHooks[a];
            if (r === t) return u && "get" in u && (s = u.get(e, !1, i)) !== t ? s : f[n];
            o = typeof r, o === "string" && (s = zt.exec(r)) && (r = (s[1] + 1) * s[2] + parseFloat(v.css(e, n)), o = "number");
            if (r == null || o === "number" && isNaN(r)) return;
            o === "number" && !v.cssNumber[a] && (r += "px");
            if (!u || !("set" in u) || (r = u.set(e, r, i)) !== t) try {
                f[n] = r
            } catch (l) { }
        },
        css: function (e, n, r, i) {
            var s, o, u, a = v.camelCase(n);
            return n = v.cssProps[a] || (v.cssProps[a] = Qt(e.style, a)), u = v.cssHooks[n] || v.cssHooks[a], u && "get" in u && (s = u.get(e, !0, i)), s === t && (s = Dt(e, n)), s === "normal" && n in Vt && (s = Vt[n]), r || i !== t ? (o = parseFloat(s), r || v.isNumeric(o) ? o || 0 : s) : s
        },
        swap: function (e, t, n) {
            var r, i, s = {};
            for (i in t) s[i] = e.style[i], e.style[i] = t[i];
            r = n.call(e);
            for (i in t) e.style[i] = s[i];
            return r
        }
    }), e.getComputedStyle ? Dt = function (t, n) {
        var r, i, s, o, u = e.getComputedStyle(t, null),
            a = t.style;
        return u && (r = u.getPropertyValue(n) || u[n], r === "" && !v.contains(t.ownerDocument, t) && (r = v.style(t, n)), Ut.test(r) && qt.test(n) && (i = a.width, s = a.minWidth, o = a.maxWidth, a.minWidth = a.maxWidth = a.width = r, r = u.width, a.width = i, a.minWidth = s, a.maxWidth = o)), r
    } : i.documentElement.currentStyle && (Dt = function (e, t) {
        var n, r, i = e.currentStyle && e.currentStyle[t],
            s = e.style;
        return i == null && s && s[t] && (i = s[t]), Ut.test(i) && !Ft.test(t) && (n = s.left, r = e.runtimeStyle && e.runtimeStyle.left, r && (e.runtimeStyle.left = e.currentStyle.left), s.left = t === "fontSize" ? "1em" : i, i = s.pixelLeft + "px", s.left = n, r && (e.runtimeStyle.left = r)), i === "" ? "auto" : i
    }), v.each(["height", "width"], function (e, t) {
        v.cssHooks[t] = {
            get: function (e, n, r) {
                if (n) return e.offsetWidth === 0 && It.test(Dt(e, "display")) ? v.swap(e, Xt, function () {
                    return tn(e, t, r)
                }) : tn(e, t, r)
            },
            set: function (e, n, r) {
                return Zt(e, n, r ? en(e, t, r, v.support.boxSizing && v.css(e, "boxSizing") === "border-box") : 0)
            }
        }
    }), v.support.opacity || (v.cssHooks.opacity = {
        get: function (e, t) {
            return jt.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
        },
        set: function (e, t) {
            var n = e.style,
                r = e.currentStyle,
                i = v.isNumeric(t) ? "alpha(opacity=" + t * 100 + ")" : "",
                s = r && r.filter || n.filter || "";
            n.zoom = 1;
            if (t >= 1 && v.trim(s.replace(Bt, "")) === "" && n.removeAttribute) {
                n.removeAttribute("filter");
                if (r && !r.filter) return
            }
            n.filter = Bt.test(s) ? s.replace(Bt, i) : s + " " + i
        }
    }), v(function () {
        v.support.reliableMarginRight || (v.cssHooks.marginRight = {
            get: function (e, t) {
                return v.swap(e, {
                    display: "inline-block"
                }, function () {
                    if (t) return Dt(e, "marginRight")
                })
            }
        }), !v.support.pixelPosition && v.fn.position && v.each(["top", "left"], function (e, t) {
            v.cssHooks[t] = {
                get: function (e, n) {
                    if (n) {
                        var r = Dt(e, t);
                        return Ut.test(r) ? v(e).position()[t] + "px" : r
                    }
                }
            }
        })
    }), v.expr && v.expr.filters && (v.expr.filters.hidden = function (e) {
        return e.offsetWidth === 0 && e.offsetHeight === 0 || !v.support.reliableHiddenOffsets && (e.style && e.style.display || Dt(e, "display")) === "none"
    }, v.expr.filters.visible = function (e) {
        return !v.expr.filters.hidden(e)
    }), v.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function (e, t) {
        v.cssHooks[e + t] = {
            expand: function (n) {
                var r, i = typeof n == "string" ? n.split(" ") : [n],
                    s = {};
                for (r = 0; r < 4; r++) s[e + $t[r] + t] = i[r] || i[r - 2] || i[0];
                return s
            }
        }, qt.test(e) || (v.cssHooks[e + t].set = Zt)
    });
    var rn = /%20/g,
        sn = /\[\]$/,
        on = /\r?\n/g,
        un = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
        an = /^(?:select|textarea)/i;
    v.fn.extend({
        serialize: function () {
            return v.param(this.serializeArray())
        },
        serializeArray: function () {
            return this.map(function () {
                return this.elements ? v.makeArray(this.elements) : this
            }).filter(function () {
                return this.name && !this.disabled && (this.checked || an.test(this.nodeName) || un.test(this.type))
            }).map(function (e, t) {
                var n = v(this).val();
                return n == null ? null : v.isArray(n) ? v.map(n, function (e, n) {
                    return {
                        name: t.name,
                        value: e.replace(on, "\r\n")
                    }
                }) : {
                        name: t.name,
                        value: n.replace(on, "\r\n")
                    }
            }).get()
        }
    }), v.param = function (e, n) {
        var r, i = [],
            s = function (e, t) {
                t = v.isFunction(t) ? t() : t == null ? "" : t, i[i.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
            };
        n === t && (n = v.ajaxSettings && v.ajaxSettings.traditional);
        if (v.isArray(e) || e.jquery && !v.isPlainObject(e)) v.each(e, function () {
            s(this.name, this.value)
        });
        else
            for (r in e) fn(r, e[r], n, s);
        return i.join("&").replace(rn, "+")
    };
    var ln, cn, hn = /#.*$/,
        pn = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg,
        dn = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,
        vn = /^(?:GET|HEAD)$/,
        mn = /^\/\//,
        gn = /\?/,
        yn = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
        bn = /([?&])_=[^&]*/,
        wn = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
        En = v.fn.load,
        Sn = {},
        xn = {},
        Tn = ["*/"] + ["*"];
    try {
        cn = s.href
    } catch (Nn) {
        cn = i.createElement("a"), cn.href = "", cn = cn.href
    }
    ln = wn.exec(cn.toLowerCase()) || [], v.fn.load = function (e, n, r) {
        if (typeof e != "string" && En) return En.apply(this, arguments);
        if (!this.length) return this;
        var i, s, o, u = this,
            a = e.indexOf(" ");
        return a >= 0 && (i = e.slice(a, e.length), e = e.slice(0, a)), v.isFunction(n) ? (r = n, n = t) : n && typeof n == "object" && (s = "POST"), v.ajax({
            url: e,
            type: s,
            dataType: "html",
            data: n,
            complete: function (e, t) {
                r && u.each(r, o || [e.responseText, t, e])
            }
        }).done(function (e) {
            o = arguments, u.html(i ? v("<div>").append(e.replace(yn, "")).find(i) : e)
        }), this
    }, v.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function (e, t) {
        v.fn[t] = function (e) {
            return this.on(t, e)
        }
    }), v.each(["get", "post"], function (e, n) {
        v[n] = function (e, r, i, s) {
            return v.isFunction(r) && (s = s || i, i = r, r = t), v.ajax({
                type: n,
                url: e,
                data: r,
                success: i,
                dataType: s
            })
        }
    }), v.extend({
        getScript: function (e, n) {
            return v.get(e, t, n, "script")
        },
        getJSON: function (e, t, n) {
            return v.get(e, t, n, "json")
        },
        ajaxSetup: function (e, t) {
            return t ? Ln(e, v.ajaxSettings) : (t = e, e = v.ajaxSettings), Ln(e, t), e
        },
        ajaxSettings: {
            url: cn,
            isLocal: dn.test(ln[1]),
            global: !0,
            type: "GET",
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            processData: !0,
            async: !0,
            accepts: {
                xml: "application/xml, text/xml",
                html: "text/html",
                text: "text/plain",
                json: "application/json, text/javascript",
                "*": Tn
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText"
            },
            converters: {
                "* text": e.String,
                "text html": !0,
                "text json": v.parseJSON,
                "text xml": v.parseXML
            },
            flatOptions: {
                context: !0,
                url: !0
            }
        },
        ajaxPrefilter: Cn(Sn),
        ajaxTransport: Cn(xn),
        ajax: function (e, n) {
            function T(e, n, s, a) {
                var l, y, b, w, S, T = n;
                if (E === 2) return;
                E = 2, u && clearTimeout(u), o = t, i = a || "", x.readyState = e > 0 ? 4 : 0, s && (w = An(c, x, s));
                if (e >= 200 && e < 300 || e === 304) c.ifModified && (S = x.getResponseHeader("Last-Modified"), S && (v.lastModified[r] = S), S = x.getResponseHeader("Etag"), S && (v.etag[r] = S)), e === 304 ? (T = "notmodified", l = !0) : (l = On(c, w), T = l.state, y = l.data, b = l.error, l = !b);
                else {
                    b = T;
                    if (!T || e) T = "error", e < 0 && (e = 0)
                }
                x.status = e, x.statusText = (n || T) + "", l ? d.resolveWith(h, [y, T, x]) : d.rejectWith(h, [x, T, b]), x.statusCode(g), g = t, f && p.trigger("ajax" + (l ? "Success" : "Error"), [x, c, l ? y : b]), m.fireWith(h, [x, T]), f && (p.trigger("ajaxComplete", [x, c]), --v.active || v.event.trigger("ajaxStop"))
            }
            typeof e == "object" && (n = e, e = t), n = n || {};
            var r, i, s, o, u, a, f, l, c = v.ajaxSetup({}, n),
                h = c.context || c,
                p = h !== c && (h.nodeType || h instanceof v) ? v(h) : v.event,
                d = v.Deferred(),
                m = v.Callbacks("once memory"),
                g = c.statusCode || {},
                b = {},
                w = {},
                E = 0,
                S = "canceled",
                x = {
                    readyState: 0,
                    setRequestHeader: function (e, t) {
                        if (!E) {
                            var n = e.toLowerCase();
                            e = w[n] = w[n] || e, b[e] = t
                        }
                        return this
                    },
                    getAllResponseHeaders: function () {
                        return E === 2 ? i : null
                    },
                    getResponseHeader: function (e) {
                        var n;
                        if (E === 2) {
                            if (!s) {
                                s = {};
                                while (n = pn.exec(i)) s[n[1].toLowerCase()] = n[2]
                            }
                            n = s[e.toLowerCase()]
                        }
                        return n === t ? null : n
                    },
                    overrideMimeType: function (e) {
                        return E || (c.mimeType = e), this
                    },
                    abort: function (e) {
                        return e = e || S, o && o.abort(e), T(0, e), this
                    }
                };
            d.promise(x), x.success = x.done, x.error = x.fail, x.complete = m.add, x.statusCode = function (e) {
                if (e) {
                    var t;
                    if (E < 2)
                        for (t in e) g[t] = [g[t], e[t]];
                    else t = e[x.status], x.always(t)
                }
                return this
            }, c.url = ((e || c.url) + "").replace(hn, "").replace(mn, ln[1] + "//"), c.dataTypes = v.trim(c.dataType || "*").toLowerCase().split(y), c.crossDomain == null && (a = wn.exec(c.url.toLowerCase()), c.crossDomain = !(!a || a[1] === ln[1] && a[2] === ln[2] && (a[3] || (a[1] === "http:" ? 80 : 443)) == (ln[3] || (ln[1] === "http:" ? 80 : 443)))), c.data && c.processData && typeof c.data != "string" && (c.data = v.param(c.data, c.traditional)), kn(Sn, c, n, x);
            if (E === 2) return x;
            f = c.global, c.type = c.type.toUpperCase(), c.hasContent = !vn.test(c.type), f && v.active++ === 0 && v.event.trigger("ajaxStart");
            if (!c.hasContent) {
                c.data && (c.url += (gn.test(c.url) ? "&" : "?") + c.data, delete c.data), r = c.url;
                if (c.cache === !1) {
                    var N = v.now(),
                        C = c.url.replace(bn, "$1_=" + N);
                    c.url = C + (C === c.url ? (gn.test(c.url) ? "&" : "?") + "_=" + N : "")
                }
            } (c.data && c.hasContent && c.contentType !== !1 || n.contentType) && x.setRequestHeader("Content-Type", c.contentType), c.ifModified && (r = r || c.url, v.lastModified[r] && x.setRequestHeader("If-Modified-Since", v.lastModified[r]), v.etag[r] && x.setRequestHeader("If-None-Match", v.etag[r])), x.setRequestHeader("Accept", c.dataTypes[0] && c.accepts[c.dataTypes[0]] ? c.accepts[c.dataTypes[0]] + (c.dataTypes[0] !== "*" ? ", " + Tn + "; q=0.01" : "") : c.accepts["*"]);
            for (l in c.headers) x.setRequestHeader(l, c.headers[l]);
            if (!c.beforeSend || c.beforeSend.call(h, x, c) !== !1 && E !== 2) {
                S = "abort";
                for (l in {
                    success: 1,
                    error: 1,
                    complete: 1
                }) x[l](c[l]);
                o = kn(xn, c, n, x);
                if (!o) T(-1, "No Transport");
                else {
                    x.readyState = 1, f && p.trigger("ajaxSend", [x, c]), c.async && c.timeout > 0 && (u = setTimeout(function () {
                        x.abort("timeout")
                    }, c.timeout));
                    try {
                        E = 1, o.send(b, T)
                    } catch (k) {
                        if (!(E < 2)) throw k;
                        T(-1, k)
                    }
                }
                return x
            }
            return x.abort()
        },
        active: 0,
        lastModified: {},
        etag: {}
    });
    var Mn = [],
        _n = /\?/,
        Dn = /(=)\?(?=&|$)|\?\?/,
        Pn = v.now();
    v.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function () {
            var e = Mn.pop() || v.expando + "_" + Pn++;
            return this[e] = !0, e
        }
    }), v.ajaxPrefilter("json jsonp", function (n, r, i) {
        var s, o, u, a = n.data,
            f = n.url,
            l = n.jsonp !== !1,
            c = l && Dn.test(f),
            h = l && !c && typeof a == "string" && !(n.contentType || "").indexOf("application/x-www-form-urlencoded") && Dn.test(a);
        if (n.dataTypes[0] === "jsonp" || c || h) return s = n.jsonpCallback = v.isFunction(n.jsonpCallback) ? n.jsonpCallback() : n.jsonpCallback, o = e[s], c ? n.url = f.replace(Dn, "$1" + s) : h ? n.data = a.replace(Dn, "$1" + s) : l && (n.url += (_n.test(f) ? "&" : "?") + n.jsonp + "=" + s), n.converters["script json"] = function () {
            return u || v.error(s + " was not called"), u[0]
        }, n.dataTypes[0] = "json", e[s] = function () {
            u = arguments
        }, i.always(function () {
            e[s] = o, n[s] && (n.jsonpCallback = r.jsonpCallback, Mn.push(s)), u && v.isFunction(o) && o(u[0]), u = o = t
        }), "script"
    }), v.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /javascript|ecmascript/
        },
        converters: {
            "text script": function (e) {
                return v.globalEval(e), e
            }
        }
    }), v.ajaxPrefilter("script", function (e) {
        e.cache === t && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
    }), v.ajaxTransport("script", function (e) {
        if (e.crossDomain) {
            var n, r = i.head || i.getElementsByTagName("head")[0] || i.documentElement;
            return {
                send: function (s, o) {
                    n = i.createElement("script"), n.async = "async", e.scriptCharset && (n.charset = e.scriptCharset), n.src = e.url, n.onload = n.onreadystatechange = function (e, i) {
                        if (i || !n.readyState || /loaded|complete/.test(n.readyState)) n.onload = n.onreadystatechange = null, r && n.parentNode && r.removeChild(n), n = t, i || o(200, "success")
                    }, r.insertBefore(n, r.firstChild)
                },
                abort: function () {
                    n && n.onload(0, 1)
                }
            }
        }
    });
    var Hn, Bn = e.ActiveXObject ? function () {
        for (var e in Hn) Hn[e](0, 1)
    } : !1,
        jn = 0;
    v.ajaxSettings.xhr = e.ActiveXObject ? function () {
        return !this.isLocal && Fn() || In()
    } : Fn,
        function (e) {
            v.extend(v.support, {
                ajax: !!e,
                cors: !!e && "withCredentials" in e
            })
        }(v.ajaxSettings.xhr()), v.support.ajax && v.ajaxTransport(function (n) {
            if (!n.crossDomain || v.support.cors) {
                var r;
                return {
                    send: function (i, s) {
                        var o, u, a = n.xhr();
                        n.username ? a.open(n.type, n.url, n.async, n.username, n.password) : a.open(n.type, n.url, n.async);
                        if (n.xhrFields)
                            for (u in n.xhrFields) a[u] = n.xhrFields[u];
                        n.mimeType && a.overrideMimeType && a.overrideMimeType(n.mimeType), !n.crossDomain && !i["X-Requested-With"] && (i["X-Requested-With"] = "XMLHttpRequest");
                        try {
                            for (u in i) a.setRequestHeader(u, i[u])
                        } catch (f) { }
                        a.send(n.hasContent && n.data || null), r = function (e, i) {
                            var u, f, l, c, h;
                            try {
                                if (r && (i || a.readyState === 4)) {
                                    r = t, o && (a.onreadystatechange = v.noop, Bn && delete Hn[o]);
                                    if (i) a.readyState !== 4 && a.abort();
                                    else {
                                        u = a.status, l = a.getAllResponseHeaders(), c = {}, h = a.responseXML, h && h.documentElement && (c.xml = h);
                                        try {
                                            c.text = a.responseText
                                        } catch (p) { }
                                        try {
                                            f = a.statusText
                                        } catch (p) {
                                            f = ""
                                        } !u && n.isLocal && !n.crossDomain ? u = c.text ? 200 : 404 : u === 1223 && (u = 204)
                                    }
                                }
                            } catch (d) {
                                i || s(-1, d)
                            }
                            c && s(u, f, c, l)
                        }, n.async ? a.readyState === 4 ? setTimeout(r, 0) : (o = ++jn, Bn && (Hn || (Hn = {}, v(e).unload(Bn)), Hn[o] = r), a.onreadystatechange = r) : r()
                    },
                    abort: function () {
                        r && r(0, 1)
                    }
                }
            }
        });
    var qn, Rn, Un = /^(?:toggle|show|hide)$/,
        zn = new RegExp("^(?:([-+])=|)(" + m + ")([a-z%]*)$", "i"),
        Wn = /queueHooks$/,
        Xn = [Gn],
        Vn = {
            "*": [function (e, t) {
                var n, r, i = this.createTween(e, t),
                    s = zn.exec(t),
                    o = i.cur(),
                    u = +o || 0,
                    a = 1,
                    f = 20;
                if (s) {
                    n = +s[2], r = s[3] || (v.cssNumber[e] ? "" : "px");
                    if (r !== "px" && u) {
                        u = v.css(i.elem, e, !0) || n || 1;
                        do a = a || ".5", u /= a, v.style(i.elem, e, u + r); while (a !== (a = i.cur() / o) && a !== 1 && --f)
                    }
                    i.unit = r, i.start = u, i.end = s[1] ? u + (s[1] + 1) * n : n
                }
                return i
            }]
        };
    v.Animation = v.extend(Kn, {
        tweener: function (e, t) {
            v.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
            var n, r = 0,
                i = e.length;
            for (; r < i; r++) n = e[r], Vn[n] = Vn[n] || [], Vn[n].unshift(t)
        },
        prefilter: function (e, t) {
            t ? Xn.unshift(e) : Xn.push(e)
        }
    }), v.Tween = Yn, Yn.prototype = {
        constructor: Yn,
        init: function (e, t, n, r, i, s) {
            this.elem = e, this.prop = n, this.easing = i || "swing", this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = s || (v.cssNumber[n] ? "" : "px")
        },
        cur: function () {
            var e = Yn.propHooks[this.prop];
            return e && e.get ? e.get(this) : Yn.propHooks._default.get(this)
        },
        run: function (e) {
            var t, n = Yn.propHooks[this.prop];
            return this.options.duration ? this.pos = t = v.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : Yn.propHooks._default.set(this), this
        }
    }, Yn.prototype.init.prototype = Yn.prototype, Yn.propHooks = {
        _default: {
            get: function (e) {
                var t;
                return e.elem[e.prop] == null || !!e.elem.style && e.elem.style[e.prop] != null ? (t = v.css(e.elem, e.prop, !1, ""), !t || t === "auto" ? 0 : t) : e.elem[e.prop]
            },
            set: function (e) {
                v.fx.step[e.prop] ? v.fx.step[e.prop](e) : e.elem.style && (e.elem.style[v.cssProps[e.prop]] != null || v.cssHooks[e.prop]) ? v.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
            }
        }
    }, Yn.propHooks.scrollTop = Yn.propHooks.scrollLeft = {
        set: function (e) {
            e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
        }
    }, v.each(["toggle", "show", "hide"], function (e, t) {
        var n = v.fn[t];
        v.fn[t] = function (r, i, s) {
            return r == null || typeof r == "boolean" || !e && v.isFunction(r) && v.isFunction(i) ? n.apply(this, arguments) : this.animate(Zn(t, !0), r, i, s)
        }
    }), v.fn.extend({
        fadeTo: function (e, t, n, r) {
            return this.filter(Gt).css("opacity", 0).show().end().animate({
                opacity: t
            }, e, n, r)
        },
        animate: function (e, t, n, r) {
            var i = v.isEmptyObject(e),
                s = v.speed(t, n, r),
                o = function () {
                    var t = Kn(this, v.extend({}, e), s);
                    i && t.stop(!0)
                };
            return i || s.queue === !1 ? this.each(o) : this.queue(s.queue, o)
        },
        stop: function (e, n, r) {
            var i = function (e) {
                var t = e.stop;
                delete e.stop, t(r)
            };
            return typeof e != "string" && (r = n, n = e, e = t), n && e !== !1 && this.queue(e || "fx", []), this.each(function () {
                var t = !0,
                    n = e != null && e + "queueHooks",
                    s = v.timers,
                    o = v._data(this);
                if (n) o[n] && o[n].stop && i(o[n]);
                else
                    for (n in o) o[n] && o[n].stop && Wn.test(n) && i(o[n]);
                for (n = s.length; n--;) s[n].elem === this && (e == null || s[n].queue === e) && (s[n].anim.stop(r), t = !1, s.splice(n, 1));
                (t || !r) && v.dequeue(this, e)
            })
        }
    }), v.each({
        slideDown: Zn("show"),
        slideUp: Zn("hide"),
        slideToggle: Zn("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function (e, t) {
        v.fn[e] = function (e, n, r) {
            return this.animate(t, e, n, r)
        }
    }), v.speed = function (e, t, n) {
        var r = e && typeof e == "object" ? v.extend({}, e) : {
            complete: n || !n && t || v.isFunction(e) && e,
            duration: e,
            easing: n && t || t && !v.isFunction(t) && t
        };
        r.duration = v.fx.off ? 0 : typeof r.duration == "number" ? r.duration : r.duration in v.fx.speeds ? v.fx.speeds[r.duration] : v.fx.speeds._default;
        if (r.queue == null || r.queue === !0) r.queue = "fx";
        return r.old = r.complete, r.complete = function () {
            v.isFunction(r.old) && r.old.call(this), r.queue && v.dequeue(this, r.queue)
        }, r
    }, v.easing = {
        linear: function (e) {
            return e
        },
        swing: function (e) {
            return .5 - Math.cos(e * Math.PI) / 2
        }
    }, v.timers = [], v.fx = Yn.prototype.init, v.fx.tick = function () {
        var e, n = v.timers,
            r = 0;
        qn = v.now();
        for (; r < n.length; r++) e = n[r], !e() && n[r] === e && n.splice(r--, 1);
        n.length || v.fx.stop(), qn = t
    }, v.fx.timer = function (e) {
        e() && v.timers.push(e) && !Rn && (Rn = setInterval(v.fx.tick, v.fx.interval))
    }, v.fx.interval = 13, v.fx.stop = function () {
        clearInterval(Rn), Rn = null
    }, v.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    }, v.fx.step = {}, v.expr && v.expr.filters && (v.expr.filters.animated = function (e) {
        return v.grep(v.timers, function (t) {
            return e === t.elem
        }).length
    });
    var er = /^(?:body|html)$/i;
    v.fn.offset = function (e) {
        if (arguments.length) return e === t ? this : this.each(function (t) {
            v.offset.setOffset(this, e, t)
        });
        var n, r, i, s, o, u, a, f = {
            top: 0,
            left: 0
        },
            l = this[0],
            c = l && l.ownerDocument;
        if (!c) return;
        return (r = c.body) === l ? v.offset.bodyOffset(l) : (n = c.documentElement, v.contains(n, l) ? (typeof l.getBoundingClientRect != "undefined" && (f = l.getBoundingClientRect()), i = tr(c), s = n.clientTop || r.clientTop || 0, o = n.clientLeft || r.clientLeft || 0, u = i.pageYOffset || n.scrollTop, a = i.pageXOffset || n.scrollLeft, {
            top: f.top + u - s,
            left: f.left + a - o
        }) : f)
    }, v.offset = {
        bodyOffset: function (e) {
            var t = e.offsetTop,
                n = e.offsetLeft;
            return v.support.doesNotIncludeMarginInBodyOffset && (t += parseFloat(v.css(e, "marginTop")) || 0, n += parseFloat(v.css(e, "marginLeft")) || 0), {
                top: t,
                left: n
            }
        },
        setOffset: function (e, t, n) {
            var r = v.css(e, "position");
            r === "static" && (e.style.position = "relative");
            var i = v(e),
                s = i.offset(),
                o = v.css(e, "top"),
                u = v.css(e, "left"),
                a = (r === "absolute" || r === "fixed") && v.inArray("auto", [o, u]) > -1,
                f = {},
                l = {},
                c, h;
            a ? (l = i.position(), c = l.top, h = l.left) : (c = parseFloat(o) || 0, h = parseFloat(u) || 0), v.isFunction(t) && (t = t.call(e, n, s)), t.top != null && (f.top = t.top - s.top + c), t.left != null && (f.left = t.left - s.left + h), "using" in t ? t.using.call(e, f) : i.css(f)
        }
    }, v.fn.extend({
        position: function () {
            if (!this[0]) return;
            var e = this[0],
                t = this.offsetParent(),
                n = this.offset(),
                r = er.test(t[0].nodeName) ? {
                    top: 0,
                    left: 0
                } : t.offset();
            return n.top -= parseFloat(v.css(e, "marginTop")) || 0, n.left -= parseFloat(v.css(e, "marginLeft")) || 0, r.top += parseFloat(v.css(t[0], "borderTopWidth")) || 0, r.left += parseFloat(v.css(t[0], "borderLeftWidth")) || 0, {
                top: n.top - r.top,
                left: n.left - r.left
            }
        },
        offsetParent: function () {
            return this.map(function () {
                var e = this.offsetParent || i.body;
                while (e && !er.test(e.nodeName) && v.css(e, "position") === "static") e = e.offsetParent;
                return e || i.body
            })
        }
    }), v.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function (e, n) {
        var r = /Y/.test(n);
        v.fn[e] = function (i) {
            return v.access(this, function (e, i, s) {
                var o = tr(e);
                if (s === t) return o ? n in o ? o[n] : o.document.documentElement[i] : e[i];
                o ? o.scrollTo(r ? v(o).scrollLeft() : s, r ? s : v(o).scrollTop()) : e[i] = s
            }, e, i, arguments.length, null)
        }
    }), v.each({
        Height: "height",
        Width: "width"
    }, function (e, n) {
        v.each({
            padding: "inner" + e,
            content: n,
            "": "outer" + e
        }, function (r, i) {
            v.fn[i] = function (i, s) {
                var o = arguments.length && (r || typeof i != "boolean"),
                    u = r || (i === !0 || s === !0 ? "margin" : "border");
                return v.access(this, function (n, r, i) {
                    var s;
                    return v.isWindow(n) ? n.document.documentElement["client" + e] : n.nodeType === 9 ? (s = n.documentElement, Math.max(n.body["scroll" + e], s["scroll" + e], n.body["offset" + e], s["offset" + e], s["client" + e])) : i === t ? v.css(n, r, i, u) : v.style(n, r, i, u)
                }, n, o ? i : t, o, null)
            }
        })
    }), e.jQuery = e.$ = v, typeof define == "function" && define.amd && define.amd.jQuery && define("jquery", [], function () {
        return v
    })
})(window);

/*

jsPDF fromHTML plugin. BETA stage. API subject to change. Needs browser, jQuery
Copyright (c) 2012 2012 Willow Systems Corporation, willow-systems.com
 jsPDF 0.9.0rc1 ( 2013-04-07T16:52 commit ID d95d8f69915bb999f6704e8021108e2e755bd868 )
Copyright (c) 2010-2012 James Hall, james@snapshotmedia.co.uk, https://github.com/MrRio/jsPDF
Copyright (c) 2012 Willow Systems Corporation, willow-systems.com
MIT license.

            -----------------------------------------------------------------------------------------------
            JavaScript PubSub library
            2012 (c) ddotsenko@willowsystems.com
            based on Peter Higgins (dante@dojotoolkit.org)
            Loosely based on Dojo publish/subscribe API, limited in scope. Rewritten blindly.
            Original is (c) Dojo Foundation 2004-2010. Released under either AFL or new BSD, see:
            http://dojofoundation.org/license for more information.
            -----------------------------------------------------------------------------------------------

jsPDF addImage plugin (JPEG only at this time)
Copyright (c) 2012 https://github.com/siefkenj/

jsPDF Silly SVG plugin
Copyright (c) 2012 Willow Systems Corporation, willow-systems.com

jsPDF split_text_to_size plugin
Copyright (c) 2012 Willow Systems Corporation, willow-systems.com
MIT license.

jsPDF standard_fonts_metrics plugin
Copyright (c) 2012 Willow Systems Corporation, willow-systems.com
MIT license.
*/
var jsPDF = function () {
    function f(g, d, e, h) {
        g = "undefined" === typeof g ? "p" : g.toString().toLowerCase();
        "undefined" === typeof d && (d = "mm");
        "undefined" === typeof e && (e = "a4");
        "undefined" === typeof h && "undefined" === typeof zpipe && (h = !1);
        var a = e.toString().toLowerCase(),
            p = [],
            j = 0,
            m = h;
        h = {
            a3: [841.89, 1190.55],
            a4: [595.28, 841.89],
            a5: [420.94, 595.28],
            letter: [612, 792],
            legal: [612, 1008]
        };
        var u = "0 g",
            c = 0,
            q = [],
            C = 2,
            F = !1,
            H = [],
            n = {},
            x = {},
            z = 16,
            A, y, s, r, I = {
                title: "",
                subject: "",
                author: "",
                keywords: "",
                creator: ""
            },
            w = 0,
            aa = 0,
            D = {},
            G = new k(D),
            B, v = function (c) {
                return c.toFixed(2)
            },
            V = function (c) {
                var e = c.toFixed(0);
                return 10 > c ? "0" + e : e
            },
            l = function (e) {
                F ? q[c].push(e) : (p.push(e), j += e.length + 1)
            },
            N = function () {
                C++;
                H[C] = j;
                l(C + " 0 obj");
                return C
            },
            Q = function (c) {
                l("stream");
                l(c);
                l("endstream")
            },
            J, ca, R, t = function (c, e) {
                var d;
                d = c;
                var j = e,
                    a, h, b, p, g, m;
                void 0 === j && (j = {});
                a = j.sourceEncoding ? a : "Unicode";
                b = j.outputEncoding;
                if ((j.autoencode || b) && n[A].metadata && n[A].metadata[a] && n[A].metadata[a].encoding)
                    if (a = n[A].metadata[a].encoding, !b && n[A].encoding && (b = n[A].encoding), !b && a.codePages && (b = a.codePages[0]), "string" === typeof b && (b = a[b]), b) {
                        g = !1;
                        p = [];
                        a = 0;
                        for (h = d.length; a < h; a++)(m = b[d.charCodeAt(a)]) ? p.push(String.fromCharCode(m)) : p.push(d[a]), p[a].charCodeAt(0) >> 8 && (g = !0);
                        d = p.join("")
                    }
                for (a = d.length; void 0 === g && 0 !== a;) d.charCodeAt(a - 1) >> 8 && (g = !0), a--;
                if (g) {
                    p = j.noBOM ? [] : [254, 255];
                    a = 0;
                    for (h = d.length; a < h; a++) {
                        m = d.charCodeAt(a);
                        j = m >> 8;
                        if (j >> 8) throw Error("Character at position " + a.toString(10) + " of string '" + d + "' exceeds 16bits. Cannot be encoded into UCS-2 BE");
                        p.push(j);
                        p.push(m - (j << 8))
                    }
                    d = String.fromCharCode.apply(void 0, p)
                }
                return d.replace(/\\/g, "\\\\").replace(/\(/g, "\\(").replace(/\)/g, "\\)")
            },
            W = function () {
                c++;
                F = !0;
                q[c] = [];
                l(v(0.200025 * r) + " w");
                l("0 G");
                0 !== w && l(w.toString(10) + " J");
                0 !== aa && l(aa.toString(10) + " j");
                G.publish("addPage", {
                    pageNumber: c
                })
            },
            E = function (c, a) {
                var d;
                void 0 === c && (c = n[A].fontName);
                void 0 === a && (a = n[A].fontStyle);
                try {
                    d = x[c][a]
                } catch (e) {
                    d = void 0
                }
                if (!d) throw Error("Unable to look up font label for font '" + c + "', '" + a + "'. Refer to getFontList() for available fonts.");
                return d
            },
            K = function () {
                F = !1;
                p = [];
                H = [];
                l("%PDF-1.3");
                J = s * r;
                ca = y * r;
                var a, d, e, b, h;
                for (a = 1; a <= c; a++) {
                    N();
                    l("<</Type /Page");
                    l("/Parent 1 0 R");
                    l("/Resources 2 0 R");
                    l("/Contents " + (C + 1) + " 0 R>>");
                    l("endobj");
                    d = q[a].join("\n");
                    N();
                    if (m) {
                        e = [];
                        for (b = 0; b < d.length; ++b) e[b] = d.charCodeAt(b);
                        h = adler32cs.from(d);
                        d = new Deflater(6);
                        d.append(new Uint8Array(e));
                        d = d.flush();
                        e = [new Uint8Array([120, 156]), new Uint8Array(d), new Uint8Array([h & 255, h >> 8 & 255, h >> 16 & 255, h >> 24 & 255])];
                        d = "";
                        for (b in e) e.hasOwnProperty(b) && (d +=
                            String.fromCharCode.apply(null, e[b]));
                        l("<</Length " + d.length + " /Filter [/FlateDecode]>>")
                    } else l("<</Length " + d.length + ">>");
                    Q(d);
                    l("endobj")
                }
                H[1] = j;
                l("1 0 obj");
                l("<</Type /Pages");
                R = "/Kids [";
                for (b = 0; b < c; b++) R += 3 + 2 * b + " 0 R ";
                l(R + "]");
                l("/Count " + c);
                l("/MediaBox [0 0 " + v(J) + " " + v(ca) + "]");
                l(">>");
                l("endobj");
                for (var g in n) n.hasOwnProperty(g) && (a = n[g], a.objectNumber = N(), l("<</BaseFont/" + a.PostScriptName + "/Type/Font"), "string" === typeof a.encoding && l("/Encoding/" + a.encoding), l("/Subtype/Type1>>"),
                    l("endobj"));
                G.publish("putResources");
                H[2] = j;
                l("2 0 obj");
                l("<<");
                l("/ProcSet [/PDF /Text /ImageB /ImageC /ImageI]");
                l("/Font <<");
                for (var f in n) n.hasOwnProperty(f) && l("/" + f + " " + n[f].objectNumber + " 0 R");
                l(">>");
                l("/XObject <<");
                G.publish("putXobjectDict");
                l(">>");
                l(">>");
                l("endobj");
                G.publish("postPutResources");
                N();
                l("<<");
                l("/Producer (jsPDF 20120619)");
                I.title && l("/Title (" + t(I.title) + ")");
                I.subject && l("/Subject (" + t(I.subject) + ")");
                I.author && l("/Author (" + t(I.author) + ")");
                I.keywords && l("/Keywords (" +
                    t(I.keywords) + ")");
                I.creator && l("/Creator (" + t(I.creator) + ")");
                g = new Date;
                l("/CreationDate (D:" + [g.getFullYear(), V(g.getMonth() + 1), V(g.getDate()), V(g.getHours()), V(g.getMinutes()), V(g.getSeconds())].join("") + ")");
                l(">>");
                l("endobj");
                N();
                l("<<");
                l("/Type /Catalog");
                l("/Pages 1 0 R");
                l("/OpenAction [3 0 R /FitH null]");
                l("/PageLayout /OneColumn");
                G.publish("putCatalog");
                l(">>");
                l("endobj");
                g = j;
                l("xref");
                l("0 " + (C + 1));
                l("0000000000 65535 f ");
                for (f = 1; f <= C; f++) a = H[f].toFixed(0), a = 10 > a.length ? Array(11 -
                    a.length).join("0") + a : a, l(a + " 00000 n ");
                l("trailer");
                l("<<");
                l("/Size " + (C + 1));
                l("/Root " + C + " 0 R");
                l("/Info " + (C - 1) + " 0 R");
                l(">>");
                l("startxref");
                l(g);
                l("%%EOF");
                F = !0;
                return p.join("\n")
            },
            Y = function (c) {
                var a = "S";
                if ("F" === c) a = "f";
                else if ("FD" === c || "DF" === c) a = "B";
                return a
            },
            Z = function (c, a) {
                var d, e, b, j;
                switch (c) {
                    case void 0:
                        return K();
                    case "save":
                        if (navigator.getUserMedia && (void 0 === window.URL || void 0 === window.URL.createObjectURL)) return D.output("dataurlnewwindow");
                        d = K();
                        e = d.length;
                        b = new Uint8Array(new ArrayBuffer(e));
                        for (j = 0; j < e; j++) b[j] = d.charCodeAt(j);
                        d = new Blob([b], {
                            type: "application/pdf"
                        });
                        saveAs(d, a);
                        break;
                    case "datauristring":
                    case "dataurlstring":
                        return "data:application/pdf;base64," + btoa(K());
                    case "datauri":
                    case "dataurl":
                        document.location.href = "data:application/pdf;base64," + btoa(K());
                        break;
                    case "dataurlnewwindow":
                        window.open("data:application/pdf;base64," + btoa(K()));
                        break;
                    default:
                        throw Error('Output type "' + c + '" is not supported.');
                }
            };
        if ("pt" === d) r = 1;
        else if ("mm" === d) r = 72 / 25.4;
        else if ("cm" === d) r = 72 / 2.54;
        else if ("in" === d) r = 72;
        else throw "Invalid unit: " + d;
        if (h.hasOwnProperty(a)) y = h[a][1] / r, s = h[a][0] / r;
        else try {
            y = e[1], s = e[0]
        } catch (M) {
            throw "Invalid format: " + e;
        }
        if ("p" === g || "portrait" === g) g = "p", s > y && (g = s, s = y, y = g);
        else if ("l" === g || "landscape" === g) g = "l", y > s && (g = s, s = y, y = g);
        else throw "Invalid orientation: " + g;
        D.internal = {
            pdfEscape: t,
            getStyle: Y,
            getFont: function () {
                return n[E.apply(D, arguments)]
            },
            getFontSize: function () {
                return z
            },
            btoa: btoa,
            write: function (c, a, d, e) {
                l(1 === arguments.length ? c : Array.prototype.join.call(arguments,
                    " "))
            },
            getCoordinateString: function (c) {
                return v(c * r)
            },
            getVerticalCoordinateString: function (c) {
                return v((y - c) * r)
            },
            collections: {},
            newObject: N,
            putStream: Q,
            events: G,
            scaleFactor: r,
            pageSize: {
                width: s,
                height: y
            },
            output: function (c, a) {
                return Z(c, a)
            }
        };
        D.addPage = function () {
            W();
            return this
        };
        D.text = function (c, a, d, e) {
            var b, j;
            "number" === typeof c && (b = c, j = a, c = d, a = b, d = j);
            "string" === typeof c && c.match(/[\n\r]/) && (c = c.split(/\r\n|\r|\n/g));
            "undefined" === typeof e ? e = {
                noBOM: !0,
                autoencode: !0
            } : (void 0 === e.noBOM && (e.noBOM = !0),
                void 0 === e.autoencode && (e.autoencode = !0));
            if ("string" === typeof c) e = t(c, e);
            else if (c instanceof Array) {
                c = c.concat();
                for (b = c.length - 1; - 1 !== b; b--) c[b] = t(c[b], e);
                e = c.join(") Tj\nT* (")
            } else throw Error('Type of text must be string or Array. "' + c + '" is not recognized.');
            l("BT\n/" + A + " " + z + " Tf\n" + z + " TL\n" + u + "\n" + v(a * r) + " " + v((y - d) * r) + " Td\n(" + e + ") Tj\nET");
            return this
        };
        D.line = function (c, a, d, e) {
            l(v(c * r) + " " + v((y - a) * r) + " m " + v(d * r) + " " + v((y - e) * r) + " l S");
            return this
        };
        D.lines = function (c, a, d, e, b) {
            var j, g,
                h, p, m, f, q, u;
            "number" === typeof c && (j = c, g = a, c = d, a = j, d = g);
            b = Y(b);
            e = void 0 === e ? [1, 1] : e;
            l((a * r).toFixed(3) + " " + ((y - d) * r).toFixed(3) + " m ");
            j = e[0];
            e = e[1];
            g = c.length;
            u = d;
            for (d = 0; d < g; d++) h = c[d], 2 === h.length ? (a = h[0] * j + a, u = h[1] * e + u, l((a * r).toFixed(3) + " " + ((y - u) * r).toFixed(3) + " l")) : (p = h[0] * j + a, m = h[1] * e + u, f = h[2] * j + a, q = h[3] * e + u, a = h[4] * j + a, u = h[5] * e + u, l((p * r).toFixed(3) + " " + ((y - m) * r).toFixed(3) + " " + (f * r).toFixed(3) + " " + ((y - q) * r).toFixed(3) + " " + (a * r).toFixed(3) + " " + ((y - u) * r).toFixed(3) + " c"));
            l(b);
            return this
        };
        D.rect = function (c, a, d, e, b) {
            b = Y(b);
            l([v(c * r), v((y - a) * r), v(d * r), v(-e * r), "re", b].join(" "));
            return this
        };
        D.triangle = function (c, a, d, e, b, j, h) {
            this.lines([
                [d - c, e - a],
                [b - d, j - e],
                [c - b, a - j]
            ], c, a, [1, 1], h);
            return this
        };
        D.roundedRect = function (c, a, d, e, b, j, h) {
            var g = 4 / 3 * (Math.SQRT2 - 1);
            this.lines([
                [d - 2 * b, 0],
                [b * g, 0, b, j - j * g, b, j],
                [0, e - 2 * j],
                [0, j * g, -(b * g), j, -b, j],
                [-d + 2 * b, 0],
                [-(b * g), 0, -b, -(j * g), -b, -j],
                [0, -e + 2 * j],
                [0, -(j * g), b * g, -j, b, -j]
            ], c + b, a, [1, 1], h);
            return this
        };
        D.ellipse = function (c, a, d, e, b) {
            b = Y(b);
            var j = 4 / 3 * (Math.SQRT2 -
                1) * d,
                g = 4 / 3 * (Math.SQRT2 - 1) * e;
            l([v((c + d) * r), v((y - a) * r), "m", v((c + d) * r), v((y - (a - g)) * r), v((c + j) * r), v((y - (a - e)) * r), v(c * r), v((y - (a - e)) * r), "c"].join(" "));
            l([v((c - j) * r), v((y - (a - e)) * r), v((c - d) * r), v((y - (a - g)) * r), v((c - d) * r), v((y - a) * r), "c"].join(" "));
            l([v((c - d) * r), v((y - (a + g)) * r), v((c - j) * r), v((y - (a + e)) * r), v(c * r), v((y - (a + e)) * r), "c"].join(" "));
            l([v((c + j) * r), v((y - (a + e)) * r), v((c + d) * r), v((y - (a + g)) * r), v((c + d) * r), v((y - a) * r), "c", b].join(" "));
            return this
        };
        D.circle = function (c, a, d, e) {
            return this.ellipse(c, a, d, d, e)
        };
        D.setProperties =
            function (c) {
                for (var a in I) I.hasOwnProperty(a) && c[a] && (I[a] = c[a]);
                return this
            };
        D.setFontSize = function (c) {
            z = c;
            return this
        };
        D.setFont = function (c, a) {
            A = E(c, a);
            return this
        };
        D.setFontStyle = D.setFontType = function (c) {
            A = E(void 0, c);
            return this
        };
        D.getFontList = function () {
            var c = {},
                a, d, e;
            for (a in x)
                if (x.hasOwnProperty(a))
                    for (d in c[a] = e = [], x[a]) x[a].hasOwnProperty(d) && e.push(d);
            return c
        };
        D.setLineWidth = function (c) {
            l((c * r).toFixed(2) + " w");
            return this
        };
        D.setDrawColor = function (c, a, d, e) {
            c = void 0 === a || void 0 === e &&
                c === a === d ? "string" === typeof c ? c + " G" : v(c / 255) + " G" : void 0 === e ? "string" === typeof c ? [c, a, d, "RG"].join(" ") : [v(c / 255), v(a / 255), v(d / 255), "RG"].join(" ") : "string" === typeof c ? [c, a, d, e, "K"].join(" ") : [v(c), v(a), v(d), v(e), "K"].join(" ");
            l(c);
            return this
        };
        D.setFillColor = function (c, a, d, e) {
            c = void 0 === a || void 0 === e && c === a === d ? "string" === typeof c ? c + " g" : v(c / 255) + " g" : void 0 === e ? "string" === typeof c ? [c, a, d, "rg"].join(" ") : [v(c / 255), v(a / 255), v(d / 255), "rg"].join(" ") : "string" === typeof c ? [c, a, d, e, "k"].join(" ") : [v(c),
            v(a), v(d), v(e), "k"
            ].join(" ");
            l(c);
            return this
        };
        D.setTextColor = function (c, a, d) {
            u = 0 === c && 0 === a && 0 === d || "undefined" === typeof a ? (c / 255).toFixed(3) + " g" : [(c / 255).toFixed(3), (a / 255).toFixed(3), (d / 255).toFixed(3), "rg"].join(" ");
            return this
        };
        D.CapJoinStyles = {
            "0": 0,
            butt: 0,
            but: 0,
            bevel: 0,
            1: 1,
            round: 1,
            rounded: 1,
            circle: 1,
            2: 2,
            projecting: 2,
            project: 2,
            square: 2,
            milter: 2
        };
        D.setLineCap = function (c) {
            var a = this.CapJoinStyles[c];
            if (void 0 === a) throw Error("Line cap style of '" + c + "' is not recognized. See or extend .CapJoinStyles property for valid styles");
            w = a;
            l(a.toString(10) + " J");
            return this
        };
        D.setLineJoin = function (c) {
            var a = this.CapJoinStyles[c];
            if (void 0 === a) throw Error("Line join style of '" + c + "' is not recognized. See or extend .CapJoinStyles property for valid styles");
            aa = a;
            l(a.toString(10) + " j");
            return this
        };
        D.output = Z;
        D.save = function (c) {
            D.output("save", c)
        };
        for (B in f.API)
            if (f.API.hasOwnProperty(B))
                if ("events" === B && f.API.events.length) {
                    g = G;
                    d = f.API.events;
                    h = a = e = void 0;
                    for (h = d.length - 1; - 1 !== h; h--) e = d[h][0], a = d[h][1], g.subscribe.apply(g, [e].concat("function" ===
                        typeof a ? [a] : a))
                } else D[B] = f.API[B];
        B = [
            ["Helvetica", "helvetica", "normal"],
            ["Helvetica-Bold", "helvetica", "bold"],
            ["Helvetica-Oblique", "helvetica", "italic"],
            ["Helvetica-BoldOblique", "helvetica", "bolditalic"],
            ["Courier", "courier", "normal"],
            ["Courier-Bold", "courier", "bold"],
            ["Courier-Oblique", "courier", "italic"],
            ["Courier-BoldOblique", "courier", "bolditalic"],
            ["Times-Roman", "times", "normal"],
            ["Times-Bold", "times", "bold"],
            ["Times-Italic", "times", "italic"],
            ["Times-BoldItalic", "times", "bolditalic"]
        ];
        g = 0;
        for (d = B.length; g < d; g++) {
            h = B[g][0];
            var T = B[g][1],
                a = B[g][2];
            e = "F" + (b(n) + 1).toString(10);
            h = n[e] = {
                id: e,
                PostScriptName: h,
                fontName: T,
                fontStyle: a,
                encoding: "StandardEncoding",
                metadata: {}
            };
            var ha = e;
            void 0 === x[T] && (x[T] = {});
            x[T][a] = ha;
            G.publish("addFont", h);
            a = e;
            e = B[g][0].split("-");
            h = e[0];
            e = e[1] || "";
            void 0 === x[h] && (x[h] = {});
            x[h][e] = a
        }
        G.publish("addFonts", {
            fonts: n,
            dictionary: x
        });
        A = "F1";
        W();
        G.publish("initialized");
        return D
    }
    "undefined" === typeof btoa && (window.btoa = function (b) {
        var d = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".split(""),
            e, h, a, p, j = 0,
            m = 0,
            f = "",
            f = [];
        do e = b.charCodeAt(j++), h = b.charCodeAt(j++), a = b.charCodeAt(j++), p = e << 16 | h << 8 | a, e = p >> 18 & 63, h = p >> 12 & 63, a = p >> 6 & 63, p &= 63, f[m++] = d[e] + d[h] + d[a] + d[p]; while (j < b.length);
        f = f.join("");
        b = b.length % 3;
        return (b ? f.slice(0, b - 3) : f) + "===".slice(b || 3)
    });
    "undefined" === typeof atob && (window.atob = function (b) {
        var d, e, h, a, p, j = 0,
            m = 0;
        a = "";
        var f = [];
        if (!b) return b;
        b += "";
        do d = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(b.charAt(j++)), e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(b.charAt(j++)),
            a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(b.charAt(j++)), p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(b.charAt(j++)), h = d << 18 | e << 12 | a << 6 | p, d = h >> 16 & 255, e = h >> 8 & 255, h &= 255, 64 === a ? f[m++] = String.fromCharCode(d) : 64 === p ? f[m++] = String.fromCharCode(d, e) : f[m++] = String.fromCharCode(d, e, h); while (j < b.length);
        return a = f.join("")
    });
    var b = "function" === typeof Object.keys ? function (b) {
        return Object.keys(b).length
    } : function (b) {
        var d = 0,
            e;
        for (e in b) b.hasOwnProperty(e) &&
            d++;
        return d
    },
        k = function (b) {
            this.topics = {};
            this.context = b;
            this.publish = function (d, b) {
                if (this.topics[d]) {
                    var h = this.topics[d],
                        a = [],
                        p, j, g, f, c = function () { };
                    b = Array.prototype.slice.call(arguments, 1);
                    j = 0;
                    for (g = h.length; j < g; j++) f = h[j], p = f[0], f[1] && (f[0] = c, a.push(j)), p.apply(this.context, b);
                    j = 0;
                    for (g = a.length; j < g; j++) h.splice(a[j], 1)
                }
            };
            this.subscribe = function (d, b, h) {
                this.topics[d] ? this.topics[d].push([b, h]) : this.topics[d] = [
                    [b, h]
                ];
                return {
                    topic: d,
                    callback: b
                }
            };
            this.unsubscribe = function (d) {
                if (this.topics[d.topic]) {
                    var b =
                        this.topics[d.topic],
                        h, a;
                    h = 0;
                    for (a = b.length; h < a; h++) b[h][0] === d.callback && b.splice(h, 1)
                }
            }
        };
    f.API = {
        events: []
    };
    return f
}();
(function (f) {
    var b = function () {
        var b = this.internal.collections.addImage_images,
            d;
        for (d in b) {
            var e = b[d],
                h = this.internal.newObject(),
                a = this.internal.write,
                p = this.internal.putStream;
            e.n = h;
            a("<</Type /XObject");
            a("/Subtype /Image");
            a("/Width " + e.w);
            a("/Height " + e.h);
            "Indexed" === e.cs ? a("/ColorSpace [/Indexed /DeviceRGB " + (e.pal.length / 3 - 1) + " " + (h + 1) + " 0 R]") : (a("/ColorSpace /" + e.cs), "DeviceCMYK" === e.cs && a("/Decode [1 0 1 0 1 0 1 0]"));
            a("/BitsPerComponent " + e.bpc);
            "f" in e && a("/Filter /" + e.f);
            "dp" in e &&
                a("/DecodeParms <<" + e.dp + ">>");
            if ("trns" in e && e.trns.constructor == Array)
                for (var j = "", f = 0; f < e.trns.length; f++) j += e[j][f] + " " + e.trns[f] + " ", a("/Mask [" + j + "]");
            "smask" in e && a("/SMask " + (h + 1) + " 0 R");
            a("/Length " + e.data.length + ">>");
            p(e.data);
            a("endobj")
        }
    },
        k = function () {
            var b = this.internal.collections.addImage_images,
                d = this.internal.write,
                e, h;
            for (h in b) e = b[h], d("/I" + e.i, e.n, "0", "R")
        };
    f.addImage = function (g, d, e, h, a, p) {
        if ("object" === typeof g && 1 === g.nodeType) {
            d = document.createElement("canvas");
            d.width = g.clientWidth;
            d.height = g.clientHeight;
            var j = d.getContext("2d");
            if (!j) throw "addImage requires canvas to be supported by browser.";
            j.drawImage(g, 0, 0, d.width, d.height);
            g = d.toDataURL("image/jpeg");
            d = "JPEG"
        }
        if ("JPEG" !== d.toUpperCase()) throw Error("addImage currently only supports format 'JPEG', not '" + d + "'");
        var f;
        d = this.internal.collections.addImage_images;
        var j = this.internal.getCoordinateString,
            u = this.internal.getVerticalCoordinateString;
        "data:image/jpeg;base64," === g.substring(0, 23) && (g = atob(g.replace("data:image/jpeg;base64,",
            "")));
        if (d)
            if (Object.keys) f = Object.keys(d).length;
            else {
                var c = d,
                    q = 0;
                for (f in c) c.hasOwnProperty(f) && q++;
                f = q
            } else f = 0, this.internal.collections.addImage_images = d = {}, this.internal.events.subscribe("putResources", b), this.internal.events.subscribe("putXobjectDict", k);
        a: {
            var c = g,
                C;
            if (255 === !c.charCodeAt(0) || 216 === !c.charCodeAt(1) || 255 === !c.charCodeAt(2) || 224 === !c.charCodeAt(3) || 74 === !c.charCodeAt(6) || 70 === !c.charCodeAt(7) || 73 === !c.charCodeAt(8) || 70 === !c.charCodeAt(9) || 0 === !c.charCodeAt(10)) throw Error("getJpegSize requires a binary jpeg file");
            C = 256 * c.charCodeAt(4) + c.charCodeAt(5);
            for (var q = 4, F = c.length; q < F;) {
                q += C;
                if (255 !== c.charCodeAt(q)) throw Error("getJpegSize could not find the size of the image");
                if (192 === c.charCodeAt(q + 1)) {
                    C = 256 * c.charCodeAt(q + 5) + c.charCodeAt(q + 6);
                    c = 256 * c.charCodeAt(q + 7) + c.charCodeAt(q + 8);
                    c = [c, C];
                    break a
                } else q += 2, C = 256 * c.charCodeAt(q) + c.charCodeAt(q + 1)
            }
            c = void 0
        }
        g = {
            w: c[0],
            h: c[1],
            cs: "DeviceRGB",
            bpc: 8,
            f: "DCTDecode",
            i: f,
            data: g
        };
        d[f] = g;
        !a && !p && (p = a = -96);
        0 > a && (a = -72 * g.w / a / this.internal.scaleFactor);
        0 > p && (p = -72 * g.h / p / this.internal.scaleFactor);
        0 === a && (a = p * g.w / g.h);
        0 === p && (p = a * g.h / g.w);
        this.internal.write("q", j(a), "0 0", j(p), j(e), u(h + p), "cm /I" + g.i, "Do Q");
        return this
    }
})(jsPDF.API);
(function (f) {
    function b(a, b, d, e) {
        this.pdf = a;
        this.x = b;
        this.y = d;
        this.settings = e;
        this.init();
        return this
    }

    function k(b) {
        var d = a[b];
        if (d) return d;
        d = {
            "xx-small": 9,
            "x-small": 11,
            small: 13,
            medium: 16,
            large: 19,
            "x-large": 23,
            "xx-large": 28,
            auto: 0
        }[b];
        if (void 0 !== d || (d = parseFloat(b))) return a[b] = d / 16;
        d = b.match(/([\d\.]+)(px)/);
        return 3 === d.length ? a[b] = parseFloat(d[1]) / 16 : a[b] = 1
    }

    function g(a, b, f) {
        var u = a.childNodes,
            c;
        c = $(a);
        a = {};
        for (var q, C = c.css("font-family").split(","), F = C.shift(); !q && F;) q = d[F.trim().toLowerCase()],
            F = C.shift();
        a["font-family"] = q || "times";
        a["font-style"] = h[c.css("font-style")] || "normal";
        q = e[c.css("font-weight")] || "normal";
        "bold" === q && (a["font-style"] = "normal" === a["font-style"] ? q : q + a["font-style"]);
        a["font-size"] = k(c.css("font-size")) || 1;
        a["line-height"] = k(c.css("line-height")) || 1;
        a.display = "inline" === c.css("display") ? "inline" : "block";
        "block" === a.display && (a["margin-top"] = k(c.css("margin-top")) || 0, a["margin-bottom"] = k(c.css("margin-bottom")) || 0, a["padding-top"] = k(c.css("padding-top")) || 0, a["padding-bottom"] =
            k(c.css("padding-bottom")) || 0);
        if (q = "block" === a.display) b.setBlockBoundary(), b.setBlockStyle(a);
        C = 0;
        for (F = u.length; C < F; C++)
            if (c = u[C], "object" === typeof c)
                if (1 === c.nodeType && "SCRIPT" != c.nodeName) {
                    var H = c,
                        n = b,
                        x = f,
                        z = !1,
                        A = void 0,
                        y = void 0,
                        s = x["#" + H.id];
                    if (s)
                        if ("function" === typeof s) z = s(H, n);
                        else {
                            A = 0;
                            for (y = s.length; !z && A !== y;) z = s[A](H, n), A++
                        }
                    s = x[H.nodeName];
                    if (!z && s)
                        if ("function" === typeof s) z = s(H, n);
                        else {
                            A = 0;
                            for (y = s.length; !z && A !== y;) z = s[A](H, n), A++
                        }
                    z || g(c, b, f)
                } else 3 === c.nodeType && b.addText(c.nodeValue,
                    a);
            else "string" === typeof c && b.addText(c, a);
        q && b.setBlockBoundary()
    }
    String.prototype.trim || (String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, "")
    });
    String.prototype.trimLeft || (String.prototype.trimLeft = function () {
        return this.replace(/^\s+/g, "")
    });
    String.prototype.trimRight || (String.prototype.trimRight = function () {
        return this.replace(/\s+$/g, "")
    });
    b.prototype.init = function () {
        this.paragraph = {
            text: [],
            style: []
        };
        this.pdf.internal.write("q")
    };
    b.prototype.dispose = function () {
        this.pdf.internal.write("Q");
        return {
            x: this.x,
            y: this.y
        }
    };
    b.prototype.splitFragmentsIntoLines = function (a, b) {
        for (var d = this.pdf.internal.scaleFactor, e = {}, c, h, g, f, k, n = [], x = [n], z = 0, A = this.settings.width; a.length;)
            if (f = a.shift(), k = b.shift(), f)
                if (c = k["font-family"], h = k["font-style"], g = e[c + h], g || (g = this.pdf.internal.getFont(c, h).metadata.Unicode, e[c + h] = g), c = {
                    widths: g.widths,
                    kerning: g.kerning,
                    fontSize: 12 * k["font-size"],
                    textIndent: z
                }, h = this.pdf.getStringUnitWidth(f, c) * c.fontSize / d, z + h > A) {
                    f = this.pdf.splitTextToSize(f, A, c);
                    for (n.push([f.shift(),
                        k
                    ]); f.length;) n = [
                        [f.shift(), k]
                    ], x.push(n);
                    z = this.pdf.getStringUnitWidth(n[0][0], c) * c.fontSize / d
                } else n.push([f, k]), z += h;
        return x
    };
    b.prototype.RenderTextFragment = function (a, b) {
        var d = this.pdf.internal.getFont(b["font-family"], b["font-style"]);
        this.pdf.internal.write("/" + d.id, (12 * b["font-size"]).toFixed(2), "Tf", "(" + this.pdf.internal.pdfEscape(a) + ") Tj")
    };
    b.prototype.renderParagraph = function () {
        for (var a = this.paragraph.text, b = 0, d = a.length, e, c = !1, h = !1; !c && b !== d;)(e = a[b] = a[b].trimLeft()) && (c = !0), b++;
        for (b =
            d - 1; d && !h && -1 !== b;)(e = a[b] = a[b].trimRight()) && (h = !0), b--;
        c = /\s+$/g;
        h = !0;
        for (b = 0; b !== d; b++) e = a[b].replace(/\s+/g, " "), h && (e = e.trimLeft()), e && (h = c.test(e)), a[b] = e;
        b = this.paragraph.style;
        e = (d = this.paragraph.blockstyle) || {};
        this.paragraph = {
            text: [],
            style: [],
            blockstyle: {},
            priorblockstyle: d
        };
        if (a.join("").trim()) {
            a = this.splitFragmentsIntoLines(a, b);
            b = 12 / this.pdf.internal.scaleFactor;
            c = (Math.max((d["margin-top"] || 0) - (e["margin-bottom"] || 0), 0) + (d["padding-top"] || 0)) * b;
            d = ((d["margin-bottom"] || 0) + (d["padding-bottom"] ||
                0)) * b;
            e = this.pdf.internal.write;
            var g, f;
            this.y += c;
            for (e("q", "BT", this.pdf.internal.getCoordinateString(this.x), this.pdf.internal.getVerticalCoordinateString(this.y), "Td"); a.length;) {
                c = a.shift();
                g = h = 0;
                for (f = c.length; g !== f; g++) c[g][0].trim() && (h = Math.max(h, c[g][1]["line-height"], c[g][1]["font-size"]));
                e(0, (-12 * h).toFixed(2), "Td");
                g = 0;
                for (f = c.length; g !== f; g++) c[g][0] && this.RenderTextFragment(c[g][0], c[g][1]);
                this.y += h * b
            }
            e("ET", "Q");
            this.y += d
        }
    };
    b.prototype.setBlockBoundary = function () {
        this.renderParagraph()
    };
    b.prototype.setBlockStyle = function (a) {
        this.paragraph.blockstyle = a
    };
    b.prototype.addText = function (a, b) {
        this.paragraph.text.push(a);
        this.paragraph.style.push(b)
    };
    var d = {
        helvetica: "helvetica",
        "sans-serif": "helvetica",
        serif: "times",
        times: "times",
        "times new roman": "times",
        monospace: "courier",
        courier: "courier"
    },
        e = {
            100: "normal",
            200: "normal",
            300: "normal",
            400: "normal",
            500: "bold",
            600: "bold",
            700: "bold",
            800: "bold",
            900: "bold",
            normal: "normal",
            bold: "bold",
            bolder: "bold",
            lighter: "normal"
        },
        h = {
            normal: "normal",
            italic: "italic",
            oblique: "italic"
        },
        a = {
            normal: 1
        };
    f.fromHTML = function (a, d, e, h) {
        if ("string" === typeof a) {
            var c = "jsPDFhtmlText" + Date.now().toString() + (1E3 * Math.random()).toFixed(0);
            $('<div style="position: absolute !important;clip: rect(1px 1px 1px 1px); /* IE6, IE7 */clip: rect(1px, 1px, 1px, 1px);padding:0 !important;border:0 !important;height: 1px !important;width: 1px !important; top:auto;left:-100px;overflow: hidden;"><iframe style="height:1px;width:1px" name="' + c + '" /></div>').appendTo(document.body);
            a = $(window.frames[c].document.body).html(a)[0]
        }
        d =
            new b(this, d, e, h);
        g(a, d, h.elementHandlers);
        return d.dispose()
    }
})(jsPDF.API);
(function (f) {
    f.addSVG = function (b, f, g, d, e) {
        if (void 0 === f || void 0 === f) throw Error("addSVG needs values for 'x' and 'y'");
        var h = document.createElement("iframe"),
            a = document.createElement("style");
        a.type = "text/css";
        a.styleSheet ? a.styleSheet.cssText = ".jsPDF_sillysvg_iframe {display:none;position:absolute;}" : a.appendChild(document.createTextNode(".jsPDF_sillysvg_iframe {display:none;position:absolute;}"));
        document.getElementsByTagName("head")[0].appendChild(a);
        h.name = "childframe";
        h.setAttribute("width",
            0);
        h.setAttribute("height", 0);
        h.setAttribute("frameborder", "0");
        h.setAttribute("scrolling", "no");
        h.setAttribute("seamless", "seamless");
        h.setAttribute("class", "jsPDF_sillysvg_iframe");
        document.body.appendChild(h);
        h = (h.contentWindow || h.contentDocument).document;
        h.write(b);
        h.close();
        h = h.getElementsByTagName("svg")[0];
        b = [1, 1];
        var a = parseFloat(h.getAttribute("width")),
            p = parseFloat(h.getAttribute("height"));
        a && p && (d && e ? b = [d / a, e / p] : d ? b = [d / a, d / a] : e && (b = [e / p, e / p]));
        h = h.childNodes;
        d = 0;
        for (e = h.length; d < e; d++)
            if (a =
                h[d], a.tagName && "PATH" === a.tagName.toUpperCase()) {
                for (var a = a.getAttribute("d").split(" "), p = parseFloat(a[1]), j = parseFloat(a[2]), m = [], u = 3, c = a.length; u < c;) "c" === a[u] ? (m.push([parseFloat(a[u + 1]), parseFloat(a[u + 2]), parseFloat(a[u + 3]), parseFloat(a[u + 4]), parseFloat(a[u + 5]), parseFloat(a[u + 6])]), u += 7) : "l" === a[u] ? (m.push([parseFloat(a[u + 1]), parseFloat(a[u + 2])]), u += 3) : u += 1;
                a = [p, j, m];
                a[0] = a[0] * b[0] + f;
                a[1] = a[1] * b[1] + g;
                this.lines.call(this, a[2], a[0], a[1], b)
            }
        return this
    }
})(jsPDF.API);
(function (f) {
    var b = f.getCharWidthsArray = function (b, e) {
        e || (e = {});
        var h = e.widths ? e.widths : this.internal.getFont().metadata.Unicode.widths,
            a = h.fof ? h.fof : 1,
            g = e.kerning ? e.kerning : this.internal.getFont().metadata.Unicode.kerning,
            f = g.fof ? g.fof : 1,
            m, k, c, q = 0,
            C = h[0] || a,
            F = [];
        m = 0;
        for (k = b.length; m < k; m++) c = b.charCodeAt(m), F.push((h[c] || C) / a + (g[c] && g[c][q] || 0) / f), q = c;
        return F
    },
        k = function (b) {
            for (var e = b.length, h = 0; e;) e-- , h += b[e];
            return h
        };
    f.getStringUnitWidth = function (d, e) {
        return k(b.call(this, d, e))
    };
    var g = function (d,
        e, h) {
        h || (h = {});
        var a = b(" ", h)[0],
            g = d.split(" "),
            f = [];
        d = [f];
        var m = h.textIndent || 0,
            u = 0,
            c = 0,
            q, C, F, H;
        F = 0;
        for (H = g.length; F < H; F++) {
            q = g[F];
            C = b(q, h);
            c = k(C);
            if (m + u + c > e) {
                if (c > e) {
                    for (var c = q, n = C, x = e, z = [], A = 0, y = c.length, s = 0; A !== y && s + n[A] < e - (m + u);) s += n[A], A++;
                    z.push(c.slice(0, A));
                    m = A;
                    for (s = 0; A !== y;) s + n[A] > x && (z.push(c.slice(m, A)), s = 0, m = A), s += n[A], A++;
                    m !== A && z.push(c.slice(m, A));
                    m = z;
                    f.push(m.shift());
                    for (f = [m.pop()]; m.length;) d.push([m.shift()]);
                    c = k(C.slice(q.length - f[0].length))
                } else f = [q];
                d.push(f);
                m = c
            } else f.push(q),
                m += u + c;
            u = a
        }
        e = [];
        F = 0;
        for (H = d.length; F < H; F++) e.push(d[F].join(" "));
        return e
    };
    f.splitTextToSize = function (b, e, h) {
        h || (h = {});
        var a = h.fontSize || this.internal.getFontSize(),
            f;
        var j = h;
        f = {
            "0": 1
        };
        var m = {};
        !j.widths || !j.kerning ? (j = this.internal.getFont(j.fontName, j.fontStyle), f = j.metadata.Unicode ? {
            widths: j.metadata.Unicode.widths || f,
            kerning: j.metadata.Unicode.kerning || m
        } : {
                widths: f,
                kerning: m
            }) : f = {
                widths: j.widths,
                kerning: j.kerning
            };
        b = b.match(/[\n\r]/) ? b.split(/\r\n|\r|\n/g) : [b];
        e = 1 * this.internal.scaleFactor * e /
            a;
        f.textIndent = h.textIndent ? 1 * h.textIndent * this.internal.scaleFactor / a : 0;
        m = [];
        h = 0;
        for (a = b.length; h < a; h++) m = m.concat(g(b[h], e, f));
        return m
    }
})(jsPDF.API);
(function (f) {
    var b = function (b) {
        for (var d = {}, a = 0; 16 > a; a++) d["klmnopqrstuvwxyz"[a]] = "0123456789abcdef"[a];
        for (var g = {}, f = 1, m, k = g, c = [], q, C = "", F = "", H, n = b.length - 1, a = 1; a != n;) q = b[a], a += 1, "'" == q ? m ? (H = m.join(""), m = void 0) : m = [] : m ? m.push(q) : "{" == q ? (c.push([k, H]), k = {}, H = void 0) : "}" == q ? (q = c.pop(), q[0][q[1]] = k, H = void 0, k = q[0]) : "-" == q ? f = -1 : void 0 === H ? d.hasOwnProperty(q) ? (C += d[q], H = parseInt(C, 16) * f, f = 1, C = "") : C += q : d.hasOwnProperty(q) ? (F += d[q], k[H] = parseInt(F, 16) * f, f = 1, H = void 0, F = "") : F += q;
        return g
    },
        k = {
            codePages: ["WinAnsiEncoding"],
            WinAnsiEncoding: b("{19m8n201n9q201o9r201s9l201t9m201u8m201w9n201x9o201y8o202k8q202l8r202m9p202q8p20aw8k203k8t203t8v203u9v2cq8s212m9t15m8w15n9w2dw9s16k8u16l9u17s9z17x8y17y9y}")
        },
        g = {
            Unicode: {
                Courier: k,
                "Courier-Bold": k,
                "Courier-BoldOblique": k,
                "Courier-Oblique": k,
                Helvetica: k,
                "Helvetica-Bold": k,
                "Helvetica-BoldOblique": k,
                "Helvetica-Oblique": k,
                "Times-Roman": k,
                "Times-Bold": k,
                "Times-BoldItalic": k,
                "Times-Italic": k
            }
        },
        d = {
            Unicode: {
                "Courier-Oblique": b("{'widths'{k3w'fof'6o}'kerning'{'fof'-6o}}"),
                "Times-BoldItalic": b("{'widths'{k3o2q4ycx2r201n3m201o6o201s2l201t2l201u2l201w3m201x3m201y3m2k1t2l2r202m2n2n3m2o3m2p5n202q6o2r1w2s2l2t2l2u3m2v3t2w1t2x2l2y1t2z1w3k3m3l3m3m3m3n3m3o3m3p3m3q3m3r3m3s3m203t2l203u2l3v2l3w3t3x3t3y3t3z3m4k5n4l4m4m4m4n4m4o4s4p4m4q4m4r4s4s4y4t2r4u3m4v4m4w3x4x5t4y4s4z4s5k3x5l4s5m4m5n3r5o3x5p4s5q4m5r5t5s4m5t3x5u3x5v2l5w1w5x2l5y3t5z3m6k2l6l3m6m3m6n2w6o3m6p2w6q2l6r3m6s3r6t1w6u1w6v3m6w1w6x4y6y3r6z3m7k3m7l3m7m2r7n2r7o1w7p3r7q2w7r4m7s3m7t2w7u2r7v2n7w1q7x2n7y3t202l3mcl4mal2ram3man3mao3map3mar3mas2lat4uau1uav3maw3way4uaz2lbk2sbl3t'fof'6obo2lbp3tbq3mbr1tbs2lbu1ybv3mbz3mck4m202k3mcm4mcn4mco4mcp4mcq5ycr4mcs4mct4mcu4mcv4mcw2r2m3rcy2rcz2rdl4sdm4sdn4sdo4sdp4sdq4sds4sdt4sdu4sdv4sdw4sdz3mek3mel3mem3men3meo3mep3meq4ser2wes2wet2weu2wev2wew1wex1wey1wez1wfl3rfm3mfn3mfo3mfp3mfq3mfr3tfs3mft3rfu3rfv3rfw3rfz2w203k6o212m6o2dw2l2cq2l3t3m3u2l17s3x19m3m}'kerning'{cl{4qu5kt5qt5rs17ss5ts}201s{201ss}201t{cks4lscmscnscoscpscls2wu2yu201ts}201x{2wu2yu}2k{201ts}2w{4qx5kx5ou5qx5rs17su5tu}2x{17su5tu5ou}2y{4qx5kx5ou5qx5rs17ss5ts}'fof'-6ofn{17sw5tw5ou5qw5rs}7t{cksclscmscnscoscps4ls}3u{17su5tu5os5qs}3v{17su5tu5os5qs}7p{17su5tu}ck{4qu5kt5qt5rs17ss5ts}4l{4qu5kt5qt5rs17ss5ts}cm{4qu5kt5qt5rs17ss5ts}cn{4qu5kt5qt5rs17ss5ts}co{4qu5kt5qt5rs17ss5ts}cp{4qu5kt5qt5rs17ss5ts}6l{4qu5ou5qw5rt17su5tu}5q{ckuclucmucnucoucpu4lu}5r{ckuclucmucnucoucpu4lu}7q{cksclscmscnscoscps4ls}6p{4qu5ou5qw5rt17sw5tw}ek{4qu5ou5qw5rt17su5tu}el{4qu5ou5qw5rt17su5tu}em{4qu5ou5qw5rt17su5tu}en{4qu5ou5qw5rt17su5tu}eo{4qu5ou5qw5rt17su5tu}ep{4qu5ou5qw5rt17su5tu}es{17ss5ts5qs4qu}et{4qu5ou5qw5rt17sw5tw}eu{4qu5ou5qw5rt17ss5ts}ev{17ss5ts5qs4qu}6z{17sw5tw5ou5qw5rs}fm{17sw5tw5ou5qw5rs}7n{201ts}fo{17sw5tw5ou5qw5rs}fp{17sw5tw5ou5qw5rs}fq{17sw5tw5ou5qw5rs}7r{cksclscmscnscoscps4ls}fs{17sw5tw5ou5qw5rs}ft{17su5tu}fu{17su5tu}fv{17su5tu}fw{17su5tu}fz{cksclscmscnscoscps4ls}}}"),
                "Helvetica-Bold": b("{'widths'{k3s2q4scx1w201n3r201o6o201s1w201t1w201u1w201w3m201x3m201y3m2k1w2l2l202m2n2n3r2o3r2p5t202q6o2r1s2s2l2t2l2u2r2v3u2w1w2x2l2y1w2z1w3k3r3l3r3m3r3n3r3o3r3p3r3q3r3r3r3s3r203t2l203u2l3v2l3w3u3x3u3y3u3z3x4k6l4l4s4m4s4n4s4o4s4p4m4q3x4r4y4s4s4t1w4u3r4v4s4w3x4x5n4y4s4z4y5k4m5l4y5m4s5n4m5o3x5p4s5q4m5r5y5s4m5t4m5u3x5v2l5w1w5x2l5y3u5z3r6k2l6l3r6m3x6n3r6o3x6p3r6q2l6r3x6s3x6t1w6u1w6v3r6w1w6x5t6y3x6z3x7k3x7l3x7m2r7n3r7o2l7p3x7q3r7r4y7s3r7t3r7u3m7v2r7w1w7x2r7y3u202l3rcl4sal2lam3ran3rao3rap3rar3ras2lat4tau2pav3raw3uay4taz2lbk2sbl3u'fof'6obo2lbp3xbq3rbr1wbs2lbu2obv3rbz3xck4s202k3rcm4scn4sco4scp4scq6ocr4scs4mct4mcu4mcv4mcw1w2m2zcy1wcz1wdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3xek3rel3rem3ren3reo3rep3req5ter3res3ret3reu3rev3rew1wex1wey1wez1wfl3xfm3xfn3xfo3xfp3xfq3xfr3ufs3xft3xfu3xfv3xfw3xfz3r203k6o212m6o2dw2l2cq2l3t3r3u2l17s4m19m3r}'kerning'{cl{4qs5ku5ot5qs17sv5tv}201t{2ww4wy2yw}201w{2ks}201x{2ww4wy2yw}2k{201ts201xs}2w{7qs4qu5kw5os5qw5rs17su5tu7tsfzs}2x{5ow5qs}2y{7qs4qu5kw5os5qw5rs17su5tu7tsfzs}'fof'-6o7p{17su5tu5ot}ck{4qs5ku5ot5qs17sv5tv}4l{4qs5ku5ot5qs17sv5tv}cm{4qs5ku5ot5qs17sv5tv}cn{4qs5ku5ot5qs17sv5tv}co{4qs5ku5ot5qs17sv5tv}cp{4qs5ku5ot5qs17sv5tv}6l{17st5tt5os}17s{2kwclvcmvcnvcovcpv4lv4wwckv}5o{2kucltcmtcntcotcpt4lt4wtckt}5q{2ksclscmscnscoscps4ls4wvcks}5r{2ks4ws}5t{2kwclvcmvcnvcovcpv4lv4wwckv}eo{17st5tt5os}fu{17su5tu5ot}6p{17ss5ts}ek{17st5tt5os}el{17st5tt5os}em{17st5tt5os}en{17st5tt5os}6o{201ts}ep{17st5tt5os}es{17ss5ts}et{17ss5ts}eu{17ss5ts}ev{17ss5ts}6z{17su5tu5os5qt}fm{17su5tu5os5qt}fn{17su5tu5os5qt}fo{17su5tu5os5qt}fp{17su5tu5os5qt}fq{17su5tu5os5qt}fs{17su5tu5os5qt}ft{17su5tu5ot}7m{5os}fv{17su5tu5ot}fw{17su5tu5ot}}}"),
                Courier: b("{'widths'{k3w'fof'6o}'kerning'{'fof'-6o}}"),
                "Courier-BoldOblique": b("{'widths'{k3w'fof'6o}'kerning'{'fof'-6o}}"),
                "Times-Bold": b("{'widths'{k3q2q5ncx2r201n3m201o6o201s2l201t2l201u2l201w3m201x3m201y3m2k1t2l2l202m2n2n3m2o3m2p6o202q6o2r1w2s2l2t2l2u3m2v3t2w1t2x2l2y1t2z1w3k3m3l3m3m3m3n3m3o3m3p3m3q3m3r3m3s3m203t2l203u2l3v2l3w3t3x3t3y3t3z3m4k5x4l4s4m4m4n4s4o4s4p4m4q3x4r4y4s4y4t2r4u3m4v4y4w4m4x5y4y4s4z4y5k3x5l4y5m4s5n3r5o4m5p4s5q4s5r6o5s4s5t4s5u4m5v2l5w1w5x2l5y3u5z3m6k2l6l3m6m3r6n2w6o3r6p2w6q2l6r3m6s3r6t1w6u2l6v3r6w1w6x5n6y3r6z3m7k3r7l3r7m2w7n2r7o2l7p3r7q3m7r4s7s3m7t3m7u2w7v2r7w1q7x2r7y3o202l3mcl4sal2lam3man3mao3map3mar3mas2lat4uau1yav3maw3tay4uaz2lbk2sbl3t'fof'6obo2lbp3rbr1tbs2lbu2lbv3mbz3mck4s202k3mcm4scn4sco4scp4scq6ocr4scs4mct4mcu4mcv4mcw2r2m3rcy2rcz2rdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3rek3mel3mem3men3meo3mep3meq4ser2wes2wet2weu2wev2wew1wex1wey1wez1wfl3rfm3mfn3mfo3mfp3mfq3mfr3tfs3mft3rfu3rfv3rfw3rfz3m203k6o212m6o2dw2l2cq2l3t3m3u2l17s4s19m3m}'kerning'{cl{4qt5ks5ot5qy5rw17sv5tv}201t{cks4lscmscnscoscpscls4wv}2k{201ts}2w{4qu5ku7mu5os5qx5ru17su5tu}2x{17su5tu5ou5qs}2y{4qv5kv7mu5ot5qz5ru17su5tu}'fof'-6o7t{cksclscmscnscoscps4ls}3u{17su5tu5os5qu}3v{17su5tu5os5qu}fu{17su5tu5ou5qu}7p{17su5tu5ou5qu}ck{4qt5ks5ot5qy5rw17sv5tv}4l{4qt5ks5ot5qy5rw17sv5tv}cm{4qt5ks5ot5qy5rw17sv5tv}cn{4qt5ks5ot5qy5rw17sv5tv}co{4qt5ks5ot5qy5rw17sv5tv}cp{4qt5ks5ot5qy5rw17sv5tv}6l{17st5tt5ou5qu}17s{ckuclucmucnucoucpu4lu4wu}5o{ckuclucmucnucoucpu4lu4wu}5q{ckzclzcmzcnzcozcpz4lz4wu}5r{ckxclxcmxcnxcoxcpx4lx4wu}5t{ckuclucmucnucoucpu4lu4wu}7q{ckuclucmucnucoucpu4lu}6p{17sw5tw5ou5qu}ek{17st5tt5qu}el{17st5tt5ou5qu}em{17st5tt5qu}en{17st5tt5qu}eo{17st5tt5qu}ep{17st5tt5ou5qu}es{17ss5ts5qu}et{17sw5tw5ou5qu}eu{17sw5tw5ou5qu}ev{17ss5ts5qu}6z{17sw5tw5ou5qu5rs}fm{17sw5tw5ou5qu5rs}fn{17sw5tw5ou5qu5rs}fo{17sw5tw5ou5qu5rs}fp{17sw5tw5ou5qu5rs}fq{17sw5tw5ou5qu5rs}7r{cktcltcmtcntcotcpt4lt5os}fs{17sw5tw5ou5qu5rs}ft{17su5tu5ou5qu}7m{5os}fv{17su5tu5ou5qu}fw{17su5tu5ou5qu}fz{cksclscmscnscoscps4ls}}}"),
                Helvetica: b("{'widths'{k3p2q4mcx1w201n3r201o6o201s1q201t1q201u1q201w2l201x2l201y2l2k1w2l1w202m2n2n3r2o3r2p5t202q6o2r1n2s2l2t2l2u2r2v3u2w1w2x2l2y1w2z1w3k3r3l3r3m3r3n3r3o3r3p3r3q3r3r3r3s3r203t2l203u2l3v1w3w3u3x3u3y3u3z3r4k6p4l4m4m4m4n4s4o4s4p4m4q3x4r4y4s4s4t1w4u3m4v4m4w3r4x5n4y4s4z4y5k4m5l4y5m4s5n4m5o3x5p4s5q4m5r5y5s4m5t4m5u3x5v1w5w1w5x1w5y2z5z3r6k2l6l3r6m3r6n3m6o3r6p3r6q1w6r3r6s3r6t1q6u1q6v3m6w1q6x5n6y3r6z3r7k3r7l3r7m2l7n3m7o1w7p3r7q3m7r4s7s3m7t3m7u3m7v2l7w1u7x2l7y3u202l3rcl4mal2lam3ran3rao3rap3rar3ras2lat4tau2pav3raw3uay4taz2lbk2sbl3u'fof'6obo2lbp3rbr1wbs2lbu2obv3rbz3xck4m202k3rcm4mcn4mco4mcp4mcq6ocr4scs4mct4mcu4mcv4mcw1w2m2ncy1wcz1wdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3xek3rel3rem3ren3reo3rep3req5ter3mes3ret3reu3rev3rew1wex1wey1wez1wfl3rfm3rfn3rfo3rfp3rfq3rfr3ufs3xft3rfu3rfv3rfw3rfz3m203k6o212m6o2dw2l2cq2l3t3r3u1w17s4m19m3r}'kerning'{5q{4wv}cl{4qs5kw5ow5qs17sv5tv}201t{2wu4w1k2yu}201x{2wu4wy2yu}17s{2ktclucmucnu4otcpu4lu4wycoucku}2w{7qs4qz5k1m17sy5ow5qx5rsfsu5ty7tufzu}2x{17sy5ty5oy5qs}2y{7qs4qz5k1m17sy5ow5qx5rsfsu5ty7tufzu}'fof'-6o7p{17sv5tv5ow}ck{4qs5kw5ow5qs17sv5tv}4l{4qs5kw5ow5qs17sv5tv}cm{4qs5kw5ow5qs17sv5tv}cn{4qs5kw5ow5qs17sv5tv}co{4qs5kw5ow5qs17sv5tv}cp{4qs5kw5ow5qs17sv5tv}6l{17sy5ty5ow}do{17st5tt}4z{17st5tt}7s{fst}dm{17st5tt}dn{17st5tt}5o{ckwclwcmwcnwcowcpw4lw4wv}dp{17st5tt}dq{17st5tt}7t{5ow}ds{17st5tt}5t{2ktclucmucnu4otcpu4lu4wycoucku}fu{17sv5tv5ow}6p{17sy5ty5ow5qs}ek{17sy5ty5ow}el{17sy5ty5ow}em{17sy5ty5ow}en{5ty}eo{17sy5ty5ow}ep{17sy5ty5ow}es{17sy5ty5qs}et{17sy5ty5ow5qs}eu{17sy5ty5ow5qs}ev{17sy5ty5ow5qs}6z{17sy5ty5ow5qs}fm{17sy5ty5ow5qs}fn{17sy5ty5ow5qs}fo{17sy5ty5ow5qs}fp{17sy5ty5qs}fq{17sy5ty5ow5qs}7r{5ow}fs{17sy5ty5ow5qs}ft{17sv5tv5ow}7m{5ow}fv{17sv5tv5ow}fw{17sv5tv5ow}}}"),
                "Helvetica-BoldOblique": b("{'widths'{k3s2q4scx1w201n3r201o6o201s1w201t1w201u1w201w3m201x3m201y3m2k1w2l2l202m2n2n3r2o3r2p5t202q6o2r1s2s2l2t2l2u2r2v3u2w1w2x2l2y1w2z1w3k3r3l3r3m3r3n3r3o3r3p3r3q3r3r3r3s3r203t2l203u2l3v2l3w3u3x3u3y3u3z3x4k6l4l4s4m4s4n4s4o4s4p4m4q3x4r4y4s4s4t1w4u3r4v4s4w3x4x5n4y4s4z4y5k4m5l4y5m4s5n4m5o3x5p4s5q4m5r5y5s4m5t4m5u3x5v2l5w1w5x2l5y3u5z3r6k2l6l3r6m3x6n3r6o3x6p3r6q2l6r3x6s3x6t1w6u1w6v3r6w1w6x5t6y3x6z3x7k3x7l3x7m2r7n3r7o2l7p3x7q3r7r4y7s3r7t3r7u3m7v2r7w1w7x2r7y3u202l3rcl4sal2lam3ran3rao3rap3rar3ras2lat4tau2pav3raw3uay4taz2lbk2sbl3u'fof'6obo2lbp3xbq3rbr1wbs2lbu2obv3rbz3xck4s202k3rcm4scn4sco4scp4scq6ocr4scs4mct4mcu4mcv4mcw1w2m2zcy1wcz1wdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3xek3rel3rem3ren3reo3rep3req5ter3res3ret3reu3rev3rew1wex1wey1wez1wfl3xfm3xfn3xfo3xfp3xfq3xfr3ufs3xft3xfu3xfv3xfw3xfz3r203k6o212m6o2dw2l2cq2l3t3r3u2l17s4m19m3r}'kerning'{cl{4qs5ku5ot5qs17sv5tv}201t{2ww4wy2yw}201w{2ks}201x{2ww4wy2yw}2k{201ts201xs}2w{7qs4qu5kw5os5qw5rs17su5tu7tsfzs}2x{5ow5qs}2y{7qs4qu5kw5os5qw5rs17su5tu7tsfzs}'fof'-6o7p{17su5tu5ot}ck{4qs5ku5ot5qs17sv5tv}4l{4qs5ku5ot5qs17sv5tv}cm{4qs5ku5ot5qs17sv5tv}cn{4qs5ku5ot5qs17sv5tv}co{4qs5ku5ot5qs17sv5tv}cp{4qs5ku5ot5qs17sv5tv}6l{17st5tt5os}17s{2kwclvcmvcnvcovcpv4lv4wwckv}5o{2kucltcmtcntcotcpt4lt4wtckt}5q{2ksclscmscnscoscps4ls4wvcks}5r{2ks4ws}5t{2kwclvcmvcnvcovcpv4lv4wwckv}eo{17st5tt5os}fu{17su5tu5ot}6p{17ss5ts}ek{17st5tt5os}el{17st5tt5os}em{17st5tt5os}en{17st5tt5os}6o{201ts}ep{17st5tt5os}es{17ss5ts}et{17ss5ts}eu{17ss5ts}ev{17ss5ts}6z{17su5tu5os5qt}fm{17su5tu5os5qt}fn{17su5tu5os5qt}fo{17su5tu5os5qt}fp{17su5tu5os5qt}fq{17su5tu5os5qt}fs{17su5tu5os5qt}ft{17su5tu5ot}7m{5os}fv{17su5tu5ot}fw{17su5tu5ot}}}"),
                "Courier-Bold": b("{'widths'{k3w'fof'6o}'kerning'{'fof'-6o}}"),
                "Times-Italic": b("{'widths'{k3n2q4ycx2l201n3m201o5t201s2l201t2l201u2l201w3r201x3r201y3r2k1t2l2l202m2n2n3m2o3m2p5n202q5t2r1p2s2l2t2l2u3m2v4n2w1t2x2l2y1t2z1w3k3m3l3m3m3m3n3m3o3m3p3m3q3m3r3m3s3m203t2l203u2l3v2l3w4n3x4n3y4n3z3m4k5w4l3x4m3x4n4m4o4s4p3x4q3x4r4s4s4s4t2l4u2w4v4m4w3r4x5n4y4m4z4s5k3x5l4s5m3x5n3m5o3r5p4s5q3x5r5n5s3x5t3r5u3r5v2r5w1w5x2r5y2u5z3m6k2l6l3m6m3m6n2w6o3m6p2w6q1w6r3m6s3m6t1w6u1w6v2w6w1w6x4s6y3m6z3m7k3m7l3m7m2r7n2r7o1w7p3m7q2w7r4m7s2w7t2w7u2r7v2s7w1v7x2s7y3q202l3mcl3xal2ram3man3mao3map3mar3mas2lat4wau1vav3maw4nay4waz2lbk2sbl4n'fof'6obo2lbp3mbq3obr1tbs2lbu1zbv3mbz3mck3x202k3mcm3xcn3xco3xcp3xcq5tcr4mcs3xct3xcu3xcv3xcw2l2m2ucy2lcz2ldl4mdm4sdn4sdo4sdp4sdq4sds4sdt4sdu4sdv4sdw4sdz3mek3mel3mem3men3meo3mep3meq4mer2wes2wet2weu2wev2wew1wex1wey1wez1wfl3mfm3mfn3mfo3mfp3mfq3mfr4nfs3mft3mfu3mfv3mfw3mfz2w203k6o212m6m2dw2l2cq2l3t3m3u2l17s3r19m3m}'kerning'{cl{5kt4qw}201s{201sw}201t{201tw2wy2yy6q-t}201x{2wy2yy}2k{201tw}2w{7qs4qy7rs5ky7mw5os5qx5ru17su5tu}2x{17ss5ts5os}2y{7qs4qy7rs5ky7mw5os5qx5ru17su5tu}'fof'-6o6t{17ss5ts5qs}7t{5os}3v{5qs}7p{17su5tu5qs}ck{5kt4qw}4l{5kt4qw}cm{5kt4qw}cn{5kt4qw}co{5kt4qw}cp{5kt4qw}6l{4qs5ks5ou5qw5ru17su5tu}17s{2ks}5q{ckvclvcmvcnvcovcpv4lv}5r{ckuclucmucnucoucpu4lu}5t{2ks}6p{4qs5ks5ou5qw5ru17su5tu}ek{4qs5ks5ou5qw5ru17su5tu}el{4qs5ks5ou5qw5ru17su5tu}em{4qs5ks5ou5qw5ru17su5tu}en{4qs5ks5ou5qw5ru17su5tu}eo{4qs5ks5ou5qw5ru17su5tu}ep{4qs5ks5ou5qw5ru17su5tu}es{5ks5qs4qs}et{4qs5ks5ou5qw5ru17su5tu}eu{4qs5ks5qw5ru17su5tu}ev{5ks5qs4qs}ex{17ss5ts5qs}6z{4qv5ks5ou5qw5ru17su5tu}fm{4qv5ks5ou5qw5ru17su5tu}fn{4qv5ks5ou5qw5ru17su5tu}fo{4qv5ks5ou5qw5ru17su5tu}fp{4qv5ks5ou5qw5ru17su5tu}fq{4qv5ks5ou5qw5ru17su5tu}7r{5os}fs{4qv5ks5ou5qw5ru17su5tu}ft{17su5tu5qs}fu{17su5tu5qs}fv{17su5tu5qs}fw{17su5tu5qs}}}"),
                "Times-Roman": b("{'widths'{k3n2q4ycx2l201n3m201o6o201s2l201t2l201u2l201w2w201x2w201y2w2k1t2l2l202m2n2n3m2o3m2p5n202q6o2r1m2s2l2t2l2u3m2v3s2w1t2x2l2y1t2z1w3k3m3l3m3m3m3n3m3o3m3p3m3q3m3r3m3s3m203t2l203u2l3v1w3w3s3x3s3y3s3z2w4k5w4l4s4m4m4n4m4o4s4p3x4q3r4r4s4s4s4t2l4u2r4v4s4w3x4x5t4y4s4z4s5k3r5l4s5m4m5n3r5o3x5p4s5q4s5r5y5s4s5t4s5u3x5v2l5w1w5x2l5y2z5z3m6k2l6l2w6m3m6n2w6o3m6p2w6q2l6r3m6s3m6t1w6u1w6v3m6w1w6x4y6y3m6z3m7k3m7l3m7m2l7n2r7o1w7p3m7q3m7r4s7s3m7t3m7u2w7v3k7w1o7x3k7y3q202l3mcl4sal2lam3man3mao3map3mar3mas2lat4wau1vav3maw3say4waz2lbk2sbl3s'fof'6obo2lbp3mbq2xbr1tbs2lbu1zbv3mbz2wck4s202k3mcm4scn4sco4scp4scq5tcr4mcs3xct3xcu3xcv3xcw2l2m2tcy2lcz2ldl4sdm4sdn4sdo4sdp4sdq4sds4sdt4sdu4sdv4sdw4sdz3mek2wel2wem2wen2weo2wep2weq4mer2wes2wet2weu2wev2wew1wex1wey1wez1wfl3mfm3mfn3mfo3mfp3mfq3mfr3sfs3mft3mfu3mfv3mfw3mfz3m203k6o212m6m2dw2l2cq2l3t3m3u1w17s4s19m3m}'kerning'{cl{4qs5ku17sw5ou5qy5rw201ss5tw201ws}201s{201ss}201t{ckw4lwcmwcnwcowcpwclw4wu201ts}2k{201ts}2w{4qs5kw5os5qx5ru17sx5tx}2x{17sw5tw5ou5qu}2y{4qs5kw5os5qx5ru17sx5tx}'fof'-6o7t{ckuclucmucnucoucpu4lu5os5rs}3u{17su5tu5qs}3v{17su5tu5qs}7p{17sw5tw5qs}ck{4qs5ku17sw5ou5qy5rw201ss5tw201ws}4l{4qs5ku17sw5ou5qy5rw201ss5tw201ws}cm{4qs5ku17sw5ou5qy5rw201ss5tw201ws}cn{4qs5ku17sw5ou5qy5rw201ss5tw201ws}co{4qs5ku17sw5ou5qy5rw201ss5tw201ws}cp{4qs5ku17sw5ou5qy5rw201ss5tw201ws}6l{17su5tu5os5qw5rs}17s{2ktclvcmvcnvcovcpv4lv4wuckv}5o{ckwclwcmwcnwcowcpw4lw4wu}5q{ckyclycmycnycoycpy4ly4wu5ms}5r{cktcltcmtcntcotcpt4lt4ws}5t{2ktclvcmvcnvcovcpv4lv4wuckv}7q{cksclscmscnscoscps4ls}6p{17su5tu5qw5rs}ek{5qs5rs}el{17su5tu5os5qw5rs}em{17su5tu5os5qs5rs}en{17su5qs5rs}eo{5qs5rs}ep{17su5tu5os5qw5rs}es{5qs}et{17su5tu5qw5rs}eu{17su5tu5qs5rs}ev{5qs}6z{17sv5tv5os5qx5rs}fm{5os5qt5rs}fn{17sv5tv5os5qx5rs}fo{17sv5tv5os5qx5rs}fp{5os5qt5rs}fq{5os5qt5rs}7r{ckuclucmucnucoucpu4lu5os}fs{17sv5tv5os5qx5rs}ft{17ss5ts5qs}fu{17sw5tw5qs}fv{17sw5tw5qs}fw{17ss5ts5qs}fz{ckuclucmucnucoucpu4lu5os5rs}}}"),
                "Helvetica-Oblique": b("{'widths'{k3p2q4mcx1w201n3r201o6o201s1q201t1q201u1q201w2l201x2l201y2l2k1w2l1w202m2n2n3r2o3r2p5t202q6o2r1n2s2l2t2l2u2r2v3u2w1w2x2l2y1w2z1w3k3r3l3r3m3r3n3r3o3r3p3r3q3r3r3r3s3r203t2l203u2l3v1w3w3u3x3u3y3u3z3r4k6p4l4m4m4m4n4s4o4s4p4m4q3x4r4y4s4s4t1w4u3m4v4m4w3r4x5n4y4s4z4y5k4m5l4y5m4s5n4m5o3x5p4s5q4m5r5y5s4m5t4m5u3x5v1w5w1w5x1w5y2z5z3r6k2l6l3r6m3r6n3m6o3r6p3r6q1w6r3r6s3r6t1q6u1q6v3m6w1q6x5n6y3r6z3r7k3r7l3r7m2l7n3m7o1w7p3r7q3m7r4s7s3m7t3m7u3m7v2l7w1u7x2l7y3u202l3rcl4mal2lam3ran3rao3rap3rar3ras2lat4tau2pav3raw3uay4taz2lbk2sbl3u'fof'6obo2lbp3rbr1wbs2lbu2obv3rbz3xck4m202k3rcm4mcn4mco4mcp4mcq6ocr4scs4mct4mcu4mcv4mcw1w2m2ncy1wcz1wdl4sdm4ydn4ydo4ydp4ydq4yds4ydt4sdu4sdv4sdw4sdz3xek3rel3rem3ren3reo3rep3req5ter3mes3ret3reu3rev3rew1wex1wey1wez1wfl3rfm3rfn3rfo3rfp3rfq3rfr3ufs3xft3rfu3rfv3rfw3rfz3m203k6o212m6o2dw2l2cq2l3t3r3u1w17s4m19m3r}'kerning'{5q{4wv}cl{4qs5kw5ow5qs17sv5tv}201t{2wu4w1k2yu}201x{2wu4wy2yu}17s{2ktclucmucnu4otcpu4lu4wycoucku}2w{7qs4qz5k1m17sy5ow5qx5rsfsu5ty7tufzu}2x{17sy5ty5oy5qs}2y{7qs4qz5k1m17sy5ow5qx5rsfsu5ty7tufzu}'fof'-6o7p{17sv5tv5ow}ck{4qs5kw5ow5qs17sv5tv}4l{4qs5kw5ow5qs17sv5tv}cm{4qs5kw5ow5qs17sv5tv}cn{4qs5kw5ow5qs17sv5tv}co{4qs5kw5ow5qs17sv5tv}cp{4qs5kw5ow5qs17sv5tv}6l{17sy5ty5ow}do{17st5tt}4z{17st5tt}7s{fst}dm{17st5tt}dn{17st5tt}5o{ckwclwcmwcnwcowcpw4lw4wv}dp{17st5tt}dq{17st5tt}7t{5ow}ds{17st5tt}5t{2ktclucmucnu4otcpu4lu4wycoucku}fu{17sv5tv5ow}6p{17sy5ty5ow5qs}ek{17sy5ty5ow}el{17sy5ty5ow}em{17sy5ty5ow}en{5ty}eo{17sy5ty5ow}ep{17sy5ty5ow}es{17sy5ty5qs}et{17sy5ty5ow5qs}eu{17sy5ty5ow5qs}ev{17sy5ty5ow5qs}6z{17sy5ty5ow5qs}fm{17sy5ty5ow5qs}fn{17sy5ty5ow5qs}fo{17sy5ty5ow5qs}fp{17sy5ty5qs}fq{17sy5ty5ow5qs}7r{5ow}fs{17sy5ty5ow5qs}ft{17sv5tv5ow}7m{5ow}fv{17sv5tv5ow}fw{17sv5tv5ow}}}")
            }
        };
    f.events.push(["addFonts", function (b) {
        var h, a, f, j;
        for (a in b.fonts)
            if (b.fonts.hasOwnProperty(a)) {
                h = b.fonts[a];
                if (f = d.Unicode[h.PostScriptName]) j = h.metadata.Unicode ? h.metadata.Unicode : h.metadata.Unicode = {}, j.widths = f.widths, j.kerning = f.kerning;
                if (f = g.Unicode[h.PostScriptName]) j = h.metadata.Unicode ? h.metadata.Unicode : h.metadata.Unicode = {}, j.encoding = f, f.codePages && f.codePages.length && (h.encoding = f.codePages[0])
            }
    }])
})(jsPDF.API);
var BlobBuilder = BlobBuilder || self.WebKitBlobBuilder || self.MozBlobBuilder || self.MSBlobBuilder || function (f) {
    var b = function (a) {
        return Object.prototype.toString.call(a).match(/^\[object\s(.*)\]$/)[1]
    },
        k = function () {
            this.data = []
        },
        g = function (a, b, c) {
            this.data = a;
            this.size = a.length;
            this.type = b;
            this.encoding = c
        },
        d = k.prototype,
        e = g.prototype,
        h = f.FileReaderSync,
        a = function (a) {
            this.code = this[this.name = a]
        },
        p = "NOT_FOUND_ERR SECURITY_ERR ABORT_ERR NOT_READABLE_ERR ENCODING_ERR NO_MODIFICATION_ALLOWED_ERR INVALID_STATE_ERR SYNTAX_ERR".split(" "),
        j = p.length,
        m = f.URL || f.webkitURL || f,
        u = m.createObjectURL,
        c = m.revokeObjectURL,
        q = m,
        C = f.btoa,
        F = f.atob,
        H = !1,
        n = function (a) {
            H = !a
        },
        x = f.ArrayBuffer,
        z = f.Uint8Array;
    for (k.fake = e.fake = !0; j--;) a.prototype[p[j]] = j + 1;
    try {
        z && n.apply(0, new z(1))
    } catch (A) { }
    m.createObjectURL || (q = f.URL = {});
    q.createObjectURL = function (a) {
        var b = a.type;
        null === b && (b = "application/octet-stream");
        if (a instanceof g) return b = "data:" + b, "base64" === a.encoding ? b + ";base64," + a.data : "URI" === a.encoding ? b + "," + decodeURIComponent(a.data) : C ? b + ";base64," + C(a.data) :
            b + "," + encodeURIComponent(a.data);
        if (u) return u.call(m, a)
    };
    q.revokeObjectURL = function (a) {
        "data:" !== a.substring(0, 5) && c && c.call(m, a)
    };
    d.append = function (c) {
        var d = this.data;
        if (z && c instanceof x)
            if (H) d.push(String.fromCharCode.apply(String, new z(c)));
            else {
                d = "";
                c = new z(c);
                for (var e = 0, f = c.length; e < f; e++) d += String.fromCharCode(c[e])
            } else if ("Blob" === b(c) || "File" === b(c))
            if (h) e = new h, d.push(e.readAsBinaryString(c));
            else throw new a("NOT_READABLE_ERR");
        else c instanceof g ? "base64" === c.encoding && F ? d.push(F(c.data)) :
            "URI" === c.encoding ? d.push(decodeURIComponent(c.data)) : "raw" === c.encoding && d.push(c.data) : ("string" !== typeof c && (c += ""), d.push(unescape(encodeURIComponent(c))))
    };
    d.getBlob = function (a) {
        arguments.length || (a = null);
        return new g(this.data.join(""), a, "raw")
    };
    d.toString = function () {
        return "[object BlobBuilder]"
    };
    e.slice = function (a, c, b) {
        var d = arguments.length;
        3 > d && (b = null);
        return new g(this.data.slice(a, 1 < d ? c : this.data.length), b, this.encoding)
    };
    e.toString = function () {
        return "[object Blob]"
    };
    return k
}(self),
    saveAs =
        saveAs || navigator.msSaveBlob && navigator.msSaveBlob.bind(navigator) || function (f) {
            var b = f.document,
                k = f.URL || f.webkitURL || f,
                g = b.createElementNS("http://www.w3.org/1999/xhtml", "a"),
                d = "download" in g,
                e = f.webkitRequestFileSystem,
                h = f.requestFileSystem || e || f.mozRequestFileSystem,
                a = function (a) {
                    (f.setImmediate || f.setTimeout)(function () {
                        throw a;
                    }, 0)
                },
                p = 0,
                j = [],
                m = function (c, b, d) {
                    b = [].concat(b);
                    for (var e = b.length; e--;) {
                        var f = c["on" + b[e]];
                        if ("function" === typeof f) try {
                            f.call(c, d || c)
                        } catch (h) {
                            a(h)
                        }
                    }
                },
                u = function (a,
                    c) {
                    var k = this,
                        u = a.type,
                        n = !1,
                        x, z, A = function () {
                            var c = (f.URL || f.webkitURL || f).createObjectURL(a);
                            j.push(c);
                            return c
                        },
                        y = function () {
                            m(k, ["writestart", "progress", "write", "writeend"])
                        },
                        s = function () {
                            if (n || !x) x = A(a);
                            z && (z.location.href = x);
                            k.readyState = k.DONE;
                            y()
                        },
                        r = function (a) {
                            return function () {
                                if (k.readyState !== k.DONE) return a.apply(this, arguments)
                            }
                        },
                        I = {
                            create: !0,
                            exclusive: !1
                        },
                        w;
                    k.readyState = k.INIT;
                    c || (c = "download");
                    if (d && (x = A(a), g.href = x, g.download = c, w = b.createEvent("MouseEvents"), w.initMouseEvent("click", !0, !1, f, 0, 0, 0, 0, 0, !1, !1, !1, !1, 0, null), g.dispatchEvent(w))) {
                        k.readyState = k.DONE;
                        y();
                        return
                    }
                    f.chrome && (u && "application/octet-stream" !== u) && (w = a.slice || a.webkitSlice, a = w.call(a, 0, a.size, "application/octet-stream"), n = !0);
                    e && "download" !== c && (c += ".download");
                    z = "application/octet-stream" === u || e ? f : f.open();
                    h ? (p += a.size, h(f.TEMPORARY, p, r(function (b) {
                        b.root.getDirectory("saved", I, r(function (b) {
                            var d = function () {
                                b.getFile(c, I, r(function (c) {
                                    c.createWriter(r(function (b) {
                                        b.onwriteend = function (a) {
                                            z.location.href =
                                                c.toURL();
                                            j.push(c);
                                            k.readyState = k.DONE;
                                            m(k, "writeend", a)
                                        };
                                        b.onerror = function () {
                                            var a = b.error;
                                            a.code !== a.ABORT_ERR && s()
                                        };
                                        ["writestart", "progress", "write", "abort"].forEach(function (a) {
                                            b["on" + a] = k["on" + a]
                                        });
                                        b.write(a);
                                        k.abort = function () {
                                            b.abort();
                                            k.readyState = k.DONE
                                        };
                                        k.readyState = k.WRITING
                                    }), s)
                                }), s)
                            };
                            b.getFile(c, {
                                create: !1
                            }, r(function (a) {
                                a.remove();
                                d()
                            }), r(function (a) {
                                a.code === a.NOT_FOUND_ERR ? d() : s()
                            }))
                        }), s)
                    }), s)) : s()
                },
                c = u.prototype;
            c.abort = function () {
                this.readyState = this.DONE;
                m(this, "abort")
            };
            c.readyState =
                c.INIT = 0;
            c.WRITING = 1;
            c.DONE = 2;
            c.error = c.onwritestart = c.onprogress = c.onwrite = c.onabort = c.onerror = c.onwriteend = null;
            f.addEventListener("unload", function () {
                for (var a = j.length; a--;) {
                    var c = j[a];
                    "string" === typeof c ? k.revokeObjectURL(c) : c.remove()
                }
                j.length = 0
            }, !1);
            return function (a, c) {
                return new u(a, c)
            }
        }(self),
    MAX_BITS = 15,
    D_CODES = 30,
    BL_CODES = 19,
    LENGTH_CODES = 29,
    LITERALS = 256,
    L_CODES = LITERALS + 1 + LENGTH_CODES,
    HEAP_SIZE = 2 * L_CODES + 1,
    END_BLOCK = 256,
    MAX_BL_BITS = 7,
    REP_3_6 = 16,
    REPZ_3_10 = 17,
    REPZ_11_138 = 18,
    Buf_size = 16,
    Z_DEFAULT_COMPRESSION = -1,
    Z_FILTERED = 1,
    Z_HUFFMAN_ONLY = 2,
    Z_DEFAULT_STRATEGY = 0,
    Z_NO_FLUSH = 0,
    Z_PARTIAL_FLUSH = 1,
    Z_FULL_FLUSH = 3,
    Z_FINISH = 4,
    Z_OK = 0,
    Z_STREAM_END = 1,
    Z_NEED_DICT = 2,
    Z_STREAM_ERROR = -2,
    Z_DATA_ERROR = -3,
    Z_BUF_ERROR = -5,
    _dist_code = [0, 1, 2, 3, 4, 4, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8, 8, 8, 9, 9, 9, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 13, 13, 13, 13, 13,
        13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 14, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 0, 0, 16, 17, 18, 18, 19, 19, 20, 20, 20, 20, 21,
        21, 21, 21, 22, 22, 22, 22, 22, 22, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28,
        28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29
    ];

function Tree() {
    var f = this;
    f.build_tree = function (b) {
        var k = f.dyn_tree,
            g = f.stat_desc.static_tree,
            d = f.stat_desc.elems,
            e, h = -1,
            a;
        b.heap_len = 0;
        b.heap_max = HEAP_SIZE;
        for (e = 0; e < d; e++) 0 !== k[2 * e] ? (b.heap[++b.heap_len] = h = e, b.depth[e] = 0) : k[2 * e + 1] = 0;
        for (; 2 > b.heap_len;) a = b.heap[++b.heap_len] = 2 > h ? ++h : 0, k[2 * a] = 1, b.depth[a] = 0, b.opt_len-- , g && (b.static_len -= g[2 * a + 1]);
        f.max_code = h;
        for (e = Math.floor(b.heap_len / 2); 1 <= e; e--) b.pqdownheap(k, e);
        a = d;
        do e = b.heap[1], b.heap[1] = b.heap[b.heap_len--], b.pqdownheap(k, 1), g = b.heap[1],
            b.heap[--b.heap_max] = e, b.heap[--b.heap_max] = g, k[2 * a] = k[2 * e] + k[2 * g], b.depth[a] = Math.max(b.depth[e], b.depth[g]) + 1, k[2 * e + 1] = k[2 * g + 1] = a, b.heap[1] = a++ , b.pqdownheap(k, 1); while (2 <= b.heap_len);
        b.heap[--b.heap_max] = b.heap[1];
        e = f.dyn_tree;
        for (var h = f.stat_desc.static_tree, p = f.stat_desc.extra_bits, j = f.stat_desc.extra_base, m = f.stat_desc.max_length, u, c, q = 0, d = 0; d <= MAX_BITS; d++) b.bl_count[d] = 0;
        e[2 * b.heap[b.heap_max] + 1] = 0;
        for (a = b.heap_max + 1; a < HEAP_SIZE; a++) g = b.heap[a], d = e[2 * e[2 * g + 1] + 1] + 1, d > m && (d = m, q++), e[2 * g + 1] =
            d, g > f.max_code || (b.bl_count[d]++ , u = 0, g >= j && (u = p[g - j]), c = e[2 * g], b.opt_len += c * (d + u), h && (b.static_len += c * (h[2 * g + 1] + u)));
        if (0 !== q) {
            do {
                for (d = m - 1; 0 === b.bl_count[d];) d--;
                b.bl_count[d]--;
                b.bl_count[d + 1] += 2;
                b.bl_count[m]--;
                q -= 2
            } while (0 < q);
            for (d = m; 0 !== d; d--)
                for (g = b.bl_count[d]; 0 !== g;) h = b.heap[--a], h > f.max_code || (e[2 * h + 1] != d && (b.opt_len += (d - e[2 * h + 1]) * e[2 * h], e[2 * h + 1] = d), g--)
        }
        e = f.max_code;
        a = b.bl_count;
        b = [];
        g = 0;
        for (d = 1; d <= MAX_BITS; d++) b[d] = g = g + a[d - 1] << 1;
        for (a = 0; a <= e; a++)
            if (p = k[2 * a + 1], 0 !== p) {
                g = k;
                d = 2 * a;
                h = b[p]++;
                j = 0;
                do j |= h & 1, h >>>= 1, j <<= 1; while (0 < --p);
                g[d] = j >>> 1
            }
    }
}
Tree._length_code = [0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14, 14, 15, 15, 15, 15, 16, 16, 16, 16, 16, 16, 16, 16, 17, 17, 17, 17, 17, 17, 17, 17, 18, 18, 18, 18, 18, 18, 18, 18, 19, 19, 19, 19, 19, 19, 19, 19, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 20, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 22, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 24, 25, 25, 25, 25, 25,
    25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 26, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 27, 28
];
Tree.base_length = [0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14, 16, 20, 24, 28, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 0];
Tree.base_dist = [0, 1, 2, 3, 4, 6, 8, 12, 16, 24, 32, 48, 64, 96, 128, 192, 256, 384, 512, 768, 1024, 1536, 2048, 3072, 4096, 6144, 8192, 12288, 16384, 24576];
Tree.d_code = function (f) {
    return 256 > f ? _dist_code[f] : _dist_code[256 + (f >>> 7)]
};
Tree.extra_lbits = [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 0];
Tree.extra_dbits = [0, 0, 0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 11, 11, 12, 12, 13, 13];
Tree.extra_blbits = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 3, 7];
Tree.bl_order = [16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15];

function StaticTree(f, b, k, g, d) {
    this.static_tree = f;
    this.extra_bits = b;
    this.extra_base = k;
    this.elems = g;
    this.max_length = d
}
StaticTree.static_ltree = [12, 8, 140, 8, 76, 8, 204, 8, 44, 8, 172, 8, 108, 8, 236, 8, 28, 8, 156, 8, 92, 8, 220, 8, 60, 8, 188, 8, 124, 8, 252, 8, 2, 8, 130, 8, 66, 8, 194, 8, 34, 8, 162, 8, 98, 8, 226, 8, 18, 8, 146, 8, 82, 8, 210, 8, 50, 8, 178, 8, 114, 8, 242, 8, 10, 8, 138, 8, 74, 8, 202, 8, 42, 8, 170, 8, 106, 8, 234, 8, 26, 8, 154, 8, 90, 8, 218, 8, 58, 8, 186, 8, 122, 8, 250, 8, 6, 8, 134, 8, 70, 8, 198, 8, 38, 8, 166, 8, 102, 8, 230, 8, 22, 8, 150, 8, 86, 8, 214, 8, 54, 8, 182, 8, 118, 8, 246, 8, 14, 8, 142, 8, 78, 8, 206, 8, 46, 8, 174, 8, 110, 8, 238, 8, 30, 8, 158, 8, 94, 8, 222, 8, 62, 8, 190, 8, 126, 8, 254, 8, 1, 8, 129, 8, 65, 8, 193, 8, 33, 8, 161,
    8, 97, 8, 225, 8, 17, 8, 145, 8, 81, 8, 209, 8, 49, 8, 177, 8, 113, 8, 241, 8, 9, 8, 137, 8, 73, 8, 201, 8, 41, 8, 169, 8, 105, 8, 233, 8, 25, 8, 153, 8, 89, 8, 217, 8, 57, 8, 185, 8, 121, 8, 249, 8, 5, 8, 133, 8, 69, 8, 197, 8, 37, 8, 165, 8, 101, 8, 229, 8, 21, 8, 149, 8, 85, 8, 213, 8, 53, 8, 181, 8, 117, 8, 245, 8, 13, 8, 141, 8, 77, 8, 205, 8, 45, 8, 173, 8, 109, 8, 237, 8, 29, 8, 157, 8, 93, 8, 221, 8, 61, 8, 189, 8, 125, 8, 253, 8, 19, 9, 275, 9, 147, 9, 403, 9, 83, 9, 339, 9, 211, 9, 467, 9, 51, 9, 307, 9, 179, 9, 435, 9, 115, 9, 371, 9, 243, 9, 499, 9, 11, 9, 267, 9, 139, 9, 395, 9, 75, 9, 331, 9, 203, 9, 459, 9, 43, 9, 299, 9, 171, 9, 427, 9, 107, 9, 363, 9, 235,
    9, 491, 9, 27, 9, 283, 9, 155, 9, 411, 9, 91, 9, 347, 9, 219, 9, 475, 9, 59, 9, 315, 9, 187, 9, 443, 9, 123, 9, 379, 9, 251, 9, 507, 9, 7, 9, 263, 9, 135, 9, 391, 9, 71, 9, 327, 9, 199, 9, 455, 9, 39, 9, 295, 9, 167, 9, 423, 9, 103, 9, 359, 9, 231, 9, 487, 9, 23, 9, 279, 9, 151, 9, 407, 9, 87, 9, 343, 9, 215, 9, 471, 9, 55, 9, 311, 9, 183, 9, 439, 9, 119, 9, 375, 9, 247, 9, 503, 9, 15, 9, 271, 9, 143, 9, 399, 9, 79, 9, 335, 9, 207, 9, 463, 9, 47, 9, 303, 9, 175, 9, 431, 9, 111, 9, 367, 9, 239, 9, 495, 9, 31, 9, 287, 9, 159, 9, 415, 9, 95, 9, 351, 9, 223, 9, 479, 9, 63, 9, 319, 9, 191, 9, 447, 9, 127, 9, 383, 9, 255, 9, 511, 9, 0, 7, 64, 7, 32, 7, 96, 7, 16, 7, 80, 7,
    48, 7, 112, 7, 8, 7, 72, 7, 40, 7, 104, 7, 24, 7, 88, 7, 56, 7, 120, 7, 4, 7, 68, 7, 36, 7, 100, 7, 20, 7, 84, 7, 52, 7, 116, 7, 3, 8, 131, 8, 67, 8, 195, 8, 35, 8, 163, 8, 99, 8, 227, 8
];
StaticTree.static_dtree = [0, 5, 16, 5, 8, 5, 24, 5, 4, 5, 20, 5, 12, 5, 28, 5, 2, 5, 18, 5, 10, 5, 26, 5, 6, 5, 22, 5, 14, 5, 30, 5, 1, 5, 17, 5, 9, 5, 25, 5, 5, 5, 21, 5, 13, 5, 29, 5, 3, 5, 19, 5, 11, 5, 27, 5, 7, 5, 23, 5];
StaticTree.static_l_desc = new StaticTree(StaticTree.static_ltree, Tree.extra_lbits, LITERALS + 1, L_CODES, MAX_BITS);
StaticTree.static_d_desc = new StaticTree(StaticTree.static_dtree, Tree.extra_dbits, 0, D_CODES, MAX_BITS);
StaticTree.static_bl_desc = new StaticTree(null, Tree.extra_blbits, 0, BL_CODES, MAX_BL_BITS);
var MAX_MEM_LEVEL = 9,
    DEF_MEM_LEVEL = 8;

function Config(f, b, k, g, d) {
    this.good_length = f;
    this.max_lazy = b;
    this.nice_length = k;
    this.max_chain = g;
    this.func = d
}
var STORED = 0,
    FAST = 1,
    SLOW = 2,
    config_table = [new Config(0, 0, 0, 0, STORED), new Config(4, 4, 8, 4, FAST), new Config(4, 5, 16, 8, FAST), new Config(4, 6, 32, 32, FAST), new Config(4, 4, 16, 16, SLOW), new Config(8, 16, 32, 32, SLOW), new Config(8, 16, 128, 128, SLOW), new Config(8, 32, 128, 256, SLOW), new Config(32, 128, 258, 1024, SLOW), new Config(32, 258, 258, 4096, SLOW)],
    z_errmsg = "need dictionary;stream end;;;stream error;data error;;buffer error;;".split(";"),
    NeedMore = 0,
    BlockDone = 1,
    FinishStarted = 2,
    FinishDone = 3,
    PRESET_DICT = 32,
    INIT_STATE = 42,
    BUSY_STATE =
        113,
    FINISH_STATE = 666,
    Z_DEFLATED = 8,
    STORED_BLOCK = 0,
    STATIC_TREES = 1,
    DYN_TREES = 2,
    MIN_MATCH = 3,
    MAX_MATCH = 258,
    MIN_LOOKAHEAD = MAX_MATCH + MIN_MATCH + 1;

function smaller(f, b, k, g) {
    var d = f[2 * b];
    f = f[2 * k];
    return d < f || d == f && g[b] <= g[k]
}

function Deflate() {
    function f() {
        var a;
        for (a = 0; a < L_CODES; a++) U[2 * a] = 0;
        for (a = 0; a < D_CODES; a++) X[2 * a] = 0;
        for (a = 0; a < BL_CODES; a++) O[2 * a] = 0;
        U[2 * END_BLOCK] = 1;
        S = la = n.opt_len = n.static_len = 0
    }

    function b(a, c) {
        var b, d = -1,
            e, f = a[1],
            h = 0,
            g = 7,
            j = 4;
        0 === f && (g = 138, j = 3);
        a[2 * (c + 1) + 1] = 65535;
        for (b = 0; b <= c; b++) e = f, f = a[2 * (b + 1) + 1], ++h < g && e == f || (h < j ? O[2 * e] += h : 0 !== e ? (e != d && O[2 * e]++ , O[2 * REP_3_6]++) : 10 >= h ? O[2 * REPZ_3_10]++ : O[2 * REPZ_11_138]++ , h = 0, d = e, 0 === f ? (g = 138, j = 3) : e == f ? (g = 6, j = 3) : (g = 7, j = 4))
    }

    function k(a) {
        n.pending_buf[n.pending++] =
            a
    }

    function g(a) {
        k(a & 255);
        k(a >>> 8 & 255)
    }

    function d(a, c) {
        L > Buf_size - c ? (P |= a << L & 65535, g(P), P = a >>> Buf_size - L, L += c - Buf_size) : (P |= a << L & 65535, L += c)
    }

    function e(a, c) {
        var b = 2 * a;
        d(c[b] & 65535, c[b + 1] & 65535)
    }

    function h(a, c) {
        var b, f = -1,
            h, g = a[1],
            j = 0,
            k = 7,
            l = 4;
        0 === g && (k = 138, l = 3);
        for (b = 0; b <= c; b++)
            if (h = g, g = a[2 * (b + 1) + 1], !(++j < k && h == g)) {
                if (j < l) {
                    do e(h, O); while (0 !== --j)
                } else 0 !== h ? (h != f && (e(h, O), j--), e(REP_3_6, O), d(j - 3, 2)) : 10 >= j ? (e(REPZ_3_10, O), d(j - 3, 3)) : (e(REPZ_11_138, O), d(j - 11, 7));
                j = 0;
                f = h;
                0 === g ? (k = 138, l = 3) : h == g ? (k = 6, l =
                    3) : (k = 7, l = 4)
            }
    }

    function a() {
        16 == L ? (g(P), L = P = 0) : 8 <= L && (k(P & 255), P >>>= 8, L -= 8)
    }

    function p(a, c) {
        var b, d, e;
        n.pending_buf[fa + 2 * S] = a >>> 8 & 255;
        n.pending_buf[fa + 2 * S + 1] = a & 255;
        n.pending_buf[ka + S] = c & 255;
        S++;
        0 === a ? U[2 * c]++ : (la++ , a-- , U[2 * (Tree._length_code[c] + LITERALS + 1)]++ , X[2 * Tree.d_code(a)]++);
        if (0 === (S & 8191) && 2 < M) {
            b = 8 * S;
            d = t - Q;
            for (e = 0; e < D_CODES; e++) b += X[2 * e] * (5 + Tree.extra_dbits[e]);
            if (la < Math.floor(S / 2) && b >>> 3 < Math.floor(d / 2)) return !0
        }
        return S == ba - 1
    }

    function j(a, c) {
        var b, f, h = 0,
            g, j;
        if (0 !== S) {
            do b = n.pending_buf[fa +
                2 * h] << 8 & 65280 | n.pending_buf[fa + 2 * h + 1] & 255, f = n.pending_buf[ka + h] & 255, h++ , 0 === b ? e(f, a) : (g = Tree._length_code[f], e(g + LITERALS + 1, a), j = Tree.extra_lbits[g], 0 !== j && (f -= Tree.base_length[g], d(f, j)), b-- , g = Tree.d_code(b), e(g, c), j = Tree.extra_dbits[g], 0 !== j && (b -= Tree.base_dist[g], d(b, j))); while (h < S)
        }
        e(END_BLOCK, a);
        ga = a[2 * END_BLOCK + 1]
    }

    function m() {
        8 < L ? g(P) : 0 < L && k(P & 255);
        L = P = 0
    }

    function u(a, c, b) {
        d((STORED_BLOCK << 1) + (b ? 1 : 0), 3);
        m();
        ga = 8;
        g(c);
        g(~c);
        n.pending_buf.set(w.subarray(a, a + c), n.pending);
        n.pending += c
    }

    function c(a) {
        var c =
            0 <= Q ? Q : -1,
            e = t - Q,
            g, k, l = 0;
        if (0 < M) {
            da.build_tree(n);
            ea.build_tree(n);
            b(U, da.max_code);
            b(X, ea.max_code);
            ja.build_tree(n);
            for (l = BL_CODES - 1; 3 <= l && 0 === O[2 * Tree.bl_order[l] + 1]; l--);
            n.opt_len += 3 * (l + 1) + 14;
            g = n.opt_len + 3 + 7 >>> 3;
            k = n.static_len + 3 + 7 >>> 3;
            k <= g && (g = k)
        } else g = k = e + 5;
        if (e + 4 <= g && -1 != c) u(c, e, a);
        else if (k == g) d((STATIC_TREES << 1) + (a ? 1 : 0), 3), j(StaticTree.static_ltree, StaticTree.static_dtree);
        else {
            d((DYN_TREES << 1) + (a ? 1 : 0), 3);
            c = da.max_code + 1;
            e = ea.max_code + 1;
            l += 1;
            d(c - 257, 5);
            d(e - 1, 5);
            d(l - 4, 4);
            for (g = 0; g < l; g++) d(O[2 *
                Tree.bl_order[g] + 1], 3);
            h(U, c - 1);
            h(X, e - 1);
            j(U, X)
        }
        f();
        a && m();
        Q = t;
        x.flush_pending()
    }

    function q() {
        var a, c, b, d;
        do {
            d = aa - E - t;
            if (0 === d && 0 === t && 0 === E) d = s;
            else if (-1 == d) d--;
            else if (t >= s + s - MIN_LOOKAHEAD) {
                w.set(w.subarray(s, s + s), 0);
                W -= s;
                t -= s;
                Q -= s;
                b = a = v;
                do c = G[--b] & 65535, G[b] = c >= s ? c - s : 0; while (0 !== --a);
                b = a = s;
                do c = D[--b] & 65535, D[b] = c >= s ? c - s : 0; while (0 !== --a);
                d += s
            }
            if (0 === x.avail_in) break;
            a = x.read_buf(w, t + E, d);
            E += a;
            E >= MIN_MATCH && (B = w[t] & 255, B = (B << N ^ w[t + 1] & 255) & l)
        } while (E < MIN_LOOKAHEAD && 0 !== x.avail_in)
    }

    function C(a) {
        var b =
            65535,
            d;
        for (b > A - 5 && (b = A - 5); ;) {
            if (1 >= E) {
                q();
                if (0 === E && a == Z_NO_FLUSH) return NeedMore;
                if (0 === E) break
            }
            t += E;
            E = 0;
            d = Q + b;
            if (0 === t || t >= d)
                if (E = t - d, t = d, c(!1), 0 === x.avail_out) return NeedMore;
            if (t - Q >= s - MIN_LOOKAHEAD && (c(!1), 0 === x.avail_out)) return NeedMore
        }
        c(a == Z_FINISH);
        return 0 === x.avail_out ? a == Z_FINISH ? FinishStarted : NeedMore : a == Z_FINISH ? FinishDone : BlockDone
    }

    function F(a) {
        var c = Y,
            b = t,
            d, e = K,
            g = t > s - MIN_LOOKAHEAD ? t - (s - MIN_LOOKAHEAD) : 0,
            f = ia,
            h = I,
            j = t + MAX_MATCH,
            k = w[b + e - 1],
            l = w[b + e];
        K >= ha && (c >>= 2);
        f > E && (f = E);
        do
            if (d =
                a, !(w[d + e] != l || w[d + e - 1] != k || w[d] != w[b] || w[++d] != w[b + 1])) {
                b += 2;
                d++;
                do; while (w[++b] == w[++d] && w[++b] == w[++d] && w[++b] == w[++d] && w[++b] == w[++d] && w[++b] == w[++d] && w[++b] == w[++d] && w[++b] == w[++d] && w[++b] == w[++d] && b < j);
                d = MAX_MATCH - (j - b);
                b = j - MAX_MATCH;
                if (d > e) {
                    W = a;
                    e = d;
                    if (d >= f) break;
                    k = w[b + e - 1];
                    l = w[b + e]
                }
            }
        while ((a = D[a & h] & 65535) > g && 0 !== --c);
        return e <= E ? e : E
    }

    function H(a) {
        for (var b = 0, d, e; ;) {
            if (E < MIN_LOOKAHEAD) {
                q();
                if (E < MIN_LOOKAHEAD && a == Z_NO_FLUSH) return NeedMore;
                if (0 === E) break
            }
            E >= MIN_MATCH && (B = (B << N ^ w[t + (MIN_MATCH -
                1)] & 255) & l, b = G[B] & 65535, D[t & I] = G[B], G[B] = t);
            K = J;
            ca = W;
            J = MIN_MATCH - 1;
            if (0 !== b && (K < Z && (t - b & 65535) <= s - MIN_LOOKAHEAD) && (T != Z_HUFFMAN_ONLY && (J = F(b)), 5 >= J && (T == Z_FILTERED || J == MIN_MATCH && 4096 < t - W))) J = MIN_MATCH - 1;
            if (K >= MIN_MATCH && J <= K) {
                e = t + E - MIN_MATCH;
                d = p(t - 1 - ca, K - MIN_MATCH);
                E -= K - 1;
                K -= 2;
                do ++t <= e && (B = (B << N ^ w[t + (MIN_MATCH - 1)] & 255) & l, b = G[B] & 65535, D[t & I] = G[B], G[B] = t); while (0 !== --K);
                R = 0;
                J = MIN_MATCH - 1;
                t++;
                if (d && (c(!1), 0 === x.avail_out)) return NeedMore
            } else if (0 !== R) {
                if ((d = p(0, w[t - 1] & 255)) && c(!1), t++ , E-- , 0 === x.avail_out) return NeedMore
            } else R =
                1, t++ , E--
        }
        0 !== R && (p(0, w[t - 1] & 255), R = 0);
        c(a == Z_FINISH);
        return 0 === x.avail_out ? a == Z_FINISH ? FinishStarted : NeedMore : a == Z_FINISH ? FinishDone : BlockDone
    }
    var n = this,
        x, z, A, y, s, r, I, w, aa, D, G, B, v, V, l, N, Q, J, ca, R, t, W, E, K, Y, Z, M, T, ha, ia, U, X, O, da = new Tree,
        ea = new Tree,
        ja = new Tree;
    n.depth = [];
    var ka, ba, S, fa, la, ga, P, L;
    n.bl_count = [];
    n.heap = [];
    U = [];
    X = [];
    O = [];
    n.pqdownheap = function (a, b) {
        for (var c = n.heap, d = c[b], e = b << 1; e <= n.heap_len;) {
            e < n.heap_len && smaller(a, c[e + 1], c[e], n.depth) && e++;
            if (smaller(a, d, c[e], n.depth)) break;
            c[b] =
                c[e];
            b = e;
            e <<= 1
        }
        c[b] = d
    };
    n.deflateInit = function (a, b, c, d, e, g) {
        d || (d = Z_DEFLATED);
        e || (e = DEF_MEM_LEVEL);
        g || (g = Z_DEFAULT_STRATEGY);
        a.msg = null;
        b == Z_DEFAULT_COMPRESSION && (b = 6);
        if (1 > e || e > MAX_MEM_LEVEL || d != Z_DEFLATED || 9 > c || 15 < c || 0 > b || 9 < b || 0 > g || g > Z_HUFFMAN_ONLY) return Z_STREAM_ERROR;
        a.dstate = n;
        r = c;
        s = 1 << r;
        I = s - 1;
        V = e + 7;
        v = 1 << V;
        l = v - 1;
        N = Math.floor((V + MIN_MATCH - 1) / MIN_MATCH);
        w = new Uint8Array(2 * s);
        D = [];
        G = [];
        ba = 1 << e + 6;
        n.pending_buf = new Uint8Array(4 * ba);
        A = 4 * ba;
        fa = Math.floor(ba / 2);
        ka = 3 * ba;
        M = b;
        T = g;
        a.total_in = a.total_out =
            0;
        a.msg = null;
        n.pending = 0;
        n.pending_out = 0;
        z = BUSY_STATE;
        y = Z_NO_FLUSH;
        da.dyn_tree = U;
        da.stat_desc = StaticTree.static_l_desc;
        ea.dyn_tree = X;
        ea.stat_desc = StaticTree.static_d_desc;
        ja.dyn_tree = O;
        ja.stat_desc = StaticTree.static_bl_desc;
        L = P = 0;
        ga = 8;
        f();
        aa = 2 * s;
        for (a = G[v - 1] = 0; a < v - 1; a++) G[a] = 0;
        Z = config_table[M].max_lazy;
        ha = config_table[M].good_length;
        ia = config_table[M].nice_length;
        Y = config_table[M].max_chain;
        E = Q = t = 0;
        J = K = MIN_MATCH - 1;
        B = R = 0;
        return Z_OK
    };
    n.deflateEnd = function () {
        if (z != INIT_STATE && z != BUSY_STATE && z != FINISH_STATE) return Z_STREAM_ERROR;
        w = D = G = n.pending_buf = null;
        n.dstate = null;
        return z == BUSY_STATE ? Z_DATA_ERROR : Z_OK
    };
    n.deflateParams = function (a, b, c) {
        var d = Z_OK;
        b == Z_DEFAULT_COMPRESSION && (b = 6);
        if (0 > b || 9 < b || 0 > c || c > Z_HUFFMAN_ONLY) return Z_STREAM_ERROR;
        config_table[M].func != config_table[b].func && 0 !== a.total_in && (d = a.deflate(Z_PARTIAL_FLUSH));
        M != b && (M = b, Z = config_table[M].max_lazy, ha = config_table[M].good_length, ia = config_table[M].nice_length, Y = config_table[M].max_chain);
        T = c;
        return d
    };
    n.deflateSetDictionary = function (a, b, c) {
        a = c;
        var d = 0;
        if (!b ||
            z != INIT_STATE) return Z_STREAM_ERROR;
        if (a < MIN_MATCH) return Z_OK;
        a > s - MIN_LOOKAHEAD && (a = s - MIN_LOOKAHEAD, d = c - a);
        w.set(b.subarray(d, d + a), 0);
        Q = t = a;
        B = w[0] & 255;
        B = (B << N ^ w[1] & 255) & l;
        for (b = 0; b <= a - MIN_MATCH; b++) B = (B << N ^ w[b + (MIN_MATCH - 1)] & 255) & l, D[b & I] = G[B], G[B] = b;
        return Z_OK
    };
    n.deflate = function (b, g) {
        var f, h, j;
        if (g > Z_FINISH || 0 > g) return Z_STREAM_ERROR;
        if (!b.next_out || !b.next_in && 0 !== b.avail_in || z == FINISH_STATE && g != Z_FINISH) return b.msg = z_errmsg[Z_NEED_DICT - Z_STREAM_ERROR], Z_STREAM_ERROR;
        if (0 === b.avail_out) return b.msg =
            z_errmsg[Z_NEED_DICT - Z_BUF_ERROR], Z_BUF_ERROR;
        x = b;
        f = y;
        y = g;
        z == INIT_STATE && (h = Z_DEFLATED + (r - 8 << 4) << 8, j = (M - 1 & 255) >> 1, 3 < j && (j = 3), h |= j << 6, 0 !== t && (h |= PRESET_DICT), z = BUSY_STATE, h += 31 - h % 31, k(h >> 8 & 255), k(h & 255));
        if (0 !== n.pending) {
            if (x.flush_pending(), 0 === x.avail_out) return y = -1, Z_OK
        } else if (0 === x.avail_in && g <= f && g != Z_FINISH) return x.msg = z_errmsg[Z_NEED_DICT - Z_BUF_ERROR], Z_BUF_ERROR;
        if (z == FINISH_STATE && 0 !== x.avail_in) return b.msg = z_errmsg[Z_NEED_DICT - Z_BUF_ERROR], Z_BUF_ERROR;
        if (0 !== x.avail_in || 0 !== E || g != Z_NO_FLUSH &&
            z != FINISH_STATE) {
            f = -1;
            switch (config_table[M].func) {
                case STORED:
                    f = C(g);
                    break;
                case FAST:
                    a: {
                        for (f = 0; ;) {
                            if (E < MIN_LOOKAHEAD) {
                                q();
                                if (E < MIN_LOOKAHEAD && g == Z_NO_FLUSH) {
                                    f = NeedMore;
                                    break a
                                }
                                if (0 === E) break
                            }
                            E >= MIN_MATCH && (B = (B << N ^ w[t + (MIN_MATCH - 1)] & 255) & l, f = G[B] & 65535, D[t & I] = G[B], G[B] = t);
                            0 !== f && (t - f & 65535) <= s - MIN_LOOKAHEAD && T != Z_HUFFMAN_ONLY && (J = F(f));
                            if (J >= MIN_MATCH)
                                if (h = p(t - W, J - MIN_MATCH), E -= J, J <= Z && E >= MIN_MATCH) {
                                    J--;
                                    do t++ , B = (B << N ^ w[t + (MIN_MATCH - 1)] & 255) & l, f = G[B] & 65535, D[t & I] = G[B], G[B] = t; while (0 !== --J);
                                    t++
                                } else t +=
                                    J, J = 0, B = w[t] & 255, B = (B << N ^ w[t + 1] & 255) & l;
                            else h = p(0, w[t] & 255), E-- , t++;
                            if (h && (c(!1), 0 === x.avail_out)) {
                                f = NeedMore;
                                break a
                            }
                        }
                        c(g == Z_FINISH);
                        f = 0 === x.avail_out ? g == Z_FINISH ? FinishStarted : NeedMore : g == Z_FINISH ? FinishDone : BlockDone
                    }
                    break;
                case SLOW:
                    f = H(g)
            }
            if (f == FinishStarted || f == FinishDone) z = FINISH_STATE;
            if (f == NeedMore || f == FinishStarted) return 0 === x.avail_out && (y = -1), Z_OK;
            if (f == BlockDone) {
                if (g == Z_PARTIAL_FLUSH) d(STATIC_TREES << 1, 3), e(END_BLOCK, StaticTree.static_ltree), a(), 9 > 1 + ga + 10 - L && (d(STATIC_TREES << 1, 3), e(END_BLOCK,
                    StaticTree.static_ltree), a()), ga = 7;
                else if (u(0, 0, !1), g == Z_FULL_FLUSH)
                    for (f = 0; f < v; f++) G[f] = 0;
                x.flush_pending();
                if (0 === x.avail_out) return y = -1, Z_OK
            }
        }
        return g != Z_FINISH ? Z_OK : Z_STREAM_END
    }
}

function ZStream() {
    this.total_out = this.avail_out = this.total_in = this.avail_in = this.next_out_index = this.next_in_index = 0
}
ZStream.prototype = {
    deflateInit: function (f, b) {
        this.dstate = new Deflate;
        b || (b = MAX_BITS);
        return this.dstate.deflateInit(this, f, b)
    },
    deflate: function (f) {
        return !this.dstate ? Z_STREAM_ERROR : this.dstate.deflate(this, f)
    },
    deflateEnd: function () {
        if (!this.dstate) return Z_STREAM_ERROR;
        var f = this.dstate.deflateEnd();
        this.dstate = null;
        return f
    },
    deflateParams: function (f, b) {
        return !this.dstate ? Z_STREAM_ERROR : this.dstate.deflateParams(this, f, b)
    },
    deflateSetDictionary: function (f, b) {
        return !this.dstate ? Z_STREAM_ERROR : this.dstate.deflateSetDictionary(this,
            f, b)
    },
    read_buf: function (f, b, k) {
        var g = this.avail_in;
        g > k && (g = k);
        if (0 === g) return 0;
        this.avail_in -= g;
        f.set(this.next_in.subarray(this.next_in_index, this.next_in_index + g), b);
        this.next_in_index += g;
        this.total_in += g;
        return g
    },
    flush_pending: function () {
        var f = this.dstate.pending;
        f > this.avail_out && (f = this.avail_out);
        0 !== f && (this.next_out.set(this.dstate.pending_buf.subarray(this.dstate.pending_out, this.dstate.pending_out + f), this.next_out_index), this.next_out_index += f, this.dstate.pending_out += f, this.total_out +=
            f, this.avail_out -= f, this.dstate.pending -= f, 0 === this.dstate.pending && (this.dstate.pending_out = 0))
    }
};

function Deflater(f) {
    var b = new ZStream,
        k = Z_NO_FLUSH,
        g = new Uint8Array(512);
    "undefined" == typeof f && (f = Z_DEFAULT_COMPRESSION);
    b.deflateInit(f);
    b.next_out = g;
    this.append = function (d, e) {
        var f, a = [],
            p = 0,
            j = 0,
            m = 0,
            u;
        if (d.length) {
            b.next_in_index = 0;
            b.next_in = d;
            b.avail_in = d.length;
            do {
                b.next_out_index = 0;
                b.avail_out = 512;
                f = b.deflate(k);
                if (f != Z_OK) throw "deflating: " + b.msg;
                b.next_out_index && (512 == b.next_out_index ? a.push(new Uint8Array(g)) : a.push(new Uint8Array(g.subarray(0, b.next_out_index))));
                m += b.next_out_index;
                e &&
                    (0 < b.next_in_index && b.next_in_index != p) && (e(b.next_in_index), p = b.next_in_index)
            } while (0 < b.avail_in || 0 === b.avail_out);
            u = new Uint8Array(m);
            a.forEach(function (a) {
                u.set(a, j);
                j += a.length
            });
            return u
        }
    };
    this.flush = function () {
        var d, e = [],
            f = 0,
            a = 0,
            k;
        do {
            b.next_out_index = 0;
            b.avail_out = 512;
            d = b.deflate(Z_FINISH);
            if (d != Z_STREAM_END && d != Z_OK) throw "deflating: " + b.msg;
            0 < 512 - b.avail_out && e.push(new Uint8Array(g.subarray(0, b.next_out_index)));
            a += b.next_out_index
        } while (0 < b.avail_in || 0 === b.avail_out);
        b.deflateEnd();
        k = new Uint8Array(a);
        e.forEach(function (a) {
            k.set(a, f);
            f += a.length
        });
        return k
    }
}
void
    function (f, b) {
        "object" === typeof module ? module.exports = b() : "function" === typeof define ? define(b) : f.adler32cs = b()
    }(this, function () {
        var f = "function" === typeof ArrayBuffer && "function" === typeof Uint8Array,
            b = null,
            k;
        if (f) {
            try {
                var g = require("buffer");
                "function" === typeof g.Buffer && (b = g.Buffer)
            } catch (d) { }
            k = function (a) {
                return a instanceof ArrayBuffer || null !== b && a instanceof b
            }
        } else k = function () {
            return !1
        };
        var e;
        e = null !== b ? function (a) {
            return (new b(a, "utf8")).toString("binary")
        } : function (a) {
            return unescape(encodeURIComponent(a))
        };
        var h = function (a, b) {
            for (var d = a & 65535, e = a >>> 16, f = 0, g = b.length; f < g; f++) d = (d + (b.charCodeAt(f) & 255)) % 65521, e = (e + d) % 65521;
            return (e << 16 | d) >>> 0
        },
            a = function (a, b) {
                for (var d = a & 65535, e = a >>> 16, f = 0, g = b.length; f < g; f++) d = (d + b[f]) % 65521, e = (e + d) % 65521;
                return (e << 16 | d) >>> 0
            },
            g = {},
            p = function (a) {
                if (!(this instanceof p)) throw new TypeError("Constructor cannot called be as a function.");
                if (!isFinite(a = null == a ? 1 : +a)) throw Error("First arguments needs to be a finite number.");
                this.checksum = a >>> 0
            },
            j = p.prototype = {};
        j.constructor =
            p;
        var m = function (a) {
            if (!(this instanceof p)) throw new TypeError("Constructor cannot called be as a function.");
            if (null == a) throw Error("First argument needs to be a string.");
            this.checksum = h(1, a.toString())
        };
        m.prototype = j;
        p.from = m;
        m = function (a) {
            if (!(this instanceof p)) throw new TypeError("Constructor cannot called be as a function.");
            if (null == a) throw Error("First argument needs to be a string.");
            a = e(a.toString());
            this.checksum = h(1, a)
        };
        m.prototype = j;
        p.fromUtf8 = m;
        f && (m = function (b) {
            if (!(this instanceof p)) throw new TypeError("Constructor cannot called be as a function.");
            if (!k(b)) throw Error("First argument needs to be ArrayBuffer.");
            b = new Uint8Array(b);
            return this.checksum = a(1, b)
        }, m.prototype = j, p.fromBuffer = m);
        j.update = function (a) {
            if (null == a) throw Error("First argument needs to be a string.");
            a = a.toString();
            return this.checksum = h(this.checksum, a)
        };
        j.updateUtf8 = function (a) {
            if (null == a) throw Error("First argument needs to be a string.");
            a = e(a.toString());
            return this.checksum = h(this.checksum, a)
        };
        f &&
            (j.updateBuffer = function (b) {
                if (!k(b)) throw Error("First argument needs to be ArrayBuffer.");
                b = new Uint8Array(b);
                return this.checksum = a(this.checksum, b)
            });
        j.clone = function () {
            return new u(this.checksum)
        };
        var u = g.Adler32 = p;
        g.from = function (a) {
            if (null == a) throw Error("First argument needs to be a string.");
            return h(1, a.toString())
        };
        g.fromUtf8 = function (a) {
            if (null == a) throw Error("First argument needs to be a string.");
            a = e(a.toString());
            return h(1, a)
        };
        f && (g.fromBuffer = function (b) {
            if (!k(b)) throw Error("First argument need to be ArrayBuffer.");
            b = new Uint8Array(b);
            return a(1, b)
        });
        return g
    });

/*!
 * html2canvas 1.0.0-rc.1 <https://html2canvas.hertzen.com>
 * Copyright (c) 2019 Niklas von Hertzen <https://hertzen.com>
 * Released under MIT License
 */
! function (A, e) {
    "object" == typeof exports && "object" == typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? exports.html2canvas = e() : A.html2canvas = e()
}(window, function () {
    return function (A) {
        var e = {};

        function t(r) {
            if (e[r]) return e[r].exports;
            var n = e[r] = {
                i: r,
                l: !1,
                exports: {}
            };
            return A[r].call(n.exports, n, n.exports, t), n.l = !0, n.exports
        }
        return t.m = A, t.c = e, t.d = function (A, e, r) {
            t.o(A, e) || Object.defineProperty(A, e, {
                enumerable: !0,
                get: r
            })
        }, t.r = function (A) {
            "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(A, Symbol.toStringTag, {
                value: "Module"
            }), Object.defineProperty(A, "__esModule", {
                value: !0
            })
        }, t.t = function (A, e) {
            if (1 & e && (A = t(A)), 8 & e) return A;
            if (4 & e && "object" == typeof A && A && A.__esModule) return A;
            var r = Object.create(null);
            if (t.r(r), Object.defineProperty(r, "default", {
                enumerable: !0,
                value: A
            }), 2 & e && "string" != typeof A)
                for (var n in A) t.d(r, n, function (e) {
                    return A[e]
                }.bind(null, n));
            return r
        }, t.n = function (A) {
            var e = A && A.__esModule ? function () {
                return A.default
            } : function () {
                return A
            };
            return t.d(e, "a", e), e
        }, t.o = function (A, e) {
            return Object.prototype.hasOwnProperty.call(A, e)
        }, t.p = "", t(t.s = 5)
    }([function (A, e, t) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        });
        var r = t(1);
        Object.defineProperty(e, "toCodePoints", {
            enumerable: !0,
            get: function () {
                return r.toCodePoints
            }
        }), Object.defineProperty(e, "fromCodePoint", {
            enumerable: !0,
            get: function () {
                return r.fromCodePoint
            }
        });
        var n = t(2);
        Object.defineProperty(e, "LineBreaker", {
            enumerable: !0,
            get: function () {
                return n.LineBreaker
            }
        })
    }, function (A, e, t) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), e.toCodePoints = function (A) {
            for (var e = [], t = 0, r = A.length; t < r;) {
                var n = A.charCodeAt(t++);
                if (n >= 55296 && n <= 56319 && t < r) {
                    var B = A.charCodeAt(t++);
                    56320 == (64512 & B) ? e.push(((1023 & n) << 10) + (1023 & B) + 65536) : (e.push(n), t--)
                } else e.push(n)
            }
            return e
        }, e.fromCodePoint = function () {
            if (String.fromCodePoint) return String.fromCodePoint.apply(String, arguments);
            var A = arguments.length;
            if (!A) return "";
            for (var e = [], t = -1, r = ""; ++t < A;) {
                var n = arguments.length <= t ? void 0 : arguments[t];
                n <= 65535 ? e.push(n) : (n -= 65536, e.push(55296 + (n >> 10), n % 1024 + 56320)), (t + 1 === A || e.length > 16384) && (r += String.fromCharCode.apply(String, e), e.length = 0)
            }
            return r
        };
        for (var r = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", n = "undefined" == typeof Uint8Array ? [] : new Uint8Array(256), B = 0; B < r.length; B++) n[r.charCodeAt(B)] = B;
        e.decode = function (A) {
            var e = .75 * A.length,
                t = A.length,
                r = void 0,
                B = 0,
                s = void 0,
                o = void 0,
                a = void 0,
                i = void 0;
            "=" === A[A.length - 1] && (e-- , "=" === A[A.length - 2] && e--);
            var c = "undefined" != typeof ArrayBuffer && "undefined" != typeof Uint8Array && void 0 !== Uint8Array.prototype.slice ? new ArrayBuffer(e) : new Array(e),
                Q = Array.isArray(c) ? c : new Uint8Array(c);
            for (r = 0; r < t; r += 4) s = n[A.charCodeAt(r)], o = n[A.charCodeAt(r + 1)], a = n[A.charCodeAt(r + 2)], i = n[A.charCodeAt(r + 3)], Q[B++] = s << 2 | o >> 4, Q[B++] = (15 & o) << 4 | a >> 2, Q[B++] = (3 & a) << 6 | 63 & i;
            return c
        }, e.polyUint16Array = function (A) {
            for (var e = A.length, t = [], r = 0; r < e; r += 2) t.push(A[r + 1] << 8 | A[r]);
            return t
        }, e.polyUint32Array = function (A) {
            for (var e = A.length, t = [], r = 0; r < e; r += 4) t.push(A[r + 3] << 24 | A[r + 2] << 16 | A[r + 1] << 8 | A[r]);
            return t
        }
    }, function (A, e, t) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), e.LineBreaker = e.inlineBreakOpportunities = e.lineBreakAtIndex = e.codePointsToCharacterClasses = e.UnicodeTrie = e.BREAK_ALLOWED = e.BREAK_NOT_ALLOWED = e.BREAK_MANDATORY = e.classes = e.LETTER_NUMBER_MODIFIER = void 0;
        var r = function () {
            function A(A, e) {
                for (var t = 0; t < e.length; t++) {
                    var r = e[t];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
                }
            }
            return function (e, t, r) {
                return t && A(e.prototype, t), r && A(e, r), e
            }
        }(),
            n = function (A, e) {
                if (Array.isArray(A)) return A;
                if (Symbol.iterator in Object(A)) return function (A, e) {
                    var t = [],
                        r = !0,
                        n = !1,
                        B = void 0;
                    try {
                        for (var s, o = A[Symbol.iterator](); !(r = (s = o.next()).done) && (t.push(s.value), !e || t.length !== e); r = !0);
                    } catch (A) {
                        n = !0, B = A
                    } finally {
                        try {
                            !r && o.return && o.return()
                        } finally {
                            if (n) throw B
                        }
                    }
                    return t
                }(A, e);
                throw new TypeError("Invalid attempt to destructure non-iterable instance")
            },
            B = t(3),
            s = function (A) {
                return A && A.__esModule ? A : {
                    default: A
                }
            }(t(4)),
            o = t(1),
            a = e.LETTER_NUMBER_MODIFIER = 50,
            i = 10,
            c = 13,
            Q = 15,
            l = 17,
            w = 18,
            u = 19,
            U = 20,
            g = 21,
            F = 22,
            C = 24,
            h = 25,
            d = 26,
            H = 27,
            f = 28,
            E = 30,
            p = 32,
            K = 33,
            m = 34,
            b = 35,
            N = 37,
            y = 38,
            v = 39,
            I = 40,
            D = 42,
            M = (e.classes = {
                BK: 1,
                CR: 2,
                LF: 3,
                CM: 4,
                NL: 5,
                SG: 6,
                WJ: 7,
                ZW: 8,
                GL: 9,
                SP: i,
                ZWJ: 11,
                B2: 12,
                BA: c,
                BB: 14,
                HY: Q,
                CB: 16,
                CL: l,
                CP: w,
                EX: u,
                IN: U,
                NS: g,
                OP: F,
                QU: 23,
                IS: C,
                NU: h,
                PO: d,
                PR: H,
                SY: f,
                AI: 29,
                AL: E,
                CJ: 31,
                EB: p,
                EM: K,
                H2: m,
                H3: b,
                HL: 36,
                ID: N,
                JL: y,
                JV: v,
                JT: I,
                RI: 41,
                SA: D,
                XX: 43
            }, e.BREAK_MANDATORY = "!"),
            T = e.BREAK_NOT_ALLOWED = "Ã—",
            S = e.BREAK_ALLOWED = "Ã·",
            X = e.UnicodeTrie = (0, B.createTrieFromBase64)(s.default),
            z = [E, 36],
            L = [1, 2, 3, 5],
            O = [i, 8],
            x = [H, d],
            V = L.concat(O),
            k = [y, v, I, m, b],
            J = [Q, c],
            R = e.codePointsToCharacterClasses = function (A) {
                var e = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "strict",
                    t = [],
                    r = [],
                    n = [];
                return A.forEach(function (A, B) {
                    var s = X.get(A);
                    if (s > a ? (n.push(!0), s -= a) : n.push(!1), -1 !== ["normal", "auto", "loose"].indexOf(e) && -1 !== [8208, 8211, 12316, 12448].indexOf(A)) return r.push(B), t.push(16);
                    if (4 === s || 11 === s) {
                        if (0 === B) return r.push(B), t.push(E);
                        var o = t[B - 1];
                        return -1 === V.indexOf(o) ? (r.push(r[B - 1]), t.push(o)) : (r.push(B), t.push(E))
                    }
                    return r.push(B), 31 === s ? t.push("strict" === e ? g : N) : s === D ? t.push(E) : 29 === s ? t.push(E) : 43 === s ? A >= 131072 && A <= 196605 || A >= 196608 && A <= 262141 ? t.push(N) : t.push(E) : void t.push(s)
                }), [r, t, n]
            },
            _ = function (A, e, t, r) {
                var n = r[t];
                if (Array.isArray(A) ? -1 !== A.indexOf(n) : A === n)
                    for (var B = t; B <= r.length;) {
                        var s = r[++B];
                        if (s === e) return !0;
                        if (s !== i) break
                    }
                if (n === i)
                    for (var o = t; o > 0;) {
                        var a = r[--o];
                        if (Array.isArray(A) ? -1 !== A.indexOf(a) : A === a)
                            for (var c = t; c <= r.length;) {
                                var Q = r[++c];
                                if (Q === e) return !0;
                                if (Q !== i) break
                            }
                        if (a !== i) break
                    }
                return !1
            },
            P = function (A, e) {
                for (var t = A; t >= 0;) {
                    var r = e[t];
                    if (r !== i) return r;
                    t--
                }
                return 0
            },
            G = function (A, e, t, r, n) {
                if (0 === t[r]) return T;
                var B = r - 1;
                if (Array.isArray(n) && !0 === n[B]) return T;
                var s = B - 1,
                    o = B + 1,
                    a = e[B],
                    E = s >= 0 ? e[s] : 0,
                    D = e[o];
                if (2 === a && 3 === D) return T;
                if (-1 !== L.indexOf(a)) return M;
                if (-1 !== L.indexOf(D)) return T;
                if (-1 !== O.indexOf(D)) return T;
                if (8 === P(B, e)) return S;
                if (11 === X.get(A[B]) && (D === N || D === p || D === K)) return T;
                if (7 === a || 7 === D) return T;
                if (9 === a) return T;
                if (-1 === [i, c, Q].indexOf(a) && 9 === D) return T;
                if (-1 !== [l, w, u, C, f].indexOf(D)) return T;
                if (P(B, e) === F) return T;
                if (_(23, F, B, e)) return T;
                if (_([l, w], g, B, e)) return T;
                if (_(12, 12, B, e)) return T;
                if (a === i) return S;
                if (23 === a || 23 === D) return T;
                if (16 === D || 16 === a) return S;
                if (-1 !== [c, Q, g].indexOf(D) || 14 === a) return T;
                if (36 === E && -1 !== J.indexOf(a)) return T;
                if (a === f && 36 === D) return T;
                if (D === U && -1 !== z.concat(U, u, h, N, p, K).indexOf(a)) return T;
                if (-1 !== z.indexOf(D) && a === h || -1 !== z.indexOf(a) && D === h) return T;
                if (a === H && -1 !== [N, p, K].indexOf(D) || -1 !== [N, p, K].indexOf(a) && D === d) return T;
                if (-1 !== z.indexOf(a) && -1 !== x.indexOf(D) || -1 !== x.indexOf(a) && -1 !== z.indexOf(D)) return T;
                if (-1 !== [H, d].indexOf(a) && (D === h || -1 !== [F, Q].indexOf(D) && e[o + 1] === h) || -1 !== [F, Q].indexOf(a) && D === h || a === h && -1 !== [h, f, C].indexOf(D)) return T;
                if (-1 !== [h, f, C, l, w].indexOf(D))
                    for (var V = B; V >= 0;) {
                        var R = e[V];
                        if (R === h) return T;
                        if (-1 === [f, C].indexOf(R)) break;
                        V--
                    }
                if (-1 !== [H, d].indexOf(D))
                    for (var G = -1 !== [l, w].indexOf(a) ? s : B; G >= 0;) {
                        var W = e[G];
                        if (W === h) return T;
                        if (-1 === [f, C].indexOf(W)) break;
                        G--
                    }
                if (y === a && -1 !== [y, v, m, b].indexOf(D) || -1 !== [v, m].indexOf(a) && -1 !== [v, I].indexOf(D) || -1 !== [I, b].indexOf(a) && D === I) return T;
                if (-1 !== k.indexOf(a) && -1 !== [U, d].indexOf(D) || -1 !== k.indexOf(D) && a === H) return T;
                if (-1 !== z.indexOf(a) && -1 !== z.indexOf(D)) return T;
                if (a === C && -1 !== z.indexOf(D)) return T;
                if (-1 !== z.concat(h).indexOf(a) && D === F || -1 !== z.concat(h).indexOf(D) && a === w) return T;
                if (41 === a && 41 === D) {
                    for (var Y = t[B], q = 1; Y > 0 && 41 === e[--Y];) q++;
                    if (q % 2 != 0) return T
                }
                return a === p && D === K ? T : S
            },
            W = (e.lineBreakAtIndex = function (A, e) {
                if (0 === e) return T;
                if (e >= A.length) return M;
                var t = R(A),
                    r = n(t, 2),
                    B = r[0],
                    s = r[1];
                return G(A, s, B, e)
            }, function (A, e) {
                e || (e = {
                    lineBreak: "normal",
                    wordBreak: "normal"
                });
                var t = R(A, e.lineBreak),
                    r = n(t, 3),
                    B = r[0],
                    s = r[1],
                    o = r[2];
                return "break-all" !== e.wordBreak && "break-word" !== e.wordBreak || (s = s.map(function (A) {
                    return -1 !== [h, E, D].indexOf(A) ? N : A
                })), [B, s, "keep-all" === e.wordBreak ? o.map(function (e, t) {
                    return e && A[t] >= 19968 && A[t] <= 40959
                }) : null]
            }),
            Y = (e.inlineBreakOpportunities = function (A, e) {
                var t = (0, o.toCodePoints)(A),
                    r = T,
                    B = W(t, e),
                    s = n(B, 3),
                    a = s[0],
                    i = s[1],
                    c = s[2];
                return t.forEach(function (A, e) {
                    r += (0, o.fromCodePoint)(A) + (e >= t.length - 1 ? M : G(t, i, a, e + 1, c))
                }), r
            }, function () {
                function A(e, t, r, n) {
                    ! function (A, e) {
                        if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                    }(this, A), this._codePoints = e, this.required = t === M, this.start = r, this.end = n
                }
                return r(A, [{
                    key: "slice",
                    value: function () {
                        return o.fromCodePoint.apply(void 0, function (A) {
                            if (Array.isArray(A)) {
                                for (var e = 0, t = Array(A.length); e < A.length; e++) t[e] = A[e];
                                return t
                            }
                            return Array.from(A)
                        }(this._codePoints.slice(this.start, this.end)))
                    }
                }]), A
            }());
        e.LineBreaker = function (A, e) {
            var t = (0, o.toCodePoints)(A),
                r = W(t, e),
                B = n(r, 3),
                s = B[0],
                a = B[1],
                i = B[2],
                c = t.length,
                Q = 0,
                l = 0;
            return {
                next: function () {
                    if (l >= c) return {
                        done: !0
                    };
                    for (var A = T; l < c && (A = G(t, a, s, ++l, i)) === T;);
                    if (A !== T || l === c) {
                        var e = new Y(t, A, Q, l);
                        return Q = l, {
                            value: e,
                            done: !1
                        }
                    }
                    return {
                        done: !0
                    }
                }
            }
        }
    }, function (A, e, t) {
        "use strict";
        Object.defineProperty(e, "__esModule", {
            value: !0
        }), e.Trie = e.createTrieFromBase64 = e.UTRIE2_INDEX_2_MASK = e.UTRIE2_INDEX_2_BLOCK_LENGTH = e.UTRIE2_OMITTED_BMP_INDEX_1_LENGTH = e.UTRIE2_INDEX_1_OFFSET = e.UTRIE2_UTF8_2B_INDEX_2_LENGTH = e.UTRIE2_UTF8_2B_INDEX_2_OFFSET = e.UTRIE2_INDEX_2_BMP_LENGTH = e.UTRIE2_LSCP_INDEX_2_LENGTH = e.UTRIE2_DATA_MASK = e.UTRIE2_DATA_BLOCK_LENGTH = e.UTRIE2_LSCP_INDEX_2_OFFSET = e.UTRIE2_SHIFT_1_2 = e.UTRIE2_INDEX_SHIFT = e.UTRIE2_SHIFT_1 = e.UTRIE2_SHIFT_2 = void 0;
        var r = function () {
            function A(A, e) {
                for (var t = 0; t < e.length; t++) {
                    var r = e[t];
                    r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
                }
            }
            return function (e, t, r) {
                return t && A(e.prototype, t), r && A(e, r), e
            }
        }(),
            n = t(1),
            B = e.UTRIE2_SHIFT_2 = 5,
            s = e.UTRIE2_SHIFT_1 = 11,
            o = e.UTRIE2_INDEX_SHIFT = 2,
            a = e.UTRIE2_SHIFT_1_2 = s - B,
            i = e.UTRIE2_LSCP_INDEX_2_OFFSET = 65536 >> B,
            c = e.UTRIE2_DATA_BLOCK_LENGTH = 1 << B,
            Q = e.UTRIE2_DATA_MASK = c - 1,
            l = e.UTRIE2_LSCP_INDEX_2_LENGTH = 1024 >> B,
            w = e.UTRIE2_INDEX_2_BMP_LENGTH = i + l,
            u = e.UTRIE2_UTF8_2B_INDEX_2_OFFSET = w,
            U = e.UTRIE2_UTF8_2B_INDEX_2_LENGTH = 32,
            g = e.UTRIE2_INDEX_1_OFFSET = u + U,
            F = e.UTRIE2_OMITTED_BMP_INDEX_1_LENGTH = 65536 >> s,
            C = e.UTRIE2_INDEX_2_BLOCK_LENGTH = 1 << a,
            h = e.UTRIE2_INDEX_2_MASK = C - 1,
            d = (e.createTrieFromBase64 = function (A) {
                var e = (0, n.decode)(A),
                    t = Array.isArray(e) ? (0, n.polyUint32Array)(e) : new Uint32Array(e),
                    r = Array.isArray(e) ? (0, n.polyUint16Array)(e) : new Uint16Array(e),
                    B = r.slice(12, t[4] / 2),
                    s = 2 === t[5] ? r.slice((24 + t[4]) / 2) : t.slice(Math.ceil((24 + t[4]) / 4));
                return new d(t[0], t[1], t[2], t[3], B, s)
            }, e.Trie = function () {
                function A(e, t, r, n, B, s) {
                    ! function (A, e) {
                        if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                    }(this, A), this.initialValue = e, this.errorValue = t, this.highStart = r, this.highValueIndex = n, this.index = B, this.data = s
                }
                return r(A, [{
                    key: "get",
                    value: function (A) {
                        var e = void 0;
                        if (A >= 0) {
                            if (A < 55296 || A > 56319 && A <= 65535) return e = ((e = this.index[A >> B]) << o) + (A & Q), this.data[e];
                            if (A <= 65535) return e = ((e = this.index[i + (A - 55296 >> B)]) << o) + (A & Q), this.data[e];
                            if (A < this.highStart) return e = g - F + (A >> s), e = this.index[e], e += A >> B & h, e = ((e = this.index[e]) << o) + (A & Q), this.data[e];
                            if (A <= 1114111) return this.data[this.highValueIndex]
                        }
                        return this.errorValue
                    }
                }]), A
            }())
    }, function (A, e, t) {
        "use strict";
        A.exports = "KwAAAAAAAAAACA4AIDoAAPAfAAACAAAAAAAIABAAGABAAEgAUABYAF4AZgBeAGYAYABoAHAAeABeAGYAfACEAIAAiACQAJgAoACoAK0AtQC9AMUAXgBmAF4AZgBeAGYAzQDVAF4AZgDRANkA3gDmAOwA9AD8AAQBDAEUARoBIgGAAIgAJwEvATcBPwFFAU0BTAFUAVwBZAFsAXMBewGDATAAiwGTAZsBogGkAawBtAG8AcIBygHSAdoB4AHoAfAB+AH+AQYCDgIWAv4BHgImAi4CNgI+AkUCTQJTAlsCYwJrAnECeQKBAk0CiQKRApkCoQKoArACuALAAsQCzAIwANQC3ALkAjAA7AL0AvwCAQMJAxADGAMwACADJgMuAzYDPgOAAEYDSgNSA1IDUgNaA1oDYANiA2IDgACAAGoDgAByA3YDfgOAAIQDgACKA5IDmgOAAIAAogOqA4AAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAK8DtwOAAIAAvwPHA88D1wPfAyAD5wPsA/QD/AOAAIAABAQMBBIEgAAWBB4EJgQuBDMEIAM7BEEEXgBJBCADUQRZBGEEaQQwADAAcQQ+AXkEgQSJBJEEgACYBIAAoASoBK8EtwQwAL8ExQSAAIAAgACAAIAAgACgAM0EXgBeAF4AXgBeAF4AXgBeANUEXgDZBOEEXgDpBPEE+QQBBQkFEQUZBSEFKQUxBTUFPQVFBUwFVAVcBV4AYwVeAGsFcwV7BYMFiwWSBV4AmgWgBacFXgBeAF4AXgBeAKsFXgCyBbEFugW7BcIFwgXIBcIFwgXQBdQF3AXkBesF8wX7BQMGCwYTBhsGIwYrBjMGOwZeAD8GRwZNBl4AVAZbBl4AXgBeAF4AXgBeAF4AXgBeAF4AXgBeAGMGXgBqBnEGXgBeAF4AXgBeAF4AXgBeAF4AXgB5BoAG4wSGBo4GkwaAAIADHgR5AF4AXgBeAJsGgABGA4AAowarBrMGswagALsGwwbLBjAA0wbaBtoG3QbaBtoG2gbaBtoG2gblBusG8wb7BgMHCwcTBxsHCwcjBysHMAc1BzUHOgdCB9oGSgdSB1oHYAfaBloHaAfaBlIH2gbaBtoG2gbaBtoG2gbaBjUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHbQdeAF4ANQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQd1B30HNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1B4MH2gaKB68EgACAAIAAgACAAIAAgACAAI8HlwdeAJ8HpweAAIAArwe3B14AXgC/B8UHygcwANAH2AfgB4AA6AfwBz4B+AcACFwBCAgPCBcIogEYAR8IJwiAAC8INwg/CCADRwhPCFcIXwhnCEoDGgSAAIAAgABvCHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIfQh3CHgIeQh6CHsIfAh9CHcIeAh5CHoIewh8CH0Idwh4CHkIegh7CHwIhAiLCI4IMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwAJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlggwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAANQc1BzUHNQc1BzUHNQc1BzUHNQc1B54INQc1B6II2gaqCLIIugiAAIAAvgjGCIAAgACAAIAAgACAAIAAgACAAIAAywiHAYAA0wiAANkI3QjlCO0I9Aj8CIAAgACAAAIJCgkSCRoJIgknCTYHLwk3CZYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiWCJYIlgiAAIAAAAFAAXgBeAGAAcABeAHwAQACQAKAArQC9AJ4AXgBeAE0A3gBRAN4A7AD8AMwBGgEAAKcBNwEFAUwBXAF4QkhCmEKnArcCgAHHAsABz4LAAcABwAHAAd+C6ABoAG+C/4LAAcABwAHAAc+DF4MAAcAB54M3gweDV4Nng3eDaABoAGgAaABoAGgAaABoAGgAaABoAGgAaABoAGgAaABoAGgAaABoAEeDqABVg6WDqABoQ6gAaABoAHXDvcONw/3DvcO9w73DvcO9w73DvcO9w73DvcO9w73DvcO9w73DvcO9w73DvcO9w73DvcO9w73DvcO9w73DvcO9w73DncPAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcABwAHAAcAB7cPPwlGCU4JMACAAIAAgABWCV4JYQmAAGkJcAl4CXwJgAkwADAAMAAwAIgJgACLCZMJgACZCZ8JowmrCYAAswkwAF4AXgB8AIAAuwkABMMJyQmAAM4JgADVCTAAMAAwADAAgACAAIAAgACAAIAAgACAAIAAqwYWBNkIMAAwADAAMADdCeAJ6AnuCR4E9gkwAP4JBQoNCjAAMACAABUK0wiAAB0KJAosCjQKgAAwADwKQwqAAEsKvQmdCVMKWwowADAAgACAALcEMACAAGMKgABrCjAAMAAwADAAMAAwADAAMAAwADAAMAAeBDAAMAAwADAAMAAwADAAMAAwADAAMAAwAIkEPQFzCnoKiQSCCooKkAqJBJgKoAqkCokEGAGsCrQKvArBCjAAMADJCtEKFQHZCuEK/gHpCvEKMAAwADAAMACAAIwE+QowAIAAPwEBCzAAMAAwADAAMACAAAkLEQswAIAAPwEZCyELgAAOCCkLMAAxCzkLMAAwADAAMAAwADAAXgBeAEELMAAwADAAMAAwADAAMAAwAEkLTQtVC4AAXAtkC4AAiQkwADAAMAAwADAAMAAwADAAbAtxC3kLgAuFC4sLMAAwAJMLlwufCzAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAApwswADAAMACAAIAAgACvC4AAgACAAIAAgACAALcLMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAvwuAAMcLgACAAIAAgACAAIAAyguAAIAAgACAAIAA0QswADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAANkLgACAAIAA4AswADAAMAAwADAAMAAwADAAMAAwADAAMAAwAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAIAAgACJCR4E6AswADAAhwHwC4AA+AsADAgMEAwwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMACAAIAAGAwdDCUMMAAwAC0MNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQw1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHPQwwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADUHNQc1BzUHNQc1BzUHNQc2BzAAMAA5DDUHNQc1BzUHNQc1BzUHNQc1BzUHNQdFDDAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAgACAAIAATQxSDFoMMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwAF4AXgBeAF4AXgBeAF4AYgxeAGoMXgBxDHkMfwxeAIUMXgBeAI0MMAAwADAAMAAwAF4AXgCVDJ0MMAAwADAAMABeAF4ApQxeAKsMswy7DF4Awgy9DMoMXgBeAF4AXgBeAF4AXgBeAF4AXgDRDNkMeQBqCeAM3Ax8AOYM7Az0DPgMXgBeAF4AXgBeAF4AXgBeAF4AXgBeAF4AXgBeAF4AXgCgAAANoAAHDQ4NFg0wADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAeDSYNMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwAIAAgACAAIAAgACAAC4NMABeAF4ANg0wADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwAD4NRg1ODVYNXg1mDTAAbQ0wADAAMAAwADAAMAAwADAA2gbaBtoG2gbaBtoG2gbaBnUNeg3CBYANwgWFDdoGjA3aBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gaUDZwNpA2oDdoG2gawDbcNvw3HDdoG2gbPDdYN3A3fDeYN2gbsDfMN2gbaBvoN/g3aBgYODg7aBl4AXgBeABYOXgBeACUG2gYeDl4AJA5eACwO2w3aBtoGMQ45DtoG2gbaBtoGQQ7aBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gZJDjUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1B1EO2gY1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQdZDjUHNQc1BzUHNQc1B2EONQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHaA41BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1B3AO2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gY1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1BzUHNQc1B2EO2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gZJDtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBtoG2gbaBkkOeA6gAKAAoAAwADAAMAAwAKAAoACgAKAAoACgAKAAgA4wADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAAwADAAMAD//wQABAAEAAQABAAEAAQABAAEAA0AAwABAAEAAgAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAKABMAFwAeABsAGgAeABcAFgASAB4AGwAYAA8AGAAcAEsASwBLAEsASwBLAEsASwBLAEsAGAAYAB4AHgAeABMAHgBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAFgAbABIAHgAeAB4AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQABYADQARAB4ABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsABAAEAAQABAAEAAUABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAkAFgAaABsAGwAbAB4AHQAdAB4ATwAXAB4ADQAeAB4AGgAbAE8ATwAOAFAAHQAdAB0ATwBPABcATwBPAE8AFgBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB0AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgBQAB4AHgAeAB4AUABQAFAAUAAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAeAB4AHgAeAFAATwBAAE8ATwBPAEAATwBQAFAATwBQAB4AHgAeAB4AHgAeAB0AHQAdAB0AHgAdAB4ADgBQAFAAUABQAFAAHgAeAB4AHgAeAB4AHgBQAB4AUAAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4ABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAJAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAkACQAJAAkACQAJAAkABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAeAB4AHgAeAFAAHgAeAB4AKwArAFAAUABQAFAAGABQACsAKwArACsAHgAeAFAAHgBQAFAAUAArAFAAKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4ABAAEAAQABAAEAAQABAAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAUAAeAB4AHgAeAB4AHgArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwAYAA0AKwArAB4AHgAbACsABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQADQAEAB4ABAAEAB4ABAAEABMABAArACsAKwArACsAKwArACsAVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAKwArACsAKwArAFYAVgBWAB4AHgArACsAKwArACsAKwArACsAKwArACsAHgAeAB4AHgAeAB4AHgAeAB4AGgAaABoAGAAYAB4AHgAEAAQABAAEAAQABAAEAAQABAAEAAQAEwAEACsAEwATAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABABLAEsASwBLAEsASwBLAEsASwBLABoAGQAZAB4AUABQAAQAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQABMAUAAEAAQABAAEAAQABAAEAB4AHgAEAAQABAAEAAQABABQAFAABAAEAB4ABAAEAAQABABQAFAASwBLAEsASwBLAEsASwBLAEsASwBQAFAAUAAeAB4AUAAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwAeAFAABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEAAQABAAEAFAAKwArACsAKwArACsAKwArACsAKwArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEAAQAUABQAB4AHgAYABMAUAArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAFAABAAEAAQABAAEAFAABAAEAAQAUAAEAAQABAAEAAQAKwArAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAArACsAHgArAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAeAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABABQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAFAABAAEAAQABAAEAAQABABQAFAAUABQAFAAUABQAFAAUABQAAQABAANAA0ASwBLAEsASwBLAEsASwBLAEsASwAeAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAAQAKwBQAFAAUABQAFAAUABQAFAAKwArAFAAUAArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUAArAFAAKwArACsAUABQAFAAUAArACsABABQAAQABAAEAAQABAAEAAQAKwArAAQABAArACsABAAEAAQAUAArACsAKwArACsAKwArACsABAArACsAKwArAFAAUAArAFAAUABQAAQABAArACsASwBLAEsASwBLAEsASwBLAEsASwBQAFAAGgAaAFAAUABQAFAAUABMAB4AGwBQAB4AKwArACsABAAEAAQAKwBQAFAAUABQAFAAUAArACsAKwArAFAAUAArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUAArAFAAUAArAFAAUAArAFAAUAArACsABAArAAQABAAEAAQABAArACsAKwArAAQABAArACsABAAEAAQAKwArACsABAArACsAKwArACsAKwArAFAAUABQAFAAKwBQACsAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwAEAAQAUABQAFAABAArACsAKwArACsAKwArACsAKwArACsABAAEAAQAKwBQAFAAUABQAFAAUABQAFAAUAArAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUAArAFAAUAArAFAAUABQAFAAUAArACsABABQAAQABAAEAAQABAAEAAQABAArAAQABAAEACsABAAEAAQAKwArAFAAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAUABQAAQABAArACsASwBLAEsASwBLAEsASwBLAEsASwAeABsAKwArACsAKwArACsAKwBQAAQABAAEAAQABAAEACsABAAEAAQAKwBQAFAAUABQAFAAUABQAFAAKwArAFAAUAArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQAKwArAAQABAArACsABAAEAAQAKwArACsAKwArACsAKwArAAQABAArACsAKwArAFAAUAArAFAAUABQAAQABAArACsASwBLAEsASwBLAEsASwBLAEsASwAeAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwAEAFAAKwBQAFAAUABQAFAAUAArACsAKwBQAFAAUAArAFAAUABQAFAAKwArACsAUABQACsAUAArAFAAUAArACsAKwBQAFAAKwArACsAUABQAFAAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwAEAAQABAAEAAQAKwArACsABAAEAAQAKwAEAAQABAAEACsAKwBQACsAKwArACsAKwArAAQAKwArACsAKwArACsAKwArACsAKwBLAEsASwBLAEsASwBLAEsASwBLAFAAUABQAB4AHgAeAB4AHgAeABsAHgArACsAKwArACsABAAEAAQABAArAFAAUABQAFAAUABQAFAAUAArAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArAFAABAAEAAQABAAEAAQABAArAAQABAAEACsABAAEAAQABAArACsAKwArACsAKwArAAQABAArAFAAUABQACsAKwArACsAKwBQAFAABAAEACsAKwBLAEsASwBLAEsASwBLAEsASwBLACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAB4AUAAEAAQABAArAFAAUABQAFAAUABQAFAAUAArAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQACsAKwAEAFAABAAEAAQABAAEAAQABAArAAQABAAEACsABAAEAAQABAArACsAKwArACsAKwArAAQABAArACsAKwArACsAKwArAFAAKwBQAFAABAAEACsAKwBLAEsASwBLAEsASwBLAEsASwBLACsAUABQACsAKwArACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAFAABAAEAAQABAAEAAQABAArAAQABAAEACsABAAEAAQABABQAB4AKwArACsAKwBQAFAAUAAEAFAAUABQAFAAUABQAFAAUABQAFAABAAEACsAKwBLAEsASwBLAEsASwBLAEsASwBLAFAAUABQAFAAUABQAFAAUABQABoAUABQAFAAUABQAFAAKwArAAQABAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUABQACsAUAArACsAUABQAFAAUABQAFAAUAArACsAKwAEACsAKwArACsABAAEAAQABAAEAAQAKwAEACsABAAEAAQABAAEAAQABAAEACsAKwArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArAAQABAAeACsAKwArACsAKwArACsAKwArACsAKwArAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXAAqAFwAXAAqACoAKgAqACoAKgAqACsAKwArACsAGwBcAFwAXABcAFwAXABcACoAKgAqACoAKgAqACoAKgAeAEsASwBLAEsASwBLAEsASwBLAEsADQANACsAKwArACsAKwBcAFwAKwBcACsAKwBcAFwAKwBcACsAKwBcACsAKwArACsAKwArAFwAXABcAFwAKwBcAFwAXABcAFwAXABcACsAXABcAFwAKwBcACsAXAArACsAXABcACsAXABcAFwAXAAqAFwAXAAqACoAKgAqACoAKgArACoAKgBcACsAKwBcAFwAXABcAFwAKwBcACsAKgAqACoAKgAqACoAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArAFwAXABcAFwAUAAOAA4ADgAOAB4ADgAOAAkADgAOAA0ACQATABMAEwATABMACQAeABMAHgAeAB4ABAAEAB4AHgAeAB4AHgAeAEsASwBLAEsASwBLAEsASwBLAEsAUABQAFAAUABQAFAAUABQAFAAUAANAAQAHgAEAB4ABAAWABEAFgARAAQABABQAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAANAAQABAAEAAQABAANAAQABABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEAAQABAAEACsABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsADQANAB4AHgAeAB4AHgAeAAQAHgAeAB4AHgAeAB4AKwAeAB4ADgAOAA0ADgAeAB4AHgAeAB4ACQAJACsAKwArACsAKwBcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqAFwASwBLAEsASwBLAEsASwBLAEsASwANAA0AHgAeAB4AHgBcAFwAXABcAFwAXAAqACoAKgAqAFwAXABcAFwAKgAqACoAXAAqACoAKgBcAFwAKgAqACoAKgAqACoAKgBcAFwAXAAqACoAKgAqAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAKgAqACoAKgAqACoAKgAqACoAKgAqACoAXAAqAEsASwBLAEsASwBLAEsASwBLAEsAKgAqACoAKgAqACoAUABQAFAAUABQAFAAKwBQACsAKwArACsAKwBQACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAeAFAAUABQAFAAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAUABQAFAAUABQAFAAUABQAFAAKwBQAFAAUABQACsAKwBQAFAAUABQAFAAUABQACsAUAArAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUAArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUAArACsAUABQAFAAUABQAFAAUAArAFAAKwBQAFAAUABQACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwAEAAQABAAeAA0AHgAeAB4AHgAeAB4AHgBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAeAB4AHgAeAB4AHgAeAB4AHgAeACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArAFAAUABQAFAAUABQACsAKwANAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAeAB4AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAA0AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQABYAEQArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAADQANAA0AUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAABAAEAAQAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAA0ADQArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQAKwArACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQACsABAAEACsAKwArACsAKwArACsAKwArACsAKwArAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXAAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoADQANABUAXAANAB4ADQAbAFwAKgArACsASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwArAB4AHgATABMADQANAA4AHgATABMAHgAEAAQABAAJACsASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAUABQAFAAUABQAAQABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABABQACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsAKwArACsABAAEAAQABAAEAAQABAAEAAQABAAEAAQAKwArACsAKwAeACsAKwArABMAEwBLAEsASwBLAEsASwBLAEsASwBLAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcACsAKwBcAFwAXABcAFwAKwArACsAKwArACsAKwArACsAKwArAFwAXABcAFwAXABcAFwAXABcAFwAXABcACsAKwArACsAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwBcACsAKwArACoAKgBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAAQABAAEACsAKwAeAB4AXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAKgAqACoAKgAqACoAKgAqACoAKgArACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgArACsABABLAEsASwBLAEsASwBLAEsASwBLACsAKwArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArACsAKwArACsAKgAqACoAKgAqACoAKgBcACoAKgAqACoAKgAqACsAKwAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArAAQABAAEAAQABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQAUABQAFAAUABQAFAAUAArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsADQANAB4ADQANAA0ADQAeAB4AHgAeAB4AHgAeAB4AHgAeAAQABAAEAAQABAAEAAQABAAEAB4AHgAeAB4AHgAeAB4AHgAeACsAKwArAAQABAAEAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAUABQAEsASwBLAEsASwBLAEsASwBLAEsAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArACsAKwArACsAKwArACsAHgAeAB4AHgBQAFAAUABQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArACsAKwANAA0ADQANAA0ASwBLAEsASwBLAEsASwBLAEsASwArACsAKwBQAFAAUABLAEsASwBLAEsASwBLAEsASwBLAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAANAA0AUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAB4AHgAeAB4AHgAeAB4AHgArACsAKwArACsAKwArACsABAAEAAQAHgAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAFAAUABQAFAABABQAFAAUABQAAQABAAEAFAAUAAEAAQABAArACsAKwArACsAKwAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAKwAEAAQABAAEAAQAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArACsAUABQAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUAArAFAAKwBQACsAUAArAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACsAHgAeAB4AHgAeAB4AHgAeAFAAHgAeAB4AUABQAFAAKwAeAB4AHgAeAB4AHgAeAB4AHgAeAFAAUABQAFAAKwArAB4AHgAeAB4AHgAeACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArACsAUABQAFAAKwAeAB4AHgAeAB4AHgAeAA4AHgArAA0ADQANAA0ADQANAA0ACQANAA0ADQAIAAQACwAEAAQADQAJAA0ADQAMAB0AHQAeABcAFwAWABcAFwAXABYAFwAdAB0AHgAeABQAFAAUAA0AAQABAAQABAAEAAQABAAJABoAGgAaABoAGgAaABoAGgAeABcAFwAdABUAFQAeAB4AHgAeAB4AHgAYABYAEQAVABUAFQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgANAB4ADQANAA0ADQAeAA0ADQANAAcAHgAeAB4AHgArAAQABAAEAAQABAAEAAQABAAEAAQAUABQACsAKwBPAFAAUABQAFAAUAAeAB4AHgAWABEATwBQAE8ATwBPAE8AUABQAFAAUABQAB4AHgAeABYAEQArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAGwAbABsAGwAbABsAGwAaABsAGwAbABsAGwAbABsAGwAbABsAGwAbABsAGwAaABsAGwAbABsAGgAbABsAGgAbABsAGwAbABsAGwAbABsAGwAbABsAGwAbABsAGwAbABsABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAB4AHgBQABoAHgAdAB4AUAAeABoAHgAeAB4AHgAeAB4AHgAeAB4ATwAeAFAAGwAeAB4AUABQAFAAUABQAB4AHgAeAB0AHQAeAFAAHgBQAB4AUAAeAFAATwBQAFAAHgAeAB4AHgAeAB4AHgBQAFAAUABQAFAAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgBQAB4AUABQAFAAUABPAE8AUABQAFAAUABQAE8AUABQAE8AUABPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBQAFAAUABQAE8ATwBPAE8ATwBPAE8ATwBPAE8AUABQAFAAUABQAFAAUABQAFAAHgAeAFAAUABQAFAATwAeAB4AKwArACsAKwAdAB0AHQAdAB0AHQAdAB0AHQAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAdAB4AHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHQAeAB0AHQAeAB4AHgAdAB0AHgAeAB0AHgAeAB4AHQAeAB0AGwAbAB4AHQAeAB4AHgAeAB0AHgAeAB0AHQAdAB0AHgAeAB0AHgAdAB4AHQAdAB0AHQAdAB0AHgAdAB4AHgAeAB4AHgAdAB0AHQAdAB4AHgAeAB4AHQAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHQAeAB4AHgAdAB4AHgAeAB4AHgAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHQAdAB4AHgAdAB0AHQAdAB4AHgAdAB0AHgAeAB0AHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAdAB0AHgAeAB0AHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB0AHgAeAB4AHQAeAB4AHgAeAB4AHgAeAB0AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeABQAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAWABEAFgARAB4AHgAeAB4AHgAeAB0AHgAeAB4AHgAeAB4AHgAlACUAHgAeAB4AHgAeAB4AHgAeAB4AFgARAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACUAJQAlACUAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBQAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB4AHgAeAB4AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHgAeAB0AHQAdAB0AHgAeAB4AHgAeAB4AHgAeAB4AHgAdAB0AHgAdAB0AHQAdAB0AHQAdAB4AHgAeAB4AHgAeAB4AHgAdAB0AHgAeAB0AHQAeAB4AHgAeAB0AHQAeAB4AHgAeAB0AHQAdAB4AHgAdAB4AHgAdAB0AHQAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHQAdAB0AHQAeAB4AHgAeAB4AHgAeAB4AHgAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AJQAlACUAJQAeAB0AHQAeAB4AHQAeAB4AHgAeAB0AHQAeAB4AHgAeACUAJQAdAB0AJQAeACUAJQAlACAAJQAlAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AJQAlACUAHgAeAB4AHgAdAB4AHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHQAdAB4AHQAdAB0AHgAdACUAHQAdAB4AHQAdAB4AHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAlAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB0AHQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AJQAlACUAJQAlACUAJQAlACUAJQAlACUAHQAdAB0AHQAlAB4AJQAlACUAHQAlACUAHQAdAB0AJQAlAB0AHQAlAB0AHQAlACUAJQAeAB0AHgAeAB4AHgAdAB0AJQAdAB0AHQAdAB0AHQAlACUAJQAlACUAHQAlACUAIAAlAB0AHQAlACUAJQAlACUAJQAlACUAHgAeAB4AJQAlACAAIAAgACAAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAdAB4AHgAeABcAFwAXABcAFwAXAB4AEwATACUAHgAeAB4AFgARABYAEQAWABEAFgARABYAEQAWABEAFgARAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAWABEAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AFgARABYAEQAWABEAFgARABYAEQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeABYAEQAWABEAFgARABYAEQAWABEAFgARABYAEQAWABEAFgARABYAEQAWABEAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AFgARABYAEQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeABYAEQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHQAdAB0AHQAdAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwAeAB4AHgAeAB4AHgAeAB4AHgArACsAKwArACsAKwArACsAKwArACsAKwArAB4AHgAeAB4AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAEAAQABAAeAB4AKwArACsAKwArABMADQANAA0AUAATAA0AUABQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAUAANACsAKwArACsAKwArACsAKwArACsAKwArACsAKwAEAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQACsAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXAA0ADQANAA0ADQANAA0ADQAeAA0AFgANAB4AHgAXABcAHgAeABcAFwAWABEAFgARABYAEQAWABEADQANAA0ADQATAFAADQANAB4ADQANAB4AHgAeAB4AHgAMAAwADQANAA0AHgANAA0AFgANAA0ADQANAA0ADQANACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACsAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAKwArACsAKwArACsAKwArACsAKwArACsAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwAlACUAJQAlACUAJQAlACUAJQAlACUAJQArACsAKwArAA0AEQARACUAJQBHAFcAVwAWABEAFgARABYAEQAWABEAFgARACUAJQAWABEAFgARABYAEQAWABEAFQAWABEAEQAlAFcAVwBXAFcAVwBXAFcAVwBXAAQABAAEAAQABAAEACUAVwBXAFcAVwA2ACUAJQBXAFcAVwBHAEcAJQAlACUAKwBRAFcAUQBXAFEAVwBRAFcAUQBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFEAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBRAFcAUQBXAFEAVwBXAFcAVwBXAFcAUQBXAFcAVwBXAFcAVwBRAFEAKwArAAQABAAVABUARwBHAFcAFQBRAFcAUQBXAFEAVwBRAFcAUQBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFEAVwBRAFcAUQBXAFcAVwBXAFcAVwBRAFcAVwBXAFcAVwBXAFEAUQBXAFcAVwBXABUAUQBHAEcAVwArACsAKwArACsAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAKwArAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwArACUAJQBXAFcAVwBXACUAJQAlACUAJQAlACUAJQAlACUAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAKwArACsAKwArACUAJQAlACUAKwArACsAKwArACsAKwArACsAKwArACsAUQBRAFEAUQBRAFEAUQBRAFEAUQBRAFEAUQBRAFEAUQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACsAVwBXAFcAVwBXAFcAVwBXAFcAVwAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlAE8ATwBPAE8ATwBPAE8ATwAlAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXACUAJQAlACUAJQAlACUAJQAlACUAVwBXAFcAVwBXAFcAVwBXAFcAVwBXACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAEcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAKwArACsAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAADQATAA0AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABLAEsASwBLAEsASwBLAEsASwBLAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAFAABAAEAAQABAAeAAQABAAEAAQABAAEAAQABAAEAAQAHgBQAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AUABQAAQABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAeAA0ADQANAA0ADQArACsAKwArACsAKwArACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAFAAUABQAFAAUABQAFAAUABQAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AUAAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgBQAB4AHgAeAB4AHgAeAFAAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArAB4AHgAeAB4AHgAeAB4AHgArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAAQAUABQAFAABABQAFAAUABQAAQAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAeAB4AHgAeACsAKwArACsAUABQAFAAUABQAFAAHgAeABoAHgArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAADgAOABMAEwArACsAKwArACsAKwArACsABAAEAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAEACsAKwArACsAKwArACsAKwANAA0ASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArACsAKwAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABABQAFAAUABQAFAAUAAeAB4AHgBQAA4AUAArACsAUABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEAA0ADQBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAKwArACsAKwArACsAKwArACsAKwArAB4AWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYAFgAWABYACsAKwArAAQAHgAeAB4AHgAeAB4ADQANAA0AHgAeAB4AHgArAFAASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArAB4AHgBcAFwAXABcAFwAKgBcAFwAXABcAFwAXABcAFwAXABcAEsASwBLAEsASwBLAEsASwBLAEsAXABcAFwAXABcACsAUABQAFAAUABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsAKwArACsAKwArACsAKwArAFAAUABQAAQAUABQAFAAUABQAFAAUABQAAQABAArACsASwBLAEsASwBLAEsASwBLAEsASwArACsAHgANAA0ADQBcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAKgAqACoAXAAqACoAKgBcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXAAqAFwAKgAqACoAXABcACoAKgBcAFwAXABcAFwAKgAqAFwAKgBcACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAFwAXABcACoAKgBQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAAQABAAEAA0ADQBQAFAAUAAEAAQAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUAArACsAUABQAFAAUABQAFAAKwArAFAAUABQAFAAUABQACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAAQADQAEAAQAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArACsAKwArACsAVABVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBUAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVAFUAVQBVACsAKwArACsAKwArACsAKwArACsAKwArAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAWQBZAFkAKwArACsAKwBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAWgBaAFoAKwArACsAKwAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYABgAGAAYAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXACUAJQBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAJQAlACUAJQAlACUAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAKwArACsAKwArAFYABABWAFYAVgBWAFYAVgBWAFYAVgBWAB4AVgBWAFYAVgBWAFYAVgBWAFYAVgBWAFYAVgArAFYAVgBWAFYAVgArAFYAKwBWAFYAKwBWAFYAKwBWAFYAVgBWAFYAVgBWAFYAVgBWAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAEQAWAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUAAaAB4AKwArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAGAARABEAGAAYABMAEwAWABEAFAArACsAKwArACsAKwAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACUAJQAlACUAJQAWABEAFgARABYAEQAWABEAFgARABYAEQAlACUAFgARACUAJQAlACUAJQAlACUAEQAlABEAKwAVABUAEwATACUAFgARABYAEQAWABEAJQAlACUAJQAlACUAJQAlACsAJQAbABoAJQArACsAKwArAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArAAcAKwATACUAJQAbABoAJQAlABYAEQAlACUAEQAlABEAJQBXAFcAVwBXAFcAVwBXAFcAVwBXABUAFQAlACUAJQATACUAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXABYAJQARACUAJQAlAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwAWACUAEQAlABYAEQARABYAEQARABUAVwBRAFEAUQBRAFEAUQBRAFEAUQBRAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAEcARwArACsAVwBXAFcAVwBXAFcAKwArAFcAVwBXAFcAVwBXACsAKwBXAFcAVwBXAFcAVwArACsAVwBXAFcAKwArACsAGgAbACUAJQAlABsAGwArAB4AHgAeAB4AHgAeAB4AKwArACsAKwArACsAKwArACsAKwAEAAQABAAQAB0AKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwBQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsADQANAA0AKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArAB4AHgAeAB4AHgAeAB4AHgAeAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgBQAFAAHgAeAB4AKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArACsAKwArAB4AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4ABAArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAAQAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsADQBQAFAAUABQACsAKwArACsAUABQAFAAUABQAFAAUABQAA0AUABQAFAAUABQACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwArAB4AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUAArACsAUAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQACsAKwArAFAAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAA0AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAB4AHgBQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsADQBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArAB4AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwBQAFAAUABQAFAABAAEAAQAKwAEAAQAKwArACsAKwArAAQABAAEAAQAUABQAFAAUAArAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsABAAEAAQAKwArACsAKwAEAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsADQANAA0ADQANAA0ADQANAB4AKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAB4AUABQAFAAUABQAFAAUABQAB4AUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEACsAKwArACsAUABQAFAAUABQAA0ADQANAA0ADQANABQAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwANAA0ADQANAA0ADQANAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAHgAeAB4AHgArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwBQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAA0ADQAeAB4AHgAeAB4AKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAEsASwBLAEsASwBLAEsASwBLAEsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAEAAQABAAEAAQABAAeAB4AHgANAA0ADQANACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwBLAEsASwBLAEsASwBLAEsASwBLACsAKwArACsAKwArAFAAUABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsASwBLAEsASwBLAEsASwBLAEsASwANAA0ADQANACsAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAeAA4AUAArACsAKwArACsAKwArACsAKwAEAFAAUABQAFAADQANAB4ADQAeAAQABAAEAB4AKwArAEsASwBLAEsASwBLAEsASwBLAEsAUAAOAFAADQANAA0AKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAAQABAAEAAQABAANAA0AHgANAA0AHgAEACsAUABQAFAAUABQAFAAUAArAFAAKwBQAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQAA0AKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAAQABAAEAAQAKwArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArACsAKwArACsABAAEAAQABAArAFAAUABQAFAAUABQAFAAUAArACsAUABQACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAArACsABAAEACsAKwAEAAQABAArACsAUAArACsAKwArACsAKwAEACsAKwArACsAKwBQAFAAUABQAFAABAAEACsAKwAEAAQABAAEAAQABAAEACsAKwArAAQABAAEAAQABAArACsAKwArACsAKwArACsAKwArACsABAAEAAQABAAEAAQABABQAFAAUABQAA0ADQANAA0AHgBLAEsASwBLAEsASwBLAEsASwBLACsADQArAB4AKwArAAQABAAEAAQAUABQAB4AUAArACsAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEACsAKwAEAAQABAAEAAQABAAEAAQABAAOAA0ADQATABMAHgAeAB4ADQANAA0ADQANAA0ADQANAA0ADQANAA0ADQANAA0AUABQAFAAUAAEAAQAKwArAAQADQANAB4AUAArACsAKwArACsAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArACsAKwAOAA4ADgAOAA4ADgAOAA4ADgAOAA4ADgAOACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXABcAFwAXAArACsAKwAqACoAKgAqACoAKgAqACoAKgAqACoAKgAqACoAKgArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAXABcAA0ADQANACoASwBLAEsASwBLAEsASwBLAEsASwBQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwBQAFAABAAEAAQABAAEAAQABAAEAAQABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAFAABAAEAAQABAAOAB4ADQANAA0ADQAOAB4ABAArACsAKwArACsAKwArACsAUAAEAAQABAAEAAQABAAEAAQABAAEAAQAUABQAFAAUAArACsAUABQAFAAUAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAA0ADQANACsADgAOAA4ADQANACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAABAAEAAQABAAEAAQABAAEACsABAAEAAQABAAEAAQABAAEAFAADQANAA0ADQANACsAKwArACsAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwAOABMAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQACsAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAArACsAKwAEACsABAAEACsABAAEAAQABAAEAAQABABQAAQAKwArACsAKwArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsADQANAA0ADQANACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAASABIAEgAQwBDAEMAUABQAFAAUABDAFAAUABQAEgAQwBIAEMAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAASABDAEMAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABIAEMAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAEsASwBLAEsASwBLAEsASwBLAEsAKwArACsAKwANAA0AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArAAQABAAEAAQABAANACsAKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAEAAQABAAEAAQABAAEAA0ADQANAB4AHgAeAB4AHgAeAFAAUABQAFAADQAeACsAKwArACsAKwArACsAKwArACsASwBLAEsASwBLAEsASwBLAEsASwArAFAAUABQAFAAUABQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAUAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsABAAEAAQABABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAEcARwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwArACsAKwArACsAKwArACsAKwArACsAKwArAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwBQAFAAUABQAFAAUABQAFAAUABQACsAKwAeAAQABAANAAQABAAEAAQAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACsAKwArACsAKwArACsAKwArACsAHgAeAB4AHgAeAB4AHgArACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4ABAAEAAQABAAEAB4AHgAeAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQAHgAeAAQABAAEAAQABAAEAAQAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAEAAQABAAEAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAB4AHgAEAAQABAAeACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwArACsAKwArACsAKwArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAKwArACsAKwArACsAKwArACsAKwArACsAKwArAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArAFAAUAArACsAUAArACsAUABQACsAKwBQAFAAUABQACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AKwBQACsAUABQAFAAUABQAFAAUAArAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAKwAeAB4AUABQAFAAUABQACsAUAArACsAKwBQAFAAUABQAFAAUABQACsAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgArACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAAeAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAFAAUABQAFAAUABQAFAAUABQAFAAUAAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAHgAeAB4AHgAeAB4AHgAeAB4AKwArAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsASwBLAEsABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAB4AHgAeAB4ABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAB4AHgAeAB4AHgAeAB4AHgAEAB4AHgAeAB4AHgAeAB4AHgAeAB4ABAAeAB4ADQANAA0ADQAeACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAAQABAAEAAQABAArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsABAAEAAQABAAEAAQABAArAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArACsABAAEAAQABAAEAAQABAArAAQABAArAAQABAAEAAQABAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwBQAFAAUABQAFAAKwArAFAAUABQAFAAUABQAFAAUABQAAQABAAEAAQABAAEAAQAKwArACsAKwArACsAKwArACsAHgAeAB4AHgAEAAQABAAEAAQABAAEACsAKwArACsAKwBLAEsASwBLAEsASwBLAEsASwBLACsAKwArACsAFgAWAFAAUABQAFAAKwBQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArAFAAUAArAFAAKwArAFAAKwBQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUAArAFAAKwBQACsAKwArACsAKwArAFAAKwArACsAKwBQACsAUAArAFAAKwBQAFAAUAArAFAAUAArAFAAKwArAFAAKwBQACsAUAArAFAAKwBQACsAUABQACsAUAArACsAUABQAFAAUAArAFAAUABQAFAAUABQAFAAKwBQAFAAUABQACsAUABQAFAAUAArAFAAKwBQAFAAUABQAFAAUABQAFAAUABQACsAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQACsAKwArACsAKwBQAFAAUAArAFAAUABQAFAAUAArAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUABQAFAAUAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArAB4AHgArACsAKwArACsAKwArACsAKwArACsAKwArACsATwBPAE8ATwBPAE8ATwBPAE8ATwBPAE8ATwAlACUAJQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAeACUAHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHgAeACUAJQAlACUAHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdAB0AHQAdACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACkAKQApACkAKQApACkAKQApACkAKQApACkAKQApACkAKQApACkAKQApACkAKQApACkAKQAlACUAJQAlACUAIAAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlAB4AHgAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAHgAeACUAJQAlACUAJQAeACUAJQAlACUAJQAgACAAIAAlACUAIAAlACUAIAAgACAAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAIQAhACEAIQAhACUAJQAgACAAJQAlACAAIAAgACAAIAAgACAAIAAgACAAIAAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAIAAgACAAIAAlACUAJQAlACAAJQAgACAAIAAgACAAIAAgACAAIAAlACUAJQAgACUAJQAlACUAIAAgACAAJQAgACAAIAAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAeACUAHgAlAB4AJQAlACUAJQAlACAAJQAlACUAJQAeACUAHgAeACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAHgAeAB4AHgAeAB4AHgAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlAB4AHgAeAB4AHgAeAB4AHgAeAB4AJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAIAAgACUAJQAlACUAIAAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAIAAlACUAJQAlACAAIAAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAeAB4AHgAeAB4AHgAeAB4AJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlAB4AHgAeAB4AHgAeACUAJQAlACUAJQAlACUAIAAgACAAJQAlACUAIAAgACAAIAAgAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AFwAXABcAFQAVABUAHgAeAB4AHgAlACUAJQAgACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAIAAgACAAJQAlACUAJQAlACUAJQAlACUAIAAlACUAJQAlACUAJQAlACUAJQAlACUAIAAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAlACUAJQAlACUAJQAlACUAJQAlACUAJQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAlACUAJQAlAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AJQAlACUAJQAlACUAJQAlAB4AHgAeAB4AHgAeAB4AHgAeAB4AJQAlACUAJQAlACUAHgAeAB4AHgAeAB4AHgAeACUAJQAlACUAJQAlACUAJQAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeAB4AHgAeACUAJQAlACUAJQAlACUAJQAlACUAJQAlACAAIAAgACAAIAAlACAAIAAlACUAJQAlACUAJQAgACUAJQAlACUAJQAlACUAJQAlACAAIAAgACAAIAAgACAAIAAgACAAJQAlACUAIAAgACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACAAIAAgACAAIAAgACAAIAAgACAAIAAgACAAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACsAKwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAJQAlACUAJQAlACUAJQAlACUAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAJQAlACUAJQAlACUAJQAlACUAJQAlAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAVwBXAFcAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQAlACUAJQArAAQAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAAEAAQABAArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsAKwArACsA"
    }, function (A, e, t) {
        "use strict";
        t.r(e);
        var r = {
            VECTOR: 0,
            BEZIER_CURVE: 1,
            CIRCLE: 2
        };

        function n(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var B = /^#([a-f0-9]{3})$/i,
            s = function (A) {
                var e = A.match(B);
                return !!e && [parseInt(e[1][0] + e[1][0], 16), parseInt(e[1][1] + e[1][1], 16), parseInt(e[1][2] + e[1][2], 16), null]
            },
            o = /^#([a-f0-9]{6})$/i,
            a = function (A) {
                var e = A.match(o);
                return !!e && [parseInt(e[1].substring(0, 2), 16), parseInt(e[1].substring(2, 4), 16), parseInt(e[1].substring(4, 6), 16), null]
            },
            i = /^rgb\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*\)$/,
            c = function (A) {
                var e = A.match(i);
                return !!e && [Number(e[1]), Number(e[2]), Number(e[3]), null]
            },
            Q = /^rgba\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d?\.?\d+)\s*\)$/,
            l = function (A) {
                var e = A.match(Q);
                return !!(e && e.length > 4) && [Number(e[1]), Number(e[2]), Number(e[3]), Number(e[4])]
            },
            w = function (A) {
                return [Math.min(A[0], 255), Math.min(A[1], 255), Math.min(A[2], 255), A.length > 3 ? A[3] : null]
            },
            u = function (A) {
                return g[A.toLowerCase()] || !1
            },
            U = function () {
                function A(e) {
                    ! function (A, e) {
                        if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                    }(this, A);
                    var t = function (A, e) {
                        return function (A) {
                            if (Array.isArray(A)) return A
                        }(A) || function (A, e) {
                            var t = [],
                                r = !0,
                                n = !1,
                                B = void 0;
                            try {
                                for (var s, o = A[Symbol.iterator](); !(r = (s = o.next()).done) && (t.push(s.value), !e || t.length !== e); r = !0);
                            } catch (A) {
                                n = !0, B = A
                            } finally {
                                try {
                                    r || null == o.return || o.return()
                                } finally {
                                    if (n) throw B
                                }
                            }
                            return t
                        }(A, e) || function () {
                            throw new TypeError("Invalid attempt to destructure non-iterable instance")
                        }()
                    }(Array.isArray(e) ? w(e) : s(e) || c(e) || l(e) || u(e) || a(e) || [0, 0, 0, null], 4),
                        r = t[0],
                        n = t[1],
                        B = t[2],
                        o = t[3];
                    this.r = r, this.g = n, this.b = B, this.a = o
                }
                return function (A, e, t) {
                    e && n(A.prototype, e)
                }(A, [{
                    key: "isTransparent",
                    value: function () {
                        return 0 === this.a
                    }
                }, {
                    key: "toString",
                    value: function () {
                        return null !== this.a && 1 !== this.a ? "rgba(".concat(this.r, ",").concat(this.g, ",").concat(this.b, ",").concat(this.a, ")") : "rgb(".concat(this.r, ",").concat(this.g, ",").concat(this.b, ")")
                    }
                }]), A
            }(),
            g = {
                transparent: [0, 0, 0, 0],
                aliceblue: [240, 248, 255, null],
                antiquewhite: [250, 235, 215, null],
                aqua: [0, 255, 255, null],
                aquamarine: [127, 255, 212, null],
                azure: [240, 255, 255, null],
                beige: [245, 245, 220, null],
                bisque: [255, 228, 196, null],
                black: [0, 0, 0, null],
                blanchedalmond: [255, 235, 205, null],
                blue: [0, 0, 255, null],
                blueviolet: [138, 43, 226, null],
                brown: [165, 42, 42, null],
                burlywood: [222, 184, 135, null],
                cadetblue: [95, 158, 160, null],
                chartreuse: [127, 255, 0, null],
                chocolate: [210, 105, 30, null],
                coral: [255, 127, 80, null],
                cornflowerblue: [100, 149, 237, null],
                cornsilk: [255, 248, 220, null],
                crimson: [220, 20, 60, null],
                cyan: [0, 255, 255, null],
                darkblue: [0, 0, 139, null],
                darkcyan: [0, 139, 139, null],
                darkgoldenrod: [184, 134, 11, null],
                darkgray: [169, 169, 169, null],
                darkgreen: [0, 100, 0, null],
                darkgrey: [169, 169, 169, null],
                darkkhaki: [189, 183, 107, null],
                darkmagenta: [139, 0, 139, null],
                darkolivegreen: [85, 107, 47, null],
                darkorange: [255, 140, 0, null],
                darkorchid: [153, 50, 204, null],
                darkred: [139, 0, 0, null],
                darksalmon: [233, 150, 122, null],
                darkseagreen: [143, 188, 143, null],
                darkslateblue: [72, 61, 139, null],
                darkslategray: [47, 79, 79, null],
                darkslategrey: [47, 79, 79, null],
                darkturquoise: [0, 206, 209, null],
                darkviolet: [148, 0, 211, null],
                deeppink: [255, 20, 147, null],
                deepskyblue: [0, 191, 255, null],
                dimgray: [105, 105, 105, null],
                dimgrey: [105, 105, 105, null],
                dodgerblue: [30, 144, 255, null],
                firebrick: [178, 34, 34, null],
                floralwhite: [255, 250, 240, null],
                forestgreen: [34, 139, 34, null],
                fuchsia: [255, 0, 255, null],
                gainsboro: [220, 220, 220, null],
                ghostwhite: [248, 248, 255, null],
                gold: [255, 215, 0, null],
                goldenrod: [218, 165, 32, null],
                gray: [128, 128, 128, null],
                green: [0, 128, 0, null],
                greenyellow: [173, 255, 47, null],
                grey: [128, 128, 128, null],
                honeydew: [240, 255, 240, null],
                hotpink: [255, 105, 180, null],
                indianred: [205, 92, 92, null],
                indigo: [75, 0, 130, null],
                ivory: [255, 255, 240, null],
                khaki: [240, 230, 140, null],
                lavender: [230, 230, 250, null],
                lavenderblush: [255, 240, 245, null],
                lawngreen: [124, 252, 0, null],
                lemonchiffon: [255, 250, 205, null],
                lightblue: [173, 216, 230, null],
                lightcoral: [240, 128, 128, null],
                lightcyan: [224, 255, 255, null],
                lightgoldenrodyellow: [250, 250, 210, null],
                lightgray: [211, 211, 211, null],
                lightgreen: [144, 238, 144, null],
                lightgrey: [211, 211, 211, null],
                lightpink: [255, 182, 193, null],
                lightsalmon: [255, 160, 122, null],
                lightseagreen: [32, 178, 170, null],
                lightskyblue: [135, 206, 250, null],
                lightslategray: [119, 136, 153, null],
                lightslategrey: [119, 136, 153, null],
                lightsteelblue: [176, 196, 222, null],
                lightyellow: [255, 255, 224, null],
                lime: [0, 255, 0, null],
                limegreen: [50, 205, 50, null],
                linen: [250, 240, 230, null],
                magenta: [255, 0, 255, null],
                maroon: [128, 0, 0, null],
                mediumaquamarine: [102, 205, 170, null],
                mediumblue: [0, 0, 205, null],
                mediumorchid: [186, 85, 211, null],
                mediumpurple: [147, 112, 219, null],
                mediumseagreen: [60, 179, 113, null],
                mediumslateblue: [123, 104, 238, null],
                mediumspringgreen: [0, 250, 154, null],
                mediumturquoise: [72, 209, 204, null],
                mediumvioletred: [199, 21, 133, null],
                midnightblue: [25, 25, 112, null],
                mintcream: [245, 255, 250, null],
                mistyrose: [255, 228, 225, null],
                moccasin: [255, 228, 181, null],
                navajowhite: [255, 222, 173, null],
                navy: [0, 0, 128, null],
                oldlace: [253, 245, 230, null],
                olive: [128, 128, 0, null],
                olivedrab: [107, 142, 35, null],
                orange: [255, 165, 0, null],
                orangered: [255, 69, 0, null],
                orchid: [218, 112, 214, null],
                palegoldenrod: [238, 232, 170, null],
                palegreen: [152, 251, 152, null],
                paleturquoise: [175, 238, 238, null],
                palevioletred: [219, 112, 147, null],
                papayawhip: [255, 239, 213, null],
                peachpuff: [255, 218, 185, null],
                peru: [205, 133, 63, null],
                pink: [255, 192, 203, null],
                plum: [221, 160, 221, null],
                powderblue: [176, 224, 230, null],
                purple: [128, 0, 128, null],
                rebeccapurple: [102, 51, 153, null],
                red: [255, 0, 0, null],
                rosybrown: [188, 143, 143, null],
                royalblue: [65, 105, 225, null],
                saddlebrown: [139, 69, 19, null],
                salmon: [250, 128, 114, null],
                sandybrown: [244, 164, 96, null],
                seagreen: [46, 139, 87, null],
                seashell: [255, 245, 238, null],
                sienna: [160, 82, 45, null],
                silver: [192, 192, 192, null],
                skyblue: [135, 206, 235, null],
                slateblue: [106, 90, 205, null],
                slategray: [112, 128, 144, null],
                slategrey: [112, 128, 144, null],
                snow: [255, 250, 250, null],
                springgreen: [0, 255, 127, null],
                steelblue: [70, 130, 180, null],
                tan: [210, 180, 140, null],
                teal: [0, 128, 128, null],
                thistle: [216, 191, 216, null],
                tomato: [255, 99, 71, null],
                turquoise: [64, 224, 208, null],
                violet: [238, 130, 238, null],
                wheat: [245, 222, 179, null],
                white: [255, 255, 255, null],
                whitesmoke: [245, 245, 245, null],
                yellow: [255, 255, 0, null],
                yellowgreen: [154, 205, 50, null]
            },
            F = new U([0, 0, 0, 0]),
            C = function (A) {
                switch (A) {
                    case "underline":
                        return 1;
                    case "overline":
                        return 2;
                    case "line-through":
                        return 3
                }
                return 4
            },
            h = function (A) {
                var e = function (A) {
                    return "none" === A ? null : A.split(" ").map(C)
                }(A.textDecorationLine ? A.textDecorationLine : A.textDecoration);
                return null === e ? null : {
                    textDecorationLine: e,
                    textDecorationColor: A.textDecorationColor ? new U(A.textDecorationColor) : null,
                    textDecorationStyle: function (A) {
                        switch (A) {
                            case "double":
                                return 1;
                            case "dotted":
                                return 2;
                            case "dashed":
                                return 3;
                            case "wavy":
                                return 4
                        }
                        return 0
                    }(A.textDecorationStyle)
                }
            };

        function d(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var H = function (A, e) {
            var t = Math.max.apply(null, A.colorStops.map(function (A) {
                return A.stop
            })),
                r = 1 / Math.max(1, t);
            A.colorStops.forEach(function (A) {
                e.addColorStop(Math.floor(Math.max(0, r * A.stop)), A.color.toString())
            })
        },
            f = function () {
                function A(e) {
                    ! function (A, e) {
                        if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                    }(this, A), this.canvas = e || document.createElement("canvas")
                }
                return function (A, e, t) {
                    e && d(A.prototype, e)
                }(A, [{
                    key: "render",
                    value: function (A) {
                        this.ctx = this.canvas.getContext("2d"), this.options = A, this.canvas.width = Math.floor(A.width * A.scale), this.canvas.height = Math.floor(A.height * A.scale), this.canvas.style.width = "".concat(A.width, "px"), this.canvas.style.height = "".concat(A.height, "px"), this.ctx.scale(this.options.scale, this.options.scale), this.ctx.translate(-A.x, -A.y), this.ctx.textBaseline = "bottom", A.logger.log("Canvas renderer initialized (".concat(A.width, "x").concat(A.height, " at ").concat(A.x, ",").concat(A.y, ") with scale ").concat(this.options.scale))
                    }
                }, {
                    key: "clip",
                    value: function (A, e) {
                        var t = this;
                        A.length && (this.ctx.save(), A.forEach(function (A) {
                            t.path(A), t.ctx.clip()
                        })), e(), A.length && this.ctx.restore()
                    }
                }, {
                    key: "drawImage",
                    value: function (A, e, t) {
                        this.ctx.drawImage(A, e.left, e.top, e.width, e.height, t.left, t.top, t.width, t.height)
                    }
                }, {
                    key: "drawShape",
                    value: function (A, e) {
                        this.path(A), this.ctx.fillStyle = e.toString(), this.ctx.fill()
                    }
                }, {
                    key: "fill",
                    value: function (A) {
                        this.ctx.fillStyle = A.toString(), this.ctx.fill()
                    }
                }, {
                    key: "getTarget",
                    value: function () {
                        return this.canvas.getContext("2d").setTransform(1, 0, 0, 1, 0, 0), Promise.resolve(this.canvas)
                    }
                }, {
                    key: "path",
                    value: function (A) {
                        var e = this;
                        this.ctx.beginPath(), Array.isArray(A) ? A.forEach(function (A, t) {
                            var n = A.type === r.VECTOR ? A : A.start;
                            0 === t ? e.ctx.moveTo(n.x, n.y) : e.ctx.lineTo(n.x, n.y), A.type === r.BEZIER_CURVE && e.ctx.bezierCurveTo(A.startControl.x, A.startControl.y, A.endControl.x, A.endControl.y, A.end.x, A.end.y)
                        }) : this.ctx.arc(A.x + A.radius, A.y + A.radius, A.radius, 0, 2 * Math.PI, !0), this.ctx.closePath()
                    }
                }, {
                    key: "rectangle",
                    value: function (A, e, t, r, n) {
                        this.ctx.fillStyle = n.toString(), this.ctx.fillRect(A, e, t, r)
                    }
                }, {
                    key: "renderLinearGradient",
                    value: function (A, e) {
                        var t = this.ctx.createLinearGradient(A.left + e.direction.x1, A.top + e.direction.y1, A.left + e.direction.x0, A.top + e.direction.y0);
                        H(e, t), this.ctx.fillStyle = t, this.ctx.fillRect(A.left, A.top, A.width, A.height)
                    }
                }, {
                    key: "renderRadialGradient",
                    value: function (A, e) {
                        var t = this,
                            r = A.left + e.center.x,
                            n = A.top + e.center.y,
                            B = this.ctx.createRadialGradient(r, n, 0, r, n, e.radius.x);
                        if (B)
                            if (H(e, B), this.ctx.fillStyle = B, e.radius.x !== e.radius.y) {
                                var s = A.left + .5 * A.width,
                                    o = A.top + .5 * A.height,
                                    a = e.radius.y / e.radius.x,
                                    i = 1 / a;
                                this.transform(s, o, [1, 0, 0, a, 0, 0], function () {
                                    return t.ctx.fillRect(A.left, i * (A.top - o) + o, A.width, A.height * i)
                                })
                            } else this.ctx.fillRect(A.left, A.top, A.width, A.height)
                    }
                }, {
                    key: "renderRepeat",
                    value: function (A, e, t, r, n) {
                        this.path(A), this.ctx.fillStyle = this.ctx.createPattern(this.resizeImage(e, t), "repeat"), this.ctx.translate(r, n), this.ctx.fill(), this.ctx.translate(-r, -n)
                    }
                }, {
                    key: "renderTextNode",
                    value: function (A, e, t, r, n) {
                        var B = this;
                        this.ctx.font = [t.fontStyle, t.fontVariant, t.fontWeight, t.fontSize, t.fontFamily].join(" "), A.forEach(function (A) {
                            if (B.ctx.fillStyle = e.toString(), n && A.text.trim().length ? (n.slice(0).reverse().forEach(function (e) {
                                B.ctx.shadowColor = e.color.toString(), B.ctx.shadowOffsetX = e.offsetX * B.options.scale, B.ctx.shadowOffsetY = e.offsetY * B.options.scale, B.ctx.shadowBlur = e.blur, B.ctx.fillText(A.text, A.bounds.left, A.bounds.top + A.bounds.height)
                            }), B.ctx.shadowColor = "", B.ctx.shadowOffsetX = 0, B.ctx.shadowOffsetY = 0, B.ctx.shadowBlur = 0) : B.ctx.fillText(A.text, A.bounds.left, A.bounds.top + A.bounds.height), null !== r) {
                                var s = r.textDecorationColor || e;
                                r.textDecorationLine.forEach(function (e) {
                                    switch (e) {
                                        case 1:
                                            var r = B.options.fontMetrics.getMetrics(t).baseline;
                                            B.rectangle(A.bounds.left, Math.round(A.bounds.top + r), A.bounds.width, 1, s);
                                            break;
                                        case 2:
                                            B.rectangle(A.bounds.left, Math.round(A.bounds.top), A.bounds.width, 1, s);
                                            break;
                                        case 3:
                                            var n = B.options.fontMetrics.getMetrics(t).middle;
                                            B.rectangle(A.bounds.left, Math.ceil(A.bounds.top + n), A.bounds.width, 1, s)
                                    }
                                })
                            }
                        })
                    }
                }, {
                    key: "resizeImage",
                    value: function (A, e) {
                        if (A.width === e.width && A.height === e.height) return A;
                        var t = this.canvas.ownerDocument.createElement("canvas");
                        return t.width = e.width, t.height = e.height, t.getContext("2d").drawImage(A, 0, 0, A.width, A.height, 0, 0, e.width, e.height), t
                    }
                }, {
                    key: "setOpacity",
                    value: function (A) {
                        this.ctx.globalAlpha = A
                    }
                }, {
                    key: "transform",
                    value: function (A, e, t, r) {
                        this.ctx.save(), this.ctx.translate(A, e), this.ctx.transform(t[0], t[1], t[2], t[3], t[4], t[5]), this.ctx.translate(-A, -e), r(), this.ctx.restore()
                    }
                }]), A
            }();

        function E(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var p = function () {
            function A(e, t, r) {
                ! function (A, e) {
                    if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                }(this, A), this.enabled = "undefined" != typeof window && e, this.start = r || Date.now(), this.id = t
            }
            return function (A, e, t) {
                e && E(A.prototype, e)
            }(A, [{
                key: "child",
                value: function (e) {
                    return new A(this.enabled, e, this.start)
                }
            }, {
                key: "log",
                value: function () {
                    if (this.enabled && window.console && window.console.log) {
                        for (var A = arguments.length, e = new Array(A), t = 0; t < A; t++) e[t] = arguments[t];
                        Function.prototype.bind.call(window.console.log, window.console).apply(window.console, [Date.now() - this.start + "ms", this.id ? "html2canvas (".concat(this.id, "):") : "html2canvas:"].concat([].slice.call(e, 0)))
                    }
                }
            }, {
                key: "error",
                value: function () {
                    if (this.enabled && window.console && window.console.error) {
                        for (var A = arguments.length, e = new Array(A), t = 0; t < A; t++) e[t] = arguments[t];
                        Function.prototype.bind.call(window.console.error, window.console).apply(window.console, [Date.now() - this.start + "ms", this.id ? "html2canvas (".concat(this.id, "):") : "html2canvas:"].concat([].slice.call(e, 0)))
                    }
                }
            }]), A
        }(),
            K = function (A, e) {
                return 0 != (A & e)
            },
            m = function (A, e) {
                return Math.sqrt(A * A + e * e)
            },
            b = function (A, e) {
                for (var t = A.length - 1; t >= 0; t--) {
                    var r = A.item(t);
                    "content" !== r && e.style.setProperty(r, A.getPropertyValue(r))
                }
                return e
            };

        function N(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var y = {
            PX: 0,
            PERCENTAGE: 1
        },
            v = function () {
                function A(e) {
                    ! function (A, e) {
                        if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                    }(this, A), this.type = "%" === e.substr(e.length - 1) ? y.PERCENTAGE : y.PX;
                    var t = parseFloat(e);
                    this.value = isNaN(t) ? 0 : t
                }
                return function (A, e, t) {
                    e && N(A.prototype, e), t && N(A, t)
                }(A, [{
                    key: "isPercentage",
                    value: function () {
                        return this.type === y.PERCENTAGE
                    }
                }, {
                    key: "getAbsoluteValue",
                    value: function (A) {
                        return this.isPercentage() ? A * (this.value / 100) : this.value
                    }
                }], [{
                    key: "create",
                    value: function (e) {
                        return new A(e)
                    }
                }]), A
            }(),
            I = function (A, e, t) {
                switch (t) {
                    case "px":
                    case "%":
                        return new v(e + t);
                    case "em":
                    case "rem":
                        var r = new v(e);
                        return r.value *= "em" === t ? parseFloat(A.style.font.fontSize) : function A(e) {
                            var t = e.parent;
                            return t ? A(t) : parseFloat(e.style.font.fontSize)
                        }(A), r;
                    default:
                        return new v("0")
                }
            },
            D = function A(e, t) {
                ! function (A, e) {
                    if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                }(this, A), this.width = e, this.height = t
            },
            M = function A(e, t) {
                ! function (A, e) {
                    if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                }(this, A), this.type = r.VECTOR, this.x = e, this.y = t
            };

        function T(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var S = function (A, e, t) {
            return new M(A.x + (e.x - A.x) * t, A.y + (e.y - A.y) * t)
        },
            X = function () {
                function A(e, t, n, B) {
                    ! function (A, e) {
                        if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                    }(this, A), this.type = r.BEZIER_CURVE, this.start = e, this.startControl = t, this.endControl = n, this.end = B
                }
                return function (A, e, t) {
                    e && T(A.prototype, e)
                }(A, [{
                    key: "subdivide",
                    value: function (e, t) {
                        var r = S(this.start, this.startControl, e),
                            n = S(this.startControl, this.endControl, e),
                            B = S(this.endControl, this.end, e),
                            s = S(r, n, e),
                            o = S(n, B, e),
                            a = S(s, o, e);
                        return t ? new A(this.start, r, s, a) : new A(a, o, B, this.end)
                    }
                }, {
                    key: "reverse",
                    value: function () {
                        return new A(this.end, this.endControl, this.startControl, this.start)
                    }
                }]), A
            }();

        function z(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var L = function () {
            function A(e, t, r, n) {
                ! function (A, e) {
                    if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                }(this, A), this.left = e, this.top = t, this.width = r, this.height = n
            }
            return function (A, e, t) {
                t && z(A, t)
            }(A, 0, [{
                key: "fromClientRect",
                value: function (e, t, r) {
                    return new A(e.left + t, e.top + r, e.width, e.height)
                }
            }]), A
        }(),
            O = function (A, e, t) {
                return L.fromClientRect(A.getBoundingClientRect(), e, t)
            },
            x = function (A) {
                var e = A.body,
                    t = A.documentElement;
                if (!e || !t) throw new Error("");
                var r = Math.max(Math.max(e.scrollWidth, t.scrollWidth), Math.max(e.offsetWidth, t.offsetWidth), Math.max(e.clientWidth, t.clientWidth)),
                    n = Math.max(Math.max(e.scrollHeight, t.scrollHeight), Math.max(e.offsetHeight, t.offsetHeight), Math.max(e.clientHeight, t.clientHeight));
                return new L(0, 0, r, n)
            },
            V = function (A, e, t, r) {
                var n = [];
                return A instanceof X ? n.push(A.subdivide(.5, !1)) : n.push(A), t instanceof X ? n.push(t.subdivide(.5, !0)) : n.push(t), r instanceof X ? n.push(r.subdivide(.5, !0).reverse()) : n.push(r), e instanceof X ? n.push(e.subdivide(.5, !1).reverse()) : n.push(e), n
            },
            k = function (A) {
                return [A.topLeftInner, A.topRightInner, A.bottomRightInner, A.bottomLeftInner]
            },
            J = function (A, e, t) {
                var r = t[R.TOP_LEFT][0].getAbsoluteValue(A.width),
                    n = t[R.TOP_LEFT][1].getAbsoluteValue(A.height),
                    B = t[R.TOP_RIGHT][0].getAbsoluteValue(A.width),
                    s = t[R.TOP_RIGHT][1].getAbsoluteValue(A.height),
                    o = t[R.BOTTOM_RIGHT][0].getAbsoluteValue(A.width),
                    a = t[R.BOTTOM_RIGHT][1].getAbsoluteValue(A.height),
                    i = t[R.BOTTOM_LEFT][0].getAbsoluteValue(A.width),
                    c = t[R.BOTTOM_LEFT][1].getAbsoluteValue(A.height),
                    Q = [];
                Q.push((r + B) / A.width), Q.push((i + o) / A.width), Q.push((n + c) / A.height), Q.push((s + a) / A.height);
                var l = Math.max.apply(Math, Q);
                l > 1 && (r /= l, n /= l, B /= l, s /= l, o /= l, a /= l, i /= l, c /= l);
                var w = A.width - B,
                    u = A.height - a,
                    U = A.width - o,
                    g = A.height - c;
                return {
                    topLeftOuter: r > 0 || n > 0 ? _(A.left, A.top, r, n, R.TOP_LEFT) : new M(A.left, A.top),
                    topLeftInner: r > 0 || n > 0 ? _(A.left + e[3].borderWidth, A.top + e[0].borderWidth, Math.max(0, r - e[3].borderWidth), Math.max(0, n - e[0].borderWidth), R.TOP_LEFT) : new M(A.left + e[3].borderWidth, A.top + e[0].borderWidth),
                    topRightOuter: B > 0 || s > 0 ? _(A.left + w, A.top, B, s, R.TOP_RIGHT) : new M(A.left + A.width, A.top),
                    topRightInner: B > 0 || s > 0 ? _(A.left + Math.min(w, A.width + e[3].borderWidth), A.top + e[0].borderWidth, w > A.width + e[3].borderWidth ? 0 : B - e[3].borderWidth, s - e[0].borderWidth, R.TOP_RIGHT) : new M(A.left + A.width - e[1].borderWidth, A.top + e[0].borderWidth),
                    bottomRightOuter: o > 0 || a > 0 ? _(A.left + U, A.top + u, o, a, R.BOTTOM_RIGHT) : new M(A.left + A.width, A.top + A.height),
                    bottomRightInner: o > 0 || a > 0 ? _(A.left + Math.min(U, A.width - e[3].borderWidth), A.top + Math.min(u, A.height + e[0].borderWidth), Math.max(0, o - e[1].borderWidth), a - e[2].borderWidth, R.BOTTOM_RIGHT) : new M(A.left + A.width - e[1].borderWidth, A.top + A.height - e[2].borderWidth),
                    bottomLeftOuter: i > 0 || c > 0 ? _(A.left, A.top + g, i, c, R.BOTTOM_LEFT) : new M(A.left, A.top + A.height),
                    bottomLeftInner: i > 0 || c > 0 ? _(A.left + e[3].borderWidth, A.top + g, Math.max(0, i - e[3].borderWidth), c - e[2].borderWidth, R.BOTTOM_LEFT) : new M(A.left + e[3].borderWidth, A.top + A.height - e[2].borderWidth)
                }
            },
            R = {
                TOP_LEFT: 0,
                TOP_RIGHT: 1,
                BOTTOM_RIGHT: 2,
                BOTTOM_LEFT: 3
            },
            _ = function (A, e, t, r, n) {
                var B = (Math.sqrt(2) - 1) / 3 * 4,
                    s = t * B,
                    o = r * B,
                    a = A + t,
                    i = e + r;
                switch (n) {
                    case R.TOP_LEFT:
                        return new X(new M(A, i), new M(A, i - o), new M(a - s, e), new M(a, e));
                    case R.TOP_RIGHT:
                        return new X(new M(A, e), new M(A + s, e), new M(a, i - o), new M(a, i));
                    case R.BOTTOM_RIGHT:
                        return new X(new M(a, e), new M(a, e + o), new M(A + s, i), new M(A, i));
                    case R.BOTTOM_LEFT:
                    default:
                        return new X(new M(a, i), new M(a - s, i), new M(A, e + o), new M(A, e))
                }
            },
            P = ["top", "right", "bottom", "left"],
            G = function (A) {
                return P.map(function (e) {
                    return new v(A.getPropertyValue("padding-".concat(e)))
                })
            },
            W = {
                BORDER_BOX: 0,
                PADDING_BOX: 1,
                CONTENT_BOX: 2
            },
            Y = W,
            q = function A(e) {
                switch (function (A, e) {
                    if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                }(this, A), e) {
                    case "contain":
                        this.size = 1;
                        break;
                    case "cover":
                        this.size = 2;
                        break;
                    case "auto":
                        this.size = 0;
                        break;
                    default:
                        this.value = new v(e)
                }
            },
            j = new q("auto"),
            Z = function (A, e, t, r) {
                var n = function (A, e) {
                    return new L(A.left + e[3].borderWidth, A.top + e[0].borderWidth, A.width - (e[1].borderWidth + e[3].borderWidth), A.height - (e[0].borderWidth + e[2].borderWidth))
                }(e, r);
                switch (A) {
                    case Y.BORDER_BOX:
                        return e;
                    case Y.CONTENT_BOX:
                        var B = t[3].getAbsoluteValue(e.width),
                            s = t[1].getAbsoluteValue(e.width),
                            o = t[0].getAbsoluteValue(e.width),
                            a = t[2].getAbsoluteValue(e.width);
                        return new L(n.left + B, n.top + o, n.width - B - s, n.height - o - a);
                    case Y.PADDING_BOX:
                    default:
                        return n
                }
            },
            $ = function (A, e, t) {
                return new M(A[0].getAbsoluteValue(t.width - e.width), A[1].getAbsoluteValue(t.height - e.height))
            },
            AA = function (A, e) {
                return {
                    backgroundColor: new U(A.backgroundColor),
                    backgroundImage: rA(A, e),
                    backgroundClip: eA(A.backgroundClip),
                    backgroundOrigin: tA(A.backgroundOrigin)
                }
            },
            eA = function (A) {
                switch (A) {
                    case "padding-box":
                        return W.PADDING_BOX;
                    case "content-box":
                        return W.CONTENT_BOX
                }
                return W.BORDER_BOX
            },
            tA = function (A) {
                switch (A) {
                    case "padding-box":
                        return Y.PADDING_BOX;
                    case "content-box":
                        return Y.CONTENT_BOX
                }
                return Y.BORDER_BOX
            },
            rA = function (A, e) {
                var t = sA(A.backgroundImage).map(function (A) {
                    if ("url" === A.method) {
                        var t = e.loadImage(A.args[0]);
                        A.args = t ? [t] : []
                    }
                    return A
                }),
                    r = A.backgroundPosition.split(","),
                    n = A.backgroundRepeat.split(","),
                    B = A.backgroundSize.split(",");
                return t.map(function (A, e) {
                    var t = (B[e] || "auto").trim().split(" ").map(nA),
                        s = (r[e] || "auto").trim().split(" ").map(BA);
                    return {
                        source: A,
                        repeat: function (A) {
                            switch (("string" == typeof n[e] ? n[e] : n[0]).trim()) {
                                case "no-repeat":
                                    return 1;
                                case "repeat-x":
                                case "repeat no-repeat":
                                    return 2;
                                case "repeat-y":
                                case "no-repeat repeat":
                                    return 3;
                                case "repeat":
                                    return 0
                            }
                            return 0
                        }(),
                        size: t.length < 2 ? [t[0], j] : [t[0], t[1]],
                        position: s.length < 2 ? [s[0], s[0]] : [s[0], s[1]]
                    }
                })
            },
            nA = function (A) {
                return "auto" === A ? j : new q(A)
            },
            BA = function (A) {
                switch (A) {
                    case "bottom":
                    case "right":
                        return new v("100%");
                    case "left":
                    case "top":
                        return new v("0%");
                    case "auto":
                        return new v("0")
                }
                return new v(A)
            },
            sA = function (A) {
                var e = /^\s$/,
                    t = [],
                    r = [],
                    n = "",
                    B = null,
                    s = "",
                    o = 0,
                    a = 0,
                    i = function () {
                        var A = "";
                        if (n) {
                            '"' === s.substr(0, 1) && (s = s.substr(1, s.length - 2)), s && r.push(s.trim());
                            var e = n.indexOf("-", 1) + 1;
                            "-" === n.substr(0, 1) && e > 0 && (A = n.substr(0, e).toLowerCase(), n = n.substr(e)), "none" !== (n = n.toLowerCase()) && t.push({
                                prefix: A,
                                method: n,
                                args: r
                            })
                        }
                        r = [], n = s = ""
                    };
                return A.split("").forEach(function (A) {
                    if (0 !== o || !e.test(A)) {
                        switch (A) {
                            case '"':
                                B ? B === A && (B = null) : B = A;
                                break;
                            case "(":
                                if (B) break;
                                if (0 === o) return void (o = 1);
                                a++;
                                break;
                            case ")":
                                if (B) break;
                                if (1 === o) {
                                    if (0 === a) return o = 0, void i();
                                    a--
                                }
                                break;
                            case ",":
                                if (B) break;
                                if (0 === o) return void i();
                                if (1 === o && 0 === a && !n.match(/^url$/i)) return r.push(s.trim()), void (s = "")
                        }
                        0 === o ? n += A : s += A
                    }
                }), i(), t
            },
            oA = Object.keys({
                TOP: 0,
                RIGHT: 1,
                BOTTOM: 2,
                LEFT: 3
            }).map(function (A) {
                return A.toLowerCase()
            }),
            aA = function (A) {
                return oA.map(function (e) {
                    var t = new U(A.getPropertyValue("border-".concat(e, "-color"))),
                        r = function (A) {
                            switch (A) {
                                case "none":
                                    return 0
                            }
                            return 1
                        }(A.getPropertyValue("border-".concat(e, "-style"))),
                        n = parseFloat(A.getPropertyValue("border-".concat(e, "-width")));
                    return {
                        borderColor: t,
                        borderStyle: r,
                        borderWidth: isNaN(n) ? 0 : n
                    }
                })
            };
        var iA = ["top-left", "top-right", "bottom-right", "bottom-left"],
            cA = function (A) {
                return iA.map(function (e) {
                    var t = function (A, e) {
                        return function (A) {
                            if (Array.isArray(A)) return A
                        }(A) || function (A, e) {
                            var t = [],
                                r = !0,
                                n = !1,
                                B = void 0;
                            try {
                                for (var s, o = A[Symbol.iterator](); !(r = (s = o.next()).done) && (t.push(s.value), !e || t.length !== e); r = !0);
                            } catch (A) {
                                n = !0, B = A
                            } finally {
                                try {
                                    r || null == o.return || o.return()
                                } finally {
                                    if (n) throw B
                                }
                            }
                            return t
                        }(A, e) || function () {
                            throw new TypeError("Invalid attempt to destructure non-iterable instance")
                        }()
                    }(A.getPropertyValue("border-".concat(e, "-radius")).split(" ").map(v.create), 2),
                        r = t[0],
                        n = t[1];
                    return void 0 === n ? [r, r] : [r, n]
                })
            },
            QA = {
                NONE: 1,
                BLOCK: 2,
                INLINE: 4,
                RUN_IN: 8,
                FLOW: 16,
                FLOW_ROOT: 32,
                TABLE: 64,
                FLEX: 128,
                GRID: 256,
                RUBY: 512,
                SUBGRID: 1024,
                LIST_ITEM: 2048,
                TABLE_ROW_GROUP: 4096,
                TABLE_HEADER_GROUP: 8192,
                TABLE_FOOTER_GROUP: 16384,
                TABLE_ROW: 32768,
                TABLE_CELL: 65536,
                TABLE_COLUMN_GROUP: 1 << 17,
                TABLE_COLUMN: 1 << 18,
                TABLE_CAPTION: 1 << 19,
                RUBY_BASE: 1 << 20,
                RUBY_TEXT: 1 << 21,
                RUBY_BASE_CONTAINER: 1 << 22,
                RUBY_TEXT_CONTAINER: 1 << 23,
                CONTENTS: 1 << 24,
                INLINE_BLOCK: 1 << 25,
                INLINE_LIST_ITEM: 1 << 26,
                INLINE_TABLE: 1 << 27,
                INLINE_FLEX: 1 << 28,
                INLINE_GRID: 1 << 29
            },
            lA = function (A, e) {
                return A | function (A) {
                    switch (e) {
                        case "block":
                            return QA.BLOCK;
                        case "inline":
                            return QA.INLINE;
                        case "run-in":
                            return QA.RUN_IN;
                        case "flow":
                            return QA.FLOW;
                        case "flow-root":
                            return QA.FLOW_ROOT;
                        case "table":
                            return QA.TABLE;
                        case "flex":
                            return QA.FLEX;
                        case "grid":
                            return QA.GRID;
                        case "ruby":
                            return QA.RUBY;
                        case "subgrid":
                            return QA.SUBGRID;
                        case "list-item":
                            return QA.LIST_ITEM;
                        case "table-row-group":
                            return QA.TABLE_ROW_GROUP;
                        case "table-header-group":
                            return QA.TABLE_HEADER_GROUP;
                        case "table-footer-group":
                            return QA.TABLE_FOOTER_GROUP;
                        case "table-row":
                            return QA.TABLE_ROW;
                        case "table-cell":
                            return QA.TABLE_CELL;
                        case "table-column-group":
                            return QA.TABLE_COLUMN_GROUP;
                        case "table-column":
                            return QA.TABLE_COLUMN;
                        case "table-caption":
                            return QA.TABLE_CAPTION;
                        case "ruby-base":
                            return QA.RUBY_BASE;
                        case "ruby-text":
                            return QA.RUBY_TEXT;
                        case "ruby-base-container":
                            return QA.RUBY_BASE_CONTAINER;
                        case "ruby-text-container":
                            return QA.RUBY_TEXT_CONTAINER;
                        case "contents":
                            return QA.CONTENTS;
                        case "inline-block":
                            return QA.INLINE_BLOCK;
                        case "inline-list-item":
                            return QA.INLINE_LIST_ITEM;
                        case "inline-table":
                            return QA.INLINE_TABLE;
                        case "inline-flex":
                            return QA.INLINE_FLEX;
                        case "inline-grid":
                            return QA.INLINE_GRID
                    }
                    return QA.NONE
                }()
            },
            wA = function (A) {
                return A.split(" ").reduce(lA, 0)
            },
            uA = function (A) {
                switch (A) {
                    case "left":
                        return 1;
                    case "right":
                        return 2;
                    case "inline-start":
                        return 3;
                    case "inline-end":
                        return 4
                }
                return 0
            },
            UA = function (A) {
                return {
                    fontFamily: A.fontFamily,
                    fontSize: A.fontSize,
                    fontStyle: A.fontStyle,
                    fontVariant: A.fontVariant,
                    fontWeight: function (A) {
                        switch (A) {
                            case "normal":
                                return 400;
                            case "bold":
                                return 700
                        }
                        var e = parseInt(A, 10);
                        return isNaN(e) ? 400 : e
                    }(A.fontWeight)
                }
            },
            gA = function (A) {
                if ("normal" === A) return 0;
                var e = parseFloat(A);
                return isNaN(e) ? 0 : e
            },
            FA = function (A) {
                switch (A) {
                    case "strict":
                        return "strict";
                    case "normal":
                    default:
                        return "normal"
                }
            },
            CA = function (A) {
                switch (A) {
                    case "disc":
                        return 0;
                    case "circle":
                        return 1;
                    case "square":
                        return 2;
                    case "decimal":
                        return 3;
                    case "cjk-decimal":
                        return 4;
                    case "decimal-leading-zero":
                        return 5;
                    case "lower-roman":
                        return 6;
                    case "upper-roman":
                        return 7;
                    case "lower-greek":
                        return 8;
                    case "lower-alpha":
                        return 9;
                    case "upper-alpha":
                        return 10;
                    case "arabic-indic":
                        return 11;
                    case "armenian":
                        return 12;
                    case "bengali":
                        return 13;
                    case "cambodian":
                        return 14;
                    case "cjk-earthly-branch":
                        return 15;
                    case "cjk-heavenly-stem":
                        return 16;
                    case "cjk-ideographic":
                        return 17;
                    case "devanagari":
                        return 18;
                    case "ethiopic-numeric":
                        return 19;
                    case "georgian":
                        return 20;
                    case "gujarati":
                        return 21;
                    case "gurmukhi":
                    case "hebrew":
                        return 22;
                    case "hiragana":
                        return 23;
                    case "hiragana-iroha":
                        return 24;
                    case "japanese-formal":
                        return 25;
                    case "japanese-informal":
                        return 26;
                    case "kannada":
                        return 27;
                    case "katakana":
                        return 28;
                    case "katakana-iroha":
                        return 29;
                    case "khmer":
                        return 30;
                    case "korean-hangul-formal":
                        return 31;
                    case "korean-hanja-formal":
                        return 32;
                    case "korean-hanja-informal":
                        return 33;
                    case "lao":
                        return 34;
                    case "lower-armenian":
                        return 35;
                    case "malayalam":
                        return 36;
                    case "mongolian":
                        return 37;
                    case "myanmar":
                        return 38;
                    case "oriya":
                        return 39;
                    case "persian":
                        return 40;
                    case "simp-chinese-formal":
                        return 41;
                    case "simp-chinese-informal":
                        return 42;
                    case "tamil":
                        return 43;
                    case "telugu":
                        return 44;
                    case "thai":
                        return 45;
                    case "tibetan":
                        return 46;
                    case "trad-chinese-formal":
                        return 47;
                    case "trad-chinese-informal":
                        return 48;
                    case "upper-armenian":
                        return 49;
                    case "disclosure-open":
                        return 50;
                    case "disclosure-closed":
                        return 51;
                    case "none":
                    default:
                        return -1
                }
            },
            hA = function (A) {
                var e = sA(A.getPropertyValue("list-style-image"));
                return {
                    listStyleType: CA(A.getPropertyValue("list-style-type")),
                    listStyleImage: e.length ? e[0] : null,
                    listStylePosition: dA(A.getPropertyValue("list-style-position"))
                }
            },
            dA = function (A) {
                switch (A) {
                    case "inside":
                        return 0;
                    case "outside":
                    default:
                        return 1
                }
            },
            HA = ["top", "right", "bottom", "left"],
            fA = function (A) {
                return HA.map(function (e) {
                    return new v(A.getPropertyValue("margin-".concat(e)))
                })
            },
            EA = {
                VISIBLE: 0,
                HIDDEN: 1,
                SCROLL: 2,
                AUTO: 3
            },
            pA = function (A) {
                switch (A) {
                    case "hidden":
                        return EA.HIDDEN;
                    case "scroll":
                        return EA.SCROLL;
                    case "auto":
                        return EA.AUTO;
                    case "visible":
                    default:
                        return EA.VISIBLE
                }
            },
            KA = function (A) {
                switch (A) {
                    case "break-word":
                        return 1;
                    case "normal":
                    default:
                        return 0
                }
            },
            mA = {
                STATIC: 0,
                RELATIVE: 1,
                ABSOLUTE: 2,
                FIXED: 3,
                STICKY: 4
            },
            bA = function (A) {
                switch (A) {
                    case "relative":
                        return mA.RELATIVE;
                    case "absolute":
                        return mA.ABSOLUTE;
                    case "fixed":
                        return mA.FIXED;
                    case "sticky":
                        return mA.STICKY
                }
                return mA.STATIC
            },
            NA = /^([+-]|\d|\.)$/i,
            yA = function (A) {
                if ("none" === A || "string" != typeof A) return null;
                for (var e = "", t = !1, r = [], n = [], B = 0, s = null, o = function () {
                    e.length && (t ? r.push(parseFloat(e)) : s = new U(e)), t = !1, e = ""
                }, a = function () {
                    r.length && null !== s && n.push({
                        color: s,
                        offsetX: r[0] || 0,
                        offsetY: r[1] || 0,
                        blur: r[2] || 0
                    }), r.splice(0, r.length), s = null
                }, i = 0; i < A.length; i++) {
                    var c = A[i];
                    switch (c) {
                        case "(":
                            e += c, B++;
                            break;
                        case ")":
                            e += c, B--;
                            break;
                        case ",":
                            0 === B ? (o(), a()) : e += c;
                            break;
                        case " ":
                            0 === B ? o() : e += c;
                            break;
                        default:
                            0 === e.length && NA.test(c) && (t = !0), e += c
                    }
                }
                return o(), a(), 0 === n.length ? null : n
            },
            vA = function (A) {
                switch (A) {
                    case "uppercase":
                        return 2;
                    case "lowercase":
                        return 1;
                    case "capitalize":
                        return 3
                }
                return 0
            },
            IA = function (A) {
                return parseFloat(A.trim())
            },
            DA = /(matrix|matrix3d)\((.+)\)/,
            MA = function (A) {
                var e = SA(A.transform || A.webkitTransform || A.mozTransform || A.msTransform || A.oTransform);
                return null === e ? null : {
                    transform: e,
                    transformOrigin: TA(A.transformOrigin || A.webkitTransformOrigin || A.mozTransformOrigin || A.msTransformOrigin || A.oTransformOrigin)
                }
            },
            TA = function (A) {
                if ("string" != typeof A) {
                    var e = new v("0");
                    return [e, e]
                }
                var t = A.split(" ").map(v.create);
                return [t[0], t[1]]
            },
            SA = function (A) {
                if ("none" === A || "string" != typeof A) return null;
                var e = A.match(DA);
                if (e) {
                    if ("matrix" === e[1]) {
                        var t = e[2].split(",").map(IA);
                        return [t[0], t[1], t[2], t[3], t[4], t[5]]
                    }
                    var r = e[2].split(",").map(IA);
                    return [r[0], r[1], r[4], r[5], r[12], r[13]]
                }
                return null
            },
            XA = function (A) {
                switch (A) {
                    case "hidden":
                        return 1;
                    case "collapse":
                        return 2;
                    case "visible":
                    default:
                        return 0
                }
            },
            zA = function (A) {
                switch (A) {
                    case "break-all":
                        return "break-all";
                    case "keep-all":
                        return "keep-all";
                    case "normal":
                    default:
                        return "normal"
                }
            },
            LA = function (A) {
                var e = "auto" === A;
                return {
                    auto: e,
                    order: e ? 0 : parseInt(A, 10)
                }
            };

        function OA(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var xA = function () {
            function A(e) {
                ! function (A, e) {
                    if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                }(this, A), this.element = e
            }
            return function (A, e, t) {
                e && OA(A.prototype, e)
            }(A, [{
                key: "render",
                value: function (A) {
                    var e = this;
                    this.options = A, this.canvas = document.createElement("canvas"), this.ctx = this.canvas.getContext("2d"), this.canvas.width = Math.floor(A.width) * A.scale, this.canvas.height = Math.floor(A.height) * A.scale, this.canvas.style.width = "".concat(A.width, "px"), this.canvas.style.height = "".concat(A.height, "px"), this.ctx.scale(A.scale, A.scale), A.logger.log("ForeignObject renderer initialized (".concat(A.width, "x").concat(A.height, " at ").concat(A.x, ",").concat(A.y, ") with scale ").concat(A.scale));
                    var t = VA(Math.max(A.windowWidth, A.width) * A.scale, Math.max(A.windowHeight, A.height) * A.scale, A.scrollX * A.scale, A.scrollY * A.scale, this.element);
                    return kA(t).then(function (t) {
                        return A.backgroundColor && (e.ctx.fillStyle = A.backgroundColor.toString(), e.ctx.fillRect(0, 0, A.width * A.scale, A.height * A.scale)), e.ctx.drawImage(t, -A.x * A.scale, -A.y * A.scale), e.canvas
                    })
                }
            }]), A
        }(),
            VA = function (A, e, t, r, n) {
                var B = "http://www.w3.org/2000/svg",
                    s = document.createElementNS(B, "svg"),
                    o = document.createElementNS(B, "foreignObject");
                return s.setAttributeNS(null, "width", A), s.setAttributeNS(null, "height", e), o.setAttributeNS(null, "width", "100%"), o.setAttributeNS(null, "height", "100%"), o.setAttributeNS(null, "x", t), o.setAttributeNS(null, "y", r), o.setAttributeNS(null, "externalResourcesRequired", "true"), s.appendChild(o), o.appendChild(n), s
            },
            kA = function (A) {
                return new Promise(function (e, t) {
                    var r = new Image;
                    r.onload = function () {
                        return e(r)
                    }, r.onerror = t, r.src = "data:image/svg+xml;charset=utf-8,".concat(encodeURIComponent((new XMLSerializer).serializeToString(A)))
                })
            },
            JA = function (A) {
                return 0 === A[0] && 255 === A[1] && 0 === A[2] && 255 === A[3]
            },
            RA = {
                get SUPPORT_RANGE_BOUNDS() {
                    var A = function (A) {
                        if (A.createRange) {
                            var e = A.createRange();
                            if (e.getBoundingClientRect) {
                                var t = A.createElement("boundtest");
                                t.style.height = "".concat(123, "px"), t.style.display = "block", A.body.appendChild(t), e.selectNode(t);
                                var r = e.getBoundingClientRect(),
                                    n = Math.round(r.height);
                                if (A.body.removeChild(t), 123 === n) return !0
                            }
                        }
                        return !1
                    }(document);
                    return Object.defineProperty(RA, "SUPPORT_RANGE_BOUNDS", {
                        value: A
                    }), A
                },
                get SUPPORT_SVG_DRAWING() {
                    var A = function (A) {
                        var e = new Image,
                            t = A.createElement("canvas"),
                            r = t.getContext("2d");
                        e.src = "data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg'></svg>";
                        try {
                            r.drawImage(e, 0, 0), t.toDataURL()
                        } catch (A) {
                            return !1
                        }
                        return !0
                    }(document);
                    return Object.defineProperty(RA, "SUPPORT_SVG_DRAWING", {
                        value: A
                    }), A
                },
                get SUPPORT_FOREIGNOBJECT_DRAWING() {
                    var A = "function" == typeof Array.from && "function" == typeof window.fetch ? function (A) {
                        var e = A.createElement("canvas");
                        e.width = 100, e.height = 100;
                        var t = e.getContext("2d");
                        t.fillStyle = "rgb(0, 255, 0)", t.fillRect(0, 0, 100, 100);
                        var r = new Image,
                            n = e.toDataURL();
                        r.src = n;
                        var B = VA(100, 100, 0, 0, r);
                        return t.fillStyle = "red", t.fillRect(0, 0, 100, 100), kA(B).then(function (e) {
                            t.drawImage(e, 0, 0);
                            var r = t.getImageData(0, 0, 100, 100).data;
                            t.fillStyle = "red", t.fillRect(0, 0, 100, 100);
                            var B = A.createElement("div");
                            return B.style.backgroundImage = "url(".concat(n, ")"), B.style.height = "".concat(100, "px"), JA(r) ? kA(VA(100, 100, 0, 0, B)) : Promise.reject(!1)
                        }).then(function (A) {
                            return t.drawImage(A, 0, 0), JA(t.getImageData(0, 0, 100, 100).data)
                        }).catch(function (A) {
                            return !1
                        })
                    }(document) : Promise.resolve(!1);
                    return Object.defineProperty(RA, "SUPPORT_FOREIGNOBJECT_DRAWING", {
                        value: A
                    }), A
                },
                get SUPPORT_CORS_IMAGES() {
                    var A = void 0 !== (new Image).crossOrigin;
                    return Object.defineProperty(RA, "SUPPORT_CORS_IMAGES", {
                        value: A
                    }), A
                },
                get SUPPORT_RESPONSE_TYPE() {
                    var A = "string" == typeof (new XMLHttpRequest).responseType;
                    return Object.defineProperty(RA, "SUPPORT_RESPONSE_TYPE", {
                        value: A
                    }), A
                },
                get SUPPORT_CORS_XHR() {
                    var A = "withCredentials" in new XMLHttpRequest;
                    return Object.defineProperty(RA, "SUPPORT_CORS_XHR", {
                        value: A
                    }), A
                }
            },
            _A = RA,
            PA = t(0),
            GA = function A(e, t) {
                ! function (A, e) {
                    if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                }(this, A), this.text = e, this.bounds = t
            },
            WA = function (A, e, t) {
                var r = A.ownerDocument.createElement("html2canvaswrapper");
                r.appendChild(A.cloneNode(!0));
                var n = A.parentNode;
                if (n) {
                    n.replaceChild(r, A);
                    var B = O(r, e, t);
                    return r.firstChild && n.replaceChild(r.firstChild, r), B
                }
                return new L(0, 0, 0, 0)
            },
            YA = function (A, e, t, r, n) {
                var B = A.ownerDocument.createRange();
                return B.setStart(A, e), B.setEnd(A, e + t), L.fromClientRect(B.getBoundingClientRect(), r, n)
            };

        function qA(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var jA = function () {
            function A(e, t, r) {
                ! function (A, e) {
                    if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                }(this, A), this.text = e, this.parent = t, this.bounds = r
            }
            return function (A, e, t) {
                t && qA(A, t)
            }(A, 0, [{
                key: "fromTextNode",
                value: function (e, t) {
                    var r = $A(e.data, t.style.textTransform);
                    return new A(r, t, function (A, e, t) {
                        for (var r = 0 !== e.style.letterSpacing ? Object(PA.toCodePoints)(A).map(function (A) {
                            return Object(PA.fromCodePoint)(A)
                        }) : function (A, e) {
                            for (var t, r = Object(PA.LineBreaker)(A, {
                                lineBreak: e.style.lineBreak,
                                wordBreak: 1 === e.style.overflowWrap ? "break-word" : e.style.wordBreak
                            }), n = []; !(t = r.next()).done;) n.push(t.value.slice());
                            return n
                        }(A, e), n = r.length, B = t.parentNode ? t.parentNode.ownerDocument.defaultView : null, s = B ? B.pageXOffset : 0, o = B ? B.pageYOffset : 0, a = [], i = 0, c = 0; c < n; c++) {
                            var Q = r[c];
                            if (null !== e.style.textDecoration || Q.trim().length > 0)
                                if (_A.SUPPORT_RANGE_BOUNDS) a.push(new GA(Q, YA(t, i, Q.length, s, o)));
                                else {
                                    var l = t.splitText(Q.length);
                                    a.push(new GA(Q, WA(t, s, o))), t = l
                                } else _A.SUPPORT_RANGE_BOUNDS || (t = t.splitText(Q.length));
                            i += Q.length
                        }
                        return a
                    }(r, t, e))
                }
            }]), A
        }(),
            ZA = /(^|\s|:|-|\(|\))([a-z])/g,
            $A = function (A, e) {
                switch (e) {
                    case 1:
                        return A.toLowerCase();
                    case 3:
                        return A.replace(ZA, Ae);
                    case 2:
                        return A.toUpperCase();
                    default:
                        return A
                }
            };

        function Ae(A, e, t) {
            return A.length > 0 ? e + t.toUpperCase() : A
        }
        var ee = function A(e, t, n) {
            ! function (A, e) {
                if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
            }(this, A), this.type = r.CIRCLE, this.x = e, this.y = t, this.radius = n
        },
            te = new U([42, 42, 42]),
            re = new U([165, 165, 165]),
            ne = new U([222, 222, 222]),
            Be = {
                borderWidth: 1,
                borderColor: re,
                borderStyle: 1
            },
            se = [Be, Be, Be, Be],
            oe = {
                backgroundColor: ne,
                backgroundImage: [],
                backgroundClip: W.PADDING_BOX,
                backgroundOrigin: Y.PADDING_BOX
            },
            ae = new v("50%"),
            ie = [ae, ae],
            ce = [ie, ie, ie, ie],
            Qe = new v("3px"),
            le = [Qe, Qe],
            we = [le, le, le, le],
            ue = function (A) {
                return "radio" === A.type ? ce : we
            },
            Ue = function (A, e) {
                if ("radio" === A.type || "checkbox" === A.type) {
                    if (A.checked) {
                        var t = Math.min(e.bounds.width, e.bounds.height);
                        e.childNodes.push("checkbox" === A.type ? [new M(e.bounds.left + .39363 * t, e.bounds.top + .79 * t), new M(e.bounds.left + .16 * t, e.bounds.top + .5549 * t), new M(e.bounds.left + .27347 * t, e.bounds.top + .44071 * t), new M(e.bounds.left + .39694 * t, e.bounds.top + .5649 * t), new M(e.bounds.left + .72983 * t, e.bounds.top + .23 * t), new M(e.bounds.left + .84 * t, e.bounds.top + .34085 * t), new M(e.bounds.left + .39363 * t, e.bounds.top + .79 * t)] : new ee(e.bounds.left + t / 4, e.bounds.top + t / 4, t / 4))
                    }
                } else he(de(A), A, e, !1)
            },
            ge = function (A, e) {
                he(A.value, A, e, !0)
            },
            Fe = function (A, e) {
                var t = A.options[A.selectedIndex || 0];
                he(t && t.text || "", A, e, !1)
            },
            Ce = function (A) {
                return A.width > A.height ? (A.left += (A.width - A.height) / 2, A.width = A.height) : A.width < A.height && (A.top += (A.height - A.width) / 2, A.height = A.width), A
            },
            he = function (A, e, t, r) {
                var n = e.ownerDocument.body;
                if (A.length > 0 && n) {
                    var B = e.ownerDocument.createElement("html2canvaswrapper");
                    b(e.ownerDocument.defaultView.getComputedStyle(e, null), B), B.style.position = "absolute", B.style.left = "".concat(t.bounds.left, "px"), B.style.top = "".concat(t.bounds.top, "px"), r || (B.style.whiteSpace = "nowrap");
                    var s = e.ownerDocument.createTextNode(A);
                    B.appendChild(s), n.appendChild(B), t.childNodes.push(jA.fromTextNode(s, t)), n.removeChild(B)
                }
            },
            de = function (A) {
                var e = "password" === A.type ? new Array(A.value.length + 1).join("â€¢") : A.value;
                return 0 === e.length ? A.placeholder || "" : e
            },
            He = ["OL", "UL", "MENU"],
            fe = function (A) {
                var e = A.parent;
                if (!e) return null;
                do {
                    if (-1 !== He.indexOf(e.tagName)) return e;
                    e = e.parent
                } while (e);
                return A.parent
            },
            Ee = function (A, e, t) {
                var r = e.style.listStyle;
                if (r) {
                    var n, B = A.ownerDocument.defaultView.getComputedStyle(A, null),
                        s = A.ownerDocument.createElement("html2canvaswrapper");
                    switch (b(B, s), s.style.position = "absolute", s.style.bottom = "auto", s.style.display = "block", s.style.letterSpacing = "normal", r.listStylePosition) {
                        case 1:
                            s.style.left = "auto", s.style.right = "".concat(A.ownerDocument.defaultView.innerWidth - e.bounds.left - e.style.margin[1].getAbsoluteValue(e.bounds.width) + 7, "px"), s.style.textAlign = "right";
                            break;
                        case 0:
                            s.style.left = "".concat(e.bounds.left - e.style.margin[3].getAbsoluteValue(e.bounds.width), "px"), s.style.right = "auto", s.style.textAlign = "left"
                    }
                    var o = e.style.margin[0].getAbsoluteValue(e.bounds.width),
                        a = r.listStyleImage;
                    if (a)
                        if ("url" === a.method) {
                            var i = A.ownerDocument.createElement("img");
                            i.src = a.args[0], s.style.top = "".concat(e.bounds.top - o, "px"), s.style.width = "auto", s.style.height = "auto", s.appendChild(i)
                        } else {
                            var c = .5 * parseFloat(e.style.font.fontSize);
                            s.style.top = "".concat(e.bounds.top - o + e.bounds.height - 1.5 * c, "px"), s.style.width = "".concat(c, "px"), s.style.height = "".concat(c, "px"), s.style.backgroundImage = B.listStyleImage
                        } else "number" == typeof e.listIndex && (n = A.ownerDocument.createTextNode(Me(e.listIndex, r.listStyleType, !0)), s.appendChild(n), s.style.top = "".concat(e.bounds.top - o, "px"));
                    var Q = A.ownerDocument.body;
                    Q.appendChild(s), n ? (e.childNodes.push(jA.fromTextNode(n, e)), Q.removeChild(s)) : e.childNodes.push(new Xe(s, e, t, 0))
                }
            },
            pe = {
                integers: [1e3, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1],
                values: ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"]
            },
            Ke = {
                integers: [9e3, 8e3, 7e3, 6e3, 5e3, 4e3, 3e3, 2e3, 1e3, 900, 800, 700, 600, 500, 400, 300, 200, 100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
                values: ["Õ”", "Õ“", "Õ’", "Õ‘", "Õ", "Õ", "ÕŽ", "Õ", "ÕŒ", "Õ‹", "ÕŠ", "Õ‰", "Õˆ", "Õ‡", "Õ†", "Õ…", "Õ„", "Õƒ", "Õ‚", "Õ", "Õ€", "Ô¿", "Ô¾", "Ô½", "Ô¼", "Ô»", "Ôº", "Ô¹", "Ô¸", "Ô·", "Ô¶", "Ôµ", "Ô´", "Ô³", "Ô²", "Ô±"]
            },
            me = {
                integers: [1e4, 9e3, 8e3, 7e3, 6e3, 5e3, 4e3, 3e3, 2e3, 1e3, 400, 300, 200, 100, 90, 80, 70, 60, 50, 40, 30, 20, 19, 18, 17, 16, 15, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
                values: ["×™×³", "×˜×³", "×—×³", "×–×³", "×•×³", "×”×³", "×“×³", "×’×³", "×‘×³", "××³", "×ª", "×©", "×¨", "×§", "×¦", "×¤", "×¢", "×¡", "× ", "×ž", "×œ", "×›", "×™×˜", "×™×—", "×™×–", "×˜×–", "×˜×•", "×™", "×˜", "×—", "×–", "×•", "×”", "×“", "×’", "×‘", "×"]
            },
            be = {
                integers: [1e4, 9e3, 8e3, 7e3, 6e3, 5e3, 4e3, 3e3, 2e3, 1e3, 900, 800, 700, 600, 500, 400, 300, 200, 100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1],
                values: ["áƒµ", "áƒ°", "áƒ¯", "áƒ´", "áƒ®", "áƒ­", "áƒ¬", "áƒ«", "áƒª", "áƒ©", "áƒ¨", "áƒ§", "áƒ¦", "áƒ¥", "áƒ¤", "áƒ³", "áƒ¢", "áƒ¡", "áƒ ", "áƒŸ", "áƒž", "áƒ", "áƒ²", "áƒœ", "áƒ›", "áƒš", "áƒ™", "áƒ˜", "áƒ—", "áƒ±", "áƒ–", "áƒ•", "áƒ”", "áƒ“", "áƒ’", "áƒ‘", "áƒ"]
            },
            Ne = function (A, e, t, r, n, B) {
                return A < e || A > t ? Me(A, n, B.length > 0) : r.integers.reduce(function (e, t, n) {
                    for (; A >= t;) A -= t, e += r.values[n];
                    return e
                }, "") + B
            },
            ye = function (A, e, t, r) {
                var n = "";
                do {
                    t || A-- , n = r(A) + n, A /= e
                } while (A * e >= e);
                return n
            },
            ve = function (A, e, t, r, n) {
                var B = t - e + 1;
                return (A < 0 ? "-" : "") + (ye(Math.abs(A), B, r, function (A) {
                    return Object(PA.fromCodePoint)(Math.floor(A % B) + e)
                }) + n)
            },
            Ie = function (A, e) {
                var t = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : ". ",
                    r = e.length;
                return ye(Math.abs(A), r, !1, function (A) {
                    return e[Math.floor(A % r)]
                }) + t
            },
            De = function (A, e, t, r, n, B) {
                if (A < -9999 || A > 9999) return Me(A, 4, n.length > 0);
                var s = Math.abs(A),
                    o = n;
                if (0 === s) return e[0] + o;
                for (var a = 0; s > 0 && a <= 4; a++) {
                    var i = s % 10;
                    0 === i && K(B, 1) && "" !== o ? o = e[i] + o : i > 1 || 1 === i && 0 === a || 1 === i && 1 === a && K(B, 2) || 1 === i && 1 === a && K(B, 4) && A > 100 || 1 === i && a > 1 && K(B, 8) ? o = e[i] + (a > 0 ? t[a - 1] : "") + o : 1 === i && a > 0 && (o = t[a - 1] + o), s = Math.floor(s / 10)
                }
                return (A < 0 ? r : "") + o
            },
            Me = function (A, e, t) {
                var r = t ? ". " : "",
                    n = t ? "ã€" : "",
                    B = t ? ", " : "";
                switch (e) {
                    case 0:
                        return "â€¢";
                    case 1:
                        return "â—¦";
                    case 2:
                        return "â—¾";
                    case 5:
                        var s = ve(A, 48, 57, !0, r);
                        return s.length < 4 ? "0".concat(s) : s;
                    case 4:
                        return Ie(A, "ã€‡ä¸€äºŒä¸‰å››äº”å…­ä¸ƒå…«ä¹", n);
                    case 6:
                        return Ne(A, 1, 3999, pe, 3, r).toLowerCase();
                    case 7:
                        return Ne(A, 1, 3999, pe, 3, r);
                    case 8:
                        return ve(A, 945, 969, !1, r);
                    case 9:
                        return ve(A, 97, 122, !1, r);
                    case 10:
                        return ve(A, 65, 90, !1, r);
                    case 11:
                        return ve(A, 1632, 1641, !0, r);
                    case 12:
                    case 49:
                        return Ne(A, 1, 9999, Ke, 3, r);
                    case 35:
                        return Ne(A, 1, 9999, Ke, 3, r).toLowerCase();
                    case 13:
                        return ve(A, 2534, 2543, !0, r);
                    case 14:
                    case 30:
                        return ve(A, 6112, 6121, !0, r);
                    case 15:
                        return Ie(A, "å­ä¸‘å¯…å¯è¾°å·³åˆæœªç”³é…‰æˆŒäº¥", n);
                    case 16:
                        return Ie(A, "ç”²ä¹™ä¸™ä¸æˆŠå·±åºšè¾›å£¬ç™¸", n);
                    case 17:
                    case 48:
                        return De(A, "é›¶ä¸€äºŒä¸‰å››äº”å…­ä¸ƒå…«ä¹", "åç™¾åƒè¬", "è² ", n, 14);
                    case 47:
                        return De(A, "é›¶å£¹è²³åƒè‚†ä¼é™¸æŸ’æŒçŽ–", "æ‹¾ä½°ä»Ÿè¬", "è² ", n, 15);
                    case 42:
                        return De(A, "é›¶ä¸€äºŒä¸‰å››äº”å…­ä¸ƒå…«ä¹", "åç™¾åƒè¬", "è´Ÿ", n, 14);
                    case 41:
                        return De(A, "é›¶å£¹è´°åè‚†ä¼é™†æŸ’æŒçŽ–", "æ‹¾ä½°ä»Ÿè¬", "è´Ÿ", n, 15);
                    case 26:
                        return De(A, "ã€‡ä¸€äºŒä¸‰å››äº”å…­ä¸ƒå…«ä¹", "åç™¾åƒä¸‡", "ãƒžã‚¤ãƒŠã‚¹", n, 0);
                    case 25:
                        return De(A, "é›¶å£±å¼å‚å››ä¼å…­ä¸ƒå…«ä¹", "æ‹¾ç™¾åƒä¸‡", "ãƒžã‚¤ãƒŠã‚¹", n, 7);
                    case 31:
                        return De(A, "ì˜ì¼ì´ì‚¼ì‚¬ì˜¤ìœ¡ì¹ íŒ”êµ¬", "ì‹­ë°±ì²œë§Œ", "ë§ˆì´ë„ˆìŠ¤", B, 7);
                    case 33:
                        return De(A, "é›¶ä¸€äºŒä¸‰å››äº”å…­ä¸ƒå…«ä¹", "åç™¾åƒè¬", "ë§ˆì´ë„ˆìŠ¤", B, 0);
                    case 32:
                        return De(A, "é›¶å£¹è²³åƒå››äº”å…­ä¸ƒå…«ä¹", "æ‹¾ç™¾åƒ", "ë§ˆì´ë„ˆìŠ¤", B, 7);
                    case 18:
                        return ve(A, 2406, 2415, !0, r);
                    case 20:
                        return Ne(A, 1, 19999, be, 3, r);
                    case 21:
                        return ve(A, 2790, 2799, !0, r);
                    case 22:
                        return ve(A, 2662, 2671, !0, r);
                    case 22:
                        return Ne(A, 1, 10999, me, 3, r);
                    case 23:
                        return Ie(A, "ã‚ã„ã†ãˆãŠã‹ããã‘ã“ã•ã—ã™ã›ããŸã¡ã¤ã¦ã¨ãªã«ã¬ã­ã®ã¯ã²ãµã¸ã»ã¾ã¿ã‚€ã‚ã‚‚ã‚„ã‚†ã‚ˆã‚‰ã‚Šã‚‹ã‚Œã‚ã‚ã‚ã‚‘ã‚’ã‚“");
                    case 24:
                        return Ie(A, "ã„ã‚ã¯ã«ã»ã¸ã¨ã¡ã‚Šã¬ã‚‹ã‚’ã‚ã‹ã‚ˆãŸã‚Œãã¤ã­ãªã‚‰ã‚€ã†ã‚ã®ãŠãã‚„ã¾ã‘ãµã“ãˆã¦ã‚ã•ãã‚†ã‚ã¿ã—ã‚‘ã²ã‚‚ã›ã™");
                    case 27:
                        return ve(A, 3302, 3311, !0, r);
                    case 28:
                        return Ie(A, "ã‚¢ã‚¤ã‚¦ã‚¨ã‚ªã‚«ã‚­ã‚¯ã‚±ã‚³ã‚µã‚·ã‚¹ã‚»ã‚½ã‚¿ãƒãƒ„ãƒ†ãƒˆãƒŠãƒ‹ãƒŒãƒãƒŽãƒãƒ’ãƒ•ãƒ˜ãƒ›ãƒžãƒŸãƒ ãƒ¡ãƒ¢ãƒ¤ãƒ¦ãƒ¨ãƒ©ãƒªãƒ«ãƒ¬ãƒ­ãƒ¯ãƒ°ãƒ±ãƒ²ãƒ³", n);
                    case 29:
                        return Ie(A, "ã‚¤ãƒ­ãƒãƒ‹ãƒ›ãƒ˜ãƒˆãƒãƒªãƒŒãƒ«ãƒ²ãƒ¯ã‚«ãƒ¨ã‚¿ãƒ¬ã‚½ãƒ„ãƒãƒŠãƒ©ãƒ ã‚¦ãƒ°ãƒŽã‚ªã‚¯ãƒ¤ãƒžã‚±ãƒ•ã‚³ã‚¨ãƒ†ã‚¢ã‚µã‚­ãƒ¦ãƒ¡ãƒŸã‚·ãƒ±ãƒ’ãƒ¢ã‚»ã‚¹", n);
                    case 34:
                        return ve(A, 3792, 3801, !0, r);
                    case 37:
                        return ve(A, 6160, 6169, !0, r);
                    case 38:
                        return ve(A, 4160, 4169, !0, r);
                    case 39:
                        return ve(A, 2918, 2927, !0, r);
                    case 40:
                        return ve(A, 1776, 1785, !0, r);
                    case 43:
                        return ve(A, 3046, 3055, !0, r);
                    case 44:
                        return ve(A, 3174, 3183, !0, r);
                    case 45:
                        return ve(A, 3664, 3673, !0, r);
                    case 46:
                        return ve(A, 3872, 3881, !0, r);
                    case 3:
                    default:
                        return ve(A, 48, 57, !0, r)
                }
            };

        function Te(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var Se = ["INPUT", "TEXTAREA", "SELECT"],
            Xe = function () {
                function A(e, t, r, n) {
                    var B = this;
                    ! function (A, e) {
                        if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                    }(this, A), this.parent = t, this.tagName = e.tagName, this.index = n, this.childNodes = [], this.listItems = [], "number" == typeof e.start && (this.listStart = e.start);
                    var s = e.ownerDocument.defaultView,
                        o = s.pageXOffset,
                        a = s.pageYOffset,
                        i = s.getComputedStyle(e, null),
                        c = wA(i.display),
                        Q = "radio" === e.type || "checkbox" === e.type,
                        l = bA(i.position);
                    if (this.style = {
                        background: Q ? oe : AA(i, r),
                        border: Q ? se : aA(i),
                        borderRadius: (e instanceof s.HTMLInputElement || e instanceof HTMLInputElement) && Q ? ue(e) : cA(i),
                        color: Q ? te : new U(i.color),
                        display: c,
                        float: uA(i.float),
                        font: UA(i),
                        letterSpacing: gA(i.letterSpacing),
                        listStyle: c === QA.LIST_ITEM ? hA(i) : null,
                        lineBreak: FA(i.lineBreak),
                        margin: fA(i),
                        opacity: parseFloat(i.opacity),
                        overflow: -1 === Se.indexOf(e.tagName) ? pA(i.overflow) : EA.HIDDEN,
                        overflowWrap: KA(i.overflowWrap ? i.overflowWrap : i.wordWrap),
                        padding: G(i),
                        position: l,
                        textDecoration: h(i),
                        textShadow: yA(i.textShadow),
                        textTransform: vA(i.textTransform),
                        transform: MA(i),
                        visibility: XA(i.visibility),
                        wordBreak: zA(i.wordBreak),
                        zIndex: LA(l !== mA.STATIC ? i.zIndex : "auto")
                    }, this.isTransformed() && (e.style.transform = "matrix(1,0,0,1,0,0)"), c === QA.LIST_ITEM) {
                        var w = fe(this);
                        if (w) {
                            var u = w.listItems.length;
                            w.listItems.push(this), this.listIndex = e.hasAttribute("value") && "number" == typeof e.value ? e.value : 0 === u ? "number" == typeof w.listStart ? w.listStart : 1 : w.listItems[u - 1].listIndex + 1
                        }
                    }
                    "IMG" === e.tagName && e.addEventListener("load", function () {
                        B.bounds = O(e, o, a), B.curvedBounds = J(B.bounds, B.style.border, B.style.borderRadius)
                    }), this.image = ze(e, r), this.bounds = Q ? Ce(O(e, o, a)) : O(e, o, a), this.curvedBounds = J(this.bounds, this.style.border, this.style.borderRadius)
                }
                return function (A, e, t) {
                    e && Te(A.prototype, e)
                }(A, [{
                    key: "getClipPaths",
                    value: function () {
                        var A = this.parent ? this.parent.getClipPaths() : [];
                        return this.style.overflow !== EA.VISIBLE ? A.concat([k(this.curvedBounds)]) : A
                    }
                }, {
                    key: "isInFlow",
                    value: function () {
                        return this.isRootElement() && !this.isFloating() && !this.isAbsolutelyPositioned()
                    }
                }, {
                    key: "isVisible",
                    value: function () {
                        return !K(this.style.display, QA.NONE) && this.style.opacity > 0 && 0 === this.style.visibility
                    }
                }, {
                    key: "isAbsolutelyPositioned",
                    value: function () {
                        return this.style.position !== mA.STATIC && this.style.position !== mA.RELATIVE
                    }
                }, {
                    key: "isPositioned",
                    value: function () {
                        return this.style.position !== mA.STATIC
                    }
                }, {
                    key: "isFloating",
                    value: function () {
                        return 0 !== this.style.float
                    }
                }, {
                    key: "isRootElement",
                    value: function () {
                        return null === this.parent
                    }
                }, {
                    key: "isTransformed",
                    value: function () {
                        return null !== this.style.transform
                    }
                }, {
                    key: "isPositionedWithZIndex",
                    value: function () {
                        return this.isPositioned() && !this.style.zIndex.auto
                    }
                }, {
                    key: "isInlineLevel",
                    value: function () {
                        return K(this.style.display, QA.INLINE) || K(this.style.display, QA.INLINE_BLOCK) || K(this.style.display, QA.INLINE_FLEX) || K(this.style.display, QA.INLINE_GRID) || K(this.style.display, QA.INLINE_LIST_ITEM) || K(this.style.display, QA.INLINE_TABLE)
                    }
                }, {
                    key: "isInlineBlockOrInlineTable",
                    value: function () {
                        return K(this.style.display, QA.INLINE_BLOCK) || K(this.style.display, QA.INLINE_TABLE)
                    }
                }]), A
            }(),
            ze = function (A, e) {
                if (A instanceof A.ownerDocument.defaultView.SVGSVGElement || A instanceof SVGSVGElement) {
                    var t = new XMLSerializer;
                    return e.loadImage("data:image/svg+xml,".concat(encodeURIComponent(t.serializeToString(A))))
                }
                switch (A.tagName) {
                    case "IMG":
                        var r = A;
                        return e.loadImage(r.currentSrc || r.src);
                    case "CANVAS":
                        var n = A;
                        return e.loadCanvas(n);
                    case "IFRAME":
                        var B = A.getAttribute("data-html2canvas-internal-iframe-key");
                        if (B) return B
                }
                return null
            };

        function Le(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var Oe = function () {
            function A(e, t, r) {
                ! function (A, e) {
                    if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                }(this, A), this.container = e, this.parent = t, this.contexts = [], this.children = [], this.treatAsRealStackingContext = r
            }
            return function (A, e, t) {
                e && Le(A.prototype, e)
            }(A, [{
                key: "getOpacity",
                value: function () {
                    return this.parent ? this.container.style.opacity * this.parent.getOpacity() : this.container.style.opacity
                }
            }, {
                key: "getRealParentStackingContext",
                value: function () {
                    return !this.parent || this.treatAsRealStackingContext ? this : this.parent.getRealParentStackingContext()
                }
            }]), A
        }(),
            xe = ["SCRIPT", "HEAD", "TITLE", "OBJECT", "BR", "OPTION"],
            Ve = function (A, e) {
                return A.isRootElement() || A.isPositionedWithZIndex() || A.style.opacity < 1 || A.isTransformed() || Je(A, e)
            },
            ke = function (A) {
                return A.isPositioned() || A.isFloating()
            },
            Je = function (A, e) {
                return "BODY" === e.nodeName && A.parent instanceof Xe && A.parent.style.background.backgroundColor.isTransparent()
            };

        function Re(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var _e = function () {
            function A(e) {
                ! function (A, e) {
                    if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                }(this, A), this._data = {}, this._document = e
            }
            return function (A, e, t) {
                e && Re(A.prototype, e)
            }(A, [{
                key: "_parseMetrics",
                value: function (A) {
                    var e = this._document.createElement("div"),
                        t = this._document.createElement("img"),
                        r = this._document.createElement("span"),
                        n = this._document.body;
                    if (!n) throw new Error("");
                    e.style.visibility = "hidden", e.style.fontFamily = A.fontFamily, e.style.fontSize = A.fontSize, e.style.margin = "0", e.style.padding = "0", n.appendChild(e), t.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7", t.width = 1, t.height = 1, t.style.margin = "0", t.style.padding = "0", t.style.verticalAlign = "baseline", r.style.fontFamily = A.fontFamily, r.style.fontSize = A.fontSize, r.style.margin = "0", r.style.padding = "0", r.appendChild(this._document.createTextNode("Hidden Text")), e.appendChild(r), e.appendChild(t);
                    var B = t.offsetTop - r.offsetTop + 2;
                    e.removeChild(r), e.appendChild(this._document.createTextNode("Hidden Text")), e.style.lineHeight = "normal", t.style.verticalAlign = "super";
                    var s = t.offsetTop - e.offsetTop + 2;
                    return n.removeChild(e), {
                        baseline: B,
                        middle: s
                    }
                }
            }, {
                key: "getMetrics",
                value: function (A) {
                    var e = "".concat(A.fontFamily, " ").concat(A.fontSize);
                    return void 0 === this._data[e] && (this._data[e] = this._parseMetrics(A)), this._data[e]
                }
            }]), A
        }(),
            Pe = /([+-]?\d*\.?\d+)(deg|grad|rad|turn)/i;

        function Ge(A, e) {
            if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
        }
        var We = /^(to )?(left|top|right|bottom)( (left|top|right|bottom))?$/i,
            Ye = /^([+-]?\d*\.?\d+)% ([+-]?\d*\.?\d+)%$/i,
            qe = /(px)|%|( 0)$/i,
            je = /^(from|to|color-stop)\((?:([\d.]+)(%)?,\s*)?(.+?)\)$/i,
            Ze = /^\s*(circle|ellipse)?\s*((?:([\d.]+)(px|r?em|%)\s*(?:([\d.]+)(px|r?em|%))?)|closest-side|closest-corner|farthest-side|farthest-corner)?\s*(?:at\s*(?:(left|center|right)|([\d.]+)(px|r?em|%))\s+(?:(top|center|bottom)|([\d.]+)(px|r?em|%)))?(?:\s|$)/i,
            $e = {
                left: new v("0%"),
                top: new v("0%"),
                center: new v("50%"),
                right: new v("100%"),
                bottom: new v("100%")
            },
            At = function (A, e, t) {
                for (var r = [], n = e; n < A.length; n++) {
                    var B = A[n],
                        s = qe.test(B),
                        o = B.lastIndexOf(" "),
                        a = new U(s ? B.substring(0, o) : B),
                        i = s ? new v(B.substring(o + 1)) : n === e ? new v("0%") : n === A.length - 1 ? new v("100%") : null;
                    r.push({
                        color: a,
                        stop: i
                    })
                }
                for (var c = r.map(function (A) {
                    var e = A.color,
                        r = A.stop;
                    return {
                        color: e,
                        stop: 0 === t ? 0 : r ? r.getAbsoluteValue(t) / t : null
                    }
                }), Q = c[0].stop, l = 0; l < c.length; l++)
                    if (null !== Q) {
                        var w = c[l].stop;
                        if (null === w) {
                            for (var u = l; null === c[u].stop;) u++;
                            for (var g = u - l + 1, F = (c[u].stop - Q) / g; l < u; l++) Q = c[l].stop = Q + F
                        } else Q = w
                    }
                return c
            },
            et = function (A, e, t) {
                var r = function (A) {
                    var e = A.match(Pe);
                    if (e) {
                        var t = parseFloat(e[1]);
                        switch (e[2].toLowerCase()) {
                            case "deg":
                                return Math.PI * t / 180;
                            case "grad":
                                return Math.PI / 200 * t;
                            case "rad":
                                return t;
                            case "turn":
                                return 2 * Math.PI * t
                        }
                    }
                    return null
                }(A[0]),
                    n = We.test(A[0]),
                    B = n || null !== r || Ye.test(A[0]),
                    s = B ? null !== r ? rt(t ? r - .5 * Math.PI : r, e) : n ? Bt(A[0], e) : st(A[0], e) : rt(Math.PI, e),
                    o = B ? 1 : 0,
                    a = Math.min(m(Math.abs(s.x0) + Math.abs(s.x1), Math.abs(s.y0) + Math.abs(s.y1)), 2 * e.width, 2 * e.height);
                return new function A(e, t) {
                    Ge(this, A), this.type = 0, this.colorStops = e, this.direction = t
                }(At(A, o, a), s)
            },
            tt = function (A, e, t) {
                var r = e[0].match(Ze),
                    n = r && ("circle" === r[1] || void 0 !== r[3] && void 0 === r[5]) ? 0 : 1,
                    B = {},
                    s = {};
                r && (void 0 !== r[3] && (B.x = I(A, r[3], r[4]).getAbsoluteValue(t.width)), void 0 !== r[5] && (B.y = I(A, r[5], r[6]).getAbsoluteValue(t.height)), r[7] ? s.x = $e[r[7].toLowerCase()] : void 0 !== r[8] && (s.x = I(A, r[8], r[9])), r[10] ? s.y = $e[r[10].toLowerCase()] : void 0 !== r[11] && (s.y = I(A, r[11], r[12])));
                var o = {
                    x: void 0 === s.x ? t.width / 2 : s.x.getAbsoluteValue(t.width),
                    y: void 0 === s.y ? t.height / 2 : s.y.getAbsoluteValue(t.height)
                },
                    a = at(r && r[2] || "farthest-corner", n, o, B, t);
                return new function A(e, t, r, n) {
                    Ge(this, A), this.type = 1, this.colorStops = e, this.shape = t, this.center = r, this.radius = n
                }(At(e, r ? 1 : 0, Math.min(a.x, a.y)), n, o, a)
            },
            rt = function (A, e) {
                var t = e.width,
                    r = e.height,
                    n = .5 * t,
                    B = .5 * r,
                    s = (Math.abs(t * Math.sin(A)) + Math.abs(r * Math.cos(A))) / 2,
                    o = n + Math.sin(A) * s,
                    a = B - Math.cos(A) * s;
                return {
                    x0: o,
                    x1: t - o,
                    y0: a,
                    y1: r - a
                }
            },
            nt = function (A) {
                return Math.acos(A.width / 2 / (m(A.width, A.height) / 2))
            },
            Bt = function (A, e) {
                switch (A) {
                    case "bottom":
                    case "to top":
                        return rt(0, e);
                    case "left":
                    case "to right":
                        return rt(Math.PI / 2, e);
                    case "right":
                    case "to left":
                        return rt(3 * Math.PI / 2, e);
                    case "top right":
                    case "right top":
                    case "to bottom left":
                    case "to left bottom":
                        return rt(Math.PI + nt(e), e);
                    case "top left":
                    case "left top":
                    case "to bottom right":
                    case "to right bottom":
                        return rt(Math.PI - nt(e), e);
                    case "bottom left":
                    case "left bottom":
                    case "to top right":
                    case "to right top":
                        return rt(nt(e), e);
                    case "bottom right":
                    case "right bottom":
                    case "to top left":
                    case "to left top":
                        return rt(2 * Math.PI - nt(e), e);
                    case "top":
                    case "to bottom":
                    default:
                        return rt(Math.PI, e)
                }
            },
            st = function (A, e) {
                var t = function (A, e) {
                    return function (A) {
                        if (Array.isArray(A)) return A
                    }(A) || function (A, e) {
                        var t = [],
                            r = !0,
                            n = !1,
                            B = void 0;
                        try {
                            for (var s, o = A[Symbol.iterator](); !(r = (s = o.next()).done) && (t.push(s.value), !e || t.length !== e); r = !0);
                        } catch (A) {
                            n = !0, B = A
                        } finally {
                            try {
                                r || null == o.return || o.return()
                            } finally {
                                if (n) throw B
                            }
                        }
                        return t
                    }(A, e) || function () {
                        throw new TypeError("Invalid attempt to destructure non-iterable instance")
                    }()
                }(A.split(" ").map(parseFloat), 2),
                    r = t[0],
                    n = t[1],
                    B = r / 100 * e.width / (n / 100 * e.height);
                return rt(Math.atan(isNaN(B) ? 1 : B) + Math.PI / 2, e)
            },
            ot = function (A, e, t, r) {
                return [{
                    x: 0,
                    y: 0
                }, {
                    x: 0,
                    y: A.height
                }, {
                    x: A.width,
                    y: 0
                }, {
                    x: A.width,
                    y: A.height
                }].reduce(function (A, n) {
                    var B = m(e - n.x, t - n.y);
                    return (r ? B < A.optimumDistance : B > A.optimumDistance) ? {
                        optimumCorner: n,
                        optimumDistance: B
                    } : A
                }, {
                    optimumDistance: r ? 1 / 0 : -1 / 0,
                    optimumCorner: null
                }).optimumCorner
            },
            at = function (A, e, t, r, n) {
                var B = t.x,
                    s = t.y,
                    o = 0,
                    a = 0;
                switch (A) {
                    case "closest-side":
                        0 === e ? o = a = Math.min(Math.abs(B), Math.abs(B - n.width), Math.abs(s), Math.abs(s - n.height)) : 1 === e && (o = Math.min(Math.abs(B), Math.abs(B - n.width)), a = Math.min(Math.abs(s), Math.abs(s - n.height)));
                        break;
                    case "closest-corner":
                        if (0 === e) o = a = Math.min(m(B, s), m(B, s - n.height), m(B - n.width, s), m(B - n.width, s - n.height));
                        else if (1 === e) {
                            var i = Math.min(Math.abs(s), Math.abs(s - n.height)) / Math.min(Math.abs(B), Math.abs(B - n.width)),
                                c = ot(n, B, s, !0);
                            a = i * (o = m(c.x - B, (c.y - s) / i))
                        }
                        break;
                    case "farthest-side":
                        0 === e ? o = a = Math.max(Math.abs(B), Math.abs(B - n.width), Math.abs(s), Math.abs(s - n.height)) : 1 === e && (o = Math.max(Math.abs(B), Math.abs(B - n.width)), a = Math.max(Math.abs(s), Math.abs(s - n.height)));
                        break;
                    case "farthest-corner":
                        if (0 === e) o = a = Math.max(m(B, s), m(B, s - n.height), m(B - n.width, s), m(B - n.width, s - n.height));
                        else if (1 === e) {
                            var Q = Math.max(Math.abs(s), Math.abs(s - n.height)) / Math.max(Math.abs(B), Math.abs(B - n.width)),
                                l = ot(n, B, s, !1);
                            a = Q * (o = m(l.x - B, (l.y - s) / Q))
                        }
                        break;
                    default:
                        o = r.x || 0, a = void 0 !== r.y ? r.y : o
                }
                return {
                    x: o,
                    y: a
                }
            },
            it = function (A) {
                var e = "",
                    t = "",
                    r = "",
                    n = "",
                    B = 0,
                    s = /^(left|center|right|\d+(?:px|r?em|%)?)(?:\s+(top|center|bottom|\d+(?:px|r?em|%)?))?$/i,
                    o = /^\d+(px|r?em|%)?(?:\s+\d+(px|r?em|%)?)?$/i,
                    a = A[B].match(s);
                a && B++;
                var i = A[B].match(/^(circle|ellipse)?\s*(closest-side|closest-corner|farthest-side|farthest-corner|contain|cover)?$/i);
                i && (e = i[1] || "", "contain" === (r = i[2] || "") ? r = "closest-side" : "cover" === r && (r = "farthest-corner"), B++);
                var c = A[B].match(o);
                c && B++;
                var Q = A[B].match(s);
                Q && B++;
                var l = A[B].match(o);
                l && B++;
                var w = Q || a;
                w && w[1] && (n = w[1] + (/^\d+$/.test(w[1]) ? "px" : ""), w[2] && (n += " " + w[2] + (/^\d+$/.test(w[2]) ? "px" : "")));
                var u = l || c;
                return u && (t = u[0], u[1] || (t += "px")), !n || e || t || r || (t = n, n = ""), n && (n = "at ".concat(n)), [
                    [e, r, t, n].filter(function (A) {
                        return !!A
                    }).join(" ")
                ].concat(A.slice(B))
            },
            ct = function (A) {
                return A.map(function (A) {
                    return A.match(je)
                }).map(function (e, t) {
                    if (!e) return A[t];
                    switch (e[1]) {
                        case "from":
                            return "".concat(e[4], " 0%");
                        case "to":
                            return "".concat(e[4], " 100%");
                        case "color-stop":
                            return "%" === e[3] ? "".concat(e[4], " ").concat(e[2]) : "".concat(e[4], " ").concat(100 * parseFloat(e[2]), "%")
                    }
                })
            };

        function Qt(A, e) {
            return function (A) {
                if (Array.isArray(A)) return A
            }(A) || function (A, e) {
                var t = [],
                    r = !0,
                    n = !1,
                    B = void 0;
                try {
                    for (var s, o = A[Symbol.iterator](); !(r = (s = o.next()).done) && (t.push(s.value), !e || t.length !== e); r = !0);
                } catch (A) {
                    n = !0, B = A
                } finally {
                    try {
                        r || null == o.return || o.return()
                    } finally {
                        if (n) throw B
                    }
                }
                return t
            }(A, e) || function () {
                throw new TypeError("Invalid attempt to destructure non-iterable instance")
            }()
        }

        function lt(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var wt = function () {
            function A(e, t) {
                ! function (A, e) {
                    if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                }(this, A), this.target = e, this.options = t, e.render(t)
            }
            return function (A, e, t) {
                e && lt(A.prototype, e)
            }(A, [{
                key: "renderNode",
                value: function (A) {
                    A.isVisible() && (this.renderNodeBackgroundAndBorders(A), this.renderNodeContent(A))
                }
            }, {
                key: "renderNodeContent",
                value: function (A) {
                    var e = this,
                        t = function () {
                            if (A.childNodes.length && A.childNodes.forEach(function (t) {
                                if (t instanceof jA) {
                                    var r = t.parent.style;
                                    e.target.renderTextNode(t.bounds, r.color, r.font, r.textDecoration, r.textShadow)
                                } else e.target.drawShape(t, A.style.color)
                            }), A.image) {
                                var t = e.options.imageStore.get(A.image);
                                if (t) {
                                    var r = function (A, e, t) {
                                        var r = e[0].value,
                                            n = e[1].value,
                                            B = e[2].value,
                                            s = e[3].value;
                                        return new L(A.left + s + t[3].borderWidth, A.top + r + t[0].borderWidth, A.width - (t[1].borderWidth + t[3].borderWidth + s + n), A.height - (t[0].borderWidth + t[2].borderWidth + r + B))
                                    }(A.bounds, A.style.padding, A.style.border),
                                        n = "number" == typeof t.width && t.width > 0 ? t.width : r.width,
                                        B = "number" == typeof t.height && t.height > 0 ? t.height : r.height;
                                    n > 0 && B > 0 && e.target.clip([k(A.curvedBounds)], function () {
                                        e.target.drawImage(t, new L(0, 0, n, B), r)
                                    })
                                }
                            }
                        },
                        r = A.getClipPaths();
                    r.length ? this.target.clip(r, t) : t()
                }
            }, {
                key: "renderNodeBackgroundAndBorders",
                value: function (A) {
                    var e = this,
                        t = !A.style.background.backgroundColor.isTransparent() || A.style.background.backgroundImage.length,
                        r = A.style.border.some(function (A) {
                            return 0 !== A.borderStyle && !A.borderColor.isTransparent()
                        }),
                        n = function () {
                            var r = function (A, e) {
                                switch (e) {
                                    case W.BORDER_BOX:
                                        return function (A) {
                                            return [A.topLeftOuter, A.topRightOuter, A.bottomRightOuter, A.bottomLeftOuter]
                                        }(A);
                                    case W.PADDING_BOX:
                                    default:
                                        return k(A)
                                }
                            }(A.curvedBounds, A.style.background.backgroundClip);
                            t && e.target.clip([r], function () {
                                A.style.background.backgroundColor.isTransparent() || e.target.fill(A.style.background.backgroundColor), e.renderBackgroundImage(A)
                            }), A.style.border.forEach(function (t, r) {
                                0 === t.borderStyle || t.borderColor.isTransparent() || e.renderBorder(t, r, A.curvedBounds)
                            })
                        };
                    if (t || r) {
                        var B = A.parent ? A.parent.getClipPaths() : [];
                        B.length ? this.target.clip(B, n) : n()
                    }
                }
            }, {
                key: "renderBackgroundImage",
                value: function (A) {
                    var e = this;
                    A.style.background.backgroundImage.slice(0).reverse().forEach(function (t) {
                        "url" === t.source.method && t.source.args.length ? e.renderBackgroundRepeat(A, t) : /gradient/i.test(t.source.method) && e.renderBackgroundGradient(A, t)
                    })
                }
            }, {
                key: "renderBackgroundRepeat",
                value: function (A, e) {
                    var t = this.options.imageStore.get(e.source.args[0]);
                    if (t) {
                        var r = Z(A.style.background.backgroundOrigin, A.bounds, A.style.padding, A.style.border),
                            n = function (A, e, t) {
                                var r = 0,
                                    n = 0,
                                    B = A.size;
                                if (1 === B[0].size || 2 === B[0].size) {
                                    var s = t.width / t.height,
                                        o = e.width / e.height;
                                    return s < o != (2 === B[0].size) ? new D(t.width, t.width / o) : new D(t.height * o, t.height)
                                }
                                return B[0].value && (r = B[0].value.getAbsoluteValue(t.width)), 0 === B[0].size && 0 === B[1].size ? n = e.height : 0 === B[1].size ? n = r / e.width * e.height : B[1].value && (n = B[1].value.getAbsoluteValue(t.height)), 0 === B[0].size && (r = n / e.height * e.width), new D(r, n)
                            }(e, t, r),
                            B = $(e.position, n, r),
                            s = function (A, e, t, r, n) {
                                switch (A.repeat) {
                                    case 2:
                                        return [new M(Math.round(n.left), Math.round(r.top + e.y)), new M(Math.round(n.left + n.width), Math.round(r.top + e.y)), new M(Math.round(n.left + n.width), Math.round(t.height + r.top + e.y)), new M(Math.round(n.left), Math.round(t.height + r.top + e.y))];
                                    case 3:
                                        return [new M(Math.round(r.left + e.x), Math.round(n.top)), new M(Math.round(r.left + e.x + t.width), Math.round(n.top)), new M(Math.round(r.left + e.x + t.width), Math.round(n.height + n.top)), new M(Math.round(r.left + e.x), Math.round(n.height + n.top))];
                                    case 1:
                                        return [new M(Math.round(r.left + e.x), Math.round(r.top + e.y)), new M(Math.round(r.left + e.x + t.width), Math.round(r.top + e.y)), new M(Math.round(r.left + e.x + t.width), Math.round(r.top + e.y + t.height)), new M(Math.round(r.left + e.x), Math.round(r.top + e.y + t.height))];
                                    default:
                                        return [new M(Math.round(n.left), Math.round(n.top)), new M(Math.round(n.left + n.width), Math.round(n.top)), new M(Math.round(n.left + n.width), Math.round(n.height + n.top)), new M(Math.round(n.left), Math.round(n.height + n.top))]
                                }
                            }(e, B, n, r, A.bounds),
                            o = Math.round(r.left + B.x),
                            a = Math.round(r.top + B.y);
                        this.target.renderRepeat(s, t, n, o, a)
                    }
                }
            }, {
                key: "renderBackgroundGradient",
                value: function (A, e) {
                    var t = Z(A.style.background.backgroundOrigin, A.bounds, A.style.padding, A.style.border),
                        r = function (A, e) {
                            var t = A.size,
                                r = t[0].value ? t[0].value.getAbsoluteValue(e.width) : e.width,
                                n = t[1].value ? t[1].value.getAbsoluteValue(e.height) : t[0].value ? r : e.height;
                            return new D(r, n)
                        }(e, t),
                        n = $(e.position, r, t),
                        B = new L(Math.round(t.left + n.x), Math.round(t.top + n.y), r.width, r.height),
                        s = function (A, e, t) {
                            var r = e.args,
                                n = e.method,
                                B = e.prefix;
                            return "linear-gradient" === n ? et(r, t, !!B) : "gradient" === n && "linear" === r[0] ? et(["to bottom"].concat(ct(r.slice(3))), t, !!B) : "radial-gradient" === n ? tt(A, "-webkit-" === B ? it(r) : r, t) : "gradient" === n && "radial" === r[0] ? tt(A, ct(it(r.slice(1))), t) : void 0
                        }(A, e.source, B);
                    if (s) switch (s.type) {
                        case 0:
                            this.target.renderLinearGradient(B, s);
                            break;
                        case 1:
                            this.target.renderRadialGradient(B, s)
                    }
                }
            }, {
                key: "renderBorder",
                value: function (A, e, t) {
                    this.target.drawShape(function (A, e) {
                        switch (e) {
                            case 0:
                                return V(A.topLeftOuter, A.topLeftInner, A.topRightOuter, A.topRightInner);
                            case 1:
                                return V(A.topRightOuter, A.topRightInner, A.bottomRightOuter, A.bottomRightInner);
                            case 2:
                                return V(A.bottomRightOuter, A.bottomRightInner, A.bottomLeftOuter, A.bottomLeftInner);
                            case 3:
                            default:
                                return V(A.bottomLeftOuter, A.bottomLeftInner, A.topLeftOuter, A.topLeftInner)
                        }
                    }(t, e), A.borderColor)
                }
            }, {
                key: "renderStack",
                value: function (A) {
                    var e = this;
                    if (A.container.isVisible()) {
                        var t = A.getOpacity();
                        t !== this._opacity && (this.target.setOpacity(A.getOpacity()), this._opacity = t);
                        var r = A.container.style.transform;
                        null !== r ? this.target.transform(A.container.bounds.left + r.transformOrigin[0].value, A.container.bounds.top + r.transformOrigin[1].value, r.transform, function () {
                            return e.renderStackContent(A)
                        }) : this.renderStackContent(A)
                    }
                }
            }, {
                key: "renderStackContent",
                value: function (A) {
                    var e = Qt(Ut(A), 5),
                        t = e[0],
                        r = e[1],
                        n = e[2],
                        B = e[3],
                        s = e[4],
                        o = Qt(ut(A), 2),
                        a = o[0],
                        i = o[1];
                    this.renderNodeBackgroundAndBorders(A.container), t.sort(gt).forEach(this.renderStack, this), this.renderNodeContent(A.container), i.forEach(this.renderNode, this), B.forEach(this.renderStack, this), s.forEach(this.renderStack, this), a.forEach(this.renderNode, this), r.forEach(this.renderStack, this), n.sort(gt).forEach(this.renderStack, this)
                }
            }, {
                key: "render",
                value: function (A) {
                    return this.options.backgroundColor && this.target.rectangle(this.options.x, this.options.y, this.options.width, this.options.height, this.options.backgroundColor), this.renderStack(A), this.target.getTarget()
                }
            }]), A
        }(),
            ut = function (A) {
                for (var e = [], t = [], r = A.children.length, n = 0; n < r; n++) {
                    var B = A.children[n];
                    B.isInlineLevel() ? e.push(B) : t.push(B)
                }
                return [e, t]
            },
            Ut = function (A) {
                for (var e = [], t = [], r = [], n = [], B = [], s = A.contexts.length, o = 0; o < s; o++) {
                    var a = A.contexts[o];
                    a.container.isPositioned() || a.container.style.opacity < 1 || a.container.isTransformed() ? a.container.style.zIndex.order < 0 ? e.push(a) : a.container.style.zIndex.order > 0 ? r.push(a) : t.push(a) : a.container.isFloating() ? n.push(a) : B.push(a)
                }
                return [e, t, r, n, B]
            },
            gt = function (A, e) {
                return A.container.style.zIndex.order > e.container.style.zIndex.order ? 1 : A.container.style.zIndex.order < e.container.style.zIndex.order ? -1 : A.container.index > e.container.index ? 1 : -1
            },
            Ft = function (A, e) {
                if (!e.proxy) return Promise.reject(null);
                var t = e.proxy;
                return new Promise(function (r, n) {
                    var B = _A.SUPPORT_CORS_XHR && _A.SUPPORT_RESPONSE_TYPE ? "blob" : "text",
                        s = _A.SUPPORT_CORS_XHR ? new XMLHttpRequest : new XDomainRequest;
                    if (s.onload = function () {
                        if (s instanceof XMLHttpRequest)
                            if (200 === s.status)
                                if ("text" === B) r(s.response);
                                else {
                                    var A = new FileReader;
                                    A.addEventListener("load", function () {
                                        return r(A.result)
                                    }, !1), A.addEventListener("error", function (A) {
                                        return n(A)
                                    }, !1), A.readAsDataURL(s.response)
                                } else n("");
                        else r(s.responseText)
                    }, s.onerror = n, s.open("GET", "".concat(t, "?url=").concat(encodeURIComponent(A), "&responseType=").concat(B)), "text" !== B && s instanceof XMLHttpRequest && (s.responseType = B), e.imageTimeout) {
                        var o = e.imageTimeout;
                        s.timeout = o, s.ontimeout = function () {
                            return n("")
                        }
                    }
                    s.send()
                })
            };

        function Ct(A, e) {
            if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
        }

        function ht(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }

        function dt(A, e, t) {
            return e && ht(A.prototype, e), t && ht(A, t), A
        }
        var Ht = function () {
            function A(e, t, r) {
                Ct(this, A), this.options = e, this._window = r, this.origin = this.getOrigin(r.location.href), this.cache = {}, this.logger = t, this._index = 0
            }
            return dt(A, [{
                key: "loadImage",
                value: function (A) {
                    var e = this;
                    if (this.hasResourceInCache(A)) return A;
                    if (Nt(A)) return this.cache[A] = vt(A, this.options.imageTimeout || 0), A;
                    if (!yt(A) || _A.SUPPORT_SVG_DRAWING) {
                        if (!0 === this.options.allowTaint || mt(A) || this.isSameOrigin(A)) return this.addImage(A, A, !1);
                        if (!this.isSameOrigin(A)) {
                            if ("string" == typeof this.options.proxy) return this.cache[A] = Ft(A, this.options).then(function (A) {
                                return vt(A, e.options.imageTimeout || 0)
                            }), A;
                            if (!0 === this.options.useCORS && _A.SUPPORT_CORS_IMAGES) return this.addImage(A, A, !0)
                        }
                    }
                }
            }, {
                key: "inlineImage",
                value: function (A) {
                    var e = this;
                    return mt(A) ? vt(A, this.options.imageTimeout || 0) : this.hasResourceInCache(A) ? this.cache[A] : this.isSameOrigin(A) || "string" != typeof this.options.proxy ? this.xhrImage(A) : this.cache[A] = Ft(A, this.options).then(function (A) {
                        return vt(A, e.options.imageTimeout || 0)
                    })
                }
            }, {
                key: "xhrImage",
                value: function (A) {
                    var e = this;
                    return this.cache[A] = new Promise(function (t, r) {
                        var n = new XMLHttpRequest;
                        if (n.onreadystatechange = function () {
                            if (4 === n.readyState)
                                if (200 !== n.status) r("Failed to fetch image ".concat(A.substring(0, 256), " with status code ").concat(n.status));
                                else {
                                    var e = new FileReader;
                                    e.addEventListener("load", function () {
                                        var A = e.result;
                                        t(A)
                                    }, !1), e.addEventListener("error", function (A) {
                                        return r(A)
                                    }, !1), e.readAsDataURL(n.response)
                                }
                        }, n.responseType = "blob", e.options.imageTimeout) {
                            var B = e.options.imageTimeout;
                            n.timeout = B, n.ontimeout = function () {
                                return r("")
                            }
                        }
                        n.open("GET", A, !0), n.send()
                    }).then(function (A) {
                        return vt(A, e.options.imageTimeout || 0)
                    }), this.cache[A]
                }
            }, {
                key: "loadCanvas",
                value: function (A) {
                    var e = String(this._index++);
                    return this.cache[e] = Promise.resolve(A), e
                }
            }, {
                key: "hasResourceInCache",
                value: function (A) {
                    return void 0 !== this.cache[A]
                }
            }, {
                key: "addImage",
                value: function (A, e, t) {
                    var r = this;
                    return this.cache[A] = new Promise(function (A, n) {
                        var B = new Image;
                        if (B.onload = function () {
                            return A(B)
                        }, (bt(e) || t) && (B.crossOrigin = "anonymous"), B.onerror = n, B.src = e, !0 === B.complete && setTimeout(function () {
                            A(B)
                        }, 500), r.options.imageTimeout) {
                            var s = r.options.imageTimeout;
                            setTimeout(function () {
                                return n("")
                            }, s)
                        }
                    }), A
                }
            }, {
                key: "isSameOrigin",
                value: function (A) {
                    return this.getOrigin(A) === this.origin
                }
            }, {
                key: "getOrigin",
                value: function (A) {
                    var e = this._link || (this._link = this._window.document.createElement("a"));
                    return e.href = A, e.href = e.href, e.protocol + e.hostname + e.port
                }
            }, {
                key: "ready",
                value: function () {
                    var A = this,
                        e = Object.keys(this.cache),
                        t = e.map(function (e) {
                            return A.cache[e].catch(function (A) {
                                return null
                            })
                        });
                    return Promise.all(t).then(function (A) {
                        return new ft(e, A)
                    })
                }
            }]), A
        }(),
            ft = function () {
                function A(e, t) {
                    Ct(this, A), this._keys = e, this._resources = t
                }
                return dt(A, [{
                    key: "get",
                    value: function (A) {
                        var e = this._keys.indexOf(A);
                        return -1 === e ? null : this._resources[e]
                    }
                }]), A
            }(),
            Et = /^data:image\/svg\+xml/i,
            pt = /^data:image\/.*;base64,/i,
            Kt = /^data:image\/.*/i,
            mt = function (A) {
                return Kt.test(A)
            },
            bt = function (A) {
                return pt.test(A)
            },
            Nt = function (A) {
                return "blob" === A.substr(0, 4)
            },
            yt = function (A) {
                return "svg" === A.substr(-3).toLowerCase() || Et.test(A)
            },
            vt = function (A, e) {
                return new Promise(function (t, r) {
                    var n = new Image;
                    n.onload = function () {
                        return t(n)
                    }, n.onerror = r, n.src = A, !0 === n.complete && setTimeout(function () {
                        t(n)
                    }, 500), e && setTimeout(function () {
                        return r("")
                    }, e)
                })
            };

        function It(A, e) {
            return function (A) {
                if (Array.isArray(A)) return A
            }(A) || function (A, e) {
                var t = [],
                    r = !0,
                    n = !1,
                    B = void 0;
                try {
                    for (var s, o = A[Symbol.iterator](); !(r = (s = o.next()).done) && (t.push(s.value), !e || t.length !== e); r = !0);
                } catch (A) {
                    n = !0, B = A
                } finally {
                    try {
                        r || null == o.return || o.return()
                    } finally {
                        if (n) throw B
                    }
                }
                return t
            }(A, e) || function () {
                throw new TypeError("Invalid attempt to destructure non-iterable instance")
            }()
        }
        var Dt = function (A, e, t) {
            if (!e || !e.content || "none" === e.content || "-moz-alt-content" === e.content || "none" === e.display) return null;
            var r = Mt(e.content),
                n = r.length,
                B = [],
                s = "",
                o = e.counterIncrement;
            if (o && "none" !== o) {
                var a = It(o.split(/\s+/), 2),
                    i = a[0],
                    c = a[1],
                    Q = t.counters[i];
                Q && (Q[Q.length - 1] += void 0 === c ? 1 : parseInt(c, 10))
            }
            for (var l = 0; l < n; l++) {
                var w = r[l];
                switch (w.type) {
                    case 0:
                        s += w.value || "";
                        break;
                    case 1:
                        A instanceof HTMLElement && w.value && (s += A.getAttribute(w.value) || "");
                        break;
                    case 3:
                        var u = t.counters[w.name || ""];
                        u && (s += Xt([u[u.length - 1]], "", w.format));
                        break;
                    case 4:
                        var U = t.counters[w.name || ""];
                        U && (s += Xt(U, w.glue, w.format));
                        break;
                    case 5:
                        s += St(e, !0, t.quoteDepth), t.quoteDepth++;
                        break;
                    case 6:
                        t.quoteDepth-- , s += St(e, !1, t.quoteDepth);
                        break;
                    case 2:
                        s && (B.push({
                            type: 0,
                            value: s
                        }), s = ""), B.push({
                            type: 1,
                            value: w.value || ""
                        })
                }
            }
            return s && B.push({
                type: 0,
                value: s
            }), B
        },
            Mt = function (A, e) {
                if (e && e[A]) return e[A];
                for (var t = [], r = A.length, n = !1, B = !1, s = !1, o = "", a = "", i = [], c = 0; c < r; c++) {
                    var Q = A.charAt(c);
                    switch (Q) {
                        case "'":
                        case '"':
                            B ? o += Q : (n = !n, s || n || (t.push({
                                type: 0,
                                value: o
                            }), o = ""));
                            break;
                        case "\\":
                            B ? (o += Q, B = !1) : B = !0;
                            break;
                        case "(":
                            n ? o += Q : (s = !0, a = o, o = "", i = []);
                            break;
                        case ")":
                            if (n) o += Q;
                            else if (s) {
                                switch (o && i.push(o), a) {
                                    case "attr":
                                        i.length > 0 && t.push({
                                            type: 1,
                                            value: i[0]
                                        });
                                        break;
                                    case "counter":
                                        if (i.length > 0) {
                                            var l = {
                                                type: 3,
                                                name: i[0]
                                            };
                                            i.length > 1 && (l.format = i[1]), t.push(l)
                                        }
                                        break;
                                    case "counters":
                                        if (i.length > 0) {
                                            var w = {
                                                type: 4,
                                                name: i[0]
                                            };
                                            i.length > 1 && (w.glue = i[1]), i.length > 2 && (w.format = i[2]), t.push(w)
                                        }
                                        break;
                                    case "url":
                                        i.length > 0 && t.push({
                                            type: 2,
                                            value: i[0]
                                        })
                                }
                                s = !1, o = ""
                            }
                            break;
                        case ",":
                            n ? o += Q : s && (i.push(o), o = "");
                            break;
                        case " ":
                        case "\t":
                            n ? o += Q : o && (Tt(t, o), o = "");
                            break;
                        default:
                            o += Q
                    }
                    "\\" !== Q && (B = !1)
                }
                return o && Tt(t, o), e && (e[A] = t), t
            },
            Tt = function (A, e) {
                switch (e) {
                    case "open-quote":
                        A.push({
                            type: 5
                        });
                        break;
                    case "close-quote":
                        A.push({
                            type: 6
                        })
                }
            },
            St = function (A, e, t) {
                var r = A.quotes ? A.quotes.split(/\s+/) : ["'\"'", "'\"'"],
                    n = 2 * t;
                return n >= r.length && (n = r.length - 2), e || ++n, r[n].replace(/^["']|["']$/g, "")
            },
            Xt = function (A, e, t) {
                for (var r = A.length, n = "", B = 0; B < r; B++) B > 0 && (n += e || ""), n += Me(A[B], CA(t || "decimal"), !1);
                return n
            };

        function zt(A, e) {
            for (var t = 0; t < e.length; t++) {
                var r = e[t];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(A, r.key, r)
            }
        }
        var Lt = function () {
            function A(e, t, r, n, B) {
                ! function (A, e) {
                    if (!(A instanceof e)) throw new TypeError("Cannot call a class as a function")
                }(this, A), this.referenceElement = e, this.scrolledElements = [], this.copyStyles = n, this.inlineImages = n, this.logger = r, this.options = t, this.renderer = B, this.resourceLoader = new Ht(t, r, window), this.pseudoContentData = {
                    counters: {},
                    quoteDepth: 0
                }, this.documentElement = this.cloneNode(e.ownerDocument.documentElement)
            }
            return function (A, e, t) {
                e && zt(A.prototype, e)
            }(A, [{
                key: "inlineAllImages",
                value: function (A) {
                    var e = this;
                    if (this.inlineImages && A) {
                        var t = A.style;
                        Promise.all(sA(t.backgroundImage).map(function (A) {
                            return "url" === A.method ? e.resourceLoader.inlineImage(A.args[0]).then(function (A) {
                                return A && "string" == typeof A.src ? 'url("'.concat(A.src, '")') : "none"
                            }).catch(function (A) { }) : Promise.resolve("".concat(A.prefix).concat(A.method, "(").concat(A.args.join(","), ")"))
                        })).then(function (A) {
                            A.length > 1 && (t.backgroundColor = ""), t.backgroundImage = A.join(",")
                        }), A instanceof HTMLImageElement && this.resourceLoader.inlineImage(A.src).then(function (e) {
                            if (e && A instanceof HTMLImageElement && A.parentNode) {
                                var t = A.parentNode,
                                    r = b(A.style, e.cloneNode(!1));
                                t.replaceChild(r, A)
                            }
                        }).catch(function (A) { })
                    }
                }
            }, {
                key: "inlineFonts",
                value: function (A) {
                    var e = this;
                    return Promise.all(Array.from(A.styleSheets).map(function (e) {
                        return e.href ? fetch(e.href).then(function (A) {
                            return A.text()
                        }).then(function (A) {
                            return xt(A, e.href)
                        }).catch(function (A) {
                            return []
                        }) : Ot(e, A)
                    })).then(function (A) {
                        return A.reduce(function (A, e) {
                            return A.concat(e)
                        }, [])
                    }).then(function (A) {
                        return Promise.all(A.map(function (A) {
                            return fetch(A.formats[0].src).then(function (A) {
                                return A.blob()
                            }).then(function (A) {
                                return new Promise(function (e, t) {
                                    var r = new FileReader;
                                    r.onerror = t, r.onload = function () {
                                        var A = r.result;
                                        e(A)
                                    }, r.readAsDataURL(A)
                                })
                            }).then(function (e) {
                                return A.fontFace.setProperty("src", 'url("'.concat(e, '")')), "@font-face {".concat(A.fontFace.cssText, " ")
                            })
                        }))
                    }).then(function (t) {
                        var r = A.createElement("style");
                        r.textContent = t.join("\n"), e.documentElement.appendChild(r)
                    })
                }
            }, {
                key: "createElementClone",
                value: function (A) {
                    var e = this;
                    if (this.copyStyles && A instanceof HTMLCanvasElement) {
                        var t = A.ownerDocument.createElement("img");
                        try {
                            return t.src = A.toDataURL(), t
                        } catch (A) { }
                    }
                    if (A instanceof HTMLIFrameElement) {
                        var r = A.cloneNode(!1),
                            n = qt();
                        r.setAttribute("data-html2canvas-internal-iframe-key", n);
                        var B = O(A, 0, 0),
                            s = B.width,
                            o = B.height;
                        return this.resourceLoader.cache[n] = Zt(A, this.options).then(function (A) {
                            return e.renderer(A, {
                                allowTaint: e.options.allowTaint,
                                backgroundColor: "#ffffff",
                                canvas: null,
                                imageTimeout: e.options.imageTimeout,
                                logging: e.options.logging,
                                proxy: e.options.proxy,
                                removeContainer: e.options.removeContainer,
                                scale: e.options.scale,
                                foreignObjectRendering: e.options.foreignObjectRendering,
                                useCORS: e.options.useCORS,
                                target: new f,
                                width: s,
                                height: o,
                                x: 0,
                                y: 0,
                                windowWidth: A.ownerDocument.defaultView.innerWidth,
                                windowHeight: A.ownerDocument.defaultView.innerHeight,
                                scrollX: A.ownerDocument.defaultView.pageXOffset,
                                scrollY: A.ownerDocument.defaultView.pageYOffset
                            }, e.logger.child(n))
                        }).then(function (e) {
                            return new Promise(function (t, n) {
                                var B = document.createElement("img");
                                B.onload = function () {
                                    return t(e)
                                }, B.onerror = function (A) {
                                    "data:," == B.src ? t(e) : n(A)
                                }, B.src = e.toDataURL(), r.parentNode && r.parentNode.replaceChild(b(A.ownerDocument.defaultView.getComputedStyle(A), B), r)
                            })
                        }), r
                    }
                    try {
                        if (A instanceof HTMLStyleElement && A.sheet && A.sheet.cssRules) {
                            var a = [].slice.call(A.sheet.cssRules, 0).reduce(function (A, e) {
                                return e && e.cssText ? A + e.cssText : A
                            }, ""),
                                i = A.cloneNode(!1);
                            return i.textContent = a, i
                        }
                    } catch (A) {
                        if (this.logger.log("Unable to access cssRules property"), "SecurityError" !== A.name) throw this.logger.log(A), A
                    }
                    return A.cloneNode(!1)
                }
            }, {
                key: "cloneNode",
                value: function (A) {
                    var e = A.nodeType === Node.TEXT_NODE ? document.createTextNode(A.nodeValue) : this.createElementClone(A),
                        t = A.ownerDocument.defaultView,
                        r = A instanceof t.HTMLElement ? t.getComputedStyle(A) : null,
                        n = A instanceof t.HTMLElement ? t.getComputedStyle(A, ":before") : null,
                        B = A instanceof t.HTMLElement ? t.getComputedStyle(A, ":after") : null;
                    this.referenceElement === A && e instanceof t.HTMLElement && (this.clonedReferenceElement = e), e instanceof t.HTMLBodyElement && Gt(e);
                    for (var s = function (A, e) {
                        if (!A || !A.counterReset || "none" === A.counterReset) return [];
                        for (var t = [], r = A.counterReset.split(/\s*,\s*/), n = r.length, B = 0; B < n; B++) {
                            var s = It(r[B].split(/\s+/), 2),
                                o = s[0],
                                a = s[1];
                            t.push(o);
                            var i = e.counters[o];
                            i || (i = e.counters[o] = []), i.push(parseInt(a || 0, 10))
                        }
                        return t
                    }(r, this.pseudoContentData), o = Dt(A, n, this.pseudoContentData), a = A.firstChild; a; a = a.nextSibling) a.nodeType === Node.ELEMENT_NODE && ("SCRIPT" === a.nodeName || a.hasAttribute("data-html2canvas-ignore") || "function" == typeof this.options.ignoreElements && this.options.ignoreElements(a)) || this.copyStyles && "STYLE" === a.nodeName || e.appendChild(this.cloneNode(a));
                    var i = Dt(A, B, this.pseudoContentData);
                    if (function (A, e) {
                        for (var t = A.length, r = 0; r < t; r++) e.counters[A[r]].pop()
                    }(s, this.pseudoContentData), A instanceof t.HTMLElement && e instanceof t.HTMLElement) switch (n && this.inlineAllImages(kt(A, e, n, o, Jt)), B && this.inlineAllImages(kt(A, e, B, i, Rt)), !r || !this.copyStyles || A instanceof HTMLIFrameElement || b(r, e), this.inlineAllImages(e), 0 === A.scrollTop && 0 === A.scrollLeft || this.scrolledElements.push([e, A.scrollLeft, A.scrollTop]), A.nodeName) {
                        case "CANVAS":
                            this.copyStyles || Vt(A, e);
                            break;
                        case "TEXTAREA":
                        case "SELECT":
                            e.value = A.value
                    }
                    return e
                }
            }]), A
        }(),
            Ot = function (A, e) {
                return (A.cssRules ? Array.from(A.cssRules) : []).filter(function (A) {
                    return A.type === CSSRule.FONT_FACE_RULE
                }).map(function (A) {
                    for (var t = sA(A.style.getPropertyValue("src")), r = [], n = 0; n < t.length; n++)
                        if ("url" === t[n].method && t[n + 1] && "format" === t[n + 1].method) {
                            var B = e.createElement("a");
                            B.href = t[n].args[0], e.body && e.body.appendChild(B);
                            var s = {
                                src: B.href,
                                format: t[n + 1].args[0]
                            };
                            r.push(s)
                        }
                    return {
                        formats: r.filter(function (A) {
                            return /^woff/i.test(A.format)
                        }),
                        fontFace: A.style
                    }
                }).filter(function (A) {
                    return A.formats.length
                })
            },
            xt = function (A, e) {
                var t = document.implementation.createHTMLDocument(""),
                    r = document.createElement("base");
                r.href = e;
                var n = document.createElement("style");
                return n.textContent = A, t.head && t.head.appendChild(r), t.body && t.body.appendChild(n), n.sheet ? Ot(n.sheet, t) : []
            },
            Vt = function (A, e) {
                try {
                    if (e) {
                        e.width = A.width, e.height = A.height;
                        var t = A.getContext("2d"),
                            r = e.getContext("2d");
                        t ? r.putImageData(t.getImageData(0, 0, A.width, A.height), 0, 0) : r.drawImage(A, 0, 0)
                    }
                } catch (A) { }
            },
            kt = function (A, e, t, r, n) {
                if (t && t.content && "none" !== t.content && "-moz-alt-content" !== t.content && "none" !== t.display) {
                    var B = e.ownerDocument.createElement("html2canvaspseudoelement");
                    if (b(t, B), r)
                        for (var s = r.length, o = 0; o < s; o++) {
                            var a = r[o];
                            switch (a.type) {
                                case 1:
                                    var i = e.ownerDocument.createElement("img");
                                    i.src = sA("url(".concat(a.value, ")"))[0].args[0], i.style.opacity = "1", B.appendChild(i);
                                    break;
                                case 0:
                                    B.appendChild(e.ownerDocument.createTextNode(a.value))
                            }
                        }
                    return B.className = "".concat(_t, " ").concat(Pt), e.className += " ".concat(n === Jt ? _t : Pt), n === Jt ? e.insertBefore(B, e.firstChild) : e.appendChild(B), B
                }
            },
            Jt = ":before",
            Rt = ":after",
            _t = "___html2canvas___pseudoelement_before",
            Pt = "___html2canvas___pseudoelement_after",
            Gt = function (A) {
                Wt(A, ".".concat(_t).concat(Jt).concat('{\n    content: "" !important;\n    display: none !important;\n}', "\n         .").concat(Pt).concat(Rt).concat('{\n    content: "" !important;\n    display: none !important;\n}'))
            },
            Wt = function (A, e) {
                var t = A.ownerDocument.createElement("style");
                t.innerHTML = e, A.appendChild(t)
            },
            Yt = function (A) {
                var e = function (A, e) {
                    return function (A) {
                        if (Array.isArray(A)) return A
                    }(A) || function (A, e) {
                        var t = [],
                            r = !0,
                            n = !1,
                            B = void 0;
                        try {
                            for (var s, o = A[Symbol.iterator](); !(r = (s = o.next()).done) && (t.push(s.value), !e || t.length !== e); r = !0);
                        } catch (A) {
                            n = !0, B = A
                        } finally {
                            try {
                                r || null == o.return || o.return()
                            } finally {
                                if (n) throw B
                            }
                        }
                        return t
                    }(A, e) || function () {
                        throw new TypeError("Invalid attempt to destructure non-iterable instance")
                    }()
                }(A, 3),
                    t = e[0],
                    r = e[1],
                    n = e[2];
                t.scrollLeft = r, t.scrollTop = n
            },
            qt = function () {
                return Math.ceil(Date.now() + 1e7 * Math.random()).toString(16)
            },
            jt = /^data:text\/(.+);(base64)?,(.*)$/i,
            Zt = function (A, e) {
                try {
                    return Promise.resolve(A.contentWindow.document.documentElement)
                } catch (t) {
                    return e.proxy ? Ft(A.src, e).then(function (A) {
                        var e = A.match(jt);
                        return e ? "base64" === e[2] ? window.atob(decodeURIComponent(e[3])) : decodeURIComponent(e[3]) : Promise.reject()
                    }).then(function (e) {
                        return $t(A.ownerDocument, O(A, 0, 0)).then(function (A) {
                            var t = A.contentWindow.document;
                            t.open(), t.write(e);
                            var r = Ar(A).then(function () {
                                return t.documentElement
                            });
                            return t.close(), r
                        })
                    }) : Promise.reject()
                }
            },
            $t = function (A, e) {
                var t = A.createElement("iframe");
                return t.className = "html2canvas-container", t.style.visibility = "hidden", t.style.position = "fixed", t.style.left = "-10000px", t.style.top = "0px", t.style.border = "0", t.width = e.width.toString(), t.height = e.height.toString(), t.scrolling = "no", t.setAttribute("data-html2canvas-ignore", "true"), A.body ? (A.body.appendChild(t), Promise.resolve(t)) : Promise.reject("")
            },
            Ar = function (A) {
                var e = A.contentWindow,
                    t = e.document;
                return new Promise(function (r, n) {
                    e.onload = A.onload = t.onreadystatechange = function () {
                        var e = setInterval(function () {
                            t.body.childNodes.length > 0 && "complete" === t.readyState && (clearInterval(e), r(A))
                        }, 50)
                    }
                })
            };
        var er = function A(e, t, r) {
            var n = e.ownerDocument,
                B = new L(t.scrollX, t.scrollY, t.windowWidth, t.windowHeight),
                s = n.documentElement ? new U(getComputedStyle(n.documentElement).backgroundColor) : F,
                o = n.body ? new U(getComputedStyle(n.body).backgroundColor) : F,
                a = e === n.documentElement ? s.isTransparent() ? o.isTransparent() ? t.backgroundColor ? new U(t.backgroundColor) : null : o : s : t.backgroundColor ? new U(t.backgroundColor) : null;
            return (t.foreignObjectRendering ? _A.SUPPORT_FOREIGNOBJECT_DRAWING : Promise.resolve(!1)).then(function (s) {
                return s ? function (A) {
                    return A.inlineFonts(n).then(function () {
                        return A.resourceLoader.ready()
                    }).then(function () {
                        var B = new xA(A.documentElement),
                            s = n.defaultView,
                            o = s.pageXOffset,
                            i = s.pageYOffset,
                            c = "HTML" === e.tagName || "BODY" === e.tagName ? x(n) : O(e, o, i),
                            Q = c.width,
                            l = c.height,
                            w = c.left,
                            u = c.top;
                        return B.render({
                            backgroundColor: a,
                            logger: r,
                            scale: t.scale,
                            x: "number" == typeof t.x ? t.x : w,
                            y: "number" == typeof t.y ? t.y : u,
                            width: "number" == typeof t.width ? t.width : Math.ceil(Q),
                            height: "number" == typeof t.height ? t.height : Math.ceil(l),
                            windowWidth: t.windowWidth,
                            windowHeight: t.windowHeight,
                            scrollX: t.scrollX,
                            scrollY: t.scrollY
                        })
                    })
                }(new Lt(e, t, r, !0, A)) : function (A, e, t, r, n, B) {
                    var s = new Lt(t, r, n, !1, B),
                        o = A.defaultView.pageXOffset,
                        a = A.defaultView.pageYOffset;
                    return $t(A, e).then(function (n) {
                        var B = n.contentWindow,
                            i = B.document,
                            c = Ar(n).then(function () {
                                s.scrolledElements.forEach(Yt), B.scrollTo(e.left, e.top), !/(iPad|iPhone|iPod)/g.test(navigator.userAgent) || B.scrollY === e.top && B.scrollX === e.left || (i.documentElement.style.top = -e.top + "px", i.documentElement.style.left = -e.left + "px", i.documentElement.style.position = "absolute");
                                var t = Promise.resolve([n, s.clonedReferenceElement, s.resourceLoader]),
                                    o = r.onclone;
                                return s.clonedReferenceElement instanceof B.HTMLElement || s.clonedReferenceElement instanceof A.defaultView.HTMLElement || s.clonedReferenceElement instanceof HTMLElement ? "function" == typeof o ? Promise.resolve().then(function () {
                                    return o(i)
                                }).then(function () {
                                    return t
                                }) : t : Promise.reject("")
                            });
                        return i.open(), i.write("".concat(function (A) {
                            var e = "";
                            return A && (e += "<!DOCTYPE ", A.name && (e += A.name), A.internalSubset && (e += A.internalSubset), A.publicId && (e += '"'.concat(A.publicId, '"')), A.systemId && (e += '"'.concat(A.systemId, '"')), e += ">"), e
                        }(document.doctype), "<html></html>")),
                            function (A, e, t) {
                                !A.defaultView || e === A.defaultView.pageXOffset && t === A.defaultView.pageYOffset || A.defaultView.scrollTo(e, t)
                            }(t.ownerDocument, o, a), i.replaceChild(i.adoptNode(s.documentElement), i.documentElement), i.close(), c
                    })
                }(n, B, e, t, r, A).then(function (A) {
                    var e = function (A, e) {
                        return function (A) {
                            if (Array.isArray(A)) return A
                        }(A) || function (A, e) {
                            var t = [],
                                r = !0,
                                n = !1,
                                B = void 0;
                            try {
                                for (var s, o = A[Symbol.iterator](); !(r = (s = o.next()).done) && (t.push(s.value), !e || t.length !== e); r = !0);
                            } catch (A) {
                                n = !0, B = A
                            } finally {
                                try {
                                    r || null == o.return || o.return()
                                } finally {
                                    if (n) throw B
                                }
                            }
                            return t
                        }(A, e) || function () {
                            throw new TypeError("Invalid attempt to destructure non-iterable instance")
                        }()
                    }(A, 3),
                        B = e[0],
                        s = e[1],
                        o = e[2],
                        i = function (A, e, t) {
                            var r = 0,
                                n = new Xe(A, null, e, r++),
                                B = new Oe(n, null, !0);
                            return function A(e, t, r, n, B) {
                                for (var s, o = e.firstChild; o; o = s) {
                                    s = o.nextSibling;
                                    var a = o.ownerDocument.defaultView;
                                    if (o instanceof a.Text || o instanceof Text || a.parent && o instanceof a.parent.Text) o.data.trim().length > 0 && t.childNodes.push(jA.fromTextNode(o, t));
                                    else if (o instanceof a.HTMLElement || o instanceof HTMLElement || a.parent && o instanceof a.parent.HTMLElement) {
                                        if (-1 === xe.indexOf(o.nodeName)) {
                                            var i = new Xe(o, t, n, B++);
                                            if (i.isVisible()) {
                                                "INPUT" === o.tagName ? Ue(o, i) : "TEXTAREA" === o.tagName ? ge(o, i) : "SELECT" === o.tagName ? Fe(o, i) : i.style.listStyle && -1 !== i.style.listStyle.listStyleType && Ee(o, i, n);
                                                var c = "TEXTAREA" !== o.tagName,
                                                    Q = Ve(i, o);
                                                if (Q || ke(i)) {
                                                    var l = Q || i.isPositioned() ? r.getRealParentStackingContext() : r,
                                                        w = new Oe(i, l, Q);
                                                    l.contexts.push(w), c && A(o, i, w, n, B)
                                                } else r.children.push(i), c && A(o, i, r, n, B)
                                            }
                                        }
                                    } else if (o instanceof a.SVGSVGElement || o instanceof SVGSVGElement || a.parent && o instanceof a.parent.SVGSVGElement) {
                                        var u = new Xe(o, t, n, B++),
                                            U = Ve(u, o);
                                        if (U || ke(u)) {
                                            var g = U || u.isPositioned() ? r.getRealParentStackingContext() : r,
                                                F = new Oe(u, g, U);
                                            g.contexts.push(F)
                                        } else r.children.push(u)
                                    }
                                }
                            }(A, n, B, e, 1), B
                        }(s, o),
                        c = s.ownerDocument;
                    return a === i.container.style.background.backgroundColor && (i.container.style.background.backgroundColor = F), o.ready().then(function (A) {
                        var e = new _e(c),
                            o = c.defaultView,
                            Q = o.pageXOffset,
                            l = o.pageYOffset,
                            w = "HTML" === s.tagName || "BODY" === s.tagName ? x(n) : O(s, Q, l),
                            u = w.width,
                            U = w.height,
                            g = w.left,
                            F = w.top,
                            C = {
                                backgroundColor: a,
                                fontMetrics: e,
                                imageStore: A,
                                logger: r,
                                scale: t.scale,
                                x: "number" == typeof t.x ? t.x : g,
                                y: "number" == typeof t.y ? t.y : F,
                                width: "number" == typeof t.width ? t.width : Math.ceil(u),
                                height: "number" == typeof t.height ? t.height : Math.ceil(U)
                            };
                        if (Array.isArray(t.target)) return Promise.all(t.target.map(function (A) {
                            return new wt(A, C).render(i)
                        }));
                        var h = new wt(t.target, C).render(i);
                        return !0 === t.removeContainer && B.parentNode && B.parentNode.removeChild(B), h
                    })
                })
            })
        };

        function tr(A, e, t) {
            return e in A ? Object.defineProperty(A, e, {
                value: t,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : A[e] = t, A
        }
        var rr = function (A, e) {
            var t = e || {},
                r = new p("boolean" != typeof t.logging || t.logging);
            r.log("html2canvas ".concat("1.0.0-rc.1"));
            var n = A.ownerDocument;
            if (!n) return Promise.reject("Provided element is not within a Document");
            var B = n.defaultView,
                s = {
                    allowTaint: !1,
                    backgroundColor: "#ffffff",
                    imageTimeout: 15e3,
                    logging: !0,
                    proxy: null,
                    removeContainer: !0,
                    foreignObjectRendering: !1,
                    scale: B.devicePixelRatio || 1,
                    target: new f(t.canvas),
                    useCORS: !1,
                    windowWidth: B.innerWidth,
                    windowHeight: B.innerHeight,
                    scrollX: B.pageXOffset,
                    scrollY: B.pageYOffset
                };
            return er(A, function (A) {
                for (var e = 1; e < arguments.length; e++) {
                    var t = null != arguments[e] ? arguments[e] : {},
                        r = Object.keys(t);
                    "function" == typeof Object.getOwnPropertySymbols && (r = r.concat(Object.getOwnPropertySymbols(t).filter(function (A) {
                        return Object.getOwnPropertyDescriptor(t, A).enumerable
                    }))), r.forEach(function (e) {
                        tr(A, e, t[e])
                    })
                }
                return A
            }({}, s, t), r)
        };
        rr.CanvasRenderer = f, e.default = rr
    }]).default
});

$(document).ready(function () {
    $(document, parent.window.document).on('click', '#test', function () {
        captureScreenshot();
    });
});

function captureScreenshot() {
    var totalBackgroundImages = 0;
    var elementsWithBackground = [];
    var iframe = $('#myIframe');
    iframe.contents().find("body");
    var imgCanvas = 0;
    var canvasElemHM = 0;
    if (iframe.contents().find(".heatmap-canvas").length) {
        canvasElemHM = iframe.contents().find(".heatmap-canvas").first();
        canvasElemHM.css({
            opacity: 0
        });
        imgCanvas = $('<img>'); //Equivalent: $(document.createElement('img'))
        imgCanvas.attr('src', canvasElemHM[0].toDataURL());
        imgCanvas.css({
            position: "absolute",
            left: 0,
            top: 0
        });
        iframe.contents().find("body").append(imgCanvas);
    }

    iframe.contents().find("*").each(function (index, element) {
        if ($(this).css("background").indexOf('url(') > -1 || element.tagName === "IMG") {
            var imageUrlOfCurrentElement = '';
            var elementType = 'img';
            if (element.tagName === "IMG") {
                imageUrlOfCurrentElement = $(this).attr("src");
                elementType = 'img';
            } else {
                elementType = 'bg';
                imageUrlOfCurrentElement = $(this).css("background");
                imageUrlOfCurrentElement = imageUrlOfCurrentElement.split("url(")[1];
                imageUrlOfCurrentElement = imageUrlOfCurrentElement.split('"')[1];
                imageUrlOfCurrentElement = imageUrlOfCurrentElement.split('"')[0];
            }
            if (!(imageUrlOfCurrentElement.indexOf('data:') > -1)) {
                totalBackgroundImages++;
                elementsWithBackground.push(element);
                var indexHere = (elementsWithBackground.length - 1);
                let xhr = new XMLHttpRequest();
                var params = 'url=' + imageUrlOfCurrentElement + '&id=' + indexHere + '&type=' + elementType;
                xhr.open('POST', 'https://www.flopanda.com/v1/website/baseEncode', true);
                xhr.setRequestHeader('Authorization', 'Bearer U2FsdGVkX1+1mAdrO2E/pJ0sUrN3MuzzS4RGGV08QBZp9mE0THBBUXRD+o7ckxUgDGYXwCo6sNVtFngcbC4z6w==');
                xhr.setRequestHeader('x-auth-token', 'bTaRn943gKfChM1');
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xhr.onload = function () {
                    if (xhr.status == 200) {
                        var responseData = JSON.parse(this.response);
                        if (responseData.type === 'bg') {
                            var currentBackgroundData = $(elementsWithBackground[responseData.id]).css("background");
                            currentBackgroundData = currentBackgroundData.replace(/url\(.*\)/, 'url(' + responseData.data + ')');
                            $(elementsWithBackground[responseData.id]).css("background", currentBackgroundData);
                        } else {
                            $(elementsWithBackground[responseData.id]).attr("src", responseData.data);
                        }
                        totalBackgroundImages--;
                    }
                }
                xhr.send(params);
            }

        }
    });
    var downloadHeatMapInterval = setInterval(function () {
        if (totalBackgroundImages === 0) {
            clearInterval(downloadHeatMapInterval);
            html2canvas(iframe.contents().find("body")[0]).then(function (canvas) {
                saveAs(canvas.toDataURL(), 'heatmap.png');
                if (imgCanvas) {
                    canvasElemHM.css({
                        opacity: 1
                    });
                    imgCanvas.remove();
                }
            });
        }
    }, 500);
}

function saveAs(uri, filename) {
    var link = document.createElement('a');
    if (typeof link.download === 'string') {
        link.href = uri;
        link.download = filename;

        //Firefox requires the link to be in the body
        document.body.appendChild(link);

        //simulate click
        link.click();

        //remove the link when done
        document.body.removeChild(link);
    } else {
        window.open(uri);
    }
}