/* eslint-disable camelcase */
const axios = require('axios');
const sgMail = require('@sendgrid/mail');
const randomstring = require('randomstring');

sgMail.setApiKey('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');


exports.facebook = async (access_token) => {
  const fields = 'id, name, email, picture';
  const url = 'https://graph.facebook.com/me';
  const params = { access_token, fields };
  const response = await axios.get(url, { params });
  const {
    id, name, email, picture,
  } = response.data;
  return {
    service: 'facebook',
    picture: picture.data.url,
    id,
    name,
    email,
  };
};

exports.randomStr = () => {
  return randomstring.generate(10);
};

exports.send_email = async (to, subject, html) => {
  const msg = {
    to: to,
    from: 'Flopanda <support@flopanda.com>',
    subject: subject,
    html: html
  };
  sgMail.send(msg, (err, result) => {
    if (err) {
      console.log("EMail ///////////////////");
      console.log(err);
      console.log(result);
    }
  });
};
