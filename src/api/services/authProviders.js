/* eslint-disable camelcase */
const axios = require('axios');
const RefreshToken = require('../models/refreshToken.model');
const Users = require('../models/users.model');
const Acl = require('../models/acl.model');

exports.facebook = async (access_token) => {
  const fields = 'id, name, email, picture';
  const url = 'https://graph.facebook.com/me';
  const params = { access_token, fields };
  const response = await axios.get(url, { params });
  const {
    id, name, email, picture,
  } = response.data;
  return {
    service: 'facebook',
    picture: picture.data.url,
    id,
    name,
    email,
  };
};

exports.google = async (access_token) => {
  const url = 'https://www.googleapis.com/oauth2/v3/userinfo';
  const params = { access_token };
  const response = await axios.get(url, { params });
  const {
    sub, name, email, picture,
  } = response.data;
  return {
    service: 'google',
    picture,
    id: sub,
    name,
    email,
  };
};

exports.authenticateUser = async (req, res, next) => {
  let apiMethod = req.method;
  let userRole = req.headers.owner;
  let resource = req.baseUrl.split('/')[2];
  let role = await Acl.find({ role: userRole, list: { $elemMatch: { resource: resource, action: apiMethod } } }).exec();
  if (role.length > 0) {
    return res.status(403).json({ status: false, message: 'Access denied' });
  }
  if (req.user !== 0) {
    let query = {};
    query.userId = new mongoose.Types.ObjectId(req.user)
    if (req.headers['ua']) {
      query.isCreatedByAdmin = true;
    } else {
      query.isCreatedByAdmin = false;
    }
    const Tokens = await RefreshToken.find(query);
    if (Tokens.length > 0) {
      if (Tokens[0].expires < new Date()) {
        return res.status(403).json({ status: false, message: 'Your session has been expired.' });
      } else if (req.headers['x-access-token'] !== Tokens[0].jwtToken) {
        return res.status(403).json({ status: false, message: 'Your session has been expired.' });
      } else {
        const user = await Users.findById(req.user).select('active isDeleted').exec();
        console.log("User status", user.isDeleted)
        if (user.active && !user.isDeleted) {
          next();
        } else {
          return res.status(403).json({ status: false, message: 'Your account has been Deactivated. Please contact Customer Support.' });
        }
      }
    }
    else {
      return res.status(405).json({ status: false, message: 'User does not exist.' });
    }
  } else {
    return res.status(403).json({ status: false, message: 'Your session has been expired.' });
  }
};

exports.validateAcl = async (req, res, next) => {
  try {
    let apiMethod = req.method;
    let userRole = req.headers.usertype;
    let resource = req.baseUrl.split('/')[2];
    let role = await Acl.find({ role: userRole, "list.resource": resource, "list.action": apiMethod }).exec();
  } catch (error) {
    return res.status(405).json({ status: false, message: 'Access token Required.' });
  }
}