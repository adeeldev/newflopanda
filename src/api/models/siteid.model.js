var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const Siteidschema = new mongoose.Schema({
    userId: String,
    siteIds: Array
});


module.exports = mongoose.model('Siteids', Siteidschema);