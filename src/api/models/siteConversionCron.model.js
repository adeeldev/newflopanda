var mongoose = require('mongoose');

const SiteConversionCron = new mongoose.Schema({
    userId: {
        type: String,
        default: ''
    },
    email: {
        type: [],
        default: []
    },
    phone: {
        type: [],
        default: []
    },
    siteId: {
        type: String,
        default: ''
    },
    duration: {
        type: Number,
        default: ''
    },
    from: {
        type: Date,
        default: ''
    },
    to: {
        type: Date,
        default: ''
    },
    rate: {
        type: Number,
        default: 0
    },
    isDaily: {
        type: Boolean,
        default: false
    },
    isReceive: {
        type: Boolean,
        default: true
    },
    message: {
        type: String,
        default: ''
    },
    isActive: {
        type: Boolean,
        default: true
    },
    notificationSentAt: { type: Date, default: Date.now },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

});


module.exports = mongoose.model('SiteConversion', SiteConversionCron);