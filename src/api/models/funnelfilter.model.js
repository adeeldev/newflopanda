const mongoose = require('mongoose');

/**
 * User Schema
 * @private
 */
const FunnelfilterSchema = new mongoose.Schema({
    siteId: {
        type: mongoose.Schema.Types.ObjectId,
    },
    funnelDate: {
        type: Date
    },
    funnelDataArray: {
        type: Array,
        default: [],
    }
});

/**
 * @typedef Funnel
 */
module.exports = mongoose.model('Funnelfilter', FunnelfilterSchema);