var mongoose = require('mongoose');
const httpStatus = require('http-status');
/**
 * User Schema
 * @private
 */
const CategorySchema = new mongoose.Schema({
    name: { type: String, default: '' },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
});


/**
 * @typedef Category
 */
module.exports = mongoose.model('Category', CategorySchema);