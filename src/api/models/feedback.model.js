var mongoose = require('mongoose');
var Schema = mongoose.Schema;
/**
 * User Schema
 * @private
 */
const feedbackSchema = new Schema({
    name: { type: String, default: '' },
    userId: { type: Schema.Types.ObjectId,ref: 'User'},
    email: { type: String, default: '' },
    answers: [],
    suveryId : {type : String , default :''},
    comments: { type: String, default: '' },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
});


/**
 * @typedef Feedback
 */
module.exports = mongoose.model('Feedback', feedbackSchema);