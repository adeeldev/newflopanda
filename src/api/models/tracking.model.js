const mongoose = require('mongoose');

const { Schema } = mongoose.Schema;

/**
 * User Schema
 * @private
 */
const TrackingSchema = new mongoose.Schema({
  recordingId: {
    type: Schema.Types.ObjectId,
  },
  pageId: {
    type: Schema.Types.ObjectId,
  },
  clicks: [{ x: Number, y: Number, value: Number }],
  mouseMoves: [{ x: Number, y: Number, value: Number }],
  scroll: [
    {
      y: Number, percentage: Number, from: Number, to: Number, colorCode: String, value: Number,
    },
  ],
  attention: [
    {
      y: Number, percentage: Number, from: Number, to: Number, colorCode: String, value: Number,
    },
  ],
  date: {
    type: Date,
    default: Date.now,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  timestamps: true,
});

/**
 * @typedef Tracking
 */
module.exports = mongoose.model('Tracking', TrackingSchema);
