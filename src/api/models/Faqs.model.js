var mongoose = require('mongoose');

const Faqschema = new mongoose.Schema({
    question: String,
    answer: String,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

});


module.exports = mongoose.model('Faqs', Faqschema);