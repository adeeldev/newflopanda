const mongoose = require('mongoose');

// const Schema = mongoose.Schema;

/**
 * User Schema
 * @private
 */
const PageSchema = new mongoose.Schema({
  websiteId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  referrer: {
    type: String,
    default: '',
  },
  name: {
    type: String,
    default: '',
  },
  image: {
    type: String,
    default: ''
  },
  countryName: {
    type: String,
    default: '',
  },
  referer: {
    type: String,
    default: '',
  },
  html: {
    type: JSON,
    default: '',
  },
  url: {
    type: String,
    default: '',
  },
  height: {
    type: Number,
    default: 0,
  },
  width: {
    type: Number,
    default: 0,
  },
  pageHeight: {
    type: Number,
    default: 0,
  },
  shareHeatMap: {
    type: Boolean,
    default: null,
  },
  startTime: {
    type: Number,
  },
  isp: {
    type: String,
    default: '',
  },
  engagmentTime: {
    type: Number,
  },
  loadingTime: {
    type: String,
  },
  renderTime: {
    type: String,
  },
  clickRage: {
    type: Number,
    default: 0,
  },
  formInteract: {
    type: Number,
    default: 0,
  },
  formSubmit: {
    type: Number,
    default: 0,
  },
  clickError: {
    type: Number,
    default: 0,
  },
  isShared: {
    type: Number,
    default: 0,
  },
  returningVisitor: {
    type: Number,
    default: 0,
  },
  traficSource: {
    type: String,
    default: 'noReferrer',
  },
  resolution: {
    type: String,
    default: '',
  },
  device: {
    type: String,
    default: '',
  },
  osName: {
    type: String,
    default: '',
  },
  browserName: {
    type: String,
    default: '',
  },
  projectId: {
    type: String,
    default: ''
  },
  date: {
    type: Date,
    default: Date.now,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  timestamps: true,
});


PageSchema.statics = {

  async createPageScreen(options) {
    const { websiteId, pageName , id } = options;
    if (!websiteId && pageName ) throw new APIError({ message: 'WebsiteId & page name are missing' });
    var page = await this.findOne({ websiteId: websiteId, name: pageName }).exec();
    convert(page.url)
      .pipe(fs.createWriteStream('pageScreens/' + id + '.png'));
    const err = {
      status: httpStatus.UNAUTHORIZED,
      isPublic: true,
      message: ''
    };
    return true;
  },
  async getPageById(options){
    const { id } = options;
    if (!id ) throw new APIError({ message: 'Id is missing' });
    var page = await this.findById(id).exec();
    return page;
  },
  async getPageBySiteId(options){
    const { websiteId } = options;
    if (!websiteId ) throw new APIError({ message: 'Website Id is missing' });
    var page = await this.findById(websiteId).exec();
    return page;
  }
};


/**
 * @typedef Page
 */
module.exports = mongoose.model('Page', PageSchema);
