var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const APIError = require('../utils/APIError');
const httpStatus = require('http-status');

/**
 * User Schema
 * @private
 */
const UserFormSchema = new mongoose.Schema({
    pageName: {
        type: String,
        default: '',
    },
    formName: {
        type: String,
    },
    formAlias: {
        type: String
    },
    siteId: {
        type: String,
    },
    index: {
        type: Number
    },
    formId: {
        type: String,
    },
    form: {
        type: Schema.Types.ObjectId,
    },
    pageId: { 
        type: String,
    },
    fieldId: {
        type: String,
        default: '',
    },
    fieldName: {
        type: String,
        default: '',
    },
    fieldType: {
        type: String,
        default: '',
    },
    fields: [],
    fieldTagName: {
        type: String,
        default: '',
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

});



/**
 * @typedef UserFormSchema
 */

module.exports = mongoose.model('UserForm', UserFormSchema);