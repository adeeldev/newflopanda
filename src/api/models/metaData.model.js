const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * User Schema
 * @private
 */
const MetaDataSchema = new mongoose.Schema({
  recordingId: {
    type: Schema.Types.ObjectId,
  },
  pageId: {
    type: Schema.Types.ObjectId,
  },
  siteId: {
    type: Schema.Types.ObjectId,
  },
  site: {
    type: String,
  },
  pageName: {
    type: String,
  },
  views: {
    type: Number,
    default: 1,
  },
  loadingTime: {
    type: Number,
    default: 0
  },
  engagementTime: {
    type: Number,
    default: 0
  },
  visitTime: {
    type: Number,
    default: 0
  },
  clickRage: {
    type: Number,
    default: 0,
  },
  formInteract: {
    type: Number,
    default: 0,
  },
  formSubmit: {
    type: Number,
    default: 0,
  },
  clickError: {
    type: Number,
    default: 0
  },
  countryName: {
    type: String,
    default: ''
  },
  browserName: {
    type: String,
    default: ''
  },
  resolution: {
    type: String,
    default: ''
  },
  referer: {
    type: String,
    default: ''
  },
  osName: {
    type: String,
    default: ''
  },
  duration: {
    type: Number,
    default: 0
  },
  device: {
    type: String,
    default: 'all'
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
  calculatedAlready:
  {
    type : Boolean,
    default:0
  },
  sessionId:
  {
    type : String,
    default:0
  },
  clicks: {
    type: Number,
    default: 0
  },
  scroll: {
    type: Number,
    default: 0
  },
  date: {
    type: String,
    default: 0
  },
  pageSize:
  {
    type : String,
    default:0
  }
}, {
  timestamps: true,
});

/**
 * @typedef MetaData
 */
module.exports = mongoose.model('MetaData', MetaDataSchema);
