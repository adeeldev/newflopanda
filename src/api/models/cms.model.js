var mongoose = require('mongoose');

const CMSSchema = new mongoose.Schema({
    title: String,
    slug: String,
    keyword: String,
    description: String,
    details: String,
    status: Boolean,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

});


module.exports = mongoose.model('CMS', CMSSchema);