var mongoose = require('mongoose');
const httpStatus = require('http-status');
/**
 * User Schema
 * @private
 */
const CommentSchema = new mongoose.Schema({
    name: { type: String, default: '' },
    email: { type: String, default: '' },
    subject: { type: String, default: '' },
    message: { type: String, default: '' },
    blogId: { type: String, default: '' },
    type: { type: String, default: '' },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
});


/**
 * @typedef Comment
 */
module.exports = mongoose.model('Comment', CommentSchema);