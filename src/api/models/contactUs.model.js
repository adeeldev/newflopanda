var mongoose = require('mongoose');

const contactUsSchema = new mongoose.Schema({
    email: String,
    subject: String,
    message: String,
    createdAt: { type: Date, default: Date.now }
});


module.exports = mongoose.model('ContactUs', contactUsSchema);