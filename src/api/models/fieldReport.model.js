var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * User Schema
 * @private
 */
const fieldReportSchema = new mongoose.Schema({
    formId: {
        type: String,
    },
    form: {
        type: Schema.Types.ObjectId,
    },
    // fieldName: {
    //     type: String,
    // },
    // fieldId: {
    //     type: String,
    // },
    siteId: {
        type: String,
    },
    recordedDate: {
        type: String,
    },
    pageName: {
        type: String,
        default: '',
    },
    referer: {
        type: String,
        default: '',
    },
    device: {
        type: String,
        default: '',
    },
    recordingId: {
        type: String,
        default: ''
    },
    recordedDate: {
        type: String,
        default: ''
    },
    // firstName: {
    //     type: String,
    //     default: ''
    // },
    // value: {
    //     type: String,
    //     default: ''
    // },
    isRead: {
        type: Boolean,
        default: false
    },
    fields: {
        type: Array,
        default: []
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

});

/**
 * @typedef FieldReport
 */

module.exports = mongoose.model('FieldReport', fieldReportSchema);