const mongoose = require('mongoose');

/**
 * User Schema
 * @private
 */
const UserConversionPackageSchema = new mongoose.Schema({
  country: {
    type: Array,
    default: []
  },
  userId: {
    type: String,
    default: ''
  },
  maxSmsPerDay: {
    type: Number,
    default: 0
  },
  maxEmailPerDay: {
    type: Number,
    default: 0
  },
  websiteAdded: {
    type: Number,
    default: 0
  },
  emailAdded: {
    type: Number,
    default: 0
  },
  numberAdded: {
    type: Number,
    default: 0
  },
  toalUsersAllowed: {
    type: Number,
    default: 0
  },
  isSmsService: {
    type: Boolean,
    default: false
  },
  packageId: {
    type: String,
    default: ''
  },
  paymentId: {
    type: String,
    default: ''
  },
  isActive: {
    type: Boolean,
    default: false
  },
  paymentSuccess: {
    type: Boolean,
    default: false
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  isActive: {
    type: Boolean,
    default: true
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
  updatedData : {type: Object, default:{}},
  upDownPaymentSuccess : {type : Boolean, default : true}
  
}, {
    timestamps: true,
  });

/**
 * @typedef UserConversionPackage
 */
module.exports = mongoose.model('UserConversionPackage', UserConversionPackageSchema);
