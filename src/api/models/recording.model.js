const mongoose = require('mongoose');
const httpStatus = require('http-status');

/**
 * User Schema
 * @private
 */
const RecordingSchema = new mongoose.Schema({
  websiteId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  siteId: {
    type: String,
    default: '',
  },
  site: {
    type: String,
    default: '',
  },
  sessionId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  pageName: {
    type: String,
    default: '',
  },
  htmlCopy: {
    type: JSON,
    default: '',
  },
  height: {
    type: Number,
    default: '',
  },
  width: {
    type: Number,
    default: '',
  },
  resolution: {
    type: String,
    default: '',
  },
  windowsHeight: {
    type: Number,
    default: '',
  },
  recordingStartTime: {
    type: Object,
    default: null,
  },
  recordingEndTime: {
    type: Date,
  },
  tags: [
    {
      name: String,
      isDefault: {
        type: Boolean,
        default: false
      },
    },
  ],
  clickRage: {
    type: Number,
    default: 0,
  },
  formInteract: {
    type: Number,
    default: 0,
  },
  formSubmit: {
    type: Number,
    default: 0,
  },
  clickError: {
    type: Number,
    default: 0
  },
  startTime: {
    type: Object,
    default: null,
  },
  duration: {
    type: Number,
    default: 0,
  },
  styles: {
    type: Array,
    default: [],
  },
  countryName: {
    type: String,
    default: '',
  },
  countryCode: {
    type: String,
    default: ''
  },
  referer: {
    type: String,
    default: 'No Referrer',
  },
  osName: {
    type: String,
    default: '',
  },
  browserName: {
    type: String,
    default: '',
  },
  device: {
    type: String,
    default: '',
  },
  ip: {
    type: String,
    default: '',
  },
  latitude: {
    type: String,
    default: '',
  },
  longitude: {
    type: String,
    default: '',
  },
  traficSource: {
    type: String,
    default: 'noReferrer',
  },
  returningVisitor: {
    type: String,
    default: 'firsttime',
  },
  timeInSec: {
    type: Number,
    default: 0
  },
  adUrl: {
    type: String,
    default: ''
  },
  scroll: {
    type: Number,
    default: 0
  },
  projectId: {
    type: String,
    default: ''
  },
  date: {
    type: Date,
    default: Date.now,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  timestamps: true,
});


RecordingSchema.statics = {
  async getRecording(options) {
    const { siteId } = options;
    if (!websiteId ) throw new APIError({ message: 'SiteId is missing' });
    const recording = await this.find({ siteId }).exec();

    const err = {
      status: httpStatus.UNAUTHORIZED,
      isPublic: true,
      message: '',
    };
    return recording;
  },
  async filterRecordingForForm(options) {
    const recording = await this.find(options).exec();
    const err = {
      status: httpStatus.UNAUTHORIZED,
      isPublic: true,
      message: '',
    };
    return recording;
  },
};

/**
 * @typedef Recording
 */
module.exports = mongoose.model('Recording', RecordingSchema);
