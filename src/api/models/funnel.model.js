const mongoose = require('mongoose');
const APIError = require('../utils/APIError');
const httpStatus = require('http-status');
const Page = require('./page.model');
var async = require("async");
var Schema = mongoose.Schema;

/**
 * User Schema
 * @private
 */
const FunnelSchema = new mongoose.Schema({
    siteId: {
        type: mongoose.Schema.Types.ObjectId,
    },
    name: {
        type: String,
        default: '',
    },
    pages: [],
    alias: [],
    createdAt: {
        type: Date,
        default: Date.now,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    },
});

/**
 * Methods
 */
FunnelSchema.method({
    transformFunnelList(funnels) {
        const transformed = [];
        funnels.forEach(f => {
            let max = 0;
            let min = f.pages[0].pageVisit;
            f.pages.forEach(p => {
                if (max < p.pageVisit) {
                    max = p.pageVisit
                }
                if (min > p.pageVisit) {
                    min = p.pageVisit;
                }
            })
            transformed.push({
                id: f._id,
                funelName: f.funelName,
                visitor: max,
                conversion: min,
                page: f.pages
            })

        })

        return transformed;
    },
   transformFunnelReport(funnel) {
        let resObj = [];
        let preObj = null;
        let dropOff = 0;
        let maxVisit = 0;
        funnel.pages.forEach(p => {
            if (maxVisit < p.pageVisit) {
                maxVisit = p.pageVisit;
            }
        })
        funnel.pages.forEach(p => {
            if (preObj == null) {
                preObj = p;
            } else {
                dropOff = preObj.pageVisit - p.pageVisit;
                let vr = 0;
                let dr = 0;
                if (maxVisit > 0) {
                    vr = (preObj.pageVisit / maxVisit * 100).toFixed(2);
                }
                if (preObj.pageVisit > 0) {
                    dr = (p.pageVisit / preObj.pageVisit * 100).toFixed(2);
                }
                resObj.push({
                    visitRatio: vr,
                    dropOff: dropOff,
                    pageVisit: preObj.pageVisit,
                    pageName: preObj.pageName,
                    dropRatio: dr
                })
                preObj = p;
            }
        })
        let vrn = 0;
        if (maxVisit > 0) {
            vrn = (funnel.pages[funnel.pages.length - 1].pageVisit / maxVisit * 100).toFixed(2)
        }
        resObj.push({
            visitRatio: vrn,
            pageName: funnel.pages[funnel.pages.length - 1].pageName,
            pageVisit: funnel.pages[funnel.pages.length - 1].pageVisit
        })
        return resObj;
    },
    getWhereClauseQueryForFilter(req) {
        try {
            let whereClauseQuery = {}

            if (req.query.pageCount != undefined) {
                whereClauseQuery['pageCount'] = req.query.pageCount;
            }
            if(req.query.duration !== undefined){
                whereClauseQuery['duration'] = req.query.duration;
            }
            if (req.query.device !=  undefined) {
                whereClauseQuery['devices'] = req.query.device;
            }
            if (req.query.countryName !=  undefined) {
                whereClauseQuery['countryName'] = req.query.countryName;
            }
            
            if (req.query.osName != undefined) {
                whereClauseQuery['osName'] = req.query.osName;
            }
            if (req.query.browserName != undefined) {
                whereClauseQuery['browserName'] = req.query.browserName;
            }
            if (req.query.resolution !== undefined) {
                whereClauseQuery['screenResolution'] = req.query.resolution;
            }

            if (req.query.traficSource !== undefined) {
                whereClauseQuery['refferrer'] = req.query.traficSource;
            }
            // if (req.body.parmeters.filter.findIndex(x => x.name === "VisitorType") > -1) {
            //     whereClauseQuery['VisitorType'] = req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "VisitorType")].value;
            // }
            if (req.body.clickError != undefined) {
                whereClauseQuery['tags'].push("click-error")
            }
            if (req.body.clickRage != undefined) {
                whereClauseQuery['tags'].push("click-rage")
            }
            if (req.body.formInteract != undefined) {
                whereClauseQuery['tags'].push("form-interact")
            }
            if (req.body.formSubmit != undefined) {
                whereClauseQuery['tags'].push("submit")
            }
            // if (req.body.parmeters['navigation'] != undefined && req.body.parmeters['navigation'].length > 0 && req.body.parmeters['navigation'] != null) {
            //     whereClauseQuery['navigation'] = true;
            // }
            return whereClauseQuery
        }
        catch (error) {
            console.log(error)
            return error
        }

    }
});


FunnelSchema.statics = {

    async getFunnel(options) {
        const { websiteId } = options;
        if (!websiteId) throw new APIError({ message: 'WebsiteId is missing' });
        var funnel = await this.find({ websiteId }).exec();

        const err = {
            status: httpStatus.UNAUTHORIZED,
            isPublic: true,
            message: ''
        };
        return funnel;
    },
    async getFunnelById(options) {
        const { funnelId } = options;
        if (!funnelId) throw new APIError({ message: 'Funnel Id is missing' });
        var funnel = await this.findById(funnelId).exec();

        const err = {
            status: httpStatus.UNAUTHORIZED,
            isPublic: true,
            message: ''
        };
        return funnel;
    }
};

/**
 * @typedef Funnel
 */
module.exports = mongoose.model('Funnel', FunnelSchema);