var mongoose = require('mongoose');

const SubscriptionSchema = new mongoose.Schema({
    email: { type: String, default: '' },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});


module.exports = mongoose.model('Subscription', SubscriptionSchema);