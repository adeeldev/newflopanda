const mongoose = require('mongoose');
const crypto = require('crypto');
const moment = require('moment-timezone');
const {
  jwtExpirationInterval
} = require('../../config/vars');
/**
 * Refresh Token Schema
 * @private
 */
const refreshTokenSchema = new mongoose.Schema({
  token: {
    type: String,
    required: true
  },
  jwtToken: {
    type: String,
    required: true
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  userEmail: {
    type: 'String',
    ref: 'User',
    required: true,
  },
  isCreatedByAdmin: {
    type: Boolean,
    default: false,
    required: true,
  },
  expires: { type: Date },
});

refreshTokenSchema.statics = {

  /**
   * Generate a refresh token object and saves it into the database
   *
   * @param {User} user
   * @returns {RefreshToken}
   */
  generate(user, jwtToken) {
    const userId = user._id;
    const userEmail = user.email;
    const token = `${userId}.${crypto.randomBytes(40).toString('hex')}`;
    const expires = moment().add(jwtExpirationInterval, 'minutes');
    const tokenObject = new RefreshToken({
      token, userId, userEmail, expires, jwtToken
    });
    tokenObject.save();
    return tokenObject;
  },
  generateUserTokenResponseForAdmin(user, jwtToken) {
    const userId = user._id;
    const userEmail = user.email;
    const token = `${userId}.${crypto.randomBytes(40).toString('hex')}`;
    const expires = moment().add(jwtExpirationInterval, 'minutes');
    const isCreatedByAdmin = true;
    const tokenObject = new RefreshToken({
      token, userId, userEmail, expires, jwtToken, isCreatedByAdmin
    });
    tokenObject.save();
    return tokenObject;
  }
};

/**
 * @typedef RefreshToken
 */
const RefreshToken = mongoose.model('RefreshToken', refreshTokenSchema);
module.exports = RefreshToken;
