var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const APIError = require('../utils/APIError');
const httpStatus = require('http-status');

/**
 * User Schema
 * @private
 */
const FormFieldSchema = new mongoose.Schema({
    pageName: {
        type: String,
        default: '',
    },
    formId: {
        type: String,
    },
    form: {
        type: Schema.Types.ObjectId,
    },
    pageId: { 
        type: String,
    }, 
    fieldId: {
        type: String,
        default: '',
    },
    fieldName: {
        type: String,
        default: '',
    },
    fieldAlias: {
        type: String,
        default: '',
    },
    fieldType: {
        type: String,
        default: '',
    },
    fieldTagName: {
        type: String,
        default: '',
    },
    fieldIndex: {
        type: Number,
    },
    path: {
        type: String
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

});



/**
 * @typedef FormFieldSchema
 */

module.exports = mongoose.model('FormField', FormFieldSchema);