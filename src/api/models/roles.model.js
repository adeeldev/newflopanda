var mongoose = require('mongoose');

const Roleschema = new mongoose.Schema({
    roleId: {
        type: String,
        default: ""
    },
    allowFeatures: {
        type: Array,
        default: []
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});


module.exports = mongoose.model('Faqs', Roleschema);