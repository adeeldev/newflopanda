var mongoose = require('mongoose');

const logSchema = new mongoose.Schema({
reasons: String,
email: String,
logType:String,
siteName:String,
sessionId: {
  type: mongoose.Schema.Types.ObjectId,
  ref: 'Session',
},
userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
createdAt: { type: Date, default: Date.now },
updatedAt: { type: Date, default: Date.now },

},{
  timestamps: true
});

module.exports = mongoose.model('log', logSchema);