var mongoose = require('mongoose');
const httpStatus = require('http-status');
/**
 * User Schema
 * @private
 */
const surveySchema = new mongoose.Schema({
    name: { type: String, default: '' },
    question: [{
        question: { type: String, default: '' },
        fieldType: { type: String, default: '' },
        options: { type: Array, default: [] },
        value: { type: String, default: '' }
    }],
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
});


/**
 * @typedef Survey
 */
module.exports = mongoose.model('Survey', surveySchema);