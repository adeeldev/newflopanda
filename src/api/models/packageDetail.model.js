var mongoose = require('mongoose');
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
const Schema = mongoose.Schema;
/**
 * User Schema
 * @private
 */
const PackageDetailSchema = new mongoose.Schema({
    packageId: { type: String },
    paymentId: { type: String },
    userId: { type: String },
    websitesId: { type: Array, default: [] },
    isDowngrade: { type: Boolean, default: false },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
});


/**
 * @typedef PackageDetail
 */
module.exports = mongoose.model('PackageDetail', PackageDetailSchema);