var mongoose = require('mongoose');
const httpStatus = require('http-status');
/**
 * User Schema
 * @private
 */
const BlogSchema = new mongoose.Schema({
    name: { type: String, default: '' },
    content: { type: String, default: '' },
    blogImage: { type: String, default: '' },
    author: { type: String, default: '' },
    categoryId: { type: String, default: '' },
    categoryName: { type: String, default: '' },
    commentCount: { type: Number, default: 0 },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
});


/**
 * @typedef Blog
 */
module.exports = mongoose.model('Blog', BlogSchema);