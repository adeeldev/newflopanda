const mongoose = require('mongoose');

/**
 * User Schema
 * @private
 */
const ConversionPriceSchema = new mongoose.Schema({
  country: {
    type: String,
    default: ''
  },
  costPerSms: {
    type: Number,
    default: 0
  },
  costPerEmail: {
    type: Number,
    default: 0
  },
  maxSmsPerDay: {
    type: Number,
    default: 0
  },
  maxEmailPerDay: {
    type: Number,
    default: 0
  },
  daysPerMonth: {
    type: Number,
    default: 0
  },
  package1: 
    {
      totalCostUSD: Number,
      totalCostGbp: Number,
      emailCostUSD: Number,
      emailCostGBP: Number,
      totalUsers: Number
    }
  ,
  package2: 
    {
      totalCostUSD: Number,
      totalCostGbp: Number,
      emailCostUSD: Number,
      emailCostGBP: Number,
      totalUsers: Number
    }
  ,
  package3: 
    {
      totalCostUSD: Number,
      totalCostGbp: Number,
      emailCostUSD: Number,
      emailCostGBP: Number,
      totalUsers: Number
    }
  ,
  package4: 
    {
      totalCostUSD: Number,
      totalCostGbp: Number,
      emailCostUSD: Number,
      emailCostGBP: Number,
      totalUsers: Number
    }
  ,
  package5: 
    {
      totalCostUSD: Number,
      totalCostGbp: Number,
      emailCostUSD: Number,
      emailCostGBP: Number,
      totalUsers: Number
    }
  ,
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  timestamps: true,
});

/**
 * @typedef ConversionPrice
 */
module.exports = mongoose.model('ConversionPrice', ConversionPriceSchema);
