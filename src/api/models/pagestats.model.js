const mongoose = require('mongoose');

// const Schema = mongoose.Schema;

/**
 * User Schema
 * @private
 */
const PagestatsSchema = new mongoose.Schema({
  websiteId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  pageName: {
    type: String,
    default: '',
  },
  clicks: {
    type: Number,
    default: 0
  },
  engageTime: {
    type: Number,
    default: 0,
  },
  visitTime: {
    type: Number,
    default: 0,
  },
  loadingTime: {
    type: Number,
    default: 0,
  },
  scrollPercentage: {
    type: Number,
    default: '',
  },
  views: {
    type: Number,
    default: '',
  },
  pageId: {
    type: String,
    default: '',
  },
  pageSize: {
    type: String,
    default: ''
  },
  clickRage: {
    type: Number,
    default: 0,
  },
  formInteract: {
    type: Number,
    default: 0,
  },
  formSubmit: {
    type: Number,
    default: 0,
  },
  clickError: {
    type: Number,
    default: 0
  },
  date: {
    type: String,
    default: '',
  }
}, {
  timestamps: true,
});

/**
 * @typedef Pagestats
 */
module.exports = mongoose.model('Pagestats', PagestatsSchema);
