const mongoose = require('mongoose');
const httpStatus = require('http-status');

const Schema = mongoose.Schema;
const APIError = require('../utils/APIError');

/**
 * User Schema
 * @private
 */
const WebsiteSchema = new mongoose.Schema({
  userId: {
    type: Schema.Types.ObjectId,
  },
  name: {
    type: String,
    default: '',
  },
  domain: [],
  recordingRate: {
    type: Number,
    default: 100,
  },
  blockingEUTraffic: {
    type: Boolean,
    default: false,
  },
  iPAnonymization: {
    type: Boolean,
    default: true,
  },
  honorTracking: {
    type: Boolean,
    default: true,
  },
  automaticallyExcludedIP: {
    type: Array,
    default: [],
  },
  pageIdentifiers: {
    type: Array,
    default: [],
  },
  mergeURLs: {
    type: Array,
    default: [],
  },
  turnOffStickyKeys: {
    type: Boolean,
    default: true,
  },
  excludedContent: {
    type: Array,
    default: [],
  },
  whitelistedFields: {
    type: Array,
    default: [],
  },
  scriptHeader: {
    type: String,
    default: '',
  },
  paymentScript: {
    type: String,
    default: ''
  },
  stopRecording: {
    type: Boolean,
    default: false,
  },
  visitor: {
    type: Number,
    default: 0,
  },
  script: {
    type: String,
    default: '',
  },
  pageIdentifiers: {
    type: Array,
    default: [],
  },
  isSubscribe: {
    type: Boolean,
    default: true
  },
  recordings: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Recording',
    },
  ],
  isDemoSite: {
    type: Boolean,
    default: false
  },
  showCompaign: {
    type: Boolean,
    default: false
  },
  subDomain: {
    type: Boolean,
    default: false
  },
  domainId: {
    type: String,
    default: ''
  },
  protocol: {
    type: String,
    default: 'https'
  },
  status: {
    type: String,
    default: 'active'
  },
  isScriptInstall: {
    type: Boolean,
    default: false
  },
  anonymizationReason: {
    type: String,
    default: ''
  },
  trackingReason: {
    type: String,
    default: ''
  },
  keystrokesReason: {
    type: String,
    default: ''
  },
  isDownloadFormDropoff: {
    type: Boolean,
    default: false
  },
  isDownloadClickError: {
    type: Boolean,
    default: false
  },
  isDownloadClickRage: {
    type: Boolean,
    default: false
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  timestamps: true,
});
WebsiteSchema.statics = {
  async get(id) {
    try {
      let websites;
      if (mongoose.Types.ObjectId.isValid(id)) {
        websites = await this.find({ userId: id }).select('name _id').exec();
      }
      if (websites) {
        return websites;
      }

      throw new APIError({
        message: 'Website not exist with this user',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
};


/**
 * @typedef Website
 */
module.exports = mongoose.model('Website', WebsiteSchema);
