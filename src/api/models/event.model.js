const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * User Schema
 * @private
 */
const EventSchema = new mongoose.Schema({
  recordingId: {
    type: Schema.Types.ObjectId,
  },
  pageId: {
    type: Schema.Types.ObjectId,
  },
  siteId: {
    type: Schema.Types.ObjectId,
  },
  site: {
    type: String,
  },
  pageName: {
    type: String,
  },
  type: {
    type: String,
  },
  x: {
    type: Number,
  },
  y: {
    type: Number,
  },
  offsetLeft: {
    type: Number,
  },
  offsetTop: {
    type: Number,
  },
  path: {
    type: String,
    default: ''
  },
  tagName: {
    type: String,
  },
  value: {
    type: Number,
    default: 1,
  },
  className: {
    type: String
  },
  innerHTML: {
    type: String
  },
  checked: {
    type: Boolean
  },
  eValue: {
    type: String,
    default: '',
  },
  css: {
    type: String,
    default: '',
  },
  cssObj: {
    type: Object,
  },
  from: {
    type: Number,
  },
  to: {
    type: Number,
  },
  percentage: {
    type: Number,
  },
  clickCount: {
    type: Number,
  },
  scrolCount: {
    type: Number,
  },
  time: {
    type: Number,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  clickRage: {
    type: Number,
    default: 0,
  },
  formInteract: {
    type: Number,
    default: 0,
  },
  formSubmit: {
    type: Number,
    default: 0,
  },
  clickError: {
    type: Number,
    default: Number
  },
  countryName: {
    type: String,
    default: ''
  },
  browserName: {
    type: String,
    default: ''
  },
  resolution: {
    type: String,
    default: ''
  },
  referer: {
    type: String,
    default: ''
  },
  osName: {
    type: String,
    default: ''
  },
  device: {
    type: String,
    default: 'all'
  },
  isFieldSets: {
    type: Boolean,
    default: false
  },
  fieldSets: {
    type: Array,
    default: []
  },
  timeInSec: {
    type: Number,
    default: 0
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
  height: {
    type: Number,
    default: 0
  },
  width: {
    type: Number,
    default: 0
  },
  scrollY: {
    type: Number,
    default: 0
  },
  calculatedAlready:
  {
    type : Boolean,
    default:0
  },
  sessionId:
  {
    type : String,
    default:0
  }
}, {
  timestamps: true,
});

/**
 * @typedef Event
 */
module.exports = mongoose.model('Event', EventSchema);
