var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const APIError = require('../utils/APIError');
const httpStatus = require('http-status');

/**
 * User Schema
 * @private
 */
const FieldTrackingSchema = new mongoose.Schema({
    pageName: {
        type: String,
        default: '',
    },
    formId: {
        type: String,
    },
    form: {
        type: Schema.Types.ObjectId,
    },
    formIndex: {
        type: Number,
    },
    index: {
        type: Number
    },
    fieldName: {
        type: String,
    },
    interactTime: {
        type: Number,
        default: 0,
    },
    startTime: {
        type: Number,
        default: 0,
    },
    pageId: {
        type: String,
    },
    fieldId: {
        type: String,
    },
    siteId: {
        type: String,
    },
    isTracked: {
        type: Number,
        default: 0,
    },
    interact: {
        type: Number,
        default: 1,
    },
    conversion: {
        type: Number,
        default: 0,
    },
    dropOff: {
        type: Number,
        default: 0,
    },
    visit: {
        type: Number,
        default: 1,
    },
    recordedDate: {
        type: String,
    },
    pageName: {
        type: String,
        default: '',
      },
    countryName: {
        type: String,
        default: '',
    },
    referer: {
        type: String,
        default: '',
    },
    osName: {
        type: String,
        default: '',
    },
    browserName: {
        type: String,
        default: '',
    },
    resolution: {
        type: String,
        default: '',
    },
    device: {
        type: String,
        default: '',
    },
    path: {
        type: String
    },
    lastFoucus: {
        type: Number
    },
    formSubmit: {
        type: String,
        default: 'false'
    },
    clickError: {
        type: String,
        default: 'false'
    },
    formInteract: {
        type: String,
        default: 'false'
    },
    clickRage: {
        type: String,
        default: 'false'
    },
    recordingId: {
        type: String,
        default: ''
    },
    value: {
        type: String,
        default: ''
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

});

/**
 * @typedef FieldTrackingSchema
 */

module.exports = mongoose.model('FieldTracking', FieldTrackingSchema);