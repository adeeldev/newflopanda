const mongoose = require('mongoose');

/**
 * User Schema
 * @private
 */
const MouseMoveSchema = new mongoose.Schema({
  recordingId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  pageId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  type: {
    type: String,
  },
  x: {
    type: Number,
  },
  y: {
    type: Number,
  },
  value: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  timestamps: true,
});

/**
 * @typedef MouseMove
 */
module.exports = mongoose.model('MouseMove', MouseMoveSchema);
