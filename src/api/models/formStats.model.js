var mongoose = require('mongoose');


/**
 * User Schema
 * @private
 */
const FormStatsSchema = new mongoose.Schema({
    pageName: {
        type: String,
        default: '',
    },
    siteId: {
        type: String,
        default: ''
    },
    recordingId: { 
        type: String,
    }, 
    device: {
        type: String,
        default: 0,
    },
    formIndex: {
        type: Number,
        default: 0,
    },
    formSubmit: {
        type: Number,
        default: 0,
    },
    dropOff: {
        type: Number,
        default: 0,
    },
    formInteract: {
        type: Number,
        default: 0
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

});



/**
 * @typedef FormStatsSchema
 */

module.exports = mongoose.model('FormStats', FormStatsSchema);