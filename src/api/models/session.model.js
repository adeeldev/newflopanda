const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const APIError = require('../utils/APIError');
const httpStatus = require('http-status');


/**
 * User Schema
 * @private
 */
const SessionSchema = new mongoose.Schema({
  siteId: {
    type: Schema.Types.ObjectId,
  },
  site: {
    type: String,
  },
  recordings: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Recording',
      default: [],
    },
  ],
  userData: [
    {
      type: Schema.Types.ObjectId,
      ref: 'FieldTracking',
      default: [],
    },
  ],
  pages: [],
  funnelPages: [],
  image: {
    type: String,
    default: '',
  },
  ip: {
    type: String,
    default: '',
  },
  city: {
    type: String,
    default: '',
  },
  region: {
    type: String,
    default: '',
  },
  regionCode: {
    type: String,
    default: '',
  },
  countryCode: {
    type: String,
    default: '',
  },
  continentCode: {
    type: String,
    default: '',
  },
  continentName: {
    type: String,
    default: '',
  },
  organisation: {
    type: String,
    default: '',
  },
  language: {
    type: String,
    default: '',
  },
  htmlCopy: {
    type: JSON,
    default: '',
  },
  pageName: {
    type: String,
    default: '',
  },
  countryName: {
    type: String,
    default: '',
  },
  referer: {
    type: String,
    default: '',
  },
  osName: {
    type: String,
    default: '',
  },
  browserName: {
    type: String,
    default: '',
  },
  resolution: {
    type: String,
    default: '',
  },
  device: {
    type: String,
    default: ''
  },
  latitude: {
    type: String,
    default: '',
  },
  longitude: {
    type: String,
    default: '',
  },
  favorite: {
    type: Boolean,
    default: false,
  },
  watch: {
    type: Boolean,
    default: false,
  },
  doNotTrack: {
    type: Boolean,
    default: false,
  },
  share: {
    type: Boolean,
    default: false,
  },
  tags: [
    {
      name: String,
      isDefault: {
        type: Boolean,
        default: false
      },
    },
  ],
  clickRage: {
    type: Number,
    default: 0,
  },
  formInteract: {
    type: Number,
    default: 0,
  },
  formSubmit: {
    type: Number,
    default: 0,
  },
  clickError: {
    type: Number,
    default: 0
  },
  paymentTag: {
    type: Number,
    default: 0
  },
  isShared: {
    type: Boolean,
    default: false,
  },
  returningVisitor: {
    type: String,
    default: 'firsttime',
  },
  pageCount: {
    type: Number,
    default: 0
  },
  recDuration: {
    type: Number,
    default: 0
  },
  traficSource: {
    type: String,
    default: 'noReferrer',
  },
  windowsHeight: {
    type: Number,
    default: '',
  },
  browserVer: {
    type: String,
    default: ''
  },
  osVer: {
    type: String,
    default: '',
  },
  editVariables: [{
    key: {
      type: String,
    },
    value: {
      type: String
    },
  }],
  utmSource: {
    type: String,
    default: ''
  },
  utmMedium: {
    type: String,
    default: ''
  },
  utmCampaign: {
    type: String,
    default: ''
  },
  utmContent: {
    type: String,
    default: ''
  }, 
  utmTerm: {
    type: String,
    default: ''
  },
  gclid: {
    type: String,
    default: ''
  },
  adUrl: {
    type: String,
    default: ''
  },
  width: {
    type: Number,
    default: 0,
  },
  recordingStartTime: {
    type: Object,
    default: null,
  },
  recordingEndTime: {
    type: Date,
  },
  network: {
    type: String,
    default: ''
  },
  engine: {
    type: String,
    default: ''
  },
  isFunnelRead: {
    type: Boolean,
    default: false
  },
  adId: {
    type: String,
    default: ''
  },
  adName: {
    type: String,
    default: ''
  },
  type: {
    type: String,
    default: 'online'
  },
  reportCode: {
    type: String
  },
  contactNo: {
    type: String,
    default: ''
  },
  cpc: {
    type: String,
    default: ''
  },
  phone: {
    type: Array,
    default: []
  },
  surname: {
    type: Array,
    default: []
  },
  zipCode: {
    type: Array,
    default: []
  },
  funnelRecordData: [],
  price: {
    type: Number,
    default: 0
  },
  date: {
    type: Date,
    default: '',
  },
  projectId: {
    type: String,
    default: ''
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  timestamps: true,
  collation: { locale: "en_US", numericOrdering: true } 
});


SessionSchema.statics = {
  async recordignData(recordingId, sessionId) {
    try {
      let session;
      if (mongoose.Types.ObjectId.isValid(sessionId)) {
        session = await this.update({ _id: sessionId }, { $push: { recordingId: recordingId } }).exec();
      }
      if (session) {
        return session;
      }

      throw new APIError({
        message: 'User does not exist',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
};


/**
 * @typedef Session
 */
module.exports = mongoose.model('Session', SessionSchema);
