var mongoose = require('mongoose');
/**
 * User Schema
 * @private
 */
const aclSchema = new mongoose.Schema({
    role: { type: String, default: '' },
    list: [{
        role: { type: String, default: '' },
        resource: { type: String, default: '' },
        action: { type: String, default: '' },
        attributes: { type: String, default: '' }
    }],
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
});


/**
 * @typedef Acl
 */
module.exports = mongoose.model('Acl', aclSchema);