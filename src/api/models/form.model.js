var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const APIError = require('../utils/APIError');
const httpStatus = require('http-status');

/**
 * User Schema
 * @private
 */
const FormSchema = new mongoose.Schema({
    siteId: {
        type: Schema.Types.ObjectId, 
        ref: 'Website' 
    },
    pageName: {
        type: String,
        default: '',
    },
    siteId: {
        type: String,
        default: '',
    },
    pageId: {
        type: String,
        default: '',
    },
    formName: {
        type: String,
        default: '',
    },
    formAlias: {
        type: String,
        default: '',
    },
    index: {
        type: Number,
    },
    fields: [
        {
          type: Schema.Types.ObjectId,
          ref: 'FormField',
          default: [],
        },
    ],
    isCreatedByUser: { type: Boolean, default: false },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

});

/**
 * Methods
 */
FormSchema.method({
    getWhereClauseQueryForFilter(req) {
        let whereClauseQuery = []
        let whereClauseRec = {}
        if (req.body.parmeters.devices.length != 0 && req.body.parmeters.devices.length < 3) {
            whereClauseQuery.push(req.body.parmeters.devices);
            whereClauseRec['devise'] = req.body.parmeters.devices;

        }
        if (req.body.parmeters.filter.findIndex(x => x.name === "OS") > -1) {
            whereClauseQuery.push(req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "OS")].value);
            whereClauseRec['osName'] = req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "OS")].value
        }
        if (req.body.parmeters.filter.findIndex(x => x.name === "VisitorType") > -1) {
            let returningVisitor = "newVisitor";
            let returningVisitorBool = false;
            if (req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "VisitorType")].value == "Returning visitor") {
                returningVisitor = "returningVisitor";
                whereClauseRec = true;
            }
            whereClauseQuery.push(returningVisitor);
            whereClauseQuery['returningVisitor'] = returningVisitorBool;
        }
        if (req.body.parmeters.filter.findIndex(x => x.name === "Browser") > -1) {
            whereClauseQuery.push(req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "Browser")].value);
            whereClauseRec['browserName'] = req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "Browser")].value;
        }
        if (req.body.parmeters.filter.findIndex(x => x.name === "ScreenResolution") > -1) {
            whereClauseQuery.push(req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "ScreenResolution")].value);
            whereClauseRec['resolution'] = req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "ScreenResolution")].value;

        }
        if (req.body.parmeters.filter.findIndex(x => x.name === "TrafficSource") > -1) {
            let noRef = null;
            if (req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "TrafficSource")].value == "noReferrer") {
                whereClauseQuery.push('unknown');
                whereClauseRec['traficSource'] = 'noReferrer';
            } else {
                whereClauseQuery.push(req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "TrafficSource")].value);
                whereClauseRec['traficSource'] = 'noReferrer';

            }
        }
        if (req.body.parmeters.filter.findIndex(x => x.name === "countries") > -1) {
            let countryName = req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "countries")].value;
            let countryCode = countries.getCode(countryName)
            whereClauseQuery.push(countryCode);
            whereClauseRec['traficSource'] = countryCode;
        }
        if (req.body.parmeters['tag'] != undefined && req.body.parmeters['tag'].length > 0 && req.body.parmeters['tag'] != null) {
            whereClauseQuery.push(req.body.parmeters['tag']);
        }
        whereClauseRec['siteId'] = req.body.siteId;
        let resObj = {
            whereClauseQuery: whereClauseQuery,
            whereClauseRec: whereClauseRec
        }
        return resObj;
    },
    getWhereClauseQueryForFilterFormReport(req) {
        let whereClauseQuery = []
        let whereClauseCode = {}
        if (req.body.parmeters.devices.length != 0 && req.body.parmeters.devices.length < 3) {
            whereClauseQuery.push(req.body.parmeters.devices);
        }
        if (req.body.parmeters.filter.findIndex(x => x.name === "OS") > -1) {
            whereClauseQuery.push(req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "OS")].value);
        }
        if (req.body.parmeters.filter.findIndex(x => x.name === "VisitorType") > -1) {
            let returningVisitor = "newVisitor";
            if (req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "VisitorType")].value == "Returning visitor") {
                returningVisitor = "returningVisitor";
            }
            whereClauseQuery.push(returningVisitor);
        }
        if (req.body.parmeters.filter.findIndex(x => x.name === "Browser") > -1) {
            whereClauseQuery.push(req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "Browser")].value);
        }
        if (req.body.parmeters.filter.findIndex(x => x.name === "ScreenResolution") > -1) {
            whereClauseQuery.push(req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "ScreenResolution")].value);
        }
        if (req.body.parmeters.filter.findIndex(x => x.name === "TrafficSource") > -1) {
            let noRef = null;
            if (req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "TrafficSource")].value == "noReferrer") {
                whereClauseQuery.push('unknown');
            } else {
                whereClauseQuery.push(req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "TrafficSource")].value);
            }
        }
        if (req.body.parmeters.filter.findIndex(x => x.name === "countries") > -1) {
            let countryName = req.body.parmeters.filter[req.body.parmeters.filter.findIndex(x => x.name === "countries")].value;
            let countryCode = countries.getCode(countryName)
            whereClauseQuery.push(countryCode);
        }
        if (req.body.parmeters['tag'] != undefined && req.body.parmeters['tag'].length > 0 && req.body.parmeters['tag'] != null) {
            whereClauseQuery.push(req.body.parmeters['tag']);
        }
        let resObj = {
            whereClauseQuery: whereClauseQuery,
            whereClauseRec: whereClauseRec
        }
        return resObj;
    }
});

FormSchema.statics = {
    async getPageForms(options) {
        const { websiteId, pageName } = options;
        if (!websiteId && !pageName) throw new APIError({ message: 'WebsiteId & Page name are missing' });
        var form = await this.findOne({ websiteId, pageName }).exec();

        const err = {
            status: httpStatus.UNAUTHORIZED,
            isPublic: true,
            message: ''
        };
        return form;
    },
    async getFormById(options) {
        const { id } = options;
        if (!id) throw new APIError({ message: 'Form Id is missing' });
        var form = await this.findById(id).exec();

        const err = {
            status: httpStatus.UNAUTHORIZED,
            isPublic: true,
            message: ''
        };
        return form;
    },
    async getallSiteForms(options) {
        const { websiteId, isCreatedByUser } = options;
        if (!websiteId && !isCreatedByUser) throw new APIError({ message: 'WebsiteId & isCreatedByUser are missing' });
        var form = await this.find({ websiteId, isCreatedByUser }).exec();

        const err = {
            status: httpStatus.UNAUTHORIZED,
            isPublic: true,
            message: ''
        };
        return form;
    },
    async getallSiteFormsBySiteId(options) {
        const { websiteId } = options;
        if (!websiteId) throw new APIError({ message: 'WebsiteId is missing' });
        var form = await this.find({ websiteId }).exec();

        const err = {
            status: httpStatus.UNAUTHORIZED,
            isPublic: true,
            message: ''
        };
        return form;
    }

};

/**
 * @typedef Form
 */

module.exports = mongoose.model('Form', FormSchema);