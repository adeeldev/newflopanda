var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const APIError = require('../utils/APIError');
const httpStatus = require('http-status');

/**
 * User Schema
 * @private
 */
const InteractionsSchema = new mongoose.Schema({
    pageName: {
        type: String,
        default: '',
    },
    formId: {
        type: String,
    },
    form: {
        type: Schema.Types.ObjectId,
    },
    formIndex: {
        type: Number,
    },
    pageId: {
        type: String,
    },
    siteId: {
        type: String,
    },
    interact: {
        type: Number,
        default: 1,
    },
    conversion: {
        type: Number,
        default: 0,
    },
    pageName: {
        type: String,
        default: '',
    },
    countryName: {
        type: String,
        default: '',
    },
    traficSource: {
        type: String,
        default: 'noReferrer',
    },
    osName: {
        type: String,
        default: '',
    },
    browserName: {
        type: String,
        default: '',
    },
    resolution: {
        type: String,
        default: '',
    },
    device: {
        type: String,
        default: '',
    },
    path: {
        type: String
    },
    lastFoucus: {
        type: Number
    },
    formSubmit: {
        type: String,
        default: 'false'
    },
    clickError: {
        type: String,
        default: 'false'
    },
    formInteract: {
        type: String,
        default: 'false'
    },
    clickRage: {
        type: String,
        default: 'false'
    },
    recordingId: {
        type: String,
        default: ''
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

});

/**
 * @typedef InteractionsSchema
 */

module.exports = mongoose.model('Interaction', InteractionsSchema);