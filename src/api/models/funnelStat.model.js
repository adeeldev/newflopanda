const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * User Schema
 * @private
 */
const FunnelStatSchema = new mongoose.Schema({
  recordingId: {
    type: Schema.Types.ObjectId,
  },
  funnelId: {
    type: Schema.Types.ObjectId,
  },
  funnelName: {
    type: String,
  },
  pages: [],
  alias: [],
  siteId: {
    type: String,
  },
  pages: [],
  views: {
    type: Number,
    default: 1,
  },
  conversion: {
    type: Number,
    default: 0
  },
  visit: {
    type: Number,
    default: 0
  },
  visitObj: {
    type: Object
  },
  firstPageInfo: {
    type: Number
  },
  secondPage: {
    type: Number
  },
  dropped: [],
  converted: [],
  Date: {
    type: String,
    default: ''
  },
}, {
  timestamps: true,
});

/**
 * @typedef FunnelStat
 */
module.exports = mongoose.model('FunnelStat', FunnelStatSchema);
