var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
/**
 * User Schema
 * @private
 */
const PaymentSchema = new mongoose.Schema({

    userId: { type: Schema.Types.ObjectId,ref: 'User'},
    paymentId: String,
    packageDetailId: String,
    packageId: { type: Schema.Types.ObjectId, ref: 'Package' },
    planeName: { type: String, default: '' },
    duration: { type: String, default: '1' },
    price: { type: Number, default: 0 },
    paymentSuccess: { type: Boolean, default: false },
    recording: { type: Number, default: 0 },
    cardName: { type: String, default: false },
    country: { type: String, default: '' },
    currency: { type: String, default: '' },
    zipCode: { type: String, default: false },
    cardNumber: { type: String, default: false },
    paymentType: { type: String, default: '' },
    paymentMethod: { type: String, default: '' },
    createdAt: { type: Date, default: Date.now },
    autoRenew: { type: Boolean, default: false },
    updatedAt: { type: Date, default: Date.now }

});

PaymentSchema.statics = {


    async getPaymentByPaymentId(options) {
        const { paymentId } = options;
        if (!paymentId) throw new APIError({ message: 'PaymentId is missing' });
        var payment = await this.findOne({ paymentId }).exec();
        const err = {
            status: httpStatus.UNAUTHORIZED,
            isPublic: true,
            message: ''
        };
        return payment;
    },
    async getPaymentByUserId_Success(options) {
        const { userId } = options;
        if (!userId) throw new APIError({ message: 'userId is missing' });
        var payment = await this.find({ userId: userId, paymentSuccess: true }).sort({_id :-1}).exec();
        const err = {
            status: httpStatus.UNAUTHORIZED,
            isPublic: true,
            message: ''
        };
        return payment;
    },
    async getPaymentSuccess() {
        var payment = await this.find({ paymentSuccess: true }).populate('packageId', 'userId planeName price duration createdAt osName browserName country devise referer updatedAt createdAt').exec();
        const err = {
            status: httpStatus.UNAUTHORIZED,
            isPublic: true,
            message: ''
        };
        return payment;
    }

};

/**
 * @typedef Payment
 */
module.exports = mongoose.model('Payment', PaymentSchema);