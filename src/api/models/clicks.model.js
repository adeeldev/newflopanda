const mongoose = require('mongoose');

/**
 * User Schema
 * @private
 */
const ClickSchema = new mongoose.Schema({
  recordingId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  pageId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  type: {
    type: String,
  },
  x: {
    type: Number,
  },
  y: {
    type: Number,
  },
  path: {
    type: String,
  },
  time: {
    type: Number,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  timestamps: true,
});

/**
 * @typedef Click
 */
module.exports = mongoose.model('Click', ClickSchema);
