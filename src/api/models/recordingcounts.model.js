const mongoose = require('mongoose');

// const Schema = mongoose.Schema;

/**
 * User Schema
 * @private
 */
const RecordingcountsSchema = new mongoose.Schema({
  siteId: {
    type: String,
    default:'',
  },
  sessionRequests: {
    type: Number,
    default: ''
  },
  savedSessions: {
    type: Number,
    default: '',
  }
}, {
  timestamps: true,
});

/**
 * @typedef Recordingcounts
 */
module.exports = mongoose.model('Recordingcounts', RecordingcountsSchema);
