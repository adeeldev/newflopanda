var mongoose = require('mongoose');

const CountrySchema = new mongoose.Schema({
label: String,
name: String,
code: String,
routeId: Number,
createdAt: { type: Date, default: Date.now },
updatedAt: { type: Date, default: Date.now },

});

module.exports = mongoose.model('Country', CountrySchema);