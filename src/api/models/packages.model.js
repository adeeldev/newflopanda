var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');
/**
 * User Schema
 * @private
 */
const PackageSchema = new mongoose.Schema({
    planeName: { type: String, default: '' },
    email: { type: String, default: '' },
    planeType: { type: String, default: '' },
    viewPerMonth: { type: Number, default: 0 },
    recordingPerMonth: { type: Number, default: 0 },
    websites: { type: Number, default: 0 },
    storage: { type: Number, default: 0 },
    recordings: { type: Boolean, default: false },
    heatMaps: { type: Boolean, default: false },
    funnels: { type: Boolean, default: false },
    ips: { type: Boolean, default: false },
    issueReports: { type: Boolean, default: false },
    analytics: { type: Boolean, default: false },
    dropOffs: { type: Boolean, default: false },
    partialData: { type: Boolean, default: false },
    recordingDownloads: { type: Boolean, default: false },
    heatmapDownloads: { type: Boolean, default: false },
    support: { type: Boolean, default: false },
    usPrice: { type: Number, default: 0 },
    gbPrice: { type: Number, default: 0 },
    isCustom: { type: Boolean, default: false },
    discount: { type: Number, default: 0 },
    conversionAlerts: { type: Boolean, default: false },
    utmTags: { type: Boolean, default: false },
    websiteVisitorSearch: { type: Boolean, default: false },
    shareRecording: { type: Boolean, default: false },
    shareHeatmap: { type: Boolean, default: false },
    fbCompaignBreakdown: { type: Boolean, default: false },
    tags: { type: Boolean, default: false },
    deviceReporting: { type: Boolean, default: false },
    packageType: { type: Number, default: 1 },
    pageBreakdownReport: { type: Boolean, default: false },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    isCustomAllowed: { type: Boolean, default: false },
    userId: { type: Schema.Types.ObjectId, ref: 'User' },
});


PackageSchema.statics = {
    async getallPackages(type, filterQuery, userId) {
        var packages = '';
        let where = {};
        let data = {};
        where.packageType = { $ne: 2 };
        if (filterQuery.planeName !== undefined) {
            where.planeName = { $regex: new RegExp(filterQuery.planeName), $options: "i" }
        }
        if (filterQuery.email !== undefined) {
            where.email = filterQuery.email;
        }
        if (filterQuery.isCustomAllowed !== undefined) {
            where.isCustomAllowed = filterQuery.isCustomAllowed
        }
        if (filterQuery.planeType !== undefined) {
            where.planeType = filterQuery.planeType;
        }
        where.isCustom = type;
        if (type === true || type === 'true') {
            if (filterQuery.utype === 'superUser') {
                packages = await this.find({
                    $and: [where]
                }).select('-updatedAt -__v').sort({ usPrice: 1 }).exec();
            } else if (filterQuery.utype === 'support') {
                where.userId =  userId;
                    packages = await this.find({
                        $and: [where]
                    }).select('-updatedAt -__v').sort({ usPrice: 1 }).exec();
                

            }
        } else if (type === false || type === 'false') {
                packages = await this.find({
                    $and: [where]
                }).select('-updatedAt -__v').sort({ usPrice: 1 }).exec();
                // if (packages !== undefined && packages !== "") {
                //     if(filterQuery.planeName !== undefined || filterQuery.planeType !== undefined){
                //         data.planeName = { $eq: "Diamond" }
                //     }
                //     if (filterQuery.planeName !== undefined && filterQuery.planeName ==="Diamond") {
                //         data.planeName = { $regex: new RegExp(filterQuery.planeName), $options: "i" }
                //     }
                //     if (filterQuery.planeType !== undefined) {
                //         data.planeType = { $regex: new RegExp(filterQuery.planeType), $options: "i" }
                //     }
                //     let appendPackages = await this.find({
                //         $and: [data]
                //     }).select('-updatedAt -__v').sort({ usPrice: 1 }).exec();
                //     packages = [...packages, ...appendPackages]
                // }
            }
            const err = {
                status: httpStatus.UNAUTHORIZED,
                isPublic: true,
                message: ''
            };
            return packages;
        },
        async getallRefillPackages(filterQuery) {
            let where = {};
            where.packageType = { $eq: 2 };
            if (filterQuery.planeName !== undefined) {
                where.planeName = { $regex: new RegExp(filterQuery.planeName), $options: "i" }
            }
            var packages = await this.find({ $and: [where] }).select('-updatedAt -__v').sort({ usPrice: 1 }).exec();
            const err = {
                status: httpStatus.UNAUTHORIZED,
                isPublic: true,
                message: ''
            };
            return packages;
        },
        async getallCustomPackages() {
            var packages = await this.find({ isCustom: true }).select('-updatedAt -__v').sort({ usPrice: 1 }).exec();
            const err = {
                status: httpStatus.UNAUTHORIZED,
                isPublic: true,
                message: ''
            };
            return packages;
        },
        async getPackageById(options) {
            const { packageId } = options;
            if (!packageId) throw new APIError({ message: 'packageId is missing' });
            var packages = await this.findById(packageId).exec();
            const err = {
                status: httpStatus.UNAUTHORIZED,
                isPublic: true,
                message: ''
            };
            return packages;
        }
    };

    /**
     * @typedef Package
     */
    module.exports = mongoose.model('Package', PackageSchema);