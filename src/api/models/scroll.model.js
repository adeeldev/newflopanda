const mongoose = require('mongoose');

/**
 * User Schema
 * @private
 */
const ScrollSchema = new mongoose.Schema({
  recordingId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  pageId: {
    type: mongoose.Schema.Types.ObjectId,
  },
  y: {
    type: Number,
  },
  percentage: {
    type: Number,
  },
  type: {
    type: String,
  },
  height: {
    type: Number,
  },
  from: {
    type: Number,
  },
  to: {
    type: Number,
  },
  colorCode: {
    type: String,
  },
  value: {
    type: Number,
  },
  time: {
    type: Number,
  },
  date: {
    type: Date,
    default: Date.now,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  timestamps: true,
});

/**
 * @typedef Scroll
 */
module.exports = mongoose.model('Scroll', ScrollSchema);
