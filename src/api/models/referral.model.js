var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReferralSchema = new mongoose.Schema({
    email: {
        type: String,
        default: ''
    },
    converted: {
        type: Boolean,
        default: false
    },
    accountType: {
        type: String,
        default: ''
    },
    packageId: {
        type: String,
        default: ''
    },
    packageName: {
        type: String,
        default: ''
    },
    userId: {
        type: Schema.Types.ObjectId
    },
    referralUserId: {
        type: Schema.Types.ObjectId
    },
    billingDate:{
        type: Date,
        default: Date.now,
      },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },

});


module.exports = mongoose.model('Referral', ReferralSchema);