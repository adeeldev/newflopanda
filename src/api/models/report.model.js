const mongoose = require('mongoose');

/**
 * User Schema
 * @private
 */
const ReportSchema = new mongoose.Schema({
  userId: {
    type: String,
    default: ''
  },
  siteId: {
    type: String,
    default: ''
  },
  name: {
    type: String,
    default: ''
  },
  reportCode: {
    type: String,
  },
  siteName: {
    type: String,
    default: ''
  },
  date: {
    type: Date,
    default: Date.now,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  timestamps: true,
});

/**
 * @typedef Report
 */
module.exports = mongoose.model('Report', ReportSchema);
