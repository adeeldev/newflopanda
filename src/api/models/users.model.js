const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const httpStatus = require('http-status');
const { omitBy, isNil } = require('lodash');
const bcrypt = require('bcryptjs');
const moment = require('moment-timezone');
const jwt = require('jwt-simple');
// const uuidv4 = require('uuid/v4');
const APIError = require('../utils/APIError');
const { env, jwtSecret, jwtExpirationInterval } = require('../../config/vars');

/**
* User Roles
*/
const roles = ['user', 'admin'];

/**
 * User Schema
 * @private
 */
const usersSchema = new mongoose.Schema({
  email: {
    type: String,
    match: /^\S+@\S+\.\S+$/,
    required: true,
    // unique: true,
    trim: true,
    lowercase: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
    maxlength: 128,
  },
  domain: [{ domainName: String, siteName: String, trackingId: String }],
  resetPasswordToken: {
    type: String,
    default: '',
  },
  accessToken: {
    type: String,
    default: '',
  },
  profilePicPath: {
    type: String,
    default: '',
    trim: true,
  },
  profilePicType: {
    type: String,
    default: '',
  },
  verified: {
    type: Boolean,
    default: false,
  },
  active: {
    type: Boolean,
    default: 1,
  },
  profilePicName: {
    type: String,
    default: '',
  },
  base32Secret: {
    type: String,
    default: '',
  },
  language: {
    type: String,
    default: '',
  },
  timeZone: {
    type: Number,
    default: 0,
  },
  demoWebsite: {
    type: Boolean,
    default: true,
  },
  twoFactorAuthentication: {
    type: Boolean,
    default: false,
  },
  emailSubscriptions: {
    type: Boolean,
    default: false,
  },
  myName: {
    type: String,
    default: '',
  },

  companyName: {
    type: String,
    default: '',
  },
  vatNo: {
    type: String,
    default: '',
  },
  address: {
    type: String,
    default: '',
  },
  city: {
    type: String,
    default: '',
  },
  postalCode: {
    type: String,
    default: '',
  },
  country: {
    type: String,
    default: '',
  },
  phone: {
    type: String,
    default: '',
  },
  billingEmail: {
    type: String,
    default: '',
  },
  tokenSentAt: {
    type: Date,
    default: Date.now,
  },
  roles: {
    type: String,
    default: 1,
  },
  packageId: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Package',
      default: [],
    },
  ],
  website: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Website',
      default: [],
    },
  ],
  admins: [
    {
      type: Schema.Types.ObjectId,
      ref: 'Users',
      default: [],
    },
  ],
  ownerId: {
    type: String,
    default: ''
  },
  resetPasswordExpires: {
    type: Date,
    default: ''
  },
  autoRenew: {
    type: Boolean,
    default: false
  },
  totalPayment: {
    type: Number,
    default: 0
  },
  packageUpdateDate: {
    type: Date,
    default: Date.now,
  },
  userType: {
    type: String,
    default: 'user'
  },
  isFromBeta: {
    type: Boolean,
    default: false
  },
  referralId: {
    type: String,
    default: ''
  },
  accountNumber: {
    type: String,
    default: ''
  },
  accountName: {
    type: String,
    default: ''
  },
  accountEmail: {
    type: String,
    default: ''
  },
  sortCode: {
    type: String,
    default: ''
  },
  referralPaidAmount: {
    type: Number,
    default: 0
  },
  billingDate: {
    type: Date,
    default: Date.now,
  },
  isPaid: {
    type: Boolean,
    default: false
  },
  customerId: {
    type: String,
    default: ''
  },
  refillCredit: {
    type: Number,
    default: 0
  },
  paidAmount: [{
    amount: Number,
    date: Date,
    transactionId: String,
    batchId: String,
    currency: String,
  }],
  usPriceCP: {
    type: Number,
    default: 0
  },
  gbPriceCP: {
    type: Number,
    default: 0
  },
  expired: {
    type: Boolean,
    default: false
  },
  isUserSiteTraffic: {
    type: Boolean,
    default: false
  },
  isDeleted: {
    type: Boolean,
    default: false
  },
  isCreateSite: {
    type: Boolean,
    default: false
  },
  loginCount: {
    type: Number,
    default: 0
  },
  recordingLeft: {
    type: Number,
    default: 0
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
}, {
  timestamps: true,
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
usersSchema.pre('save', async function save(next) {
  try {
    if (!this.isModified('password')) return next();

    const rounds = env === 'test' ? 1 : 10;

    const hash = await bcrypt.hash(this.password, rounds);
    this.password = hash;

    return next();
  } catch (error) {
    return next(error);
  }
});

/**
 * Methods
 */
usersSchema.method({
  transform() {
    const transformed = {};
    const fields = ['id', 'name', 'email', 'picture', 'role', 'createdAt', 'twoFactorAuthentication', 'roles', 'website', 'packageId', 'admins', 'ownerId', 'verified', 'timeZone', 'resetPasswordToken', 'customerId', 'loginCount'];

    fields.forEach((field) => {
      transformed[field] = this[field];
    });

    return transformed;
  },

  token() {
    const playload = {
      exp: moment().add(jwtExpirationInterval, 'minutes').unix(),
      iat: moment().unix(),
      sub: this._id,
    };
    return jwt.encode(playload, jwtSecret);
  },

  async passwordMatches(password) {
    return bcrypt.compare(password, this.password);
  },
});

/**
 * Statics
 */
usersSchema.statics = {

  roles,

  /**
   * Get user
   *
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  async get(id) {
    try {
      let user;

      if (mongoose.Types.ObjectId.isValid(id)) {
        user = await this.findById(id).exec();
      }
      if (user) {
        return user;
      }

      throw new APIError({
        message: 'User does not exist',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
  /**
   * Find user by email and tries to generate a JWT token
   *
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  async findAndGenerateToken(options) {
    const { email, password, refreshObject, isAdmin } = options;
    if (!email) throw new APIError({ message: 'An email is required to generate a token' });
    let roles = isAdmin ? ["1", "2", "3"] : ["4", "5"]
    const user = await this.findOne({ $and: [{ email: email, isDeleted: false }] }).exec();
    const err = {
      status: httpStatus.UNAUTHORIZED,
      isPublic: true,
    };
    if (user === null) {
      err.message = 'Incorrect email or password';
    } else if (roles.indexOf(user.roles) !== -1) {
      err.message = 'You are not allowed to Login.';
    } else if (!user.active) {
      err.message = 'Your account has been Deactivated. Please contact Customer Support.';
    } else if (password) {
      if (user && await user.passwordMatches(password)) {
        return { user, accessToken: user.token() };
      }
      err.message = 'Incorrect email or password';
    } else if (refreshObject && refreshObject.userEmail === email) {
      if (moment(refreshObject.expires).isBefore()) {
        err.message = 'Invalid refresh token.';
      } else {
        return { user, accessToken: user.token() };
      }
    } else {
      err.message = 'Incorrect email or refreshToken';
    }
    console.log(err);
    throw new APIError(err);
  },
  async findAndGenerateTokenForBeta(options) {
    const { email } = options;
    if (!email) throw new APIError({ message: 'An email is required to generate a token' });
    const user = await this.findOne({ $and: [{ email: email, isDeleted: false }] }).exec();
    const err = {
      status: httpStatus.UNAUTHORIZED,
      isPublic: true,
    };
    if (user === null) {
      err.message = 'No user exist with this Email.';
    } else if (user) {
      if (user.isFromBeta === true) {
        return { user, accessToken: user.token() };
      }
      err.message = 'Your are not redirected from the beta flopanda';
    } else {
      err.message = 'Incorrect email ';
    }
    console.log(err);
    throw new APIError(err);
  },
  async generateTokenByEmail(options) {
    const { email } = options;
    if (!email) throw new APIError({ message: 'An email is required to generate a token' });
    const user = await this.findOne({ $and: [{ email: email, isDeleted: false }] }).exec();
    const err = {
      status: httpStatus.UNAUTHORIZED,
      isPublic: true,
    };
    if (user === null) {
      err.message = 'Incorrect email';
    } else if (user) {
      return { user, accessToken: user.token() };
    } else {
      err.message = 'Incorrect email ';
    }
    console.log(err);
    throw new APIError(err);
  },
  async checkUserExistance(options, requestBy) {
    const { email, userId } = options;
    if (requestBy === "forgetPassword") {
      if (!email) throw new APIError({ message: 'An email is required to reset your password' });
      var user = await this.findOne({ email, isDeleted: false }).exec();
    } else if (requestBy === "validateToken" || "resetPassword") {
      if (!userId) throw new APIError({ message: 'userid is required to validate token' });
      var user = await this.findById(userId).exec();
    }

    const err = {
      status: httpStatus.UNAUTHORIZED,
      isPublic: true,
      message: ''
    };
    if (!user) {
      if (requestBy === "forgetPassword") {
        err.message = 'Invalid email';
      } else if (requestBy === "validateToken" || "resetPassword") {
        err.message = 'Invalid userId';
      }
    } else {
      return { user, user };
    }
    throw new APIError(err);
  },
  async matchPassword(userId, password) {
    const err = {
      status: httpStatus.UNAUTHORIZED,
      isPublic: true,
    };
    if (userId) {
      const user = await this.findById(userId).exec();
      console.log(user)
      if (user && await user.passwordMatches(password)) {
        return true;
      } else {
        err.message = 'Incorrect password';
      }
    } else {
      err.message = 'UserID is required' + userId;
    }

    throw new APIError(err);
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   *
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({
    page = 1, perPage = 30, name, email, role,
  }) {
    const options = omitBy({ name, email, role }, isNil);

    return this.find(options)
      .sort({ createdAt: -1 })
      .skip(perPage * (page - 1))
      .limit(perPage)
      .exec();
  },

  /**
   * Return new validation error
   * if error is a mongoose duplicate key error
   *
   * @param {Error} error
   * @returns {Error|APIError}
   */
  checkDuplicateEmail(error) {
    if (error.name === 'MongoError' && error.code === 11000) {
      return new APIError({
        message: 'Validation Error',
        errors: [{
          field: 'email',
          location: 'body',
          messages: ['email already exists'],
        }],
        status: httpStatus.CONFLICT,
        isPublic: true,
        stack: error.stack,
      });
    }
    return error;
  },
  async findIdAndPassword(userId) {
    var user = await this.findOne({ _id: userId, isDeleted: false }).exec();
    if (user.password) {
      const result = await user.passwordMatches(password);
    }
  },
};


/**
 * @typedef Users
 */
module.exports = mongoose.model('Users', usersSchema);
