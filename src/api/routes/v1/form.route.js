const express = require('express');
const controller = require('../../controllers/form.controller');
const Auth = require('../../services/authProviders')
// var RateLimit = require('express-rate-limit');
// var MongoStore = require('rate-limit-mongo');
// var limiter = new RateLimit({
//   store: new MongoStore({
//     uri: 'mongodb://127.0.0.1:27017/mongostore',
//     collectionName: 'rate',
//     expireTimeMs: 2 * 60 * 1000
//   }),
//   max: 100,
//   windowMs: 2 *  60 * 1000
// });

const router = express.Router();
// router.use(limiter);
router
 .route('/site/:siteId/:from/:to')
 .get(controller.siteTrackData);

 router
 .route('/pages/:siteId/:pageId/:offset/:limit')
 .get(controller.formsByPageId);

 router
  .route('/detail/summary')
  .post(controller.getFormFieldDetail);


router.use((req, res, next)=>Auth.authenticateUser(req, res, next))

router
  .route('/track/:siteId/:from/:to')
  .get(controller.formTrackData);

router
  .route('/fields/remove')
  .delete(controller.deleteFormFields);

router
  .route('/site/:siteId/:from/:to')
  .get(controller.siteTrackData);

router
  .route('/:formId/track/fields/:siteId/:from/:to')
  .get(controller.fieldDetials);


router
  .route('/record/interactions')
  .post(controller.recordInteractions);

router
  .route('/index')
  .post(controller.formIndex);

router
  .route('/fieldTracking')
  .post(controller.fieldTracking);

router
  .route('/save')
  .post(controller.createUserForm);

router
  .route('/edit')
  .post(controller.editForm);

router
  .route('/')
  .delete(controller.delete);

router
  .route('/fields')
  .post(controller.editFormData);

router
  .route('/report/:formId')
  .get(controller.report);

router
  .route('/allForms/:siteId')
  .get(controller.get);

router
  .route('/filterForm')
  .put(controller.filterForm);

router
  .route('/filterFormReport')
  .put(controller.filterFormReport);

router
  .route('/filterFormDate')
  .put(controller.filterFormDate);
// router
// .route('/test')
// .post(controller.test);

module.exports = router;
