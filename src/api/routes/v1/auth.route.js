const express = require('express');
const validate = require('express-validation');
const controller = require('../../controllers/auth.controller');
const oAuthLogin = require('../../middlewares/auth').oAuth;
const {
  login,
  register,
  oAuth,
  refresh,
  forgetPassword,
  validateToken,
  registerAdmin,
  resetPassword,
} = require('../../validations/auth.validation');
const consolidate = require('consolidate');
const authProviders = require('../../services/emailProvider');
const Auth = require('../../services/authProviders');
const path = require('path');
const crypto = require('crypto');
const Users = require('../../models/users.model');
const { DEV_DOMAIN, MONGO_STORE_URL } = require('../../../config/vars');
const moment = require('moment');
var RateLimit = require('express-rate-limit');
var MongoStore = require('rate-limit-mongo');
var limiterLogin = new RateLimit({
  store: new MongoStore({
    uri: MONGO_STORE_URL,
    collectionName: 'rate',
    expireTimeMs: 30 * 60 * 1000,
    resetExpireDateOnChange: true
  }),
  max: 5,
  windowMs: 10000,
  skipSuccessfulRequests: true,
  // keyGenerator: function (req) {
  //   return req.headers['x-forwarded-for'].split(",")[0];
  // },
  message: {
    code: 429,
    message: "Too many failed login attempts. Please try again in 30 minutes."
  },
  onLimitReached: async function (req, res) {
    var token1 = await crypto.randomBytes(48).toString('hex');
    await Users.findOneAndUpdate({ email: req.body.email }, { resetPasswordExpires: Date.now() + 60000 * 60, resetPasswordToken: token1 });
    const link = `${DEV_DOMAIN}/resetPassword/${token1}`;
    const templateFilePath = path.join(__dirname, '../../uploadedFiles/') + 'failedLogin.html';
    consolidate.swig(templateFilePath, {
      date: moment().format('MMMM Do YYYY, h:mm:ss a'),
      link: link,
    }, (err, html) => {
      if (err) {
        console.log(err);
      } else {
        authProviders.send_email(req.body.email, 'Flopanda Failed Login Alert', html);
      }
    });
  }
});

const router = express.Router();


router.route('/register')
  .post(validate(register), controller.register);

router.route('/checkExpirePackage')
  .post(controller.checkExpirePackage);


router.route('/admin/register')
  .post(validate(registerAdmin), controller.registerAdmin);

router.route('/login')
  .post(limiterLogin, controller.login);

router.route('/admin/userlogin').post(
  (req, res, next) => Auth.authenticateUser(req, res, next), controller.userLoginForAdmin
)

router.route('/betaLogin')
  .post(controller.betalogin);

router.route('/logout')
  .post(controller.logout);

router.route('/verifiedUser/:id')
  .get(controller.verifyUser);

router.route('/invite')
  .post(controller.generateUserByEmail);

router.route('/refresh-token')
  .post(validate(refresh), controller.refresh);

router.route('/update/:userId').put(controller.update);

router.route('/:userId').delete(controller.delete);

router.route('/facebook')
  .post(validate(oAuth), oAuthLogin('facebook'), controller.oAuth);

router.route('/google')
  .post(validate(oAuth), oAuthLogin('google'), controller.oAuth);

router.route('/forgetPassword')
  .post(validate(forgetPassword), controller.forgetPassword);

router.route('/validateToken')
  .post(validate(validateToken), controller.validateToken);

router.route('/resetPassword')
  .post(validate(resetPassword), controller.resetPassword);

router.route('/update2FA')
  .post(controller.update2FA);

router.route('/validate2FA')
  .post(controller.validate2FA);


router.route('/update/email')
  .post(controller.updateEmail);

router.route('/update/password')
  .post(controller.updatePassword);

module.exports = router;

