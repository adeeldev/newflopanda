const express = require('express');
const userRoutes = require('./user.route');
const authRoutes = require('./auth.route');
const trackingRoutes = require('./tracking.route');
const websiteRoutes = require('./website.route');
const sessionRoutes = require('./session.route');
const recordingRoutes = require('./recording.route');
const funnelRoutes = require('./funnel.route');
const formRoutes = require('./form.route');
const packageRoutes = require('./package.route');
const paymentRoutes = require('./payment.route');
const contactUsRoutes = require('./contactUs.route');
const faqsRoutes = require('./faqs.route');
const cmsRoutes = require('./cms.route');
const pageRoute = require('./page.route');
const cronRoute = require('./cron.route');
const referralRoute = require('./referral.route');

const router = express.Router();

/**
 * GET v1/status
 */
router.get('/status', (req, res) => res.send('OK'));

/**
 * GET v1/docs
 */
router.use('/docs', express.static('docs'));

router.use('/users', userRoutes);
router.use('/auth', authRoutes);
router.use('/tracking', trackingRoutes);
router.use('/website', websiteRoutes);
router.use('/session', sessionRoutes);
router.use('/recording', recordingRoutes);
router.use('/funnel', funnelRoutes);
router.use('/form', formRoutes);
router.use('/package', packageRoutes);
router.use('/payment', paymentRoutes);
router.use('/contactUs', contactUsRoutes);
router.use('/faqs', faqsRoutes);
router.use('/cms', cmsRoutes); 
router.use('/page', pageRoute);
router.use('/cron', cronRoute);
router.use('/referral',referralRoute)

module.exports = router;
