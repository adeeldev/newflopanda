const express = require('express');
const controller = require('../../controllers/cms.controller');
const blog = require('../../controllers/blog.controller');
const survey = require('../../controllers/survey.controller');
const Auth = require('../../services/authProviders')
var aws = require('aws-sdk');
var multer = require('multer');
var multerS3 = require('multer-s3');
var s3 = new aws.S3({ accessKeyId: 'AKIAI6FN6GALLBUTYK6Q', secretAccessKey: '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' });

var upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: 'flopandascripts/assets/images',
    acl: 'public-read',
    ContentType: 'image/png',
    metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
      cb(null, file.originalname)
    }
  })
});

const router = express.Router();


// router.use((req, res, next)=>Auth.validateAcl(req, res, next));
router.use((req, res, next)=>Auth.authenticateUser(req, res, next));

router.route('/add/role').post(controller.addRole);
router.route('/blog/post/:type').get(blog.recentPosts);
router.route('/blog/all').get(blog.getAll);


router
  .route('/')
  .post(controller.post)
  .delete(controller.delete)
  .put(controller.put)
  .get(controller.get);

router
  .route('/blog')
  .put(upload.single('file'), blog.edit)
  .post(upload.single('file'), blog.create)
  .delete(blog.delete);


router
  .route('/blog/:limit/:offset')
  .get(blog.get);

router
  .route('/survey')
  .get(survey.get)
  .put(survey.edit)
  .post(survey.create)
  .delete(survey.delete);

router
  .route('/category')
  .get(blog.getCategories)
  .post(blog.createCategory);


router
  .route('/blog/:categoryId')
  .get(blog.blogByCategoryId);


router.route('/userExpired').put(controller.userExpiry);
router
  .route('/category/all')
  .get(blog.getAllCategory);


router.route('/comment/blog').post(blog.saveComment);
router.route('/comment/blog/:blogId').get(blog.getBlogComments);

router.route('/feedback/all/:offset/:limit').get(survey.getAll);
router.route('/conversion/prices').get(controller.getAllPricePackages);
router.route('/conversion/price').post(controller.savePackagePrices);
router.route('/conversion/price').delete(controller.deletePackagePrices);
router.route('/conversion/price').put(controller.editPricePackages);
router.route('/conversion/price/deleteMany').delete(controller.deleteMany);
//user
router.route('/users/emails/:offset/:limit').get(controller.getCurrentUsersEmails);
router.route('/all/admin/:support/:offset/:limit').get(controller.listAllUsers);
router.route('/userViewDetail/:userId').get(controller.getUserViewDetail)
router.route('/userStatus').put(controller.userStatus)
router.route('/deleteSupportUser').delete(controller.deleteUser)
router.route('/getOverView/:from/:to').get(controller.adminOverViewData);
router.route('/siteDetail/:userId').get(controller.siteDetail)
router.route('/combine/users/emails/:logType/:offset/:limit').get(controller.getCombineUserEmails)

//country
router.route('/country').post(controller.create);
// router.route('/country/:from/:to').get(controller.getCountry)
router.route('/country').get(controller.getCountry);
router.route('/country/:id').get(controller.editCountry);
router.route('/country').put(controller.update);
router.route('/country').delete(controller.deleteCountry);
router.route('/country/deleteMany').delete(controller.deleteManyCountry);
//payment
router.route('/paymentHistory/:offset/:limit').get(controller.paymentHistory);
router.route('/deleteUserHistory/:offset/:limit/:logType').get(controller.deleteUserHistory);
//site
router.route('/site/search/:offset/:limit').get(controller.sites);
router.route('/site/detail/:siteId').get(controller.getSiteDetail);
router.route('/site/:offset/:limit').get(controller.getSite);
module.exports = router;
