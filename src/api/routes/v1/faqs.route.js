const express = require('express');
const Auth = require('../../services/authProviders')
const controller = require('../../controllers/faqs.controller');

const router = express.Router();
router.use((req, res, next)=>Auth.authenticateUser(req, res, next))
router
    .route('/')
    .post(controller.post)
    .delete(controller.delete)
    .put(controller.put)
    .get(controller.get);

module.exports = router;
