const express = require('express');
const session = require('../../controllers/session.controller');
const { ACCESS_KEY_ID, SECRET_ACCESS_KEY, S3_BUCKET, IP_API_KEY } = require('../../../config/vars');
var aws = require('aws-sdk')
var multer = require('multer')
var multerS3 = require('multer-s3');
const Auth = require('../../services/authProviders');
// var RateLimit = require('express-rate-limit');
// var MongoStore = require('rate-limit-mongo');
// var limiter = new RateLimit({
//     store: new MongoStore({
//       uri: 'mongodb://127.0.0.1:27017/mongostore',
//       collectionName: 'rate',
//       expireTimeMs:  60 * 1000
//     }),
//     max: 100,
//     windowMs:   60 * 1000
//   });
var s3 = new aws.S3({ accessKeyId: ACCESS_KEY_ID, secretAccessKey: SECRET_ACCESS_KEY });

var upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: S3_BUCKET,
    acl: 'public-read',
    metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
    	},
    key: function (req, file, cb) {
      cb(null, file.originalname)
    }
  })
});

const router = express.Router();
// router.use(limiter);

router.route('/upload/file').post(upload.single('file'), session.uploadFile);
router.route('/:sessionId/recording').get(session.sessionRecording);
router.route('/update/payment/tag').post(session.updatePaymentTag);

router.use((req, res, next)=>Auth.authenticateUser(req, res, next))


router.route('/ips/:siteId/:from/:to').get(session.recordingIps);
router.route('/utm/:siteId/:offset/:limit/:from/:to').get(session.utmTags);
router.route('/utm/csv/:siteId/:from/:to').get(session.utmCsvReport);
//router.route('/:sessionId/recording').get(session.sessionRecording);
// router.route('/geo/:siteId').get(session.geo);
router.route('/geo/:siteId/:from/:to').get(session.geo);
router.route('/filter/range/:siteId').get(session.durationRange);
router.route('/filter/comapaigns').post(session.compaignUrl);
router.route('/save').post(session.save);
router.route('/save/offline/report').post(session.saveOfflineReport);
router.route('/tag').post(session.saveTag);
router.route('/recording/many/tags').post(session.addTagInMulRecording);
router.route('/tag').delete(session.deleteTag);
router.route('/variable').post(session.saveVariable);
router.route('/share').post(session.share);
router.route('/').delete(session.delete);
router.route('/recording').delete(session.deleteMany);
router.route('/favourite').post(session.favourite);
router.route('/multiFav').post(session.multiFav);
router.route('/multiWatch').post(session.multiWatch);
router.route('/upload').post(session.upload);
//router.route('/upload/file').post(upload.single('file'), session.uploadFile);
//router.route('/upload/file').post(upload.single('file'), session.uploadFile);
router.route('/check/db/update').post(session.updateDb);


module.exports = router;
