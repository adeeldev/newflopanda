const express = require('express');
const tracking = require('../../controllers/tracking.controller');
const Auth = require('../../services/authProviders')
// var RateLimit = require('express-rate-limit');
// var MongoStore = require('rate-limit-mongo');
// var limiter = new RateLimit({
//   store: new MongoStore({
//     uri: 'mongodb://127.0.0.1:27017/mongostore',
//     collectionName: 'rate',
//     expireTimeMs:  2 * 60 * 1000
//   }),
//   max: 100,
//   windowMs: 2 *  60 * 1000
// });
const router = express.Router();
// router.use(limiter);
router.route('/trackData').post(tracking.pageTrackData);

router.use((req, res, next)=>Auth.authenticateUser(req, res, next));

router.route('/:siteId/:offset/:limit/:from/:to/:pageCount/:isBatch/:ip').get(tracking.get);
router.route('/csv/:siteId/:from/:to/:pageCount').get(tracking.getCsvDetails);
router.route('/save').post(tracking.addTracking);
// router.route('/page/:siteId/:from/:to').get(tracking.newPageData);
router.route('/page/:siteId/:offset/:limit/:from/:to').get(tracking.pageStats);

router.route('/trackFunnel').post(tracking.trackFunnel);
router.route('/site/:siteId/fields/:from/:to').post(tracking.getFormDropFields);


module.exports = router;
