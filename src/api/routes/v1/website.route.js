const express = require('express');
const website = require('../../controllers/website.controller');
const Auth = require('../../services/authProviders')
// var RateLimit = require('express-rate-limit');
// var MongoStore = require('rate-limit-mongo');
// var limiter = new RateLimit({
//     store: new MongoStore({
//       uri: 'mongodb://127.0.0.1:27017/mongostore',
//       collectionName: 'rate',
//       expireTimeMs:  60 * 1000
//     }),
//     max: 100,
//     windowMs:   60 * 1000
//   });
const router = express.Router();
// router.use(limiter);
router.route('/payment/script').get(website.getPaymentScript);
router.route('/embedded/script/:siteId').get((req, res, next) => Auth.authenticateUser(req, res, next), website.embeddedScript);
router.route('/getSite/:siteId/:userId').get((req, res, next) => Auth.authenticateUser(req, res, next),
    website.getSiteData);
router.route('/pages/:siteId/:start/:end').get(website.getWebsitePages);
router.route('/:userId/:from/:to').get(website.get);
router.route('/baseEncode').post(website.encodeBase64);

router.use((req, res, next) => Auth.authenticateUser(req, res, next))

router.route('/filter/:siteId').get(website.getSourceFilter);
router.route('/verifyInstallation/:siteId').get(website.verifyInstallation);
router.route('/downloadInfo/:siteId').get(website.downloadDataInfo);
router.route('/downloadInfo/:siteId').put(website.allowDownloadWebsiteIssueData);
router.route('/sendTrackingCode').post(website.sendTrackingCode);
router.route('/code/installation/alert').post(website.emailToInActiveSiteUser);
router.route('/code/email').post(website.emailTrackingCode);

router.route('/:siteName/site').get(website.findByName);

router.route('/script/:siteId').get(website.getSiteScript);
router.route('/save').post(website.save);
router.route('/deleteWebsite').delete(website.delete);
router.route('/advancedSettings').put(website.advancedSettings);
router.route('/editExcludeWhitelist').put(website.editExcludeWhitelist);
router.route('/editSitePrivacy').put(website.editSitePrivacy);
router.route('/editSiteExcludedIP').put(website.editSiteExcludedIP);
router.route('/editSiteSetting').put(website.editSiteSetting);
router.route('/:siteId').put(website.update);


module.exports = router;
