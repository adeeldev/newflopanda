const express = require('express');
const contactUs = require('../../controllers/contactUs.controller');
const Auth = require('../../services/authProviders')


const router = express.Router();

router.route('/').post(contactUs.post);

module.exports = router;