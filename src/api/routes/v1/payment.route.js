const express = require('express');
const payment = require('../../controllers/payment.controller');
const Auth = require('../../services/authProviders')
// var RateLimit = require('express-rate-limit');
// var MongoStore = require('rate-limit-mongo');
// var limiter = new RateLimit({
//     store: new MongoStore({
//       uri: 'mongodb://127.0.0.1:27017/mongostore',
//       collectionName: 'rate',
//       expireTimeMs:  60 * 1000
//     }),
//     max: 100,
//     windowMs:   60 * 1000
//   });

const router = express.Router();
router.use((req, res, next)=>Auth.authenticateUser(req, res, next))
// router.use(limiter);

router.route('/makePayment').post(payment.makePayment);
router.route('/paypalResponse').post(payment.paypalResponse);
router.route('/paypalCancel').get(payment.paypalCancel);
// router.route('/payout').get(payment.transferReferralCommission2);
// router.route('/checkpayout/:id').get(payment.checkPayoutStatus);
router.route('/paymentHistory/:userId').get(payment.paymentHistory);
// router.route('/paymentHistory').get(payment.paymentHistoryByAdmin);
router.route('/stipe/payment').post(payment.stipePayment);
router.route('/refill/stripe').post(payment.refillPayment);
router.route('/refill/paypal').post(payment.createRefill);
router.route('/refill/paypal/response').post(payment.refillPaypalResponse);
// router.route('/changePackage').post(payment.changePackage);
// router.route('/stipe/paymentAgain').post(payment.makeAutoPayment);

module.exports = router;