const express = require('express');
const package = require('../../controllers/package.controller');
const Auth = require('../../services/authProviders');
// var RateLimit = require('express-rate-limit');
// var MongoStore = require('rate-limit-mongo');
// var limiter = new RateLimit({
//     store: new MongoStore({
//       uri: 'mongodb://127.0.0.1:27017/mongostore',
//       collectionName: 'rate',
//       expireTimeMs:  2 * 60 * 1000
//     }),
//     max: 100,
//     windowMs: 2 *  60 * 1000
//   });

const router = express.Router();
// router.use(limiter);

router.route('/all').get(package.getUserPackage);

router.use((req, res, next)=>Auth.authenticateUser(req, res, next))

// router.route('/').get(package.get);
// router.route('/detail/:packageId').get(package.detail);

router.route('/getrefill').get(package.getRefill);
router.route('/:type').get(package.get);
router.route('/').post(package.save);
router.route('/').put(package.update);
router.route('/').delete(package.delete);
router.route('/deleteMany').delete(package.deleteMany);

router.route('/refillsave').post(package.saveRefill);
// router.route('/custom/package').get(package.getCustomPackage);
router.route('/getrefill/:packageId').get(package.detail);
router.route('/unSubscribe').post(package.unSubscribe);
// router.route('/:email/:type').get(package.get);

module.exports = router;
