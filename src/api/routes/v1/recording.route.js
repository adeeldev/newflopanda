const express = require('express');
const recording = require('../../controllers/recording.controller');
const website = require('../../controllers/website.controller');
const Auth = require('../../services/authProviders')
// var RateLimit = require('express-rate-limit');
// var MongoStore = require('rate-limit-mongo');
// var limiter = new RateLimit({
//     store: new MongoStore({
//       uri: 'mongodb://127.0.0.1:27017/mongostore',
//       collectionName: 'rate',
//       expireTimeMs:  60 * 1000
//     }),
//     max: 100,
//     windowMs:   60 * 1000
//   });
const router = express.Router();
// router.use(limiter);
router.route('/download/:sessionId').get(recording.zipFiles);
router.route('/pages/:siteId').get(recording.pageInfo);
router.route('/:recordingId/session/:sessionId').get(recording.get);


router.use((req, res, next)=>Auth.authenticateUser(req, res, next));

router.route('/:recordingId').get(recording.playOflineRecording);
router.route('/:siteId/stats/:from/:to').get(recording.recordingStats);
//router.route('/pages/:siteId').get(recording.pageInfo);
router.route('/resolution/:siteId').get(recording.resolutions);
// router.route('/stats/:siteId').get(recording.recordingStats);
router.route('/all/:siteId/:from/:to').get(recording.siteAllRecordings);
router.route('/save').post(website.save);

module.exports = router;
