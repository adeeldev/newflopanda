const express = require('express');
const referral = require('../../controllers/referral.controller');
const Auth = require('../../services/authProviders')
// var RateLimit = require('express-rate-limit');
// var MongoStore = require('rate-limit-mongo');
// var limiter = new RateLimit({
//     store: new MongoStore({
//       uri: 'mongodb://127.0.0.1:27017/mongostore',
//       collectionName: 'rate',
//       expireTimeMs:  60 * 1000
//     }),
//     max: 100,
//     windowMs:   60 * 1000
//   });

const router = express.Router();
router.route('/pageView')
    .post(referral.createPageView);
// router.use(limiter);



router.use((req, res, next) => Auth.authenticateUser(req, res, next))
// router.route('/').get(package.get);
// router.route('/detail/:packageId').get(package.detail);

router.route('/:userId/:offset/:limit/:from/:to')
    .get(referral.convertedReferralUser);

router.route('/states/:userId/:from/:to')
    .get(referral.referralStats);

router.route('/graph/:userId/:from/:to').get(referral.referralGraph);

module.exports = router;