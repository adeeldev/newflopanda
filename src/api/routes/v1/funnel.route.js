const express = require('express');
const controller = require('../../controllers/funnel.controller');
const Auth = require('../../services/authProviders');
// var RateLimit = require('express-rate-limit');
// var MongoStore = require('rate-limit-mongo');
// var limiter = new RateLimit({
//     store: new MongoStore({
//       uri: 'mongodb://127.0.0.1:27017/mongostore',
//       collectionName: 'rate',
//       expireTimeMs: 2 * 60 * 1000
//     }),
//     max: 100,
//     windowMs:  2 * 60 * 1000
//   });

const router = express.Router();
// router.use(limiter);

router.use((req, res, next)=>Auth.authenticateUser(req, res, next))
router
    .route('/')
    .post(controller.create)
    .delete(controller.delete)
    .put(controller.edit);

router.route('/:funnelId').get(controller.get);

router.route('/funnelData/:siteId/:from/:to').get(controller.getFunnelData);

// router.route('/funnelData/test/:siteId/:from/:to').get(controller.getCronFunnelDataTest);

router.route('/detail/:funnelId').get(controller.detail)

router.route('/report/:funnelId/:from/:to').get(controller.report)

router.route('/imagePrview/:id').get(controller.imagePrview);

router.route('/:siteId/tracking/:from/:to/:pageCount').get(controller.getFunnelDataCron);

router.route('/:funnelId/site/:siteId/tracking/:from/:to/:pageCount').get(controller.getFunnelById);

module.exports = router;