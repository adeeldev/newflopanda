const express = require('express');
const validate = require('express-validation');
const controller = require('../../controllers/user.controller');
const { authorize, ADMIN, LOGGED_USER } = require('../../middlewares/auth');
const Auth = require('../../services/authProviders')
const {
  listUsers,
  createUser,
  replaceUser,
  updateUser,
} = require('../../validations/user.validation');
// var RateLimit = require('express-rate-limit');
// var MongoStore = require('rate-limit-mongo');
// var limiter = new RateLimit({
//     store: new MongoStore({
//       uri: 'mongodb://127.0.0.1:27017/mongostore',
//       collectionName: 'rate',
//       expireTimeMs: 2 * 60 * 1000
//     }),
//     max: 100,
//     windowMs: 2 *  60 * 1000
//   });

const router = express.Router();
// router.use(limiter);
router.use((req, res, next)=>Auth.authenticateUser(req, res, next))

router
  .route('/all/admin/:offset/:limit/:from/:to').get(controller.listAllUsers);
/**
 * Load user when API with userId route parameter is hit
 */
router.param('userId', controller.load);


router
  .route('/report')
  .post(controller.deleteUserReport);

router
  .route('/')
  .get(authorize(ADMIN), validate(listUsers), controller.list)
  .post(authorize(ADMIN), validate(createUser), controller.create);

router
  .route('/admin')
  .post(controller.createAdmin)
  .put(controller.updateAdmin);


router
  .route('/profile')
  .get(authorize(), controller.loggedIn);


router
  .route('/billing/:userId')
  .get(controller.billing);

router
  .route('/:userId/plan')
  .get(controller.checkUserPlan);



router
  .route('/remove')
  .delete(controller.remove);

router
  .route('/feedback')
  .post(controller.feedback);

router
  .route('/subscription')
  .post(controller.subscription);

router.route('/account/:userId').get(controller.getAccountInfo);
router.route('/account').put(controller.updateAccountInfo);
router.route('/account/paypal/disconnect').put(controller.disconnectPaypal);
router.route('/website/stats').post(controller.getUserSitesInfo);
router.route('/package/stats').post(controller.packageAndSmsInfo);
router.route('/customPackage').get(controller.checkCustomPackage);


router
  .route('/:userId').get(controller.userById);
// router
// .route('/transection')
// .post(controller.transection);



module.exports = router;
