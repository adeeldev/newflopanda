const express = require('express');
const controller = require('../../controllers/form.controller');
const Auth = require('../../services/authProviders')

const router = express.Router();

router.route('/conversion/rate').post(controller.runCron);

router.use((req, res, next)=>Auth.authenticateUser(req, res, next))

router.route('/save')
    .post(controller.saveCronInfo);

router.route('/user/cns/:userId')
    .get(controller.getCurrentConversionPackage);
router.route('/user/siteconversions/:userId')
    .get(controller.getAllSiteConversions);
router.route('/user/setsiteconversions/:userId')
    .post(controller.setSiteConversions);

router.route('/:siteId/user/:userId')
    .get(controller.getCronInfo)
    .delete(controller.deleteCronInfo);

router.route('/sendSms')
    .post(controller.sendSmssss);

module.exports = router;
