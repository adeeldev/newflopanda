const express = require('express');
const page = require('../../controllers/page.controller');
const Auth = require('../../services/authProviders');
// var RateLimit = require('express-rate-limit');
// var MongoStore = require('rate-limit-mongo');
// var limiter = new RateLimit({
//     store: new MongoStore({
//       uri: 'mongodb://127.0.0.1:27017/mongostore',
//       collectionName: 'rate',
//       expireTimeMs:  2 * 60 * 1000
//     }),
//     max: 100,
//     windowMs:  2 * 60 * 1000
//   });

const router = express.Router();
// router.use(limiter);

router.route('/error/detail/:siteId/:pageId/:from/:to').get(page.detailPageError);
router.route('/:pageId/type/:type/:from/:to').get(page.pageDetailData);

router.use((req, res, next)=>Auth.authenticateUser(req, res, next));



router.route('/error/:siteId/:offset/:limit/:from/:to').get(page.getErrPageDetail);
router.route('/cp/:projectId/:pageName/:from/:to').get(page.getCpPageDetail);
router.route('/detail/:siteId/:offset/:limit/:from/:to').get(page.allPagesDetail);
router.route('/share').post(page.share);
// router.route('/:pageName/site/:siteId/:from/:from').get(page.viewGraphData);
router.route('/graph/data').post(page.viewGraphData);
router.route('/tags').put(page.updateTags);
router.route('/stats').post(page.getSitePageStats);
router.route('/form/:siteId/:offset/:limit/:from/:to').get(page.getFormPageDetail);




module.exports = router;
