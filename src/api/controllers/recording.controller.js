const Recording = require('../models/recording.model');
const Event = require('../models/event.model');
const Website = require('../models/website.model');
const moment = require('moment');
const Session = require('../models/session.model');
const Page = require('../models/page.model');
const Siteids = require('../models/siteid.model');
const Users = require('../models/users.model');
const fs = require('fs-extra');
const path = require('path');
const { _ } = require('underscore');
const zip = new require('node-zip')();
const mongoose = require('mongoose');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
  accessKeyId: 'AKIAI6FN6GALLBUTYK6Q',
  secretAccessKey: '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'
});
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const { MONGO_DB_URI } = require('../../config/vars');

//Check if user is sending its own website.
async function checkWebsites(siteId = 0, userId = 0, owner = 1) {
  var siteIdsArray = [];

  if (!userId) {
    return false;
  }

  if (!siteId) {
    return false;
  }
  let currentUserSites = await Siteids.find({ userId: userId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
  if (currentUserSites.length) {
    return true;
  }
  else if (owner !== 1) {
    let currentUserOwnerId = await Users.findOne({ _id: userId }, { ownerId: 1 }).select('siteIds').exec();
    if (currentUserOwnerId && currentUserOwnerId.ownerId) {
      let currentUserSitesAgain = await Siteids.find({ userId: currentUserOwnerId.ownerId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
      if (currentUserSitesAgain.length) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
}

exports.get = async (req, res, next) => {
  try {
    const recordingId = {
      recordingId: req.params.recordingId
    };
    const recording = await Recording.findOne({
      _id: req.params.recordingId
    }).exec();
    // const events = await Event.find(recordingId, '-recordingId -pageId -__v -_id -createdAt').exec();
    const website = await Website.findOne({
      _id: recording.siteId
    }).select('excludedContent name protocol').exec();
    const url = MONGO_DB_URI;
    const dbName = website._id.toString();
    const client = new MongoClient(url, { useNewUrlParser: true });
    let events = [];
    client.connect(function (err, client) {
      assert.equal(null, err);
      console.log(err);
      const db = client.db(dbName);
      const col = db.collection('events');
      col.find(recordingId).project({ recordingId: 0, sessionId: 0, createdAt: 0, pageId: 0, __v: 0, _id: 0, createdAt: 0, site: 0, siteId: 0 }).sort({ time: 1 }).maxTimeMS(3000).toArray(function (err, docs) {
        events = docs;
        // client.close();
        if (website.protocol === 'https') {
          return res.status(200).send({
            code: 200,
            data: {
              excludedContent: website.excludedContent,
              domain: website.name,
              recording: recording,
              events: events,
              protocol: website.protocol
            }
          });
        } else {
          let params = {
            Bucket: 'flopandascripts',
            Key: 'websites/' + recording.siteId + '/' + req.params.recordingId + '.html',
            ResponseContentEncoding: 'text/html'
          };
          s3.getObject(params, function (s3Err, newData) {
            if (s3Err) {
              console.log(s3Err);
            } else {
              let htmlCopy = newData.Body.toString('utf-8');
              recording.htmlCopy = htmlCopy;
              return res.json({
                code: 200,
                data: {
                  excludedContent: website.excludedContent,
                  domain: website.name,
                  recording: recording,
                  events: events,
                  protocol: website.protocol
                }
              });
            }
          });
        }
      });
    });
  } catch (error) {
    next(error);
  }
};

// exports.get = async (req, res, next) => {
//   try {
//     const recordingId = {
//       recordingId: req.params.recordingId
//     };
//     const recording = await Recording.findOne({
//       _id: req.params.recordingId
//     }).exec();
//     const events = await Event.find(recordingId, '-recordingId -pageId -__v -_id -createdAt').exec();
//     const website = await Website.findOne({
//       _id: recording.siteId
//     }).select('excludedContent name protocol').exec();
//     if (website.protocol === 'https') {
//       return res.status(200).send({
//         code: 200,
//         data: {
//           excludedContent: website.excludedContent,
//           domain: website.name,
//           recording: recording,
//           events: events,
//           protocol: website.protocol
//         }
//       });
//     } else {
//       let params = {
//         Bucket: 'flopandascripts',
//         Key: 'websites/'+recording.siteId+'/'+req.params.recordingId+'.html',
//         ResponseContentEncoding: 'text/html'
//       };
//       console.log(params);
//       s3.getObject(params, function(s3Err, newData) {
//         if (s3Err) {
//           console.log(s3Err);
//           return res.send(s3Err);
//         } else {
//           let htmlCopy = newData.Body.toString('utf-8');
//           recording.htmlCopy = htmlCopy;
//           return res.json({
//             code: 200,
//             data: {
//               excludedContent: website.excludedContent,
//               domain: website.name,
//               recording: recording,
//               events: events,
//               protocol: website.protocol
//             }
//           });
//         }
//       });
//     }
//     // let params = {
//     //   Bucket: 'flopandascripts',
//     //   Key: 'websites/'+recording.siteId+'/'+req.params.recordingId+'.html',
//     //   ResponseContentEncoding: 'text/html'
//     // };
//     // s3.getObject(params, function(s3Err, data) {
//     //   if (s3Err) {
//     //     console.log(s3Err);
//     //   } else {
//     //     recording.htmlCopy = data.Body.toString('utf-8');
//     //     console.log(recording.html);
//     //     res.json({
//     //       code: 200,
//     //       data: {
//     //         excludedContent: website.excludedContent,
//     //         domain: website.name,
//     //         recording: recording,
//     //         events: events
//     //       }
//     //     });
//     //     // return data.Body.toString('utf-8');
//     //   }
//     // });
//   } catch (error) {
//     next(error);
//   }
// };

exports.playOflineRecording = async (req, res, next) => {
  try {
    const recordingId = {
      recordingId: req.params.recordingId
    };
    const recording = await Recording.findOne({
      _id: recordingId.recordingId
    }).exec();
    const events = await Event.find(recordingId, '-recordingId -pageId -__v -_id -createdAt').exec();
    const website = await Website.findOne({
      _id: recording.siteId
    }).exec();
    res.json({
      code: 200,
      data: {
        excludedContent: website.excludedContent,
        domain: website.name,
        recording: recording,
        events: events
      }
    });
  } catch (error) {
    next(error);
  }
};

exports.pageInfo = async (req, res, next) => {
  try {
    const siteId = req.params.siteId;
    const pages = await Page.find({
      websiteId: siteId
    }).select('name').exec();
    return res.status(200).send({
      code: 200,
      data: pages
    });
  } catch (error) {
    next(error);
  }
};

exports.siteAllRecordings = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const siteId = req.params.siteId;
    const from = req.params.from;
    const to = req.params.to;
    const fromDate = new Date(from);
    const toDate = new Date(to);
    var timeDiff = Math.abs(fromDate.getTime() - toDate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    const queryData = req.query;
    queryData.createdAt = {
      $gte: fromDate,
      $lte: toDate
    };
    queryData.type = 'online';
    queryData.siteId = new mongoose.Types.ObjectId(siteId);
    if (Object.size(queryData)) {
      if (queryData.clickRage !== undefined) {
        queryData.clickRage = parseInt(queryData.clickRage);
      }

      if (queryData.clickError !== undefined) {
        queryData.clickError = parseInt(queryData.clickError);
      }

      if (queryData.formInteract !== undefined) {
        queryData.formInteract = parseInt(queryData.formInteract);
      }

      if (queryData.formSubmit !== undefined) {
        queryData.formSubmit = parseInt(queryData.formSubmit);
      }
      if (queryData.paymentTag !== undefined) {
        queryData.paymentTag = parseInt(queryData.paymentTag);
      }
      if ((req.query.visitMin !== undefined) && (req.query.visitMax)) {
        queryData.recDuration = { $gte: parseInt(req.query.visitMin), $lte: parseInt(req.query.visitMax) };
      } else if (req.query.visitMin !== undefined) {
        queryData.recDuration = { $gte: parseInt(req.query.visitMin) };
      } else if (req.query.visitMax !== undefined) {
        queryData.recDuration = { $gte: 0, $lte: parseInt(req.query.visitMax) };
      }
      delete queryData.visitMin;
      delete queryData.visitMax;
    }
    const data = await Session.aggregate(
      [
        {
          $match: {
            $and: [
              queryData
            ]
          }
        },
        {
          $group: {
            _id: {
              yearMonthDayUTC: {
                $dateToString: {
                  format: "%Y-%m-%d",
                  date: "$createdAt"
                }
              }
            },
            total: {
              $sum: 1
            }
          }
        },
      ]
      , (err, result) => {
        if (err) {
          console.log(err);
        }
        return result;
      });
    const dates = [];
    const counts = [];
    for (let i = 0; i < diffDays; i++) {
      let newFromDate;
      newFromDate = new Date(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate() + i);
      newFromDate.setHours(23, 59, 59, 999);
      let pushDate = moment(newFromDate).format('YYYY/MM/DD');
      let index = _.filter(data, function (num) {
        let nnnDate = new Date(num._id.yearMonthDayUTC);
        nnnDate.setHours(23, 59, 59, 999);
        var diff = (newFromDate - nnnDate)
        if (diff === 0) {
          return num;
        }
      });
      if (index.length > 0) {
        counts.push(index[0].total);
        dates.push(index[0]._id.yearMonthDayUTC);
      } else {
        dates.push(pushDate);
        counts.push(0);
      }
    }
    return res.status(200).send({
      code: 200,
      data: dates,
      counts: counts
    });
  } catch (error) {
    next(error);
  }
};

exports.resolutions = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const siteId = req.params.siteId;
    const resolutions = await Session.distinct('resolution', {
      siteId: siteId
    }).exec();
    return res.send({
      code: 200,
      data: resolutions
    });
  } catch (error) {
    next(error);
  }
}


exports.zipFiles = async (req, res, next) => {
  try {
    // let sessionPath = path.join(__dirname, '..', 'uploadedFiles/recording/js/', 'session.js');
    let htmlPath = path.join(__dirname, '..', 'uploadedFiles/recording/', 'recording.html');
    let css1FilePath = path.join(__dirname, '..', 'uploadedFiles/recording/css/', 'bootstrap.min.css');
    let css2FilePath = path.join(__dirname, '..', 'uploadedFiles/recording/css/', 'style.css');
    let js1FilePath = path.join(__dirname, '..', 'uploadedFiles/recording/js/', 'bootstrap.min.js');
    let js2FilePath = path.join(__dirname, '..', 'uploadedFiles/recording/js/', 'player.js');
    let imagePath = path.join(__dirname, '..', 'uploadedFiles/recording/images/', 'cursor.png');
    let momentJs = path.join(__dirname, '..', 'uploadedFiles/recording/js/', 'moment.js');
    let imagePath2 = path.join(__dirname, '..', 'uploadedFiles/recording/images/', 'transperant.png');
    let zipStorePath = path.join(__dirname, '..', '../public/zip/');


    await fs.readFile(htmlPath, (err, data) => {
      zip.file('recording.html', data);
    });
    await fs.readFile(css1FilePath, (err, data) => {
      zip.file('bootstrap.min.css', data);
    });
    await fs.readFile(css2FilePath, (err, data) => {
      zip.file('style.css', data);
    });
    await fs.readFile(js1FilePath, (err, data) => {
      zip.file('bootstrap.min.js', data);
    })
    await fs.readFile(js2FilePath, 'utf8', (err, data) => {
      let jsData = data.toString().replace('/session/N_wAyldv5/recording', `/session/${req.params.sessionId}/recording`);
      zip.file('player.js', jsData);
      data.toString().replace(req.params.sessionId, 'N_wAyldv5');
    });
    await fs.readFile(imagePath, (err, data) => {
      zip.file('cursor.png', data);
    });
    await fs.readFile(imagePath2, (err, data) => {
      zip.file('transperant.png', data);
    });

    await fs.readFile(momentJs, (err, momentLib) => {
      zip.file('moment.js', momentLib);
    });

    setTimeout(function () {
      let dataZip = zip.generate({
        base64: false,
        compression: 'DEFLATE'
      });
      fs.writeFile(zipStorePath + req.params.sessionId + '.zip', dataZip, 'binary', (err) => {

        if (err) console.log(err);
        res.download(zipStorePath + req.params.sessionId + '.zip');
      });
    }, 1000);




  } catch (error) {
    next(error);
  }
};

exports.recordingStats = async (req, res, next) => {
  try {
    const from = new Date(req.params.from);
    const to = new Date(req.params.to);
    const siteId = new mongoose.Types.ObjectId(req.params.siteId);
    const queryData = req.query;
    if (Object.keys(req.query).length > 0) {
      if (queryData.clickRage !== undefined) {
        queryData.clickRage = { $gte: parseInt(req.query.clickRage) };
      }

      if (queryData.clickError !== undefined) {
        queryData.clickError = parseInt(req.query.clickError);
      }

      if (queryData.formInteract !== undefined) {
        queryData.formInteract = parseInt(req.query.formInteract);
      }

      if (queryData.formSubmit !== undefined) {
        queryData.formSubmit = parseInt(req.query.formSubmit);

      }
      if (queryData.paymentTag !== undefined) {
        queryData.paymentTag = parseInt(req.query.paymentTag);
      }
    }
    queryData.siteId = siteId;
    queryData.createdAt = {
      $gt: from,
      $lt: to
    };
    queryData.type = 'online';
    const website = await Website.findOne({
      _id: siteId
    }).select('stopRecording').exec();
    let data = await Session.aggregate([
      {
        $match: {
          $and: [queryData]
        }
      }, {
        $group: {
          _id: null,
          avgRecPerSess: {
            $avg: "$pageCount"
          },
          pagesviews: {
            $sum: "$pageCount"
          },
          duration: {
            $avg: "$recDuration"
          },
          recording: {
            $sum: 1
          },
          uniqueVis: {
            $addToSet: "$ip"
          }
        }
      }, {
        $project: {
          pagesviews: 1,
          avgRecPerSess: 1,
          duration: 1,
          recording: 1,
          size: {
            $size: "$uniqueVis"
          }
        }
      }
    ]);
    res.json({
      code: 200,
      data: {
        recStatus: website.stopRecording,
        recordings: data.length > 0 ? data[0].recording : 0,
        visitors: data.length > 0 ? data[0].size : 0,
        views: data.length > 0 ? data[0].pagesviews : 0,
        ppr: data.length > 0 ? data[0].avgRecPerSess : 0,
        avgRecording: data.length > 0 ? data[0].duration : 0
      }
    });
  } catch (error) {
    next(error);
  }
};

async function getStats(queryData) {
  try {
    const stats = await Session.find({
      $and: [queryData]
    }).select('pageName ip').populate('recordings', 'duration').exec();
    return stats;
  } catch (error) {
    return error;
  }
}

async function getRecordings(queryData) {
  try {
    const recordingss = await Recording.aggregate([
      {
        $match: {
          $and: [queryData]
        }
      },
      {
        $group: {
          _id: null,
          duration: {
            $avg: '$duration'
          },
          total: {
            $sum: 1
          },
        },
      },
    ]);
    return recordingss;
  } catch (error) {
    return recordingss;
  }
}


async function getRecordingHtml(siteId, recordingId) {
  try {
    console.log('websites/' + siteId + '/' + recordingId + '.html');
    let params = {
      Bucket: 'flopandascripts',
      Key: 'websites/5c86792f2ccd3925f0e45511/54545645645hh45hh45h.html',
      ResponseContentEncoding: 'text/html'
    };
    s3.getObject(params, function (s3Err, data) {
      if (s3Err) {
        console.log(s3Err);
      } else {
        return data.Body.toString('utf-8');
      }
    });
  } catch (error) {

  }
}