const CMS = require("../models/cms.model");
const Users = require("../models/users.model");
const Sites = require("../models/website.model");
const Payments = require("../models/payment.model");
const Website = require("../models/website.model");
const Session = require("../models/session.model");
const Recording = require("../models/recording.model");
const Country = require("../models/country.model");
const Logs = require("../models/log.model");
const ConversionPrice = require("../models/convertionPrice.model");
const FeedBack = require("../models/feedback.model");
const Siteids = require("../models/siteid.model");
const SiteConversion = require("../models/siteConversionCron.model");
const UserConversionPackage = require("../models/userConversionPackage.model");
const Package = require("../models/packages.model");
const MetaData = require("../models/metaData.model");
const Page = require("../models/page.model");
const RefreshToken = require("../models/refreshToken.model");
const Interaction = require("../models/interactions.model");
const UserForm = require("../models/userForm.model");
const FieldTracking = require("../models/fieldTracking.model");
const Event = require("../models/event.model");
const Form = require("../models/form.model");

exports.post = async (req, res, next) => {
  try {
    const cms = await CMS.create({
      title: req.body.title,
      slug: req.body.slug,
      keyword: req.body.keyword,
      description: req.body.description,
      details: req.body.details,
      status: req.body.status
    });
    res.status(200).send({
      code: 200,
      data: [],
      message: "CMS has been created."
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};
exports.put = async (req, res, next) => {
  try {
    const cms = await CMS.findById(req.body.cmsId).exec();
    (cms.title = req.body.title),
      (cms.slug = req.body.slug),
      (cms.keyword = req.body.keyword),
      (cms.description = req.body.description),
      (cms.details = req.body.details),
      (cms.status = req.body.status);
    const cmsUpdated = await cms.save();
    res.status(200).send({
      code: 200,
      data: [],
      message: "CMS has been updated."
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};
exports.get = async (req, res, next) => {
  try {
    const cms = await CMS.find({}).exec();
    res.status(200).send({
      code: 200,
      data: cms,
      message: ""
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};
exports.delete = async (req, res, next) => {
  try {
    const cms = await CMS.deleteOne({ _id: req.body.cmsId });
    res.status(200).send({
      code: 200,
      data: [],
      message: "CMS deleted with success ."
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

exports.savePackagePrices = async (req, res) => {
  try {
    const conversionPrice = new ConversionPrice(req.body);
    await conversionPrice.save();
    res.status(200).send({
      code: 200,
      data: [],
      message: "CMS has been created."
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};
exports.editPricePackages = async (req, res) => {
  try {
    let packages = await ConversionPrice.findOne({ _id: req.body.id }).exec();
    packages.country = req.body.country;
    packages.costPerSms = req.body.costPerSms;
    packages.costPerEmail = req.body.costPerEmail;
    packages.maxSmsPerDay = req.body.maxSmsPerDay;
    packages.maxEmailPerDay = req.body.maxEmailPerDay;
    packages.daysPerMonth = req.body.daysPerMonth;
    packages.package1.totalCostUSD = req.body.package1.totalCostUSD;
    packages.package1.totalCostGbp = req.body.package1.totalCostGbp;
    packages.package1.emailCostUSD = req.body.package1.emailCostUSD;
    packages.package1.emailCostGBP = req.body.package1.emailCostGBP;
    packages.package1.totalUsers = req.body.package1.totalUsers;
    packages.package2.totalCostUSD = req.body.package2.totalCostUSD;
    packages.package2.totalCostGbp = req.body.package2.totalCostGbp;
    packages.package2.emailCostUSD = req.body.package2.emailCostUSD;
    packages.package2.emailCostGBP = req.body.package2.emailCostGBP;
    packages.package2.totalUsers = req.body.package2.totalUsers;
    packages.package3.totalCostUSD = req.body.package3.totalCostUSD;
    packages.package3.totalCostGbp = req.body.package3.totalCostGbp;
    packages.package3.emailCostUSD = req.body.package3.emailCostUSD;
    packages.package3.emailCostGBP = req.body.package3.emailCostGBP;
    packages.package3.totalUsers = req.body.package3.totalUsers;
    packages.package4.totalCostUSD = req.body.package4.totalCostUSD;
    packages.package4.totalCostGbp = req.body.package4.totalCostGbp;
    packages.package4.emailCostUSD = req.body.package4.emailCostUSD;
    packages.package4.emailCostGBP = req.body.package4.emailCostGBP;
    packages.package4.totalUsers = req.body.package4.totalUsers;
    packages.package5.totalCostUSD = req.body.package5.totalCostUSD;
    packages.package5.totalCostGbp = req.body.package5.totalCostGbp;
    packages.package5.emailCostUSD = req.body.package5.emailCostUSD;
    packages.package5.emailCostGBP = req.body.package5.emailCostGBP;
    packages.package5.totalUsers = req.body.package5.totalUsers;
    const packageUpdated = await packages.save();
    res.status(200).send({
      code: 200,
      data: packageUpdated,
      message: "Packages has been updated."
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};
exports.deleteMany = async (req, res, next) => {
  try {
    const packageId = req.body.packageIds;
    for (let index = 0; index < packageId.length; index++) {
      await ConversionPrice.findOneAndDelete({ _id: packageId[index] }).exec();
    }
    res.status(200).send({
      code: 200,
      data: [],
      message: "Package deleted with success ."
    });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};
exports.getAllPricePackages = async (req, res) => {
  try {
    // const fromDate = new Date(req.params.from);
    // const toDate = new Date(req.params.to);
    let packages = await ConversionPrice.find({}).exec();
    return res.status(200).send({ code: 200, packages: packages });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};
exports.deletePackagePrices = async (req, res) => {
  try {
    let id = req.body.id;
    let packages = await ConversionPrice.deleteOne({ _id: id }).exec();
    return res
      .status(200)
      .send({ code: 200, packages: [], message: "Delete Successfully." });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

exports.getCurrentUsersEmails = async (req, res, next) => {
  try {
    let data = {};
    if (req.query.email !== undefined) {
      let email = "";
      if (req.query.email.includes("+")) {
        email = req.query.email;
        email = req.query.email.split("+");
        email = email[0] + "\\+" + email[1];
      } else {
        email = req.query.email;
      }
      data.email = { $regex: new RegExp(email), $options: "i" };
    }
    data.roles = { $nin: ["4", "5"] };
    data.isDeleted = false;
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * limit);
    let userEmails = await Users.aggregate([
      {
        $match: {
          $and: [data]
        }
      },
      {
        $facet: {
          userEmails: [
            { $group: { _id: "$email" } },
            { $sort: { createdAt: -1 } },
            { $skip: offset },
            { $limit: limit }
          ],
          totalCount: [
            {
              $group: {
                _id: "null",
                total: {
                  $sum: 1
                }
              }
            }
          ]
        }
      }
    ]);
    return res.status(200).send({
      code: 200,
      data: userEmails
      // total: totalCount
    });
    return res.send();
  } catch (error) {
    next(error);
  }
};

exports.listAllUsers = async (req, res, next) => {
  try {
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 20);
    let data = {};
    if (req.query.myName) {
      data.myName = { $regex: new RegExp(req.query.myName), $options: "i" };
    }
    if (req.query.email !== undefined) {
      let email = "";
      if (req.query.email.includes("+")) {
        email = req.query.email;
        email = req.query.email.split("+");
        email = email[0] + "\\+" + email[1];
      } else {
        email = req.query.email;
      }
      data.email = { $regex: new RegExp(email), $options: "i" };
    }
    if (req.query.packageId) {
      var ObjectId = require("mongoose").Types.ObjectId;
      data.packageId = new ObjectId(req.query.packageId);
    }
    if (req.query.from !== undefined && req.query.to !== undefined) {
      data.createdAt = {
        $gt: new Date(req.query.from),
        $lt: new Date(req.query.to)
      };
    }
    if (req.params.support === "true") {
      data.roles = { $in: ["4", "5"] };
    } else {
      data.roles = { $nin: ["4", "5"] };
    }
    if (req.query.roles) {
      data.roles = req.query.roles;
    }
    data.isDeleted = false;
    const count = await Users.count({
      $and: [data]
    }).exec();
    const users = await Users.aggregate([
      { $sort: { createdAt: -1 } },
      {
        $match: {
          $and: [data]
        }
      },
      {
        $lookup: {
          from: "packages",
          localField: "packageId",
          foreignField: "_id",
          as: "PackageDetail"
        }
      },
      { $unwind: { path: "$PackageDetail", preserveNullAndEmptyArrays: true } },
      {
        $lookup: {
          from: "websites",
          localField: "_id",
          foreignField: "userId",
          as: "totalSites"
        }
      },
      {
        $skip: offset
      },
      {
        $limit: limit
      }
    ]);
    // .find({
    //     $and: [
    //         data
    //     ]
    // }).populate('packageId', 'planeName planeType')
    //     .skip(offset)
    //     .limit(limit)
    //     .sort({ createdAt: -1 })
    //     .exec();
    return res.json({ code: 200, data: users, totalUser: count });
  } catch (error) {
    next(error);
  }
};
exports.userStatus = async (req, res, next) => {
  try {
    const user = await Users.findOne({ _id: req.body.userId });
    user.active = req.body.active;
    const userUpdate = await user.save();
    res.status(200).send({
      code: 200,
      data: [],
      message: "User update successfully ."
    });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};
exports.deleteUser = async (req, res, next) => {
  try {
    const userId = req.body.id;
    const userData = await Users.findById(userId).exec();
    // const user = await Users.deleteOne({ _id: userId }).exec();
    const user = await Users.updateOne(
      { _id: userId },
      {
        $set: {
          isDeleted: true
        }
      }
    );
    await RefreshToken.deleteMany({
      userId: new mongoose.Types.ObjectId(userId)
    });
    if (user !== undefined && userData) {
      const acitveUser = await Users.findById(req.user)
        .select("_id email")
        .exec();
      const accountInfo = new Logs({
        userId,
        email: userData.email,
        reasons: "Deleted by " + acitveUser.email,
        logType: "account"
      });
      await accountInfo.save();
    }
    res.status(200).send({
      code: 200,
      data: [],
      message: "User deleted with success ."
    });
    if (
      userData &&
      (userData.roles === "1" ||
        userData.roles === "2" ||
        userData.roles === "3")
    ) {
      await Users.deleteMany({ ownerId: userId }).exec();
      const websites = await Website.find({ userId: userId })
        .select("_id")
        .exec();
      for (let index = 0; index < websites.length; index++) {
        let websiteId = [];
        websiteId.push(websites[index]._id);
        await Users.updateMany(
          { website: { $in: websiteId } },
          { $pull: { website: { $in: websiteId } } },
          { multi: true }
        );
        await Siteids.updateMany(
          { siteIds: { $in: websiteId } },
          { $pull: { siteIds: { $in: websiteId } } },
          { multi: true }
        );
        await Package.deleteOne({ email: userData.email }).exec();
        await Website.deleteOne({ _id: websites[index]._id }).exec();
        await Event.deleteMany({ siteId: websites[index]._id }).exec();
        await MetaData.deleteMany({ siteId: websites[index]._id }).exec();
        await Page.deleteMany({ websiteId: websites[index]._id }).exec();
        await Interaction.deleteMany({ siteId: websites[index]._id }).exec();
        await UserForm.deleteMany({ siteId: websites[index]._id }).exec();
        await Session.deleteMany({ siteId: websites[index]._id }).exec();
        await Recording.deleteMany({ siteId: websites[index]._id }).exec();
        await FieldTracking.deleteMany({ siteId: websites[index]._id }).exec();
        await Interaction.deleteMany({ siteId: websites[index]._id }).exec();
        const forms = await Form.find({ siteId: websites[index]._id }).exec();
        for (let i = 0; i < forms.length; i++) {
          await Form.deleteMany({ siteId: websites[index]._id }).exec();
          await FormField.deleteMany({ formId: forms[i]._id }).exec();
        }
      }
      await SiteConversion.deleteMany({ userId: req.body.userId }).exec();
      await UserConversionPackage.deleteOne({ userId: req.body.userId }).exec();
    }
  } catch (error) {
    console.log(error);
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

exports.adminOverViewData = async (req, res, next) => {
  try {
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    let data = {};
    // let where = {};
    data.createdAt = { $gt: fromDate, $lt: toDate };
    // where.active = true
    // const users = await Users.count({
    //     $and: [
    //         where
    //     ]
    // }).exec();
    const sites = await Sites.count({
      $and: [{ createdAt: { $gt: fromDate, $lt: toDate } }]
    }).exec();
    // data.paymentSuccess = true;
    const payments = await Payments.aggregate([
      {
        $match: {
          $and: [
            { createdAt: { $gt: fromDate, $lt: toDate } },
            { paymentSuccess: true }
          ]
        }
      },
      {
        $facet: {
          monthlyPyament: [
            {
              $match: {
                duration: "monthly"
              }
            },
            {
              $group: {
                _id: null,
                USD: {
                  $sum: {
                    $cond: [
                      {
                        $eq: ["$currency", "USD"]
                      },
                      "$price",
                      0
                    ]
                  }
                },
                GBP: {
                  $sum: {
                    $cond: [
                      {
                        $eq: ["$currency", "GBP"]
                      },
                      "$price",
                      0
                    ]
                  }
                }
              }
            },
            {
              $project: {
                _id: 0
              }
            }
          ],
          yearlyPyament: [
            {
              $match: {
                duration: "annually"
              }
            },
            {
              $group: {
                _id: null,
                USD: {
                  $sum: {
                    $cond: [
                      {
                        $eq: ["$currency", "USD"]
                      },
                      "$price",
                      0
                    ]
                  }
                },
                GBP: {
                  $sum: {
                    $cond: [
                      {
                        $eq: ["$currency", "GBP"]
                      },
                      "$price",
                      0
                    ]
                  }
                }
              }
            },
            {
              $project: {
                _id: 0
              }
            }
          ],
          payments: [
            {
              $group: {
                _id: null,
                USD: {
                  $sum: {
                    $cond: [
                      {
                        $eq: ["$currency", "USD"]
                      },
                      "$price",
                      0
                    ]
                  }
                },
                GBP: {
                  $sum: {
                    $cond: [
                      {
                        $eq: ["$currency", "GBP"]
                      },
                      "$price",
                      0
                    ]
                  }
                }
              }
            }
          ]
        }
      }
    ]);
    let usersData = await Users.aggregate([
      {
        $match: {
          $and: [{ ...data, isDeleted: false }]
        }
      },
      {
        $facet: {
          freeUsers: [
            {
              $match: {
                isPaid: false
              }
            },
            {
              $group: {
                _id: "null",
                count: {
                  $sum: 1
                }
              }
            },
            {
              $project: {
                _id: 0
              }
            }
          ],
          paidUsers: [
            {
              $match: {
                isPaid: true
              }
            },
            {
              $group: {
                _id: "null",
                count: {
                  $sum: 1
                }
              }
            },
            {
              $project: {
                _id: 0
              }
            }
          ],
          totalUsers: [
            {
              $match: {
                active: true
              }
            },
            {
              $group: {
                _id: "null",
                count: {
                  $sum: 1
                }
              }
            },
            {
              $project: {
                _id: 0
              }
            }
          ]
        }
      }
    ]);
    let deletedUsers = await Users.find({
      updatedAt: { $gt: fromDate, $lt: toDate },
      isDeleted: true
    }).count();
    return res.status(200).send({
      code: 200,
      data: {
        users: usersData[0],
        sites: sites,
        payments: payments,
        deletedUsers: deletedUsers
      },
      message: "Retrieve Records Successfully."
    });
  } catch (error) {
    console.log(error);
  }
};

exports.siteDetail = async (req, res, next) => {
  try {
    let userId = req.params.userId;
    var ObjectId = require("mongoose").Types.ObjectId;
    let result = await Users.aggregate([
      {
        $lookup: {
          from: "websites",
          localField: "_id",
          foreignField: "userId",
          as: "totalSites"
        }
      },
      { $unwind: "$totalSites" },
      {
        $match: {
          $and: [{ _id: new ObjectId(userId) }]
        }
      },
      {
        $lookup: {
          from: "sessions",
          localField: "totalSites._id",
          foreignField: "siteId",
          as: "totalSession"
        }
      },
      { $unwind: { path: "$totalSession", preserveNullAndEmptyArrays: true } },
      {
        $project: {
          _id: 1,
          siteName: "$totalSites.name",
          numberOfRecording: {
            $cond: {
              if: { $isArray: "$totalSession.recordings" },
              then: { $size: "$totalSession.recordings" },
              else: "0"
            }
          }
        }
      },
      {
        $group: {
          _id: "$siteName",
          countRecording: { $sum: "$numberOfRecording" },
          numberOfSession: { $sum: 1 }
        }
      },
      {
        $project: {
          countRecording: 1,
          countSesstion: {
            $cond: {
              if: { $lte: ["$countRecording", 0] },
              then: 0,
              else: "$numberOfSession"
            }
          }
        }
      }
    ]).exec((err, result) => {
      if (err) {
        console.log("error", err);
      }
      if (result) {
        res.status(200).send({
          code: 200,
          data: result,
          message: ""
        });
      }
    });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};

//country
exports.create = async (req, res, next) => {
  try {
    const country = await Country.create({
      label: req.body.label,
      name: req.body.name,
      code: req.body.code,
      routeId: req.body.routeId
    });
    res.status(200).send({
      code: 200,
      data: [],
      message: "Country has been created."
    });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};
exports.getCountry = async (req, res, next) => {
  try {
    // const fromDate = new Date(req.params.from);
    // const toDate = new Date(req.params.to);
    // const countries = await Country.find({ createdAt: { $gt: fromDate, $lt: toDate } }).exec();
    let where = {};
    if (req.query.label !== undefined) {
      where.label = { $regex: new RegExp(req.query.label), $options: "i" };
    }
    const countries = await Country.find({ $and: [where] }).exec();
    res.status(200).send({
      code: 200,
      data: countries,
      message: ""
    });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};
exports.editCountry = async (req, res, next) => {
  try {
    let id = req.params.id;
    const country = await Country.findById({ _id: id }).exec();
    res.status(200).send({
      code: 200,
      data: country,
      message: ""
    });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};
exports.update = async (req, res, next) => {
  try {
    const country = await Country.findById(req.body.id).exec();
    country.label = req.body.label;
    country.name = req.body.name;
    country.code = req.body.code;
    country.routeId = req.body.routeId;
    const countryUpdated = await country.save();
    res.status(200).send({
      code: 200,
      data: [],
      message: "Country has been updated."
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};
exports.deleteCountry = async (req, res, next) => {
  try {
    const country = await Country.deleteOne({ _id: req.body.id });
    res.status(200).send({
      code: 200,
      data: [],
      message: "Country deleted with success ."
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};
exports.deleteManyCountry = async (req, res, next) => {
  try {
    const countryId = req.body.countryIds;
    for (let index = 0; index < countryId.length; index++) {
      await Country.findOneAndDelete({ _id: countryId[index] }).exec();
    }
    res.status(200).send({
      code: 200,
      data: [],
      message: "Country deleted with success ."
    });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};
exports.paymentHistory = async (req, res, next) => {
  try {
    let totalRecord = "";
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 20);
    let data = {};
    if (Object.keys(req.query).length > 0) {
      if (req.query.email !== undefined) {
        data["totalRecords.email"] = req.query.email;
      }
      if (req.query.paymentMethod !== undefined) {
        data.paymentMethod = {
          $regex: new RegExp(req.query.paymentMethod),
          $options: "i"
        };
      }
      if (req.query.currency !== undefined) {
        data.currency = {
          $regex: new RegExp(req.query.currency),
          $options: "i"
        };
      }
      if (req.query.from !== undefined && req.query.to !== undefined) {
        data.createdAt = {
          $gt: new Date(req.query.from),
          $lt: new Date(req.query.to)
        };
      }
    }
    data.paymentSuccess = true;
    totalRecord = await Payments.aggregate([
      {
        $lookup: {
          from: "users",
          localField: "userId",
          foreignField: "_id",
          as: "totalRecords"
        }
      },
      {
        $match: { $and: [data] }
      },
      {
        $facet: {
          paymentCount: [
            {
              $group: {
                _id: null,
                USD: {
                  $sum: {
                    $cond: [{ $eq: ["$currency", "USD"] }, "$price", 0]
                  }
                },
                GBP: {
                  $sum: {
                    $cond: [{ $eq: ["$currency", "GBP"] }, "$price", 0]
                  }
                },
                totalCount: {
                  $sum: 1
                }
              }
            }
          ]
        }
      }
    ]);
    payments = await Payments.aggregate([
      {
        $sort: { createdAt: -1 }
      },
      {
        $lookup: {
          from: "users",
          localField: "userId",
          foreignField: "_id",
          as: "totalRecords"
        }
      },
      {
        $match: { $and: [data] }
      },
      {
        $skip: offset
      },
      {
        $limit: limit
      }
    ]);
    return res.status(200).send({
      code: 200,
      payments: payments,
      totalRecord: totalRecord,
      message: ""
    });
  } catch (error) {
    next(error);
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};
exports.getUserViewDetail = async (req, res, next) => {
  try {
    const userId = req.params.userId;
    let data = {};
    if (userId !== undefined) {
      var ObjectId = require("mongoose").Types.ObjectId;
      data._id = new ObjectId(userId);
    }
    data.isDeleted = false;
    // data['PaymentDetail.paymentSuccess'] = true;
    const users = await Users.aggregate([
      { $sort: { createdAt: -1 } },
      {
        $match: {
          $and: [data]
        }
      },
      {
        $lookup: {
          from: "packages",
          localField: "packageId",
          foreignField: "_id",
          as: "PackageDetail"
        }
      },
      { $unwind: { path: "$PackageDetail", preserveNullAndEmptyArrays: true } },
      {
        $lookup: {
          from: "websites",
          localField: "_id",
          foreignField: "userId",
          as: "totalSites"
        }
      }
    ]);
    return res.json({ code: 200, data: users });
  } catch (error) {
    next(error);
  }
};
exports.userExpiry = async (req, res, next) => {
  try {
    const userId = req.body.userId;
    const users = await Users.findById({ _id: userId }).exec();
    users.expired = true;
    const userUpdated = await users.save();
    return res.json({
      code: 200,
      data: [],
      message: "User expiry status has been update"
    });
  } catch (error) {
    next(error);
  }
};
exports.getCombineUserEmails = async (req, res, next) => {
  try {
    let data = {};
    let totalCount = 0;
    let userEmails = [];
    if (req.query.email !== undefined) {
      let email = "";
      if (req.query.email.includes("+")) {
        email = req.query.email;
        email = req.query.email.split("+");
        email = email[0] + "\\+" + email[1];
      } else {
        email = req.query.email;
      }
      data.email = { $regex: new RegExp(email), $options: "i" };
    }
    if (
      req.params.logType !== undefined &&
      req.params.logType !== "feedbacks"
    ) {
      data.logType = req.params.logType;
    }
    // data.email = { $nin: [null, ''] }
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * limit);
    if (
      req.params.logType !== undefined &&
      req.params.logType !== "feedbacks"
    ) {
      userEmails = await Logs.aggregate([
        {
          $match: {
            $and: [data]
            // $and: [ { email: { $ne: ['', null]  } } ]
          }
        },
        {
          $facet: {
            userEmails: [
              { $group: { _id: "$email" } },
              { $sort: { createdAt: -1 } },
              { $skip: offset },
              { $limit: limit }
            ]
          }
        }
      ]);
      totalCount = await Logs.distinct("email", data).exec();
    } else {
      // totalCount = await FeedBack.count({ $and: [data] }).exec();
      userEmails = await FeedBack.aggregate([
        {
          $match: {
            $and: [data]
          }
        },
        {
          $facet: {
            userEmails: [
              { $group: { _id: "$email" } },
              { $skip: offset },
              { $sort: { createdAt: -1 } },
              { $limit: limit }
            ]
          }
        }
      ]);
      totalCount = await FeedBack.distinct("email", data).exec();
    }
    if (userEmails.length > 0) {
      let arr = [];
      arr.push({
        total: totalCount.length
      });
      userEmails[0].totalCount = arr;
    }
    return res.status(200).send({
      code: 200,
      data: userEmails
      // totalCount
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};
exports.deleteUserHistory = async (req, res, next) => {
  try {
    let users = "";
    let email = "";
    let count = "";
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 20);
    let data = {};
    data.logType = req.params.logType;

    if (req.query.email !== undefined) {
      data.email = req.query.email;
    }
    if (req.query.from !== undefined && req.query.to !== undefined) {
      data.createdAt = {
        $gt: new Date(req.query.from),
        $lt: new Date(req.query.to)
      };
    }
    if (req.query.siteName !== undefined) {
      data.siteName = { $regex: new RegExp(req.query.siteName), $options: "i" };
    }

    count = await Logs.count({
      $and: [data]
    }).exec();
    users = await Logs.find({ $and: [data] })
      .sort({ createdAt: -1 })
      .skip(offset)
      .limit(limit)
      .exec();
    return res.json({ code: 200, data: users, totalUserRecord: count });
  } catch (error) {
    next(error);
  }
};
exports.getSite = async (req, res, next) => {
  try {
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 20);
    var data = {};
    if (Object.keys(req.query).length > 0) {
      if (req.query.protocol !== undefined) {
        data.protocol = req.query.protocol;
      }
      if (req.query.name !== undefined) {
        let name = "";
        if (req.query.name.includes("+")) {
          name = req.query.name;
          name = req.query.name.split("+");
          name = name[0] + "\\+" + name[1];
        } else {
          name = req.query.name;
        }
        data.name = { $regex: new RegExp(name), $options: "i" };
      }
      if (req.query.iPAnonymization !== undefined) {
        data.iPAnonymization = JSON.parse(req.query.iPAnonymization);
      }
      if (req.query.honorTracking !== undefined) {
        data.honorTracking = JSON.parse(req.query.honorTracking);
      }
      if (req.query.turnOffStickyKeys !== undefined) {
        data.turnOffStickyKeys = JSON.parse(req.query.turnOffStickyKeys);
      }
      if (req.query.stopRecording !== undefined) {
        data.stopRecording = JSON.parse(req.query.stopRecording);
      }
    }
    let count = await Website.count({ $and: [data] }).exec();
    let website = await Website.find(
      { $and: [data] },
      {
        _id: 1,
        name: 1,
        protocol: 1,
        stopRecording: 1,
        turnOffStickyKeys: 1,
        iPAnonymization: 1,
        honorTracking: 1,
        userId: 1,
        isDownloadClickError: 1,
        isDownloadClickRage: 1,
        isDownloadFormDropoff: 1,
        createdAt: 1
      }
    )
      .sort({ createdAt: -1 })
      .skip(offset)
      .limit(limit)
      .exec();
    return res.json({ code: 200, data: website, totalCount: count });
  } catch (error) {
    console.log(error);
    next(error);
  }
};
exports.getSiteDetail = async (req, res, next) => {
  try {
    let id = req.params.siteId;
    var ObjectId = require("mongodb").ObjectId;
    var siteId = new ObjectId(id);
    let session = await Session.find({ siteId })
      .count({})
      .exec();
    let recording = await Recording.find({ siteId })
      .count({})
      .exec();
    return res.json({ code: 200, session: session, recording: recording });
  } catch (error) {
    next(error);
  }
};

exports.sites = async (req, res, next) => {
  try {
    let data = {};
    if (req.query.name !== undefined) {
      let name = "";
      if (req.query.name.includes("+")) {
        name = req.query.name;
        name = req.query.name.split("+");
        name = name[0] + "\\+" + name[1];
      } else {
        name = req.query.name;
      }
      data.name = { $regex: new RegExp(name), $options: "i" };
    }
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * limit);
    let sites = await Website.aggregate([
      {
        $match: {
          $and: [data]
        }
      },
      {
        $facet: {
          sites: [
            { $group: { _id: "$_id", name: { $addToSet: "$name" } } },
            { $unwind: "$name" },
            { $sort: { createdAt: -1 } },
            { $skip: offset },
            { $limit: limit }
          ],
          totalCount: [
            {
              $group: {
                _id: "null",
                total: {
                  $sum: 1
                }
              }
            }
          ]
        }
      }
    ]);
    return res.status(200).send({
      code: 200,
      data: sites
    });
  } catch (error) {
    next(error);
  }
};
exports.addRole = async (req, res, next) => {
  try {
    console.log(req.method);
    // let role = req.body.role;
    // let newRole = new Acl({
    //     role: role
    // });
    // await newRole.save();
    return res.send({ msg: "Role Created SUccessfully!" });
  } catch (error) {
    console.log(error);
  }
};
