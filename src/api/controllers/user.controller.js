const httpStatus = require('http-status');
const { omit } = require('lodash');
const User = require('../models/user.model');
const Logs = require('../models/log.model');
const Users = require('../models/users.model');
const Package = require('../models/packages.model');
const Website = require('../models/website.model');
const MetaData = require('../models/metaData.model');
const Page = require('../models/page.model');
const Interaction = require('../models/interactions.model');
const UserForm = require('../models/userForm.model');
const Recording = require('../models/recording.model');
const Session = require('../models/session.model');
const Event = require('../models/event.model');
const Form = require('../models/form.model');
const Report = require('../models/report.model');
const FormField = require('../models/formField.model');
const FieldTracking = require('../models/fieldTracking.model');
const Payment = require('../models/payment.model');
const Feedback = require('../models/feedback.model');
const Subscription = require('../models/subscription.model');
const SiteConversion = require('../models/siteConversionCron.model');
const UserConversionPackage = require('../models/userConversionPackage.model');
const Siteids = require('../models/siteid.model');
const { handler: errorHandler } = require('../middlewares/error');
var paypal = require('paypal-rest-sdk');
const moment = require('moment');
const path = require('path');
const consolidate = require('consolidate');
const authProviders = require('../services/emailProvider');
const { DOMAIN, DEV_DOMAIN, BETADOMAIN } = require('../../config/vars');
const crypto = require('crypto');
const RefreshToken = require('../models/refreshToken.model');

exports.userById = async (req, res) => {
  try {
    const user = await Users.findOne({
      _id: req.params.userId
    }).populate('admins', 'email roles active website _id').exec();
    let websites;
    if (user.roles === '1') {
      websites = await Website.find({ userId: req.params.userId }).select('name isSubscribe').exec();
    } else {
      websites = await Website.find({ userId: user.ownerId }).select('name isSubscribe').exec();
    }
    return res.send({
      code: 200,
      data: user,
      sites: websites
    });
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

// exports.update = (req, res, next) => {
//   const updatedUser = omit(req.body, ommitRole);
//   const user = Object.assign(req.locals.user, updatedUser);
//   user.save()
//     .then(savedUser => res.json(savedUser.transform()))
//     .catch(e => next(User.checkDuplicateEmail(e)));
// };

/**
 * Load user and append to req.
 * @public
 */
exports.load = async (req, res, next, id) => {
  try {
    const user = await User.get(id);
    req.locals = {
      user
    };
    return next();
  } catch (error) {
    return errorHandler(error, req, res);
  }
};

/**
 * Get user
 * @public
 */
exports.get = (req, res) => res.json(req.locals.user.transform());

/**
 * Get logged in user info
 * @public
 */
exports.loggedIn = (req, res) => res.json(req.user.transform());

/**
 * Create new user
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const user = new User(req.body);
    const savedUser = await user.save();
    res.status(httpStatus.CREATED);
    res.json(savedUser.transform());
  } catch (error) {
    next(User.checkDuplicateEmail(error));
  }
};

exports.createAdmin = async (req, res, next) => {
  try {
    const ownerId = await Users.findOne({
      _id: req.body.userId
    }).exec();
    const freePackage = await Package.findOne({
      planeName: 'Free'
    }).select('_id').exec();
    var token1 = await crypto.randomBytes(48).toString('hex');
    const checkUserExistance = await Users.findOne({ "email": req.body.email, "isDeleted": false }).exec();
    if (checkUserExistance) {
      return res.status(409).json({ code: 409, message: 'User already exist with this email' });
    } else {
      const newUser = new Users(req.body);
      newUser.password = await crypto.randomBytes(48).toString('hex');
      newUser.packageId = freePackage._id;
      newUser.ownerId = ownerId._id;
      newUser.resetPasswordToken = token1;
      newUser.resetPasswordExpires = Date.now() + 60000 * 60;
      // newUser.website = website
      newUser.verified = true;
      const user = await newUser.save();
      const owner = await Users.findOne({
        _id: req.body.userId
      }).exec();
      owner.admins.push(newUser._id);
      await owner.save();
      await Siteids.create(
        {
          userId: newUser._id,
          siteIds: req.body.website
        });
      const ownerEmail = owner.email;
      const email = req.body.email;
      // const password = req.body.password;
      const loginLink = `${DOMAIN}/resetPassword/${token1}`;
      const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'role-assign.html';
      consolidate.swig(templateFilePath, {
        ownerEmail,
        email,
        // password,
        loginLink
      }, (err, html) => {
        if (err) {
          console.log(err);
        } else {
          authProviders.send_email(req.body.email, 'Flopanda Login Details', html);
        }
      });
      const userTransformed = user.transform();
      return res.status(200).json({
        code: 200,
        user: userTransformed
      });
    }
  } catch (error) {
    console.log(error)
    return res.status(400).json({ code: 409, message: error.message });
    next(User.checkDuplicate(error));
  }
};
exports.updateAdmin = async (req, res, next) => {
  try {
    const checkUserExistance = await Users.findOne({ _id: req.body.userId , "isDeleted": false }).exec();
    if (checkUserExistance) {
      checkUserExistance.email = req.body.email;
      // checkUserExistance.password = req.body.password;
      checkUserExistance.roles = req.body.roles;
      checkUserExistance.website = req.body.website;
      await Siteids.update(
        {
          userId: req.body.userId
        },
        {
          siteIds: req.body.website,
          userId: req.body.userId
        },
        {
          upsert: true
        });
      const updatedUser = await checkUserExistance.save();
      return res.status(200).json({
        code: 200,
        data: [],
        message: "Your user has been updated"
      });
    } else {
      return res.status(404).json({ code: 404, message: 'No user found' });
    }
  } catch (error) {
    next(User.checkDuplicate(error));
  }
}
/**
 * Replace existing user
 * @public
 */
exports.replace = async (req, res, next) => {
  try {
    const {
      user
    } = req.locals;
    const newUser = new User(req.body);
    const ommitRole = user.role !== 'admin' ? 'role' : '';
    const newUserObject = omit(newUser.toObject(), '_id', ommitRole);

    await user.update(newUserObject, {
      override: true,
      upsert: true
    });
    const savedUser = await User.findById(user._id);

    res.json(savedUser.transform());
  } catch (error) {
    next(User.checkDuplicateEmail(error));
  }
};

/**
 * Update existing user
 * @public
 */
exports.update = (req, res, next) => {
  const ommitRole = req.locals.user.role !== 'admin' ? 'role' : '';
  const updatedUser = omit(req.body, ommitRole);
  const user = Object.assign(req.locals.user, updatedUser);
  user.save()
    .then(savedUser => res.json(savedUser.transform()))
    .catch(e => next(User.checkDuplicateEmail(e)));
};

/**
 * Get user list
 * @public
 */
exports.list = async (req, res, next) => {
  try {
    const users = await User.list(req.query);
    const transformedUsers = users.map(user => user.transform());
    res.json(transformedUsers);
  } catch (error) {
    next(error);
  }
};

/**
 * Delete user
 * @public
 */
// exports.remove = (req, res, next) => {
//   const {
//     user
//   } = req.locals;

//   user.remove()
//     .then(() => res.status(httpStatus.NO_CONTENT).end())
//     .catch(e => next(e));
// };

/*
Get user billing , websites data
*/
exports.billing = async (req, res, next) => {
  try {
    const user = await Users.findOne({
      _id: req.params.userId
    }).exec();
    const package = await Package.findOne({
      _id: user.packageId[0]
    }).limit(1).sort({ createdAt: -1 }).exec();
    const websiteCount = await Website.find({
      userId: req.params.userId
    }).select('_id autoRenew').exec();
    let queryData = {}
    queryData.siteId = { $in: websiteCount };

    if (user.packageUpdateDate) {
      queryData.createdAt = { $gte: new Date(user.packageUpdateDate) }
    }
    const sessions = await Session.countDocuments(queryData).exec();
    const payment = await Payment.findOne({ userId: req.params.userId, paymentType: 'package' })
      .sort({ createdAt: -1 })
      .exec();
    const lastPackage = await Payment.findOne({ userId: req.params.userId })
      .sort({ createdAt: -1 })
      .skip(1)
      .limit(1)
      .exec();
    let lastPack;
    let refillDate = '--';
    if (payment) {
      let duration = payment.duration;
      if (duration === 'monthly') {
        let startDate = moment(payment.createdAt);
        refillDate = startDate.add(1, 'months').format('YYYY-MM-DD');
      } else if (duration === 'annually') {
        let startDate = moment(payment.createdAt);
        refillDate = startDate.add(12, 'months').format('YYYY-MM-DD');
      }
    } else {
      let startDate = moment(user.createdAt);
      refillDate = startDate.add(1, 'months').format('YYYY-MM-DD');
    }
    let userConversionPackage = await UserConversionPackage.findOne({ $and: [{ userId: req.params.userId }, { isActive: true }] }).sort({ createdAt: -1 }).limit(1).exec();
    recordingLeft = (package.recordingPerMonth + user.refillCredit) - sessions;
    recordingLeft = (recordingLeft < 0) ? 0 : recordingLeft;
    user.recordingLeft = recordingLeft;
    console.log('##############');
    console.log(recordingLeft);
    await user.save();
    let resObj = {};
    resObj.pacakgeName = package.planeName;
    resObj.duration = package.planeType;
    resObj.packageId = package._id;
    resObj.website = websiteCount.length + "/" + package.websites;
    resObj.recordingLeft = recordingLeft;
    resObj.usPrice = package.usPrice;
    resObj.gbPrice = package.gbPrice;
    resObj.refillDate = refillDate;
    resObj.autoRenew = user.autoRenew;
    resObj.refillRecording = package.recordingPerMonth;
    resObj.refillData = user.refillCredit;
    resObj.expiryDate = userConversionPackage ? moment(userConversionPackage.createdAt).add(1, 'months') : '';
    res.status(200).send({
      code: '200',
      data: resObj,
      message: ""
    });
  } catch (error) {
    next(error);
  }
};

exports.checkUserPlan = async (req, res, next) => {
  try {
    const user = await Users.findById(req.params.userId).select('packageId').populate('packageId ').exec();
    const website = await Website.countDocuments({ userId: req.params.userId }).where({ status: 'active' }).exec();
    if (website >= user.packageId[0].websites) {
      return res.status(200).send({ code: 409, data: 'Your current plan expired. Please upgrade your plan.' });
    } else {
      return res.status(200).send({ code: 200, data: false });
    }
  } catch (error) {
    next(error);
  }
};

exports.remove = async (req, res, next) => {
  try {
    const userId = req.body.userId;
    const email = req.body.email;
    const reasons = req.body.reasons;
    const logType = req.body.logType;
    const userData = await Users.findById(req.body.userId).exec();
    // const user = await Users.deleteOne({ _id: userId }).exec();
    const user = await Users.updateOne({ _id: userId }, {
      $set: {
        isDeleted: true,
        active:false
      }
    })
    if (user !== undefined) {
      const accountInfo = new Logs({
        userId,
        email,
        reasons,
        logType
      });
      await accountInfo.save();
    }
    res.send({ code: 200, data: 'User account deleted successfully.' });
    await Users.deleteMany({ ownerId: userId }).exec();
    const websites = await Website.find({ userId: userId }).select('_id').exec();
    for (let index = 0; index < websites.length; index++) {
      let websiteId = [];
      websiteId.push(websites[index]._id)
      await Users.updateMany({ website: { $in: websiteId } },
        { $pull: { website: { $in: websiteId } } },
        { multi: true }
      )
      await Siteids.updateMany({ siteIds: { $in: websiteId } },
        { $pull: { siteIds: { $in: websiteId } } },
        { multi: true }
      )
      await Package.deleteOne({ email: userData.email }).exec();
      await Website.deleteOne({ _id: websites[index]._id }).exec();
      await Event.deleteMany({ siteId: websites[index]._id }).exec();
      await MetaData.deleteMany({ siteId: websites[index]._id }).exec();
      await Page.deleteMany({ websiteId: websites[index]._id }).exec();
      await Interaction.deleteMany({ siteId: websites[index]._id }).exec();
      await UserForm.deleteMany({ siteId: websites[index]._id }).exec();
      await Session.deleteMany({ siteId: websites[index]._id }).exec();
      await Recording.deleteMany({ siteId: websites[index]._id }).exec();
      await FieldTracking.deleteMany({ siteId: websites[index]._id }).exec();
      await Interaction.deleteMany({ siteId: websites[index]._id }).exec();
      const forms = await Form.find({ siteId: websites[index]._id }).exec();
      for (let i = 0; i < forms.length; i++) {
        await Form.deleteMany({ siteId: websites[index]._id }).exec();
        await FormField.deleteMany({ formId: forms[i]._id }).exec();
      }

    }
    await SiteConversion.deleteMany({ userId: req.body.userId }).exec();
    await UserConversionPackage.deleteOne({ userId: req.body.userId }).exec();

    const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'email-delete.html';
    consolidate.swig(templateFilePath, {

    }, (err, html) => {
      if (err) {
        console.log(err);
      } else {
        authProviders.send_email(userData.email, "We're sorry to see you go!", html);
      }
    });


    // return res.send({ code: 200, data: 'User account deleted successfully.' });
  } catch (error) {
    console.log(error)
    next(error);
  }
};

exports.deleteUserReport = async (req, res, next) => {
  try {
    const reportId = req.body.reportId;
    const reportCode = req.body.reportCode;
    await Report.deleteOne({ _id: reportId }).exec();
    await Session.deleteMany({ reportCode: reportCode }).exec();
    return res.send({ code: 200, data: 'Report deleted successfully.' });
  } catch (error) {
    next(error);
  }
};

exports.listAllUsers = async (req, res, next) => {
  try {
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 20);
    const count = await Users.count({ createdAt: { $gt: fromDate, $lt: toDate } }).exec();
    const users = await Users.find({ createdAt: { $gt: fromDate, $lt: toDate } })
      .populate('packageId', 'planeName planeType')
      .skip(offset)
      .limit(limit)
      .sort({ createdAt: -1 })
      .exec();
    return res.json({ code: 200, data: users, totalUser: count });
  } catch (error) {
    next(error);
  }
};

exports.feedback = async (req, res, next) => {
  try {
    const feedback = new Feedback({
      userId: req.body.user_id,
      answers: req.body.answers,
      comments: req.body.comments,
      email: req.body.email
    });
    const savedUser = await feedback.save();
    return res.status(200).json({ code: 200, data: 'Thanks for your feedback.' });
    // res.json(savedUser.transform());
  } catch (error) {
    next(User.checkDuplicateEmail(error));
  }
};

exports.subscription = async (req, res, next) => {
  try {
    const checkUserExistance = await Subscription.findOne({ email: req.body.email }).exec();
    if (checkUserExistance) {
      return res.status(409).json({ code: 409, message: 'You have already subscribed flopanda' });
    } else {
      const subscribe = new Subscription({
        email: req.body.email
      });
      await subscribe.save();
      return res.status(200).json({ code: 200, data: 'You have subscribed successfully flopanda' });
    }
  } catch (error) {
    next(error);
  }
}

exports.updateAccountInfo = async (req, res, next) => {
  try {
    paypal.configure({
      'mode': 'sandbox', //sandbox or live
      'openid_client_id': 'AQSLWfdSW-tflaUaFV2ke7dcrVVf3te_CUAZ3N81X3Dxx5FHxLwTjFdNwXFpY8zBsXjdkpH9zVaGQN-N',
      'openid_client_secret': 'EEaqBhZeD3i6yTP3oCTS1c97Z_7lCQGw5qhl6cYw-iZVDkRHJKP0VNb0e9_oXCdN1r8Oquv3ogG83Lgr',
      'openid_redirect_uri': DEV_DOMAIN + '/referral',
    });
    paypal.openIdConnect.authorizeUrl({ 'scope': 'openid profile email' });
    paypal.openIdConnect.tokeninfo.create(req.body.code, function (error, tokeninfo) {
      if (error) {
        return res.status(404).json({ code: 404, message: error });
      }
      paypal.openIdConnect.userinfo.get(tokeninfo.access_token, async function (error, userinfo) {
        if (error) {
          return res.status(404).json({ code: 404, message: error });
        }
        const checkUserExistance = await Users.findOne({ _id: req.body.userId }).exec();
        if (checkUserExistance) {
          checkUserExistance.accountName = userinfo.name;
          checkUserExistance.accountEmail = userinfo.email;
          if (checkUserExistance.paidAmount && checkUserExistance.paidAmount.length === 1 && checkUserExistance.paidAmount[0] === '') {
            checkUserExistance.paidAmount = []
          }
          const updatedUser = await checkUserExistance.save();
          return res.status(200).json({
            code: 200,
            data: [],
            message: "Your user has been updated"
          });
        } else {
          return res.status(404).json({ code: 404, message: 'No user found' });
        }
      });
    });
  } catch (error) {
    next(error);
  }
}

exports.disconnectPaypal = async (req, res, next) => {
  try {
    const checkUserExistance = await Users.findOne({ _id: req.body.userId }).exec();
    if (checkUserExistance) {
      checkUserExistance.accountName = '';
      checkUserExistance.accountEmail = '';
      if (checkUserExistance.paidAmount && checkUserExistance.paidAmount.length === 1 && checkUserExistance.paidAmount[0] === '') {
        checkUserExistance.paidAmount = []
      }
      const updatedUser = await checkUserExistance.save();
      return res.status(200).json({
        code: 200,
        data: [],
        message: "Your user has been updated"
      });
    } else {
      return res.status(404).json({ code: 404, message: 'No user found' });
    }
  } catch (error) {
    next(error);
  }
}

exports.getAccountInfo = async (req, res, next) => {
  try {
    const checkUserExistance = await Users.findOne({ _id: req.params.userId }).select('accountNumber accountName sortCode accountEmail').exec();
    if (checkUserExistance) {
      return res.status(200).json({
        code: 200,
        data: checkUserExistance,
        message: ""
      });

    } else {
      return res.status(404).json({ code: 404, message: 'No user found' });
    }
  } catch (error) {
    next(User.checkDuplicate(error));
  }
}

/*
 GET user Recording overall stats
 */
exports.getUserSitesInfo = async (req, res, next) => {
  try {
    let userId = req.body.userId;
    let userDetail = await Users.findOne({ _id: userId }).select('refillCredit _id packageId email').exec();
    let userPackage = await Package.findOne({ _id: userDetail.packageId.toString() }).exec();
    if (userPackage.planeName === 'Free') {
      return res.status(200).send({ code: 200, showDialog: false });
    }
    let userWebsites = await Website.find({ userId: userId }).select('_id').exec();
    let websiteArray = [];
    for (let index = 0; index < userWebsites.length; index++) {
      websiteArray.push(userWebsites[index]._id);
    }
    let recordings = await Session.aggregate([
      {
        '$match': {
          'site': {
            '$in': websiteArray
          }
        }
      }, {
        '$group': {
          '_id': 'null',
          'count': {
            '$sum': 1
          }
        }
      }
    ]).exec();
    let recordedSession = recordings.length > 0 ? recordings[0].count : 0;
    let showDialog = false;
    let totalQuota = userPackage.recordingPerMonth + userDetail.refillCredit - recordedSession;
    // totalQuota = 90;
    if (totalQuota <= 100) {
      showDialog = true;
      const link = `${BETADOMAIN}refferalLogin`;
      const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'refill-recording.html';
      consolidate.swig(templateFilePath, {
        linkCreated: link,
      }, (err, html) => {
        if (err) {
          console.log(err);
        } else {
          authProviders.send_email(userDetail.email, '[Attention Required] Account Recordings About to Run Out', html);
        }
      });
    }
    return res.status(200).send({ code: 200, showDialog: showDialog });
  } catch (error) {
    next(error);
  }
}

/**
 * Get User package and sms info
 */

exports.packageAndSmsInfo = async (req, res, next) => {
  try {
    let userId = req.body.userId;
    let user = await Users.findOne({ _id: userId }).select('packageId').exec();
    if (user) {
      let packageDetail = await Package.findOne({ _id: user.packageId[0] }).exec();
      if (packageDetail.planeName === 'Free') {
        return res.status(200).send({ code: 200, data: packageDetail.planeName });
      } else {
        let smsPurchasePackage = await UserConversionPackage.findOne({ userId: userId }).sort({ createdAt: -1 }).limit(1).exec();
        let expiryDate = moment(smsPurchasePackage.createdAt).add(1, 'months');
        return res.status(200).send({ code: 200, data: expiryDate });
      }

    } else {
      return res.status(200).send({ code: 200, err: 'No User exist with this ID.' });
    }
  } catch (error) {
    next(error);
  }
};


exports.checkCustomPackage = async (req, res, next) => {
  try {
    if (req.query.email) {
      const CustomPackage = await Package.findOne({ email: req.query.email, isCustomAllowed: false }).select('_id').exec();
      if (CustomPackage) {
        return res.status(200).json({
          code: 200,
          isCustomPackage: true,
          customPackage: CustomPackage
        });
      } else {
        return res.status(200).json({
          code: 200,
          isCustomPackage: false,
          customPackage: {}
        });
      }
    } else {
      return res.status(400).json({
        code: 400,
        message: 'No user found'
      });
    }

  } catch (error) {
    next(error);
  }
}


