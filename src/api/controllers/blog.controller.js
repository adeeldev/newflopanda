const Blog = require('../models/blog.model');
const Category = require('../models/category.model');
const Comment = require('../models/comment.model');

exports.get = async (req, res, next) => {
  try {
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 10);
    let query = req.query;
    if (req.query.name !==  undefined) {
      query.name = { $regex: new RegExp(req.query.name), $options: "si" }
    }
    let blogCount = await Blog.count({}).exec();
    let blogs = await Blog.find(query).sort({ createdAt: -1 }).skip(0).limit(limit).exec();
    return res.status(200).send({
      code: 200,
      data: blogs,
      total: blogCount
    });
  } catch (error) {
    next(error);
  }
};

exports.getAll = async (req, res, next) => {
  try {
    let blogCount = await Blog.count({}).exec();
    let blogs = await Blog.find({}).sort({ createdAt: -1 }).exec();
    return res.status(200).send({
      code: 200,
      data: blogs,
      total: blogCount
    });
  } catch (error) {
    next(error);
  }
};


exports.edit = async (req, res, next) => {
  try {
    if (req.body.blogId === undefined) {
      return res.send({ code: 404, msg: "No Blog found with this ID" });
    }
    let data = {
      name: req.body.name,
      author: req.body.author,
      content: req.body.content
    }
    if (req.file === undefined) {
      await Blog.findByIdAndUpdate(req.body.blogId, data, { new: true });
    } else {
      data.blogImage = req.file.location;
      await Blog.findByIdAndUpdate(req.body.blogId, data, { new: true });
    }
    return res.status(200).send({
      code: 200,
      data: 'Blog Created Successfully.'
    });
  } catch (error) {
    next(error);
  }
};

exports.create = async (req, res, next) => {
  try {
    let category = await Category.findOne({ _id: req.body.categoryId }).exec();
    let newBlog = new Blog({
      name: req.body.name,
      author: req.body.author, 
      content: req.body.content,
      blogImage: req.file.location,
      categoryId: req.body.categoryId,
      categoryName: category.name
    });
    newBlog.save();
    return res.status(200).send({
      code: 200,
      data: 'Blog Created Successfully.'
    });
  } catch (error) {
    next(error);
  }
};

exports.delete = async (req, res, next) => {
  try {
    let blogIds = req.body.blogIds;
    let blogs = await Blog.deleteMany({ _id:{ $in: blogIds } }).exec();
    return res.status(200).send({
      code: 200,
      data: 'Blog deleted successfully.'
    });
  } catch (error) {
    next(error);
  }
};

exports.getCategories = async (req, res, next) => {
  try {
    // let categories = await Category.find({}).sort({ createdAt: -1 }).select('name _id').exec();
    let categories = await Blog.aggregate([
      {
        '$group': {
          '_id': '$categoryId', 
          'total': {
            '$sum': 1
          }, 
          'categoryName': {
            '$first': '$categoryName'
          }
        }
      }, {
        '$sort': {
          'total': -1
        }
      }
    ]);
    return res.status(200).send({
      code: 200,
      data: categories
    });
  } catch (error) {
    next(error);
  }
};


exports.createCategory = async (req, res, next) => {
  try {
    let newCategory = new Category({
      name: req.body.name
    });
    newCategory.save();
    return res.status(200).send({
      code: 200,
      data: 'Category Created Successfully.'
    });
  } catch (error) {
    next(error);
  }
};

exports.blogByCategoryId = async (req, res, next) => {
  try {
    let categoryId = req.params.categoryId;
    let blogs = await Blog.find({ categoryId: categoryId}).sort({ createdAt: -1 }).exec();
    return res.status(200).send({
      code: 200,
      data: blogs,
      total: blogs.length
    });
  } catch (error) {
    next(error);
  }
};

exports.recentPosts = async (req, res, next) => { 
  try {
    let type = req.params.type;
    let blogs = [];
    if (type === 'recent') {
      blogs = await Blog.find({}).sort({ createdAt: -1 }).limit(3).exec();
    }
    if (type === 'popular') {
      blogs = await Blog.find({}).sort({ commentCount: -1 }).limit(3).exec();
    }
    return res.status(200).send({
      code: 200,
      data: blogs,
      total: blogs.length
    });
  } catch (error) {
    next(error);
  }
};


exports.getAllCategory = async (req, res, next) => {
  try {
    let blogs = await Category.find({}).sort({ createdAt: -1 }).exec();
    return res.status(200).send({
      code: 200,
      data: blogs,
      total: blogs.length
    });
  } catch (error) {
    next(error);
  }
};



exports.saveComment = async (req, res, next) => {
  try {
    let blog = await Blog.findOne({ _id: req.body.blogId }).exec();
    blog.commentCount = blog.commentCount + 1;
    await blog.save();
    let newComment = new Comment(req.body);
    newComment.save();
    return res.status(200).send({
      code: 200,
      data: 'Comment Created Successfully.'
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.getBlogComments = async (req, res, next) => {
  try {
    let blogComments = await Comment.find({ blogId: req.params.blogId }).sort({ createdAt: 1 }).exec();
    return res.status(200).send({
      code: 200,
      data: blogComments
    });
  } catch (error) {
    next(error);
  }
};


