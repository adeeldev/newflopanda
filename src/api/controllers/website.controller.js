const Website = require('../models/website.model');
const Page = require('../models/page.model');
const Recording = require('../models/recording.model');
const Session = require('../models/session.model');
const Recordingcounts = require('../models/recordingcounts.model');
const Users = require('../models/users.model');
const Logs = require('../models/log.model');
const Package = require('../models/packages.model');
const Report = require('../models/report.model');
const { handler: errorHandler } = require('../middlewares/error');
const fs = require('fs');
const path = require('path');
const consolidate = require('consolidate');
const SiteConversion = require('../models/siteConversionCron.model');
const authProviders = require('../services/emailProvider');
const MetaData = require('../models/metaData.model');
const Event = require('../models/event.model');
const Siteids = require('../models/siteid.model');
const UserConversionPackage = require('../models/userConversionPackage.model');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
  accessKeyId: 'AKIAI6FN6GALLBUTYK6Q',
  secretAccessKey: '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'
});
const Terser = require("terser");
const { DOMAIN, BETADOMAIN, BLOGDOMAIN, TRAKING_CODE_SCRIPT, env } = require('../../config/vars');
const moment = require('moment');

//Check if user is sending its own website.
async function checkWebsites(siteId = 0, userId = 0, owner = 1) {
  var siteIdsArray = [];

  if (!userId) {
    return false;
  }

  if (!siteId) {
    return false;
  }
  let currentUserSites = await Siteids.find({ userId: userId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
  if (currentUserSites.length) {
    return true;
  }
  else if (owner !== 1) {
    let currentUserOwnerId = await Users.findOne({ _id: userId }, { ownerId: 1 }).select('siteIds').exec();
    if (currentUserOwnerId && currentUserOwnerId.ownerId) {
      let currentUserSitesAgain = await Siteids.find({ userId: currentUserOwnerId.ownerId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
      if (currentUserSitesAgain.length) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
  // console.log("currentUserSitescurrentUserSitescurrentUserSitescurrentUserSites");
  // console.log(currentUserSites);
  // return false;

  // if (currentUserSites[0] && currentUserSites[0].siteIds) {
  //   currentUserSites[0].siteIds.forEach(function (site) {
  //     siteIdsArray.push(site);
  //   })
  // }
  // if (siteIdsArray.indexOf(siteId) > -1) {
  //   return true;
  // }
  // else {
  //   return false;
  // }
}

exports.getWebsitePages = async (req, res, next) => {
  try {
    const start = parseInt(req.params.start);
    const limit = parseInt(req.params.end);
    const siteId = req.params.siteId;
    var startingOffset = start * limit;
    const pages = await Page.find({
      websiteId: siteId
    }, {
      '_id': 1, 'name': 1
    }).skip(startingOffset).limit(limit).exec();

    const totalCount = await Page.count({
      websiteId: siteId
    }).exec();
    var hasMore = false;
    if (totalCount > ((start * limit) + limit)) {
      hasMore = true;
    }
    res.status(200).send({ code: 200, data: pages, pageListTotal: hasMore });
  }
  catch (error) {
    next(error);
  }
}

exports.encodeBase64 = async (req, res, next) => {
  try {
    const url = req.body.url;
    var request = require('request').defaults({ encoding: null });
    request.get(url, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        data = "data:" + response.headers["content-type"] + ";base64," + new Buffer(body).toString('base64');
        res.status(200).send({ code: 200, data: data, id: req.body.id, type: req.body.type });
      }
      else {
        res.status(200).send({ code: 200, data: 0, id: req.body.id, type: req.body.type });
      }
    });
  }
  catch (error) {
    next(error);
  }
}

exports.get = async (req, res, next) => {
  try {
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    let websites = [];
    let query = {}
    if (req.query.status) {
      // console.log(req.query.status);
    } else {
      query.status = 'active'
    }
    const demoSite = await Website.findOne({ isDemoSite: true }).select('isDemoSite name').exec();
    const user = await Users.findOne({ _id: req.params.userId }).select('demoWebsite roles ownerId website isCreateSite').exec();
    if (user.roles === '1') {
      websites = await Website.find({ userId: user._id }).select('isDemoSite name stopRecording showCompaign protocol isScriptInstall').where(query).exec();
    } else {
      websites = await Website.find({ _id: { $in: user.website } }).select('isDemoSite name stopRecording showCompaign protocol isScriptInstall').where(query).exec();
    }
    if (user.demoWebsite === true) {
      if (demoSite) {
        websites.push(demoSite);
      }
    }
    const sites = [];
    for (let index = 0; index < websites.length; index++) {
      const recordings = await Session.countDocuments({
        $and: [
          { siteId: websites[index]._id },
          {
            createdAt: {
              $gt: fromDate,
              $lt: toDate
            },
          },
          {
            type: { $ne: 'offline' }
          }
        ]
      }).exec();
      sites.push({ _id: websites[index]._id, websiteName: websites[index].name, recordingCount: recordings, userRole: 'admin', isDemoSite: websites[index].isDemoSite, stopRecording: websites[index].stopRecording, showCompaign: websites[index].showCompaign, protocol: websites[index].protocol, isScriptInstall: websites[index].isScriptInstall, isCreateSite: user.isCreateSite });
    }
    res.status(200).send({ code: 200, data: sites });
  } catch (error) {
    next(error);
  }
};



exports.findByName = async (req, res, next) => {
  try {
    const website = await Website.find({ name: req.params.siteName }).select('name').exec();
    if (website.length > 0) {
      return res.status(200).send({ code: 409, isExist: true });
    } else {
      return res.status(200).send({ code: 200, isExist: false });
    }
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};

exports.getSiteScript = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const siteId = req.params.siteId;
    const websites = await Website.findOne({ _id: siteId }).exec();
    if (websites) {
      res.status(200).send(websites.script);
    } else {
      res.status(200).send({ code: 404, data: 'Script not found.' });
    }
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};

exports.getPaymentScript = async (req, res, next) => {
  try {
    let paymentScript;
    if (env === 'production') {
      paymentScript = '<script type="text/javascript"> window._pfq = window._pfq || [];(function() {var pf = document.createElement("script");pf.type = "text/javascript"; pf.async = true;pf.src = "https://flopandascripts.s3.eu-west-2.amazonaws.com/assets/js/paymentScript.js";document.getElementsByTagName("head")[0].appendChild(pf);})();</script>';
    } else {
      paymentScript = '<script type="text/javascript"> window._pfq = window._pfq || [];(function() {var pf = document.createElement("script");pf.type = "text/javascript"; pf.async = true;pf.src = "https://flopandascripts.s3.eu-west-2.amazonaws.com/assets/js/devPaymentScript.js";document.getElementsByTagName("head")[0].appendChild(pf);})();</script>';
    }
    return res.status(200).send(paymentScript);
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};

exports.embeddedScript = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const siteId = req.params.siteId;
    const websites = await Website.findOne({ _id: siteId }).select('scriptHeader').exec();
    if (websites) {
      let script = '<script type="text/javascript"> window._pfq = window._pfq || [];(function() {var pf = document.createElement("script");pf.type = "text/javascript"; pf.async = true;pf.src = "//' + 'www.flopanda.com/v1/website/payment/script";document.getElementsByTagName("head")[0].appendChild(pf);})();</script>';
      websites.paymentScript = script;
      res.status(200).send({ code: 200, scriptHeader: websites });
    } else {
      res.status(200).send({ code: 404, data: 'Script not found.' });
    }
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};

exports.save = async (req, res, next) => {
  try {
    const website = new Website(req.body);
    const savedWeb = await website.save();
    let user = await Users.findOne({ _id: req.body.userId }).exec();
    user.website.push(savedWeb._id);
    user.isCreateSite = true;
    user.save();
    let script = '<script type="text/javascript"> window._pfq = window._pfq || [];(function() {var pf = document.createElement("script");pf.type = "text/javascript";pf.id = "' + website._id + '";pf.dataset.name ="flopandaScript";pf.async = true;pf.src ="https://flopandascripts.s3.eu-west-2.amazonaws.com/assets/js/' + TRAKING_CODE_SCRIPT + '";document.getElementsByTagName("head")[0].appendChild(pf);})();</script>';
    savedWeb.scriptHeader = script;
    savedWeb.domain.push(req.body.name);
    // const fileData = await readFile(website._id);
    savedWeb.paymentScript = await readPaymentScript();
    // savedWeb.script = 'var siteId = "' + website._id + '";\n' + fileData;
    const newSite = await website.save();

    var siteIdsArray = [];
    let currentUserSites = await Website.find({ userId: req.body.userId }).select('_id').exec();
    currentUserSites.forEach(function (site) {
      siteIdsArray.push(site._id.toHexString());
    });

    await Siteids.updateOne(
      {
        userId: req.body.userId
      },
      {
        siteIds: siteIdsArray,
        userId: req.body.userId
      },
      {
        upsert: true
      });

    res.status(200).send({ code: 200, data: { siteId: newSite._id, siteName: newSite.name, script: newSite.scriptHeader } });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};

// const websites = await Website.findOneAndUpdate({ _id: siteId }, req.body, { new: true }).exec();
// return res.status(200).send({ code: 200, data: websites.stopRecording, showCompaign: websites.showCompaign, siteLimit: limit });

exports.update = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const siteId = req.params.siteId;
    let user = await Website.findOne({ _id: siteId }).select('userId').exec();
    let activeSites = await Website.count({ $and: [{ userId: user.userId }, { stopRecording: false }] }).exec();
    let userData = await Users.findOne({ _id: user.userId }).select('packageId').exec();
    let userPackages = await Package.findOne({ _id: userData.packageId[0] }).select('websites').exec();
    let limit = false;
    if (req.body.stopRecording === true) {
      const websites = await Website.findOneAndUpdate({ _id: siteId }, req.body, { new: true }).exec();
      await SiteConversion.updateOne({ siteId: siteId }, { isActive: false });
      return res.status(200).send({ code: 200, data: websites.stopRecording, showCompaign: websites.showCompaign, siteLimit: limit });
    } else {
      await SiteConversion.updateOne({ siteId: siteId }, { isActive: true, notificationSentAt: moment() });
      if (activeSites >= userPackages.websites) {
        limit = true;
        return res.status(200).send({ code: 200, data: req.body.stopRecording, siteLimit: limit });
      } else {
        const websites = await Website.findOneAndUpdate({ _id: siteId }, req.body, { new: true }).exec();
        return res.status(200).send({ code: 200, data: websites.stopRecording, showCompaign: websites.showCompaign, siteLimit: limit });
      }
    }
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.delete = async (req, res, next) => {
  try {
    let userId = '';
    const email = req.body.email;
    const siteName = req.body.siteName;
    const logType = req.body.logType;
    let website = await Website.findOne({ _id: req.body.siteId }).select('name subDomain domainId userId').exec();
    if (website && website.subDomain) {
      let site = await Website.findOne({ _id: website.domainId }).exec();
      if (site !== null) {
        let index = site.domain.indexOf(website.name);
        if (index > -1) {
          site.domain.splice(index, 1)
          site.domain = site.domain;
          await site.save();
        }
      }
    }
    if (website !== undefined) {
      const accountInfo = new Logs({
        userId: req.body.userId,
        email,
        logType,
        siteName
      });
      const savedLogs = await accountInfo.save();
    }
    res.status(200).send({
      code: 200,
      data: [],
      message: "Website deleted successfully"
    });
    let websiteId = [];
    websiteId.push(req.body.siteId)
    await Users.updateMany({ website: { $in: websiteId } },
      { $pull: { website: { $in: websiteId } } },
      { multi: true }
    )
    await Siteids.updateMany({ siteIds: { $in: websiteId } },
      { $pull: { siteIds: { $in: websiteId } } },
      { multi: true }
    )
    await Page.getPageBySiteId({ websiteId: req.body.siteId });
    await Recording.deleteMany({ siteId: req.body.siteId }).exec();
    await Page.deleteMany({ websiteId: req.body.siteId }).exec();
    await Website.deleteOne({ _id: req.body.siteId }).exec();
    await Event.deleteMany({ siteId: req.body.siteId });
    await MetaData.deleteMany({ websiteId: req.body.siteId });
    let siteConversion = await SiteConversion.findOne({ siteId: req.body.siteId }).exec();
    if (siteConversion) {
      let numberAdded = 0;
      let emailAdded = siteConversion.email.length;
      userId = siteConversion.userId;
      for (let i = 0; i < siteConversion.phone.length; i++) {
        for (let j = 0; j < siteConversion.phone[i].phone.length; j++) {
          numberAdded += 1;
        }
      }
      let conversionPackage = await UserConversionPackage.findOne({ userId: userId }).exec();
      await SiteConversion.deleteOne({ siteId: req.body.siteId }).exec();
      conversionPackage.numberAdded -= numberAdded;
      conversionPackage.emailAdded -= emailAdded;
      await conversionPackage.save();
    }

  } catch (error) {
    next(errorHandler(error, req, res));
  }
};

exports.advancedSettings = async (req, res, next) => {
  try {
    const website = await Website.findById(req.body.siteId).exec();
    website.pageIdentifiers = req.body.pageIdentifiers;
    website.mergeURLs = req.body.mergeURLs;
    const savedWeb = await website.save();
    res.status(200).send({ code: 200, data: [], message: "" });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};

exports.editExcludeWhitelist = async (req, res, next) => {
  try {
    const website = await Website.findById(req.body.siteId).exec();
    website.excludedContent = req.body.excludedContent;
    website.whitelistedFields = req.body.whitelistedFields;
    const savedWeb = await website.save();
    res.status(200).send({ code: 200, data: [], message: "" });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};

exports.editSitePrivacy = async (req, res, next) => {
  try {
    const website = await Website.findById(req.body.siteId).exec();
    if (req.body.anonymizationReason !== undefined) {
      website.anonymizationReason = req.body.anonymizationReason;
      website.iPAnonymization = req.body.iPAnonymization;
    }
    if (req.body.trackingReason !== undefined) {
      website.trackingReason = req.body.trackingReason;
      website.honorTracking = req.body.honorTracking;
    }
    if (req.body.keystrokesReason !== undefined) {
      website.keystrokesReason = req.body.keystrokesReason;
      website.turnOffStickyKeys = req.body.turnOffStickyKeys;
    }
    if (req.body.checkApi == "all") {
      website.anonymizationReason = req.body.anonymizationReason;
      website.iPAnonymization = req.body.iPAnonymization;
      website.turnOffStickyKeys = req.body.turnOffStickyKeys;
    }
    const savedWeb = await website.save();
    res.status(200).send({ code: 200, data: [], message: "Setting saved successfully" });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};

exports.editSiteExcludedIP = async (req, res, next) => {
  try {
    const website = await Website.findById(req.body.siteId).exec();
    website.automaticallyExcludedIP = req.body.automaticallyExcludedIP;
    const savedWeb = await website.save();
    res.status(200).send({ code: 200, data: [], message: "Setting saved successfully" });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};

exports.editSiteSetting = async (req, res, next) => {
  try {
    const website = await Website.findById(req.body.siteId).exec();
    website.name = req.body.websiteName;
    website.domain = req.body.domainName;
    website.recordingRate = req.body.recordingRate;
    const savedWeb = await website.save();
    if (req.body.isRecordingRateChange === true) {
      await Recordingcounts.deleteOne({ _id: req.body.siteId }).exec();
    }
    res.status(200).send({ code: 200, data: [], message: "Setting saved successfully" });
  } catch (error) {
    next(error);
  }
};


exports.allowDownloadWebsiteIssueData = async (req, res, next) => {
  try {
    const website = await Website.findById(req.params.siteId).exec();
    website.isDownloadFormDropoff = req.body.isDownloadFormDropoff;
    website.isDownloadClickError = req.body.isDownloadClickError;
    website.isDownloadClickRage = req.body.isDownloadClickRage;
    await website.save();
    res.status(200).send({ code: 200, data: [], message: "Setting saved successfully" });
  } catch (error) {
    next(error);
  }
};

exports.downloadDataInfo = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const website = await Website.findById(req.params.siteId).select('-_id isDownloadFormDropoff isDownloadClickError isDownloadClickRage').exec();
    res.status(200).send({ code: 200, data: website, message: "Setting saved successfully" });
  } catch (error) {
    next(error);
  }
};

exports.getSiteData = async (req, res, next) => {
  try {
    const website = await Website.findById(req.params.siteId).select('-script -paymentScript').exec();
    const reports = await Report.find({ userId: req.params.userId }).sort({ createdAt: -1 }).exec();
    let script = '<script type="text/javascript"> window._pfq = window._pfq || [];(function() {var pf = document.createElement("script");pf.type = "text/javascript"; pf.async = true;pf.src = "//' + 'www.flopanda.com/v1/website/payment/script";document.getElementsByTagName("head")[0].appendChild(pf);})();</script>';
    website.paymentScript = script;
    return res.status(200).send({ code: 200, data: website, reports: reports });
  } catch (error) {
    console.log(error);
    next(error);
  }
};


// async function readFile() {
//   var jsonPath = path.join(__dirname, '../', 'uploadedFiles/', 'script.js');
//   return new Promise((resolve, reject) => {
//     fs.readFile(jsonPath, "utf8", (err, data) => {
//       if (err) reject(err);
//       else resolve(data);
//     });
//   });
// }  

async function readFile(siteId) {
  var jsonPath = path.join(__dirname, '../', 'uploadedFiles/', 'script.js');
  var scriptName = 'projects/' + siteId + '.js';
  return new Promise((resolve, reject) => {
    fs.readFile(jsonPath, "utf8", (err, data) => {
      if (err) reject(err);
      else {
        var params = {
          Bucket: 'flopandascripts',
          Key: scriptName,
          ContentType: 'text/javascript'
        };

        // data = 'var siteId = "' + siteId + '";\n' + data;
        var result = Terser.minify(data);
        if (result.error) {
          params.Body = data;
        }
        else {
          params.Body = result.code;
        }
        s3.upload(params, function (s3Err, data) {
          if (s3Err) {
            throw s3Err;
          }
          else {
            resolve(data);
          }
        });
      }
    });
  });
}


async function readPaymentScript() {
  var jsonPath = path.join(__dirname, '../', 'uploadedFiles/', 'paymentScript.js');
  return new Promise((resolve, reject) => {
    fs.readFile(jsonPath, "utf8", (err, data) => {
      if (err) reject(err);
      else resolve(data);
    });
  });
}

exports.getSourceFilter = async (req, res, next) => {
  try {
    // var currentUserSites = await checkWebsites(req.params.siteId, req.user);
    // if(!currentUserSites)
    // {
    //     return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    // }
    const siteId = req.params.siteId;
    let search = await Session.distinct("engine", { $and: [{ siteId: siteId }, { engine: { $ne: '' } }] });
    let websites = await Session.distinct("referer", { $and: [{ siteId: siteId }, { referer: { $ne: 'noReferrer' } }, { traficSource: { $ne: 'social' } }, { traficSource: { $ne: 'search' } }] });
    let social = await Session.distinct("network", { $and: [{ siteId: siteId }, { traficSource: 'social' }] });
    let checkCampaign = await Session.findOne({ siteId: siteId, utmSource: 'facebook' }).exec();
    let fbCamExist = false;
    if (checkCampaign !== null) {
      fbCamExist = true;
    }
    return res.status(200).send({ code: 200, social: social, search: search, isFbCamExist: fbCamExist, websites: websites });
  } catch (error) {
    next(error);
  }
};

exports.verifyInstallation = async (req, res, next) => {
  try {
    // var currentUserSites = await checkWebsites(req.params.siteId, req.user);
    // if(!currentUserSites)
    // {
    //     return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    // }
    let site = await Website.findById(req.params.siteId).select('isScriptInstall').exec();
    res.send({ code: 200, data: site });
  } catch (error) {
    next(error);
  }
};


// exports.verifyInstallation = async (req, res, next) => {
//   try {
//     let site = await Website.findById(req.params.siteId).select('isScriptInstall').exec();
//     res.send({ code: 200, data: site });
//   } catch (error) {
//     next(error);
//   }
// };

exports.sendTrackingCode = async (req, res, next) => {
  try {
    let site = await Website.findById(req.body.siteId).select('isScriptInstall').exec();
    if (site && site.isScriptInstall === false) {
      const loginLink = `${BETADOMAIN}refferalLogin`;
      const guideLink = `${BLOGDOMAIN}category/all-instillation-guides/`;
      const contactLink = `${DOMAIN}/contactUs`;
      let trackingCode = '<script type="text/javascript"> window._pfq = window._pfq || [];(function() {var pf = document.createElement("script");pf.type = "text/javascript";pf.id = "' + req.body.siteId + '";pf.dataset.name ="flopandaScript";pf.async = true;pf.src ="https://flopandascripts.s3.eu-west-2.amazonaws.com/assets/js/' + TRAKING_CODE_SCRIPT + '";document.getElementsByTagName("head")[0].appendChild(pf);})();</script>';
      const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'not-install-trackingcode.html';
      consolidate.swig(templateFilePath, {
        trackingCode
      }, (err, html) => {
        if (err) {
          console.log(err);
        } else {
          authProviders.send_email(req.body.email, '[Action Required] Start Tracking Your Website Visitors', html);
        }
      });
    }
    res.send({ code: 200, data: site });
  } catch (error) {
    next(error);
  }
};

exports.emailToInActiveSiteUser = async (req, res, next) => {
  try {
    let date = moment().subtract(1, 'days');
    let endDate = moment().subtract(1, 'days');
    endDate.add(24, 'hours');
    // 
    let inActiveUsers = await Website.find({ $and: [{ createdAt: { $gte: date, $lte: endDate } }, { isScriptInstall: false }] }).select('userId scriptHeader createdAt _id').exec();
    let userIds = [];
    let trackingCodeID = [];
    for (let index = 0; index < inActiveUsers.length; index++) {
      userIds.push(inActiveUsers[index].userId);
    }
    let users = await Users.find({ _id: { $in: userIds } }).select('email _id').exec();
    for (let index = 0; index < users.length; index++) {
      let siteId = '';

      let siteIndex = inActiveUsers.findIndex(x => x.userId.toString() === inActiveUsers[index].userId.toString())
      if (siteIndex > -1) {
        siteId = inActiveUsers[siteIndex]._id
        trackingCodeID.push({
          siteId: inActiveUsers[siteIndex]._id,
          email: users[index].email,
          id: users[index]._id,
        })
      }
      const contactLink = `${DOMAIN}/contactUs`;
      let trackingCode = '<script type="text/javascript"> window._pfq = window._pfq || [];(function() {var pf = document.createElement("script");pf.type = "text/javascript";pf.id = "' + siteId + '";pf.dataset.name ="flopandaScript";pf.async = true;pf.src ="https://flopandascripts.s3.eu-west-2.amazonaws.com/assets/js/' + TRAKING_CODE_SCRIPT + '";document.getElementsByTagName("head")[0].appendChild(pf);})();</script>';
      const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'install-trackingcode.html';
      consolidate.swig(templateFilePath, {
        contactLink,
        trackingCode
      }, (err, html) => {
        if (err) {
          console.log(err);
        } else {
          authProviders.send_email(users[index].email, ' [Action Needed] Let’s install your tracking code', html);
        }
      });
    }

    return res.send(users);
  } catch (error) {
    next(error);
  }
};

exports.emailTrackingCode = async (req, res, next) => {
  try {
    let site = await Website.findById(req.body.siteId).select('name').exec();
    if (req.body.siteId && req.body.siteId !== '' && site) {
      const websiteName = site.name;
      let trackingCode = '<script type="text/javascript"> window._pfq = window._pfq || [];(function() {var pf = document.createElement("script");pf.type = "text/javascript";pf.id = "' + req.body.siteId + '";pf.dataset.name ="flopandaScript";pf.async = true;pf.src ="https://flopandascripts.s3.eu-west-2.amazonaws.com/assets/js/' + TRAKING_CODE_SCRIPT + '";document.getElementsByTagName("head")[0].appendChild(pf);})();</script>';
      const paymentCode = '<script type="text/javascript"> window._pfq = window._pfq || [];(function() {var pf = document.createElement("script");pf.type = "text/javascript"; pf.async = true;pf.src = "//www.flopanda.com/v1/website/payment/script";document.getElementsByTagName("head")[0].appendChild(pf);})();</script>';
      const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'email-tracking-code.html';
      consolidate.swig(templateFilePath, {
        websiteName,
        trackingCode,
        paymentCode
      }, (err, html) => {
        if (err) {
          console.log(err);
          res.send({ code: 400, data: err });
        } else {
          authProviders.send_email(req.body.email, 'Verify your tracking code', html);
          res.send({ code: 200, data: site });
        }
      });
    } else {
      res.send({ code: 400, data: [], message: 'Site not found' });
    }

  } catch (error) {
    next(error);
  }
};

// cron.schedule('* * * * *', () => {
//   console.log('running a task every minute');
//   funnelCron();
// })
