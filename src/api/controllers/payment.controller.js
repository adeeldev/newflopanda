const Package = require('../models/packages.model');
const Payment = require('../models/payment.model');
const Users = require('../models/users.model');
const Referral = require('../models/referral.model');
const Website = require('../models/website.model');
const PackageDetailSchema = require('../models/packageDetail.model.js');
const UserConversionPackage = require('../models/userConversionPackage.model');
const SiteConversion = require('../models/siteConversionCron.model');
const { STRIPE_SECRET_KEY, mode, client_id, client_secret, DOMAIN, DEV_DOMAIN } = require('../../config/vars');
const { handler: errorHandler } = require('../middlewares/error');
const moment = require('moment');
var paypal = require('paypal-rest-sdk');
const cron = require('node-cron');
mongoose = require('mongoose');
const authProviders = require('../services/emailProvider');
const fs = require('fs');
const path = require('path');
const consolidate = require('consolidate');
const transactionProvider = require('../services/transactionProvider');
paypal.configure({
  'mode': 'live', //sandbox or live
  'client_id': 'AVCPJAbd1pIeJ9ghILUCsjdueCFjXUQScXuvKZXB-pdkKqYcnI8L6O9i37XvEk-gk1ByD1nIjQESFRot',
  'client_secret': 'EOXA1iN3ox4ADDT751CdPHEGzHDRPkWmnBWdBu0VFGD6mXt0RrfhkijW875h8abj1lx1QCz7LnVc9a2h',
});

// paypal.configure({
//   'mode': mode, //sandbox or live
//   'client_id': client_id,
//   'client_secret':  client_secret
// });


let packegeType = { 0: 'monthly', 1: 'annually' };
const configureStripe = require('stripe');
const stripe = configureStripe(STRIPE_SECRET_KEY);


exports.makePayment = async (req, res, next) => {
  try {
    const package = await Package.getPackageById({ packageId: req.body.packageId });
    let price = req.body.currency === 'GBP' ? package.gbPrice : package.usPrice;
    let months = 1;
    if (package.planeType === 'annually') {
      price = RoundOff(getDiscountedPrice(price, package.discount) * 12);
      months = 12;
    } else {
      price = getDiscountedPrice(price, package.discount);
    }
    price = price.toString();
    let paymentId = '';
    var create_payment_json = {
      "intent": "sale",
      "payer": {
        "payment_method": "paypal"
      },
      "redirect_urls": {
        "return_url": `${DEV_DOMAIN}/upgrade`,
        "cancel_url": `${DEV_DOMAIN}/upgrade`,
      },
      "transactions": [{
        "item_list": {
          "items": [{
            "name": "item",
            "sku": "item",
            "price": price,
            "currency": req.body.currency,
            "quantity": 1
          }]
        },
        "amount": {
          "currency": req.body.currency,
          "total": price
        },
        "description": "This is the payment description."
      }]
    };
    var approvUrl = '';
    paypal.payment.create(create_payment_json, function (error, payment) {
      if (error) {
        res.send(error.response);
      } else {
        paymentId = payment.id;
        for (var index = 0; index < payment.links.length; index++) {
          if (payment.links[index].rel === 'approval_url') {
            approvUrl = payment.links[index].href;
          }
        }
        PackageDetailSchema.create({
          packageId: req.body.packageId,
          paymentId: paymentId,
          userId: req.body.userId,
          websitesId: req.body.websitesId,
          isDowngrade: req.body.isDowngrade
        }, function (err, packageDetail) {
          Payment.create({
            userId: req.body.userId,
            packageDetailId: packageDetail._id,
            paymentId: paymentId,
            currency: req.body.currency,
            packageId: req.body.packageId,
            planeName: package.planeName,
            duration: package.planeType,
            country: req.body.country,
            price: price,
            paymentType: 'package',
            paymentMethod: 'paypal'
          }, function (err, payment) {
            res.status(200).send({
              code: 200,
              data: {
                url: approvUrl
              },
              message: ""
            });
          })
        });


      }
    });
  } catch (error) {
    next(error);
  }
};
exports.paypalResponse = async (req, res, next) => {
  try {
    const userData = await Users.findById(req.body.userId).exec();
    const previousPackage = await Package.findOne({ _id: userData.packageId[0] }).exec();
    let paymentId = req.body.paymentId;
    const payment = await Payment.findOne({
      paymentId: paymentId
    });
    var execute_payment_json = {
      "payer_id": req.body.payerId,
      "transactions": [{
        "amount": {
          "currency": payment.currency,
          "total": payment.price
        }
      }]
    };
    paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
      if (error) {
        console.log(error.response);
        throw error;
      } else {
        console.log("Get Payment Response");
        console.log(JSON.stringify(payment));
      }
    });
    const package = await Package.findOne({
      _id: payment.packageId
    }).exec();
    if (package.isCustom) {
      package.isCustomAllowed = true;
      await package.save();
    }
    const user = await Users.findOne({
      _id: req.body.userId
    }).exec();
    const referral = await Referral.findOne({ referralUserId: req.body.userId }).exec();
    const packageDetail = await PackageDetailSchema.findOne({
      paymentId: paymentId
    }).exec();
    if (packageDetail.isDowngrade) {
      await changePackage(payment.userId, packageDetail.websitesId)
    }
    user.packageId = [];
    user.packageId = payment.packageId;
    user.totalPayment += payment.price;
    if (user.referralPaidAmount) {
      user.referralPaidAmount += percentage(payment.price, 15);
    } else {
      user.referralPaidAmount = percentage(payment.price, 15);
    }
    user.packageUpdateDate = new Date().toISOString();
    user.usPriceCP = package.planeType === 'annually' ? percentage(RoundOff(getDiscountedPrice(package.usPrice, package.discount) * 12), 15) : percentage(getDiscountedPrice(package.usPrice, package.discount), 15);
    user.gbPriceCP = package.planeType === 'annually' ? percentage(RoundOff(getDiscountedPrice(package.gbPrice, package.discount) * 12), 15) : percentage(getDiscountedPrice(package.gbPrice, package.discount), 15);
    user.isPaid = true;
    let paidAmount = [];
    if (user.paidAmount && user.paidAmount.length > 0) {
      paidAmount = user.paidAmount;
      user.paidAmount = [];
      paidAmount.push({
        date: new Date().toISOString(),
        amount: percentage(payment.price, 15)
      })
      user.paidAmount = paidAmount;
    } else {
      paidAmount.push({
        date: new Date().toISOString(),
        amount: percentage(payment.price, 15)
      })
      user.paidAmount = paidAmount;
    }
    if (package.planeType === 'monthly') {
      user.billingDate = moment().add(1, 'months').format('YYYY-MM-DD');
      if (referral) {
        referral.billingDate = moment().add(1, 'months').format('YYYY-MM-DD');
        referral.accountType = 'Paid';
        referral.packageId = package._id;
        referral.packageName = package.planeName;
        referral.save();
      }
    } else if (package.planeType === 'annually') {
      user.billingDate = moment().add(12, 'months').format('YYYY-MM-DD');
      if (referral) {
        referral.billingDate = moment().add(12, 'months').format('YYYY-MM-DD');
        referral.accountType = 'Paid';
        referral.packageId = package._id;
        referral.packageName = package.planeName;
        referral.save();
      }
    }
    const savedUser = await user.save();
    payment.paymentSuccess = true;
    payment.planeName = package.planeName;
    payment.duration = package.planeType;
    const updatedPayment = await payment.save();

    var upgrade = "upgraded";
    if (packageDetail.isDowngrade) {
      upgrade = "downgraded";
    }
    if (package.planeName === previousPackage.planeName && package.planeType != previousPackage.planeType) {
      upgrade = "update";
    }
    if (user && user.referralId !== '') {
      let email = '';
      let price = percentage(payment.price, 15);
      let referralUser = await Users.findOne({ _id: userData.referralId }).select('accountEmail').exec();
      if (referralUser && referralUser.accountEmail) {
        email = referralUser.accountEmail;
      }
      if (email !== '') {
        transferReferralCommission(req.body.userId, email, price, payment.currency)
      }
    }
    transection(payment, userData, package, "PayPal");
    res.status(200).send({
      code: 200,
      data: {
        price: payment.price,
        packageType: package.planeType,
        plane: package.planeName,
        paymentId: paymentId,
        currency: payment.currency,
        date: payment.updatedAt
      },
      message: ""
    });
  } catch (error) {
    next(error);
  }
};

exports.paypalCancel = async (req, res, next) => {
  try {
    res.status(200).send({
      code: '200',
      data: [],
      message: ""
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

exports.paymentHistory = async (req, res, next) => {
  try {
    const payment = await Payment.getPaymentByUserId_Success({
      userId: req.params.userId,
      // paymentSuccess: true
    });
    // let transection = [];
    // payment.forEach(h => {
    //     let price = 0;
    //     if (h['packageId']['duration'] == packegeType[1]) {
    //         price = h['packageId']['price'] * 12;
    //     } else {
    //         price = h['packageId']['price'];
    //     }
    //     transection.push({
    //         plane: h['packageId']['planeName'],
    //         price: price,
    //         planeType: h['packageId']['duration'],
    //         paymentId: h['paymentId'],
    //         createdAt: moment(h.createdAt).calendar()
    //     })
    // })
    res.status(200).send({
      code: 200,
      data: payment,
      message: ""
    });
  } catch (error) {
    next(error);
    // return res.status(400).send({
    //   code: 400,
    //   data: [],
    //   message: error.message
    // });
  }
};

exports.paymentHistoryByAdmin = async (req, res, next) => {
  try {
    const payment = await Payment.getPaymentSuccess();
    let transection = [];
    payment.forEach(h => {
      let price = 0;
      if (h['packageId']['duration'] == packegeType[1]) {
        price = h['packageId']['price'] * 12;
      } else {
        price = h['packageId']['price'];
      }
      transection.push({
        plane: h['packageId']['planeName'],
        price: price,
        planeType: h['packageId']['duration'],
        paymentId: h['paymentId'],
        createdAt: moment(h.createdAt).calendar()
      })
    })
    res.status(200).send({
      code: '200',
      data: transection,
      message: ""
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};


exports.stipePayment = async (req, res, next) => {
  try {
    let { amount, currency, source, userId, packageId, cardName, country, zipCode, cardNumber, autoRenew, websitesId } = req.body;
    const userData = await Users.findById(userId).exec();
    const previousPackage = await Package.findOne({ _id: userData.packageId[0] }).exec();
    await Package.findOneAndUpdate({ _id: previousPackage._id }, { isCustomAllowed: false });
    const packageInfo = await Package.findOne({ _id: packageId }).exec();
    let price = req.body.currency === 'GBP' ? packageInfo.gbPrice.toFixed(2) : packageInfo.usPrice.toFixed(2);
    let billingDate = moment().format('YYYY-MM-DD');
    let usPrice = 0;
    let gbPrice = 0;
    if (packageInfo.planeType === 'annually') {
      price = RoundOff(getDiscountedPrice(price, packageInfo.discount) * 12);
      usPrice = percentage(RoundOff(getDiscountedPrice(packageInfo.usPrice, packageInfo.discount) * 12), 15);
      gbPrice = percentage(RoundOff(getDiscountedPrice(packageInfo.gbPrice, packageInfo.discount) * 12), 15);
      billingDate = moment().add(12, 'months').format('YYYY-MM-DD');
    } else {
      price = getDiscountedPrice(price, packageInfo.discount);
      usPrice = percentage(getDiscountedPrice(packageInfo.usPrice, packageInfo.discount), 15);
      gbPrice = percentage(getDiscountedPrice(packageInfo.gbPrice, packageInfo.discount), 15);
      billingDate = moment().add(1, 'months').format('YYYY-MM-DD');
    }
    let customerId;
    if (userData.customerId) {
      customerId = userData.customerId;
    } else {
      const customer = await stripe.customers.create({
        source: source,
        email: userData.email,
      });
      customerId = customer.id;
    }
    const charge = await stripe.charges.create({
      amount: Math.round(price * 100),
      currency: currency,
      customer: customerId,
      description: 'Flopanda Testing Sandbox Payment'
    });
    if (charge) {
      console.log(charge);
      let status = false;
      if (charge.status === 'succeeded') {
        status = true;
      }
      var payment = new Payment({
        paymentId: charge.id,
        userId: userId,
        packageId: packageInfo._id,
        paymentSuccess: status,
        price: price,
        currency: currency,
        duration: packageInfo.planeType,
        planeName: packageInfo.planeName,
        cardName: cardName,
        country: country,
        zipCode: zipCode,
        cardNumber: cardNumber,
        paymentType: 'package',
        paymentMethod: 'stripe'
      });
      const paymentInfo = await payment.save();
      if (packageInfo.isCustom) {
        packageInfo.isCustomAllowed = true;
        await packageInfo.save();
      }
      if (req.body.isDowngrade) {
        const packageDetail = await PackageDetailSchema.create({
          packageId: packageInfo._id,
          paymentId: charge.id,
          userId: userId,
          websitesId: req.body.websitesId,
          isDowngrade: req.body.isDowngrade
        });
        await changePackage(userId, req.body.websitesId)
      }
      let user = await Users.findOne({ _id: userId }).exec();
      user.totalPayment += parseInt(price);
      user.packageId = packageId;
      user.autoRenew = autoRenew;
      user.billingDate = billingDate;
      user.usPriceCP = usPrice;
      user.gbPriceCP = gbPrice;
      user.isPaid = true;
      user.packageUpdateDate = new Date().toISOString();
      user.customerId = customerId;
      let referral = await Referral.findOne({ referralUserId: userId }).exec();
      if (referral) {
        referral.accountType = 'Paid';
        referral.packageId = packageId;
        referral.packageName = packageInfo.planeName;
        referral.billingDate = billingDate;
        referral.save();
      }
      user.save();
      var upgrade = "upgraded";
      if (req.body.isDowngrade) {
        upgrade = "downgraded";
      }
      if (packageInfo.planeName === previousPackage.planeName && packageInfo.planeType != previousPackage.planeType) {
        upgrade = "update";
      }
      if (userData && userData.referralId !== '') {
        let email = '';
        let price = percentage(payment.price, 15);
        let referralUser = await Users.findOne({ _id: userData.referralId }).select('accountEmail').exec();
        if (referralUser && referralUser.accountEmail) {
          email = referralUser.accountEmail;
        }
        if (email !== '') {
          transferReferralCommission(userId, email, price, currency)
        }
      }
      paymentScript(payment, packageInfo, userData);
      GradePackage(upgrade, packageInfo, userData, payment);
      transection(paymentInfo, userData, packageInfo, "Stripe");
      return res.status(200).send({ code: 200, data: paymentInfo, charge });
    }
  } catch (error) {
    console.log(error);
    next(error);
  }
}

/** 
 * Stripe Refill payment
 */
exports.refillPayment = async (req, res, next) => {
  try {
    let userId = req.body.userId;
    let type = req.body.type;
    let userDetail = await Users.findOne({ _id: userId }).select('refillCredit _id packageId email customerId').exec();
    let packageDetail = await Package.findOne({ _id: req.body.packageId }).exec();
    var amount = req.body.amount;
    amount.toFixed(2);
    let currency = req.body.currency;
    if (req.body.token) {
      let { amount, currency, source, userId, packageId, country, autoRenew, websitesId } = req.body;
      let price = amount.toFixed(2);
      billingDate = moment().add(1, 'months').format('YYYY-MM-DD');
      const customer = await stripe.customers.create({
        source: req.body.token,
        email: userDetail.email
      });
      const charge = await stripe.charges.create({
        amount: Math.round(price * 100),
        currency: currency,
        customer: customer.id,
        // source: req.body.token,
        description: 'Flopanda Testing Sandbox Payment'
      });
      if (charge) {
        let status = false;
        if (charge.status === 'succeeded') {
          status = true;
          await Users.findOneAndUpdate({ _id: userId }, { customerId: customer.id });
        }
        userDetail.customerId = customer.id;
        await userDetail.save();
        var payment = new Payment({
          paymentId: charge.id,
          userId: userId,
          packageId: req.body.packageId,
          paymentSuccess: status,
          price: price,
          currency: currency,
          planeName: 'conversion',
          country: country,
          duration: 'monthly',
          paymentType: 'conversion',
          paymentMethod: 'stripe'
        });
        const paymentInfo = await payment.save();

        let userConversionDetail = await UserConversionPackage.findOne({ $and: [{ userId: userId }, { isActive: true }] }).exec();
        if (userConversionDetail) {
          userConversionDetail.isActive = false;
          await userConversionDetail.save();
        }

        var newPricePackageData = {
          paymentId: charge.id,
          userId: userId,
          paymentSuccess: status,
          packageId: req.body.packageId,
          maxSmsPerDay: req.body.maxSmsPerDay,
          toalUsersAllowed: req.body.toalUsersAllowed,
          isSmsService: req.body.isSmsService,
          maxEmailPerDay: req.body.maxEmailPerDay,
          country: req.body.cncountry,
          isActive: true
        }
        if (req.body.emailAdded !== undefined) {
          newPricePackageData.numberAdded = req.body.numberAdded;
          newPricePackageData.emailAdded = req.body.emailAdded;
          newPricePackageData.country = req.body.cncountry;
        }
        let newPricePackage = new UserConversionPackage(newPricePackageData);
        await newPricePackage.save();
        transection(paymentInfo, userDetail, { planeName: 'Conversion', planeType: 'monthly' }, "Stripe");
        return res.status(200).send({ code: 200, data: paymentInfo, charge });
      }
    } else {
      console.log('IN ELSE CONDITION ======   >>>>>>>>>>>>>>>>..   ');
      const charge = await stripe.charges.create({
        amount: Math.round(amount * 100),
        currency: currency,
        customer: userDetail.customerId,
      });
      if (charge) {
        let success = false;
        if (charge.status === 'succeeded') {
          success = true;
        }
        if (type === 'refill') {
          var payment = new Payment({
            paymentId: charge.id,
            userId: userId,
            paymentSuccess: success,
            planeName: packageDetail && packageDetail.planeName ? packageDetail.planeName : 'Refill',
            price: amount,
            currency: currency,
            paymentType: 'refill',
            paymentMethod: 'stripe',
            duration: 'monthly'
          });
          await payment.save();
          userDetail.refillCredit = userDetail.refillCredit + req.body.recording;
          await userDetail.save();
          let paymentInfo = await Payment.findOne({ paymentId: charge.id }).exec();
          let createdAt = moment(paymentInfo.createdAt).format('YYYY/MM/DD');
          let Currency = req.body.currency;
          let planeName = 'Refill';
          if (paymentInfo) {
            const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'Payment-Email-script.html';
            consolidate.swig(templateFilePath, {
              planeName,
              Currency,
              createdAt,
              amount
            }, (err, html) => {
              if (err) {
                console.log(err);
              } else {
                authProviders.send_email(userDetail.email, '[Attention Required] Account Recordings About to Run Out', html);
              }
            });
            transection(paymentInfo, userDetail, { planeName, planeType: 'monthly' }, "Stripe");
          }
        }
        if (type === 'conversion') {
          let userConversionDetail = await UserConversionPackage.findOne({ $and: [{ userId: userId }, { isActive: true }] }).exec();
          if (userConversionDetail) {
            if (req.body.toalUsersAllowed != userConversionDetail.toalUsersAllowed) {

              await UserConversionPackage.updateOne({ _id: userConversionDetail._id }, { isActive: false }, { new: true }).exec();

              let newPricePackage = new UserConversionPackage({
                paymentId: charge.id,
                userId: userId,
                paymentSuccess: success,
                packageId: req.body.packageId,
                maxSmsPerDay: req.body.maxSmsPerDay,
                toalUsersAllowed: req.body.toalUsersAllowed,
                emailAdded: req.body.emailAdded,
                numberAdded: req.body.numberAdded,
                isSmsService: req.body.isSmsService,
                maxEmailPerDay: req.body.maxEmailPerDay,
                country: req.body.cncountry,
                isActive: true
              });
              updatedPricePackage = await newPricePackage.save();



              //userConversionDetail.isActive = false;


            } else {
              if (req.body.cncountry) {
                userConversionDetail.country = req.body.cncountry;
              }
              userConversionDetail.isSmsService = req.body.isSmsService
              userConversionDetail.createdAt = moment().toISOString();
              await userConversionDetail.save();
            }
            var payment = new Payment({
              paymentId: charge.id,
              userId: userId,
              paymentSuccess: success,
              planeName: "Conversion",
              price: amount,
              currency: currency,
              paymentType: 'conversion',
              paymentMethod: 'stripe',
              duration: 'monthly'
            });
            const paymentInfo = await payment.save();
            transection(paymentInfo, userDetail, { planeName: "Conversion", planeType: 'monthly' }, "Stripe");
          } else {
            let newPricePackage = new UserConversionPackage({
              paymentId: charge.id,
              userId: userId,
              paymentSuccess: success,
              packageId: req.body.packageId,
              maxSmsPerDay: req.body.maxSmsPerDay,
              toalUsersAllowed: req.body.toalUsersAllowed,
              emailAdded: 0,
              numberAdded: 0,
              isSmsService: req.body.isSmsService,
              maxEmailPerDay: req.body.maxEmailPerDay,
              country: req.body.cncountry,
              isActive: true
            });
            await newPricePackage.save();
            var payment = new Payment({
              paymentId: charge.id,
              userId: userId,
              paymentSuccess: success,
              price: amount,
              currency: currency,
              planeName: "Conversion",
              paymentType: 'conversion',
              paymentMethod: 'stripe',
              duration: 'monthly'
            });
            const paymentInfo = await payment.save();
            transection(paymentInfo, userDetail, { planeName: "Conversion", planeType: 'monthly' }, "Stripe");
          }
        }
        return res.status(200).send({ code: 200, data: charge.status });
      }
    }
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.createRefill = async (req, res, next) => {
  try {
    let price = req.body.amount;
    let type = req.body.type;
    let recording = req.body.recording;
    price = price.toString();
    var return_url = `${DEV_DOMAIN}/accountSetting`
    var cancel_url = `${DEV_DOMAIN}/accountSetting`;
    var packageModificationType = 0;
    if (req.body.packageModificationType) {
      packageModificationType = req.body.packageModificationType;
    }
    if (req.body.siteId) {
      return_url = `${DEV_DOMAIN}/dashboard/${req.body.siteId}?show=false`
      cancel_url = `${DEV_DOMAIN}/dashboard/${req.body.siteId}?show=false`
    }
    if (type === 'refill') {
      var create_payment_json = {
        "intent": "sale",
        "payer": {
          "payment_method": "paypal"
        },
        "redirect_urls": {
          "return_url": `${DEV_DOMAIN}/dashboard/${req.body.siteId}?show=true`,
          "cancel_url": `${DEV_DOMAIN}/dashboard/${req.body.siteId}?show=true`
        },
        "transactions": [{
          "item_list": {
            "items": [{
              "name": "item",
              "sku": "item",
              "price": price,
              "currency": req.body.currency,
              "quantity": 1
            }]
          },
          "amount": {
            "currency": req.body.currency,
            "total": price
          },
          "description": "This is the payment description."
        }]
      };
      var approvUrl = '';
      paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
          res.send(error.response);
        } else {
          paymentId = payment.id;

          for (var index = 0; index < payment.links.length; index++) {
            if (payment.links[index].rel === 'approval_url') {
              approvUrl = payment.links[index].href;
            }
          }
          Payment.create({
            userId: req.body.userId,
            recording: req.body.recording,
            paymentId: paymentId,
            currency: req.body.currency,
            planeName: "Refill",
            country: req.body.country,
            paymentSuccess: false,
            price: price,
            paymentType: 'refill',
            paymentMethod: 'paypal'
          }, function (err, payment) {
            res.status(200).send({
              code: 200,
              data: {
                url: approvUrl
              },
              message: ""
            });
          })
        }
      });
    }
    if (type === 'conversion') {
      var create_payment_json = {
        "intent": "sale",
        "payer": {
          "payment_method": "paypal"
        },
        "redirect_urls": {
          "return_url": return_url,
          "cancel_url": cancel_url
        },
        "transactions": [{
          "item_list": {
            "items": [{
              "name": "item",
              "sku": "item",
              "price": price,
              "currency": req.body.currency,
              "quantity": 1
            }]
          },
          "amount": {
            "currency": req.body.currency,
            "total": price
          },
          "description": "This is the payment description."
        }]
      };
      var approvUrl = '';
      paypal.payment.create(create_payment_json, async function (error, payment) {
        if (error) {
          res.send(error.response);
        } else {
          paymentId = payment.id;
          for (var index = 0; index < payment.links.length; index++) {
            if (payment.links[index].rel === 'approval_url') {
              approvUrl = payment.links[index].href;
            }
          }
          if (req.body.refill === true) {
            var userconversionPkgHere = await UserConversionPackage.findOne({ $and: [{ userId: req.body.userId }, { isActive: true }] });
            userconversionPkgHere.createdAt = moment().toISOString();
            userconversionPkgHere.isSmsService = req.body.isSmsService;
            await userconversionPkgHere.save();
            Payment.create({
              paymentId: paymentId,
              userId: req.body.userId,
              paymentSuccess: false,
              price: req.body.amount,
              planeName: "Conversion",
              currency: req.body.currency,
              paymentType: 'conversion',
              paymentMethod: 'paypal',
              duration: 'monthly'
            }, function (err, payment) {
              res.status(200).send({
                code: 200,
                data: {
                  url: approvUrl
                },
                message: ""
              });
            })
            if (req.body.emailAdded !== undefined) {
              var updatedData = {};
              updatedData.toalUsersAllowed = req.body.toalUsersAllowed;
              updatedData.emailAdded = req.body.emailAdded;
              updatedData.numberAdded = req.body.numberAdded;
              updatedData.country = req.body.country;
              updatedData.allSiteConversions = req.body.allSiteConversions;
              var xyz = await UserConversionPackage.findOneAndUpdate({ $and: [{ userId: req.body.userId }, { isActive: true }] }, { updatedData: updatedData, paymentId: paymentId, upDownPaymentSuccess: false }, { new: true }).exec();
            }

          } else {
            Payment.create({
              paymentId: paymentId,
              userId: req.body.userId,
              paymentSuccess: false,
              price: req.body.amount,
              currency: req.body.currency,
              paymentType: 'conversion',
              paymentMethod: 'paypal',
              duration: 'monthly'
            }, function (err, payment) {
              if (err) console.log(err);
              // res.status(200).send({
              //   code: 200,
              //   data: { 
              //     url: approvUrl
              //   },
              //   message: ""
              // });
            })
            UserConversionPackage.create({
              paymentId: paymentId,
              userId: req.body.userId,
              packageId: req.body.packageId,
              maxSmsPerDay: req.body.maxSmsPerDay,
              maxEmailPerDay: req.body.maxEmailPerDay,
              toalUsersAllowed: req.body.toalUsersAllowed,
              isSmsService: req.body.isSmsService,
              country: req.body.country,
              paymentSuccess: false,
              isActive: false,
            }, function (err, payment) {
              res.status(200).send({
                code: 200,
                data: {
                  url: approvUrl
                },
                message: ""
              });
            })
          }
        }
      });
    }
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.refillPaypalResponse = async (req, res, next) => {
  try {
    let paymentId = req.body.paymentId;
    let type = req.body.type;
    if (type === 'refill') {
      let payment = await Payment.findOne({ paymentId: paymentId }).exec();
      payment.paymentSuccess = true;
      await payment.save();
      var execute_payment_json = {
        "payer_id": req.body.payerId,
        "transactions": [{
          "amount": {
            "currency": payment.currency,
            "total": payment.price
          }
        }]
      };
      paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
        if (error) {
          console.log(error.response);
          throw error;
        } else {
          console.log("Get Payment Response");
          console.log(JSON.stringify(payment));
        }
      });
      let user = await Users.findOne({ _id: payment.userId }).exec();
      user.refillCredit = user.refillCredit + payment.recording;
      await user.save();
      let planeName = 'Refill';
      let createdAt = moment(payment.createdAt).format('YYYY/MM/DD');
      let Currency = payment.currency;
      let amount = payment.price;
      const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'refill-recording.html';
      consolidate.swig(templateFilePath, {
        planeName,
        currency,
        createdAt,
        amount
      }, (err, html) => {
        if (err) {
          console.log(err);
        } else {
          authProviders.send_email(user.email, '[Attention Required] Account Recordings About to Run Out', html);
        }
      });
      transection(payment, user, { planeName: planeName, planeType: 'monthly' }, "PayPal");
    }
    if (type === 'conversion') {
      let payment = await Payment.findOne({ paymentId: paymentId }).exec();
      payment.paymentSuccess = true;
      await payment.save();
      var execute_payment_json = {
        "payer_id": req.body.payerId,
        "transactions": [{
            "amount": {
                "currency": payment.currency,
                "total": payment.price
            }
        }]
    };
    paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
        if (error) {
            console.log(error.response);
            throw error;
        } else {
            console.log("Get Payment Response");
            console.log(JSON.stringify(payment));
        }
    });
      let user = await Users.findOne({ _id: payment.userId }).exec();
      let conversionPayment = await UserConversionPackage.findOne({ paymentId: paymentId }).exec();
      conversionPayment.isActive = true;
      conversionPayment.paymentSuccess = true;
      if (req.body.upDown) {
        conversionPayment.paymentSuccess = true;
        conversionPayment.toalUsersAllowed = conversionPayment.updatedData.toalUsersAllowed;
        conversionPayment.emailAdded = conversionPayment.updatedData.emailAdded;
        conversionPayment.numberAdded = conversionPayment.updatedData.numberAdded;
        conversionPayment.country = conversionPayment.updatedData.country;
      }
      await conversionPayment.save();
      if (req.body.upDown) {
        if (conversionPayment.updatedData && conversionPayment.updatedData.allSiteConversions) {
          for (var i = 0; i < conversionPayment.updatedData.allSiteConversions.length; i++) {
            await SiteConversion.findOneAndUpdate({ _id: conversionPayment.updatedData.allSiteConversions[i]._id }, { email: conversionPayment.updatedData.allSiteConversions[i].email, phone: conversionPayment.updatedData.allSiteConversions[i].phone }).exec();
            conversionPayment.updatedData = {};
            await conversionPayment.save();
          }
        }
      }
      transection(payment, user, { planeName: "Conversion", planeType: 'monthly' }, "PayPal");
    }
    return res.send({ code: 200, data: 'success' });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

// exports.changePackage = async (req, res, next) => {
//   try {
//     let { userId, websiteId } = req.body;
//     const inactive = await Website.updateMany({ _id: { $exists: true, $nin: websiteId }, userId: userId },
//       { status: "inactive" },
//       { upsert: true, new: true })
//     return res.send({ code: 200, msg: 'success' });
//   } catch (error) {
//     next(error);
//   }
// }
async function transferReferralCommission(userId, email, price, currency) {
  var sender_batch_id = Math.random().toString(36).substring(9);
  var create_payout_json = {
    "sender_batch_header": {
      "sender_batch_id": sender_batch_id,
      "email_subject": "You have a payment from flopanda"
    },
    "items": [
      {
        "recipient_type": "EMAIL",
        "amount": {
          "value": price.toFixed(2),
          "currency": currency
        },
        "receiver": email,//"flopanda90@gmail.com"
        "note": "Thank you."
        // "sender_item_id": "item_3"
      }
    ]
  };
  var sync_mode = 'false';
  paypal.payout.create(create_payout_json, sync_mode, function (error, payout) {
    if (error) {
      console.log(error.response);
    } else {
      setTimeout(() => {
        verifyReferralPayoutStatus(payout.batch_header.payout_batch_id, userId, email, price)
      }, 10000)
    }
  });
}
async function verifyReferralPayoutStatus(payoutId, userId, email, price) {
  // var payoutId = req.params.id;
  paypal.payout.get(payoutId, async function (error, payout) {
    if (payout.items && payout.items[0].errors) {
      // console.log('error');

      // return res.send({ code: 200, msg: error.response });
      // throw error;
    } else {
      const user = await Users.findOne({ _id: userId }).exec();
      // console.log(user)
      if (user) {
        if (user.referralPaidAmount) {
          user.referralPaidAmount += price;
        } else {
          user.referralPaidAmount = price;
        }
        let paidAmount = [];
        if (user.paidAmount && user.paidAmount.length > 0) {
          paidAmount = user.paidAmount;
          user.paidAmount = [];
          paidAmount.push({
            date: new Date().toISOString(),
            amount: price
          })
          user.paidAmount = paidAmount;
        } else {
          paidAmount.push({
            transactionId: payout.items[0].transaction_id,
            batchId: payoutId,
            currency: payout.items[0].payout_item_fee.currency,
            transactionId: payout.items[0].transaction_id,
            date: new Date().toISOString(),
            amount: price
          })
          user.paidAmount = paidAmount;
        }
        await user.save();
      }
    }
  });
}

async function paymentScript(payment, packageInfo, userData) {
  var Currency = "";
  if (payment.currency == "USD") {
    Currency = "$";
  } else {
    Currency = "£";
  }
  var createdAt = moment(payment.createdAt).format('YYYY/MM/DD');
  const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'Payment-Email-script.html';
  consolidate.swig(templateFilePath, {
    payment: payment,
    packageInfo: packageInfo,
    userData: userData,
    createdAt: createdAt,
    Currency: Currency
  }, (err, html) => {
    if (err) {
      console.log(err);
    } else {
      authProviders.send_email(userData.email, 'Thank You for your payment', html);
    }
  });
}



async function GradePackage(upgrade, packageInfo, userData, payment) {
  var Currency = "";
  if (payment.currency == "USD") {
    Currency = "$";
  } else {
    Currency = "£";
  }
  var subject = "";
  if (upgrade == "upgraded") {
    subject = 'Your upgraded FloPanda account'
  } else {
    subject = 'Your downgraded FloPanda account'
  }
  if (upgrade == "update") {
    subject = 'Your updated FloPanda account'
  }

  const templateFilePath1 = path.join(__dirname, '../uploadedFiles/') + 'Email-upgrade-downgrade.html';
  consolidate.swig(templateFilePath1, {
    upgrade: upgrade,
    packageInfo: packageInfo,
    userData: userData,
    payment: payment,
    Currency: Currency,
    subject: subject
  }, (err, html) => {
    if (err) {
      console.log(err);
    } else {
      console.log(subject);
      authProviders.send_email(userData.email, subject, html);
    }
  });
}



async function transection(payment, userData, packageInfo, paymentType) {
  var createdAt = moment(payment.createdAt).format('YYYY/MM/DD');
  var Currency = "";
  if (payment.currency == "USD") {
    Currency = "$";
  } else {
    Currency = "£";
  }
  const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'invoice.html';
  consolidate.swig(templateFilePath, {
    payment: payment,
    Currency: Currency,
    createdAt: createdAt,
    userData: userData,
    paymentType: paymentType,
    packageInfo: packageInfo
  }, (err, html) => {
    if (err) {
      console.log(err);
    } else {
      var path = transactionProvider.transectionDoc(html, payment);
    }
  });
}




async function changePackage(userId, websiteId, ) {
  try {
    const inactive = await Website.updateMany({
      _id: { $exists: true, $in: websiteId },
      userId: userId
    },
      // { status: "inactive", stopRecording : true })
      { stopRecording: true })
    const active = await Website.updateMany({
      _id: { $exists: true, $nin: websiteId },
      userId: userId
    },
      // { status: "active",stopRecording : false })
      { stopRecording: false })
    return true;
  } catch (error) {
    return error;
  }
}



async function makeAutoPayment() {
  try {
    const user = await Users.find({}).populate('paymentId packageId').exec();
    for (let i = 0; i < user.length; i++) {
      var du;
      if (user[i].packageId[0].planeType === 'monthly') {
        du = 1;
      } else { du = 12 }
      var m = du;
      var dt = moment(user[i].paymentId ? user[i].paymentId.createdAt : null).format('YYYY-MM-DD');
      if (moment(dt).add(m, 'months').format('YYYY-MM-DD') < moment().format('YYYY-MM-DD')) {
        if (user[i].paymentId.autoRenew || user[i].packageId[0]._id !== null || user[i].packageId[0]._id !== undefined) {
          const customer = await stripe.customers.create({
            email: user[i].email
          });
          const source = await stripe.customers.createSource(customer.id, {
            source: 'tok_visa'
          });
          const charges = await stripe.charges.create({
            amount: user[i].paymentId.price * 100,
            currency: 'usd',
            customer: source.customer
          });
          if (charges.status === 'succeeded') {
            let payment = new Payment({
              paymentId: charges.id,
              userId: user[i],
              packageId: user[i].packageId[0]._id,
              paymentSuccess: true,
              currency: 'USD',
              price: user[i].paymentId.price,
              duration: user[i].packageId[0].planeType,
              planeName: user[i].paymentId.planeName,
              cardName: user[i].paymentId.cardName,
              country: user[i].paymentId.country,
              zipCode: user[i].paymentId.zipCode,
              cardNumber: user[i].paymentId.cardNumber
            })
            var paymentInfo = await payment.save();
            await Users.findOneAndUpdate({ _id: user[i]._id }, { packageId: paymentInfo.packageId, paymentId: paymentInfo._id }).exec();
          }
        }
      }
    }
    return true;
  } catch (error) {
    return error;
  }
}

function percentage(num, per) {
  return (num / 100) * per;
}
function getDiscountedPrice(price, discount) {
  var numVal1 = Number(price);
  var numVal2 = Number(discount) / 100;
  var totalValue = numVal1 - (numVal1 * numVal2)
  return totalValue.toFixed(2);
}
function RoundOff(price) {
  let str = price.toFixed(2).toString()
  if (str.length > 3 && str.substr(str.length - 3) === ".00") {
    return price;
  } else {
    return price.toFixed(2);
  }
}
// cron.schedule('*/1 * * * *', function () {
//   makeAutoPayment();
// });