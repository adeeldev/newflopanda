const Survey = require('../models/survey.model');
const Feedback = require('../models/feedback.model');

exports.get = async (req, res, next) => {
  try {
    let survey = await Survey.find({}).sort({ createdAt: -1 }).select('-createdAt -updatedAt -__v -value').exec();
    return res.status(200).send({
      code: 200,
      data: survey
    });
  } catch (error) {
    next(error);
  }
};

exports.getAll = async (req, res, next) => {
  try {
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 20);
    let data = {};
    if (req.query.name) {
      data.name = { $regex: new RegExp(req.query.name), $options: "i" };
    }
    if (req.query.email !== undefined) {
      data.email = req.query.email;
    }
    if (req.query.from !== undefined && req.query.to !== undefined) {

      data.createdAt = { $gt: new Date(req.query.from), $lt: new Date(req.query.to) }
    }
    const totalFeedBackRecord = await Feedback.count({
      $and: [
        data
      ]
    }).exec();
    let feedbacks = await Feedback.find({
      $and: [
        data
      ]
    }).skip(offset).limit(limit).sort({ createdAt: -1 }).select('-createdAt -updatedAt -__v -value').exec();
    return res.status(200).send({
      code: 200,
      data: feedbacks,
      totalFeedBackRecord: totalFeedBackRecord
    });
  } catch (error) {
    next(error);
  }
};


exports.edit = async (req, res, next) => {
  try {
    let survey = await Survey.findOne({ _id: req.body.id }).exec();
    survey.question = req.body.question;
    survey.name = req.body.name;
    await survey.save();
    // await Survey.deleteMany({});
    // let newBlog = await Survey.insertMany(req.body.survey);
    return res.status(200).send({
      code: 200,
      data: 'Question updated Successfully.'
    });
  } catch (error) {
    next(error);
  }
};

exports.create = async (req, res, next) => {
  try {
    await Survey.create(req.body);
    return res.status(200).send({
      code: 200,
      data: 'Survey Created Successfully.'
    });
  } catch (error) {
    next(error);
  }
};

exports.delete = async (req, res, next) => {
  try {
    let blogs = await Survey.findOneAndDelete({ _id: req.body.packageId }).exec();
    return res.status(200).send({
      code: 200,
      data: 'Survey deleted successfully.'
    });
  } catch (error) {
    next(error);
  }
};






