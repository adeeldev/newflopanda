const Form = require('../models/form.model');
const FormField = require('../models/formField.model');
const UserForm = require('../models/userForm.model');
const Recording = require('../models/recording.model');
const FieldTracking = require('../models/fieldTracking.model');
const Interaction = require('../models/interactions.model');
const Website = require('../models/website.model');
const Page = require('../models/page.model');
const Session = require('../models/session.model');
const FieldReport = require('../models/fieldReport.model');
const FormStats = require('../models/formStats.model');
const SiteConversion = require('../models/siteConversionCron.model');
const UserConversionPackage = require('../models/userConversionPackage.model');
const Payment = require('../models/payment.model');
const ConversionPrice = require('../models/convertionPrice.model');
const { _ } = require('underscore');
const httpStatus = require('http-status');
const moment = require('moment');
const inbound = require('inbound');
const request = require('request');
const authProviders = require('../services/emailProvider');
const sms = require('../services/smsProvider');
const Siteids = require('../models/siteid.model');
const Users = require('../models/users.model');
const mongoose = require('mongoose');
const AWS = require('aws-sdk');
var CryptoJS = require("crypto-js");
const s3 = new AWS.S3({
  accessKeyId: 'AKIAI6FN6GALLBUTYK6Q',
  secretAccessKey: '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'
});
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const { MONGO_DB_URI } = require('../../config/vars');

var count = 0;

/**
 * Create new form
 * @public
 */

//Check if user is sending its own website.
async function checkWebsites(siteId = 0, userId = 0, owner = 1) {
  var siteIdsArray = [];

  if (!userId) {
    return false;
  }

  if (!siteId) {
    return false;
  }
  let currentUserSites = await Siteids.find({ userId: userId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
  if (currentUserSites.length) {
    return true;
  }
  else if (owner !== 1) {
    let currentUserOwnerId = await Users.findOne({ _id: userId }, { ownerId: 1 }).select('siteIds').exec();
    if (currentUserOwnerId && currentUserOwnerId.ownerId) {
      let currentUserSitesAgain = await Siteids.find({ userId: currentUserOwnerId.ownerId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
      if (currentUserSitesAgain.length) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
}

exports.pageForm = async (req, res, next) => {
  try {
    const pageId = req.params.pageId;
    const formsData = [];
    const forms = await Form.find({
      pageId: pageId
    }).exec();
    for (let index = 0; index < forms.length; index++) {
      const fields = await FormField.find({
        formId: forms[index]._id
      }).sort({
        fieldIndex: 1
      }).select('-__v -form -updatedAt -pageName -_id -createdAt').exec();
      formsData.push({
        form: forms[index].formName,
        formId: forms[index]._id,
        index: forms[index].index,
        fields: fields
      });
    }
    res.status(200).send({
      code: 200,
      data: formsData
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

exports.formsByPageId = async (req, res, next) => {
  try {
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 20);
    const siteId = req.params.siteId;
    const pages = await Page.find({$or:[{ websiteId: siteId },{_id:req.params.pageId}]}).skip(offset).limit(limit).select('name _id').exec();
    const totalPages= await Page.count({websiteId: siteId}).exec();
    const forms = await Form.find({ siteId: siteId }).select('formName pageId _id index pageName').exec();
    res.status(200).send({
      code: 200,
      data: {
        pages: pages,
        totalPages:totalPages,
        forms: forms
      }
    });
  } catch (error) {
    next(error);
  }
};

exports.createUserForm = async (req, res) => {
  try {
    const isExist = await UserForm.findOne({
      formId: req.body.formId
    }).exec();
    if (isExist) {
      return res.status(200).send({
        code: 409,
        msg: 'Form Already Exists.'
      });
    }
    const forms = await Form.findOneAndUpdate({
      $and: [{
        siteId: req.body.siteId
      }, {
        _id: req.body.formId
      }, {
        index: req.body.index
      }]
    }, {
      $set: {
        formAlias: req.body.name
      }
    }, {
      new: true
    }).exec();
    const fields = req.body.fields;
    for (let field = 0; field < fields.length; field++) {
      const element = fields[field];
      await FormField.findOneAndUpdate({
        $and: [{
          formId: req.body.formId
        },
        {
          fieldIndex: fields[field].fieldIndex
        }
        ]
      }, {
        $set: {
          fieldAlias: fields[field].fieldName
        }
      }, {
        new: true
      }).exec();
    }
    const userForm = new UserForm({
      siteId: req.body.siteId,
      formId: req.body.formId,
      formName: req.body.name,
      index: req.body.index,
      pageName: req.body.pageName,
      siteId: req.body.siteId,
      fields: fields
    });
    await userForm.save();
    res.status(200).send({
      code: 200,
      msg: 'Form Added successfully.'
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

exports.editForm = async (req, res, next) => {
  try {
    const formId = req.body.formId;
    const siteId = req.body.siteId;
    const fields = req.body.fields;
    const name = req.body.formName;
    const userForm = await UserForm.findOne({
      _id: formId
    }).exec();
    userForm.formName = name;
    userForm.fields = [];
    userForm.fields = fields;
    userForm.save();
    return res.send({
      code: 200,
      data: 'Form updated successfully.'
    });
  } catch (error) {
    next(error);
  }
};

exports.delete = async (req, res, next) => {
  try {
    const formId = req.body.formId;
    const userForm = await UserForm.findOneAndDelete({
      _id: formId
    }).exec();
    return res.send({
      code: 200,
      data: 'Form Deleted successfully.'
    });
  } catch (error) {
    next(error);
  }
};

exports.editFormData = async (req, res, next) => {
  try {
    const formId = req.body.formId;
    const editFieldsData = await UserForm.findOne({
      _id: formId
    }).exec();
    if (editFieldsData.fields.length > 0) {
      return res.send({
        code: 200,
        formId: formId,
        pageName: editFieldsData.pageName,
        pageId: editFieldsData.fields[0].pageId,
        fields: editFieldsData.fields
      });
    } else {
      return res.send({
        code: 200,
        formId: formId,
        pageName: editFieldsData.pageName,
        pageId: "",
        fields: []
      });
    }

  } catch (error) {
    next(error);
  }
}

exports.fieldTracking = async (req, res, next) => {
  try {
    const pageId = req.body.pageId;
    const index = req.body.index;
    const fields = await UserForm.find({
      pageId: pageId
    }).sort({
      createdAt: 1
    }).exec();
    return res.send(fields);
  } catch (error) {
    next(error);
  }
};


exports.formIndex = async (req, res, next) => {
  try {
    const pageName = req.body.pageName;
    const siteId = req.body.siteId;
    const form = await Form.findOne({
      $and: [{
        pageName: pageName
      }, {
        siteId: siteId
      }]
    }).select('index').exec();
    if (typeof form.index === 'undefined') {
      res.status(200).send({
        code: 404,
        data: 404
      });
    } else {
      res.status(200).send({
        code: 200,
        data: form.index
      });
    }
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

exports.formTrackData = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    const siteId = req.params.siteId;
    const queryData = req.query;
    queryData.createdAt = {
      $gte: fromDate,
      $lte: toDate
    };
    delete queryData.duration;
    delete queryData.pageCount;
    queryData.siteId = siteId;
    if (queryData.clickRage !== undefined) {
      if (req.query.clickRage === 'true') {
        queryData.clickRage = { $nin: ["false", "0"] };
      }
    }
    const forms = await UserForm.find({
      siteId: siteId
    }).select('-fieldType -fieldName -fieldId -fieldTagName').exec();
    const data = [];
    if (forms.length > 0) {
      for (let index = 0; index < forms.length; index++) {
        queryData.pageName = forms[index].pageName;
        delete queryData.index;
        const views = await Recording.countDocuments({
          $and: [queryData]
        }).exec();
        const summary = await Interaction.aggregate([{
          $match: {
            $and: [queryData]
          },
        },
        {
          $project: {
            interact: 1,
            pageName: 1,
            conversion: 1
          },
        },
        {
          $group: {
            _id: {
              pageName: '$pageName',
            },
            interact: {
              $sum: '$interact'
            },
            conversion: {
              $sum: '$conversion'
            },
            total: {
              $sum: 1
            }
          },
        },
        ]).exec();
        const fieldss = [];
        const fieldName = forms[index].fields;
        for (let j = 0; j < fieldName.length; j++) {
          queryData.index = fieldName[j].fieldIndex;
          const fieldSummary = await FieldTracking.aggregate([{
            $match: {
              $and: [queryData]
            }
          },
          {
            $sort: {
              index: 1
            }
          },
          {
            $group: {
              _id: '$fieldName',

              interact: {
                $sum: '$interact'
              },
              conversion: {
                $sum: '$conversion'
              },
              dropOff: {
                $sum: '$dropOff'
              },
              interactTime: {
                $avg: '$interactTime'
              },
              total: {
                $sum: 1
              },
            },
          },
          ]).exec();
          if (fieldSummary.length === 0) {
            let fieldData = {
              _id: fieldName[j].fieldName,
              interact: 0,
              conversion: 0,
              dropOff: 0,
              interactTime: 0,
              total: 0
            }
            fieldData.name = fieldName[j].fieldName;
            fieldss.push(fieldData);
          } else {
            fieldSummary[0].name = fieldName[j].fieldName;
            fieldss.push(fieldSummary[0]);
          }
        }
        let interact = 0;
        let conversion = 0;
        if (typeof summary[0] !== 'undefined') {
          interact = summary[0].interact;
        }
        if (typeof summary[0] !== 'undefined') {
          conversion = summary[0].conversion;
        }
        forms[index].formName = forms[index].formName;
        forms[index].visitttt = views;
        if (summary.length > 0) {
          data.push({
            formName: forms[index].formName,
            fields: fieldss,
            visit: views,
            interact: interact,
            conversion: conversion,
            total: summary.total,
            formId: forms[index]._id
          });
        } else {
          data.push({
            formName: forms[index].formName,
            fields: [],
            visit: views,
            interact: 0,
            conversion: 0,
            total: 0,
            formId: forms[index]._id
          });
        }
      }
      return res.send({
        code: 200,
        summary: data
      });
    } else {
      return res.send({
        code: 200,
        summary: []
      });
    }
  } catch (error) {
    next(error);
  }
};

exports.siteTrackData = async (req, res, next) => {
  try {
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    const siteId = req.params.siteId;
    const queryData = req.query;
    queryData.site = siteId;
    let to;
    if (req.query.funnel !== undefined) {
      let toDateMinusOne = moment(req.params.to).subtract('1', 'days').toDate();
      let dateDifference = moment().diff(toDateMinusOne, 'days');
      if (dateDifference === 0) {
        let updatedTime = moment(toDateMinusOne).format('MMM DD YYYY') + " " + moment().subtract(6, 'minutes').format("hh:mm A");
        to = new Date(updatedTime);
      } else {
        to = new Date(req.params.to);
      }
    } else {
      to = new Date(req.params.to);
    }
    delete req.query.funnel;
    queryData.createdAt = {
      $gt: fromDate,
      $lt: to
    };
    let stringArr = [];
    let countryArr = [];
    let arrPage = [];
    let sort = {};
    if (Object.keys(req.query).length > 1) {
      if ((req.query.visitMin !== undefined) && (req.query.visitMax !== undefined)) {
        queryData.recDuration = { $gte: parseInt(req.query.visitMin), $lte: parseInt(req.query.visitMax) };
      } else if (req.query.visitMin !== undefined) {
        queryData.recDuration = { $gte: parseInt(req.query.visitMin), $lte: 65000 };
      } else if (req.query.visitMax !== undefined) {
        queryData.recDuration = { $gte: 0, $lte: parseInt(req.query.visitMax) };
      }
      if (queryData.device !== undefined) {
        if (typeof queryData.device === 'string') {
          queryData.device = queryData.device;
        } else {
          queryData.device = {
            $in: req.query.device
          };
        }
      }
      if (queryData.type !== undefined) {
        queryData.type = req.query.type;
      }
      if (queryData.pageName !== undefined) {
        if (typeof queryData.pageName === 'string') {
          stringArr.push(queryData.pageName);
          queryData.pageName = req.query.pageName;
        } else {
          queryData.pageName = { $in: req.query.pageName };
        }
      }
      if (queryData.formInteract !== undefined) {
        queryData.formInteract = parseInt(req.query.formInteract);
      }

      if (req.query.formSubmit !== undefined) {
        req.query.formSubmit = parseInt(req.query.formSubmit);
      }
      if (queryData.clickRage !== undefined) {
        if (parseInt(req.query.clickRage) === 0) {
          req.query.clickRage = 0;
        } else {
          req.query.clickRage = { $gte: parseInt(req.query.clickRage) };
        }
      }
      if (queryData.clickError !== undefined) {
        queryData.clickError = parseInt(req.query.clickError);
      }
      if (queryData.paymentTag !== undefined) {
        req.query.paymentTag = parseInt(req.query.paymentTag);
      }
      if (queryData.countryCode !== undefined) {
        if (typeof queryData.countryCode === 'string') {
          countryArr.push(queryData.countryCode);
          queryData.countryCode = { $in: countryArr };
        } else {
          queryData.countryCode = { $in: req.query.countryCode };
        }
      }
      if (queryData.network !== undefined) {
        if (typeof queryData.network === 'string') {
          queryData.network = req.query.network;
        } else {
          queryData.network = { $in: req.query.network };
        }
      }
      if (queryData.utmCampaign !== undefined) {
        sort.utmCampaign = parseInt(queryData.utmCampaign);
      }
      if (queryData.adName !== undefined) {
        sort.adName = parseInt(queryData.adName);
      }
      if (queryData.utmContent !== undefined) {
        sort.utmContent = parseInt(queryData.utmContent);
      }
      if (req.query.drops && req.query.pages) {
        if (typeof req.query.pages === 'string') {
          arrPage.push(req.query.pages);
        } else {
          arrPage = req.query.pages;
        }
        delete queryData.pages;
      } else if (req.query.pages) {
        if (typeof req.query.pages === 'string') {
          arrPage.push(req.query.pageName);
          if (arrPage[0] !== req.query.pages) {
            arrPage.push(req.query.pages);
          }
          delete queryData.pages;
          queryData.funnelPages = { $all: arrPage };
        }
        else if (req.query.pages.length === 1) {
          queryData.pages = { $in: req.query.pages };
        } else if (req.query.pages.length === 2) {
          arrPage = req.query.pages;
          arrPage.unshift(req.query.pageName);
          delete queryData.pages;
          queryData.funnelPages = { $eq: arrPage };
        }
        else {
          queryData.pages = { $nin: req.query.pages };
        }
      }
      if (req.query.utmSource !== undefined) {
        if (typeof req.query.utmSource === 'string') {
          queryData.utmSource = req.query.utmSource;
        } else {
          queryData.utmSource = { $in: ['google', 'bing'] };
        }
      }
      if (req.query.engine !== undefined) {
        queryData.engine = req.query.engine;
      }
      if (req.query.surname !== undefined) {
        queryData['surname.surname'] = { $regex: new RegExp("^" + req.query.surname), $options: "si" };
      }
      if (req.query.zipCode !== undefined) {
        queryData['zipCode.zipCode'] = { $regex: new RegExp("^" + req.query.zipCode), $options: "si" };
      }
      if (req.query.phone !== undefined) {
        queryData['phone.phone'] = { $regex: new RegExp("^" + req.query.phone), $options: "si" };
      }
      if (req.query.price !== undefined) {
        queryData.price = { $regex: new RegExp("^" + req.query.price), $options: "si" };
      }
    }
    delete queryData.visitMin;
    delete queryData.visitMax;
    delete queryData.utmCampaign;
    delete queryData.adName;
    delete queryData.utmContent;
    delete queryData.search;
    delete queryData.phone;
    delete queryData.zipCode;
    delete queryData.surname;


    if (sort.utmCampaign !== undefined) {
      queryData.utmCampaign = { $ne: '' };
    }
    if (sort.adName !== undefined) {
      queryData.adName = { $ne: '' };
    }
    if (sort.utmContent !== undefined) {
      queryData.utmContent = { $ne: '' };
    }
    let recordings = [];
    if (req.query.drops !== undefined) {
      if (req.query.drops === '2') {
        delete queryData.drops;
        recordings = await Session.aggregate(
          [
            {
              '$match': {
                $and: [queryData]
              }
            }, {
              '$match': {
                'funnelRecordData.page2': { $ne: arrPage[0] }
              }
            },
            {
              $group: {
                _id: null,
                interactions: {
                  $sum: '$formInteract'
                },
                submissions: {
                  $sum: '$formSubmit'
                },
                sum: {
                  $sum: 1
                },
              },
            },
            {
              $project: {
                _id: 0,
                submissions: 1,
                sum: 1,
                interactions: 1
              }
            }
          ]
        );
      } else if (req.query.drops === '3') {
        delete queryData.drops;
        recordings = await Session.aggregate(
          [
            {
              '$match': {
                $and: [queryData]
              }
            }, {
              '$match': {
                'funnelRecordData.page2': arrPage[0],
                'funnelRecordData.page3': { $ne: arrPage[1] }
              }
            },
            {
              $group: {
                _id: null,
                interactions: {
                  $sum: '$formInteract'
                },
                submissions: {
                  $sum: '$formSubmit'
                },
                sum: {
                  $sum: 1
                },
              },
            },
            {
              $project: {
                _id: 0,
                submissions: 1,
                sum: 1,
                interactions: 1
              }
            }
          ]
        );
      }

    } else {
      if (arrPage.length === 0) {
        recordings = await Session.aggregate(
          [
            {
              '$match': {
                $and: [queryData]
              }
            },
            {
              $group: {
                _id: null,
                interactions: {
                  $sum: '$formInteract'
                },
                submissions: {
                  $sum: '$formSubmit'
                },
                payments: {
                  $sum: '$paymentTag'
                },
                sum: {
                  $sum: 1
                },
              },
            },
            {
              $project: {
                _id: 0,
                submissions: 1,
                payments:1,
                sum: 1,
                interactions: 1
              }
            }
          ]
        );
      } else if (arrPage.length === 2) {
        delete queryData.funnelPages;
        recordings = await Session.aggregate(
          [
            {
              '$match': {
                $and: [queryData]
              }
            }, {
              '$match': {
                'funnelRecordData.page2': arrPage[1]
              }
            },
            {
              $group: {
                _id: null,
                interactions: {
                  $sum: '$formInteract'
                },
                submissions: {
                  $sum: '$formSubmit'
                },
                sum: {
                  $sum: 1
                },
              },
            },
            {
              $project: {
                _id: 0,
                submissions: 1,
                sum: 1,
                interactions: 1
              }
            }
          ]
        );
      } else if (arrPage.length === 3) {
        delete queryData.funnelPages;
        recordings = await Session.aggregate(
          [
            {
              '$match': {
                $and: [queryData]
              }
            }, {
              '$match': {
                'funnelRecordData.page2': arrPage[1],
                'funnelRecordData.page3': arrPage[2]
              }
            },
            {
              $group: {
                _id: null,
                interactions: {
                  $sum: '$formInteract'
                },
                submissions: {
                  $sum: '$formSubmit'
                },
                sum: {
                  $sum: 1
                },
              },
            },
            {
              $project: {
                _id: 0,
                submissions: 1,
                sum: 1,
                interactions: 1
              }
            }
          ]
        );
      }

    }
    if (recordings.length > 0) {
      return res.send({
        code: 200,
        total: { totalVisit: recordings[0].sum, submissions: recordings[0].submissions, totalInteract: recordings[0].interactions, totalSubmit: recordings[0].submissions, payments: recordings[0].payments ? recordings[0].payments : 0 }
      });
    } else {
      return res.send({
        code: 200,
        summary: []
      });
    }
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.fieldDetials = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const formId = req.params.formId;
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    const siteId = req.params.siteId;
    const queryData = req.query;
    queryData.createdAt = {
      $gte: fromDate,
      $lte: toDate
    };
    delete queryData.duration;
    queryData.siteId = siteId;
    if (queryData.clickRage !== undefined) {
      if (req.query.clickRage === 'true') {
        queryData.clickRage = { $nin: ["false", "0"] };
      }
    }
    const forms = await UserForm.find({
      siteId: siteId,
      _id: formId
    }).select('-fieldType -fieldName -fieldId -fieldTagName').exec();
    const data = [];
    if (forms.length > 0) {
      for (let index = 0; index < forms.length; index++) {
        queryData.pageName = forms[index].pageName;
        delete queryData.index;
        const views = await Recording.countDocuments({
          $and: [queryData]
        }).exec();
        const summary = await Interaction.aggregate([{
          $match: {
            $and: [queryData]
          },
        },
        {
          $project: {
            interact: 1,
            pageName: 1,
            conversion: 1
          },
        },
        {
          $group: {
            _id: {
              pageName: '$pageName',
            },
            interact: {
              $sum: '$interact'
            },
            conversion: {
              $sum: '$conversion'
            },
            total: {
              $sum: 1
            }
          },
        },
        ]).exec();
        const fieldss = [];
        const fieldName = forms[index].fields;
        for (let j = 0; j < fieldName.length; j++) {
          queryData.index = fieldName[j].fieldIndex;
          const fieldSummary = await FieldTracking.aggregate([{
            $match: {
              $and: [queryData]
            }
          },
          {
            $sort: {
              index: 1
            }
          },
          {
            $group: {
              _id: '$fieldName',

              interact: {
                $sum: '$interact'
              },
              conversion: {
                $sum: '$conversion'
              },
              dropOff: {
                $sum: '$dropOff'
              },
              interactTime: {
                $avg: '$interactTime'
              },
              path: {
                $first: '$path'
              },
              total: {
                $sum: 1
              },
            },
          },
          ]).exec();
          if (fieldSummary.length === 0) {
            let fieldData = {
              _id: fieldName[j].fieldName,
              interact: 0,
              conversion: 0,
              dropOff: 0,
              interactTime: 0,
              total: 0
            }
            fieldData.name = fieldName[j].fieldName;
            fieldss.push(fieldData);
          } else {
            fieldSummary[0].name = fieldName[j].fieldName;
            fieldss.push(fieldSummary[0]);
          }
        }
        let interact = 0;
        let conversion = 0;
        if (typeof summary[0] !== 'undefined') {
          interact = summary[0].interact;
        }
        if (typeof summary[0] !== 'undefined') {
          conversion = summary[0].conversion;
        }
        forms[index].formName = forms[index].formName;
        forms[index].visitttt = views;
        if (summary.length > 0) {
          data.push({
            formName: forms[index].formName,
            fields: fieldss,
            visit: views,
            interact: interact,
            conversion: conversion,
            total: summary.total,
            formId: forms[index]._id
          });
        } else {
          data.push({
            formName: forms[index].formName,
            fields: [],
            visit: views,
            interact: 0,
            conversion: 0,
            total: 0,
            formId: forms[index]._id
          });
        }
      }
      return res.send({
        code: 200,
        summary: data
      });
    } else {
      return res.send({
        code: 200,
        summary: []
      });
    }
  } catch (error) {
    next(error);
  }
};

exports.create = async (req, res, next) => {
  try {
    const forms = await Form.getPageForms(req.body);
    if (forms.length > 0) {
      form.isCreatedByUser = true;
      if (form.inputs) {
        form.formName = req.body.formName;
        req.body.inputs.forEach(i => {
          let index = form.inputs.findIndex(input => input.inputName == i.inputName);
          if (index >= 0) {
            form.inputs[index]['isTracked'] = true;
            form.inputs[index]['inputValue'] = i.inputValue;
          } else {
            index = form.inputs.findIndex(input => input.inputType == 'submit');
            form.inputs[index]['isTracked'] = true;
            form.inputs[index]['inputValue'] = i.inputValue;
          }
        })
        const savedForm = await form.save();
        const screens = await Page.createPageScreen({
          websiteId: req.body.siteId,
          pageName: req.body.pageName,
          id: form._id
        });
      } else {
        return res.status(500).send({
          code: 500,
          data: [],
          message: "No form found"
        });
      }
    } else {
      return res.status(500).send({
        code: 500,
        data: [],
        message: "No form found"
      });
    }
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};



exports.recordInteractions = async (req, res, next) => {
  try {
    var decryption_string = req.headers['x-auth-token'];
    var bytes = CryptoJS.AES.decrypt(req.body.data, decryption_string);
    var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    req.body = decryptedData;
    const website = await Website.findOne({ _id: req.body.siteId }).exec();
    if (website.stopRecording === null) {
      return res.status(200).send({ status: 404, data: { msg: 'No site exist with this ID ' } });
    }
    if (req.body.domainName !== website.name) {
      return res.status(200).send({ status: 404, data: { msg: 'Domain Mismatch.' } });
    }
    if (website.stopRecording === true) {
      return res.status(200).send({ status: 404, data: { msg: 'Account Admin has stopped recording for this site. ' } });
    }
    res.send({
      code: 200,
      data: 'Data inserted successfully.'
    });

    await recordInteractionProcess(req, website);
  } catch (error) {
    next(error);
    console.log(error);
  }
};

async function recordInteractionProcess(req, website) {
  const url = MONGO_DB_URI;
  const dbName = website._id.toString();
  const client = new MongoClient(url, { useNewUrlParser: true });
  let date = moment().format('YYYY-MM-DD');
  const resolution = req.body.resolution;
  const osName = req.body.osName;
  const browserName = req.body.browserName;
  const device = req.body.device;
  const countryName = req.body.countryName;
  const pageName = req.body.pageName;
  const converted = req.body.converted;
  let referer = req.body.referer;
  let recordingId = req.body.recordingId;
  let conversion = 0;
  let dropOutFields = [];
  let checkInteract = await FormStats.findOne({ recordingId: req.body.recordingId }).exec();

  if (checkInteract !== null) {
    if (checkInteract.formInteract !== req.body.formInteract
      || checkInteract.formSubmit !== req.body.formSubmit
      || checkInteract.clickError !== req.body.clickError
      || checkInteract.clickRage !== req.body.clickRage
      || checkInteract.conversion !== req.body.clickRage) {
      if (req.body.formSubmit === 'true') {
        conversion = 1;
      }
      await Interaction.findOneAndUpdate({ recordingId: req.body.recordingId }, {
        formInteract: req.body.formInteract,
        formSubmit: req.body.formSubmit,
        clickError: req.body.clickError,
        clickRage: req.body.clickRage,
        conversion: conversion
      }, { new: true }).exec();

    }
    await FieldTracking.deleteMany({ recordingId: req.body.recordingId }).exec();
    let phone = '';
    let surName = '';
    let zipCode = '';
    let price = '';
    if (req.body.interactions.length > 0) {
      for (let index = 0; index < req.body.interactions.length; index++) {
        if (req.body.interactions[index].formSubmit === 1) {
          req.body.interactions[index].formSubmit = 1;
          req.body.interactions[index].dropOff = 0;
        } else {
          req.body.interactions[index].formSubmit = 0;
          req.body.interactions[index].dropOff = 1;
        }
        await FormStats.findOneAndUpdate({ recordingId: req.body.recordingId, formIndex: req.body.interactions[index].formIndex }, {
          formInteract: req.body.interactions[index].formInteract,
          formSubmit: req.body.interactions[index].formSubmit,
          dropOff: req.body.interactions[index].dropOff,
          formIndex: req.body.interactions[index].formIndex
        }, { new: true }).exec();
        const fieldsData = _.map(req.body.interactions[index].fieldInteract, function (field) {
          field.siteId = req.body.siteId;
          field.recordedDate = date;
          field.referer = referer;
          field.resolution = resolution;
          field.osName = osName;
          field.browserName = browserName;
          field.device = device;
          field.countryName = countryName;
          field.pageName = req.body.pageName.split("?")[0];
          field.clickRage = req.body.clickRage;
          field.formInteract = req.body.formInteract;
          field.formSubmit = req.body.formSubmit;
          field.clickError = req.body.clickError;
          field.recordingId = req.body.recordingId;
          field.formIndex = req.body.interactions[index].formIndex;
          field.createdAt = new Date().toISOString();

          if (field.fieldName === 'phone1' || field.fieldName === 'mobile' || field.fieldName === 'number') {
            phone = field.value;
          } else if (field.fieldName === 'last_name' || field.fieldName === 'lastname') {
            surName = field.value !== undefined ? field.value : ''
          } else if (field.fieldName === 'zip') {
            zipCode = field.value;
          }
          return field;
        });

        let sessionData = await Session.findById(req.body.sessionId).exec();
        if (sessionData.surname && sessionData.surname.findIndex(x => x.recordingId === req.body.recordingId) === -1) {
          if (surName && surName !== '') {
            let resSurName = sessionData.surname;
            sessionData.surname = []
            resSurName.push({ surname: surName, recordingId: req.body.recordingId });
            sessionData.surname = resSurName;
          }
        } else {
          let index = sessionData.surname.findIndex(x => x.recordingId === req.body.recordingId);
          let resSurName = sessionData.surname;
          sessionData.surname = [];
          resSurName[index].surname = surName;
          sessionData.surname = resSurName;
        }
        if (sessionData.phone && sessionData.phone.findIndex(x => x.recordingId === req.body.recordingId) === -1) {
          if (phone && phone !== '') {
            let resphone = sessionData.phone;
            sessionData.resphone = []
            resphone.push({ phone: phone, recordingId: req.body.recordingId });
            sessionData.phone = resphone;
          }
        } else {
          let index = sessionData.phone.findIndex(x => x.recordingId === req.body.recordingId);
          let resphone = sessionData.phone;
          sessionData.phone = [];
          resphone[index].phone = phone;
          sessionData.phone = resphone;
        }
        if (sessionData.zipCode && sessionData.zipCode.findIndex(x => x.recordingId === req.body.recordingId) === -1) {
          if (zipCode && zipCode !== '') {
            let reszipCode = sessionData.zipCode;
            sessionData.reszipCode = []
            reszipCode.push({ zipCode: zipCode, recordingId: req.body.recordingId });
            sessionData.zipCode = reszipCode;
          }
        } else {
          let index = sessionData.zipCode.findIndex(x => x.recordingId === req.body.recordingId);
          let reszipCode = sessionData.zipCode;
          sessionData.zipCode = [];
          reszipCode[index].zipCode = zipCode;
          sessionData.zipCode = reszipCode;
        }
        await sessionData.save();
        let fields = [];
        let queryString = "";
        let filterDropOut = [];
        dropOutFields = [];
        if (req.body.formSubmit !== 1) {
          const filterDropOut = _.filter(fieldsData, function (dropOut) {
            if (dropOut.formSubmit === 0 && dropOut.interact === 1 && dropOut.value !== undefined) {
              let fieldName = dropOut.fieldName;
              let fieldVal = dropOut.value;
              dropOutFields.push({ fieldName: fieldName, fieldVal: fieldVal });
              return dropOut;
            }
          });
        }
        if (dropOutFields.length > 0) {
          await FieldReport.deleteOne({ recordingId: req.body.recordingId }).exec();
          let newDrops = new FieldReport({
            pageName: req.body.pageName.split("?")[0],
            referer: req.body.referer,
            device: req.body.device,
            recordedDate: date,
            recordingId: req.body.recordingId,
            siteId: req.body.siteId,
            fields: dropOutFields
          });
          await newDrops.save();
          // await FieldReport.insertMany(filterDropOut);
          if (website.name === 'ppichecker.team') {
            await saveFieldReport(filterDropOut);
          }
        } else {
          await FieldReport.deleteOne({ recordingId: req.body.recordingId }).exec();
        }
        // if (filterDropOut.length > 0) {
        //   await FieldReport.insertMany(filterDropOut);
        // } else {
        //   await FieldReport.deleteMany({ recordingId: req.body.recordingId }).exec();
        // }
        // client.connect(function(err, client) {
        //   assert.equal(null, err);  
        //   const db = client.db(dbName);    
        //   db.collection('fieldtrackings').insertMany(fieldsData, function(err, r) {
        //     console.log('DB Error => Inserting FieldTracking into database > Line no = 1075  '+ err);
        //   });
        //   client.close();
        // });
        await updateFormStatsData(dbName, req.body.recordingId, req.body.interactions[index].formIndex, req.body.interactions[index].formInteract, req.body.interactions[index].formSubmit, req.body.interactions[index].dropOff, req.body.interactions[index].formIndex, fieldsData);
        // await saveFieldTracking(dbName, fieldsData);
        await saveTracking(fieldsData);
        // return res.send({
        //   code: 200,
        //   data: 'Data inserted successfully.'
        // });
      }
    } else {
      // return res.send({
      //   code: 200,
      //   data: 'Data inserted successfully.'
      // });
    }
  }
  else {
    // if (req.body.referer !== 'noReferrer') {
    //   referer = await checkReferer(req.body.referer);
    // } else {
    //   referer = 'noReferrer';
    // }
    const convertData = new Interaction({
      referer: referer,
      resolution: req.body.resolution,
      osName: req.body.osName,
      browserName: req.body.browserName,
      device: req.body.device,
      countryName: req.body.countryName,
      clickRage: req.body.clickRage,
      formInteract: req.body.formInteract,
      formSubmit: req.body.formSubmit,
      clickError: req.body.clickError,
      conversion: converted,
      siteId: req.body.siteId,
      pageName: req.body.pageName.split("?")[0],
      recordingId: req.body.recordingId
    });
    if (req.body.formSubmit === 'true') {
      convertData.conversion = 1;
    }
    await convertData.save();
    const interactions = req.body.interactions;
    let phone = '';
    let surName = '';
    let zipCode = '';
    // let price = [];
    let formStats = [];
    if (req.body.interactions.length > 0) {
      for (let index = 0; index < req.body.interactions.length; index++) {
        if (req.body.interactions[index].formSubmit === 1) {
          req.body.interactions[index].formSubmit = 1;
          req.body.interactions[index].dropOff = 0;
        } else {
          req.body.interactions[index].formSubmit = 0;
          req.body.interactions[index].dropOff = 1;
        }
        let formStat = new FormStats({
          siteId: req.body.siteId,
          pageName: req.body.pageName.split("?")[0],
          recordingId: req.body.recordingId,
          device: req.body.device,
          formIndex: interactions[index].formIndex,
          formSubmit: interactions[index].formSubmit,
          dropOff: interactions[index].dropOff,
          formInteract: interactions[index].formInteract,
          createdAt: new Date().toISOString()
        });
        await formStat.save();
        let dataFieldsArray = [];
        const fieldsData = _.map(interactions[index].fieldInteract, function (field) {
          field.siteId = req.body.siteId;
          field.recordedDate = date;
          field.referer = referer;
          field.resolution = resolution;
          field.osName = osName;
          field.browserName = browserName;
          field.device = device;
          field.countryName = countryName;
          field.pageName = req.body.pageName.split("?")[0];
          field.clickRage = req.body.clickRage;
          field.formInteract = req.body.formInteract;
          field.formSubmit = req.body.formSubmit;
          field.clickError = req.body.clickError;
          field.recordingId = recordingId;
          field.formIndex = interactions[index].formIndex;
          field.createdAt = new Date().toISOString();
          if (field.fieldName === 'phone1' || field.fieldName === 'mobile' || field.fieldName === 'number') {
            phone = field.value;
          } else if (field.fieldName === 'last_name' || field.fieldName === 'lastname') {
            surName = field.value !== undefined ? field.value : ''
          } else if (field.fieldName === 'zip') {
            zipCode = field.value;
          }
          return field;
        });
        let sessionData = await Session.findById(req.body.sessionId).exec();
        if (sessionData.surname && sessionData.surname.findIndex(x => x.recordingId === req.body.recordingId) === -1) {
          if (surName && surName !== '') {
            let resSurName = sessionData.surname;
            sessionData.surname = []
            resSurName.push({ surname: surName, recordingId: req.body.recordingId });
            sessionData.surname = resSurName;
          }
        } else {
          let index = sessionData.surname.findIndex(x => x.recordingId === req.body.recordingId);
          let resSurName = sessionData.surname;
          sessionData.surname = [];
          resSurName[index].surname = surName;
          sessionData.surname = resSurName;
        }
        if (sessionData.phone && sessionData.phone.findIndex(x => x.recordingId === req.body.recordingId) === -1) {
          if (phone && phone !== '') {
            let resphone = sessionData.phone;
            sessionData.resphone = []
            resphone.push({ phone: phone, recordingId: req.body.recordingId });
            sessionData.phone = resphone;
          }
        } else {
          let index = sessionData.phone.findIndex(x => x.recordingId === req.body.recordingId);
          let resphone = sessionData.phone;
          sessionData.phone = [];
          resphone[index].phone = phone;
          sessionData.phone = resphone;
        }
        if (sessionData.zipCode && sessionData.zipCode.findIndex(x => x.recordingId === req.body.recordingId) === -1) {
          if (zipCode && zipCode !== '') {
            let reszipCode = sessionData.zipCode;
            sessionData.reszipCode = []
            reszipCode.push({ zipCode: zipCode, recordingId: req.body.recordingId });
            sessionData.zipCode = reszipCode;
          }
        } else {
          let index = sessionData.zipCode.findIndex(x => x.recordingId === req.body.recordingId);
          let reszipCode = sessionData.zipCode;
          sessionData.zipCode = [];
          reszipCode[index].zipCode = zipCode;
          sessionData.zipCode = reszipCode;
        }
        await sessionData.save();
        let fields = [];
        let queryString = "";
        dropOutFields = [];
        const filterDropOut = _.filter(fieldsData, function (dropOut) {
          if (dropOut.formSubmit === 0 && dropOut.interact === 1 && dropOut.value !== undefined) {
            let fieldName = dropOut.fieldName;
            let fieldVal = dropOut.value;
            dropOutFields.push({ fieldName: fieldName, fieldVal: fieldVal });
            queryString += '&' + dropOut.fieldName + '=' + dropOut.value;
            return dropOut;
          }
        });
        if (dropOutFields.length > 0) {
          filterDropOut[0].query = queryString;
          let newDrops = new FieldReport({
            pageName: req.body.pageName.split("?")[0],
            referer: req.body.referer,
            device: req.body.device,
            recordedDate: date,
            recordingId: req.body.recordingId,
            siteId: req.body.siteId,
            fields: dropOutFields
          });
          await newDrops.save();
          if (website.name === 'ppichecker.team') {
            await saveFieldReport(filterDropOut);
          }
        }
        await saveAllData(dbName, formStat, fieldsData);
        await saveTracking(fieldsData);
      }
    }
    else {
      // return res.send({
      //   code: 200,
      //   data: 'Data inserted successfully.'
      // });
    }
  }
}


async function checkReferer(referer) {
  return new Promise(
    (resolve, reject) => {
      if (referer === 'https://www.google.com/') {
        resolve('search');
      } else if (referer === 'http://instagram.com/') {
        resolve('social');
      } else {
        inbound.referrer.parse(url, referer, function (err, description) {
          if (err) reject(err);
          resolve(description.referrer.type);
        });
      }
    }
  );
}

/**
 * Form Detail 
 * @public
 */
exports.detail = async (req, res, next) => {
  try {
    const form = await Form.getFormById({
      id: req.params.formId
    });
    res.status(httpStatus.CREATED);
    return res.status(200).send({
      code: 200,
      data: form,
      message: ""
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

/**
 * Form Report 
 * @public
 */
exports.report = async (req, res, next) => {
  try {
    const form = await Form.getFormById({
      id: req.params.formId
    });
    const recording = await Recording.getRecording({
      siteId: form.websiteId
    });
    visitor = rec.length;
    let intrection = form.inputs[0]['interactions'];
    form.inputs.forEach(i => {
      if (i.isTracked) {
        if (intrection < i.interactions) {
          intrection += i.interactions;
        }
      }
    })
    resObj.push({
      pageName: form.pageName,
      formName: form.formName,
      Visitors: visitor,
      Intrection: intrection,
      inputs: form.inputs
    })
    res.status(httpStatus.CREATED);
    return res.status(200).send({
      code: 200,
      data: resObj,
      message: ""
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

/**
 * Get all form  
 * @public
 */
exports.get = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    let visitor = 0;
    const recording = await Recording.getRecording({
      siteId: req.params.siteId
    });
    visitor = recording.length;
    const form = await Form.getallSiteForms({
      websiteId: req.params.siteId,
      isCreatedByUser: true
    });
    let resObj = [];
    forms.forEach(f => {
      let intrection = f.inputs[0]['interactions'];
      f.inputs.forEach(i => {
        if (i.isTracked) {
          if (intrection < i.interactions) {
            intrection += i.interactions;
          }
        }
      })
      resObj.push({
        pageName: f.pageName,
        formId: f._id,
        formName: f.formName,
        Visitors: visitor,
        Intrection: intrection,
        conversion: 21,
        inputs: f.inputs
      })
    })
    res.status(httpStatus.CREATED);
    return res.status(200).send({
      code: 200,
      data: resObj,
      message: ""
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

/**
 * Filter form
 * @public
 */
exports.filterForm = async (req, res, next) => {
  try {
    const form = new Form();
    let filters = form.getWhereClauseQueryForFilter(req);
    let whereClauseQuery = filters.whereClauseQuery;
    let whereClauseRec = filters.whereClauseRec;
    let visitor = 0;
    const recording = await Recording.filterRecordingForForm(whereClauseRec);
    visitor = recording.length;
    const forms = await Form.getPageForms({
      websiteId: req.body.siteId,
      isCreatedByUser: true
    });
    let resObj = [];
    forms.forEach(f => {
      let intrection = 0;
      f.inputs.forEach(i => {
        i.interactions = 0;
        i.visitorInfo.forEach(v => {
          let a = Object.values(v);
          let fill = true;
          whereClauseQuery.forEach(filter => {
            if (Array.isArray(filter)) {
              let fill2 = false;
              filter.forEach(f => {
                if (a.indexOf(f) != -1) {
                  fill2 = true;
                }
              })
              fill = fill2;
            } else {
              if (a.indexOf(filter) == -1) {
                fill = false;
              }
            }
          })
          if (!fill) {
            i.interactions += 1;
          } else {
            i.interactions = 0;
          }
        })
        if (i.isTracked) {
          if (intrection < i.interactions) {
            intrection += i.interactions;
          }
        }
      })
      resObj.push({
        pageName: f.pageName,
        formId: f._id,
        formName: f.formName,
        Visitors: visitor,
        Intrection: intrection,
        conversion: 21,
        inputs: f.inputs
      })
    })
    res.status(httpStatus.CREATED);
    return res.status(200).send({
      code: 200,
      data: resObj,
      message: ""
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

/**
 * Filter form report
 * @public
 */
exports.filterFormReport = async (req, res, next) => {
  try {
    // const form = new Form();
    // let filters = form.getWhereClauseQueryForFilterFormReport(req);
    // let whereClauseQuery = filters.whereClauseQuery;
    // let whereClauseCode = filters.whereClauseCode;
    let resObj = [];
    let visitor = 0;
    const form = await Form.getFormById({
      id: req.body.formId
    });
    const recording = await Recording.getRecording({
      siteId: form.websiteId
    });
    visitor = recording.length;
    let intrection = form.inputs[0]['interactions'];
    form.inputs.forEach(i => {
      i.interactions = 0;
      i.visitorInfo.forEach(v => {
        let a = Object.values(v);
        let fill = true;
        whereClauseQuery.forEach(filter => {
          if (Array.isArray(filter)) {
            let fill2 = false;
            filter.forEach(f => {
              if (a.indexOf(f) != -1) {
                fill2 = true;
              }
            })
            fill = fill2;
          } else {
            if (a.indexOf(filter) == -1) {
              fill = false;
            }
          }
        })
        if (fill) {
          i.interactions += 1;
        }
      })
      if (i.isTracked) {
        if (intrection < i.interactions) {
          intrection += i.interactions;
        }
      }
    })
    resObj.push({
      pageName: form.pageName,
      formName: form.formName,
      Visitors: visitor,
      Intrection: intrection,
      inputs: form.inputs
    })
    res.status(httpStatus.CREATED);
    return res.status(200).send({
      code: 200,
      data: resObj,
      message: ""
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

/**
 * Filter form date
 * @public
 */
exports.filterFormDate = async (req, res, next) => {
  try {
    let visitor = 0;
    const recording = await Recording.getRecording({
      siteId: req.body.siteId
    });
    visitor = recording.length;
    const forms = await Form.getallSiteFormsBySiteId({
      websiteId: req.body.siteId
    });
    let resObj = [];
    forms.forEach(f => {
      let intrection = 0;
      let inps = [];
      f.inputs.forEach(i => {
        if (i.isTracked) {
          if (intrection < i.interactions) {
            intrection += i.interactions;
          }
        }
      })
      resObj.push({
        pageName: f.pageName,
        formId: f._id,
        formName: f.formName,
        Visitors: visitor,
        Intrection: intrection,
        conversion: 21,
        inputs: f.inputs
      })
    })
    res.status(httpStatus.CREATED);
    return res.status(200).send({
      code: 200,
      data: resObj,
      message: ""
    });
  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
};

/**
 * GET Form DROP OFF fields details. 
 */
exports.getFormFieldDetail = async (req, res, next) => {
  try {
    console.log('IN DROP OFF FORM API CALL');
    console.log('#########################');
    console.log(req.body);
    const pageName = req.body.pageName;
    const queryData = {};
    const queryData1 = {};
    const device = req.body.device;
    queryData.siteId = req.body.siteId;
    queryData.pageName = pageName;
    queryData.device = device;
    queryData.formIndex = req.body.formIndex;
    //=================
    queryData1.siteId = req.body.siteId;
    queryData1.pageName = pageName;
    queryData1.device = device;
    queryData1.formIndex = req.body.formIndex;
    //=================
    let website = await Website.findOne({ _id: req.body.siteId }).select('protocol').exec();
    let dbName = website._id.toString();
    console.log('#################################');
    console.log('#################################');
    console.log('#################################');
    console.log(pageName);
    console.log(req.body.siteId);
    let pageData = await Page.findOne({ $and: [{ name: pageName }, { websiteId: req.body.siteId }] }).select('name height pageHeight width').exec();
    console.log('DEBUG at 1815 ##############################');
    console.log(pageData);
    let recordingHtml = await Recording.findOne({ $and: [{ siteId: req.body.siteId }, { pageName: pageData.name }] }).sort({ createdAt: -1 }).exec();
    //let fieldSummary =  getFieldStatsData(dbName, queryData, req.body.from, req.body.to);
    //let data = await getFormStats(dbName, queryData, req.body.from, req.body.to);
    const url = MONGO_DB_URI;
    queryData.createdAt = {
      $gt: req.body.from,
      $lt: req.body.to
    };
    queryData.isDeleted = {
      $ne: true
    }

    queryData1.createdAt = {
      $gte: new Date(req.body.from),
      $lte: new Date(req.body.to)
    };
    queryData1.isDeleted = {
      $ne: true
    }
    // Use connect method to connect to the server
    MongoClient.connect(url, { useNewUrlParser: true }, function (err, client) {
      assert.equal(null, err);
      //let formstatsDataaa = [];
      console.log(err)
      console.log("Connected successfully to server");
      const db = client.db(dbName);
      let formstatsDataaa = [];
      db.collection("formstats").aggregate([
        {
          $match: {
            $and: [
              queryData1
            ]
          }
        },
        {
          $group: {
            _id: "$formIndex",
            formInteract: {
              $sum: '$formInteract'
            },
            formSubmit: {
              $sum: '$formSubmit'
            },
            dropOff: {
              $sum: '$dropOff'
            },
            total: {
              $sum: 1
            },
          },
        }
      ], { allowDiskUse: true }, function (err, cursor) {
        assert.equal(err, null);
        cursor.toArray(function (err, docs) {
          console.log(docs);
          formstatsDataaa = docs;
          // return formstatsDataaa
        });
      });
      db.collection("fieldtrackings").aggregate(
        [{
          $facet: {
            "fieldSummary": [
              {
                $match: {
                  $and: [queryData]
                }
              }, {
                $group: {
                  _id: '$fieldName',
                  interact: {
                    $sum: '$interact'
                  },
                  conversion: {
                    $sum: '$conversion'
                  },
                  dropOff: {
                    $sum: '$dropOff'
                  },
                  interactTime: {
                    $avg: '$interactTime'
                  },
                  path: {
                    $last: '$path'
                  },
                  total: {
                    $sum: 1
                  }
                }
              }
            ],
          }
        }
        ], { allowDiskUse: true }, function (err, cursor) {
          console.log(err);
          assert.equal(err, null);
          cursor.toArray(function (err, documents) {
            if (website.protocol === 'https') {
              return res.send(
                {
                  code: 200,
                  data: documents.length > 0 ? documents[0].fieldSummary : [],
                  formData: formstatsDataaa.length > 0 && formstatsDataaa[0] ? formstatsDataaa[0] : {},
                  height: pageData.height,
                  pageHeight: pageData.pageHeight,
                  width: pageData.width,
                  html: (recordingHtml !== null) ? recordingHtml.htmlCopy : '',
                  protocol: website.protocol
                });
            } else {
              let params = {
                Bucket: 'flopandascripts',
                Key: 'websites/' + req.body.siteId + '/' + pageData._id + '.html',
                ResponseContentEncoding: 'text/html'
              };
              s3.getObject(params, function (s3Err, newData) {
                if (s3Err) {
                  console.log(s3Err);
                } else {
                  let htmlCopy = newData.Body.toString('utf-8');
                  return res.json({
                    data: documents.length > 0 ? documents[0].fieldSummary : [],
                    formData: formstatsDataaa.length > 0 && formstatsDataaa[0] ? formstatsDataaa[0] : {},
                    height: pageData.height,
                    pageHeight: pageData.pageHeight,
                    width: pageData.width,
                    html: htmlCopy,
                    protocol: website.protocol
                  });
                }
              });
            }
          });
        });
      client.close();
    });

  } catch (error) {
    console.log(error);
    next(error);
  }
};





async function saveTracking(fields) {
  try {
    const result = await FieldTracking.insertMany(fields);
    return result;
  } catch (error) {
    next(error);
  }
}

async function saveFieldReport(reportFields) {
  try {
    // request('https://bizmiuk.waypointsoftware.com/capture2.php?xAuthentication=sHY6jh030P0b7jurdjDaMHtk45l0'+reportFields[0].query, function (error, response, body) {
    //  // console.log('error:', error); // Print the error if one occurred
    //   //console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
    //   //console.log('body:', body); // Print the HTML for the Google homepage.
    // return response;
    // });
  } catch (error) {
    // nex(error);
    console.log(error);
  }
}

async function getSessionData(queryData) {
  let visits = await Session.aggregate([
    {
      $match: {
        $and: [queryData]
      }
    },
    {
      $group: {
        _id: null,
        interactions: {
          $sum: '$formInteract'
        },
        submissions: {
          $sum: '$formSubmit'
        },
        sum: {
          $sum: 1
        },
      },
    },
    {
      $project: {
        _id: 0,
        submissions: 1,
        sum: 1,
        interactions: 1
      }
    },
  ]);
  return visits[0];
}

async function getTotalViews(queryData) {
  let viewData = queryData;
  if (viewData.pages !== undefined) {
    viewData.pageName = { $in: viewData.pages.$in };
    delete viewData.pages;
  }
  if (viewData.recDuration !== undefined) {
    viewData.duration = { $gte: viewData.recDuration.$gte, $lte: viewData.recDuration.$lte };
    delete viewData.pages;
    delete viewData.recDuration;
  }

  let pages = await Recording.aggregate([
    {
      $match: {
        $and: [queryData],

      }
    },
    {
      $group: {
        _id: null,

        interactCount: {
          $sum: '$formInteract'
        },
        formSubmit: {
          $sum: '$formSubmit'
        },
        total: {
          $sum: 1
        },
      },
    },
    { $sort: { total: -1 } },
  ]);
  return pages;
}

async function conversionCron() {
  try {
    count = count + 1;
    let sites = await SiteConversion.find({}).exec();
    if (sites.length > 0) {
      for (let index = 0; index < sites.length; index++) {
        let fromDate = new Date(sites[index].from).getDay();
        let toDate = new Date(sites[index].to).getDay();
        let todayDate = new Date().getDay();
        if (sites[index].isDaily === true) {
          let visits = await Session.aggregate([
            {
              $match: {
                siteId: new mongoose.Types.ObjectId(sites[index].siteId)
              }
            },
            {
              $group: {
                _id: null,
                interactions: {
                  $sum: '$formInteract'
                },
                submissions: {
                  $sum: '$formSubmit'
                },
                sum: {
                  $sum: 1
                },
              },
            },
            {
              $project: {
                _id: 0,
                submissions: 1,
                sum: 1,
                interactions: 1
              }
            },
          ]);
          let total = visits[0] && visits[0].sum ? visits[0].sum : 0;
          let submission = visits[0] && visits[0].submissions ? visits[0].submissions : 0;
          let conversion = Math.floor((submission / total) * 100);
          if (isNaN(conversion)) {
            console.log('Invalid conversion rate');
          } else {
            let remainder = count % sites[index].duration;
            if (remainder === 0) {
              if (conversion <= sites[index].rate) {
                authProviders.send_email(sites[index].email, 'Lead Submission Alert', sites[index].message);
              }
            }
          }
        } else {
          if (todayDate >= fromDate && todayDate <= toDate) {
            let visits = await Session.aggregate([
              {
                $match: {
                  siteId: new mongoose.Types.ObjectId(sites[index].siteId)
                }
              },
              {
                $group: {
                  _id: null,
                  interactions: {
                    $sum: '$formInteract'
                  },
                  submissions: {
                    $sum: '$formSubmit'
                  },
                  sum: {
                    $sum: 1
                  },
                },
              },
              {
                $project: {
                  _id: 0,
                  submissions: 1,
                  sum: 1,
                  interactions: 1
                }
              },
            ]);
            let total = visits[0] && visits[0].sum ? visits[0].sum : 0;
            let submission = visits[0] && visits[0].submissions ? visits[0].submissions : 0;
            let conversion = Math.floor((submission / total) * 100);
            if (isNaN(conversion)) {
              console.log('Invalid conversion rate');
            } else {
              let remainder = count % sites[index].duration;
              if (remainder === 0) {
                if (conversion <= sites[index].rate) {
                  authProviders.send_email(sites[index].email, 'Lead Submission Alert', sites[index].message);
                }
              }
            }
          }
        }
      }
    }
  } catch (error) {
    console.log(error);
  }
};

exports.getCurrentConversionPackage = async (req, res, next) => {
  var userId = req.params.userId;
  let conversionPackage = await UserConversionPackage.findOne({ $and: [{ userId: userId }, { isActive: true }] }).exec();
  res.send(conversionPackage);
}

exports.getAllSiteConversions = async (req, res, next) => {
  var userId = req.params.userId;
  let siteConversions = await SiteConversion.find({ $and: [{ userId: userId }, { isActive: true }] }).exec();
  res.send({ data: siteConversions });
}

exports.setSiteConversions = async (req, res, next) => {
  var totalEmails = 0;
  var totalPhones = 0;
  var userId = req.params.userId;
  for (var i = 0; i < req.body.length; i++) {
    totalEmails = totalEmails + req.body[i].email.length;
    for (var j = 0; j < req.body[i].phone.length; j++) {
      totalPhones = totalPhones + req.body[i].phone[j].phone.length;
    }
    // totalPhones = totalPhones + req.body[i].phone.length;
    await SiteConversion.updateOne({ _id: new mongoose.Types.ObjectId(req.body[i]._id) },
      {
        $set: { email: req.body[i].email, phone: req.body[i].phone }
      })
  }
  // await UserConversionPackage.updateOne({ $and: [{ userId: userId }, { isActive: true }] }, { $set: { emailAdded: totalEmails, numberAdded: totalPhones } }).exec();
  res.send({
    success: true
  });
}

exports.saveCronInfo = async (req, res, next) => {
  try {
    let userId = req.body.userId;
    let siteId = req.body.siteId;
    let email = req.body.email;
    let message = req.body.message;
    let from = req.body.from;
    let phone = req.body.phone;
    let to = req.body.to;
    let duration = req.body.duration;
    let isDaily = req.body.isDaily;
    let isReceive = req.body.isReceive;
    let numberAdded = req.body.numberAdded;
    let emailAdded = req.body.emailAdded;
    let conversionPackage = await UserConversionPackage.findOne({ $and: [{ userId: userId }, { isActive: true }] }).exec();
    if (conversionPackage && conversionPackage.numberAdded > conversionPackage.toalUsersAllowed) {
      return res.send({ code: 400, data: 'SMS package limit exceeded.' });
    }
    if (conversionPackage && conversionPackage.emailAdded > conversionPackage.toalUsersAllowed) {
      return res.send({ code: 400, data: 'Email package limit exceeded.' });
    }
    let site = await SiteConversion.findOne({ siteId: siteId }).exec();
    if (site) {
      if (site.duration !== duration) {
        let notiUpdatedTime = moment();
        site.notificationSentAt = notiUpdatedTime;
      }
      site.email = email;
      site.message = message;
      site.from = from;
      site.to = to;
      site.rate = req.body.rate;
      site.duration = duration;
      site.isDaily = isDaily;
      site.phone = phone;
      site.isReceive = isReceive;
      conversionPackage.numberAdded += numberAdded;
      conversionPackage.emailAdded += emailAdded;
      await site.save();
      await conversionPackage.save();
    } else {
      let newConversionRule = new SiteConversion({
        userId: userId,
        siteId: siteId,
        email: email,
        phone: phone,
        message: message,
        from: from,
        to: to,
        rate: req.body.rate,
        duration: duration,
        isDaily: isDaily,
        isReceive: isReceive,
      });
      conversionPackage.numberAdded += numberAdded;
      conversionPackage.emailAdded += emailAdded;
      await newConversionRule.save();
      await conversionPackage.save();
    }
    return res.send({ code: 200, data: 'Data inserted successfully.' });
  } catch (error) {
    next(error);
  }
};

exports.getCronInfo = async (req, res, next) => {
  try {
    // var currentUserSites = await checkWebsites(req.params.siteId, req.user);
    // if (!currentUserSites) {
    //   return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    // }
    let siteId = req.params.siteId;
    let userId = req.params.userId;
    let site = await SiteConversion.findOne({ $and: [{ siteId: siteId }, { isActive: true }] }).exec();
    let userConversionPackage = await UserConversionPackage.findOne({ $and: [{ userId: userId }, { isActive: true }] }).exec();
    let checkUserType = await Payment.findOne({ $and: [{ userId: userId }, { paymentType: "package" }, { paymentSuccess: true }] }).sort({ createdAt: -1 }).limit(1).exec();
    let isFreePackage;
    if (checkUserType) {
      isFreePackage = false;
    } else {
      isFreePackage = true;
    }
    let showModal = false;
    if (userConversionPackage) {
      showModal = true;
    }
    let userConversionCostInfo;
    if (userConversionPackage) {
      let packageInfo = await ConversionPrice.findOne({ _id: userConversionPackage.packageId }).exec();
      if (userConversionPackage.toalUsersAllowed === 1) {
        userConversionCostInfo = packageInfo.package1;
      } else if (userConversionPackage.toalUsersAllowed === 2) {
        userConversionCostInfo = packageInfo.package2;
      } else if (userConversionPackage.toalUsersAllowed === 3) {
        userConversionCostInfo = packageInfo.package3;
      } else if (userConversionPackage.toalUsersAllowed === 4) {
        userConversionCostInfo = packageInfo.package4;
      } else if (userConversionPackage.toalUsersAllowed === 5) {
        userConversionCostInfo = packageInfo.package5;
      } else {
        userConversionCostInfo = packageInfo.package1;
      }
    }

    if (!site) {
      site = {};
    }
    return res.send({
      code: 200,
      data: site,
      country: userConversionPackage && userConversionPackage.country ? userConversionPackage.country : [],
      isSmsService: userConversionPackage ? userConversionPackage.isSmsService : true,
      showModal: showModal,
      toalUsersAllowed: userConversionPackage && userConversionPackage.toalUsersAllowed ? userConversionPackage.toalUsersAllowed : 0,
      numberAdded: userConversionPackage && userConversionPackage.numberAdded ? userConversionPackage.numberAdded : 0,
      emailAdded: userConversionPackage && userConversionPackage.emailAdded ? userConversionPackage.emailAdded : 0,
      isFreePackage: isFreePackage,
      packageExpiry: userConversionPackage ? moment(userConversionPackage.createdAt).add(1, 'months') : '',
      packageInfo: userConversionCostInfo ? userConversionCostInfo : ''
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.deleteCronInfo = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    let siteId = req.params.siteId;
    let userId = req.body.userId;
    let numberAdded = req.body.numberAdded;
    let emailAdded = req.body.emailAdded;
    let conversionPackage = await UserConversionPackage.findOne({ $and: [{ userId: userId }, { isActive: true }] }).exec();
    let site = await SiteConversion.deleteOne({ siteId: siteId }).exec();
    conversionPackage.numberAdded -= numberAdded;
    conversionPackage.emailAdded -= emailAdded;
    await conversionPackage.save();
    return res.send({ code: 200, data: [] });
  } catch (error) {
    next(error);
  }
};

exports.runCron = async (req, res, next) => {
  try {
    let sites = await SiteConversion.find({ isActive: true }).exec();
    if (sites.length > 0) {
      for (let index = 0; index < sites.length; index++) {
        let fromDate = moment(sites[index].from).format('YYYY-MM-DD');
        let toDate = moment(sites[index].to).format('YYYY-MM-DD');
        let todayDate = moment().format('YYYY-MM-DD');
        if (sites[index].siteId) {
          let currentTime = moment();
          let cronTime = moment(sites[index].notificationSentAt).add(sites[index].duration, 'minutes');
          let currTimeDiff = moment().diff(moment(cronTime), 'minutes');
          if (sites[index].isDaily === true) {
            if (currTimeDiff === 0) {
              let visits = await Session.aggregate([
                {
                  $match: {
                    siteId: new mongoose.Types.ObjectId(sites[index].siteId),
                    createdAt: { $gt: new Date(moment().format('YYYY-MM-DD')), $lte: new Date(moment().format('YYYY-MM-DD')) }
                  }
                },
                {
                  $group: {
                    _id: null,
                    interactions: {
                      $sum: '$formInteract'
                    },
                    submissions: {
                      $sum: '$formSubmit'
                    },
                    sum: {
                      $sum: 1
                    },
                  },
                },
                {
                  $project: {
                    _id: 0,
                    submissions: 1,
                    sum: 1,
                    interactions: 1
                  }
                },
              ]);
              let total = visits[0] && visits[0].sum ? visits[0].sum : 0;
              let submission = visits[0] && visits[0].submissions ? visits[0].submissions : 0;
              let conversion = Math.floor((submission / total) * 100);
              conversion.toFixed(0);
              if (isNaN(conversion)) {
                conversion = 0;
              }
              if (conversion <= sites[index].rate) {
                let numbers = sites[index].phone;
                if (numbers && numbers.length > 0) {
                  await sms.send(numbers, sites[index].message);
                }
                if (sites[index].email.length > 0) {
                  authProviders.send_email(sites[index].email, 'Lead Submission Alert', sites[index].message);
                }
                await SiteConversion.updateOne({ _id: sites[index]._id }, { notificationSentAt: currentTime });
              }
            }
          } else {
            if (todayDate >= fromDate && todayDate <= toDate) {
              if (currTimeDiff === 0) {
                let a = moment(sites[index].from).get('hour');
                let b = moment(sites[index].to).get('hour');
                let c = moment().get('hour');
                let fromDatea = moment().diff(sites[index].from, 'minutes');
                let toDatea = moment().diff(sites[index].to, 'minutes');
                if (fromDatea >= 0 && toDatea <= 0) {
                  let visits = await Session.aggregate([
                    {
                      $match: {
                        siteId: new mongoose.Types.ObjectId(sites[index].siteId),
                        createdAt: { $gt: moment(sites[index].from).toISOString(), $lte: moment(sites[index].to).toISOString() }
                      }
                    },
                    {
                      $group: {
                        _id: null,
                        interactions: {
                          $sum: '$formInteract'
                        },
                        submissions: {
                          $sum: '$formSubmit'
                        },
                        sum: {
                          $sum: 1
                        },
                      },
                    },
                    {
                      $project: {
                        _id: 0,
                        submissions: 1,
                        sum: 1,
                        interactions: 1
                      }
                    },
                  ]);
                  let total = visits[0] && visits[0].sum ? visits[0].sum : 0;
                  let submission = visits[0] && visits[0].submissions ? visits[0].submissions : 0;
                  let conversion = Math.floor((submission / total) * 100);
                  conversion.toFixed(0);
                  if (isNaN(conversion)) {
                    conversion = 0;
                  }
                  if (conversion <= sites[index].rate) {
                    let numbers = sites[index].phone;
                    if (numbers && numbers.length > 0) {
                      await sms.send(numbers, sites[index].message);
                    }
                    if (sites[index].email.length > 0) {
                      await authProviders.send_email(sites[index].email, 'Lead Submission Alert', sites[index].message);
                    }
                    await SiteConversion.updateOne({ _id: sites[index]._id }, { notificationSentAt: currentTime })
                  }
                }
              }
            }
          }
        }
      }
      return res.send({ code: 200, data: 'cron run successfully!' });
    } else {
      return res.send({ code: 200, data: 'Notification does not exist.' });
    }
  } catch (error) {
    console.log(error);
    next(error);
  }
};
exports.deleteFormFields = async (req, res, next) => {
  try {
    let fieldIds = req.body.fieldIds;
    let formIndex = req.body.formIndex;
    let website = await Website.findOne({ _id: req.body.siteId }).select('protocol').exec();
    let dbName = website._id.toString();
    await updateFormFieldsStatus(dbName, fieldIds, formIndex, true);
    return res.send({ code: 200, data: [] });
  } catch (error) {
    console.log(error);
    next(error);
  }
};


exports.sendSmssss = async (req, res, next) => {
  try {
    console.log('<< IN SEND MESSAGE APIIIIIIIIII>>>>>>>>>>>');
    request('http://portal.seven67.com/app/smsapi/index?key=55D3851F011C79&campaign=1&routeid=117&type=text&contacts=447365035690&senderid=FloPanda&msg=test flopanda msg";', function (error, response, body) {
      console.log('error:', error); // Print the error if one occurred
      console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
      console.log('body:', body); // Print the HTML for the Google homepage.
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

/**
 * GET Page All forms and their fields with dropoff calculation 
 */
async function getFieldStatsData(dbName, queryData, from, to) {
  try {
    const url = `mongodb://admin:admin123Abc@10.0.82.12:27017/${dbName}?authSource=admin&w=0&replicaSet=s0&readPreference=secondary`;
    queryData.createdAt = {
      $gt: from,
      $lt: to
    };
    queryData.isDeleted = {
      $ne: true
    }
    // MongoClient.connect(url, { useNewUrlParser: true }, function(err, db) {
    //   db.collection("fieldtrackings").aggregate([
    //           {
    //             $match: {
    //               $and: [queryData]
    //             }
    //           }, 
    //           {
    //             $group: {
    //               _id: '$fieldName',      
    //               interact: {                
    //               $sum: '$interact'
    //               },
    //               conversion: {
    //                 $sum: '$conversion'
    //               },
    //               dropOff: {
    //                 $sum: '$dropOff'
    //               },
    //               interactTime: {
    //                 $avg: '$interactTime'
    //               },
    //               path: {
    //                 $last: '$path'
    //               },
    //               total: {
    //                 $sum: 1
    //               }
    //             }
    //   ], function (err, cursor) {
    //     assert.equal(err, null);
    //     cursor.toArray(function (err, documents) {
    //       err ? reject(err) : resolve(documents);
    //     });
    //   });
    // });



    // await client.connect();
    // const db = client.db(dbName);
    // let fieldData = await db.collection('fieldtrackings').aggregate([
    //   {
    //     $facet: {
    //       "fieldSummary": [
    //         {
    //           $match: {
    //             $and: [queryData]
    //           }
    //         }, {
    //           $group: {
    //             _id: '$fieldName',
    //             interact: {
    //               $sum: '$interact'
    //             },
    //             conversion: {
    //               $sum: '$conversion'
    //             },
    //             dropOff: {
    //               $sum: '$dropOff'
    //             },
    //             interactTime: {
    //               $avg: '$interactTime'
    //             },
    //             path: {
    //               $last: '$path'
    //             },
    //             total: {
    //               $sum: 1
    //             }
    //           }
    //         }
    //       ],
    //     }
    //   }
    // ]).toArray();
    // client.close();
    console.log('Data Returned successfuly!!');
    return fieldData;
  } catch (error) {
    console.log(error);
    return error;
  }
}

async function updateFormFieldsStatus(dbName, fieldIds, formIndex, isDeleted) {
  try {
    return new Promise((resolve, reject) => {
      const url = MONGO_DB_URI;
      const client = new MongoClient(url, { useNewUrlParser: true });
      client.connect(async function (err, client) {
        assert.equal(null, err);
        const db = client.db(dbName);
        const collection = db.collection('fieldtrackings');
        await collection.updateMany({
          siteId: "5db1461759a9bc19bf4e0ace",
          fieldId: { $in: fieldIds },
          formIndex: formIndex
        },
          { $set: { "isDeleted": isDeleted } });
        resolve();
      });
      client.close();
    });
  } catch (error) {
    return error;
  }
}

async function getFormStats(dbName, queryData, from, to) {
  try {
    const url = MONGO_DB_URI;
    const client = new MongoClient(url, { useNewUrlParser: true });
    queryData.createdAt = {
      $gte: new Date(from),
      $lte: new Date(to)
    };
    await client.connect();
    console.log("Connected correctly to server");
    const db = client.db(dbName);
    let r = await db.collection('formstats').aggregate([
      {
        $match: {
          $and: [
            queryData
          ]
        }
      },
      {
        $group: {
          _id: "$formIndex",
          formInteract: {
            $sum: '$formInteract'
          },
          formSubmit: {
            $sum: '$formSubmit'
          },
          dropOff: {
            $sum: '$dropOff'
          },
          total: {
            $sum: 1
          },
        },
      }
    ]).toArray();
    client.close();
    return r;
  } catch (error) {
    console.log(error);
  }
}


async function saveAllData(dbName, statData, fieldData) {
  try {
    console.log('IN SAVE FIELD AND STATS FILE');
    const url = MONGO_DB_URI;
    const client = new MongoClient(url, { useNewUrlParser: true });
    client.connect(function (err, client) {
      console.log('IN formstat db connection');
      assert.equal(null, err);
      const db = client.db(dbName);
      db.collection('formstats').insertOne(statData, function (err, r) {
        if (err) {
          console.log('Database error  >>> ' + err);
        }
      });
      console.log('FIELD DATA LENGTH' + fieldData.length);
      db.collection('fieldtrackings').insertMany(fieldData, function (err, r) {
        if (err) {
          console.log('Database error  fields table >>> ' + err);
        }
      });
      client.close();
    });
  } catch (error) {

  }
}

async function saveFormStats(dbName, data) {
  try {
    console.log('IN SAVE STATS FUNCTION >>>>>>>>>');
    return new Promise((resolve, reject) => {
      const url = MONGO_DB_URI;
      const client = new MongoClient(url, { useNewUrlParser: true });
      client.connect(function (err, client) {
        console.log('IN formstat db connection');
        assert.equal(null, err);
        const db = client.db(dbName);
        db.collection('formstats').insertOne(data, function (err, r) {
          if (err) {
            console.log('Database error stats table  >>> ' + err);
          }
        });
        client.close();
      });
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
}

async function saveFieldTracking(dbName, data) {
  try {
    console.log('IN SAVE FIELD DATAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA');
    console.log(data.length);
    console.log("<<<<<<<<<<<<<<< SAVE INSERT TRACKING FILE >>>>>>>>>>>>>>>>>>>");
    const url = MONGO_DB_URI;
    const client = new MongoClient(url, { useNewUrlParser: true });
    client.connect(function (err, client) {
      assert.equal(null, err);
      const db = client.db(dbName);
      db.collection('fieldtrackings').insertMany(data, function (err, r) {
        if (err) {
          console.log('Database error  >>> ' + err);
        }
      });
      client.close();
    });
  } catch (error) {
    console.log(error);
  }
}

async function deleteManyFieldTracking(dbName, recordingId) {
  try {
    console.log("<<<<<<<<<<<<<<< Delete TRACKING FILE >>>>>>>>>>>>>>>>>>>");
    return new Promise((resolve, reject) => {
      const url = MONGO_DB_URI;
      // let dbName = dbName;
      const client = new MongoClient(url, { useNewUrlParser: true });
      client.connect(function (err, client) {
        assert.equal(null, err);
        const db = client.db(dbName);
        db.collection('fieldtrackings').deleteMany({ recordingId: recordingId }, function (err, r) {
          if (err) {
            console.log('DB Error => Deleting field tracking DATA from database... Line no = 924  ' + err)
          }
        });
        client.close();
      });
    });
  } catch (error) {
    console.log(error);
  }
}


async function updateFormStatsData(dbName, recordingId, formIndex, formInteract, formSubmit, dropOff, formIndex, fieldTrackData) {
  try {
    console.log('LINE 2370');
    console.log(dbName);
    let promise = new Promise((resolve, reject) => {
      const url = MONGO_DB_URI;
      const client = new MongoClient(url, { useNewUrlParser: true });
      client.connect(function (err, client) {
        assert.equal(null, err);
        const db = client.db(dbName);
        db.collection('formstats').updateOne({ recordingId: recordingId, formIndex: formIndex }, {
          $set: {
            formInteract: formInteract,
            formSubmit: formSubmit,
            dropOff: dropOff,
            formIndex: formIndex
          }
        }, function (err, r) {
          if (err) {
            console.log('DB Error => FormStat data updation ERROR = Line no = 951  ' + err);
          }
        });
        db.collection('fieldtrackings').deleteMany({ recordingId: recordingId }, function (err, r) {
          if (err) {
            console.log('DB Error => Deleting field tracking DATA from database... Line no = 924  ' + err)
          }
          db.collection('fieldtrackings').insertMany(fieldTrackData, function (err, r) {
            if (err) {
              console.log('Database error 2393  >>> ' + err);
            }
            resolve(r);
          });
        });
        // client.close();
      });
    });
    let result = await promise;
  } catch (error) {
  }
}

