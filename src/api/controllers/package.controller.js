const Package = require('../models/packages.model');
// const Payment = require('../models/payment.model');
const { handler: errorHandler } = require('../middlewares/error');
const SiteConversion = require('../models/siteConversionCron.model');
const UserConversionPackage = require('../models/userConversionPackage.model');
const httpStatus = require('http-status');
const path = require('path');
const consolidate = require('consolidate');
const authProviders = require('../services/emailProvider');
const {
    jwtExpirationInterval,
    DOMAIN,
    DEV_DOMAIN,
    BETADOMAIN,
    TRAKING_CODE_SCRIPT
} = require('../../config/vars');

exports.get = async (req, res, next) => {
    try {
        let type = req.params.type;
        let filterQuery = req.query;
        let userId=req.user;
        let packages = await Package.getallPackages(type, filterQuery,userId);
        return res.status(200).send({
            code: 200,
            data: packages,
            message: ""
        });
    } catch (error) {
        next(errorHandler(error, req, res));
    }
};

exports.getUserPackage = async (req, res, next) => {
    try {
        let email = req.query.email;
        let packages = await Package.find({ email: email }).select('-updatedAt -__v').sort({ usPrice: 1 }).exec();
        let totalPackage = [];
        if (packages.length > 0) {
            let diamondPackage = await Package.find({ email: "" }).exec();
            totalPackage = [...packages, ...diamondPackage];
        } else {
            let where = {};
            where.packageType = { $ne: 2 };
            where.email = "";
            totalPackage = await Package.find({
                $and: [where]
            }).exec();
        }
        return res.status(200).send({
            code: 200,
            data: totalPackage,
            message: ""
        });
    } catch (error) {
        console.log(error);
        next(error);
    }
};


exports.detail = async (req, res, next) => {
    try {
        const packages = await Package.getPackageById({ packageId: req.params.packageId });
        res.status(200).send({
            code: '200',
            data: packages,
            message: ""
        });
    } catch (error) {
        next(errorHandler(error, req, res));
    }
};

exports.save = async (req, res, next) => {
    try {
        const packages = await Package.create({
            planeName: req.body.planeName,
            email: req.body.email !== undefined ? req.body.email : '',
            packageType: req.body.packageType !== undefined ? req.body.packageType : "1",
            planeType: req.body.planeType,
            viewPerMonth: req.body.viewPerMonth,
            recordingPerMonth: req.body.recordingPerMonth,
            websites: req.body.websites,
            storage: req.body.storage,
            usPrice: req.body.usPrice,
            gbPrice: req.body.gbPrice,
            recordings: req.body.recordings,
            heatMaps: req.body.heatMaps,
            funnels: req.body.funnels,
            ips: req.body.ips,
            issueReports: req.body.issueReports,
            analytics: req.body.analytics,
            dropOffs: req.body.dropOffs,
            partialData: req.body.partialData,
            recordingDownloads: req.body.recordingDownloads,
            heatmapDownloads: req.body.heatmapDownloads,
            support: req.body.support,
            isCustom: req.body.isCustom,
            conversionAlerts: req.body.conversionAlerts,
            utmTags: req.body.utmTags,
            websiteVisitorSearch: req.body.websiteVisitorSearch,
            shareRecording: req.body.shareRecording,
            shareHeatmap: req.body.shareHeatmap,
            fbCompaignBreakdown: req.body.fbCompaignBreakdown,
            tags: req.body.tags,
            deviceReporting: req.body.deviceReporting,
            pageBreakdownReport: req.body.pageBreakdownReport,
            discount: req.body.discount,
            userId: req.user !== undefined ?  req.user : 0
        });
        if (req.body.isCustom === true && req.body.email !== undefined && packages !== undefined) {
            const link = `${BETADOMAIN}refferalLogin?isCP=true&cpId=${packages._id}`;
            const correct = "https://cdn3.iconfinder.com/data/icons/flat-actions-icons-9/792/Tick_Mark_Dark-512.png";
            const wrong = "https://cdn3.iconfinder.com/data/icons/flat-actions-icons-9/792/Close_Icon_Dark-512.png";
            const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'custom-package.html';
            consolidate.swig(templateFilePath, {
                linkCreated: link,
                recordings: packages.recordings ? correct : wrong,
                heatMaps: packages.heatMaps ? correct : wrong,
                funnels: packages.funnels ? correct : wrong,
                ips: packages.ips ? correct : wrong,
                issueReports: packages.issueReports ? correct : wrong,
                analytics: packages.analytics ? correct : wrong,
                dropOffs: packages.dropOffs ? correct : wrong,
                partialData: packages.partialData ? correct : wrong,
                recordingDownloads: packages.recordingDownloads ? correct : wrong,
                heatmapDownloads: packages.heatmapDownloads ? correct : wrong,
                conversionAlerts: packages.conversionAlerts ? correct : wrong,
                utmTags: packages.utmTags ? correct : wrong,
                websiteVisitorSearch: packages.websiteVisitorSearch ? correct : wrong,
                shareRecording: packages.shareRecording ? correct : wrong,
                shareHeatmap: packages.shareHeatmap ? correct : wrong,
                fbCompaignBreakdown: packages.fbCompaignBreakdown ? correct : wrong,
                tags: packages.tags ? correct : wrong,
                deviceReporting: packages.deviceReporting ? correct : wrong,
                pageBreakdownReport: packages.pageBreakdownReport ? correct : wrong,
                support: packages.support ? correct : wrong,
                isCustom: packages.isCustom ? correct : wrong,
                packageInfo: packages
            }, (err, html) => {
                if (err) {
                    console.log(err);
                } else {
                    authProviders.send_email(req.body.email, 'Verify Your Custom Package is created.', html);
                }
            });
        }
        res.status(200).send({
            code: '200',
            data: packages,
            message: "Package created successfully"
        });
    } catch (error) {
        next(error);
    }
};
exports.update = async (req, res, next) => {
    try {
        const package = await Package.findById(req.body.packageId).exec();
        package.planeName = req.body.planeName !== undefined ? req.body.planeName : package.planeName;
        package.email = req.body.email !== undefined ? req.body.email : package.email;
        package.packageType = req.body.packageType !== undefined ? req.body.packageType : package.packageType;
        package.planeType = req.body.planeType !== undefined ? req.body.planeType : package.planeType;
        package.viewPerMonth = req.body.viewPerMonth !== undefined ? req.body.viewPerMonth : package.viewPerMonth;
        package.recordingPerMonth = req.body.recordingPerMonth !== undefined ? req.body.recordingPerMonth : package.recordingPerMonth;
        package.websites = req.body.websites !== undefined ? req.body.websites : package.websites;
        package.storage = req.body.storage !== undefined ? req.body.storage : package.storage;
        package.usPrice = req.body.usPrice !== undefined ? req.body.usPrice : package.usPrice;
        package.gbPrice = req.body.gbPrice !== undefined ? req.body.gbPrice : package.gbPrice;
        package.recordings = req.body.recordings !== undefined ? req.body.recordings : package.recordings;
        package.heatMaps = req.body.heatMaps !== undefined ? req.body.heatMaps : package.heatMaps;
        package.funnels = req.body.funnels !== undefined ? req.body.funnels : package.funnels;
        package.ips = req.body.ips !== undefined ? req.body.ips : package.ips;
        package.issueReports = req.body.issueReports !== undefined ? req.body.issueReports : package.issueReports;
        package.analytics = req.body.analytics !== undefined ? req.body.analytics : package.analytics;
        package.dropOffs = req.body.dropOffs !== undefined ? req.body.dropOffs : package.dropOffs;
        package.partialData = req.body.partialData !== undefined ? req.body.partialData : package.partialData;
        package.recordingDownloads = req.body.recordingDownloads !== undefined ? req.body.recordingDownloads : package.recordingDownloads;
        package.heatmapDownloads = req.body.heatmapDownloads !== undefined ? req.body.heatmapDownloads : package.heatmapDownloads;
        package.support = req.body.support !== undefined ? req.body.support : package.support;
        package.isCustom = req.body.isCustom !== undefined ? req.body.isCustom : package.isCustom;
        package.conversionAlerts = req.body.conversionAlerts !== undefined ? req.body.conversionAlerts : package.conversionAlerts;
        package.utmTags = req.body.utmTags !== undefined ? req.body.utmTags : package.utmTags;
        package.websiteVisitorSearch = req.body.websiteVisitorSearch !== undefined ? req.body.websiteVisitorSearch : package.websiteVisitorSearch;
        package.shareRecording = req.body.shareRecording !== undefined ? req.body.shareRecording : package.shareRecording;
        package.shareHeatmap = req.body.shareHeatmap !== undefined ? req.body.shareHeatmap : package.shareHeatmap;
        package.fbCompaignBreakdown = req.body.fbCompaignBreakdown !== undefined ? req.body.fbCompaignBreakdown : package.fbCompaignBreakdown;
        package.tags = req.body.tags !== undefined ? req.body.tags : package.tags;
        package.deviceReporting = req.body.deviceReporting !== undefined ? req.body.deviceReporting : package.deviceReporting;
        package.pageBreakdownReport = req.body.pageBreakdownReport !== undefined ? req.body.pageBreakdownReport : package.pageBreakdownReport
        package.discount = req.body.discount !== undefined ? req.body.discount : package.discount;
        package.isCustomAllowed = req.body.isCustomAllowed !== undefined ? req.body.isCustomAllowed : package.isCustomAllowed
        const packageUpdated = await package.save();
        if (req.body.isCustom === true && req.body.email !== undefined && packageUpdated !== undefined) {
            const link = `${BETADOMAIN}refferalLogin?isCP=true&cpId=${packageUpdated._id}`;
            const correct = "https://cdn3.iconfinder.com/data/icons/flat-actions-icons-9/792/Tick_Mark_Dark-512.png";
            const wrong = "https://cdn3.iconfinder.com/data/icons/flat-actions-icons-9/792/Close_Icon_Dark-512.png";
            const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'custom-package.html';
            consolidate.swig(templateFilePath, {
                linkCreated: link,
                recordings: package.recordings ? correct : wrong,
                heatMaps: package.heatMaps ? correct : wrong,
                funnels: package.funnels ? correct : wrong,
                ips: package.ips ? correct : wrong,
                issueReports: package.issueReports ? correct : wrong,
                analytics: package.analytics ? correct : wrong,
                dropOffs: package.dropOffs ? correct : wrong,
                partialData: package.partialData ? correct : wrong,
                recordingDownloads: package.recordingDownloads ? correct : wrong,
                heatmapDownloads: package.heatmapDownloads ? correct : wrong,
                conversionAlerts: package.conversionAlerts ? correct : wrong,
                utmTags: package.utmTags ? correct : wrong,
                websiteVisitorSearch: package.websiteVisitorSearch ? correct : wrong,
                shareRecording: package.shareRecording ? correct : wrong,
                shareHeatmap: package.shareHeatmap ? correct : wrong,
                fbCompaignBreakdown: package.fbCompaignBreakdown ? correct : wrong,
                tags: package.tags ? correct : wrong,
                deviceReporting: package.deviceReporting ? correct : wrong,
                pageBreakdownReport: package.pageBreakdownReport ? correct : wrong,
                support: package.support ? correct : wrong,
                isCustom: package.isCustom ? correct : wrong,
                packageInfo: packageUpdated
            }, (err, html) => {
                if (err) {
                    console.log(err);
                } else {
                    authProviders.send_email(req.body.email, 'Verify Your Custom Package is updated.', html);
                }
            });
        }
        res.status(200).send({
            code: 200,
            data: packageUpdated,
            message: "Packages has been updated."
        });
    } catch (error) {
        next(errorHandler(error, req, res));
    }
};
exports.delete = async (req, res, next) => {
    try {
        const package = await Package.deleteOne({ "_id": req.body.packageId })
        res.status(200).send({
            code: 200,
            data: [],
            message: "Package deleted with success ."
        });
    } catch (error) {
        next(errorHandler(error, req, res));
    }
};

exports.deleteMany = async (req, res, next) => {
    try {
        const packageId = req.body.packageIds;
        for (let index = 0; index < packageId.length; index++) {
            await Package.findOneAndDelete({ _id: packageId[index] }).exec();
        }
        res.status(200).send({
            code: 200,
            data: [],
            message: "Package deleted with success ."
        });
    } catch (error) {
        next(errorHandler(error, req, res));
    }
};

exports.saveRefill = async (req, res, next) => {
    try {
        const packages = await Package.create({
            planeName: req.body.planeName,
            usPrice: req.body.usPrice,
            gbPrice: req.body.gbPrice,
            viewPerMonth: req.body.viewPerMonth,
            recordingPerMonth: req.body.recordingPerMonth,
            planeType: req.body.planeType,
            packageType: req.body.packageType
        });
        res.status(200).send({
            code: '200',
            data: packages,
            message: "Refill Package created successfully"
        });
    } catch (error) {
        next(error);
    }
};
exports.getRefill = async (req, res, next) => {
    try {
        let filterQuery = req.query;
        const packages = await Package.getallRefillPackages(filterQuery);
        res.status(200).send({
            code: 200,
            data: packages,
            message: ""
        });
    } catch (error) {
        next(errorHandler(error, req, res));
    }
};
exports.getCustomPackage = async (req, res, next) => {
    try {
        const packages = await Package.getallCustomPackages();
        res.status(200).send({
            code: 200,
            data: packages,
            message: ""
        });
    } catch (error) {
        next(errorHandler(error, req, res));
    }
};
// Unsubscribe SMS AND Sell Package
exports.unSubscribe = async (req, res) => {
    try {
        let userId = req.body.userId;
        // let currentPackage = await UserConversionPackage.findOne({ $and: [{ userId: userId }, { isActive: true } ] }).sort({createdAt: -1}).limit(1).exec();
        await UserConversionPackage.remove({ userId: userId }).exec();
        // currentPackage.isActive = false;
        // currentPackage.save();
        // await SiteConversion.updateMany({ userId: userId }, { isActive: false }).exec();
        await SiteConversion.remove({ userId: userId }).exec();
        return res.send({ code: 200, data: 'User Unsubscribed succesfully.' });
    } catch (error) {
        return res.send(error);
    }
}