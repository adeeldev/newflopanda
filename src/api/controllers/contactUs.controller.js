const { adminEmail } = require('./../../config/vars');
const ContactUs = require('../models/contactUs.model');
const Users = require('../models/users.model');
const sgMail = require('@sendgrid/mail');
sgMail.setApiKey('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');

exports.post = async (req, res, next) => {
    try {
        let subject = req.body.subject;
        let message = req.body.message;
        var email = "";
        if (req.body.userId !== undefined) {
            let user = await Users.findById(req.body.userId).exec();
            email = user.email;
        } else {
            email = req.body.email;
        }
        const contactUs = await ContactUs.create({
            email: email,
            subject: subject,
            message: message
        });
        await send_email("support@flopanda.com", subject, message);
        res.status(200).send({
            code: 200,
            data: [],
            message: ""
        });
    } catch (error) {
        return res.status(400).send({
            code: 400,
            data: [],
            message: error.message
        });
    }
};
function send_email(to, subject, html) {

    const msg = {
        to: to,
        from: 'Flopanda <support@flopanda.com>',
        subject: subject,
        html: html
    };
    sgMail.send(msg, (err, result) => {
        if (err) {
            console.log(err);
        }
    });
}
