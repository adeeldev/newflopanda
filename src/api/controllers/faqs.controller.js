const Faqs = require('../models/Faqs.model');
var moment = require('moment');

exports.post = async (req, res, next) => {
    try {
        const faq = await Faqs.create({
            question: req.body.question,
            answer: req.body.answer
        });
        let resObj = {};
        resObj._id = faq._id;
        resObj.createdAt = moment(faq.createdAt).format('MMMM Do YYYY')
        res.status(200).send({
            code: 200,
            data: resObj,
            message: ""
        });
    } catch (error) {
        return res.status(400).send({
            code: 400,
            data: [],
            message: error.message
        });
    }
};
exports.put = async (req, res, next) => {
    try {
        const faq = await Faqs.findById(req.body.faqId).exec();
        faq.question = req.body.question;
        faq.answer = req.body.answer;
        const faqUpdated = await faq.save();
        res.status(200).send({
            code: 200,
            data: [],
            message: "Faq has been updated."
        });
    } catch (error) {
        return res.status(400).send({
            code: 400,
            data: [],
            message: error.message
        });
    }
};
exports.get = async (req, res, next) => {
    try {
        const faq = await Faqs.find({}).exec();
        let resObj = [];
        for (let i = 0; i < faq.length; i++) {
            resObj.push({
                _id: faq[i]._id,
                answer: faq[i].answer,
                question: faq[i].question,
                createdAt: moment(faq[i].createdAt).format('MMMM Do YYYY')
            })
        }
        res.status(200).send({
            code: 200,
            data: resObj,
            message: ""
        });
    } catch (error) {
        return res.status(400).send({
            code: 400,
            data: [],
            message: error.message
        });
    }
};
exports.delete = async (req, res, next) => {
    try {
        const faq = await Faqs.deleteOne({ "_id": req.body.faqId })
        res.status(200).send({
            code: 200,
            data: [],
            message: "Faq deleted with success ."
        });
    } catch (error) {
        return res.status(400).send({
            code: 400,
            data: [],
            message: error.message
        });
    }
};

