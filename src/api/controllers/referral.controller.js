const Referral = require('../models/referral.model');
const Users = require('../models/users.model');
const Payment = require('../models/payment.model');
const mongoose = require('mongoose');
var moment = require('moment');
const { _ } = require('underscore');

exports.createPageView = async (req, res, next) => {
    try {
        console.log(req.body.userId)
        const referral = await Referral.create({
            userId: req.body.userId,
        });
        res.status(200).send({
            code: 200,
            data: referral,
            message: "Page view has been created."
        });
    } catch (error) {
        return res.status(400).send({
            code: 400,
            data: [],
            message: error.message
        });
    }
};

exports.referralStats = async (req, res, next) => {
    try {
        const from = req.params.from;
        const to = req.params.to;
        const fromDate = new Date(from);
        const toDate = new Date(to);
        var timeDiff = Math.abs(fromDate.getTime() - toDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        const queryData = req.query;
        queryData.createdAt = {
            $gte: fromDate,
            $lte: toDate
        };
        let paidAmountQuery = {};
        let expectedPayoutQuery = {}
        paidAmountQuery.referralId = req.params.userId;
        paidAmountQuery.isPaid = true;
        paidAmountQuery["paidAmount.date"] = {
            $gte: fromDate,
            $lte: toDate
        };
        expectedPayoutQuery.referralId = req.params.userId;
        expectedPayoutQuery.isPaid = true;
        // expectedPayoutQuery.billingDate = {
        //     $gte: fromDate,
        //     $lte: toDate
        // };
        if (req.query.email) {
            paidAmountQuery.email = req.query.email
            expectedPayoutQuery.email = req.query.email
        }
        queryData.userId = new mongoose.Types.ObjectId(req.params.userId);
        const referral = await Referral.aggregate([{
            $match: {
                $and: [
                    queryData
                ]
            }
        },
        {
            "$facet": {
                "pageViews": [
                    { "$match": { "userId": new mongoose.Types.ObjectId(req.params.userId) } },
                    { "$count": "pageViews" },
                ],
                "referrals": [
                    { "$match": { "converted": true, "userId": new mongoose.Types.ObjectId(req.params.userId) } },
                    { "$count": "referrals" }
                ]

            }
        },
        {
            "$project": {
                "pageViews": { "$arrayElemAt": ["$pageViews.pageViews", 0] },
                "referrals": { "$arrayElemAt": ["$referrals.referrals", 0] },
            }
        }
        ]);
        queryData.converted = true;
        const expectedPayout = await Users.aggregate([
            {
                $match: {
                    $and: [
                        expectedPayoutQuery
                    ]
                }
            },
            {
                $group:
                {
                    _id: 1,
                    usPriceCP: { "$sum": "$usPriceCP" },
                    gbPriceCP: { "$sum": "$gbPriceCP" }
                }

            }
        ]).exec();
        const paidAmount = await Users.aggregate([
            {
                $match: {
                    $and: [
                        paidAmountQuery
                    ]
                }
            },
            {
                $group:
                {
                    _id: 1,
                    paidAmount: {
                        "$push": {
                            "$cond": [
                                { "$gte": ["$paidAmount.amount", 0] },
                                { "amount": { $sum: "$paidAmount.amount" } },
                                0
                            ]
                        }
                    }
                }
            }, {
                '$unwind': {
                    'path': '$paidAmount'
                }
            }, {
                '$group': {
                    '_id': 1,
                    'paidAmount': {
                        '$sum': '$paidAmount.amount'
                    }
                }
            }
        ]).exec();
        let resObj = {}
        if (referral.length > 0) {
            resObj.pageViews = referral[0].pageViews ? referral[0].pageViews : 0;
            resObj.referrals = referral[0].referrals ? referral[0].referrals : 0;
        }
        if (paidAmount.length > 0) {
            resObj.paidAmount = paidAmount[0].paidAmount ? paidAmount[0].paidAmount : 0;
        }
        if (expectedPayout.length > 0) {
            resObj.expectedUsPayout = expectedPayout[0].usPriceCP ? expectedPayout[0].usPriceCP : 0;
            resObj.expectedGbPayout = expectedPayout[0].gbPriceCP ? expectedPayout[0].gbPriceCP : 0;
        }
        res.status(200).send({
            code: 200,
            data: resObj,
            message: ""
        });
    } catch (error) {
        return res.status(400).send({
            code: 400,
            data: [],
            message: error.message
        });
    }
};

exports.convertedReferralUser = async (req, res, next) => {
    try {
        const from = req.params.from;
        const to = req.params.to;
        const fromDate = new Date(from);
        const toDate = new Date(to);
        var timeDiff = Math.abs(fromDate.getTime() - toDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        const limit = parseInt(req.params.limit);
        const offset = parseInt(req.params.offset * 20);
        let sort = {};
        sort.createdAt = -1;
        const queryData = req.query;
        queryData.createdAt = {
            $gte: fromDate,
            $lte: toDate
        };
        queryData.userId = new mongoose.Types.ObjectId(req.params.userId);
        queryData.converted = true;
        const total = await Referral.countDocuments(queryData).sort(sort).exec();
        const referral = await Referral.aggregate([
            {
                $match: {
                    $and: [
                        queryData
                    ]
                }
            }, {
                $lookup:
                {
                    from: 'payments',
                    let: { referralUserId: "$referralUserId" },
                    pipeline: [
                        {
                            $match:
                            {
                                $expr:
                                {
                                    $and:
                                        [
                                            { $eq: ["$userId", "$$referralUserId"] },
                                            { $eq: ["$paymentSuccess", true] },
                                            { $gte: ["$createdAt", fromDate] },
                                            { $lte: ["$createdAt", toDate] }

                                        ]
                                }
                            }
                        }, {
                            $project: {
                                price: { "$multiply": [{ "$divide": ["$price", 100] }, 15] }
                            }
                        }
                    ],
                    as: 'paidAmount'
                }
            }, {
                $project: {
                    "email": 1,
                    "converted": 1,
                    "accountType": 1,
                    "packageId": 1,
                    "packageName": 1,
                    "userId": 1,
                    "createdAt": 1,
                    "billingDate": 1,
                    price: { $sum: "$paidAmount.price" }
                }
            }]).skip(offset).limit(limit).exec();
        res.status(200).send({
            code: 200,
            data: referral,
            total,
            message: ''
        });
    } catch (error) {
        return res.status(400).send({
            code: 400,
            data: [],
            message: error.message
        });
    }
}

exports.referralGraph = async (req, res, next) => {
    try {
        const from = req.params.from;
        const to = req.params.to;
        const fromDate = new Date(from);
        const toDate = new Date(to);
        var timeDiff = Math.abs(fromDate.getTime() - toDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        const queryData = req.query;
        queryData.createdAt = {
            $gte: fromDate,
            $lte: toDate
        };
        queryData.userId = new mongoose.Types.ObjectId(req.params.userId);
        const data = await Referral.aggregate(
            [
                {
                    $match: {
                        $and: [
                            queryData
                        ]
                    }
                },
                {
                    $group: {
                        _id: {
                            yearMonthDayUTC: {
                                $dateToString: {
                                    format: "%Y/%m/%d",
                                    date: "$createdAt"
                                }
                            }
                        },
                        total: {
                            $sum: 1
                        },
                        converted: {
                            $sum: {
                                $cond: { if: { $eq: ['$converted', true] }, then: 1, else: 0 }
                            }
                        }
                    }
                }
            ]
            , (err, result) => {
                if (err) {
                    console.log(err);
                }
                return result;
            });
        const paidAmount = await Users.aggregate([[
            {
                '$match': {
                    'referralId': req.params.userId,
                    'isPaid': true,
                    'paidAmount.date': {
                        '$gte': fromDate,
                        '$lte': toDate
                    }
                }
            }, {
                '$unwind': {
                    'path': '$paidAmount'
                }
            }, {
                '$group': {
                    '_id': {
                        'yearMonthDayUTC': {
                            '$dateToString': {
                                'format': '%Y/%m/%d',
                                'date': '$paidAmount.date'
                            }
                        }
                    },
                    'total': {
                        '$sum': '$paidAmount.amount'
                    }
                }
            }
        ]])
        const dates = [];
        const counts = [];
        const amount = [];
        const converted = [];
        for (let i = 0; i < diffDays; i++) {
            let newFromDate;
            newFromDate = new Date(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate() + i);
            newFromDate.setHours(23, 59, 59, 999);
            let pushDate = moment(newFromDate).format('YYYY/MM/DD');
            let index = _.filter(data, function (num) {
                let nnnDate = new Date(num._id.yearMonthDayUTC);
                nnnDate.setHours(23, 59, 59, 999);
                var diff = (newFromDate - nnnDate)
                if (diff === 0) {
                    return num;
                }
            });
            let index2 = _.filter(paidAmount, function (num) {
                let nnnDate = new Date(num._id.yearMonthDayUTC);
                nnnDate.setHours(23, 59, 59, 999);
                var diff = (newFromDate - nnnDate)
                if (diff === 0) {
                    return num;
                }
            });
            if (index.length > 0) {
                counts.push(index[0].total);
                converted.push(index[0].converted)
                dates.push(index[0]._id.yearMonthDayUTC);
            } else {
                dates.push(pushDate);
                counts.push(0);
                converted.push(0)
            }
            if (index2.length > 0) {
                amount.push(index2[0].total);
            } else {
                amount.push(0);
            }
        }
        return res.status(200).send({
            code: 200,
            converted,
            dates: dates,
            amount,
            counts: counts
        });
    } catch (error) {
        next(error);
    }
};


