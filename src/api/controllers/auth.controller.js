const httpStatus = require('http-status');
const Users = require('../models/users.model');
const Website = require('../models/website.model');
const RefreshToken = require('../models/refreshToken.model');
const Package = require('../models/packages.model');
const Referral = require('../models/referral.model');
const moment = require('moment-timezone');
const {
  jwtExpirationInterval,
  DOMAIN,
  TRAKING_CODE_SCRIPT
} = require('../../config/vars');
const consolidate = require('consolidate');
const authProviders = require('../services/emailProvider');
const fs = require('fs');
const path = require('path');
const speakeasy = require('speakeasy');
const QRCode = require('qrcode');
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const Terser = require("terser");
const AWS = require('aws-sdk');
const Siteids = require('../models/siteid.model');
var generatorPassword = require('generate-password');
const s3 = new AWS.S3({
  accessKeyId: 'AKIAI6FN6GALLBUTYK6Q',
  secretAccessKey: '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'
});

/**
 * Returns a formated object with tokens
 * @private
 */
async function generateTokenResponse(user, accessToken) {
  const tokenType = 'Bearer';
  const isLogin = await RefreshToken.find({ userId: user._id, isCreatedByAdmin: false }).exec();
  if (isLogin.length > 0) {
    await RefreshToken.remove({ userId: user._id, isCreatedByAdmin: false }).exec();
  }
  const refreshToken = await RefreshToken.generate(user, accessToken).token;
  const expiresIn = moment().add(jwtExpirationInterval, 'minutes');
  return {
    tokenType,
    accessToken,
    refreshToken,
    expiresIn,
  };
}
async function generateUserTokenResponseForAdmin(user, accessToken) {
  const tokenType = 'Bearer';
  const isLogin = await RefreshToken.find({ userId: user._id, isCreatedByAdmin: true }).exec();
  if (isLogin.length > 0) {
    await RefreshToken.remove({ userId: user._id, isCreatedByAdmin: true }).exec();
  }
  const refreshToken = await RefreshToken.generateUserTokenResponseForAdmin(user, accessToken).token;
  const expiresIn = moment().add(jwtExpirationInterval, 'minutes');
  return {
    tokenType,
    accessToken,
    refreshToken,
    expiresIn,
  };
}
/**
 * Returns jwt token if registration was successful
 * @public
 */
exports.register = async (req, res, next) => {
  try {
    var freePackage = await Package.findOne({ "planeName": "Free" }).select('_id planeName usPrice gbPrice').exec();
    const checkUserExistance = await Users.findOne({ email: req.body.email, "isDeleted": false }).exec();
    if (checkUserExistance) {
      return res.status(409).json({ code: 409, message: 'User already exist with this email' });
    } else {
      const newUser = new Users(req.body);
      if (req.body.isFromBeta == true) {
        // freePackage = await Package.findOne({ planeName: 'Platinum', planeType: 'monthly' }).select('_id planeName usPrice gbPrice').exec();
        if (req.body.referralId) {
          let referral = await Referral.findById(req.body.referralId).exec();
          if (referral) {
            referral.email = req.body.email;
            referral.packageId = freePackage._id;
            referral.packageName = freePackage.planeName;
            referral.accountType = 'Free';
            referral.converted = true;
            referral.billingDate = moment().add(1, 'months').format('YYYY-MM-DD');
            newUser.referralId = referral.userId;
            referral.save();
          }
        }
      }
      newUser.usPriceCP = percentage(freePackage.usPrice, 15);
      newUser.gbPriceCP = percentage(freePackage.gbPrice, 15);
      newUser.billingDate = moment().add(1, 'months').format('YYYY-MM-DD');
      newUser.packageId = freePackage._id;
      newUser.loginCount = 1;
      const user = await newUser.save();
      if (req.body.isFromBeta == true) {
        if (req.body.referralId) {
          await Referral.findOneAndUpdate({
            _id: req.body.referralId
          }, {
            $set: {
              referralUserId: user.id
            }
          }, {
            new: true
          }).exec();
        }
      }
      const link = `${DOMAIN}/verifyAccount/${user.id}`;
      const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'email-confirmation.html';
      consolidate.swig(templateFilePath, {
        linkCreated: link,
      }, (err, html) => {
        if (err) {
          console.log(err);
        } else {
          authProviders.send_email(req.body.email, 'Verify Your Account', html);
        }
      });
      const newWeb = new Website({
        name: req.body.domainName,
        userId: user.id,
        protocol: req.body.protocol
      });
      const website = await newWeb.save();
      user.website.push(newWeb._id);
      await user.save();
      let script = '<script type="text/javascript"> window._pfq = window._pfq || [];(function() {var pf = document.createElement("script");pf.type = "text/javascript";pf.id = "' + website._id + '";pf.dataset.name ="flopandaScript";pf.async = true;pf.src ="https://flopandascripts.s3.eu-west-2.amazonaws.com/assets/js/' + TRAKING_CODE_SCRIPT + '";document.getElementsByTagName("head")[0].appendChild(pf);})();</script>';
      website.scriptHeader = script;
      website.domain.push(website.name);
      // const fileData = await readFile(website._id);
      // website.script = 'var siteId = "' + website._id + '";\n' + fileData;
      await website.save();


      var siteIdsArray = [];
      let currentUserSites = await Website.find({ userId: user.id }).select('_id').exec();
      currentUserSites.forEach(function (site) {
        siteIdsArray.push(site._id.toHexString());
      });

      await Siteids.updateOne(
        {
          userId: user.id
        },
        {
          siteIds: siteIdsArray,
          userId: user.id
        },
        {
          upsert: true
        });


      const userTransformed = user.transform();
      const token = await generateTokenResponse(user, user.token());
      res.status(httpStatus.CREATED);
      return res.status(200).json({ code: 200, token, user: userTransformed });
    }
  } catch (error) {
    return next(Users.checkDuplicateEmail(error));
  }
};

exports.registerAdmin = async (req, res, next) => {
  try {
    const checkUserExistance = await Users.findOne({ email: req.body.email, "isDeleted": false }).exec();
    if (checkUserExistance) {
      return res.status(409).json({ code: 409, message: 'User already exist with this email' });
    } else {
      var token1 = crypto.randomBytes(48).toString('hex');
      const newUser = new Users({
        ...req.body,
        verified: true,
        resetPasswordExpires: Date.now() + 60000 * 60,
        resetPasswordToken: token1
      });
      newUser.loginCount = 1;
      newUser.password = crypto.randomBytes(48).toString('hex');
      const user = await newUser.save();
      const userType = req.body.roles === '4' ? 'Support' : req.body.roles === '5' ? 'Super Admin' : '';
      const logoLink = `${DOMAIN}`;
      const link = `${DOMAIN}/admin/resetPassword/${token1}`;
      const loginLink = `${DOMAIN}/admin/login`;
      const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'admin-role-assign.html';
      consolidate.swig(templateFilePath, {
        linkCreated: link,
        logoLink: logoLink,
        loginLink: loginLink,
        userType: userType,
        email: req.body.email,
        // password: req.body.password
      }, (err, html) => {
        if (err) {
          console.log(err);
        } else {
          authProviders.send_email(req.body.email, 'Flopanda Login Details', html);
        }
      });
      const userTransformed = user.transform();
      res.status(httpStatus.CREATED);
      return res.status(200).json({ code: 200, user: userTransformed });
    }
  } catch (error) {
    return next(Users.checkDuplicateEmail(error));
  }
};

/**
 * Returns jwt token if valid username and password is provided
 * @public
 */
exports.login = async (req, res, next) => {
  try {
    var {
      user,
      accessToken
    } = await Users.findAndGenerateToken(req.body);
    let updateObj = {
      loginCount: (user.loginCount + 1)
    }
    if (user && req.body.isFromBeta) {
      updateObj.isFromBeta = true;
    }
    await Users.findOneAndUpdate({
      _id: user.id
    }, {
      $set: updateObj
    }, {
      new: true
    }).exec();
    const token = await generateTokenResponse(user, accessToken);
    await Users.findOneAndUpdate({ _id: user.id }, { accessToken: accessToken });
    if (req.body.role === '2') {
      await Users.findOneAndUpdate({ _id: user.id }, { resetPasswordExpires: token.expiresIn, resetPasswordToken: token.refreshToken });
      user = await Users.findById(user.id);
    }
    const CustomPackage = await Package.findOne({ email: req.body.email, isCustomAllowed: false }).select('_id').exec();
    var userTransformed = user.transform();
    userTransformed.loginCount = user.loginCount + 1
    return res.json({
      code: 200,
      token,
      user: userTransformed,
      isCustomPackage: CustomPackage ? true : false,
      customPackage: CustomPackage
    });
  } catch (error) {
    console.log(error);
    return next(error);
  }
};
exports.userLoginForAdmin = async (req, res, next) => {
  try {
    await Users.matchPassword(req.user, req.body.password);
    var {
      user,
      accessToken
    } = await Users.generateTokenByEmail({ email: req.body.email });
    const token = await generateUserTokenResponseForAdmin(user, accessToken);
    await Users.findOneAndUpdate({ _id: user.id }, { accessToken: accessToken });
    if (req.body.role === '2') {
      await Users.findOneAndUpdate({ _id: user.id }, { resetPasswordExpires: token.expiresIn, resetPasswordToken: token.refreshToken });
      user = await Users.findById(user.id);
    }
    const CustomPackage = await Package.findOne({ email: req.body.email, isCustomAllowed: false }).select('_id').exec();
    const userTransformed = user.transform();
    return res.json({
      code: 200,
      token,
      user: userTransformed,
      isCustomPackage: CustomPackage ? true : false,
      customPackage: CustomPackage
    });
  } catch (error) {
    console.log(error);
    return next(error);
  }
}
exports.update = async (req, res, next) => {
  try {
    const user = await Users.findOneAndUpdate({
      _id: req.params.userId
    }, {
      $set: req.body
    }, {
      new: true
    }).exec();
    return res.json({
      code: 200,
      user: user
    });
  } catch (error) {
    return next(error);
  }
};


exports.betalogin = async (req, res, next) => {
  try {
    const {
      user,
      accessToken
    } = await Users.findAndGenerateTokenForBeta(req.body);
    if (user) {
      await Users.findOneAndUpdate({
        _id: user.id
      }, {
        $set: {
          isFromBeta: false,
        }
      }, {
        new: true
      }).exec();
      const token = await generateTokenResponse(user, accessToken);
      const CustomPackage = await Package.findOne({ email: req.body.email, isCustomAllowed: false }).select('_id').exec();
      var userTransformed = user.transform();
      return res.json({
        code: 200,
        token,
        user: userTransformed,
        isCustomPackage: CustomPackage ? true : false,
        customPackage: CustomPackage
      });
    }

  } catch (error) {
    return next(error);
  }
};

exports.logout = async (req, res, next) => {
  try {
    res.json({
      code: 200,
      message: "Logout Successfull"
    });
    let query = { userId: new mongoose.Types.ObjectId(req.user) };
    if (req.headers['ua']) {
      query.isCreatedByAdmin = true;
    } else {
      query.isCreatedByAdmin = false;
    }
    await RefreshToken.deleteOne(query);
  } catch (error) {
    return next(error);
  }
};
/**
 * login with an existing user or creates a new one if valid accessToken token
 * Returns jwt token
 * @public
 */
exports.oAuth = async (req, res, next) => {
  try {
    const {
      user
    } = req;
    const accessToken = user.token();
    const token = await generateTokenResponse(user, accessToken);
    const userTransformed = user.transform();
    return res.json({
      token,
      user: userTransformed
    });
  } catch (error) {
    return next(error);
  }
};

/**
 * Returns a new jwt when given a valid refresh token
 * @public
 */
exports.refresh = async (req, res, next) => {
  try {
    const {
      email,
      refreshToken
    } = req.body;
    const refreshObject = await RefreshToken.findOneAndRemove({
      userEmail: email,
      token: refreshToken,
    });
    const {
      user,
      accessToken
    } = await Users.findAndGenerateToken({
      email,
      refreshObject
    });
    const response = await generateTokenResponse(user, accessToken);
    return res.json(response);
  } catch (error) {
    return next(error);
  }
};

/**
 * Send reset password link if user exist
 * @public
 */
exports.forgetPassword = async (req, res, next) => {
  try {
    const {
      user
    } = await Users.checkUserExistance(req.body, "forgetPassword");
    const token = await generateTokenResponse(user, user.token());
    var token1 = await crypto.randomBytes(48).toString('hex');
    await Users.findOneAndUpdate({ _id: user._id }, { resetPasswordExpires: Date.now() + 60000 * 60, resetPasswordToken: token1 });
    const link = `${DOMAIN}/resetPassword/${token1}`;
    const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'email-reset-password.html';
    consolidate.swig(templateFilePath, {
      linkCreated: link,
    }, (err, html) => {
      if (err) {
        console.log(err);
      } else {
        authProviders.send_email(req.body.email, 'Reset your password', html);
      }
    });
    return res.status(200).json({
      code: 200,
      data: [],
      message: "Further instructions have been sent to your email account."
    });
  } catch (error) {
    if (error.message === 'Invalid email') {
      return res.status(200).json({
        code: 200,
        data: [],
        message: "Further instructions have been sent to your email account."
      });
    } else {
      return res.status(error.status).send({
        code: error.status,
        data: [],
        message: error.message
      });
    }

  }
};

exports.generateUserByEmail = async (req, res, next) => {
  try {
    const freePackage = await Package.findOne({ planeName: 'Free' }).select('_id').exec();
    const checkUserExistance = await Users.findOne({ email: req.body.email, "isDeleted": false }).exec();
    if (checkUserExistance) {
      return res.status(409).json({ code: 409, message: 'User already exist with this email' });
    } else {
      let password = generatorPassword.generate({
        length: 10,
        numbers: true
      })
      const newUser = new Users({
        email: req.body.email,
        password: password,
        packageId: freePackage._id
      });
      const user = await newUser.save();
      const link = `${DOMAIN}/verifyAccount/${user.id}`;
      const templateFilePath = path.join(__dirname, '../uploadedFiles/') + 'email-confirmation-with-password.html';
      consolidate.swig(templateFilePath, {
        linkCreated: link,
        password: password
      }, (err, html) => {
        if (err) {
          console.log(err);
          return res.status(400).json({ code: 400, message: err });
        } else {
          authProviders.send_email(req.body.email, 'Verify Your Account', html);
          res.status(httpStatus.CREATED);
          return res.status(200).json({ code: 200, message: 'User has been created successfully' });
        }
      });

    }
  } catch (error) {
    return next(Users.checkDuplicateEmail(error));
  }
}

/**
 * Validate token for reset password
 * @public
 */
exports.validateToken = async (req, res, next) => {
  try {
    // const {
    //   user
    // } = await Users.checkUserExistance(req.body, "validateToken");
    const user = await Users.findOne({ resetPasswordToken: req.body.token });
    if (user) {
      if (req.body.token === user.resetPasswordToken) {
        if (user.resetPasswordExpires < Date.now()) {
          res.status(400).send({
            code: 400,
            data: [],
            message: "Token has been expired"
          });
        } else {
          res.status(200).send({
            code: 200,
            data: [],
            message: "Token is verified"
          })
        }
      } else {
        return res.status(400).send({
          code: 400,
          message: "Token is not valid Test"
        });
      }
    } else {
      return res.status(400).send({
        code: 400,
        message: "Token is not valid"
      });
    }

  } catch (error) {
    return res.status(error.status).send({
      code: error.status,
      data: [],
      message: error.message
    });
  }
};


/**
 * Reset user password
 * @public
 */
exports.resetPassword = async (req, res, next) => {
  try {
    const user = await Users.findOne({ resetPasswordToken: req.body.token });
    if (user.resetPasswordExpires < Date.now()) {
      return res.status(400).send({
        code: 400,
        data: [],
        message: "Token has been expired"
      });
    } else {
      user.password = req.body.password;
      user.resetPasswordToken = Date.now();
      const updatedUser = await user.save();
      return res.status(200).json({
        code: 200,
        data: [],
        message: "Your password has been updated"
      });
    }

  } catch (error) {
    return res.status(error.status).send({
      code: error.status,
      data: [],
      message: error.message
    });
  }
};


/**
 * Update 2FA
 * @public
 */
exports.update2FA = async (req, res, next) => {
  try {
    var secret = speakeasy.generateSecret();
    var user = await Users.findOne({
      _id: req.body.userId
    }).exec();
    var otp = await speakeasy.otpauthURL({
      secret: secret.base32,
      label: user.email,
      issuer: 'FloPanda',
      encoding: 'base32'
    });
    const data_url = await QRCode.toDataURL(otp);
    user.base32Secret = secret.base32;
    await user.save();
    return res.status(200).json({
      code: 200,
      data: {
        url: data_url
      },
      message: ""
    });
  } catch (error) {
    next(error);
  }
};

/**
 * Validate 2FA
 * @public
 */
exports.validate2FA = async (req, res, next) => {
  try {
    const userToken = req.body.userToken;
    const user = await Users.findById(req.body.userId).exec();
    var base32secret = user.base32Secret;
    var tokenValidates = speakeasy.totp.verify({
      secret: base32secret,
      encoding: 'base32',
      token: userToken,
      window: 6
    });
    if (tokenValidates === true) {
      user.twoFactorAuthentication = true;
      user.save();
    }
    return res.status(200).json({
      code: 200,
      data: {
        verified: tokenValidates
      },
      message: ""
    });
  } catch (error) {
    return next(error);
  }
};

exports.delete = async (req, res, next) => {
  try {
    // await Users.where({
    //   _id: req.params.userId
    // }).findOneAndRemove();
    await Users.updateOne({ _id: req.params.userId }, {
      $set: {
        isDeleted: true
      }
    })
    await Siteids.where({
      userId: req.params.userId
    }).findOneAndRemove();
    return res.json({
      code: 200,
      data: 'User deleted successfully.'
    });
  } catch (error) {
    return next(error);
  }
};



exports.verifyUser = async (req, res, next) => {
  try {
    const user = await Users.findOneAndUpdate({ _id: req.params.id }, { verified: true }, { new: 1 }).exec();
    return res.json({ code: 200, data: 'User verified successfully.' });
  } catch (error) {
    return next(error);
  }
};

async function readFile(siteId) {
  var jsonPath = path.join(__dirname, '../', 'uploadedFiles/', 'script.js');
  var scriptName = 'projects/' + siteId + '.js';
  return new Promise((resolve, reject) => {
    fs.readFile(jsonPath, "utf8", (err, data) => {
      if (err) reject(err);
      else {
        var params = {
          Bucket: 'flopandascripts',
          Key: scriptName,
          ContentType: 'text/javascript'
        };

        // data = 'var siteId = "' + siteId + '";\n' + data;
        var result = Terser.minify(data);
        if (result.error) {
          params.Body = data;
        }
        else {
          params.Body = result.code;
        }
        s3.upload(params, function (s3Err, data) {
          if (s3Err) {
            throw s3Err;
          }
          else {
            resolve(data);
          }
        });
      }
    });
  });
}


exports.updateEmail = async (req, res, next) => {
  try {
    const user = await Users.findOne({
      _id: req.body.userId
    }).exec();
    if (req.body.oldEmail === user.email) {
      const checkUser = await Users.findOne({
        email: req.body.newEmail
      }).exec();
      if (checkUser === null) {
        let checkPassword = await bcrypt.compare(req.body.password, user.password);
        if (checkPassword) {
          user.email = req.body.newEmail;
          const userUpdated = await user.save();
          return res.json({
            code: 200,
            data: 'Email updated successfully'
          })
        } else {
          return res.json({
            code: 400,
            data: "password is incorrect"
          })
        }

      } else {
        return res.json({
          code: 400,
          data: 'User already exist with your new email'
        })
      }
    } else {
      return res.json({
        code: 400,
        data: 'Old email is incorrect'
      });
    }
  } catch (error) {
    next(error);
  }
};
exports.updatePassword = async (req, res, next) => {
  try {
    const user = await Users.findOne({
      _id: req.body.userId
    }).exec();
    let checkPassword = await bcrypt.compare(req.body.oldPassword, user.password);
    if (checkPassword) {
      user.password = req.body.newPassword;
      const updatedUser = await user.save();
      return res.json({
        code: 200,
        data: "Password updated successfully"
      })
    } else {
      return res.json({
        code: 400,
        data: "Old password is incorrect"
      })
    }
  } catch (error) {
    next(error);
  }
}

function percentage(num, per) {
  return (num / 100) * per;
}

exports.checkExpirePackage = async (req, res, next) => {
  try {
    const user = await Users.aggregate([
      {
        '$lookup': {
          'from': 'packages',
          'localField': 'packageId',
          'foreignField': '_id',
          'as': 'packageDetail'
        }
      }, {
        '$unwind': {
          'path': '$packageDetail'
        }
      }, {
        '$project': {
          '_id': 1,
          'packageUpdateDate': 1,
          'email': 1,
          'planeType': '$packageDetail.planeType',
          'planeName': '$packageDetail.planeName',
          'test123': new Date("Nov 7, 2003").setMonth(new Date("Nov 7, 2003").getMonth() + 1),
          'billingDate': {
            '$cond': {
              'if': {
                '$eq': [
                  '$packageDetail.planeType', 'monthly'
                ]
              },
              'then': test("$packageUpdateDate"),
              'else': moment("2019-09-24T07:31:00.382Z").add(12, 'months').format('YYYY-MM-DD')
            }
          }
        }
      }
    ]).exec();
    return res.json({ code: 200, data: user });
  } catch (error) {
    return next(error);
  }
};

function test(params) {
  return moment(new Date(params)).add(1, 'months').format('YYYY-MM-DD')
}