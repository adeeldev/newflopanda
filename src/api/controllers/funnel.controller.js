const Funnel = require('../models/funnel.model');
const Page = require('../models/page.model');
const Session = require('../models/session.model');
const Recording = require('../models/recording.model');
const FunnelStats = require('../models/funnelStat.model');
const Users = require('../models/users.model');
const httpStatus = require('http-status');
const moment = require('moment-timezone');
const { _ } = require('underscore');
const cron = require('node-cron');
const curl = new (require( 'curl-request' ))();
const mongoose = require('mongoose');
const Funnelfilter = require('../models/funnelfilter.model');
const Website = require('../models/website.model');
const Siteids = require('../models/siteid.model');
const { FUNNEL_CRON_ADDRESS } = require('../../config/vars');

//Check if user is sending its own website.
async function checkWebsites(siteId = 0, userId = 0, owner=1) {
  var siteIdsArray = [];
  
  if (!userId) {
    return false;
  }

  if (!siteId) {
    return false;
  }
  let currentUserSites = await Siteids.find({ userId: userId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
  if(currentUserSites.length)
  {
    return true;
  }
  else if(owner !== 1)
  {
    let currentUserOwnerId = await Users.findOne({ _id: userId}, {ownerId:1}).select('siteIds').exec();
    if(currentUserOwnerId && currentUserOwnerId.ownerId)
    {
      let currentUserSitesAgain = await Siteids.find({ userId: currentUserOwnerId.ownerId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
      if(currentUserSitesAgain.length)
      {
        return true;
      }
      else
      {
        return false;    
      }
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

exports.getFunnelData = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    let today = moment().format('YYYY-MM-DD');
    const siteId = req.params.siteId;
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    let queryData = req.query;
    queryData.siteId = siteId;
    queryData.createdAt = {
      $gt: fromDate,
      $lt: toDate
    };
    const funnel = await Funnel.find({siteId: siteId}).exec();
    const funnelTrackData = [];
    for (let i = 0; i < funnel.length; i++) {
      queryData.pageName; 
      queryData.pageName = funnel[i].pages[0].pageName;
      queryData.type = 'online';
      const filtersRecordings = await Session.find({
        $and: [queryData]
      }).populate('recordings', 'pageName').exec();

      let recordings = filtersRecordings;
      const data = [];
      let conversion = 0;
      let visit = 0;
      let firstPageData = 0;
      let secondPage = 0;
      let newData = [];
      let dropped = [];
      let converted = [];
      data.push(funnel[i]);
      let VisitObj = {}
      for (let index = 0; index < funnel[i].pages.length; index++) {
        VisitObj[index] = 0;
      }
      recordings.forEach((recording) => {
        const recordingArray = _.map(recording.funnelPages, function (value, key) {
          if (key < funnel[i].pages.length) {
            return pageName = {
              pageName: value
            };
          }
        });
        const filterFunnelArray = _.filter(recordingArray, function (record) {
          if (record !== undefined) {
            return record;
          }
        });
        const diff = _.isEqual(filterFunnelArray, funnel[i].pages);
        if (diff === true) {
          conversion += 1;
          visit += 1;
          let pageVisit = 0;
          for (let index = 0; index < Object.keys(VisitObj).length; index++) {
            VisitObj[index] = VisitObj[index] + 1;
          }
          pageVisit += 1;
        } else {
          const funnelPagesData = funnel[i].pages;
          const filterFUnnelData  = _.filter(funnelPagesData, function (record) {
            if (record !== undefined) {
                return record;
            }
            });
          for (let d = 0; d < filterFunnelArray.length; d++) {
            if (filterFunnelArray[d].pageName === filterFUnnelData[d].pageName) {
              VisitObj[d] += 1;
            } else {
              break;
            }
          }
          if (recordingArray[0].pageName === funnel[i].pages[0].pageName) {
            firstPageData += 1;
            visit += 1;
          }
        }
        const funnelPages = funnel[i].pages;
        a = VisitObj;
      });
      funnelTrackData.push({
        funnelId: funnel[i]._id,
        conversion: conversion,
        visit: visit,
        VisitObj: VisitObj,
        firstPageInfo: firstPageData,
        secondPage: secondPage,
        data: data,
        dropped: dropped,
        converted: converted
      });

    }
    return res.send({
      code: 200,
      funnelTrackData
    });
  } catch (error) {
    next(error);
  }
};

exports.getFunnelDataCron = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const siteId = req.params.siteId;
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    let queryData = req.query;
    queryData.siteId = new mongoose.Types.ObjectId(siteId);
    queryData.funnelDate = {
      $gte: fromDate,
      $lte: toDate
    };
    let funnelData = await Funnelfilter.aggregate([
      {
        $match : queryData
      },
      {$unwind:"$funnelDataArray"},
      {
        $group:
        {
          _id:"$funnelDataArray.funnelId",
          VisitObj0:{$sum:"$funnelDataArray.VisitObj.0"},
          VisitObj1:{$sum:"$funnelDataArray.VisitObj.1"},
          VisitObj2:{$sum:"$funnelDataArray.VisitObj.2"},
          VisitObj3:{$sum:"$funnelDataArray.VisitObj.3"},
          VisitObj4:{$sum:"$funnelDataArray.VisitObj.4"},
          VisitObj5:{$sum:"$funnelDataArray.VisitObj.5"},
          funnelId : {$first : "$funnelDataArray.funnelId"},
          conversion : {$sum : "$funnelDataArray.conversion"},
          visit : {$sum : "$funnelDataArray.visit"},
          data : {$first : "$funnelDataArray.data"},
          VisitObj : {$first : "$funnelDataArray.VisitObj"}
        }
      }
   ]).exec();
   let newFunnelDataArray = [];
   let calculatedFunnels = _.map(funnelData, function(funnel){
    newFunnelDataArray.push(funnel);
     return funnel._id
   })
   //{ _id: { $ne: calculatedFunnels } }
  const funnels = await Funnel.find({ $and: [ { siteId: siteId }, { _id: { $nin: calculatedFunnels } } ]  }).sort({ createdAt: -1 }).exec();
  for (let index = 0; index < funnels.length; index++) {
    newFunnelDataArray.push({
            "_id": funnels[index]._id,
            "VisitObj0": 0,
            "VisitObj1": 0,
            "VisitObj2": 0,
            "VisitObj3": 0,
            "VisitObj4": 0,
            "VisitObj5": 0,
            "funnelId": funnels[index]._id,
            "conversion": 0,
            "visit": 0,
            "data": [
              funnels[index]
            ],
            "VisitObj": {
                "0": 0,
                "1": 0
            }
    });
  }
    let sortedArray = _.sortBy(newFunnelDataArray, '_id');
    sortedArray.reverse()
    return res.send({code:200, "funnelTrackData" : sortedArray}); 
  } catch (error) {
    next(error);
  }
};

exports.getFunnelById = async (req, res, next) => {
    try {
      // var currentUserSites = await checkWebsites(req.params.siteId, req.user);
      // if(!currentUserSites)
      // {
      //   return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
      // }
      const siteId = req.params.siteId;
      const funnelId = req.params.funnelId;
      const fromDate = new Date(req.params.from);
      const toDate = new Date(req.params.to);
      let siteProtocol = await Website.findOne({ _id: siteId }).select('protocol -_id').exec();
      let funnelPages = await Funnel.findOne({ _id: funnelId }).select('pages -_id').exec();
      let pagesHtml = [];
      for (let index = 0; index < funnelPages.pages.length; index++) {
        let funnelPagesData = await Recording.findOne({ 
          $and: [ 
            { pageName: funnelPages.pages[index].pageName },
            { siteId: siteId } ]   })
            .select('htmlCopy')
            .sort('-createdAt').exec();
        pagesHtml.push({
          pageName: funnelPages.pages[index].pageName,
          html: funnelPagesData.htmlCopy
        });
      }
      await Funnelfilter.aggregate([
        {
          '$match': {
            siteId: new mongoose.Types.ObjectId(siteId),
            funnelDate: {
              $gte: fromDate,
              $lte: toDate
            }
          }
        },
        {$unwind:"$funnelDataArray"},
        {
          $group:
          {
            _id:"$funnelDataArray.funnelId",
            VisitObj0:{$sum:"$funnelDataArray.VisitObj.0"},
            VisitObj1:{$sum:"$funnelDataArray.VisitObj.1"},
            VisitObj2:{$sum:"$funnelDataArray.VisitObj.2"},
            VisitObj3:{$sum:"$funnelDataArray.VisitObj.3"},
            VisitObj4:{$sum:"$funnelDataArray.VisitObj.4"},
            VisitObj5:{$sum:"$funnelDataArray.VisitObj.5"},
            funnelId : {$first : "$funnelDataArray.funnelId"},
            conversion : {$sum : "$funnelDataArray.conversion"},
            visit : {$sum : "$funnelDataArray.visit"},
            data : {$first : "$funnelDataArray.data"},
            VisitObj : {$first : "$funnelDataArray.VisitObj"}
          }
        }
      ], function(err, funnelData)
     {
       if(err)
       {
        res.send({code:200, "funnelData" : []});
       }
       else
       {
        let filtered = funnelData.filter(function(value, index, arr){
          if (value.funnelId == funnelId) {
            return value;
          }
        });
        res.send({code:200, "funnelData" : filtered, pages: pagesHtml, protocol: siteProtocol.protocol });
       }
     })
    } catch (error) {
      console.log(error);
      next(error);
    }
  };

  exports.get = async (req, res, next) => {
    try {
        const funnelId = req.params.funnelId;
        const funnel = await Funnel.findOne({ _id: funnelId }).exec();
        return res.send({
            code: 200,
            funnel: funnel
        });
    } catch (error) {
      next(error);
    }
  };

  exports.edit = async (req, res, next) => {
    try {
        const funnelId = req.body.funnelId;
        let funnelObjId = new mongoose.Types.ObjectId(req.body.funnelId)
        const name = req.body.name;
        const pages = req.body.pages;
        const alias = req.body.alias;
        const siteId = req.body.siteId;
        const funnel = await Funnel.findOne({ _id: funnelId }).exec();
        funnel.name = name;
        funnel.alias = alias;
        funnel.pages = pages;
        await funnel.save();
        await Funnelfilter.update(
          { siteId: siteId } , 
          { $pull: { funnelDataArray: { funnelId: funnelObjId } } },
          { multi: true }).exec();
        let curlBody = {
          siteId : siteId
        }
        curl.setBody(curlBody)
        .post(`${FUNNEL_CRON_ADDRESS}/recreate-funnel-data`)
        .then(({statusCode, body, headers}) => {
        })
        .catch((e) => {
        console.log(e);
        });
        return res.send({
            code: 200,
            funnel: funnel
        });
    } catch (error) {
      next(error);
    }
  };

/**
 * Create new funnel
 * @public
 */
exports.create = async (req, res, next) => {
  try {
    const siteId = req.body.siteId;
    const name = req.body.name;
    const pages = req.body.pages;
    const alias = req.body.alias;
    const funnel = new Funnel({
      siteId: siteId,
      name: name,
      pages: pages,
      alias: alias
    });
    await funnel.save((err, newFUnnel) => {
      if (err) {
        next(err);
      }
      let curlBody = {
        siteId : siteId
      }
      curl.setBody(curlBody)
      .post(`${FUNNEL_CRON_ADDRESS}/recreate-funnel-data`)
      .then(({statusCode, body, headers}) => {
      console.log("recreated successfully");
      })
      .catch((e) => {
      console.log("error in request");
      console.log(e);
      });
      return res.send({
        code: 200,
        msg: 'Funnel created succesfully.'
      });
    });
  } catch (error) {
    next(next);
  }
}

/**
 * Delete funnel
 * @public
 */
exports.delete = async (req, res, next) => {
  try {
    console.log(req.body.funnelId);
    let funnelId = new mongoose.Types.ObjectId(req.body.funnelId);
    const deletedFunnel = await Funnel.deleteOne({
      _id: req.body.funnelId
    });
    await Funnelfilter.update(
      {} , 
      { $pull: { funnelDataArray: { funnelId: funnelId } } },
      { multi: true }).exec();
    res.status(httpStatus.CREATED);
    return res.status(200).send({
      code: 200,
      data: [],
      message: "Funnel delete successfully"
    });
  } catch (error) {
    next(error);
  }
}



/**
 * Detail funnel
 * @public
 */
exports.detail = async (req, res, next) => {
  try {
    const funnels = await Funnel.findById(req.params.funnelId).exec();
    //const funnel = new Funnel();
    //let funnelTransformed = await Funnel.transformFunnelDetail(funnels);
    let pagesArr = [];
    var funnelName = funnels.funelName;
    var funnelId = funnels._id;
    for (let index = 0; index < funnels.pages.length; index++) {
      let pa = await Page.findById(funnels.pages[index].pageId).exec();
      pagesArr.push({
        name: pa.name,
        alias: funnels.pages[index].name,
        id: pa._id,
        pageVisit: pa.pageVisit
      });
    }
    let resObj = {
      funnelId: funnelId,
      name: funnels.funelName,
      pages: pagesArr
    }
    res.status(httpStatus.CREATED);
    return res.status(200).send({
      code: 200,
      data: resObj,
      message: ""
    });

  } catch (error) {
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
}

/**
 * Report funnel
 * @public
 */
exports.report = async (req, res, next) => {
  try {
    let from = req.params.from
    let to = req.params.to;
    const funnelInstance = new Funnel();
    let whereClauseQuery = funnelInstance.getWhereClauseQueryForFilter(req);
    const funnel = await Funnel.findById(req.params.funnelId).exec();
    let resObj = [];
    let preObj = null;
    let pages = [];
    let dropOff = 0;
    let maxVisit = 0;
    for (let index = 0; index < funnel.pages.length; index++) {
      let p = funnel.pages[index]
      maxVisit = 0;
      let queryObj = Object.keys(whereClauseQuery);
      for (let j = 0; j < p.pageDetail.length; j++) {
        const dateIsAfter = moment(p.pageDetail[j].date).isAfter(moment(from));
        const dateIsBefore = moment(p.pageDetail[j].date).isBefore(moment(to).add(1, 'days'));
        if (dateIsAfter && dateIsBefore) {
          if (queryObj.length > 0) {
            const recordings = await Recording.find({
              sessionId: p.pageDetail[j].userSessionId
            }).exec();
            for (let i = 0; i < queryObj.length; i++) {
              if (queryObj[i] == "devices") {

                if (typeof (whereClauseQuery[queryObj[i]]) !== 'string') {
                  whereClauseQuery[queryObj[i]].forEach(item => {
                    if (item == p.pageDetail[j][queryObj[i]]) {
                      if (i == queryObj.length - 1) {
                        maxVisit = maxVisit + 1;
                      }
                    }
                  })
                } else {
                  if (p.pageDetail[j][queryObj[i]] === whereClauseQuery[queryObj[i]]) {
                    if (i == queryObj.length - 1) {
                      maxVisit = maxVisit + 1;
                    }
                  }
                }

              } else if (queryObj[i] == "tags") {
                whereClauseQuery[queryObj[i]].forEach(item => {
                  if (item == p.pageDetail[j][queryObj[i]]) {
                    if (i == queryObj.length - 1) {
                      maxVisit = maxVisit + 1;
                    }
                  }
                })
              } else if (queryObj[i].toLocaleLowerCase() == "VisitorType".toLocaleLowerCase()) {

                if (whereClauseQuery[queryObj[i]] === "First-time visitor") {

                  if (p.pageDetail[j].visitorType !== "false") {
                    break;
                  }
                } else if (whereClauseQuery[queryObj[i]] === "Returning visitor") {
                  if (p.pageDetail[j].visitorType !== "true") {
                    break;
                  }
                }

              } else if (queryObj[i] == "pageCount") {
                // let recording
                if (recordings.length > whereClauseQuery[queryObj[i]]) {
                  break;
                } else if (recordings.length <= whereClauseQuery[queryObj[i]]) {
                  if (i == queryObj.length - 1) {
                    maxVisit = maxVisit + 1;
                  }
                }

              } else if (queryObj[i] == "duration") {
                // let recording
                let duration = 0;
                for (let recIndex = 0; recIndex < recordings.length; recIndex++) {
                  duration = duration + recordings[recIndex].duration;
                }
                if (duration > whereClauseQuery[queryObj[i]]) {
                  break;
                } else if (duration <= whereClauseQuery[queryObj[i]]) {
                  if (i == queryObj.length - 1) {
                    maxVisit = maxVisit + 1;
                  }
                }

              } else if (queryObj[i] == "countryName") {
                if (p.pageDetail[j][queryObj[i]] === whereClauseQuery[queryObj[i]]) {
                  if (i == queryObj.length - 1) {
                    maxVisit = maxVisit + 1;
                  }
                }
              } else if (p.pageDetail[j][queryObj[i]] !== undefined) {
                if (p.pageDetail[j][queryObj[i]] === whereClauseQuery[queryObj[i]]) {
                  if (i == queryObj.length - 1) {
                    maxVisit = maxVisit + 1;
                  }
                } else {
                  break;
                }
              }
            }

          } else {
            maxVisit = p.pageVisit;
          }
        } else {
          break;
        }
      }
      pages[index] = {
        "pageVisit": maxVisit,
        "_id": p._id,
        "pageId": p.pageId,
        "pageName": p.pageName,
        "name": p.name
      };

    }
    pages.forEach(p => {
      if (preObj == null) {
        preObj = p;
      } else {
        dropOff = preObj.pageVisit - p.pageVisit;
        let vr = 0;
        let dr = 0;
        if (maxVisit > 0) {
          vr = (preObj.pageVisit / maxVisit * 100).toFixed(2);
        }
        if (preObj.pageVisit > 0) {
          dr = (p.pageVisit / preObj.pageVisit * 100).toFixed(2);
        }
        resObj.push({
          visitRatio: vr,
          dropOff: dropOff,
          pageVisit: preObj.pageVisit,
          pageName: preObj.pageName,
          dropRatio: dr
        })
        preObj = p;
      }
    })
    let vrn = 0;
    if (maxVisit > 0) {
      vrn = (pages[pages.length - 1].pageVisit / maxVisit * 100).toFixed(2)
    }
    resObj.push({
      visitRatio: vrn,
      pageName: pages[pages.length - 1].pageName,
      pageVisit: pages[pages.length - 1].pageVisit
    })


    res.status(httpStatus.CREATED);
    return res.status(200).send({
      code: 200,
      data: resObj,
      message: ""
    });
  } catch (error) {
    console.log(error)
    return res.status(400).send({
      code: 400,
      data: [],
      message: error.message
    });
  }
}


exports.imagePrview = async (req, res, next) => {
  res.status(200).download('src/pageScreens/' + req.params.id + '.png');
}

exports.getSiteFunnel = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    let siteId = req.params.siteId;
    let siteFunells = await FunnelStats.find({ siteId: siteId }).exec();
    return res.status(200).send({
      code: 200,
      data: siteFunells
    });
  } catch (error) {
    next(error);
  }
}

exports.getCronFunnelData = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    let siteId = req.params.siteId;
    let siteFunells = await FunnelStats.find({ siteId: siteId }).exec();
    return res.status(200).send({
      code: 200,
      data: siteFunells
    });
  } catch (error) {
    next(error);
  }
}


exports.getCronFunnelDataTest = async (req, res, next) => {
  try {
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    let siteId = req.params.siteId;
    let siteFunells = await FunnelStats.aggregate([
      {
        '$match': {
          'siteId': siteId,
          createdAt: { $gt: fromDate, $lt: toDate },
        }
      }, {
        '$group': {
          '_id': "$funnelId", 
          'pages': {
            '$first': {
              'pages': '$pages', 
              'alias': '$alias', 
              'funnelId': '$funnelId', 
              'funnelName': '$funnelName', 
              'siteId': '$siteId'
            }
          }, 
          'total': {
            '$sum': '$visitObj.0'
          }, 
          'total2': {
            '$sum': '$visitObj.1'
          }, 
          'total3': {
            '$sum': '$visitObj.2'
          }, 
          'total4': {
            '$sum': '$visitObj.2'
          }
        }
      }, {
        '$project': {
          'visitObj.0': '$total', 
          'visitObj.1': '$total2', 
          'visitObj.2': '$total3', 
          'visitObj.3': '$total4', 
          'pages': '$pages.pages', 
          'alias': '$pages.alias', 
          'funnelId': '$pages.funnelId', 
          'funnelName': '$pages.funnelName', 
          'siteId': '$pages.siteId'
        }
      }
    ]);
    return res.status(200).send({
      code: 200,
      data: siteFunells
    });
  } catch (error) {
    next(error);
  }
}

async function funnelCron() {
  try {
    let today = moment().format('YYYY-MM-DD');
    // const fromDate = new Date(req.params.from);
    // const toDate = new Date(req.params.to);
    let queryData = {};
    // queryData.createdAt = {
    //   $gt: fromDate,
    //   $lt: toDate
    // };
    const funnel = await Funnel.find({}).exec();
    const funnelTrackData = [];
    for (let i = 0; i < funnel.length; i++) {
      queryData.siteId = funnel[i].siteId;
      queryData.pageName; 
      queryData.pageName = funnel[i].pages[0].pageName;
      queryData.type = 'online';
      queryData.isFunnelRead = false;
      const filtersRecordings = await Session.find({
        $and: [queryData]
      }).populate('recordings', 'pageName').exec();

      let recordings = filtersRecordings;
      const data = [];
      let conversion = 0;
      let visit = 0;
      let firstPageData = 0;
      let secondPage = 0;
      let newData = [];
      let dropped = [];
      let converted = [];
      data.push(funnel[i]);
      let VisitObj = {}
      for (let index = 0; index < funnel[i].pages.length; index++) {
        VisitObj[index] = 0;
      }
      recordings.forEach((recording) => {
        const recordingArray = _.map(recording.recordings, function (value, key) {
          if (key < funnel[i].pages.length) {
            return pageName = {
              pageName: value.pageName
            };
          }
        });
        const filterFunnelArray = _.filter(recordingArray, function (record) {
          if (record !== undefined) {
            return record;
          }
        });
        const diff = _.isEqual(filterFunnelArray, funnel[i].pages);
        if (diff === true) {
           converted.push(recording._id);
          conversion += 1;
          visit += 1;
          let pageVisit = 0;
          for (let index = 0; index < Object.keys(VisitObj).length; index++) {
            VisitObj[index] = VisitObj[index] + 1;
          }
          pageVisit += 1;
        } else {
           dropped.push(recording._id);
          const funnelPagesData = funnel[i].pages;
          const filterFUnnelData  = _.filter(funnelPagesData, function (record) {
            if (record !== undefined) {
                return record;
            }
            });
          for (let d = 0; d < filterFunnelArray.length; d++) {
            if (filterFunnelArray[d].pageName === filterFUnnelData[d].pageName) {
              VisitObj[d] += 1;
            } else {
              break;
            }
          }
          if (recordingArray[0].pageName === funnel[i].pages[0].pageName) {
            firstPageData += 1;
            visit += 1;
          }
        }
        const funnelPages = funnel[i].pages;
        a = VisitObj;
      });
        const funnelStats = await FunnelStats.findOne(
          {
            $and: [ { siteId: funnel[i].siteId }, { funnelId: funnel[i]._id }, { Date: today }]
          }).exec();
          console.log('<<<<<<<<< DID STAT EXIST >>>>>>>>>>');
          console.log(funnelStats);
        if (funnelStats !== null) {
          console.log('<<<<<<<<<<<< Funnel Stats >>>>>>>>');
          console.log(funnelStats);
          console.log('<<<<<<<< FUNNEL VISIT STATS >>>>>>>>');
          console.log(funnelStats.visitObj);
          console.log('<<<<<<<<  Old Funnel Stats >>>>>>>>');
          console.log(VisitObj);
          // if(funnelStats.visitObj.length > 0) {
          if(Object.size(VisitObj) === 2 ) {
            funnelStats.visitObj['0'] += VisitObj['0'];
            funnelStats.visitObj['1'] += VisitObj['1'];
          } else if(Object.size(VisitObj) === 3 ) {
            funnelStats.visitObj['0'] += VisitObj['0'];
            funnelStats.visitObj['1'] += VisitObj['1'];
            funnelStats.visitObj['2'] += VisitObj['2'];
          } else if(Object.size(VisitObj) === 4 ) {
            funnelStats.visitObj['0'] += VisitObj['0'];
            funnelStats.visitObj['1'] += VisitObj['1'];
            funnelStats.visitObj['2'] += VisitObj['2'];
            funnelStats.visitObj['3'] += VisitObj['3'];
          } else {
            funnelStats.visitObj['0'] += VisitObj['0'];
            funnelStats.visitObj['1'] += VisitObj['1'];
            funnelStats.visitObj['2'] += VisitObj['2'];
            funnelStats.visitObj['3'] += VisitObj['3'];
            funnelStats.visitObj['4'] += VisitObj['3'];
          }

          let newArray = funnelStats.converted.concat(converted);
          let newDroppedArr = funnelStats.dropped.concat(dropped);
          await FunnelStats.findOneAndUpdate(
            {
              $and: [ { siteId: funnel[i].siteId }, { funnelId: funnel[i]._id }, { Date: today }]
            },
            {
              $set:
              {
                visitObj: funnelStats.visitObj[0],
                conversion: funnelStats.conversion + conversion,
                visit: funnelStats.visit + visit,
                siteId: funnel[i].siteId,
                converted: newArray,
                dropped: newDroppedArr
              }
            }, {
              new: true
            }).exec();
          // }
        } else {
          let newFunnelStat = new FunnelStats({
            funnelId: funnel[i]._id,
            firstPageInfo: firstPageData,
            secondPage: secondPage,
            funnelName: funnel[i].name,
            pages: funnel[i].pages,
            alias: funnel[i].alias,
            siteId: funnel[i].siteId,
            conversion:  conversion,
            visit: visit,
            visit: visit,
            Date: today,
            dropped: dropped, 
            converted: converted,
            visitObj: VisitObj
          });
          newFunnelStat.save();
        }


        const updateRec = await Session.updateMany(
          {
            $and: [queryData]
          },
          {
            $set: { isFunnelRead: true }
          }
        ).exec();


      funnelTrackData.push({
        funnelId: funnel[i]._id,
        conversion: conversion,
        visit: visit,
        VisitObj: VisitObj[i],
        firstPageInfo: firstPageData,
        secondPage: secondPage,
        data: data,
        dropped: dropped[i],
        converted: converted
      });

    }
    // return res.send({
    //   code: 200,
    //   funnelTrackData
    // });
  } catch (error) {
    console.log(error);
  }
}

// async function runFunnelCron() {
//   let funnel = await Funnel.find({}).exec();
//   console.log(funnel);

// }
// cron.schedule('* * * * *', () => {
//   console.log('running a task every minute');
//   funnelCron();
// })
