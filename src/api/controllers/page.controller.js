const Page = require('../models/page.model');
const Recording = require('../models/recording.model');
const MetaData = require('../models/metaData.model');
const Website = require('../models/website.model');
const Users = require('../models/users.model');
const moment = require('moment');
const { _ } = require('underscore');
const mongoose = require('mongoose');
const Event = require('../models/event.model');
const Session = require('../models/session.model');
const Siteids = require('../models/siteid.model');
const AWS = require('aws-sdk');
const s3 = new AWS.S3({
  accessKeyId: 'AKIAI6FN6GALLBUTYK6Q',
  secretAccessKey: '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'
});
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const { MONGO_DB_URI } = require('../../config/vars');

//Check if user is sending its own website.
async function checkWebsites(siteId = 0, userId = 0, owner=1) {
  var siteIdsArray = [];
  console.log('siteId',siteId)
  console.log('userId',userId)
  if (!userId) {
    console.log('inside user');
    return false;
  }

  if (!siteId) {
    console.log('inside site');
    return false;
  }
  let currentUserSites = await Siteids.find({ userId: userId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
  console.log('currentUserSites',currentUserSites);
  if(currentUserSites.length)
  {
    return true;
  }
  else if(owner !== 1)
  {
    let currentUserOwnerId = await Users.findOne({ _id: userId}, {ownerId:1}).select('siteIds').exec();
    if(currentUserOwnerId && currentUserOwnerId.ownerId)
    {
      let currentUserSitesAgain = await Siteids.find({ userId: currentUserOwnerId.ownerId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
      if(currentUserSitesAgain.length)
      {
        return true;
      }
      else
      {
        console.log('3rd last else')
        return false;    
      }
    }
    else
    { console.log('2nd last else')
      return false;
    }
  }
  else
  { console.log('last else')
    return false;
  }
}

exports.get = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const siteId = req.params.siteId;
    const pages = await Recording.find({
      websiteId: siteId
    }).select('recordings', '-htmlCopy').exec();
    return res.status(200).send({
      code: 200,
      data: pages
    });
  } catch (error) {
    next(error);
  }
};

exports.share = async (req, res, next) => {
  try {
    const pageId = req.body.pageId;
    const isShare = req.body.isShare;
    const page = await Page.findOne({
      _id: pageId
    }).select('_id isShare').exec();
    page.isShared = isShare;
    const update = await page.save();
    return res.send({
      code: 200,
      data: update
    });
  } catch (error) {
    next(error);
  }
};

exports.viewGraphData = async (req, res, next) => {
  try {
    const siteId = req.body.siteId;
    const pageName = req.body.pageName;
    const from = req.body.from;
    const to = req.body.to;
    const fromDate = new Date(from);
    const toDate = new Date(to);
    const queryString = req.query;
    var timeDiff = Math.abs(fromDate.getTime() - toDate.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    let data = await Recording.aggregate([{
      $match: {
        $and: [
          queryString,
          {
            createdAt:
            {
              $gte: fromDate, $lte: toDate
            }
          },
          {
            siteId: siteId
          },
          {
            pageName: pageName
          }
        ]
      }
    },
    {
      $group: {
        _id: {
          yearMonthDayUTC: {
            $dateToString: {
              format: "%Y-%m-%d",
              date: "$createdAt"
            }
          }
        },
        total: {
          $sum: 1
        }
      },
    },
    ], (err, result) => {
      if (err) {
        console.log(err);
      }
    });
    const dates = [];
    const counts = [];
    for (let i = 0; i < diffDays; i++) {
      let newFromDate;
      newFromDate = new Date(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate() + i);
      newFromDate.setHours(23, 59, 59, 999);
      let pushDate = moment(newFromDate).format('YYYY/MM/DD');
      let index = _.filter(data, function (num) {
        let nnnDate = new Date(num._id.yearMonthDayUTC);
        nnnDate.setHours(23, 59, 59, 999);
        var diff = (newFromDate - nnnDate)
        if (diff === 0) {
          return num;
        }
      });
      if (index.length > 0) {
        counts.push(index[0].total);
        dates.push(index[0]._id.yearMonthDayUTC);
      } else {
        dates.push(pushDate);
        counts.push(0);
      }
    }
    return res.status(200).send({
      code: 200,
      data: dates,
      counts: counts
    });
  } catch (error) {
    next(error);
  }
};

exports.updateTags = async (req, res, next) => {
  try {
    const pageName = req.body.pageName;
    const siteId = req.body.siteId;
    let tag = req.body.tag;
    // await MetaData.findOneAndUpdate(
    //   {
    //     pageName: pageName,
    //     siteId: siteId
    //   },
    //   tag,
    //   {
    //     new: true,
    //     upsert: true
    //   }
    // ).exec();
    return res.send({
      code: 200,
      data: 'Page updaetd successfully!'
    });
  } catch (error) {
    next(error);
  }
};

exports.getErrPageDetail = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    let queryString = req.query;
    const from = new Date(req.params.from);
    const to = new Date(req.params.to);
    const siteId = req.params.siteId;
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 20);
    queryString.createdAt = {
      $gte: from,
      $lte: to
    };
    queryString.siteId = new mongoose.Types.ObjectId(siteId);
    if (Object.size(queryString)) {
      if ((req.query.visitMin !== undefined) && (req.query.visitMax)) {
        queryString.duration = { $gte: parseInt(req.query.visitMin), $lte: parseInt(req.query.visitMax) };
      } else if (req.query.visitMin !== undefined) {
        queryString.duration = { $gte: parseInt(req.query.visitMin) };
      } else if (req.query.visitMax !== undefined) {
        queryString.duration = { $gte: 0, $lte: parseInt(req.query.visitMax) };
      }
      if (queryString.pageName !== undefined) {
        if (typeof queryString.pageName === 'string') {
          queryString.pageName = req.query.pageName;
        }
        if (typeof queryString.pageName === 'object') {
          queryString.pageName = { $in: req.query.pageName }
        }
      }
      if (queryString.formInteract !== undefined) {
        queryString.formInteract = parseInt(req.query.formInteract);
      }

      if (queryString.formSubmit !== undefined) {
        queryString.formSubmit = parseInt(req.query.formSubmit);
      }
      if (queryString.clickRage !== undefined) {
        // queryData.clickRage = parseInt(req.query.clickRage);
        queryString.clickRage = { $gte: parseInt(req.query.clickRage) };
      }

      if (queryString.clickError !== undefined) {
        queryString.clickError = parseInt(req.query.clickError);
      }
      delete queryString.visitMin;
      delete queryString.visitMax;

    }
    const data = await MetaData.aggregate([
      {
        $match: {
          $and: [
            queryString
          ]
        }
      },
      {
        $sort: {
          views: -1
        }
      },
      {
        $group: {
          _id: "$pageName",
          pageName: {
            $first: '$pageName'
          },
          pageId: {
            $first: '$pageId'
          },
          formInteract: {
            $sum: '$formInteract'
          },
          clickRage: {
            $sum: '$clickRage'
          },
          formSubmit: {
            $sum: '$formSubmit'
          },
          clickError: {
            $sum: '$clickError'
          },
          clicks: {
            $sum: '$clicks'
          },
          device: {
            $last: '$device'
          },
          visitTime: {
            $avg: '$engagementTime'
          },
          renderTime: {
            $avg: '$loadingTime'
          },
          views: {
            $sum: 1
          },
        },
      },{
        $sort: {
          views: -1
        }
      }, {
        '$facet': {
          'metaData': [
            {
              '$count': 'total'
            }
          ],
          'data': [
            {
              '$skip': offset
            }, {
              '$limit': limit
            }
          ]
        }
      }
    ]).option({ allowDiskUse: true });
    // let pageWithForm = [];
    // for (let index = 0; index < data.length; index++) {
    //   if (data[index].formInteract > 0) {
    //     pageWithForm.push(data[index]);
    //   }
    // }
    return res.send({
      code: 200, data: { data: data[0].data }, total: data[0].metaData[0] ?
        data[0].metaData[0].total : 0
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};


exports.getCpPageDetail = async (req, res, next) => {
  try {
    let queryString = req.query;
    const from = new Date(req.params.from);
    const to = new Date(req.params.to);
    const Site = await Session.findOne({ projectId: req.params.projectId }).select('siteId').exec();
    const siteId = Site.siteId;
    queryString.createdAt = {
      $gte: from,
      $lte: to
    };

    queryString.siteId = new mongoose.Types.ObjectId(siteId);
    queryString.pageName = req.params.pageName;
    if (Object.size(queryString)) {
      if ((req.query.visitMin !== undefined) && (req.query.visitMax)) {
        queryString.duration = { $gte: parseInt(req.query.visitMin), $lte: parseInt(req.query.visitMax) };
      } else if (req.query.visitMin !== undefined) {
        queryString.duration = { $gte: parseInt(req.query.visitMin) };
      } else if (req.query.visitMax !== undefined) {
        queryString.duration = { $gte: 0, $lte: parseInt(req.query.visitMax) };
      }
      if (queryString.pageName !== undefined) {
        if (typeof queryString.pageName === 'string') {
          queryString.pageName = req.query.pageName;
        }
        if (typeof queryString.pageName === 'object') {
          queryString.pageName = { $in: req.query.pageName }
        }
      }
      if (queryString.formInteract !== undefined) {
        queryString.formInteract = parseInt(req.query.formInteract);
      }

      if (queryString.formSubmit !== undefined) {
        queryString.formSubmit = parseInt(req.query.formSubmit);
      }
      if (queryString.clickRage !== undefined) {
        // queryData.clickRage = parseInt(req.query.clickRage);
        queryString.clickRage = { $gte: parseInt(req.query.clickRage) };
      }

      if (queryString.clickError !== undefined) {
        queryString.clickError = parseInt(req.query.clickError);
      }
      delete queryString.visitMin;
      delete queryString.visitMax;

    }
    const data = await MetaData.aggregate([
      {
        $match: {
          $and: [
            queryString
          ]
        }
      },
      {
        $group: {
          _id: "$pageName",
          pageName: {
            $first: '$pageName'
          },
          pageId: {
            $first: '$pageId'
          },
          formInteract: {
            $sum: '$formInteract'
          },
          // clickRage: {
          //   $sum: '$clickRage'
          // },
          formSubmit: {
            $sum: '$formSubmit'
          },
          // clickError: {
          //   $sum: '$clickError'
          // },
          clicks: {
            $sum: '$clicks'
          },
          visitTime: {
            $avg: '$visitTime'
          },
          renderTime: {
            $avg: '$loadingTime'
          },
          views: {
            $sum: 1
          },
        },
      },
      {
        $sort: {
          views: -1
        }
      }
    ]);
    return res.send({ code: 200, data: data });
  } catch (error) {
    next(error);
  }
};

exports.detailPageError = async (req, res, next) => {
  try {
    let queryString = req.query;
    const from = new Date(req.params.from);
    const to = new Date(req.params.to);
    const siteId = req.params.siteId;
    const pageId = req.params.pageId;
    queryString.createdAt = {
      $gte: from,
      $lt: to
    };
    queryString.pageId = new mongoose.Types.ObjectId(pageId);
    queryString.siteId = new mongoose.Types.ObjectId(siteId);
    // queryString.device = device;
    console.log(queryString);
    const data = await MetaData.aggregate([
      {
        $match: {
          $and: [
            queryString
          ]
        }
      },
      {
        $sort: {
          pageId: -1
        }
      },
      {
        $group: {
          _id: "$pageId",
          pageName: {
            $first: '$pageName'
          },
          pageId: {
            $first: '$pageId'
          },
          formInteract: {
            $sum: '$formInteract'
          },
          clickRage: {
            $sum: '$clickRage'
          },
          formSubmit: {
            $sum: '$formSubmit'
          },
          clickError: {
            $sum: '$clickError'
          },
          clicks: {
            $sum: '$clicks'
          },
          visitTime: {
            $avg: '$visitTime'
          },
          renderTime: {
            $avg: '$loadingTime'
          },
          views: {
            $sum: 1
          },
        }
      },{
        $sort : { 'views': -1 } 
      }
    ]).exec();
    return res.send({ code: 200, data: { data: data[0] } });
  } catch (error) {
    next(error);
  }
};

/*
GET Form detail regarding each page for from drop off
*/
exports.getFormPageDetail = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    let queryString = req.query;
    const from = new Date(req.params.from);
    const to = new Date(req.params.to);
    const siteId = req.params.siteId;
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 10);
    queryString.createdAt = {
      $gte: from,
      $lte: to
    };
    queryString.siteId = new mongoose.Types.ObjectId(siteId);
    if (Object.size(queryString)) {
      if ((req.query.visitMin !== undefined) && (req.query.visitMax)) {
        queryString.duration = { $gte: parseInt(req.query.visitMin), $lte: parseInt(req.query.visitMax) };
      } else if (req.query.visitMin !== undefined) {
        queryString.duration = { $gte: parseInt(req.query.visitMin) };
      } else if (req.query.visitMax !== undefined) {
        queryString.duration = { $gte: 0, $lte: parseInt(req.query.visitMax) };
      }
      if (queryString.pageName !== undefined) {
        if (typeof queryString.pageName === 'string') {
          queryString.pageName = req.query.pageName;
        }
        if (typeof queryString.pageName === 'object') {
          queryString.pageName = { $in: req.query.pageName }
        }
      }
      if (queryString.formInteract !== undefined) {
        queryString.formInteract = parseInt(req.query.formInteract);
      }

      if (queryString.formSubmit !== undefined) {
        queryString.formSubmit = parseInt(req.query.formSubmit);
      }
      if (queryString.clickRage !== undefined) {
        // queryData.clickRage = parseInt(req.query.clickRage);
        queryString.clickRage = { $gte: parseInt(req.query.clickRage) };
      }

      if (queryString.clickError !== undefined) {
        queryString.clickError = parseInt(req.query.clickError);
      }
      delete queryString.visitMin;
      delete queryString.visitMax;
      queryString.formInteract = 1
    }
    const data = await MetaData.aggregate([
      {
        $match: {
          $and: [
            queryString
          ]
        }
      },
      {
        $sort: {
          views: -1
        }
      },
      {
        $group: {
          _id: "$pageName",
          pageName: {
            $first: '$pageName'
          },
          pageId: {
            $first: '$pageId'
          },
          formInteract: {
            $sum: '$formInteract'
          },
          clickRage: {
            $sum: '$clickRage'
          },
          formSubmit: {
            $sum: '$formSubmit'
          },
          clickError: {
            $sum: '$clickError'
          },
          clicks: {
            $sum: '$clicks'
          },
          visitTime: {
            $avg: '$engagementTime'
          },
          renderTime: {
            $avg: '$loadingTime'
          },
          views: {
            $sum: 1
          },
        },
      },{
        $sort : { 'views': -1 } 
      }, {
        '$facet': {
          'metaData': [
            {
              '$count': 'total'
            }
          ],
          'data': [
            {
              '$skip': offset
            }, {
              '$limit': limit
            }
          ]
        }
      }
    ]).exec();
    // let pageWithForm = [];
    // for (let index = 0; index < data[0].data.length; index++) {
    //   if (data[0].data[index].formInteract > 0) {
    //     pageWithForm.push(data[0].data[index]);
    //   }
    // }
    return res.send({ code: 200, data: { data: data[0] && data[0].data ? data[0].data : [] }, total: data[0] && data[0].metaData[0] ? data[0].metaData[0].total : 0 });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.allPagesDetail = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    const siteId = req.params.siteId;
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 20);
    let pages = await Session.aggregate([
      {
        $match: {
          site: siteId,
          type: 'online',
          createdAt: {
            $gte: fromDate,
            $lte: toDate
          },
        }
      },
      {
        $group: {
          _id: "$pageName",

          interactCount: {
            $sum: '$formInteract'
          },
          formSubmit: {
            $sum: '$formSubmit'
          },
          total: {
            $sum: 1
          },
        },
      },
      {
        '$sort': {
          'total': -1
        }
      },
      { $skip : 0 },
      { $limit: 10 }
    ]);
    
    return res.send({
      code: 200,
      summary: pages,
      // total: pages[0].metaData[0] ? pages[0].metaData[0].total : 0,
    });
  } catch (error) {
    next(error);
  }
}


exports.pageDetailData = async (req, res, next) => {
  try {
    console.log('IN click error and click rage ');
    let queryString = req.query;
    const from = new Date(req.params.from);
    const to = new Date(req.params.to);
    const pageId = req.params.pageId;
    queryString.createdAt = {
      $gte: from,
      $lte: to
    };
    let total = 0;
    let pageHtmlData = '';
    let pageData = await Page.findOne({ _id: pageId }).select('name websiteId height pageHeight width').exec();
    let recordingHtml = await Recording.findOne({ $and: [{ siteId: pageData.websiteId }, { pageName: pageData.name }] }).sort({ createdAt: -1 }).limit(1).select('htmlCopy').exec();
    let website = await Website.findOne({ _id: pageData.websiteId }).select('protocol').exec();
    let dbName = website._id.toString();
    queryString.pageId = new mongoose.Types.ObjectId(pageId);
    if (req.query.device !== undefined) {
      queryString.device = req.query.device;
    }
    queryString.type = req.params.type;
    let data;
    if (queryString.type === 'clickError') {
      data = await calculateClickError(dbName, queryString, req.params.from, req.params.to);
      data.forEach((data) => {
        total += data.total;
      })
      if (website.protocol === 'https') {
        return res.json(
          {
            code: 200,
            data: {
              data: data,
              total: total,
              height:
                pageData.height,
              pageHeight: pageData.pageHeight,
              width: pageData.width,
              html: recordingHtml.htmlCopy,
              protocol: website.protocol
            }
          }
        );
      } else {
        let params = {
          Bucket: 'flopandascripts',
          Key: 'websites/' + pageData.websiteId + '/' + pageData._id + '.html',
          ResponseContentEncoding: 'text/html'
        };
        s3.getObject(params, function (s3Err, newData) {
          if (s3Err) {
            console.log(s3Err);
          } else {
            // let htmlCopy = newData.Body.toString('utf-8');
            return res.json(
              {
                code: 200,
                data: {
                  data: data,
                  total: total,
                  height:
                    pageData.height,
                  pageHeight: pageData.pageHeight,
                  width: pageData.width,
                  html: newData.Body.toString('utf-8'),
                  protocol: website.protocol
                }
              }
            );
          }
        });
      }
    } else if(queryString.type === 'clickRage'){
        data = await calculateClickRage(dbName, queryString, req.params.from, req.params.to);
        data.forEach((data) => {
          total += data.total;
        })
        if (website.protocol === 'https') {
          return res.json(
            {
              code: 200,
              data: {
                data: data,
                total: total,
                height:
                  pageData.height,
                pageHeight: pageData.pageHeight,
                width: pageData.width,
                html: recordingHtml.htmlCopy,
                protocol: website.protocol
              }
            }
          );
        } else {
          let params = {
            Bucket: 'flopandascripts',
            Key: 'websites/' + pageData.websiteId + '/' + pageData._id + '.html',
            ResponseContentEncoding: 'text/html'
          };
          s3.getObject(params, function (s3Err, newData) {
            if (s3Err) {
              console.log(s3Err);
            } else {
              // let htmlCopy = newData.Body.toString('utf-8');
              return res.json(
                {
                  code: 200,
                  data: {
                    data: data,
                    total: total,
                    height:
                      pageData.height,
                    pageHeight: pageData.pageHeight,
                    width: pageData.width,
                    html: newData.Body.toString('utf-8'),
                    protocol: website.protocol
                  }
                }
              );
            }
          });
        }
    }
  } catch (error) {
    console.log(error);
    next(error);
  }
};



exports.pageDetailData0ld = async (req, res, next) => {
  try {
    let queryString = req.query;
    const from = new Date(req.params.from);
    const to = new Date(req.params.to);
    const pageId = req.params.pageId;
    queryString.createdAt = {
      $gte: from,
      $lte: to
    };
    let total = 0;
    let pageHtmlData = '';
    let pageData = await Page.findOne({ _id: pageId }).select('name websiteId height pageHeight width').exec();
    let recordingHtml = await Recording.findOne({ $and: [{ siteId: pageData.websiteId }, { pageName: pageData.name }] }).sort({ createdAt: -1 }).limit(1).select('htmlCopy').exec();
    let website = await Website.findOne({ _id: pageData.websiteId }).select('protocol').exec();
    let dbName = website._id.toString();
    queryString.pageId = new mongoose.Types.ObjectId(pageId);
    if (req.query.device !== undefined) {
      queryString.device = req.query.device;
    }
    queryString.type = req.params.type;
    let data;
    if (queryString.type === 'clickError') {
      data = await calculateClickError(dbName, queryString, req.params.from, req.params.to);
      data.forEach((data) => {
        total += data.total;
      })
      console.log(data);
      if (website.protocol === 'https') {
        return res.json(
          {
            code: 200,
            data: {
              data: data,
              total: total,
              height: pageData.height,
              pageHeight: pageData.pageHeight,
              width: pageData.width,
              html: recordingHtml.htmlCopy,
              protocol: website.protocol
            }
          }
        );
      } else {
        let params = {
          Bucket: 'flopandascripts',
          Key: 'websites/' + pageData.websiteId + '/' + pageData._id + '.html',
          ResponseContentEncoding: 'text/html'
        };
        s3.getObject(params, function (s3Err, newData) {
          if (s3Err) {
            console.log(s3Err);
          } else {
            // let htmlCopy = newData.Body.toString('utf-8');
            return res.json(
              {
                code: 200,
                data: {
                  data: data,
                  total: total,
                  height:
                    pageData.height,
                  pageHeight: pageData.pageHeight,
                  width: pageData.width,
                  html: newData.Body.toString('utf-8'),
                  protocol: website.protocol
                }
              }
            );
          }
        });
      }
    } 
    if (queryString.type === 'clickRage'){
      data = await calculateClickRage(dbName, queryString, req.params.from, req.params.to);
      data.forEach((data) => {
        total += data.total;
      })
      if (website.protocol === 'https') {
        return res.json(
          {
            code: 200,
            data: {
              data: data,
              total: total,
              height:
                pageData.height,
              pageHeight: pageData.pageHeight,
              width: pageData.width,
              html: recordingHtml.htmlCopy,
              protocol: website.protocol
            }
          }
        );
      } else {
        let params = {
          Bucket: 'flopandascripts',
          Key: 'websites/' + pageData.websiteId + '/' + pageData._id + '.html',
          ResponseContentEncoding: 'text/html'
        };
        s3.getObject(params, function (s3Err, newData) {
          if (s3Err) {
            console.log(s3Err);
          } else {
            // let htmlCopy = newData.Body.toString('utf-8');
            return res.json(
              {
                code: 200,
                data: {
                  data: data,
                  total: total,
                  height:
                    pageData.height,
                  pageHeight: pageData.pageHeight,
                  width: pageData.width,
                  html: newData.Body.toString('utf-8'),
                  protocol: website.protocol
                }
              }
            );
          }
        });
      }
    }
  } catch (error) {
    next(error);
  }
};

Object.size = function (obj) {
  var size = 0, key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};

exports.getSitePageStats = async (req, res, next) => {
  try {
    const siteId = req.body.siteId;
    const pageName = req.body.pageName;
    const projectId = req.body.projectId;
    let visits = await Session.aggregate([
      {
        $match: {
          $and: [
            {
              site: siteId,
              pageName: pageName,
              projectId: projectId
            }
          ]
        }
      },
      {
        $group: {
          _id: null,
          interactions: {
            $sum: '$formInteract'
          },
          submissions: {
            $sum: '$formSubmit'
          },
          sum: {
            $sum: 1
          },
        },
      },
      {
        $project: {
          _id: 0,
          submissions: 1,
          sum: 1,
          interactions: 1
        }
      },
    ]);
    let conversionRate = ((visits[0].submissions / visits[0].sum) * 100).toFixed(0);
    return res.send({ code: 200, data: visits });
  } catch (error) {
    next(error);
  }
}


async function getPageHtml(siteId, pageName) {
  try {
    let params = {
      Bucket: 'flopandascripts',
      Key: 'websites/' + siteId + '/' + pageName,
      ResponseContentEncoding: 'text/html'
    };

    return s3.getObject(params, (err) => {
      if (err) {
        // handle errors
      }
    }).promise();
  } catch (error) {
    next(error);
  }
}

async function calculateClickError(dbName, queryData, from, to) {
  try {
    return new Promise((resolve, reject) => {
      const url = MONGO_DB_URI;
      queryData.createdAt = {
        $gt: from,
        $lt: to
      };
      const client = MongoClient(url, { useNewUrlParser: true });
      client.connect(async function (err, client) {
        assert.equal(null, err);
        const db = client.db(dbName);
        const collection = db.collection('events');
        collection.aggregate([
          {
            $match: {
              $and: [
                queryData
              ]
            }
          },
          {
            $sort: {
              pageId: -1
            },
          },
          {
            $group: {
              _id: "$path",
              fieldSet: {
                $first: "$fieldSets"
              },
              isFieldSet: {
                $first: "$isFieldSets"
              },
              width: {
                $first: "$width"
              },
              height: {
                $first: "$height"
              },
              total: {
                $sum: 1
              },
            },
          }
        ], { allowDiskUse: true }, function (err, cursor) {
          assert.equal(err, null);
          cursor.toArray(function (err, documents) {
            err ? reject(err) : resolve(documents);
          });
        });
      });
      client.close();
    });
  } catch (error) {
    return error;
  }
};

// Function to Calculate Page Click Rage 
async function calculateClickRage(dbName, queryData, from, to) {
  try {
    return new Promise((resolve, reject) => {
      const url = MONGO_DB_URI;
      queryData.createdAt = {
        $gt: from,
        $lt: to
      };
      const client = MongoClient(url, { useNewUrlParser: true });
      client.connect(async function (err, client) {
        assert.equal(null, err);
        const db = client.db(dbName);
        const collection = db.collection('events');
        collection.aggregate([
          {
            $match: {
              $and: [
                queryData
              ]
            }
          },
          {
            $sort: {
              pageId: -1
            },
          },
          {
            $group: {
              _id: {
                y: "$y",
                x: "$x",
                height: "$height",
                width: "$width",
                path: "$path",
                offsetLeft: "$offsetLeft",
                offsetTop: "$offsetTop"
              },
              fieldSet: {
                $first: "$fieldSets"
              },
              isFieldSet: {
                $first: "$isFieldSets"
              },
              total: {
                $sum: 1
              },
            },
          }
        ],{ allowDiskUse: true }, function (err, cursor) {
          assert.equal(err, null);
          cursor.toArray(function (err, documents) {
            err ? reject(err) : resolve(documents);
          });
        });
      });
      client.close();
    });
  } catch (error) {
    return error;
  }
}
