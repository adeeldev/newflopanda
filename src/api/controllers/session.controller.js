
const Session = require('../models/session.model');
const Website = require('../models/website.model');
const Page = require('../models/page.model');
const Recording = require('../models/recording.model');
const Form = require('../models/form.model');
const Event = require('../models/event.model');
const MetaData = require('../models/metaData.model');
const FormField = require('../models/formField.model');
const FormStats = require('../models/formStats.model');
const Interaction = require('../models/interactions.model');
const Users = require('../models/user.model');
const User = require('../models/users.model');
const Logs = require('../models/log.model');
const Recordingcounts = require('../models/recordingcounts.model');
const Report = require('../models/report.model');
const Siteids = require('../models/siteid.model');
const inbound = require('inbound');
const { _ } = require('underscore');
const mongoose = require('mongoose');
const { ACCESS_KEY_ID, SECRET_ACCESS_KEY, S3_BUCKET, IP_API_KEY, DOMAIN } = require('../../config/vars');
const AWS = require('aws-sdk');
AWS.config.update({ accessKeyId: ACCESS_KEY_ID, secretAccessKey: SECRET_ACCESS_KEY });
const s3 = new AWS.S3();
const { handler: errorHandler } = require('../middlewares/error');
const ipstack = require('ipstack');
const moment = require('moment');
const rp = require('request-promise');
var request = require('sync-request');
var CryptoJS = require("crypto-js");

//Check if user is sending its own website.
async function checkWebsites(siteId = 0, userId = 0, owner = 1) {
  var siteIdsArray = [];

  if (!userId) {
    return false;
  }

  if (!siteId) {
    return false;
  }
  let currentUserSites = await Siteids.find({ userId: userId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
  if (currentUserSites.length) {
    return true;
  }
  else if (owner !== 1) {
    let currentUserOwnerId = await User.findOne({ _id: userId }, { ownerId: 1 }).select('siteIds').exec();
    if (currentUserOwnerId && currentUserOwnerId.ownerId) {
      let currentUserSitesAgain = await Siteids.find({ userId: currentUserOwnerId.ownerId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
      if (currentUserSitesAgain.length) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
}


exports.save = async (req, res, next) => {
  try {
    var decryption_string = req.headers['x-auth-token'];
    var bytes = CryptoJS.AES.decrypt(req.body.data, decryption_string);
    var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    req.body = decryptedData;
    let todayDate = moment().format('YYYY-MM-DD');
    const forms = req.body.forms;
    // const machineIp = req.headers['x-forwarded-for'];
    var machineIp = req.headers['x-forwarded-for'];
    machineIp = '39.37.189.202';//machineIp.split(",")[0];
    let ipData = await getIpData(machineIp);

    const website = await Website.findOne({ _id: req.body.siteId }).exec();
    if (website === null) {
      return res.status(200).send({ status: 404, data: { msg: 'Site not exist. ' } });
    }
    if (website.stopRecording === true) {
      return res.status(200).send({ status: 404, data: { msg: 'Account Admin has stopped recording for this site. ' } });
    }
    if (website.name !== req.body.domainName) {
      return res.status(200).send({ status: 404, data: { msg: 'Domain Name not matches with added script.' } });
    }
    if (website.honorTracking === true && req.body.honorTracking === true) {
      return res.status(200).send({ status: 404, data: { msg: 'you have enabled setting for Honour Tracking.' } });
    }
    let ipExist = _.indexOf(website.automaticallyExcludedIP, ipData.ip);
    if (ipExist !== -1) {
      return res.status(200).send({ status: 404, data: { msg: 'This IP is added in list to not track recording.' } });
    }
    if (website.recordingRate < 100) {
      if (website.recordingRate === 0) {
        return res.status(200).send({ status: 404, data: { msg: 'No new recording recording rate is set to 0' } });
      }
      else {
        let recordingPercentage = await checkRecordingRate(website._id, website.recordingRate);
        if (!recordingPercentage) {
          return res.status(200).send({ status: 404, data: { msg: 'No new recording yet becuase of recording rate. ' } });
        }
      }
    }

    let ip = ipData.ip;
    if (website.iPAnonymization === true) {
      ip = ip.slice(0, -3);
      ip += '####';
    } else {
      ip = ip;
    }
    let pageName = req.body.pageName;
    let queryIndex = [];
    let idenfrs = pageName.split("?");
    let newStr = idenfrs[0];
    let queryString = idenfrs[0];
    let referer;
    let network = '';
    let engine = '';
    if (req.body.referer !== 'noReferrer') {
      let resultData = await checkReferer(req.body.recording.url, req.body.referer);
      referer = resultData.referrer.type;
      if (resultData.referrer.engine !== undefined) {
        engine = resultData.referrer.engine;
      }
      if (resultData.referrer.network !== undefined) {
        network = resultData.referrer.network;
      }
    } else {
      referer = 'noReferrer'
    }
    let newPageName = req.body.pageName.split("?")[0];
    const user = await Session.findOne({ $and: [{ siteId: req.body.siteId }, { ip: req.body.ip }] }).exec();
    let returningVisitor;
    if (user) {
      if (user.length > 0) {
        returningVisitor = 'returning';
      } else {
        returningVisitor = 'firsttime';
      }
    }
    else {
      returningVisitor = 'firsttime';
    }
    if (website.isScriptInstall === null || website.isScriptInstall === false) {
      website.isScriptInstall = true;
      await website.save();
    }
    const newSession = new Session({
      siteId: req.body.siteId,
      site: req.body.siteId,
      ip: ip,
      city: ipData.city,
      region: '',
      regionCode: '',
      countryCode: ipData.country_code,
      continentCode: ipData.continent_code,
      continentName: ipData.continent_name,
      countryName: ipData.country_name,
      organisation: '',
      language: '',
      htmlCopy: req.body.htmlCopy,
      referer: req.body.referer,
      resolution: req.body.resolution,
      osName: req.body.osName,
      browserName: req.body.browserName,
      device: req.body.device,
      traficSource: referer,
      pageName: req.body.pageName.split("?")[0],
      adUrl: req.body.pageName.split("?")[1],
      latitude: ipData.latitude,
      longitude: ipData.longitude,
      browserVer: req.body.browserVer,
      osVer: req.body.osVer,
      returningVisitor: returningVisitor,
      recDuration: req.body.recording.duration,
      utmSource: req.body.utm_source,
      utmMedium: req.body.utm_medium,
      utmCampaign: req.body.utm_campaign,
      utmContent: req.body.utm_content,
      utmTerm: req.body.utm_term,
      adId: req.body.ad_id,
      adName: req.body.ad_name,
      gclid: req.body.gclid,
      network: network,
      engine: engine,
      projectId: req.body.projectId
    });
    // const session = await new Session(newSession);
    const savedSession = await newSession.save();
    const recording = new Recording({
      siteId: req.body.siteId,
      site: req.body.siteId,
      sessionId: savedSession._id,
      htmlCopy: req.body.recording.htmlCopy,
      docHeight: req.body.docHeight,
      width: req.body.recording.width,
      height: req.body.height,
      path: req.body.recording.path,
      url: req.body.recording.url,
      traficSource: referer,
      startTime: req.body.recording.startTime,
      time: req.body.recording.time,
      duration: req.body.recording.duration,
      resolution: req.body.resolution,
      scroll: req.body.scrollPercentage,
      referer: req.body.referer,
      osName: req.body.osName,
      browserName: req.body.browserName,
      countryCode: ipData.country_code,
      ip: ip,
      device: req.body.device,
      countryName: ipData.country_name,
      pageName: req.body.pageName.split("?")[0],
      adUrl: req.body.pageName.split("?")[1],
      recordings: [],
      clickRage: req.body.clickRage,
      formInteract: req.body.formInteract,
      formSubmit: req.body.formSubmit,
      clickError: req.body.clickError,
      styles: req.body.recording.styles,
      returningVisitor: returningVisitor,
      latitude: ipData.latitude,
      longitude: ipData.longitude,
      projectId: req.body.projectId
    });
    const recordignData = await recording.save();
    savedSession.pages.push(req.body.pageName.split("?")[0]);
    savedSession.funnelPages.push(req.body.pageName.split("?")[0]);
    savedSession.funnelRecordData.push({ 'page1': req.body.pageName.split("?")[0] });
    savedSession.recordings.push(recordignData._id);
    savedSession.pageCount += 1;
    await savedSession.save();
    let userPageName = req.body.pageName.split('?')[0];
    const pages = await Page.findOne({
      $and: [{
        name: req.body.pageName.split("?")[0]
      }, {
        websiteId: req.body.siteId
      }]
    }).exec();
    let page;
    if (pages) {
      page = pages;
    } else {
      const newPage = new Page({
        websiteId: req.body.siteId,
        sessionId: savedSession._id,
        name: newPageName,
        pageHeight: req.body.docHeight,
        html: req.body.recording.htmlCopy,
        // width: req.body.width,
        height: req.body.height,
        width: req.body.recording.width,
        path: req.body.path,
        startTime: req.body.recording.startTime,
        engagmentTime: req.body.engagmentTime,
        loadingTime: req.body.loadingTime,
        referer: req.body.referer,
        resolution: req.body.resolution,
        osName: req.body.osName,
        browserName: req.body.browserName,
        device: req.body.device,
        countryName: savedSession.countryName,
        traficSource: req.body.traficSource,
        ip: ip,
        pageName: req.body.pageName.split("?")[0],
        clickRage: req.body.clickRage,
        formInteract: req.body.formInteract,
        formSubmit: req.body.formSubmit,
        clickError: req.body.clickError,
        projectId: req.body.projectId
      });
      const newPageData = await newPage.save();
      page = newPageData;
    }
    if (website.protocol === 'http') {
      if (req.body.pageName === '/index') {
        userPageName = '/';
      }
      var options = {
        method: 'POST',
        uri: 'http://52.56.228.186:9000/v1/website/page/download',
        body: {
          siteId: req.body.siteId,
          recordingId: recordignData._id,
          domain: website.protocol + '://' + website.name + userPageName,
          pageId: page._id
        },
        json: true // Automatically stringifies the body to JSON
      };
      rp(options)
        .then(function (parsedBody) {
          // POST succeeded...
        })
        .catch(function (err) {
          console.log(err);
        });
    }
    if (req.body.recording.forms.length > 0) {
      const pageForm = await Form.findOne({
        $and: [{
          siteId: req.body.siteId
        }, {
          pageName: req.body.pageName.split("?")[0]
        }]
      }).exec();
      if (pageForm === null) {
        if (req.body.recording.forms[0].fields.length > 0) {
          let forms = req.body.recording.forms;
          for (let index = 0; index < forms.length; index++) {
            const newForm = new Form({
              siteId: req.body.siteId,
              pageName: req.body.pageName.split("?")[0],
              pageId: page._id,
              index: forms[index].index,
              formName: forms[index].formName,
            });
            const formData = await newForm.save();
            const formFields = _.map(forms[index].fields, (field) => {
              field.pageName = req.body.pageName.split("?")[0];
              field.pageId = page._id;
              field.formId = formData._id;
              field.form = formData._id;
              return field;
            });
            await FormField.insertMany(formFields);
          }
        }
      }
    }

    let pageSize = await calSize(req.body.recording.htmlCopy.length);
    const newMetaData = new MetaData({
      recordingId: recordignData._id,
      pageId: page._id,
      siteId: req.body.siteId,
      site: req.body.siteId,
      pageName: req.body.pageName.split("?")[0],
      loadingTime: req.body.loadingTime,
      engagementTime: req.body.engagmentTime,
      visitTime: req.body.duration,
      clickRage: req.body.clickRage,
      formInteract: req.body.formInteract,
      formSubmit: req.body.formSubmit,
      clickError: req.body.clickError,
      sessionId: savedSession._id,
      pageSize: pageSize,
      device: savedSession.device,
      date: todayDate,
      clicks: 0,
      scroll: req.body.scrollPercentage
    });
    await newMetaData.save();
    if (req.body.formStats !== undefined) {
      const convertData = new Interaction({
        referer: req.body.referer,
        resolution: req.body.resolution,
        osName: req.body.osName,
        browserName: req.body.browserName,
        device: req.body.device,
        countryName: req.body.countryName,
        clickRage: req.body.clickRage,
        formInteract: req.body.formInteract,
        formSubmit: req.body.formSubmit,
        clickError: req.body.clickError,
        conversion: req.body.formStats.converted,
        siteId: req.body.siteId,
        pageName: req.body.pageName.split("?")[0],
        recordingId: recordignData._id
      });
      if (req.body.formSubmit === 'true') {
        convertData.conversion = 1;
      }
      await convertData.save();
      const interactions = req.body.formStats.interactions;
      let phone = '';
      let surName = '';
      let zipCode = '';
      // let price = [];
      let formStats = [];
      if (req.body.formStats.interactions.length > 0) {
        for (let index = 0; index < req.body.formStats.interactions.length; index++) {
          if (req.body.formStats.interactions[index].formSubmit === 1) {
            req.body.formStats.interactions[index].formSubmit = 1;
            req.body.formStats.interactions[index].dropOff = 0;
          } else {
            req.body.formStats.interactions[index].formSubmit = 0;
            req.body.formStats.interactions[index].dropOff = 1;
          }
          let formStat = new FormStats({
            siteId: req.body.siteId,
            pageName: req.body.pageName.split("?")[0],
            recordingId: recordignData._id,
            device: req.body.device,
            formIndex: req.body.formStats.interactions[index].formIndex,
            formSubmit: req.body.formStats.interactions[index].formSubmit,
            dropOff: req.body.formStats.interactions[index].dropOff,
            formInteract: req.body.formStats.interactions[index].formInteract,
            createdAt: new Date().toISOString()
          });
          await formStat.save();
          let dataFieldsArray = [];
          const fieldsData = _.map(interactions[index].fieldInteract, function (field) {
            field.siteId = req.body.siteId;
            field.recordedDate = new Date().toISOString();
            field.referer = req.body.referer;
            field.resolution = req.body.formStats.resolution;
            field.osName = req.body.formStats.osName;
            field.browserName = req.body.formStats.browserName;
            field.device = req.body.formStats.device;
            field.countryName = req.body.formStats.countryName;
            field.pageName = req.body.pageName.split("?")[0];
            field.clickRage = req.body.clickRage;
            field.formInteract = req.body.formInteract;
            field.formSubmit = req.body.formSubmit;
            field.clickError = req.body.clickError;
            field.recordingId = recordignData._id;
            field.formIndex = req.body.formStats.interactions[index].formIndex;
            field.createdAt = new Date().toISOString();
            if (field.fieldName === 'phone1' || field.fieldName === 'mobile' || field.fieldName === 'number') {
              phone = field.value;
            } else if (field.fieldName === 'last_name' || field.fieldName === 'lastname') {
              surName = field.value !== undefined ? field.value : ''
            } else if (field.fieldName === 'zip') {
              zipCode = field.value;
            }
            return field;
          });
          let sessionData = await Session.findById(req.body.sessionId).exec();
          if (sessionData.surname && sessionData.surname.findIndex(x => x.recordingId === recordignData._id) === -1) {
            if (surName && surName !== '') {
              let resSurName = sessionData.surname;
              sessionData.surname = []
              resSurName.push({ surname: surName, recordingId: recordignData._id });
              sessionData.surname = resSurName;
            }
          } else {
            let index = sessionData.surname.findIndex(x => x.recordingId === recordignData._id);
            let resSurName = sessionData.surname;
            sessionData.surname = [];
            resSurName[index].surname = surName;
            sessionData.surname = resSurName;
          }
          if (sessionData.phone && sessionData.phone.findIndex(x => x.recordingId === recordignData._id) === -1) {
            if (phone && phone !== '') {
              let resphone = sessionData.phone;
              sessionData.resphone = []
              resphone.push({ phone: phone, recordingId: recordignData._id });
              sessionData.phone = resphone;
            }
          } else {
            let index = sessionData.phone.findIndex(x => x.recordingId === recordignData._id);
            let resphone = sessionData.phone;
            sessionData.phone = [];
            resphone[index].phone = phone;
            sessionData.phone = resphone;
          }
          if (sessionData.zipCode && sessionData.zipCode.findIndex(x => x.recordingId === recordignData._id) === -1) {
            if (zipCode && zipCode !== '') {
              let reszipCode = sessionData.zipCode;
              sessionData.reszipCode = []
              reszipCode.push({ zipCode: zipCode, recordingId: recordignData._id });
              sessionData.zipCode = reszipCode;
            }
          } else {
            let index = sessionData.zipCode.findIndex(x => x.recordingId === recordignData._id);
            let reszipCode = sessionData.zipCode;
            sessionData.zipCode = [];
            reszipCode[index].zipCode = zipCode;
            sessionData.zipCode = reszipCode;
          }
          await sessionData.save();
          let fields = [];
          let queryString = "";
          dropOutFields = [];
          const filterDropOut = _.filter(fieldsData, function (dropOut) {
            if (dropOut.formSubmit === 0 && dropOut.interact === 1 && dropOut.value !== undefined) {
              let fieldName = dropOut.fieldName;
              let fieldVal = dropOut.value;
              dropOutFields.push({ fieldName: fieldName, fieldVal: fieldVal });
              queryString += '&' + dropOut.fieldName + '=' + dropOut.value;
              return dropOut;
            }
          });
          await saveAllData(dbName, formStat, fieldsData);
          await saveTracking(fieldsData);
        }
      }
    }
    var encryptedData = {
      sessionId: req.body.siteId,
      userSessionId: savedSession._id,
      whitelistedFields: website.whitelistedFields,
      turnOffStickyKeys: website.turnOffStickyKeys,
      automaticallyExcludedIP: website.automaticallyExcludedIP,
      honorTracking: website.honorTracking,
      iPAnonymization: website.iPAnonymization,
      blockingEUTraffic: website.blockingEUTraffic,
      excludedContent: website.excludedContent,
      recordingId: recordignData._id
    }

    var stringifyJSONToSent = JSON.stringify(encryptedData);
    var encrypted = CryptoJS.AES.encrypt(stringifyJSONToSent, process.env.CRYPTO_SECURE_STRING).toString();

    res.status(200).send({
      status: 200,
      data: encrypted
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

// exports.save = async (req, res, next) => {
//   try {
//     console.log('IN TRACKING SAVE FILE =============');
//     var decryption_string = req.headers['x-auth-token'];
//     var bytes  = CryptoJS.AES.decrypt(req.body.data, decryption_string);
//     var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
//     req.body = decryptedData;
//     let todayDate = moment().format('YYYY-MM-DD');
//     const forms = req.body.forms;
//     const machineIp = req.headers['x-forwarded-for'];
//     // let ipData = await getIpData(machineIp);
//     const website = await Website.findOne({ _id: req.body.siteId }).exec();
//     if (website === null) {
//       return res.status(200).send({ status: 404, data: { msg: 'Site not exist. '} });
//     }
//     if (website.stopRecording === true) {
//       return res.status(200).send({ status: 404, data: { msg: 'Account Admin has stopped recording for this site. ' } });
//     }
//     // if (website.name !== req.body.domainName) {
//     //   return res.status(200).send({ status: 404, data: { msg: 'Domain Name not matches with added script.' } });
//     // }
//     if (website.honorTracking === true && req.body.honorTracking === true) {
//       return res.status(200).send({ status: 404, data: { msg: 'you have enabled setting for Honour Tracking.' } });
//     }
//     // let ipExist = _.indexOf(website.automaticallyExcludedIP, ipData.ip);
//     // if (ipExist !== -1) {
//     //   return res.status(200).send({ status: 404, data: { msg: 'This IP is added in list to not track recording.' } });
//     // }
//     if (website.recordingRate < 100) {
//       if (website.recordingRate === 0) {
//         return res.status(200).send({ status: 404, data: { msg: 'No new recording recording rate is set to 0' } });
//       }
//       else {
//         let recordingPercentage = await checkRecordingRate(website._id, website.recordingRate);
//         if (!recordingPercentage) {
//           return res.status(200).send({ status: 404, data: { msg: 'No new recording yet becuase of recording rate. ' } });
//         }
//       }
//     }

//     let ip = 'sdfsf';//ipData.ip;
//     if (website.iPAnonymization === true) {
//       ip = ip.slice(0, -3);
//       ip += '####';
//     } else {
//       ip = ip;
//     }
//     let pageName = req.body.pageName;
//     let queryIndex = [];
//     let idenfrs = pageName.split("?");
//     let newStr = idenfrs[0];
//     let queryString = idenfrs[0];
//     let referer;
//     let network = '';
//     let engine = '';
//     if (req.body.referer !== 'noReferrer') {
//       let resultData = await checkReferer(req.body.recording.url, req.body.referer);
//       referer = resultData.referrer.type;
//       if (resultData.referrer.engine !== undefined) {
//         engine = resultData.referrer.engine;
//       }
//       if (resultData.referrer.network !== undefined) {
//         network = resultData.referrer.network;
//       }
//     } else {
//       referer = 'noReferrer'
//     }
//     let newPageName = req.body.pageName.split("?")[0];
//     const user = await Session.findOne({ $and: [{ siteId: req.body.siteId }, { ip: req.body.ip }] }).exec();
//     let returningVisitor;
//     if (user) {
//       if (user.length > 0) {
//         returningVisitor = 'returning';
//       } else {
//         returningVisitor = 'firsttime';
//       }
//     }
//     else {
//       returningVisitor = 'firsttime';
//     }
//     if (website.isScriptInstall === null || website.isScriptInstall === false) {
//       website.isScriptInstall = true;
//       await website.save();
//     }
//     const newSession = new Session({
//       siteId: req.body.siteId,
//       site: req.body.siteId,
//       ip: ip,
//       city: 'ee',//ipData.city,
//       region: 'werwer', //ipData.region_name,
//       regionCode: '34', //ipData.region_code,
//       countryCode: '45', //ipData.country_code,
//       continentCode: 'rgerge',//ipData.continent_code,
//       continentName: 'eger',//ipData.continent_name,
//       countryName: 'ergerg',//ipData.country_name,
//       organisation: 'ergerg',//ipData.organisation,
//       language: 'wwe',//ipData.language,
//       htmlCopy: req.body.htmlCopy,
//       referer: req.body.referer,
//       resolution: req.body.resolution,
//       osName: req.body.osName,
//       browserName: req.body.browserName,
//       device: req.body.device,
//       traficSource: referer,
//       pageName: req.body.pageName.split("?")[0],
//       adUrl: req.body.pageName.split("?")[1],
//       latitude: '53.345',//ipData.latitude,
//       longitude: '3467.45',//ipData.longitude,
//       browserVer: req.body.browserVer,
//       osVer: req.body.osVer,
//       returningVisitor: returningVisitor,
//       recDuration: req.body.recording.duration,
//       utmSource: req.body.utm_source,
//       utmMedium: req.body.utm_medium,
//       utmCampaign: req.body.utm_campaign,
//       utmContent: req.body.utm_content,
//       utmTerm: req.body.utm_term,
//       adId: req.body.ad_id,
//       adName: req.body.ad_name,
//       gclid: req.body.gclid,
//       network: network,
//       engine: engine,
//       projectId: req.body.projectId
//     });
//     // const session = await new Session(newSession);
//     const savedSession = await newSession.save();
//     const recording = new Recording({
//       siteId: req.body.siteId,
//       site: req.body.siteId,
//       sessionId: savedSession._id,
//       htmlCopy: req.body.recording.htmlCopy,
//       docHeight: req.body.docHeight,
//       width: req.body.recording.width,
//       height: req.body.height,
//       path: req.body.recording.path,
//       url: req.body.recording.url,
//       traficSource: referer,
//       startTime: req.body.recording.startTime,
//       time: req.body.recording.time,
//       duration: req.body.recording.duration,
//       resolution: req.body.resolution,
//       scroll: req.body.scrollPercentage,
//       referer: req.body.referer,
//       osName: req.body.osName,
//       browserName: req.body.browserName,
//       // countryCode: ipData.country_code,
//       ip: ip,
//       device: req.body.device,
//       // countryName: ipData.country_name,
//       pageName: req.body.pageName.split("?")[0],
//       adUrl: req.body.pageName.split("?")[1],
//       recordings: [],
//       clickRage: req.body.clickRage,
//       formInteract: req.body.formInteract,
//       formSubmit: req.body.formSubmit,
//       clickError: req.body.clickError,
//       styles: req.body.recording.styles,
//       returningVisitor: returningVisitor,
//       // latitude: ipData.latitude,
//       // longitude: ipData.longitude,
//       projectId: req.body.projectId
//     });
//     const recordignData = await recording.save();
//     savedSession.pages.push(req.body.pageName.split("?")[0]);
//     savedSession.funnelPages.push(req.body.pageName.split("?")[0]);
//     savedSession.recordings.push(recordignData._id);
//     savedSession.pageCount += 1;
//     await savedSession.save();
//     let userPageName = req.body.pageName.split('?')[0];
//     const pages = await Page.findOne({
//       $and: [{
//         name: req.body.pageName.split("?")[0]
//       }, {
//         websiteId: req.body.siteId
//       }]
//     }).exec();
//     let page;
//     if (pages) {
//       page = pages;
//     } else {
//       const newPage = new Page({
//         websiteId: req.body.siteId,
//         sessionId: savedSession._id,
//         name: newPageName,
//         pageHeight: req.body.docHeight,
//         html: req.body.recording.htmlCopy,
//         // width: req.body.width,
//         height: req.body.height,
//         width: req.body.recording.width,
//         path: req.body.path,
//         startTime: req.body.recording.startTime,
//         engagmentTime: req.body.engagmentTime,
//         loadingTime: req.body.loadingTime,
//         referer: req.body.referer,
//         resolution: req.body.resolution,
//         osName: req.body.osName,
//         browserName: req.body.browserName,
//         device: req.body.device,
//         countryName: savedSession.countryName,
//         traficSource: req.body.traficSource,
//         ip: ip,
//         pageName: req.body.pageName.split("?")[0],
//         clickRage: req.body.clickRage,
//         formInteract: req.body.formInteract,
//         formSubmit: req.body.formSubmit,
//         clickError: req.body.clickError,
//         projectId: req.body.projectId
//       });
//       const newPageData = await newPage.save();
//       page = newPageData;
//     }
//     console.log('<<<<<<<<<<<<<<<<<  IN DEBUGGING SESSION ONROLLER >>>>>>>>>>');
//     console.log(website.protocol);
//     if (website.protocol === 'http') {
//       if (req.body.pageName === '/index') {
//         userPageName = '/';
//       }
//       var options = {
//         method: 'POST',
//         uri: "http://52.56.228.186:9000/v1/website/page/download",
//         body: {
//           siteId: req.body.siteId,
//           recordingId: recordignData._id,
//           domain: website.protocol + '://' + website.name + userPageName,
//           pageId: page._id
//         },
//         json: true // Automatically stringifies the body to JSON
//       };
//       console.log(options);
//       rp(options)
//         .then(function (parsedBody) {
//           // POST succeeded...
//         })
//         .catch(function (err) {
//            console.log(err);
//         });
//     }
//     if (req.body.recording.forms.length > 0) {
//       const pageForm = await Form.findOne({
//         $and: [{
//           siteId: req.body.siteId
//         }, {
//           pageName: req.body.pageName.split("?")[0]
//         }]
//       }).exec();
//       if (pageForm === null) {
//         if (req.body.recording.forms[0].fields.length > 0) {
//           let forms = req.body.recording.forms;
//           for (let index = 0; index < forms.length; index++) {
//             const newForm = new Form({
//               siteId: req.body.siteId,
//               pageName: req.body.pageName.split("?")[0],
//               pageId: page._id,
//               index: forms[index].index,
//               formName: forms[index].formName,
//             });
//             const formData = await newForm.save();
//             const formFields = _.map(forms[index].fields, (field) => {
//               field.pageName = req.body.pageName.split("?")[0];
//               field.pageId = page._id;
//               field.formId = formData._id;
//               field.form = formData._id;
//               return field;
//             });
//             await FormField.insertMany(formFields);
//           }
//         }
//       }
//     }

//     let pageSize = await calSize(req.body.recording.htmlCopy.length);
//     const newMetaData = new MetaData({
//       recordingId: recordignData._id,
//       pageId: page._id,
//       siteId: req.body.siteId,
//       site: req.body.siteId,
//       pageName: req.body.pageName.split("?")[0],
//       loadingTime: req.body.loadingTime,
//       engagementTime: req.body.engagmentTime,
//       visitTime: req.body.duration,
//       clickRage: req.body.clickRage,
//       formInteract: req.body.formInteract,
//       formSubmit: req.body.formSubmit,
//       clickError: req.body.clickError,
//       sessionId: savedSession._id,
//       pageSize: pageSize,
//       device: savedSession.device,
//       date: todayDate,
//       clicks: 0,
//       scroll: req.body.scrollPercentage
//     });
//     await newMetaData.save();
//     var encryptedData = {
//       sessionId: req.body.siteId,
//       userSessionId: savedSession._id,
//       whitelistedFields: website.whitelistedFields,
//       turnOffStickyKeys: website.turnOffStickyKeys,
//       automaticallyExcludedIP: website.automaticallyExcludedIP,
//       honorTracking: website.honorTracking,
//       iPAnonymization: website.iPAnonymization,
//       blockingEUTraffic: website.blockingEUTraffic,
//       excludedContent: website.excludedContent,
//       recordingId: recordignData._id
//     }
//     var stringifyJSONToSent = JSON.stringify(encryptedData);
//     var encrypted = CryptoJS.AES.encrypt(stringifyJSONToSent, process.env.CRYPTO_SECURE_STRING).toString();

//     res.status(200).send({
//       status: 200,
//       data: encrypted
//     });
//   } catch (error) {
//     console.log(error);
//     next(error);
//   }
// };

exports.sessionRecording = async (req, res, next) => {
  try {
    const sessionId = req.params.sessionId;
    const sessionData = await Session.findOne({
      _id: sessionId
    }).select('createdAt').populate('recordings', 'pageName duration clickRage formInteract formSubmit clickError tags').exec();
    res.status(200).send({
      code: 200,
      data: sessionData.recordings
    });
  } catch (error) {
    next(errorHandler(error, req, res));
  }
};

exports.recordingIps = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const isBatch = req.query.isBatch;
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    const limit = parseInt(req.query.limit);
    const offset = parseInt(req.query.offset) * 10;
    const siteId = req.params.siteId;
    let queryString = {};
    queryString.createdAt = { $gt: fromDate, $lt: toDate };
    queryString.siteId = siteId;
    if (req.query.ip !== undefined) {
      queryString.ip = { $regex: new RegExp("^" + req.query.ip) };
    }
    let total = 0;
    let ips = [];
    if (isBatch === 'false') {
      total = await Session.countDocuments({ $and: [queryString] }).exec();
      ips = await Session.find(
        { $and: [queryString] }
      ).select('pageName ip countryCode osName device browserName')
        .populate('recordings', 'duration')
        .skip(offset)
        .limit(limit)
        .exec();
    } else {
      // queryString.ip = { $regex: req.query.ip, $options: 'i' };
      total = await Session.countDocuments({ $and: [queryString] }).exec();
      ips = await Session.find(
        { $and: [queryString] }
      ).select('pageName ip countryCode osName device browserName')
        .populate('recordings', 'duration')
        .skip(offset)
        .limit(limit)
        .sort({ ip: -1 })
        .exec();
    }
    res.send({ code: 200, data: ips, total: total });
  } catch (error) {
    next(error);
  }
};

exports.utmTags = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset) * limit;
    const siteId = req.params.siteId;
    let queryString = {};
    let sort = {};
    let orCondition = {};
    let sortData = {};
    queryString.createdAt = { $gt: fromDate, $lt: toDate };
    queryString.siteId = siteId;
    queryString.utmSource = { $in: ['google', 'bing'] };
    if (Object.keys(req.query).length > 1) {
      if (req.query.pageName !== undefined) {
        if (typeof req.query.pageName === 'string') {
          queryString.pageName = req.query.pageName;
        } else {
          queryString.pageName = { $in: req.query.pageName };
        }
      }
      if (req.query.formInteract !== undefined) {
        queryString.formInteract = parseInt(req.query.formInteract);
      }
      if (req.query.formSubmit !== undefined) {
        queryString.formSubmit = parseInt(req.query.formSubmit);
      }
      if (req.query.clickRage !== undefined) {
        queryString.clickRage = { $gte: parseInt(req.query.clickRage) };
      }
      if (req.query.clickError !== undefined) {
        queryString.clickError = parseInt(req.query.clickError);
      }
      if (queryString.paymentTag !== undefined) {
        queryString.paymentTag = parseInt(req.query.paymentTag);
      }
      if (req.query.ip !== undefined) {
        queryString.ip = { $regex: new RegExp("^" + req.query.ip) };
        // queryString.ip = { $regex: new RegExp(req.query.ip), $options: "si" };
      }
      if (req.query.utmTerm !== undefined) {
        if (typeof req.query.utmTerm === 'string') {
          orCondition.utmTerm = req.query.utmTerm;
        } else {
          for (let index = 0; index < req.query.utmTerm.length; index++) {
            sortData[req.query.utmTerm[index]] = -1;
          }
          orCondition.utmTerm = { $in: req.query.utmTerm };
        }
      }
      if (req.query.utmMedium !== undefined) {
        if (typeof req.query.utmMedium === 'string') {
          orCondition.utmMedium = req.query.utmMedium;
        } else {
          orCondition.utmMedium = { $in: req.query.utmMedium };
        }
      }
      if (req.query.utmCampaign !== undefined) {
        if (typeof req.query.utmCampaign === 'string') {
          orCondition.utmCampaign = req.query.utmCampaign;
        } else {
          orCondition.utmCampaign = { $in: req.query.utmCampaign };
        }
      }
      if (req.query.utmContent !== undefined) {
        if (typeof req.query.utmContent === 'string') {
          orCondition.utmContent = req.query.utmContent;
        } else {
          orCondition.utmContent = { $in: req.query.utmContent };
        }
      }
      if (req.query.gclid !== undefined) {
        if (typeof req.query.gclid === 'string') {
          orCondition.gclid = req.query.gclid;
        } else {
          orCondition.gclid = { $in: req.query.gclid };
        }
      }
      if (req.query.referer !== undefined) {
        queryString.referer = req.query.referer;
      }
      if (req.query.engine !== undefined) {
        if (typeof req.query.engine === 'string') {
          queryString.engine = req.query.engine;
        } else {
          queryString.engine = { $in: req.query.engine };
        }
      }
      if (req.query.surname !== undefined) {
        queryString.surname = { $regex: new RegExp("^" + req.query.surname), $options: "si" };
      }
      if (req.query.zipCode !== undefined) {
        queryString.zipCode = { $regex: new RegExp("^" + req.query.zipCode), $options: "si" };
      }
      if (req.query.phone !== undefined) {
        queryString.phone = { $regex: new RegExp("^" + req.query.phone), $options: "si" };
      }
      if (req.query.price !== undefined) {
        queryString.price = { $regex: new RegExp("^" + req.query.price), $options: "si" };
      }
      if (req.query.adUrl !== undefined) {
        if (typeof req.query.adUrl === 'string') {
          queryString.adUrl = req.query.adUrl;
        } else {
          queryString.adUrl = { $in: req.query.adUrl };
        }
      }
    }
    if (req.query.utmSource !== undefined) {
      if (typeof req.query.utmSource === 'string') {
        queryString.utmSource = req.query.utmSource;
      }
    }
    delete queryString.search;
    let isCampaignExist = await Session.findOne({ $and: [{ siteId: siteId }, { utmSource: 'google' }] }).exec();
    let bingCampaignExist = await Session.findOne({ $and: [{ siteId: siteId }, { utmSource: 'bing' }] }).exec();
    let isExist = false;
    let isBingExist = false;
    if (isCampaignExist) {
      isExist = true;
    }
    if (bingCampaignExist) {
      isBingExist = true;
    }
    let total = await Session.countDocuments(
      { $and: [queryString, { $or: [orCondition] }] }
    ).exec();
    var nowTime = new Date().getTime();
    console.log('before count time');
    console.log(nowTime);
    let utmTags = [];
    if (Object.keys(sortData).length > 0) {
      utmTags = await Session.find(
        { $and: [queryString, { $or: [orCondition] }] }
      ).sort(sortData)
        .skip(offset)
        .limit(limit)
        .exec();
    } else {
      utmTags = await Session.find(
        { $and: [queryString, { $or: [orCondition] }] }
      ).sort({ createdAt: -1 })
        .skip(offset)
        .limit(limit)
        .exec();
    }
    var afterTime = new Date().getTime();
    console.log('After first query time');
    console.log(afterTime - nowTime);
    return res.send({ code: 200, data: utmTags, total: total, isExist: isExist, isBingExist: isBingExist });
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.utmCsvReport = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    const siteId = req.params.siteId;
    let queryString = req.query;
    let sort = {};
    queryString.createdAt = { $gt: fromDate, $lt: toDate };
    queryString.siteId = siteId;
    queryString.utmSource = { $in: ['google', 'bing'] };
    if (Object.keys(req.query).length > 0) {
      if (req.query.formInteract !== undefined) {
        queryString.formInteract = parseInt(req.query.formInteract);
      }
      if (req.query.formSubmit !== undefined) {
        queryString.formSubmit = parseInt(req.query.formSubmit);
      }
      if (req.query.clickRage !== undefined) {
        queryString.clickRage = { $gte: parseInt(req.query.clickRage) };
      }
      if (req.query.clickError !== undefined) {
        queryString.clickError = parseInt(req.query.clickError);
      }
      if (queryString.paymentTag !== undefined) {
        queryString.paymentTag = parseInt(req.query.paymentTag);
      }
      if (req.query.ip !== undefined) {
        queryString.ip = { $regex: new RegExp("^" + req.query.ip) }
      }
      if (req.query.utmSource !== undefined) {
        if (typeof req.query.utmSource === 'string') {
          queryString.utmSource = req.query.utmSource;
        }
      }
      if (req.query.engine !== undefined) {
        queryString.engine = req.query.engine;
      }
      if (req.query.surname !== undefined) {
        queryString.surname = { $regex: new RegExp("^" + req.query.surname), $options: "si" };
      }
      if (req.query.zipCode !== undefined) {
        queryString.zipCode = { $regex: new RegExp("^" + req.query.zipCode), $options: "si" };
      }
      if (req.query.phone !== undefined) {
        queryString.phone = { $regex: new RegExp("^" + req.query.phone), $options: "si" };
      }
      if (req.query.price !== undefined) {
        queryString.price = { $regex: new RegExp("^" + req.query.price), $options: "si" };
      }
      if (req.query.adUrl !== undefined) {
        if (typeof req.query.adUrl === 'string') {
          queryString.adUrl = req.query.adUrl;
        } else {
          queryString.adUrl = { $in: req.query.adUrl };
        }
      }

    }
    delete queryString.search;
    let total = Session.countDocuments({ $and: [queryString] }).exec();
    let utmTags = await Session.find(
      { $and: [queryString] }
    ).sort({ createdAt: -1 })
      .exec();
    return res.send({ code: 200, data: utmTags, total: total });
  } catch (error) {
    next(error);
  }
};


exports.geo = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    let siteId = req.params.siteId;
    let queryString = req.query;
    const from = new Date(req.params.from);
    const to = new Date(req.params.to);
    let pageName = '';
    if (Object.size(queryString)) {
      if (queryString.clickRage !== undefined) {
        queryString.clickRage = parseInt(req.query.clickRage);
      }

      if (queryString.clickError !== undefined) {
        queryString.clickError = parseInt(req.query.clickError);
      }

      if (queryString.formInteract !== undefined) {
        queryString.formInteract = parseInt(req.query.formInteract);
      }

      if (queryString.formSubmit !== undefined) {
        queryString.formSubmit = parseInt(req.query.formSubmit);

      }
      if (queryString.paymentTag !== undefined) {
        queryString.paymentTag = parseInt(req.query.paymentTag);
      }
      // if (queryString.pageName !== undefined)
      // {
      //   if (req.query.pageName !== "")
      //   {
      //     pageName = req.query.pageName;
      //     delete queryString.pageName;
      //   }
      // }
      if (queryString.countryCode !== undefined) {
        if (typeof queryString.countryCode === 'string') {
          queryString.countryCode = req.query.countryCode;
        }
        if (typeof queryString.countryCode === 'object') {
          queryString.countryCode = { $in: req.query.countryCode }
        }
      }
    }
    queryString.siteId = new mongoose.Types.ObjectId(siteId);
    queryString.createdAt = { $gte: from, $lte: to }
    let countries = await Session.aggregate([{
      $match: {
        $and: [
          queryString
        ]
      }
    },
    {
      $sort: {
        count: -1
      }
    },
    {
      $group: {
        // _id: "$_id"
        _id: {
          "countryCode": "$countryCode",
          "countryName": "$countryName",
        },
        ip: { $addToSet: "$ip" },
        count: { $sum: 1 },
        coordinates: { $push: { latitude: "$latitude", longitude: "$longitude" } }
      },
    },
    {
      $sort: {
        count: -1
      }
    },
    ]).allowDiskUse(true);
    res.send({ code: 200, data: countries });
  } catch (error) {
    next(error);
  }
};

Object.size = function (obj) {
  var size = 0, key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};

exports.saveTag = async (req, res, next) => {
  try {
    const sessionId = req.body.sessionId;
    const name = req.body.tagName;
    const session = await Session.findOne({ _id: sessionId }).select('_id tags').exec();
    const isExist = _.filter(session.tags, function (tag) {
      return tag.name === name;
    });
    if (isExist.length > 0) {
      return res.send({ code: 409, data: name + ' tag already exists.' });
    } else {
      session.tags.push({
        name: name
      });
      const update = await session.save();
      return res.send({ code: 200, data: update });
    }
  } catch (error) {
    next(error);
  }
};

exports.addTagInMulRecording = async (req, res, next) => {
  try {
    const sessions = req.body.sessions;
    const name = req.body.tagName;
    for (let index = 0; index < sessions.length; index++) {
      let session = await Session.findOne({ _id: sessions[index] }).select('_id tags').exec();
      session.tags.push({
        name: name
      });
      const update = await session.save();
    }
    return res.send({ code: 200, data: 'Data updated successfully.' });
  } catch (error) {
    next(error);
  }
};

exports.deleteTag = async (req, res, next) => {
  try {
    const sessionId = req.body.sessionId;
    const tagId = req.body.tagId;
    const session = await Session.findOne({ _id: sessionId }).select('_id tags').exec();
    const tag = session.tags.id(tagId).remove();
    const removed = await session.save();
    return res.send({ code: 200, data: removed });
  } catch (error) {
    next(error);
  }
};


exports.saveVariable = async (req, res, next) => {
  try {
    const sessionId = req.body.sessionId;
    const variables = req.body.variables;
    const session = await Session.findOne({ _id: sessionId }).select('_id editVariables').exec();
    session.editVariables = variables;
    const update = await session.save();
    return res.send({ code: 200, data: update });
  } catch (error) {
    next(error);
  }
};


exports.share = async (req, res, next) => {
  try {
    const sessionId = req.body.sessionId;
    const isShare = req.body.isShare;
    const session = await Session.findOne({ _id: sessionId }).select('_id share').exec();
    session.share = isShare;
    const update = await session.save();
    return res.send({ code: 200, data: update });
  } catch (error) {
    next(error);
  }
};

exports.delete = async (req, res, next) => {
  try {
    const sessionId = req.body.sessionId;
    let session = await Session.findOne({ _id: sessionId }).exec();
    let website = await Website.findOne({ _id: session.siteId }).exec();
    if (req.user !== undefined && req.user !== 0) {
      let userId = req.user;
      let user = await Users.findOne({ _id: userId }).exec();
      const accountInfo = new Logs({
        userId,
        email: user.email,
        siteName: website.name,
        sessionId: session._id,
        logType: "recording"
      });
      const savedLogs = await accountInfo.save();
      await Session.findOneAndDelete({ _id: sessionId }).exec();
      return res.send({ code: 200, data: 'Session removed sunccessfully.' });
    }
  }
  catch (error) {
    next(error);
  }
};

exports.deleteMany = async (req, res, next) => {
  try {
    const sessionId = req.body.userSessionIds;
    for (let index = 0; index < sessionId.length; index++) {
      let session = await Session.findOne({ _id: sessionId[index] }).exec();
      let website = await Website.findOne({ _id: session.siteId }).exec();
      let userId = req.user;
      let user = await Users.findOne({ _id: userId }).exec();
      const accountInfo = new Logs({
        userId,
        email: user.email,
        siteName: website.name,
        sessionId: session._id,
        logType: "recording"
      });
      const savedLogs = await accountInfo.save();
      await Session.findOneAndDelete({ _id: sessionId[index] }).exec();
    }
    return res.send({ code: 200, data: 'Session removed sunccessfully.' });
  } catch (error) {
    next(error);
  }
};


exports.favourite = async (req, res, next) => {
  try {
    const sessionId = req.body.sessionId;
    const favorite = req.body.isFav;
    const session = await Session.findOne({ _id: sessionId }).select('_id favorite').exec();
    session.favorite = favorite;
    const update = await session.save();
    return res.send({ code: 200, data: update });
  } catch (error) {
    next(error);
  }
};

exports.multiFav = async (req, res, next) => {
  try {
    const sessions = req.body.sessions;
    const favorite = req.body.isFav;
    const session = await Session.updateMany({ _id: { $in: sessions } }, { $set: { favorite: favorite } }).exec();
    return res.send({ code: 200, data: session });
  } catch (error) {
    next(error);
  }
};

exports.multiWatch = async (req, res, next) => {
  try {
    const sessions = req.body.sessions;
    const watch = req.body.isWatch;
    const session = await Session.updateMany({ _id: { $in: sessions } }, { $set: { watch: watch } }).exec();
    return res.send({ code: 200, data: session });
  } catch (error) {
    next(error);
  }
};

exports.upload = async (req, res, next) => {
  try {
    const siteId = req.body.siteId;
    const pageName = req.body.pageName;
    const base64 = req.body.base64;
    const base64Data = Buffer.from(base64.replace(/^data:image\/\w+;base64,/, ""), 'base64');
    const type = base64.split(';')[0].split('/')[1];
    const digit = Math.random(4);
    const params = {
      Bucket: S3_BUCKET,
      Key: `${siteId}${pageName}.${type}`,
      Body: base64Data,
      ACL: 'public-read',
      ContentEncoding: 'base64',
      ContentType: `image/${type}`
    }
    s3.upload(params, (err, data) => {
      if (err) { return console.log(err) }
      Page.findByIdAndUpdate({ $and: [{ websiteId: siteId }, { name: pageName }] }, { image: data.Location }, { new: true, upsert: true }, function (err, page) {
        if (err) return err;
      });
      return res.send({ code: 200, data: 'image uploaded successfully.' });
    });
  } catch (error) {
    next(error);
  }
};

exports.durationRange = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const siteId = req.params.siteId;
    const filtersData = await Session.aggregate([
      {
        $match: {
          'siteId': new mongoose.Types.ObjectId(siteId),
          type: 'online'
        }
      },
      {
        $group:
        {
          _id: null,
          country: { $addToSet: { code: "$countryCode", name: "$countryName" } },
          pages: { $addToSet: { pageName: "$pageName" } },
          minDur: { $min: "$recDuration" },
          maxDur: { $max: "$recDuration" }
        }
      },
      {
        $project: {
          _id: 0,
          country: 1,
          pages: 1,
          minDur: 1,
          maxDur: 1
        }
      }
    ]).exec();
    if (filtersData.length > 0) {
      return res.send({
        code: 200,
        data: {
          durations: {
            minDur: filtersData[0].minDur ? filtersData[0].minDur : 0,
            maxDur: filtersData[0].maxDur ? filtersData[0].maxDur : 0
          },
          countryData: filtersData[0].country,
          startingPage: filtersData[0].pages.slice(0, 10)
        },
      });
    } else {
      return res.send({
        code: 200,
        data: {
          durations: {
            minDur: 0,
            maxDur: 1000
          },
          countryData: [],
          startingPage: []
        },
      });
    }

  } catch (error) {
    next(error);
  }
};


exports.saveOfflineReport = async (req, res, next) => {
  try {
    const siteId = req.body.siteId;
    const reportName = req.body.reportName;
    const userId = req.body.userId;
    const type = req.body.type;
    const data = req.body.data;
    const siteName = req.body.siteName;
    const reportCode = req.body.reportCode;
    let newReport = new Report({
      userId: userId,
      siteId: siteId,
      name: reportName,
      type: type,
      reportCode: reportCode,
      siteName: siteName
    });
    const reportFields = _.map(data, (report) => {
      report.site = siteId;
      report.reportCode = reportCode;
      return report;
    });
    await Session.insertMany(reportFields);
    await newReport.save();
    return res.send({ code: 200, data: 'uploaded successfully.' });
  } catch (error) {
    next(error);
  }
};


exports.updatePaymentTag = async (req, res, next) => {
  try {
    const sessionId = req.body.sessionId;
    const price = req.body.price;
    let newSession = await Session.findOneAndUpdate({ _id: sessionId }, { paymentTag: 1, formInteract: 1, formSubmit: 1 }).exec();
    return res.send({ code: 200, data: 'Tag updated succesfully!' });
  } catch (error) {
    next(error);
  }
}

exports.updateDb = async (req, res, next) => {
  try {
    var user = await Users.find({}).select('_id').exec();
    for (let index = 0; index < user.length; index++) {
      var siteIdsArray = [];
      let sites = await Website.find({ userId: user[index]._id }).select('_id').exec();
      for (let i = 0; i < sites.length; i++) {
        siteIdsArray.push(sites[i]._id.toHexString());
      }
      await Siteids.update(
        {
          userId: user[index]._id
        },
        {
          siteIds: siteIdsArray,
          userId: user[index]._id
        },
        {
          upsert: true
        });
    }
  } catch (error) {
    next(error);
  }
}


async function checkReferer(url, referrer) {
  return new Promise(
    (resolve, reject) => {
      let host = referrer.split("?")[0];
      if (host.indexOf('google') !== -1) {
        let description = { referrer: { type: 'search', engine: 'google' } };
        resolve(description);
      } else if (host.indexOf('yahoo') !== -1) {
        let description = { referrer: { type: 'search', engine: 'yahoo' } };
        resolve(description);
      } else if (host.indexOf('bing.com') !== -1) {
        let description = { referrer: { type: 'search', engine: 'bing' } };
        resolve(description);
      } else if (host.indexOf('facebook.com') !== -1) {
        let description = { referrer: { type: 'social', network: 'facebook' } }
        resolve(description);
      } else if (host.indexOf('instagram') !== -1) {
        let description = { referrer: { type: 'social', network: 'instagram' } }
        resolve(description);
      } else if (host.indexOf('twitter') !== -1 || host.indexOf('t.co') !== -1) {
        let description = { referrer: { type: 'social', network: 'twitter' } }
        resolve(description);
      } else if (host.indexOf('linkedin.com') !== -1) {
        let description = { referrer: { type: 'social', network: 'linkedin' } }
        resolve(description);
      } else {
        inbound.referrer.parse(url, referrer, function (err, description) {
          if (err) reject(err);
          resolve(description);
        });
      }
    }
  );
};

async function getIpData(ip) {
  // return new Promise(
  //   (resolve, reject) => {
  //     ipstack(ip, IP_API_KEY, (err, response) => {
  //       if (err) reject(err);
  //       resolve(response);
  //     })
  //   }
  // );
  var res = request('POST', 'http://18.130.152.240:8000/v1/ip/ipinfo', {
    json: { ip: ip },
  });
  return JSON.parse(res.getBody('utf8'));
}

async function checkRecordingRate(siteId, recordingRate) {
  let saveThisRecording = false;
  const recordingCounts = await Recordingcounts.findOne({
    siteId: siteId
  }).exec();

  if (!recordingCounts) {
    saveThisRecording = true;
    let newStatsRecording = { siteId: siteId.toString(), sessionRequests: 1, savedSessions: 1 }

    const newStatsRecordingData = await new Recordingcounts(newStatsRecording);
    const savedSession = await newStatsRecordingData.save();
  }
  else {
    let totalRequests = (recordingCounts.sessionRequests + 1);
    let currentSaved = recordingCounts.savedSessions;

    let requestsByPercent = (recordingRate * totalRequests) / 100;

    if (requestsByPercent > currentSaved) {
      saveThisRecording = true;
      recordingCounts.sessionRequests = totalRequests;
      recordingCounts.savedSessions = currentSaved + 1;
    }
    else {
      recordingCounts.sessionRequests = totalRequests;
      saveThisRecording = false;
    }
    await recordingCounts.save();
    return saveThisRecording;
  }
}

async function calSize(bytes) {
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Byte';
  let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

async function findKey(key, string) {
  try {
    let value = string.split("&").find(function (v) {
      // let keyValue = v.indexOf(key) > -1;
      // console.log(keyValue.split('=')[1]);
      return v.indexOf(key) > -1;
    })
  } catch (error) {
    console.log(error);
  }
}


exports.uploadFile = async (req, res, next) => {
  try {
    console.log(req.file);
    res.send({ code: 200, data: 'Successfully uploaded ' });
  } catch (error) {
    console.log(error);
    next(error);
  }
};


exports.updateDb = async (req, res, next) => {
  try {
    var user = await Users.find({}).select('_id').exec();
    for (let index = 0; index < user.length; index++) {
      var siteIdsArray = [];
      let sites = await Website.find({ userId: user[index]._id }).select('_id').exec();
      for (let i = 0; i < sites.length; i++) {
        siteIdsArray.push(sites[i]._id.toHexString());
      }
      await Siteids.update(
        {
          userId: user[index]._id
        },
        {
          siteIds: siteIdsArray,
          userId: user[index]._id
        },
        {
          upsert: true
        });
    }
  } catch (error) {
    next(error);
  }
}

async function saveAllData(dbName, statData, fieldData) {
  try {
    console.log('IN SAVE FIELD AND STATS FILE');
    const url = MONGO_DB_URI;
    const client = new MongoClient(url, { useNewUrlParser: true });
    client.connect(function (err, client) {
      console.log('IN formstat db connection');
      assert.equal(null, err);
      const db = client.db(dbName);
      db.collection('formstats').insertOne(statData, function (err, r) {
        if (err) {
          console.log('Database error  >>> ' + err);
        }
      });
      console.log('FIELD DATA LENGTH' + fieldData.length);
      db.collection('fieldtrackings').insertMany(fieldData, function (err, r) {
        if (err) {
          console.log('Database error  fields table >>> ' + err);
        }
      });
      client.close();
    });
  } catch (error) {

  }
}

async function saveTracking(fields) {
  try {
    const result = await FieldTracking.insertMany(fields);
    return result;
  } catch (error) {
    next(error);
  }
}

/**
 * 
 *  Fetch comapign url data with pagination  
 * */
exports.compaignUrl = async (req, res) => {
  try {
    let siteId = req.body.siteId;
    let compaignUrls = await Session.aggregate([
      {
        '$match': {
          'siteId': new mongoose.Types.ObjectId(siteId),
          'utmSource': 'google'
        }
      }, {
        '$facet': {
          'engine': [
            {
              '$match': {
                'engine': {
                  '$ne': ''
                }
              }
            }, {
              '$group': {
                '_id': '$engine',
                'engine': {
                  '$addToSet': '$engine'
                }
              }
            }, {
              '$unwind': '$engine'
            }, {
              '$project': {
                '_id': 0
              }
            }
          ],
          'utmTerm': [
            {
              '$match': {
                'utmTerm': {
                  '$ne': ''
                }
              }
            }, {
              '$group': {
                '_id': '$utmTerm',
                'utmTerm': {
                  '$addToSet': '$utmTerm'
                }
              }
            }, {
              '$unwind': '$utmTerm'
            }, {
              '$project': {
                '_id': 0
              }
            }
          ],
          'utmContent': [
            {
              '$match': {
                'utmContent': {
                  '$ne': ''
                },
              }
            }, {
              '$group': {
                '_id': '$utmContent',
                'utmContent': {
                  '$addToSet': '$utmContent'
                }
              }
            }, {
              '$unwind': '$utmContent'
            }, {
              '$project': {
                '_id': 0
              }
            }
          ],
          'utmMedium': [
            {
              '$match': {
                'utmMedium': {
                  '$ne': ''
                }
              }
            }, {
              '$group': {
                '_id': '$utmMedium',
                'utmMedium': {
                  '$addToSet': '$utmMedium'
                }
              }
            }, {
              '$unwind': '$utmMedium'
            }, {
              '$project': {
                '_id': 0
              }
            }
          ],
          'utmCampaign': [
            {
              '$match': {
                'utmCampaign': {
                  '$ne': ''
                }
              }
            }, {
              '$group': {
                '_id': '$utmCampaign',
                'utmCampaign': {
                  '$addToSet': '$utmCampaignn'
                }
              }
            }, {
              '$unwind': '$utmCampaign'
            }, {
              '$project': {
                '_id': 0
              }
            }
          ],
          'gclid': [
            {
              '$match': {
                'gclid': {
                  '$ne': ''
                }
              }
            }, {
              '$group': {
                '_id': '$gclid',
                'gclid': {
                  '$addToSet': '$gclid'
                }
              }
            }, {
              '$unwind': '$gclid'
            }, {
              '$project': {
                '_id': 0
              }
            }
          ],
          'records': [
            {
              '$match': { siteId: new mongoose.Types.ObjectId(siteId), utmTerm: { $ne: "" } },
            },
            {
              '$group': {
                '_id': '$utmTerm',
                'myCount': {
                  '$sum': 1
                }
              }
            },
            { $sort: { myCount: -1 } }
          ]
        }
      }
    ]);
    return res.send({ code: 200, data: compaignUrls });
  } catch (error) {
    console.log(error);
    return res.send({ code: 500, data: error });
  }
}

