const Recording = require('../models/recording.model');
const Page = require('../models/page.model');
const Pagestats = require('../models/pagestats.model');
const Session = require('../models/session.model');
const Event = require('../models/event.model');
const MetaData = require('../models/metaData.model');
const Form = require('../models/form.model');
const FormField = require('../models/formField.model');
const FieldReport = require('../models/fieldReport.model');
const FieldTracking = require('../models/fieldTracking.model');
const Website = require('../models/website.model');
const FormStats = require('../models/formStats.model');
const Interaction = require('../models/interactions.model');
const Users = require('../models/users.model');
const { _ } = require('underscore');
const inbound = require('inbound');
const Funnel = require('../models/funnel.model');
const Siteids = require('../models/siteid.model');
const mongoose = require('mongoose');
const cron = require('node-cron');
const moment = require('moment');
const rp = require('request-promise');
const AWS = require('aws-sdk');
var CryptoJS = require("crypto-js");
const s3 = new AWS.S3({
  accessKeyId: 'AKIAI6FN6GALLBUTYK6Q',
  secretAccessKey: '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'
});
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const { DOMAIN, MONGO_DB_URI } = require('../../config/vars');

//Check if user is sending its own website.
async function checkWebsites(siteId = 0, userId = 0, owner = 1) {
  var siteIdsArray = [];

  if (!userId) {
    return false;
  }

  if (!siteId) {
    return false;
  }
  let currentUserSites = await Siteids.find({ userId: userId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
  if (currentUserSites.length) {
    return true;
  }
  else if (owner !== 1) {
    let currentUserOwnerId = await Users.findOne({ _id: userId }, { ownerId: 1 }).select('siteIds').exec();
    if (currentUserOwnerId && currentUserOwnerId.ownerId) {
      let currentUserSitesAgain = await Siteids.find({ userId: currentUserOwnerId.ownerId, siteIds: { $in: [siteId] } }).select('siteIds').exec();
      if (currentUserSitesAgain.length) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
}

exports.addTracking = async (req, res, next) => {
  try {
    var decryption_string = req.headers['x-auth-token'];
    var bytes = CryptoJS.AES.decrypt(req.body.data, decryption_string);
    var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    req.body = decryptedData;
    let todayDate = moment().format('YYYY-MM-DD');
    const website = await Website.findOne({
      _id: req.body.siteId
    }).exec();
    if (website.honorTracking === true && req.body.honorTracking === true) {
      return res.status(200).send({ status: 404, data: { msg: 'you have enabled setting for Honour Tracking.' } });
    }
    if (website.stopRecording === true) {
      return res.status(200).send({
        status: 404,
        data: {
          msg: 'Account Admin has stopped recording for this site. '
        }
      });
    }
    const url = MONGO_DB_URI;
    const dbName = website._id.toString();
    console.log(dbName);
    const client = new MongoClient(url, { useNewUrlParser: true });
    let pageName = req.body.pageName;
    let queryIndex = [];
    let idenfrs = pageName.split("?");
    let newStr = idenfrs[0];
    let queryString = idenfrs[0];
    let mergeURLs = website.mergeURLs;
    let newPageName = req.body.pageName.split("?")[0];
    let data = req.body;
    let recordingId;
    let userPageName = req.body.pageName.split('?')[0];
    const session2 = await Session.findOne({
      _id: req.body.sessionId
    }).select('funnelRecordData countryCode ip countryName _id pages recordings clickRage formInteract formSubmit clickError pageCount recDuration latitude longitude device funnelPages').exec();
    let ip;
    if (website.iPAnonymization === true) {
      ip = session2.ip.slice(0, -3);
      ip += '####';
    } else {
      ip = session2.ip;
    }
    let recordingHtml;
    if (req.body.recordingId === "") {
      recordingHtml = req.body.htmlCopy;
      const recording = new Recording({
        siteId: req.body.siteId,
        site: req.body.siteId,
        sessionId: req.body.sessionId,
        htmlCopy: req.body.htmlCopy,
        docHeight: req.body.docHeight,
        width: req.body.width,
        height: req.body.height,
        path: req.body.path,
        url: req.body.url,
        startTime: req.body.startTime,
        duration: req.body.duration,
        resolution: req.body.resolution,
        referer: req.body.referer,
        osName: req.body.osName,
        browserName: req.body.browserName,
        countryCode: session2.countryCode,
        ip: ip,
        device: req.body.device,
        countryName: session2.countryName,
        pageName: req.body.pageName.split("?")[0],
        adUrl: req.body.pageName.split("?")[1],
        recordings: [],
        scroll: req.body.scrollPercentage,
        clickRage: req.body.clickRage,
        formInteract: req.body.formInteract,
        formSubmit: req.body.formSubmit,
        clickError: req.body.clickError,
        styles: req.body.styles,
        latitude: session2.latitude,
        longitude: session2.longitude,
        projectId: req.body.projectId
      });
      const recordignData = await recording.save();
      recordingId = recordignData._id;
      if (session2.clickRage === 0) {
        session2.clickRage = req.body.clickRage;
      } else if (session2.clickRage === 0) {
        session2.clickRage = req.body.clickRage;
      }
      if (session2.formInteract === 0) {
        session2.formInteract = req.body.formInteract;
      }
      if (session2.formSubmit === 0) {
        session2.formSubmit = req.body.formSubmit;
      }
      if (session2.clickError === 0) {
        session2.clickError = req.body.clickError;
      }
      session2.pages.push(req.body.pageName.split("?")[0]);
      let funelPages = session2.funnelPages;
      let indexPageName = req.body.pageName.split("?")[0];
      let valExist = funelPages.findIndex(page => page === indexPageName);
      if (valExist !== undefined) {
        if (valExist === -1) {
          let funnelCalArrLength = {};
          funnelCalArrLength['page' + (session2.funnelPages.length + 1)] = req.body.pageName.split("?")[0];
          console.log(funnelCalArrLength);
          session2.funnelRecordData.push(funnelCalArrLength);
          session2.funnelPages.push(req.body.pageName.split("?")[0]);
        }
      }
      session2.recordings.push(recordingId);
      session2.pageCount += 1;
      session2.recDuration += parseInt(req.body.currentRecTime);
      await session2.save();
      if (website.protocol === 'http') {
        if (req.body.pageName === '/index') {
          userPageName = '/';
        }

        // uri: DOMAIN+'/v1/website/page/download',
        var options = {
          method: 'POST',
          uri: "http://52.56.228.186:9000/v1/website/page/download",
          body: {
            siteId: req.body.siteId,
            recordingId: recordignData._id,
            domain: website.protocol + '://' + website.name + userPageName,
            pageName: userPageName.split('?')
          },
          json: true // Automatically stringifies the body to JSON
        };
        rp(options)
          .then(function (parsedBody) {
            // POST succeeded...
          })
          .catch(function (err) {
            // POST failed...
          });
      }
    } else {
      recordingId = req.body.recordingId;
      const recordings = await Recording.findOne({
        _id: recordingId
      }, (err, recording) => {
        recording.duration = req.body.duration;
        recording.clickRage = req.body.clickRage;
        recording.formInteract = req.body.formInteract;
        recording.formSubmit = req.body.formSubmit;
        recording.clickError = req.body.clickError;
        recording.scroll = req.body.scrollPercentage;
        recording.save();
      });
      recordingHtml = recordings.htmlCopy;
      const session2 = await Session.findOne({
        _id: req.body.sessionId
      }).select('_id recordings clickRage formInteract formSubmit clickError pageCount recDuration latitude longitude device').exec();
      if (session2.clickRage === 0) {
        session2.clickRage = req.body.clickRage;
      }
      if (session2.formInteract === 0) {
        session2.formInteract = req.body.formInteract;
      }
      if (session2.formSubmit === 0) {
        session2.formSubmit = req.body.formSubmit;
      }
      if (session2.clickError === 0) {
        session2.clickError = req.body.clickError;
      }
      session2.recDuration += parseInt(req.body.currentRecTime);
      await session2.save();
    }
    const pages = await Page.findOne({
      $and: [{
        name: req.body.pageName.split("?")[0]
      }, {
        websiteId: req.body.siteId
      }]
    }).exec();
    let page;
    if (pages) {
      page = pages;
    } else {
      const newPage = new Page({
        websiteId: req.body.siteId,
        sessionId: req.body.sessionId,
        name: req.body.pageName.split("?")[0],
        pageHeight: req.body.docHeight,
        html: req.body.htmlCopy,
        width: req.body.width,
        height: req.body.height,
        path: req.body.path,
        startTime: req.body.startTime,
        engagmentTime: req.body.engagmentTime,
        loadingTime: req.body.loadingTime,
        referer: req.body.referer,
        resolution: req.body.resolution,
        osName: req.body.osName,
        browserName: req.body.browserName,
        device: req.body.device,
        countryName: session2.countryName,
        traficSource: req.body.traficSource,
        ip: ip,
        pageName: newPageName,
        clickRage: req.body.clickRage,
        formInteract: req.body.formInteract,
        formSubmit: req.body.formSubmit,
        clickError: req.body.clickError,
        projectId: req.body.projectId
      });
      const newPageData = await newPage.save();
      page = newPageData;
    }
    if (req.body.forms.length > 0) {
      const pageForm = await Form.findOne({
        $and: [{
          siteId: req.body.siteId
        }, {
          pageName: req.body.pageName.split("?")[0]
        }]
      }).exec();
      if (pageForm === null) {
        if (req.body.forms[0].fields.length > 0) {
          let forms = req.body.forms;
          for (let index = 0; index < forms.length; index++) {
            const newForm = new Form({
              siteId: req.body.siteId,
              pageName: req.body.pageName.split("?")[0],
              pageId: page._id,
              index: forms[index].index,
              formName: forms[index].formName,
            });
            const formData = await newForm.save();
            const formFields = _.map(forms[index].fields, (field) => {
              field.pageName = req.body.pageName.split("?")[0];
              field.pageId = page._id;
              field.formId = formData._id;
              field.form = formData._id;
              return field;
            });
            await FormField.insertMany(formFields);
          }
        }
      }
    }

    const events = req.body.events;
    let dataEv = _.map(events, (event) => {
      if (event.type === 'click') {
        event.clickCount = 1;
      }
      if (event.type === 'mousemove') {
        event.scrolCount = 1;
      }
      event.recordingId = recordingId;
      event.pageId = page._id;
      event.siteId = req.body.siteId;
      event.site = req.body.siteId;
      event.pageName = req.body.pageName.split("?")[0];
      event.clickRage = req.body.clickRage;
      event.formInteract = req.body.formInteract;
      event.formSubmit = req.body.formSubmit;
      event.clickError = req.body.clickError;
      event.sessionId = session2._id;
      event.device = session2.device;
      event.createdAt = new Date().toISOString();
      return event;
    });

    await Event.insertMany(dataEv);
    if (dataEv.length > 0) {
      client.connect(function (err, client) {
        assert.equal(null, err);
        const db = client.db(dbName);
        db.collection('events').insertMany(dataEv, function (err, r) {

        });
        client.close();
      });
    }

    let totalClicks = _.filter(req.body.events, function (clickData) {
      if (clickData.type === 'click') {
        return clickData;
      }
    });
    if (totalClicks.length > 0) {
      totalClicks = totalClicks.length;
    } else {
      totalClicks = 0
    }
    let pageSize = await calSize(recordingHtml.length);
    let isRecExist = await MetaData.findOne({ recordingId: recordingId }).exec();
    if (isRecExist) {
      let metaData = await MetaData.findOne({ $and: [{ recordingId: req.body.recordingId }, { date: todayDate }] }).exec();
      metaData.visitTime = req.body.duration;
      metaData.engagementTime = req.body.engagmentTime;
      metaData.loadingTime = req.body.loadingTime;
      metaData.clicks += totalClicks;
      if (metaData.formSubmit === 0) {
        metaData.formSubmit += req.body.formSubmit;
      }
      metaData.clickError = req.body.clickError;
      metaData.clickRage = req.body.clickRage;
      if (metaData.formInteract === 0) {
        metaData.formInteract += req.body.formInteract;
      }
      metaData.duration += req.body.duration;
      metaData.scroll = req.body.scrollPercentage;
      metaData.save();
    } else {
      const pageData = new MetaData({
        recordingId: recordingId,
        pageId: page._id,
        siteId: req.body.siteId,
        site: req.body.siteId,
        pageName: req.body.pageName.split("?")[0],
        loadingTime: req.body.loadingTime,
        engagementTime: req.body.engagmentTime,
        visitTime: req.body.duration,
        clickRage: req.body.clickRage,
        formInteract: req.body.formInteract,
        formSubmit: req.body.formSubmit,
        clickError: req.body.clickError,
        sessionId: session2._id,
        pageSize: pageSize,
        device: session2.device,
        date: todayDate,
        clicks: totalClicks,
        scroll: req.body.scrollPercentage
      });
      await pageData.save();
    }
    console.log(req.body.formStats);
    if (req.body.formStats !== undefined) {
      const convertData = new Interaction({
        referer: req.body.referer,
        resolution: req.body.resolution,
        osName: req.body.osName,
        browserName: req.body.browserName,
        device: req.body.device,
        countryName: req.body.countryName,
        clickRage: req.body.clickRage,
        formInteract: req.body.formInteract,
        formSubmit: req.body.formSubmit,
        clickError: req.body.clickError,
        conversion: req.body.formStats.converted,
        siteId: req.body.siteId,
        pageName: req.body.pageName.split("?")[0],
        recordingId: recordingId
      });
      if (req.body.formSubmit === 'true') {
        convertData.conversion = 1;
      }
      await convertData.save();
      const interactions = req.body.formStats.interactions;
      let phone = '';
      let surName = '';
      let zipCode = '';
      // let price = [];
      let formStats = [];
      if (req.body.formStats.interactions.length > 0) {
        for (let index = 0; index < req.body.formStats.interactions.length; index++) {
          if (req.body.formStats.interactions[index].formSubmit === 1) {
            req.body.formStats.interactions[index].formSubmit = 1;
            req.body.formStats.interactions[index].dropOff = 0;
          } else {
            req.body.formStats.interactions[index].formSubmit = 0;
            req.body.formStats.interactions[index].dropOff = 1;
          }
          let formStat = new FormStats({
            siteId: req.body.siteId,
            pageName: req.body.pageName.split("?")[0],
            recordingId: recordingId,
            device: req.body.device,
            formIndex: req.body.formStats.interactions[index].formIndex,
            formSubmit: req.body.formStats.interactions[index].formSubmit,
            dropOff: req.body.formStats.interactions[index].dropOff,
            formInteract: req.body.formStats.interactions[index].formInteract,
            createdAt: new Date().toISOString()
          });
          await formStat.save();
          let dataFieldsArray = [];
          const fieldsData = _.map(interactions[index].fieldInteract, function (field) {
            field.siteId = req.body.siteId;
            field.recordedDate = new Date().toISOString();
            field.referer = req.body.referer;
            field.resolution = req.body.formStats.resolution;
            field.osName = req.body.formStats.osName;
            field.browserName = req.body.formStats.browserName;
            field.device = req.body.formStats.device;
            field.countryName = req.body.formStats.countryName;
            field.pageName = req.body.pageName.split("?")[0];
            field.clickRage = req.body.clickRage;
            field.formInteract = req.body.formInteract;
            field.formSubmit = req.body.formSubmit;
            field.clickError = req.body.clickError;
            field.recordingId = recordingId;
            field.formIndex = req.body.formStats.interactions[index].formIndex;
            field.createdAt = new Date().toISOString();
            if (field.fieldName === 'phone1' || field.fieldName === 'mobile' || field.fieldName === 'number') {
              phone = field.value;
            } else if (field.fieldName === 'last_name' || field.fieldName === 'lastname') {
              surName = field.value !== undefined ? field.value : ''
            } else if (field.fieldName === 'zip') {
              zipCode = field.value;
            }
            return field;
          });
          let sessionData = await Session.findById(req.body.sessionId).exec();
          if (sessionData.surname && sessionData.surname.findIndex(x => x.recordingId === req.body.recordingId) === -1) {
            if (surName && surName !== '') {
              let resSurName = sessionData.surname;
              sessionData.surname = []
              resSurName.push({ surname: surName, recordingId: req.body.recordingId });
              sessionData.surname = resSurName;
            }
          } else {
            let index = sessionData.surname.findIndex(x => x.recordingId === req.body.recordingId);
            let resSurName = sessionData.surname;
            sessionData.surname = [];
            resSurName[index].surname = surName;
            sessionData.surname = resSurName;
          }
          if (sessionData.phone && sessionData.phone.findIndex(x => x.recordingId === req.body.recordingId) === -1) {
            if (phone && phone !== '') {
              let resphone = sessionData.phone;
              sessionData.resphone = []
              resphone.push({ phone: phone, recordingId: req.body.recordingId });
              sessionData.phone = resphone;
            }
          } else {
            let index = sessionData.phone.findIndex(x => x.recordingId === req.body.recordingId);
            let resphone = sessionData.phone;
            sessionData.phone = [];
            resphone[index].phone = phone;
            sessionData.phone = resphone;
          }
          if (sessionData.zipCode && sessionData.zipCode.findIndex(x => x.recordingId === req.body.recordingId) === -1) {
            if (zipCode && zipCode !== '') {
              let reszipCode = sessionData.zipCode;
              sessionData.reszipCode = []
              reszipCode.push({ zipCode: zipCode, recordingId: req.body.recordingId });
              sessionData.zipCode = reszipCode;
            }
          } else {
            let index = sessionData.zipCode.findIndex(x => x.recordingId === req.body.recordingId);
            let reszipCode = sessionData.zipCode;
            sessionData.zipCode = [];
            reszipCode[index].zipCode = zipCode;
            sessionData.zipCode = reszipCode;
          }
          await sessionData.save();
          let fields = [];
          let queryString = "";
          dropOutFields = [];
          const filterDropOut = _.filter(fieldsData, function (dropOut) {
            if (dropOut.formSubmit === 0 && dropOut.interact === 1 && dropOut.value !== undefined) {
              let fieldName = dropOut.fieldName;
              let fieldVal = dropOut.value;
              dropOutFields.push({ fieldName: fieldName, fieldVal: fieldVal });
              queryString += '&' + dropOut.fieldName + '=' + dropOut.value;
              return dropOut;
            }
          });
          await saveAllData(dbName, formStat, fieldsData);
          await saveTracking(fieldsData);
        }
      }
    }
    var encryptedData = {
      recordingId: recordingId,
      image: page.image
    };
    var stringifyJSONToSent = JSON.stringify(encryptedData);
    var encrypted = CryptoJS.AES.encrypt(stringifyJSONToSent, "Recording And Session Started For FloPanda").toString();

    return res.send({
      status: '200',
      data: encrypted
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
};


exports.get = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const fromDate = new Date(req.params.from);
    let to;
    if (req.query.funnel !== undefined) {
      let toDateMinusOne = moment(req.params.to).subtract('1', 'days').toDate();
      let dateDifference = moment().diff(toDateMinusOne, 'days');
      if (dateDifference === 0) {
        let timeMinusTimeZone = moment(toDateMinusOne).format('MMM DD YYYY') + " " + moment().subtract(5, 'hours').format("hh:mm A")
        let updatedTime = moment(timeMinusTimeZone).format('MMM DD YYYY') + " " + moment().subtract(12, 'minutes').format("hh:mm A");
        to = new Date(updatedTime);
      } else {
        to = new Date(req.params.to);
      }
    } else {
      to = new Date(req.params.to);
    }
    delete req.query.funnel;
    const limit = parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 20);
    const site = req.params.siteId;
    const isBatch = req.params.isBatch;
    let queryData = req.query;
    let sort = {};
    if (isBatch === 'true' && req.params.ip === 'false') {
      sort.ip = -1;
    } else if (isBatch === 'true' && req.params.ip === 'true') {
      sort.ip = 1;
    } else {
      sort.createdAt = -1;
    }
    let reqQueryLenth = Object.keys(req.query).length;
    queryData.siteId = new mongoose.Types.ObjectId(site);
    queryData.createdAt = {
      $gt: fromDate,
      $lt: to
    };
    queryData.type = req.query.type;
    let stringArr = [];
    let orData = {};
    if (reqQueryLenth > 1) {
      if (req.query.ip !== undefined) {
        queryData.ip = { $regex: new RegExp(req.query.ip), $options: "si" };
      }
      if ((req.query.visitMin !== undefined) && (req.query.visitMax)) {
        queryData.recDuration = { $gte: parseInt(req.query.visitMin), $lte: parseInt(req.query.visitMax) };
      } else if (req.query.visitMin !== undefined) {
        queryData.recDuration = { $gte: parseInt(req.query.visitMin) };
      } else if (req.query.visitMax !== undefined) {
        queryData.recDuration = { $gte: 0, $lte: parseInt(req.query.visitMax) };
      }
      if (queryData.device !== undefined) {
        if (typeof queryData.device === 'string') {
          queryData.device = req.query.device;
        } else {
          queryData.device = { $in: req.query.device };
        }
      }
      if (queryData.pageName !== undefined) {
        if (typeof queryData.pageName === 'string') {
          queryData.pageName = req.query.pageName;
        } else {
          queryData.pageName = { $in: req.query.pageName };
        }
      }
      if (queryData.network !== undefined) {
        if (typeof queryData.network === 'string') {
          queryData.network = req.query.network;
        } else {
          queryData.network = { $in: req.query.network };
        }
      }
      if (queryData.formInteract !== undefined) {
        req.query.formInteract = parseInt(req.query.formInteract);
      }

      if (queryData.formSubmit !== undefined) {
        req.query.formSubmit = parseInt(req.query.formSubmit);
      }
      if (queryData.clickRage !== undefined) {
        if (parseInt(req.query.clickRage) === 0) {
          req.query.clickRage = 0;
        } else {
          req.query.clickRage = { $gte: parseInt(req.query.clickRage) };
        }
      }
      if (queryData.clickError !== undefined) {
        req.query.clickError = parseInt(req.query.clickError);
      }
      if (queryData.paymentTag !== undefined) {
        req.query.paymentTag = parseInt(req.query.paymentTag);
      }
      if (queryData.utmCampaign !== undefined) {
        sort.utmCampaign = parseInt(queryData.utmCampaign);
      }
      if (queryData.adName !== undefined) {
        sort.adName = parseInt(queryData.adName);
      }
      if (queryData.utmContent !== undefined) {
        sort.utmContent = parseInt(queryData.utmContent);
      }

      var arrPage = [];
      if (req.query.drops && req.query.pages) {
        // arrPage.push(req.query.pageName);
        // arrPage.push(req.query.pages);
        if (typeof req.query.pages === 'string') {
          arrPage.push(req.query.pages);
        } else {
          arrPage = req.query.pages;
        }
        delete queryData.pages;
      } else if (req.query.pages) {
        if (typeof req.query.pages === 'string') {
          arrPage.push(req.query.pageName);
          if (arrPage[0] !== req.query.pages) {
            arrPage.push(req.query.pages);
          }
          delete queryData.pages;
          queryData.funnelPages = { $all: arrPage };
        }
        else if (req.query.pages.length === 1) {
          queryData.pages = { $in: req.query.pages };
        } else if (req.query.pages.length === 2) {
          arrPage = req.query.pages;
          arrPage.unshift(req.query.pageName);
          delete queryData.pages;
          queryData.funnelPages = { $eq: arrPage };
        }
        else {
          queryData.pages = { $nin: req.query.pages };
        }
      }
      if (req.query.surname !== undefined) {
        queryData['surname.surname'] = { $regex: new RegExp("^" + req.query.surname), $options: "si" };
      }
      if (req.query.zipCode !== undefined) {
        queryData['zipCode.zipCode'] = { $regex: new RegExp("^" + req.query.zipCode), $options: "si" };
      }
      if (req.query.phone !== undefined) {
        queryData['phone.phone'] = { $regex: new RegExp("^" + req.query.phone), $options: "si" };
      }
      if (req.query.price !== undefined) {
        queryData.price = { $regex: new RegExp("^" + req.query.price), $options: "si" };
      }
    }
    delete queryData.visitMin;
    delete queryData.visitMax;
    // delete queryData.drops;
    delete queryData.utmCampaign;
    delete queryData.adName;
    delete queryData.utmContent;
    delete queryData.phone;
    delete queryData.zipCode;
    delete queryData.surname;
    // delete queryData.network;
    if (sort.utmCampaign !== undefined) {
      queryData.utmCampaign = { $ne: '' };
    }
    if (sort.adName !== undefined) {
      queryData.adName = { $ne: '' };
    }
    if (sort.utmContent !== undefined) {
      queryData.utmContent = { $ne: '' };
    }
    delete queryData.search;
    let recordings = [];
    let total = 0;
    if (arrPage === undefined) {
      arrPage = [];
    }
    console.log(queryData);
    if (req.query.drops === undefined && arrPage.length === 0) {
      recordings = await Session.find({
        $and: [queryData]
      }).sort(sort).collation({ locale: "en_US", numericOrdering: true }).skip(offset).limit(limit).exec();
      // total = await Session.countDocuments({
      //   $and: [queryData]
      // }).sort(sort).exec();
      total = await Session.aggregate(
        [
          {
            '$match': {
              $and: [queryData]
            }
          },
          {
            $group:
            {
              _id: null,
              myCount: { "$sum": 1 }
            }
          }
        ]
      );
    } else {
      if (req.query.drops !== undefined) {
        if (req.query.drops === '2') {
          delete queryData.drops;
          recordings = await Session.aggregate(
            [
              {
                '$match': {
                  $and: [queryData]
                }
              }, {
                '$match': {
                  'funnelRecordData.page2': { $ne: arrPage[0] }
                }
              }, {
                '$sort': {
                  'createdAt': -1
                }
              }, {
                '$skip': offset
              }, {
                '$limit': limit
              }
            ]
          );
          total = await Session.aggregate(
            [
              {
                '$match': {
                  $and: [queryData]
                }
              }, {
                '$match': {
                  'funnelRecordData.page2': { $ne: arrPage[0] }
                }
              },
              {
                $group:
                {
                  _id: null,
                  myCount: { "$sum": 1 }
                }
              }
            ]
          );
        } else if (req.query.drops === '3') {
          delete queryData.drops;
          recordings = await Session.aggregate(
            [
              {
                '$match': {
                  $and: [queryData]
                }
              }, {
                '$match': {
                  'funnelRecordData.page2': arrPage[0],
                  'funnelRecordData.page3': { $ne: arrPage[1] }
                }
              }, {
                '$sort': {
                  'createdAt': -1
                }
              }, {
                '$skip': offset
              }, {
                '$limit': limit
              }
            ]
          );
          total = await Session.aggregate(
            [
              {
                '$match': {
                  $and: [queryData]
                }
              }, {
                '$match': {
                  'funnelRecordData.page2': arrPage[0],
                  'funnelRecordData.page3': { $ne: arrPage[1] }
                }
              },
              {
                $group:
                {
                  _id: null,
                  myCount: { "$sum": 1 }
                }
              }
            ]
          );
        }

      } else {
        if (arrPage.length === 0) {
          recordings = await Session.aggregate(
            [
              {
                '$match': {
                  $and: [queryData]
                }
              }, {
                '$sort': {
                  'createdAt': -1
                }
              }, {
                '$skip': offset
              }, {
                '$limit': limit
              }
            ]
          );
        } else if (arrPage.length === 2) {
          delete queryData.funnelPages;
          recordings = await Session.aggregate(
            [
              {
                '$match': {
                  $and: [queryData]
                }
              }, {
                '$match': {
                  'funnelRecordData.page2': arrPage[1]
                }
              }, {
                '$sort': {
                  'createdAt': -1
                }
              }, {
                '$skip': offset
              }, {
                '$limit': limit
              }
            ]
          );
          total = await Session.aggregate(
            [
              {
                '$match': {
                  $and: [queryData]
                }
              }, {
                '$match': {
                  'funnelRecordData.page2': arrPage[1]
                }
              },
              {
                $group:
                {
                  _id: null,
                  myCount: { "$sum": 1 }
                }
              }
            ]
          );
        } else if (arrPage.length === 3) {
          delete queryData.funnelPages;
          recordings = await Session.aggregate(
            [
              {
                '$match': {
                  $and: [queryData]
                }
              }, {
                '$match': {
                  'funnelRecordData.page2': arrPage[1],
                  'funnelRecordData.page3': arrPage[2]
                }
              }, {
                '$sort': {
                  'createdAt': -1
                }
              }, {
                '$skip': offset
              }, {
                '$limit': limit
              }
            ]
          );
          total = await Session.aggregate(
            [
              {
                '$match': {
                  $and: [queryData]
                }
              }, {
                '$match': {
                  'funnelRecordData.page2': arrPage[1],
                  'funnelRecordData.page3': arrPage[2]
                }
              },
              {
                $group:
                {
                  _id: null,
                  myCount: { "$sum": 1 }
                }
              }
            ]
          );
        }

      }
    }
    return res.status(200).send({
      code: 200,
      sessions: recordings,
      totalRecording: total.length > 0 ? total[0].myCount : 0
    });
  } catch (error) {
    next(error);
  }
};


exports.getCsvDetails = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    const site = req.params.siteId;
    let queryData = req.query;
    queryData.siteId = new mongoose.Types.ObjectId(site);
    queryData.createdAt = {
      $gt: fromDate,
      $lt: toDate
    };
    queryData.pageCount = { $lte: req.params.pageCount };
    if (req.query.duration !== undefined) {
      queryData.recDuration = { $lte: req.query.duration };
    }
    if (queryData.device !== undefined) {
      queryData.device = {
        $in: ['device', req.query.device]
      };
    }
    if (queryData.favorite !== undefined) {
      let favorite = parseInt(req.query.favorite);
      if (favorite === 1) {
        queryData.favorite = true;
      } else {
        queryData.favorite = false;
      }
    }
    if (queryData.watch !== undefined) {
      let watch = parseInt(req.query.watch);
      if (watch === 1) {
        queryData.watch = true;
      } else {
        queryData.watch = false;
      }
    }

    if (queryData.formInteract !== undefined) {
      queryData.formInteract = parseInt(req.query.formInteract);
    }

    if (queryData.formSubmit !== undefined) {
      queryData.formSubmit = parseInt(req.query.formSubmit);
    }
    if (queryData.clickRage !== undefined) {
      // queryData.clickRage = parseInt(req.query.clickRage);
      queryData.clickRage = { $gte: parseInt(req.query.clickRage) };
    }

    if (queryData.clickError !== undefined) {
      queryData.clickError = parseInt(req.query.clickError);
    }
    if (queryData.paymentTag !== undefined) {
      req.query.paymentTag = parseInt(req.query.paymentTag);
    }
    const total = await Session.countDocuments({
      $and: [queryData]
    }).exec();
    const recordings = await Session.find({
      $and: [queryData]
    }).populate('recordings', 'duration').sort({
      createdAt: -1
    }).sort({
      createdAt: -1
    }).exec();
    return res.status(200).send({
      code: 200,
      sessions: recordings,
      totalRecording: total
    });
  } catch (error) {
    next(error);
  }
};


exports.pageTrackData = async (req, res, next) => {
  try {
    const pageId = req.body.pageId;
    const siteId = req.body.siteId;
    const type = req.body.type;
    const from = new Date(req.body.fromDate);
    const to = new Date(req.body.toDate);
    const device = req.body.device;
    let website = await Website.findOne({ _id: siteId }).select('protocol name').exec();
    let dbName = website._id.toString();
    const page = await Page.findOne({
      _id: pageId
    }, 'height pageHeight name width', (err, pages) => {
      if (err) {
        console.log(err);
      }
    });
    let recordingHtml = await Recording.findOne({ $and: [{ siteId: siteId }, { pageName: page.name }] }).sort({ createdAt: -1 }).limit(1).select('htmlCopy').exec();

    let heatMapData;
    if (type === 'click') {
      heatMapData = await getEventBySite(dbName, pageId, device, req.body.fromDate, req.body.toDate, 'click');
    } else if (type === 'movement') {
      heatMapData = await getEventBySite(dbName, pageId, device, req.body.fromDate, req.body.toDate, 'mousemove');
    } else if (type === 'scroll') {
      let scrolls = await calculateScroll(dbName, pageId, device, req.body.fromDate, req.body.toDate, 'scroll');
      heatMapData = await scrolCalculate(page, scrolls, page.height);
    } else if (type === 'attention') {
      let mouseMoves = await calculateAttention(dbName, pageId, device, req.body.fromDate, req.body.toDate, 'mousemove');
      heatMapData = await attentionCalculate(page, mouseMoves, page.height);
    }
    const data = await MetaData.aggregate([
      {
        $match:
        {

          $and: [
            {
              device: req.body.device
            },
            {
              pageName: page.name
            },
            {
              createdAt:
              {
                $gte: from, $lte: to
              }
            },
            {
              siteId: new mongoose.Types.ObjectId(siteId)
            }
          ]
        }
      },
      {
        $sort: {
          pageId: -1
        }
      },
      {
        $group: {
          _id: '$pageName',
          pageName: {
            $first: '$pageName'
          },
          pageSize: {
            $first: '$pageSize'
          },
          pageName: {
            $first: '$pageName'
          },
          pageId: {
            $first: '$pageId'
          },
          formInteract: {
            $first: '$formInteract'
          },
          clickRage: {
            $first: '$clickRage'
          },
          formSubmit: {
            $first: '$formSubmit'
          },
          clickError: {
            $first: '$clickError'
          },
          clicks: {
            $sum: '$clicks'
          },
          scrollPercentage: {
            $avg: '$scroll'
          },
          engageTime: {
            $avg: '$engagementTime'
          },
          visitTime: {
            $avg: '$visitTime'
          },
          loadingTime: {
            $avg: '$loadingTime'
          },
          views: {
            $sum: 1
          },
        },
      }
    ]);
    if (req.body.device === 'Desktop') {
      width = '1301';
      height = '663';
    } else if (req.body.device === 'Tablet') {
      width = '768';
      height = '992';
    } else {
      width = '320';
      height = '767';
    }
    if (website.protocol === 'https') {
      return res.status(200).send({
        code: 200,
        data: {
          pageHeight: page.pageHeight,
          width: page.width,
          height: page.height,
          html: recordingHtml.htmlCopy,
          heatMap: heatMapData,
          pages: data,
          protocol: website.protocol
        }
      });
    } else {
      let params = {
        Bucket: 'flopandascripts',
        Key: 'websites/' + siteId + '/' + page._id + '.html',
        ResponseContentEncoding: 'text/html'
      };
      s3.getObject(params, function (s3Err, newData) {
        if (s3Err) {
          console.log(s3Err);
        } else {
          let htmlCopy = newData.Body.toString('utf-8');
          return res.json({
            code: 200,
            data: {
              pageHeight: page.pageHeight,
              width: page.width,
              height: page.height,
              html: htmlCopy,
              heatMap: heatMapData,
              pages: data,
              protocol: website.protocol
            }
          });
        }
      });
    }
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.pageData = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const from = new Date(req.params.from);
    const to = new Date(req.params.to);
    const siteId = req.params.siteId;
    const timeData = await pageMeta(from, to, siteId);
    const cliclsData = await clickData(from, to, siteId);
    const pages = [];
    let queryString = req.query;
    for (let i = 0; i < timeData.length; i++) {
      queryString.websiteId = siteId;
      queryString.name = timeData[i]._id;
      let page = await Page.findOne({
        $and: [queryString]
      }).exec();
      if (page !== null) {
        let views = await Recording.countDocuments({
          $and: [{
            siteId: siteId
          }, {
            pageName: timeData[i]._id
          }, {
            createdAt: {
              $gt: from,
              $lt: to
            }
          }]
        }).exec();
        let size = await calSize(page.html.length);
        let pageIndex = pages.findIndex(x => x.name === timeData[i]._id);
        let clicks = 0;
        let scroll = 0;
        if (pageIndex === -1) {
          let clickIndex = cliclsData.findIndex(x => x._id === timeData[i]._id);
          if (clickIndex !== -1) {
            clicks = cliclsData[clickIndex].clicks;
            scroll = Math.round((cliclsData[clickIndex].scroll / cliclsData[clickIndex].total) * 100);
          }
          pages.push({
            name: timeData[i]._id,
            engageTime: timeData[i].engageTime,
            visitTime: timeData[i].visitTime,
            loadingTime: timeData[i].loadingTime,
            clicks: clicks,
            scrollPercentage: scroll,
            pageSize: size,
            views: views,
            pageId: page._id,
          });
        }
      }
    }
    const sortedData = _.sortBy(pages, 'views');
    sortedData.reverse();
    res.status(200).send({
      code: 200,
      data: sortedData
    });
  } catch (error) {
    next(error);
  }
};

async function pageMeta(fromDate, toDate, siteId) {
  const data = MetaData.aggregate([{
    $match: {
      $and: [{
        createdAt: {
          $gt: fromDate,
          $lt: toDate
        }
      },
      {
        site: siteId
      }
      ]
    }
  },
  {
    $sort: {
      createdAt: -1
    }
  },
  {
    $group: {
      _id: '$pageName',
      engageTime: {
        $avg: '$engagementTime'
      },
      visitTime: {
        $avg: '$visitTime'
      },
      loadingTime: {
        $avg: '$loadingTime'
      },
      total: {
        $sum: 1
      },
    },
  },
  ]).option({ allowDiskUse: true });
  return data;
};

async function heatMapMeta(fromDate, toDate, siteId, pageId) {
  const data = MetaData.aggregate([{
    $match: {
      $and: [{
        createdAt: {
          $gt: fromDate,
          $lt: toDate
        }
      },
      {
        site: siteId
      },
      {
        pageId: new mongoose.Types.ObjectId(pageId)
      }
      ]
    }
  },
  {
    $group: {
      _id: '$pageName',
      engageTime: {
        $avg: '$engagementTime'
      },
      visitTime: {
        $avg: '$visitTime'
      },
      loadingTime: {
        $avg: '$loadingTime'
      },
      total: {
        $sum: 1
      },
    },
  },
  ]).option({ allowDiskUse: true });
  return data;
};

async function clickData(fromDate, toDate, siteId) {
  const data = Event.aggregate([{
    $match: {
      $and: [{
        createdAt: {
          $gt: fromDate,
          $lt: toDate
        }
      },
      {
        site: siteId
      }
      ]
    }
  },
  {
    $group: {
      _id: '$pageName',
      clicks: {
        $sum: '$clickCount'
      },
      scroll: {
        $sum: '$scrolCount'
      },
      total: {
        $sum: 1
      },
    },
  },
  ]).option({ allowDiskUse: true });
  return data;
};

async function heatMapClickData(fromDate, toDate, siteId, pageId) {
  const data = Event.aggregate([{
    $match: {
      $and: [{
        createdAt: {
          $gt: fromDate,
          $lt: toDate
        }
      },
      {
        site: siteId
      },
      {
        pageId: new mongoose.Types.ObjectId(pageId)
      }
      ]
    }
  },
  // {
  //   $sort: {
  //     createdAt: -1
  //   }
  // },
  {
    $group: {
      _id: '$pageName',
      clicks: {
        $sum: '$clickCount'
      },
      scroll: {
        $sum: '$scrolCount'
      },
      total: {
        $sum: 1
      },
    },
  },
  ]).option({ allowDiskUse: true });
  return data;
};


async function scrolCalculate(page, scroll, heightS) {
  let compressed2 = [];
  var heightScreen = 0;
  compressed2 = scroll;
  let sum = 0;
  var everage = 0;
  let max = 0;
  let min;
  heightScreen = heightS;
  if (compressed2.length > 0) {
    min = parseInt(compressed2[0]['value']);
  }
  compressed2.forEach(cordinate => {
    sum += cordinate['value'];
    if (parseInt(cordinate['value']) > max) {
      max = parseInt(cordinate['value']);
    }
    if (parseInt(cordinate['value']) < min) {
      min = parseInt(cordinate['value']);
    }
  });
  if (compressed2.length > 1) {
    for (let index = 0; index < compressed2.length; index++) {
      everage = (compressed2[index].value / sum) * 100;
      let color = mapIntensityToColor(compressed2[index].value, min, max);
      if (compressed2[index]._id !== 0) {
        let to = 0;
        if (index !== 0) {
          to = compressed2[index - 1].to - 1;
        }
        compressed2[index]['from'] = to;
        compressed2[index]['to'] = compressed2[index]['_id'];
        compressed2[index]['colorCode'] = color;
      } else {
        let to = compressed2[index + 1]._id - 1;
        compressed2[index]['from'] = 0;
        compressed2[index]['to'] = to;
        compressed2[index]['colorCode'] = color;
      }
    }
  } else {
    for (let index = 0; index < compressed2.length; index++) {
      everage = ([index].value / sum) * 100;
      let color = mapIntensityToColor(compressed2[index].value, min, max);
      compressed2[index]['from'] = 0;
      compressed2[index]['to'] = compressed2[index]._id;
      compressed2[index]['colorCode'] = color;
    }
  }

  return compressed2;
}


function mapIntensityToColor(intensity, min, max) {
  let cint = map(intensity, min, max, 0, 255);
  /**
   * Based On Rainbow Gradient
   */
  if (cint > 204) {
    return '(' + 255 + ',' + Math.round(map(intensity, min, max, 255, 0)) + ',' + 0 + ')';
  }

  if (cint > 153) {
    max = (203 / 255 * 100) * (max / 100);
    return '(' + Math.round(map(intensity, 0, max, 255, 0)) + ',' + 255 + ',' + 0 + ')';
  }

  if (cint > 102) {
    max = (153 / 255 * 100) * (max / 100);
    return '(' + 0 + ',' + 255 + ',' + Math.round(map(intensity, 0, max, 255, 0)) + ')';
  }

  if (cint > 0) {
    max = (102 / 255 * 100) * (max / 100);
    return '(' + 0 + ',' + Math.round(map(intensity, 0, max, 255, 0)) + ',' + 255 + ')';
  }
  max = (51 / 255 * 100) * (max / 100);
  return '(' + 0 + ',' + 0 + ',' + Math.round(map(intensity, 0, max, 0, 255)) + ')';


  // var cint = mapc(intensity, min, max, 0, 255);
  // var step = (max - min) / 5;
  // if (cint > 204)
  //   return '(' + 255 + ',' + mapc(intensity, max - step, max, 255, 0) + ',' + 0 + ')';
  // if (cint > 153)
  //   return '(' + mapc(intensity, max - 2 * step, max - step, 0, 255) + ',' + 255 + ',' + 0 + ')';
  // if (cint > 102)
  //   return '(' + 0 + ',' + 255 + ',' + mapc(intensity, max - 3 * step, max - 2 * step, 255, 0) + ')';
  // if (cint > 51)
  //   return '(' + 0 + ',' + mapc(intensity, max - 4 * step, max - 3 * step, 0, 255) + ',' + 255 + ')';
  // return '(' + mapc(intensity, min, max - 4 * step, 255, 0) + ',' + 0 + ',' + 255 + ')';
}

function map(value, istart, istop, ostart, ostop) {
  return ostart + (ostop - ostart) * ((value - istart) / (istop - istart));
}

function attentionCalculate(page, events, heightS) {
  let attenCompressed2 = events;
  let sum = 0;
  var everage = 0;
  let max = 0;
  let min = 0;
  if (attenCompressed2.length > 0) {
    min = parseInt(attenCompressed2[0]['value']);
  }
  attenCompressed2.forEach(cordinate => {
    sum += cordinate['value'];
    if (parseInt(cordinate['value']) > max) {
      max = parseInt(cordinate['value']);
    }
    if (parseInt(cordinate['value']) < min) {
      min = parseInt(cordinate['value']);
    }
  });
  if (attenCompressed2.length > 1) {
    for (let index = 0; index < attenCompressed2.length; index++) {
      everage = ([index].value / sum) * 100;
      let color = mapIntensityToColor(attenCompressed2[index].value, min, max);
      if (attenCompressed2[index] !== 0 && attenCompressed2[index - 1] !== undefined) {
        let to = attenCompressed2[index - 1].to - 1;
        attenCompressed2[index]['from'] = to;
        attenCompressed2[index]['to'] = attenCompressed2[index]['_id'];
        attenCompressed2[index]['colorCode'] = color;
      } else {
        let to = attenCompressed2[index + 1]._id - 1;
        attenCompressed2[index]['from'] = 0;
        attenCompressed2[index]['to'] = to;
        attenCompressed2[index]['colorCode'] = color;
      }
    }
  } else {
    for (let index = 0; index < attenCompressed2.length; index++) {
      everage = ([index].value / sum) * 100;
      let color = mapIntensityToColor(attenCompressed2[index].value, min, max);
      attenCompressed2[index]['from'] = 0;
      attenCompressed2[index]['to'] = attenCompressed2[index]._id;
      attenCompressed2[index]['colorCode'] = color;
    }
  }
  return attenCompressed2;
}

async function calSize(bytes) {
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Byte';
  let i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

exports.trackFunnel = async (req, res, next) => {
  try {
    let referrer = req.body.ref;
    let currentPage = req.body.currenPath;
    let country = req.body.country;
    let trafSou = null;
    if (referrer == "") {
      trafSou = "No Referrer";
      res.send(trafSou);
      let r = await saveFunnel(req, res, country, trafSou, currentPage, referrer);
      res.status(200).send({
        data: ''
      })
    } else {
      let url = 'pandaflow.com';
      inbound.referrer.parse(url, referrer, function (err, description) {
        let trafSou = null;
        if (description['referrer']['type']) {
          trafSou = description['referrer']['type'];
          let r = saveFunnel(req, res, country, trafSou, currentPage, referrer);
          res.status(200).send({
            data: ''
          })
        }
      });
    }


  } catch (error) {
    next(error);
  }
}

async function saveFunnel(req, res, country, trafSou, currentPage, referrer) {

  let getRef = '';
  let getPage = '';
  // getPage = currentPage.split("/").slice(-1);
  getRef = referrer.split("/").slice(-1);
  const session = await Session.findOne({
    _id: req.body.userSessionId,
    siteId: req.body.siteId
  }).populate('recordings', 'pageName').exec();
  const recordings = session.recordings;
  let pageCount = 0;
  let returningVisitor = false;


  const funnels = await Funnel.find({
    websiteId: req.body.siteId
  }).exec();

  for (let index = 0; index < funnels.length; index++) {
    const funnel = await Funnel.findOne({
      _id: funnels[index]._id
    }).exec();
    if (recordings.length === 0) {

      if (currentPage.replace("/", "") === funnels[index].pages[0].pageName) {

        funnel.pages[0].pageVisit += 1;
        funnel.pages[0].pageDetail.push({
          osName: req.body.osName,
          countryName: country,
          browserName: req.body.browserName,
          trafficSource: trafSou,
          visitorType: returningVisitor,
          screenResolution: req.body.resolution,
          devices: req.body.deviseV,
          refferrer: getRef[0],
          userSessionId: req.body.userSessionId,
          pageCount: 1
        });
        funnel.save();
      }
    } else if (recordings.length < funnel.pages.length) {
      const pages = funnel.pages;
      let checkFunnelNavigation = true;
      for (let i = 0; i <= recordings.length; i++) {

        if (i < recordings.length) {
          let pageName = recordings[i].pageName;
          if (pageName[0] === "/") {
            pageName = pageName.substr(1);
          }
          if (pages[i].pageName === pageName) {
            checkFunnelNavigation = true;
          } else {
            checkFunnelNavigation = false;
            break;
          }
        } else if (i === recordings.length) {
          if (pages[i].pageName === currentPage) {
            checkFunnelNavigation = true;
          } else {
            checkFunnelNavigation = false;
            break;
          }
        }
      }
      if (checkFunnelNavigation === true) {
        let pageIndex = pages.findIndex(x => x.pageName === currentPage);
        if (pageIndex > -1) {
          funnel.pages[pageIndex].pageVisit += 1;
          funnel.pages[pageIndex].pageDetail.push({
            osName: req.body.osName,
            countryName: country,
            browserName: req.body.browserName,
            trafficSource: trafSou,
            visitorType: returningVisitor,
            screenResolution: req.body.resolution,
            devices: req.body.deviseV,
            refferrer: getRef[0],
            userSessionId: req.body.userSessionId,
            pageCount: 1
          });
          //funnel.save();
          funnel.save(function (err, success) {
            Funnel.find({
              "pages.pageDetail.userSessionId": req.body.userSessionId
            }, function (err, funnelData) {
              for (let i = 0; i < funnelData.length; i++) {
                for (let j = 0; j < funnelData[i].pages.length; j++) {
                  for (let k = 0; k < funnelData[i].pages[j].pageDetail.length; k++) {
                    if (funnelData[i].pages[j].pageDetail[k].userSessionId === req.body.userSessionId) {
                      funnelData[i].pages[j].pageDetail[k].pageCount = recordings.length + 1
                      funnelData[i].save(function (err, success) {

                      })
                    }
                  }
                }
              }
            })
          })
        }
      }
    }
  }
}

exports.newPageData = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const from = new Date(req.params.from);
    const to = new Date(req.params.to);
    const siteId = req.params.siteId;
    const data = await Pagestats.aggregate([
      {
        $match:
        {

          $and: [
            {
              createdAt:
              {
                $gte: from, $lte: to
              }
            },
            {
              websiteId: new mongoose.Types.ObjectId(siteId)
            }
          ]
        }
      },
      {
        $sort: {
          pageId: -1
        }
      },
      {
        $group: {
          _id: '$pageId',
          pageName: {
            $first: '$pageName'
          },
          pageSize: {
            $first: '$pageSize'
          },
          formInteract: {
            $first: '$formInteract'
          },
          formSubmit: {
            $first: '$formSubmit'
          },
          clickError: {
            $first: '$clickError'
          },
          clickRage: {
            $first: '$clickRage'
          },
          views: {
            $sum: '$views'
          },
          clicks: {
            $sum: '$clicks'
          },
          scrollPercentage: {
            $sum: '$scroll'
          },
          engageTime: {
            $avg: '$engageTime'
          },
          visitTime: {
            $avg: '$visitTime'
          },
          loadingTime: {
            $avg: '$loadingTime'
          },
          total: {
            $sum: 1
          },
        },
      },
    ], (err, result) => {
      if (err) {
        console.log(err);
      }
    });

    const sortedData = _.sortBy(data, 'views');
    sortedData.reverse();
    res.status(200).send({
      code: 200,
      data: sortedData
    });
  } catch (error) {

  }
}

exports.pageStats = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    let queryString = req.query;
    const from = new Date(req.params.from);
    const to = new Date(req.params.to);
    const siteId = req.params.siteId;
    const limit = 10;//parseInt(req.params.limit);
    const offset = parseInt(req.params.offset * 10);
    queryString.createdAt = {
      $gte: from,
      $lte: to
    };
    queryString.siteId = new mongoose.Types.ObjectId(siteId);
    if (Object.size(queryString)) {
      if (queryString.clickRage !== undefined) {
        queryString.clickRage = parseInt(queryString.clickRage);
      }

      if (queryString.clickError !== undefined) {
        queryString.clickError = parseInt(queryString.clickError);
      }

      if (queryString.formInteract !== undefined) {
        queryString.formInteract = parseInt(queryString.formInteract);
      }

      if (queryString.formSubmit !== undefined) {
        queryString.formSubmit = parseInt(queryString.formSubmit);
      }

      // if (queryString.visitMin !== undefined) {
      //   queryString.duration = { $gte: parseInt(req.query.visitMin), $lte: parseInt(req.query.visitMax) };
      // }
      // if ((queryString.visitMin !== undefined) && (queryString.visitMax !== undefined)) {
      //   queryString.duration = { $gte: parseInt(queryString.visitMin), $lte: parseInt(queryString.visitMax) };
      // } else if (queryString.visitMin !== undefined) {
      //   queryString.duration = { $gte: parseInt(queryString.visitMin), $lte: 65000 };
      // } else if (queryString.visitMax !== undefined) {
      //   queryString.duration = { $gte: 0, $lte: parseInt(queryString.visitMax) };
      // }
      if (queryString.pageName !== undefined) {
        if (typeof queryString.pageName === 'string') {
          queryString.pageName = req.query.pageName;
        }
        if (typeof queryString.pageName === 'object') {
          queryString.pageName = { $in: req.query.pageName }
        }
      }
      // delete queryString.visitMin;
      // delete queryString.visitMax;

    }
    if (Object.size(queryString) > 2) {
      let views = 0;
      let pageMatch = [];
      // let sessions = await Session.find({ $and: [queryString] }).select('_id').exec();
      const data = await MetaData.aggregate([
        {
          $match: {
            $and: [
              queryString
            ]
          }
        },
        {
          $sort: {
            pageId: -1
          }
        },
        {
          $group: {
            _id: "$pageName",
            pageName: {
              $first: '$pageName'
            },
            pageSize: {
              $first: '$pageSize'
            },
            pageName: {
              $first: '$pageName'
            },
            pageId: {
              $first: '$pageId'
            },
            formInteract: {
              $sum: '$formInteract'
            },
            clickRage: {
              $sum: '$clickRage'
            },
            formSubmit: {
              $sum: '$formSubmit'
            },
            clickError: {
              $sum: '$clickError'
            },
            clicks: {
              $sum: '$clicks'
            },
            scrollPercentage: {
              $avg: '$scroll'
            },
            engageTime: {
              $avg: '$engagementTime'
            },
            visitTime: {
              $avg: '$visitTime'
            },
            loadingTime: {
              $avg: '$loadingTime'
            },
            views: {
              $sum: 1
            },
          },
        }, {
          $sort: {
            views: -1
          }
        }, {
          '$facet': {
            'metaData': [
              {
                '$count': 'total'
              }
            ],
            'data': [
              {
                '$skip': offset
              }, {
                '$limit': limit
              }
            ]
          }
        }
      ]).allowDiskUse(true);
      let sortData = _.sortBy(data[0].data, 'views');
      sortData.reverse();
      res.status(200).send({
        code: 200,
        data: sortData,
        total: data[0].metaData[0] ? data[0].metaData[0].total : 0
      });
    }
    else {
      const data = await MetaData.aggregate([
        {
          $match:
          {

            $and: [
              {
                createdAt:
                {
                  $gte: from, $lte: to
                }
              },
              {
                siteId: new mongoose.Types.ObjectId(siteId)
              }
            ]
          }
        },
        {
          $sort: {
            views: -1
          }
        },
        {
          $group: {
            _id: '$pageName',
            pageName: {
              $first: '$pageName'
            },
            pageSize: {
              $first: '$pageSize'
            },
            pageName: {
              $first: '$pageName'
            },
            pageId: {
              $first: '$pageId'
            },
            formInteract: {
              $sum: '$formInteract'
            },
            clickRage: {
              $sum: '$clickRage'
            },
            formSubmit: {
              $sum: '$formSubmit'
            },
            clickError: {
              $sum: '$clickError'
            },
            clicks: {
              $sum: '$clicks'
            },
            scrollPercentage: {
              $avg: '$scroll'
            },
            engageTime: {
              $avg: '$engagementTime'
            },
            visitTime: {
              $avg: '$visitTime'
            },
            loadingTime: {
              $avg: '$loadingTime'
            },
            views: {
              $sum: 1
            },
          },
        }, {
          $sort: {
            views: -1
          }
        }, {
          '$facet': {
            'metaData': [
              {
                '$count': 'total'
              }
            ],
            'data': [
              {
                '$skip': offset
              }, {
                '$limit': limit
              }
            ]
          }
        }
      ]).allowDiskUse(true);

      const sortedData = _.sortBy(data[0].data, 'views');
      sortedData.reverse();
      res.status(200).send({
        code: 200,
        data: sortedData,
        total: data[0].metaData[0] ? data[0].metaData[0].total : 0
      });
    }
  }
  catch (error) {
    next(error);
  }
};

async function pageMetaFilter(sessionIdsArray) {
  const data = MetaData.aggregate([{
    $match: {
      sessionId:
      {
        $in:
          sessionIdsArray
      }
    }
  },
  {
    $sort: {
      pageId: -1
    }
  },
  {
    $group: {
      _id: '$pageId',
      pageName: { $first: '$pageName' },
      siteID: { $first: '$siteId' },
      pageSize: { $first: '$pageSize' },
      engageTime: {
        $avg: '$engagementTime'
      },
      visitTime: {
        $avg: '$visitTime'
      },
      loadingTime: {
        $avg: '$loadingTime'
      },
      total: {
        $sum: 1
      },
    },
  },
  ], (err, result) => {
    if (err) {
      console.log(err);
    }
  });
  return data;
};

async function clickDataFilter(sessionIdsArray) {
  const data = Event.aggregate([{
    $match: {
      sessionId:
      {
        $in:
          sessionIdsArray
      }
    }
  },
  {
    $group: {
      _id: '$pageId',
      pageName: { $first: '$pageName' },
      siteId: { $first: '$siteId' },
      clicks: {
        $sum: '$clickCount'
      },
      scroll: {
        $sum: '$scrolCount'
      },
      total: {
        $sum: 1
      },
    },
  },
  ]).option({ allowDiskUse: true });
  return data;
};

async function pageDataCron() {
  try {
    let todayStart = new Date();
    let dd = todayStart.getDate();
    let mm = todayStart.getMonth() + 1; //January is 0!

    let yyyy = todayStart.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    let todayDate = yyyy + '/' + mm + '/' + dd;

    let minutesBefore = todayStart.getMinutes() - 7;
    let hoursBefore = todayStart.getHours();
    if (minutesBefore < 0) {
      hoursBefore = hoursBefore - 1;
      minutesBefore = 60 + minutesBefore;
    }

    var datetimeStart = todayStart.getFullYear() + "/" + (todayStart.getMonth() + 1) + "/" + todayStart.getDate() + " @ " + hoursBefore + ":" + (minutesBefore) + ":" + todayStart.getSeconds();
    var datetimeEnd = todayStart.getFullYear() + "/" + (todayStart.getMonth() + 1) + "/" + todayStart.getDate() + " @ " + todayStart.getHours() + ":" + todayStart.getMinutes() + ":" + todayStart.getSeconds();

    // let start = "2019-01-07";
    // let end = "2019-01-11";
    // let from = new Date(start);
    // todayStart = from;
    // let to = new Date(end);
    // todayEnd = to;

    todayStart = new Date(datetimeStart);
    todayEnd = new Date(datetimeEnd);

    let cliclsData = await clickDataCron(todayStart, todayEnd);

    if (!cliclsData.length) {
      return;
    }

    let siteIdsArray = cliclsData.map(a => a.siteId);
    let pagesIdsArray = cliclsData.map(a => a._id);
    const timeData = await pageMetaCron(todayStart, todayEnd, pagesIdsArray);

    const pages = [];

    for (let i = 0; i < timeData.length; i++) {
      let pageData = await Page.findOne({ name: timeData[i].pageName }).select('formInteract formSubmit clickError clickRage').exec();
      let views = await Recording.countDocuments({
        $and: [{
          siteId: timeData[i].siteID
        }, {
          pageName: timeData[i].pageName
        }, {
          createdAt: {
            $gte: todayStart,
            $lte: todayEnd
          }
        }]
      }).exec();
      let pageIndex = pages.findIndex(x => x.pageName === timeData[i].pageName);
      let clicks = 0;
      let scroll = 0;
      if (pageIndex === -1) {
        let clickIndex = cliclsData.findIndex(x => x.pageName === timeData[i].pageName);
        if (clickIndex !== -1) {
          clicks = cliclsData[clickIndex].clicks;
          scroll = Math.round((cliclsData[clickIndex].scroll / cliclsData[clickIndex].total) * 100);
        }

        pages.push({
          pageName: timeData[i].pageName,
          engageTime: timeData[i].engageTime,
          visitTime: timeData[i].visitTime,
          loadingTime: timeData[i].loadingTime,
          pageSize: timeData[i].pageSize,
          clicks: clicks,
          scrollPercentage: scroll,
          views: views,
          pageId: timeData[i]._id,
          websiteId: timeData[i].siteID,
          date: todayDate,
          formInteract: pageData.formInteract,
          formSubmit: pageData.formSubmit,
          clickError: pageData.clickError,
          clickRage: pageData.clickRage
        });
      }
    }
    for (let k = 0; k < pages.length; k++) {
      const todayPageStats = await Pagestats.findOne(
        {
          date: todayDate,
          pageId: pages[k].pageId
        }).exec();
      if (todayPageStats) {
        await Pagestats.findOneAndUpdate(
          {
            pageId: pages[k].pageId,
            date: todayDate,
          },
          {
            $set:
            {
              clicks: parseInt(todayPageStats.clicks) + parseInt(pages[k].clicks),
              engageTime: parseFloat(todayPageStats.engageTime) + parseFloat(pages[k].engageTime),
              visitTime: parseFloat(todayPageStats.visitTime) + parseFloat(pages[k].visitTime),
              loadingTime: parseFloat(todayPageStats.loadingTime) + parseFloat(pages[k].loadingTime),
              scrollPercentage: parseFloat(pages[k].scroll),
              views: parseFloat(pages[k].views),
              pageSize: pages[k].pageSize,
              clickRage: pages[k].clickRage,
              formInteract: pages[k].formInteract,
              formSubmit: pages[k].formSubmit,
              clickError: pages[k].clickError
            }
          }, {
          new: true
        }).exec();
      }
      else {
        Pagestats.insertMany(pages[k]);
      }
    }
  } catch (error) {
  }
};

async function clickDataCron(todayStart, todayEnd) {
  const data = await Event.aggregate([
    {
      $match:
      {
        createdAt:
        {
          $gte: todayStart, $lte: todayEnd
        },
        calculatedAlready: false,
      }
    },
    {
      $sort: {
        pageId: -1
      }
    },
    {
      $group: {
        _id: '$pageId',
        pageName: { $first: '$pageName' },
        siteId: { $first: '$siteId' },
        clicks: {
          $sum: '$clickCount'
        },
        scroll: {
          $sum: '$scrolCount'
        },
        total: {
          $sum: 1
        },
      },
    },
  ]).option({ allowDiskUse: true });
  await Event.updateMany(
    {
      createdAt:
      {
        $gte: todayStart, $lte: todayEnd
      },
      calculatedAlready: false,
    },
    {
      $set:
      {
        calculatedAlready: true
      },
    }).exec();
  return data;
};

async function pageMetaCron(todayStart, todayEnd, siteIdsArray) {
  const data = await MetaData.aggregate([
    {
      $match:
      {
        createdAt:
        {
          $gte: todayStart, $lte: todayEnd
        },
        calculatedAlready: false,
        pageId:
        {
          $in: siteIdsArray
        }
      }
    },
    {
      $sort: {
        pageId: -1
      }
    },
    {
      $group: {
        _id: '$pageId',
        pageName: { $first: '$pageName' },
        siteID: { $first: '$siteId' },
        pageSize: { $first: '$pageSize' },
        engageTime: {
          $avg: '$engagementTime'
        },
        visitTime: {
          $avg: '$visitTime'
        },
        loadingTime: {
          $avg: '$loadingTime'
        },
        total: {
          $sum: 1
        },
      },
    },
  ], (err, result) => {
    if (err) {
      console.log(err);
    }
  });

  await MetaData.updateMany(
    {
      createdAt:
      {
        $gte: todayStart, $lte: todayEnd
      },
      calculatedAlready: false,
    },
    {
      $set:
      {
        calculatedAlready: true
      },
    }).exec();


  return data;
};

Object.size = function (obj) {
  var size = 0, key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};

exports.getFormDropFields = async (req, res, next) => {
  try {
    var owner = parseInt(req.headers.owner);
    var currentUserSites = await checkWebsites(req.params.siteId, req.user, owner);
    if (!currentUserSites) {
      return res.status(460).json({ status: false, message: 'You`re not authorized to access this' });
    }
    const fromDate = new Date(req.params.from);
    const toDate = new Date(req.params.to);
    const site = req.params.siteId;
    let queryData = req.body;
    let sort = {};
    sort.createdAt = -1;
    let reqQueryLenth = Object.keys(req.body).length;
    queryData.site = site;
    queryData.createdAt = {
      $gt: fromDate,
      $lt: toDate
    };
    if (reqQueryLenth > 0) {
      if (queryData.pageName !== undefined) {
        if (typeof req.body.pageName === 'string') {
          queryData.pageName = req.body.pageName;
        } else {
          queryData.pageName = {
            $in: req.body.pageName
          };
        }
      }
      if (queryData.formInteract !== undefined) {
        req.body.formInteract = parseInt(req.body.formInteract);
      }
      if (queryData.clickRage !== undefined) {
        req.body.clickRage = { $gte: parseInt(req.body.clickRage) };
      }
      if (queryData.clickError !== undefined) {
        req.body.clickError = parseInt(req.body.clickError);
      }
    }
    // delete queryData.pageName;
    const recordings = await Recording.find({
      $and: [queryData]
    }).select('_id').exec();
    let data = [];
    let resData = [];
    let totalClicks = _.map(recordings, function (r) {
      data.push(r._id.toString());
    });
    if (data.length > 0) {
      let matchHeader = {};
      matchHeader.siteId = req.params.siteId;
      if (queryData.pageName) {
        matchHeader.pageName = queryData.pageName;
      }
      let arrayOfPromises = [
        getHeaderByFieldTracking(matchHeader),
        getDropOutFields(data)
      ];
      let responses = await Promise.all(arrayOfPromises);
      let header = responses[0]
      let dropOutFields = responses[1]
      if (header.length > 0) {
        header.forEach((element, index) => {
          let obj = {}
          let pageIndex = -1
          dropOutFields.findIndex((x, i) => {
            if (x._id.indexOf(element._id) > -1) {
              pageIndex = i
            }
          });
          let header = ['recordedDate', 'pageName', 'referer', 'device']
          obj.pageName = element._id;
          obj.pageHeader = header.concat(element.pageHeader);
          obj.data = dropOutFields[pageIndex] ? dropOutFields[pageIndex] : [];
          resData.push(obj)
        });
      }
    }
    res.status(200).send({
      code: 200,
      data: resData
    });
  } catch (error) {
    next(error);
  }
};

async function getDropOutFields(query) {
  try {
    return new Promise((resolve, reject) => {
      FieldReport.aggregate([
        {
          '$match': {
            'recordingId': {
              $in: query
            }
          }
        }, {
          '$group': {
            '_id': '$pageName',
            'fields': {
              '$addToSet': {
                'recordedDate': '$recordedDate',
                'recordingId': '$recordingId',
                'referer': '$referer',
                'device': '$device',
                'pageName': '$pageName',
                'fields': '$fields'
              }
            }
          }
        }
      ], function (err, docs) {
        err ? reject(err) : resolve(docs);
      });
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
}

async function getHeaderByFieldTracking(query) {
  try {
    return new Promise((resolve, reject) => {
      const url = MONGO_DB_URI;
      const dbName = query.siteId;
      const client = new MongoClient(url, { useNewUrlParser: true });
      client.connect(function (err, client) {
        assert.equal(null, err);
        const db = client.db(dbName);
        const collection = db.collection('fieldtrackings');
        collection.aggregate([
          {
            '$match': query
          }, {
            '$group': {
              '_id': '$pageName',
              'pageHeader': {
                '$addToSet': '$fieldName'
              }
            }
          }
        ]).toArray(function (err, docs) {
          err ? reject(err) : resolve(docs);
        });
      });
      client.close();
    });
  } catch (error) {
    next(error);
  }
}

async function getEventBySite(siteName, pageId, device, from, to, eventType) {
  try {
    return new Promise((resolve, reject) => {
      const url = MONGO_DB_URI;
      const dbName = siteName;
      const client = new MongoClient(url, { useNewUrlParser: true });
      client.connect(function (err, client) {
        assert.equal(null, err);
        const db = client.db(dbName);
        const collection = db.collection('events');
        collection.find({
          $and: [
            {
              pageId: new mongoose.Types.ObjectId(pageId)
            },
            {
              type: eventType
            },
            {
              createdAt: {
                $gt: from,
                $lt: to
              }
            },
            {
              device: device
            }
          ]
        }).project({ x: 1, y: 1, offsetLeft: 1, offsetTop: 1, path: 1, value: 1, width: 1, height: 1 }).toArray(function (err, docs) {
          err ? reject(err) : resolve(docs);
        });
      });
      client.close();
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
}

async function calculateScroll(siteName, pageId, device, from, to, eventType) {
  try {

    return new Promise((resolve, reject) => {
      const url = MONGO_DB_URI;
      const dbName = siteName;
      const client = new MongoClient(url, { useNewUrlParser: true });
      client.connect(function (err, client) {
        assert.equal(null, err);
        const db = client.db(dbName);
        const collection = db.collection('events');
        let deviceWidth = [];
        if (device === 'Desktop') {
          deviceWidth = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 3100, 3200, 3300, 3400, 3500, 3600, 3700, 3800, 3900, 4000, 4100, 4200, 4300, 4400, 4500, 4600, 4700, 4800, 4900, 5000, 5100, 5200, 5300, 5400, 5500, 5600, 5700, 5800, 5900, 6000, 6100, 6200, 6300, 6400, 6500, 6600, 6700, 6800, 6900, 7000, 7100, 7200, 7300, 7400, 7500, 7600, 7700, 7800, 7900, 8000, 8100, 8200, 8300, 8400, 8500, 8600, 8700, 8800, 8900, 9000, 9100, 9200, 9300, 9400, 9500, 9600, 9700, 9800, 9900, 10000]
        } else {
          deviceWidth = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 3100, 3200, 3300, 3400, 3500, 3600, 3700, 3800, 3900, 4000, 4100, 4200, 4300, 4400, 4500, 4600, 4700, 4800, 4900, 5000, 5100, 5200, 5300, 5400, 5500, 5600, 5700, 5800, 5900, 6000, 6100, 6200, 6300, 6400, 6500, 6600, 6700, 6800, 6900, 7000, 7100, 7200, 7300, 7400, 7500, 7600, 7700, 7800, 7900, 8000, 8100, 8200, 8300, 8400, 8500, 8600, 8700, 8800, 8900, 9000, 9100, 9200, 9300, 9400, 9500, 9600, 9700, 9800, 9900, 10000, 10100, 10200, 10300, 10400, 10500, 10600, 10700, 10800, 10900, 11000, 11100, 11200, 11300, 11400, 11500, 11600, 11700, 11800, 11900, 12000, 12100, 12200, 12300, 12400, 12500, 12600, 12700, 12800, 12900, 13000, 13100, 13200, 13300, 13400, 13500, 13600, 13700, 13800, 13900, 14000, 14100, 14200, 14300, 14400, 14500, 14600, 14700, 14800, 14900, 15000];
        }
        collection.aggregate(
          [
            {
              $match: {
                $and: [
                  {
                    pageId: new mongoose.Types.ObjectId(pageId)
                  },
                  {
                    type: 'scroll'
                  },
                  {
                    createdAt: {
                      $gt: from,
                      $lt: to
                    }
                  },
                  {
                    device: device
                  }
                ]
              }
            },
            {
              $bucket: {
                groupBy: "$scrollY",
                boundaries: deviceWidth,
                default: "Other",
                output: {
                  "value": { $sum: 1 },
                }
              }
            }
          ],
          function (err, cursor) {
            assert.equal(err, null);
            cursor.toArray(function (err, documents) {
              err ? reject(err) : resolve(documents);
            });
          }
        );
      });
      client.close();
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
}

async function scrolCalculate(page, scroll, heightS) {
  let compressed2 = [];
  var heightScreen = 0;
  compressed2 = scroll;
  let sum = 0;
  var everage = 0;
  let max = 0;
  let min;
  heightScreen = heightS;
  if (compressed2.length > 0) {
    min = parseInt(compressed2[0]['value']);
  }
  compressed2.forEach(cordinate => {
    sum += cordinate['value'];
    if (parseInt(cordinate['value']) > max) {
      max = parseInt(cordinate['value']);
    }
    if (parseInt(cordinate['value']) < min) {
      min = parseInt(cordinate['value']);
    }
  });
  if (compressed2.length > 1) {
    for (let index = 0; index < compressed2.length; index++) {
      everage = (compressed2[index].value / sum) * 100;
      let color = mapIntensityToColor(compressed2[index].value, min, max);
      if (compressed2[index]._id !== 0) {
        let to = 0;
        if (index !== 0) {
          to = compressed2[index - 1].to - 1;
        }
        compressed2[index]['from'] = to;
        compressed2[index]['to'] = compressed2[index]['_id'];
        compressed2[index]['colorCode'] = color;
      } else {
        let to = compressed2[index + 1]._id - 1;
        compressed2[index]['from'] = 0;
        compressed2[index]['to'] = to;
        compressed2[index]['colorCode'] = color;
      }
    }
  } else {
    for (let index = 0; index < compressed2.length; index++) {
      everage = ([index].value / sum) * 100;
      let color = mapIntensityToColor(compressed2[index].value, min, max);
      compressed2[index]['from'] = 0;
      compressed2[index]['to'] = compressed2[index]._id;
      compressed2[index]['colorCode'] = color;
    }
  }

  return compressed2;
}

async function calculateAttention(siteName, pageId, device, from, to, eventType) {
  try {
    let deviceWidth = [];

    if (device === 'Desktop') {
      deviceWidth = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 3100, 3200, 3300, 3400, 3500, 3600, 3700, 3800, 3900, 4000, 4100, 4200, 4300, 4400, 4500, 4600, 4700, 4800, 4900, 5000, 5100, 5200, 5300, 5400, 5500, 5600, 5700, 5800, 5900, 6000, 6100, 6200, 6300, 6400, 6500, 6600, 6700, 6800, 6900, 7000, 7100, 7200, 7300, 7400, 7500, 7600, 7700, 7800, 7900, 8000, 8100, 8200, 8300, 8400, 8500, 8600, 8700, 8800, 8900, 9000, 9100, 9200, 9300, 9400, 9500, 9600, 9700, 9800, 9900, 10000]
    } else {
      deviceWidth = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 3100, 3200, 3300, 3400, 3500, 3600, 3700, 3800, 3900, 4000, 4100, 4200, 4300, 4400, 4500, 4600, 4700, 4800, 4900, 5000, 5100, 5200, 5300, 5400, 5500, 5600, 5700, 5800, 5900, 6000, 6100, 6200, 6300, 6400, 6500, 6600, 6700, 6800, 6900, 7000, 7100, 7200, 7300, 7400, 7500, 7600, 7700, 7800, 7900, 8000, 8100, 8200, 8300, 8400, 8500, 8600, 8700, 8800, 8900, 9000, 9100, 9200, 9300, 9400, 9500, 9600, 9700, 9800, 9900, 10000, 10100, 10200, 10300, 10400, 10500, 10600, 10700, 10800, 10900, 11000, 11100, 11200, 11300, 11400, 11500, 11600, 11700, 11800, 11900, 12000, 12100, 12200, 12300, 12400, 12500, 12600, 12700, 12800, 12900, 13000, 13100, 13200, 13300, 13400, 13500, 13600, 13700, 13800, 13900, 14000, 14100, 14200, 14300, 14400, 14500, 14600, 14700, 14800, 14900, 15000];
    }
    return new Promise((resolve, reject) => {
      const url = MONGO_DB_URI;
      const dbName = siteName;
      const client = new MongoClient(url, { useNewUrlParser: true });
      client.connect(function (err, client) {
        assert.equal(null, err);
        const db = client.db(dbName);
        const collection = db.collection('events');
        let deviceWidth = [];
        if (device === 'Desktop') {
          deviceWidth = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 3100, 3200, 3300, 3400, 3500, 3600, 3700, 3800, 3900, 4000, 4100, 4200, 4300, 4400, 4500, 4600, 4700, 4800, 4900, 5000, 5100, 5200, 5300, 5400, 5500, 5600, 5700, 5800, 5900, 6000, 6100, 6200, 6300, 6400, 6500, 6600, 6700, 6800, 6900, 7000, 7100, 7200, 7300, 7400, 7500, 7600, 7700, 7800, 7900, 8000, 8100, 8200, 8300, 8400, 8500, 8600, 8700, 8800, 8900, 9000, 9100, 9200, 9300, 9400, 9500, 9600, 9700, 9800, 9900, 10000]
        } else {
          deviceWidth = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100, 2200, 2300, 2400, 2500, 2600, 2700, 2800, 2900, 3000, 3100, 3200, 3300, 3400, 3500, 3600, 3700, 3800, 3900, 4000, 4100, 4200, 4300, 4400, 4500, 4600, 4700, 4800, 4900, 5000, 5100, 5200, 5300, 5400, 5500, 5600, 5700, 5800, 5900, 6000, 6100, 6200, 6300, 6400, 6500, 6600, 6700, 6800, 6900, 7000, 7100, 7200, 7300, 7400, 7500, 7600, 7700, 7800, 7900, 8000, 8100, 8200, 8300, 8400, 8500, 8600, 8700, 8800, 8900, 9000, 9100, 9200, 9300, 9400, 9500, 9600, 9700, 9800, 9900, 10000, 10100, 10200, 10300, 10400, 10500, 10600, 10700, 10800, 10900, 11000, 11100, 11200, 11300, 11400, 11500, 11600, 11700, 11800, 11900, 12000, 12100, 12200, 12300, 12400, 12500, 12600, 12700, 12800, 12900, 13000, 13100, 13200, 13300, 13400, 13500, 13600, 13700, 13800, 13900, 14000, 14100, 14200, 14300, 14400, 14500, 14600, 14700, 14800, 14900, 15000];
        }
        collection.aggregate(
          [
            {
              $match: {
                $and: [
                  {
                    pageId: new mongoose.Types.ObjectId(pageId)
                  },
                  {
                    type: 'mousemove'
                  },
                  {
                    createdAt: {
                      $gt: from,
                      $lt: to
                    }
                  },
                  {
                    device: device
                  }
                ]
              }
            },
            {
              $bucket: {
                groupBy: "$y",
                boundaries: deviceWidth,
                default: "Other",
                output: {
                  "value": { $sum: 1 },
                }
              }
            }
          ],
          function (err, cursor) {
            assert.equal(err, null);
            cursor.toArray(function (err, documents) {
              err ? reject(err) : resolve(documents);
            });
          }
        );
        client.close();
      });
    });
  } catch (error) {
    console.log(error);
    next(error);
  }
}


async function saveAllData(dbName, statData, fieldData) {
  try {
    console.log('IN SAVE FIELD AND STATS FILE');
    const url = MONGO_DB_URI;
    const client = new MongoClient(url, { useNewUrlParser: true });
    client.connect(function (err, client) {
      console.log('IN formstat db connection');
      assert.equal(null, err);
      const db = client.db(dbName);
      db.collection('formstats').insertOne(statData, function (err, r) {
        if (err) {
          console.log('Database error  >>> ' + err);
        }
      });
      console.log('FIELD DATA LENGTH' + fieldData.length);
      db.collection('fieldtrackings').insertMany(fieldData, function (err, r) {
        if (err) {
          console.log('Database error  fields table >>> ' + err);
        }
      });
      client.close();
    });
  } catch (error) {

  }
}

async function saveTracking(fields) {
  try {
    const result = await FieldTracking.insertMany(fields);
    return result;
  } catch (error) {
    next(error);
  }
}

// cron.schedule('*/1 * * * *', () => {
//   pageDataCron();
// });

// pageDataCron();
