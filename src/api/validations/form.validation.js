const Joi = require('joi');

module.exports = {

  // POST /v1/form/
  createForm: {
    body: {
      siteId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
      formName: Joi.string().max(128),
      pageName: Joi.string().max(128),
    },
  },

  // DELETE /v1/form/
  deleteForm: {
    body: {
      formId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required()
    },

  }
};
