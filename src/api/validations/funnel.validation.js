const Joi = require('joi');
const Funnel = require('../models/funnel.model');

module.exports = {



    // POST /v1/funnel/
    createFunnel: {
        body: {
            websiteId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
            funelName: Joi.string().max(128),
        },
    },

    // DELETE /v1/funnel/
    deleteFunnel: {
        body: {
            funnelId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required()
        },
    },

    // PUT /v1/funnel/
    editFunnel: {
        body: {
            funnelId: Joi.string().regex(/^[a-fA-F0-9]{24}$/).required(),
            funelName: Joi.string().max(128),
        },
    },

};