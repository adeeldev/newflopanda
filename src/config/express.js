const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const compress = require('compression');
const methodOverride = require('method-override');
const cors = require('cors');
const helmet = require('helmet');
const passport = require('passport');
const routes = require('../api/routes/v1');
const { logs } = require('./vars');
const strategies = require('./passport');
const error = require('../api/middlewares/error');
const frameguard = require('frameguard');
const path = require('path');
// const rateLimit = require("express-rate-limit");
const bearerToken = require('express-bearer-token');
var jwt = require('jsonwebtoken');
var CryptoJS = require("crypto-js");
const crypto = require('crypto');
var byPassedRoutes = ['/v1/cron/conversion/rate'];
const mung = require('express-mung');

/**
* Express instance
* @public
*/
const app = express();
let root = path.join(__dirname, '../../client/build');

app.use(morgan(logs));
app.use(helmet.xssFilter());
app.use(helmet.frameguard());
const sixtyDaysInSeconds = 31536000;
app.use(helmet.hsts({
  maxAge: sixtyDaysInSeconds
}));
app.disable('x-powered-by');
// parse body params and attache them to req.body
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
// app.use(bearerToken());

// lets you use HTTP verbs such as PUT or DELETE
// in places where the client doesn't support it
app.use(methodOverride());

// const apiRequestLimiterAll = rateLimit({
//   windowMs: 15 * 60 * 1000, // 15 minutes
//   max: 90000
// });

// app.use("/v1/");
var corsOptions = {
  origin: '*',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.use(cors(corsOptions));

// enable authentication
app.use(passport.initialize());
passport.use('jwt', strategies.jwt);

app.use(async function (req, res, next) {
  req.user = 0;
  if (req.headers['x-access-token']) {
    await jwt.verify(req.headers['x-access-token'], 'bA2xcjpf8y5aSUFsNB2qN5yymUBSs6es3qHoFpGkec75RCeBb8cpKauGefw5qy4', (err, authorizedData) => {
      if (err) {
        req.user = 0;
      }
      else {
        req.user = authorizedData.sub;
      }
    })
  }
  else if (req.method.toLocaleLowerCase() !== 'options') {
    req.user = 0;
  }
  next();
});

app.use(bearerToken());
app.use(function (req, res, next) {
  if (req.originalUrl.indexOf("/v1/") > -1) {
    if (byPassedRoutes.indexOf(req.originalUrl) > -1 || req.originalUrl.indexOf("/v1/recording/download") > -1 || req.originalUrl.indexOf("/v1/website/baseEncode") > -1 || req.originalUrl.indexOf("/v1/session/update/payment/tag") > -1) {
      next();
    }
    else {
      if (req.headers['x-auth-token']) {
        var decryption_string = req.headers['x-auth-token'];
        if (req.token) {
          var bytes = CryptoJS.AES.decrypt(req.token, decryption_string);
          var decryptedData = bytes.toString(CryptoJS.enc.Utf8);
          if (decryptedData !== "recordingandsessionstartedforfloPanda") {
            return res.status(405).json({ status: false, message: 'Authenticatd requests only, please!' });
          }
          else {
            if (req.method !== 'GET' && req.body && Object.keys(req.body).length !== 0) {
              if (req.body.data) {
                let textParts = req.body.data.split(':');
                let iv = Buffer.from(textParts.shift(), 'hex');
                let encryptedText = Buffer.from(textParts.join(':'), 'hex');
                let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from('FHgk2GAT5WLF6TBY5YM5GnbEgQhUpMNU'), iv);
                let decrypted = decipher.update(encryptedText);
                decrypted = Buffer.concat([decrypted, decipher.final()]);
                req.body = JSON.parse(decrypted.toString());
              } else {
                return res.status(405).json({ status: false, message: 'Invalid request'});
              }
            }
            next();
          }
        }
        else if (req.method.toLocaleLowerCase() !== 'options') {
          return res.status(405).json({ status: false, message: 'Authenticatd requests only, please!' });
        }
        else {
          next();
        }
      }
      else if (req.method.toLocaleLowerCase() !== 'options') {
        return res.status(405).json({ status: false, message: 'Authenticatd requests only, please!' });
      }
      else {
        next();
      }
    }
  }
  else {
    next();
  }

});

app.use(mung.json(
  function transform(body, req, res) {
    if (req.originalUrl.indexOf("/v1/website/baseEncode") > -1) {
      return body;
    }else{
      const ENCRYPTION_KEY = "FHgk2GAT5WLF6TBY5YM5GnbEgQhUpMNU";
      const IV_LENGTH = 16;
      let iv = crypto.randomBytes(IV_LENGTH);
      let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
      let encrypted = cipher.update(JSON.stringify(body));
      encrypted = Buffer.concat([encrypted, cipher.final()]);
      return { fp_data: iv.toString('hex') + ':' + encrypted.toString('hex') };
    }
  }
));
// mount api v1 routes
app.use('/v1', routes);
// app.use(express.static(path.join(__dirname, '../public')));
// app.get('*', (req,res) =>{
//   res.sendFile((path.join(__dirname, '../../client/build/index.html')));
// });

app.get('*.js', function (req, res, next) {
  req.url = req.url + '.gz';
  res.set('Content-Encoding', 'gzip');
  res.set('Content-Type', 'text/javascript');
  next();
});

app.get('*.css', function (req, res, next) {
  req.url = req.url + '.gz';
  res.set('Content-Encoding', 'gzip');
  res.set('Content-Type', 'text/css');
  next();
});

app.use(express.static(root));
app.use(express.static(path.join(__dirname, '../public')));


app.get('/admin*', (req, res) => {
  res.sendFile('index.html', { root });
});

app.get('*', (req, res) => {
  res.sendFile('index.html', { root });
});

// if error is not an instanceOf APIError, convert it.
app.use(error.converter);

// catch 404 and forward to error handler
app.use(error.notFound);

// error handler, send stacktrace only during development
app.use(error.handler);

module.exports = app;
